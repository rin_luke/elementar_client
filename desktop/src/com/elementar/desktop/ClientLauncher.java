package com.elementar.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Graphics;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Window;
import com.elementar.gameserver.GSGUITier;
import com.elementar.gui.GUITier;
import com.elementar.gui.widgets.IDisplayTranslator;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.ExecutionMode;
import com.elementar.mainserver.MainserverApplication;

public class ClientLauncher {

	public static void main(String[] arg) {

		if (Shared.EXECUTION_MODE == ExecutionMode.CLIENT)
			startClient();
		else if (Shared.EXECUTION_MODE == ExecutionMode.GAMESERVER)
			startGameserver();
		else if (Shared.EXECUTION_MODE == ExecutionMode.MAINSERVER)
			startMainserver();

	}

	private static void startClient() {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		config.setTitle("Elementar");
		config.setIdleFPS(60);
		config.setForegroundFPS(60);
		config.useVsync(true);
		config.setDecorated(true);
//		config.setAutoIconify(true);
		config.setMaximized(true);
		config.setResizable(true);
		config.setWindowIcon("graphic/icon16.png", "graphic/icon32.png", "graphic/icon128.png");
		new Lwjgl3Application(new GUITier(), config);

	}

	private static void startGameserver() {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();

		config.setTitle("Elementar Game Server");
		config.useVsync(false);
		config.setDecorated(false);
		config.setIdleFPS(60);
		config.setMaximized(true);
		config.setResizable(false);
//			config.setWindowIcon("graphic/icon16.png", "graphic/icon32.png", "graphic/icon128.png");

		GSGUITier gsGuiTier = new GSGUITier();
		GSGUITier.setWindowTranslator(new IDisplayTranslator() {

			@Override
			public void translate(float x, float y) {
				Lwjgl3Window window = ((Lwjgl3Graphics) Gdx.graphics).getWindow();
				window.setPosition(window.getPositionX() + Gdx.input.getX() - (int) x,
						window.getPositionY() + Gdx.input.getY() - (int) y);
			}
		});

		new Lwjgl3Application(gsGuiTier, config);
	}

	private static void startMainserver() {
		new HeadlessApplication(new MainserverApplication());
	}

}
