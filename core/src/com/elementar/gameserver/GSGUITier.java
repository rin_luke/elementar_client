package com.elementar.gameserver;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.elementar.data.DataTier;
import com.elementar.data.container.StaticGraphic;
import com.elementar.gui.AGUITier;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.util.CustomStage;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.widgets.IDisplayTranslator;
import com.elementar.logic.network.gameserver.GameserverLogic;
import com.elementar.logic.util.Shared;

public class GSGUITier extends AGUITier {

	{
		// Log.set(Log.LEVEL_DEBUG);
	}

	public static IDisplayTranslator DISPLAY_TRANSLATOR;

	private GameserverLogic gameserver_logic;

	public GSGUITier() {
		super();
	}

	public static void setWindowTranslator(IDisplayTranslator displayTranslator) {
		DISPLAY_TRANSLATOR = displayTranslator;
	}

	@Override
	public void create() {

		Shared.setPorts();

		DataTier.loadServer();

		Gdx.graphics.setCursor(
				Gdx.graphics.newCursor(StaticGraphic.gui_cursor1, OFFSET_CURSOR, OFFSET_CURSOR));

		Gdx.graphics.setWindowedMode((int) StaticGraphic.gui_server_bg.getWidth(),
				(int) StaticGraphic.gui_server_bg.getHeight());

		ExtendViewport viewport = new ExtendViewport(StaticGraphic.gui_server_bg.getWidth(),
				StaticGraphic.gui_server_bg.getHeight());

		stage_viewport = viewport;

		ShaderPolyBatch batch = new ShaderPolyBatch();

		CustomStage stage;

		for (int i = 0; i < 4; i++) {
			stage = new CustomStage(viewport, batch);
			stage.getRoot().setTransform(false); // TODO evtl. gefährlich
			STAGES.add(stage);
		}

		screen = new BasicScreen();

		AModule setupRoot = new GSSetupRoot();

		setupRoot.setIntro();

		changeRootModule(setupRoot);
		this.setScreen(screen);

	}

	@Override
	public void dispose() {
		super.dispose();

		if (gameserver_logic != null) {
			gameserver_logic.getWorld().dispose();
			gameserver_logic.getGameserverClientConnection().getServer().close();
		}
	}

	@Override
	public void render() {
		super.render();

		if (gameserver_logic != null)
			gameserver_logic.update();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
	}

	public GameserverLogic getGameserverLogic() {
		return gameserver_logic;
	}

	public void setGameserverLogic(GameserverLogic gameserverLogic) {

		// dispose old server if it's set
		if (gameserver_logic != null) {
			gameserver_logic.getWorld().dispose();
			gameserver_logic.getGameserverClientConnection().getServer().close();

			gameserver_logic = null;
		}

		gameserver_logic = gameserverLogic;
	}

}