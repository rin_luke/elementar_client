package com.elementar.gameserver;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.modules.AYesNoConfirmationModule;
import com.elementar.gui.widgets.ConsoleWidget;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.network.gameserver.GameserverLogic;
import com.elementar.logic.util.Shared;

public class GSMonitoringRoot extends AModule {

	private AYesNoConfirmationModule confirmation_module;

	private DragButton drag_button;

	private CustomIconbutton bg_icon;
	private ConsoleWidget console_widget;

	private CustomLabel console_label, name_label, ip_label, password_label, players_label,
			seed_label, uptime_label;

	private CustomIconbutton leaf_icon, line_icon, close_server_button;

	private GameserverLogic gameserver_logic;

	private String time_text_parsed;

	public GSMonitoringRoot() {
		super(true, true, BasicScreen.DRAW_REGULAR);
		gameserver_logic = ((GSGUITier) gui_tier).getGameserverLogic();
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		console_widget.getScrollPane().layout();

		// update #players
		players_label.setText(
				TextData.getWord("players") + ": " + GameserverLogic.PLAYER_MAP.values().size()
						+ " / " + gameserver_logic.getMetaDataGameserver().capacity);

		// update time label
		float time = gameserver_logic.getTime();
		int seconds = (int) (gameserver_logic.getTime() % 60);
		// represented as: ['minutes' : '0 (if seconds < 10) + seconds']
		time_text_parsed = (int) ((time) / 60f) + ":" + (seconds < 10 ? "0" : "") + "" + seconds;
		uptime_label.setText(TextData.getWord("uptime") + ": " + time_text_parsed);

		// update console
		console_widget.update(gameserver_logic, time_text_parsed);

	}

	@Override
	public void loadWidgets() {
		bg_icon = new CustomIconbutton(StaticGraphic.gui_server_bg,
				StaticGraphic.gui_server_bg.getWidth(), StaticGraphic.gui_server_bg.getHeight());

		// header
		leaf_icon = new CustomIconbutton(StaticGraphic.gui_server_icon_leaf,
				StaticGraphic.gui_server_icon_leaf.getWidth(),
				StaticGraphic.gui_server_icon_leaf.getHeight());
		console_label = new CustomLabel(TextData.getWord("monitoring"),
				StyleContainer.label_bold_white_30_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.left);

		// meta section labels
		name_label = new CustomLabel(gameserver_logic.getMetaDataGameserver().server_name,
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH9, Shared.HEIGHT1,
				Align.center);
		ip_label = new CustomLabel(
				TextData.getWord("ip") + ": " + gameserver_logic.getMetaDataGameserver().ip,
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.left);
		password_label = new CustomLabel(
				TextData.getWord("passwordConsole") + ": "
						+ gameserver_logic.getMetaDataGameserver().password,
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.left);
		// text will be set continuously in update()
		players_label = new CustomLabel("", StyleContainer.label_bold_whitepure_18_style,
				Shared.WIDTH3, Shared.HEIGHT1, Align.center);
		uptime_label = new CustomLabel(TextData.getWord("uptime"),
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.center);
		seed_label = new CustomLabel(
				TextData.getWord("seed") + ": " + gameserver_logic.getMetaDataGameserver().seed,
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.left);

		line_icon = new CustomIconbutton(StaticGraphic.gui_server_line,
				StaticGraphic.gui_server_line.getWidth(),
				StaticGraphic.gui_server_line.getHeight());

		console_widget = new ConsoleWidget();

		drag_button = new DragButton();

		close_server_button = new CustomIconbutton(StaticGraphic.gui_server_leave,
				StaticGraphic.gui_server_leave.getWidth(),
				StaticGraphic.gui_server_leave.getHeight());

	}

	@Override
	public void setLayout() {

		// bg
		CustomTable tableBG = new CustomTable();
		tableBG.setFillParent(true);
		tables.add(tableBG);

		tableBG.add(bg_icon);

		// title console
		CustomTable tableTitle = new CustomTable();
		tableTitle.top().padTop(GSShared.PAD_TOP_TITLE).setFillParent(true);
		tables.add(tableTitle);

		tableTitle.add(leaf_icon).padRight(GSShared.PAD_RIGHT_ICON_TITLE);
		tableTitle.add(console_label).padRight(GSShared.PAD_RIGHT_TITLE);

		// drag
		CustomTable tableDrag = new CustomTable();
		tableDrag.top().setFillParent(true);
		tables.add(tableDrag);

		tableDrag.add(drag_button).expandX().fillX();

		// console
		CustomTable tableConsole = new CustomTable();
		tableConsole.bottom().left().setFillParent(true);
		tables.add(tableConsole);

		float padBottom = 30;
		float padTop = 360;
		float padLeft = 150, padRight = 150;

		tableConsole.padBottom(padBottom).padTop(padTop).padLeft(padLeft).padRight(padRight)
				.defaults().expand().fill();
		tableConsole.add(console_widget.getScrollPane());

		// meta section
		CustomTable tableMetaSection = new CustomTable();
		tableMetaSection.top().padTop(110).setFillParent(true);
		tables.add(tableMetaSection);

		tableMetaSection.add(name_label).colspan(4);
		tableMetaSection.row().padTop(25);
		tableMetaSection.add(ip_label).padRight(50);
		tableMetaSection.add(players_label);
		tableMetaSection.add();
		tableMetaSection.add(seed_label);
		tableMetaSection.row().padTop(25);
		tableMetaSection.add(password_label).padRight(50);
		tableMetaSection.add(uptime_label);
		tableMetaSection.row().padTop(25);
		tableMetaSection.add(line_icon).colspan(4);

		// close server button
		CustomTable tableClosebutton = new CustomTable();
		tableClosebutton.top().right().padTop(4f).padRight(4f).setFillParent(true);
		tables.add(tableClosebutton);
		tableClosebutton.add(close_server_button);

	}

	@Override
	public void setListeners() {

		close_server_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				confirmation_module.setVisibleGlobal(true);
			}
		});

	}

	@Override
	public void loadSubModules() {
		confirmation_module = new AYesNoConfirmationModule(TextData.getWord("closeServer"),
				BasicScreen.DRAW_COVERMODULE_1) {

			@Override
			public void clickedYes() {
				console_widget.clear();
				((GSGUITier) gui_tier).setGameserverLogic(null);
				Gdx.app.exit();
			}
		};
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		subModules.add(confirmation_module);
	}

	@Override
	protected boolean clickedEscape() {
		return true;
	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(close_server_button);
		return new HoverHandler(widgets);
	}

}
