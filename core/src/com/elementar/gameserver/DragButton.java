package com.elementar.gameserver;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.elementar.gui.widgets.CustomTextbutton;

public class DragButton extends CustomTextbutton {
	public DragButton() {
		super();
		setHeight(74);
		addListener(new InputListener() {
			float offset_x;
			float offset_y;

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {

				offset_x = Gdx.input.getX();
				offset_y = Gdx.input.getY();

				// true = using touchDragged
				return true;
			}

			@Override
			public void touchDragged(InputEvent event, float x, float y, int pointer) {
				GSGUITier.DISPLAY_TRANSLATOR.translate(offset_x, offset_y);

				// super.touchDragged(event, x, y, pointer);
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				offset_x = offset_y = 0;
				super.touchUp(event, x, y, pointer, button);
			}

		});
	}
}
