package com.elementar.gameserver;

import static com.elementar.data.container.StyleContainer.slider_enabled_style;
import static com.elementar.logic.util.Shared.MAX_GAMESERVERNAME_LENGTH;
import static com.elementar.logic.util.Shared.MAX_SERVER_SIZE;
import static com.elementar.logic.util.Shared.MIN_SERVER_SIZE;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomSlider;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextfield;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.SeedTextfield;
import com.elementar.gui.widgets.TooltipTextArea;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.listener.TooltipListener;
import com.elementar.logic.network.gameserver.GameserverLogic;
import com.elementar.logic.network.gameserver.MetaDataGameserver;
import com.elementar.logic.network.util.NetworkUtil;
import com.elementar.logic.util.Shared;

public class GSSetupRoot extends AModule {

	// drag
	private DragButton drag_button;

	// close
	private CustomIconbutton close_app_button;

	// config
	private CustomSlider number_players_slider;
	private CustomLabel setup_label, name_label, password_label, map_seed_label, max_players_label;
	private CustomTextfield name_field, password_field;
	private SeedTextfield map_seed_field;
	private CustomIconbutton start_server_button, portframe_icon;
	private CustomIconbutton bg_icon, leaf_icon, question_mark_icon;

	// ports
	private CustomLabel ports_label, port_tcp_game_label, port_udp_game_label, how_to_host_label;
	private CustomTextfield port_tcp_game_field, port_udp_game_field;
	private TooltipTextArea how_to_host_tooltip;

	private GSGUITier gs_gui_tier;

	public GSSetupRoot() {
		super(true, true, BasicScreen.DRAW_REGULAR);
		gs_gui_tier = (GSGUITier) gui_tier;
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		if (gs_gui_tier.getGameserverLogic() != null) {
			GSMonitoringRoot consoleRoot = new GSMonitoringRoot();
			consoleRoot.setIntro();
			gui_tier.changeRootModule(consoleRoot);
		}
	}

	@Override
	public void loadWidgets() {
		// slider
		number_players_slider
				= new CustomSlider(MIN_SERVER_SIZE, MAX_SERVER_SIZE, 2, slider_enabled_style);

		number_players_slider.setValue(Shared.DEFAULT_SERVER_SIZE);

		// labels
		setup_label = new CustomLabel(TextData.getWord("serverSetup"),
				StyleContainer.label_bold_white_30_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.left);
		name_label = new CustomLabel(TextData.getWord("serverName"),
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.right);
		ports_label = new CustomLabel(TextData.getWord("ports"),
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.center);
		password_label = new CustomLabel(TextData.getWord("password"),
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.right);
		port_tcp_game_label = new CustomLabel(TextData.getWord("portTCPGame"),
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.right);
		port_udp_game_label = new CustomLabel(TextData.getWord("portUDPGame"),
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.right);
		map_seed_label = new CustomLabel(TextData.getWord("mapSeed"),
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.right);
		max_players_label = new CustomLabel(
				TextData.getWord("maxPlayers") + ": " + (int) (number_players_slider.getValue()),
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.right);
		how_to_host_label = new CustomLabel(TextData.getWord("howToHost"),
				StyleContainer.label_semibold_ivory2_14_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.right);

		Sprite background = StaticGraphic.gui_server_inputfield;
		name_field = new CustomTextfield(background, MAX_GAMESERVERNAME_LENGTH);
		password_field = new CustomTextfield(background, 20, TextData.getWord("optional"));
		map_seed_field = new SeedTextfield(background, TextData.getWord("optional"));

		Sprite background2 = StaticGraphic.gui_server_inputfield_ports;
		port_tcp_game_field = new CustomTextfield(background2, 20);
		port_udp_game_field = new CustomTextfield(background2, 20);

		// TODO das muss nachher raus
		port_tcp_game_field.setText("40001");
		port_udp_game_field.setText("40002");

		portframe_icon = new CustomIconbutton(StaticGraphic.gui_host_portframe,
				StaticGraphic.gui_host_portframe.getWidth(),
				StaticGraphic.gui_host_portframe.getHeight());

		start_server_button = new CustomIconbutton(StaticGraphic.gui_server_start,
				StaticGraphic.gui_server_start.getWidth(),
				StaticGraphic.gui_server_start.getHeight());
		start_server_button.setSound(ClickSoundType.HEAVY);

		leaf_icon = new CustomIconbutton(StaticGraphic.gui_server_icon_leaf,
				StaticGraphic.gui_server_icon_leaf.getWidth(),
				StaticGraphic.gui_server_icon_leaf.getHeight());

		bg_icon = new CustomIconbutton(StaticGraphic.gui_server_bg,
				StaticGraphic.gui_server_bg.getWidth(), StaticGraphic.gui_server_bg.getHeight());

		question_mark_icon = new CustomIconbutton(StaticGraphic.gui_server_icon_help,
				StaticGraphic.gui_server_icon_help.getWidth(),
				StaticGraphic.gui_server_icon_help.getHeight());

		how_to_host_tooltip = new TooltipTextArea(TextData.getWord("hostDesc"), Shared.WIDTH6);

		drag_button = new DragButton();

		close_app_button = new CustomIconbutton(StaticGraphic.gui_server_leave,
				StaticGraphic.gui_server_leave.getWidth(),
				StaticGraphic.gui_server_leave.getHeight());

	}

	@Override
	public void setLayout() {

		// bg
		CustomTable tableBG = new CustomTable();
		tableBG.setFillParent(true);
		tables.add(tableBG);

		tableBG.add(bg_icon);

		// port frame
		CustomTable tablePortframe = new CustomTable();
		tablePortframe.padLeft(2 * 312f).padBottom(2 * 52f).setFillParent(true);
		tables.add(tablePortframe);
		tablePortframe.add(portframe_icon);

		// title
		CustomTable tableTitle = new CustomTable();
		tableTitle.top().padTop(GSShared.PAD_TOP_TITLE).setFillParent(true);
		tables.add(tableTitle);

		tableTitle.add(leaf_icon).padRight(GSShared.PAD_RIGHT_ICON_TITLE);
		tableTitle.add(setup_label).padRight(GSShared.PAD_RIGHT_TITLE);

		// drag
		CustomTable tableDrag = new CustomTable();
		tableDrag.top().setFillParent(true);
		tables.add(tableDrag);

		tableDrag.add(drag_button).expandX().fillX();

		// ports
		CustomTable tablePorts = new CustomTable();
		tablePorts.padLeft(500f).padBottom(100f).setFillParent(true);
		tables.add(tablePorts);
		tablePorts.columnDefaults(1).spaceLeft(DISTANCE_BETWEEN_ACTORS * 0.5f);
		tablePorts.columnDefaults(2).spaceLeft(DISTANCE_BETWEEN_ACTORS * 0.5f);
		tablePorts.add(port_tcp_game_label);
		tablePorts.add(port_tcp_game_field);
		tablePorts.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);
		tablePorts.add(port_udp_game_label);
		tablePorts.add(port_udp_game_field);
		tablePorts.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);

		// config
		CustomTable tableConfig = new CustomTable();
		tableConfig.padRight(500f).padBottom(110).setFillParent(true);
		tables.add(tableConfig);
		tableConfig.columnDefaults(1).spaceLeft(DISTANCE_BETWEEN_ACTORS * 0.5f);
		tableConfig.add(name_label);
		tableConfig.add(name_field);
		tableConfig.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.4f);
		tableConfig.add(password_label);
		tableConfig.add(password_field);
		tableConfig.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.4f);
		tableConfig.add(max_players_label);
		tableConfig.add(number_players_slider);
		tableConfig.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.4f);
		tableConfig.add(map_seed_label);
		tableConfig.add(map_seed_field);

		// ports header
		CustomTable tableHeaderPorts = new CustomTable();
		tableHeaderPorts.padLeft(620f).padBottom(290f).setFillParent(true);
		tables.add(tableHeaderPorts);
		tableHeaderPorts.add(ports_label);

		// start server button
		CustomTable tableStartbutton = new CustomTable();
		tableStartbutton.padTop(390f).padLeft(1f).setFillParent(true);
		tables.add(tableStartbutton);
		tableStartbutton.add(start_server_button);

		// close app button
		CustomTable tableClosebutton = new CustomTable();
		tableClosebutton.top().right().padTop(4f).padRight(4f).setFillParent(true);
		tables.add(tableClosebutton);
		tableClosebutton.add(close_app_button);

		// how to host
		CustomTable tableHowTo = new CustomTable();
		tableHowTo.padTop(500).padRight(70).setFillParent(true);
		tables.add(tableHowTo);
		tableHowTo.add(how_to_host_label).padRight(20);
		tableHowTo.add(question_mark_icon).left();
		how_to_host_tooltip.setPosition(tables, tableHowTo, question_mark_icon);

	}

	@Override
	public void setListeners() {
		number_players_slider.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				max_players_label.setText(TextData.getWord("maxPlayers") + ": "
						+ (int) (number_players_slider.getVisualValue()));
			}
		});
		start_server_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {

				// JarExecutor executor = new JarExecutor();
				// try {
				// executor.executeJar();
				// } catch (Exception e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

				int serverSize = (int) (number_players_slider.getVisualValue());
				int portTCP = Integer.valueOf(port_tcp_game_field.getText());
				int portUDPGame = Integer.valueOf(port_udp_game_field.getText());
				MetaDataGameserver mdgs = new MetaDataGameserver(NetworkUtil.getExternalIP(),
						portTCP, portUDPGame, name_field.getText(), password_field.getText(),
						serverSize, GUIUtil.getValidSeed(map_seed_field));

				gs_gui_tier.setGameserverLogic(new GameserverLogic(mdgs));

			};
		});

		question_mark_icon.addListener(new TooltipListener(how_to_host_tooltip));

		close_app_button.addListener(new ClickListener() {
			
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit();
			}
		});

	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(number_players_slider);
		widgets.add(name_field);
		widgets.add(password_field);
		widgets.add(map_seed_field);
		widgets.add(port_tcp_game_field);
		widgets.add(port_udp_game_field);
		widgets.add(start_server_button);
		widgets.add(close_app_button);

		return new HoverHandler(widgets);
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

	@Override
	protected boolean clickedEscape() {
		return true;
	}

	@Override
	public void unfocus() {
		name_field.unfocusWidget();
		password_field.unfocusWidget();
		map_seed_field.unfocusWidget();
		port_tcp_game_field.unfocusWidget();
		port_udp_game_field.unfocusWidget();
	}

}
