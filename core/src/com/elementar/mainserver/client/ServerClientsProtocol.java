package com.elementar.mainserver.client;

import com.elementar.logic.network.requests.CreateGameserverRequest;
import com.elementar.logic.network.requests.ServerListRequest;
import com.elementar.logic.network.response.CreateGameserverResponse;
import com.elementar.logic.network.response.JoinSuccessfulToMainserverResponse;
import com.elementar.logic.network.response.ServerlistResponse;
import com.elementar.mainserver.gameserver.ServerGameServersProtocol;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

public class ServerClientsProtocol extends Listener {

	@Override
	public void connected(Connection arg0) {
		super.connected(arg0);
		arg0.sendTCP(new JoinSuccessfulToMainserverResponse(arg0.getID()));
	}

	@Override
	public void received(Connection connection, Object object) {
		if (object instanceof CreateGameserverRequest)
			connection.sendTCP(new CreateGameserverResponse(
					((CreateGameserverRequest) object).metadata_gameserver));

		if (object instanceof ServerListRequest) {
			connection.sendTCP(new ServerlistResponse(ServerGameServersProtocol.GAMESERVER_LIST));
		}

	}
}
