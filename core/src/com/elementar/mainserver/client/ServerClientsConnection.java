package com.elementar.mainserver.client;

import com.elementar.logic.network.util.ConnectionEstablisher;
import com.elementar.logic.util.Shared;
import com.esotericsoftware.kryonet.Server;

public class ServerClientsConnection {

	public ServerClientsConnection() {
		ConnectionEstablisher.serverInitialize(new Server(), Shared.CLIENT_TCP_TO_MAIN, Shared.CLIENT_UDP_TO_MAIN,
				new ServerClientsProtocol());
	}

}
