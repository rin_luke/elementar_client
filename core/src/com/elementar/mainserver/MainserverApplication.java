package com.elementar.mainserver;

import java.util.Iterator;

import com.badlogic.gdx.ApplicationListener;
import com.elementar.logic.network.gameserver.MetaDataGameserver;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.elementar.mainserver.client.ServerClientsConnection;
import com.elementar.mainserver.gameserver.ServerGameServersConnection;
import com.elementar.mainserver.gameserver.ServerGameServersProtocol;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.minlog.Log;

public class MainserverApplication implements ApplicationListener {
	private ServerGameServersConnection server_gameservers_connection;

	private Iterator<MetaDataGameserver> it;

	public void create() {
		
		Log.set(Log.LEVEL_DEBUG);
		
		Shared.setPorts();
		
		new ServerClientsConnection();
		server_gameservers_connection = new ServerGameServersConnection(this);

	}

	public void dispose() {
	}

	public void render() {
		/*
		 * game server list gets updated constantly. On each request the newest list is sent.
		 */

		deleteDisconnectedGameServer();

		Util.sleep(3000);
	}

	private boolean remove;

	/**
	 * A disconnected server might be still in
	 * {@link ServerGameServersProtocol#GAMESERVER_LIST}
	 */
	private void deleteDisconnectedGameServer() {
		synchronized (ServerGameServersProtocol.GAMESERVER_LIST) {
			it = ServerGameServersProtocol.GAMESERVER_LIST.iterator();
			MetaDataGameserver mdgs;
			while (it.hasNext() == true) {
				mdgs = it.next();
				remove = true;
				synchronized (server_gameservers_connection.getServer()
						.getConnections()) {
					for (Connection connection : server_gameservers_connection
							.getServer().getConnections()) {
						if (mdgs.connection_id == connection.getID())
							remove = false;
					}
				}
				if (remove == true)
					it.remove();
			}
		}
	}

	public void resize(int width, int height) {
	}

	public void pause() {
	}

	public void resume() {
	}

}
