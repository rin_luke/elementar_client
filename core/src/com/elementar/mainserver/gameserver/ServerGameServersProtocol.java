package com.elementar.mainserver.gameserver;

import com.elementar.logic.network.gameserver.MetaDataGameserver;
import com.elementar.logic.network.requests.GameserverJoinRequest;
import com.elementar.logic.network.response.JoinSuccessfulToGameserverResponse;
import com.elementar.logic.util.lists.GameserverArrayList;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

public class ServerGameServersProtocol extends Listener {

	public static GameserverArrayList<MetaDataGameserver> GAMESERVER_LIST = new GameserverArrayList<MetaDataGameserver>();

	@Override
	public void received(Connection connection, Object object) {
		if (object instanceof GameserverJoinRequest)
			handleGameServerJoinRequest(connection, object);
	}

	/**
	 * diese metode wird benötigt wenn man mit dem host befehle senden will an den
	 * mainserver betreffend seines servers und den server ermitteln will.
	 * 
	 * @param id
	 * @return
	 */
	private MetaDataGameserver getGameserverByID(int id) {
		synchronized (GAMESERVER_LIST) {
			for (MetaDataGameserver metaDataGameServer : GAMESERVER_LIST)
				if (metaDataGameServer.connection_id == id)
					return metaDataGameServer;
		}
		return null;
	}

	private void handleGameServerJoinRequest(Connection connection, Object object) {
		MetaDataGameserver mdgs = ((GameserverJoinRequest) object).metadata_gameserver;
		mdgs.connection_id = connection.getID();
		mdgs.has_password = mdgs.password.equals("") == false;
		mdgs.password = "";
		synchronized (GAMESERVER_LIST) {
			GAMESERVER_LIST.add(mdgs);
		}
		synchronized (connection) {
			connection.sendTCP(new JoinSuccessfulToGameserverResponse());
		}
	}
}
