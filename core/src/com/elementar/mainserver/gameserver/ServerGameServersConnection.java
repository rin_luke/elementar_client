package com.elementar.mainserver.gameserver;

import com.elementar.logic.network.connection.AServerConnection;
import com.elementar.logic.network.util.ConnectionEstablisher;
import com.elementar.logic.util.Shared;
import com.elementar.mainserver.MainserverApplication;

/**
 * Connection between mainserver and gameserver
 * 
 * @author lukassongajlo
 * 
 */
public class ServerGameServersConnection extends AServerConnection {

	public ServerGameServersConnection(MainserverApplication mainThread) {
		ConnectionEstablisher.serverInitialize(server, Shared.GAMESERVER_TCP_TO_MAIN,
				Shared.GAMESERVER_UDP_TO_MAIN, new ServerGameServersProtocol());
	}

}
