package com.elementar.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.elementar.data.DataTier;
import com.elementar.data.container.StaticGraphic;
import com.elementar.gui.abstracts.ARootWorldModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.modules.roots.IntroRoot;
import com.elementar.gui.util.CustomStage;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.util.SteamInterface;
import com.elementar.logic.LogicTier;
import com.elementar.logic.network.connection.ClientGameserverConnection;
import com.elementar.logic.network.connection.ClientMainserverConnection;
import com.elementar.logic.util.Shared;

public class GUITier extends AGUITier {

	public static SteamInterface STEAM_INTERFACE;

	public static int SCREEN_HEIGHT, SCREEN_WIDTH;

	{

//		Log.set(Log.LEVEL_TRACE);
		DisplayMode displayMode = Lwjgl3ApplicationConfiguration.getDisplayMode();

		SCREEN_HEIGHT = displayMode.height;
		SCREEN_WIDTH = displayMode.width;

		// SCREEN_HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height;
		// SCREEN_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width;

	}

	public GUITier() {
		super();
	}

	@Override
	public void create() {

		if (Shared.STEAM_ON == true)
			STEAM_INTERFACE = new SteamInterface();

		Shared.setPorts();

		logic_tier = new LogicTier();

		StaticGraphic.loadCursorImages();

		cursor_invisible = Gdx.graphics.newCursor(StaticGraphic.gui_cursor_transparent, 0, 0);
		cursor_regular = Gdx.graphics.newCursor(StaticGraphic.gui_cursor1, OFFSET_CURSOR, OFFSET_CURSOR);
		cursor_world = Gdx.graphics.newCursor(StaticGraphic.gui_cursor_world_regular1, OFFSET_CURSOR, OFFSET_CURSOR);

		setCursor(cursor_regular);

		// Create stages now there are 4 stages:
		// 1 - regular
		// 2 - notifications
		// 3 - cover1 (i.e. optionsscreen)
		// 4 - cover2 (i.e. confirmationscreen)

		ExtendViewport viewport = new ExtendViewport(1920, 1080);

		stage_viewport = viewport;

		ShaderPolyBatch batch = new ShaderPolyBatch();
		CustomStage stage;
		for (int i = 0; i < 4; i++) {
			stage = new CustomStage(viewport, batch);
			stage.getRoot().setTransform(false); // TODO evtl. gefährlich
			STAGES.add(stage);
		}
		screen = new BasicScreen();

		IntroRoot introRoot = new IntroRoot();

		introRoot.setIntro();

		changeRootModule(introRoot);
		this.setScreen(screen);

	}

	@Override
	public void resume() {
		super.resume();

		if (screen.getRootModule() instanceof ARootWorldModule)
			setCursor(cursor_world);
		else
			setCursor(cursor_regular);
	}

	@Override
	public void dispose() {
		super.dispose();
		
		DataTier.dispose();
		
		logic_tier.disposeGameserverLogic();
		ClientMainserverConnection.getInstance().disconnect();
		ClientGameserverConnection.getInstance().disconnect();
	}

}
