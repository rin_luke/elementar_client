package com.elementar.gui.modules;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StyleContainer;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.ARootWorldModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.util.Shared;

/**
 * This module show FPS, outgoing bytes per seconds, etc. and is a cover2 module
 * to guarantee that it has the highest draw priority.
 * 
 * @author lukassongajlo
 * 
 */
public class StatsModule extends AChildModule {

	private CustomLabel fps_label, bytes_sent_label, correction_label, server_input_queue_label, vel_label;
	private AtomicInteger bytes_sent, correction_count;

	private float accumulator;

	public StatsModule(boolean visible) {
		super(visible, BasicScreen.DRAW_COVERMODULE_2);
	}

	/**
	 * at the beginning of {@link ARootWorldModule} the integer reference is set
	 * 
	 * @param bytesSentRef
	 */
	public void setBytesSent(AtomicInteger bytesSentRef) {
		this.bytes_sent = bytesSentRef;
	}

	public void setCorrectionCount(AtomicInteger correctionCount) {
		correction_count = correctionCount;
	}

	@Override
	public void update(float delta) {

		accumulator += delta;
		while (accumulator >= 1f) {
			accumulator -= 1f;

			fps_label.setText(String.valueOf(Gdx.graphics.getFramesPerSecond()) + " fps");

//			if (bytes_sent != null && bytes_sent.get() > 0) {
//				int bytes = bytes_sent.get();
//				int kbits = (bytes * 8) / 1000;
//				bytes_sent_label.setText("out: " + kbits + " Kbps");
//				bytes_sent.set(0);
//			}
		}

//		if (correction_count != null)
//			correction_label.setText("correction: " + correction_count);

//		if (LogicTier.WORLD_PLAYER_MAP != null) {
//			String s = "vel factors: ";
//			synchronized (LogicTier.WORLD_PLAYER_MAP) {
//				for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {
//					if (client.getMetaClient().gameclass != null)
//						s += "\nclient = " + client.getMetaClient().name + ", vel = "
//								+ client.getPhysicalClient().getVelXFactor();
//				}
//			}
//			vel_label.setText(s);
//		}

//		if (GameserverLogic.PLAYER_MAP != null) {
//
//			String s = "input_queues: ";
//			synchronized (GameserverLogic.PLAYER_MAP) {
//				for (ServerPhysicalClient client : GameserverLogic.PLAYER_MAP.values())
//					s += "\nclient = " + client.getMetaClient().name + ", size = " + client.getInputQueue().size();
//
//			}
//			server_input_queue_label.setText(s);
//		}

	}

	@Override
	public void loadWidgets() {
		fps_label = new CustomLabel("", StyleContainer.label_bold_whitepure_18_style, 150, 50, Align.center);
		bytes_sent_label = new CustomLabel("", StyleContainer.label_bold_whitepure_18_style, 150, 50, Align.center);
		correction_label = new CustomLabel("", StyleContainer.label_bold_whitepure_18_style, 150, 50, Align.center);
		server_input_queue_label = new CustomLabel("", StyleContainer.label_bold_whitepure_18_style, 150, 50,
				Align.center);
		vel_label = new CustomLabel("", StyleContainer.label_bold_whitepure_18_style, 150, Shared.HEIGHT4,
				Align.center);
	}

	@Override
	public void setLayout() {
		CustomTable table = new CustomTable();
		table.setFillParent(true);
		table.right().top().padTop(100).padRight(10);
		tables.add(table);

		table.add(fps_label);
		table.row();
//		table.add(bytes_sent_label);
//		table.row();
//		table.add(correction_label);
//		table.row();
//		table.add(server_input_queue_label).padRight(100);
//		table.row();
//		table.add(vel_label).padRight(100);

	}

	@Override
	public void setListeners() {
	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return new HoverHandler(widgets);
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

}
