package com.elementar.gui.modules;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AChildCoverModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.CustomTextfield;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.util.Shared;

public abstract class APasswordModule extends AChildCoverModule {

	private String password;
	private CustomLabel password_label;
	private CustomTextfield password_field;
	private CustomTextbutton join_button;

	public APasswordModule() {
		super(StaticGraphic.gui_bg_confirmation, BasicScreen.DRAW_COVERMODULE_1, true);
	}

	public abstract void clickedYes();

	@Override
	public void loadWidgets() {
		super.loadWidgets();

		password_label = new CustomLabel(TextData.getWord("password"), StyleContainer.label_medium_whitepure_20_style,
				Shared.WIDTH3, Shared.HEIGHT1, Align.center);

		Sprite background = StaticGraphic.gui_bg_inputfield_height1_width4;
		password_field = new CustomTextfield(background, 20, "");

		join_button = new CustomTextbutton(TextData.getWord("join"), StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		join_button.setSound(ClickSoundType.LIGHT);
	}

	@Override
	public void setLayout() {
		super.setLayout();

		table_clicksafe.padBottom(200);

		CustomTable table = new CustomTable();
		table.padBottom(80).setFillParent(true);
		tables.add(table);

		table.add(password_label);
		table.add(password_field);
		table.row().padTop(30).padBottom(20);
		table.add(join_button).colspan(2);

	}

	@Override
	public void setListeners() {
		super.setListeners();

		join_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				clickedYes();
				setVisibleGlobal(false);
				/*
				 * the exit method of the HoverListener isn't called after clicking this button,
				 * so we have to set its state manually
				 */
				join_button.setHovered(false);
			}
		});
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(join_button);
		widgets.add(password_field);
		return new HoverHandler(widgets);
	}

	@Override
	protected boolean clickedEscape() {
		setVisibleGlobal(false);
		return true;
	}

	@Override
	public void setVisibleGlobal(boolean visible) {
		super.setVisibleGlobal(visible);

		if (password_field != null) {
			password = password_field.getText();
			password_field.setText("");
		}
	}

	public String getPassword() {
		return password;
	}

	public void resetPassword() {
		password = "";
	}

}
