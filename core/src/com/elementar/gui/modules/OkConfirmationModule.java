package com.elementar.gui.modules;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AChildCoverModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.util.Shared;

public class OkConfirmationModule extends AChildCoverModule {

	private CustomLabel statement_label;
	private CustomTextbutton ok_button;

	public OkConfirmationModule(String statementText, int drawOrder) {
		super(StaticGraphic.gui_bg_confirmation, drawOrder, true);

		statement_label.setText(statementText);

	}

	public CustomLabel getStatementLabel() {
		return statement_label;
	}

	@Override
	public void loadWidgets() {
		super.loadWidgets();
		ok_button = new CustomTextbutton(TextData.getWord("ok"), StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		ok_button.setSound(ClickSoundType.LIGHT);

		statement_label = new CustomLabel("", StyleContainer.label_medium_whitepure_20_style,
				background_sprite.getWidth(), Shared.HEIGHT1, Align.center);
	}

	@Override
	public void setLayout() {
		super.setLayout();

		table_clicksafe.padBottom(200);

		CustomTable table = new CustomTable();
		table.padBottom(100).setFillParent(true);
		tables.add(table);

		table.add(statement_label);
		table.row().padTop(30).padBottom(20);
		table.add(ok_button);

	}

	@Override
	public void setListeners() {
		super.setListeners();

		ok_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				setVisibleGlobal(false);
				/*
				 * the exit method of the HoverListener isn't called after clicking this button,
				 * so we have to set its state manually
				 */
				ok_button.setHovered(false);
			}
		});
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(ok_button);
		return new HoverHandler(widgets);
	}

	@Override
	protected boolean clickedEscape() {
		return true;
	}

}
