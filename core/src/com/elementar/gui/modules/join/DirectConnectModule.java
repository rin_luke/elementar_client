package com.elementar.gui.modules.join;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.CustomTextfield;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;
import com.elementar.logic.LogicTier;
import com.elementar.logic.util.ErrorMessage;
import com.elementar.logic.util.Shared;

public class DirectConnectModule extends AChildModule {

	// ip
	private CustomLabel ip_label;
	private CustomTextfield ip_field;

	// ports
	private CustomLabel port_tcp_game_label, port_udp_game_label;
	private CustomTextfield port_tcp_game_field, port_udp_game_field;

	// connect
	private CustomTextbutton connect_button;

	public DirectConnectModule() {
		super(true);
	}

	@Override
	public void loadWidgets() {

		ip_label = new CustomLabel(TextData.getWord("ip"), StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3,
				Shared.HEIGHT1, Align.right);
		ip_field = new CustomTextfield(StaticGraphic.gui_bg_inputfield_height1_width3, 20);

		port_tcp_game_label = new CustomLabel(TextData.getWord("portTCPGame"),
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1, Align.right);
		port_udp_game_label = new CustomLabel(TextData.getWord("portUDPGame"),
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1, Align.right);

		Sprite background2 = StaticGraphic.gui_bg_inputfield_height1_width3;
		port_tcp_game_field = new CustomTextfield(background2, 20);
		port_udp_game_field = new CustomTextfield(background2, 20);

		connect_button = new CustomTextbutton(TextData.getWord("connect"), StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width3, Align.center) {
			@Override
			public boolean disabledCondition() {
				return ip_field.getText().equals("") == true || port_tcp_game_field.getText().equals("") == true
						|| port_udp_game_field.getText().equals("") == true;
			}
		};
		connect_button.setSound(ClickSoundType.LIGHT);

//		password_label = new CustomLabel(TextData.getWord("password"), StyleContainer.label_bold_whitepure_18_style,
//				Shared.WIDTH2, Shared.HEIGHT1, Align.left);
//		password_field = new CustomTextfield(StaticGraphic.gui_bg_inputfield_height1_width3, 20);
//		password_field.setPasswordMode(true);
//		password_field.setPasswordCharacter('*');

	}

	@Override
	public void setLayout() {
		// port frame
		CustomTable table = new CustomTable();
		table.padLeft(850f).padBottom(200f).setFillParent(true);
		tables.add(table);
		table.columnDefaults(1).spaceLeft(DISTANCE_BETWEEN_ACTORS * 0.3f);
		table.add(ip_label);
		table.add(ip_field);
		table.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.4f);
		table.add(port_tcp_game_label);
		table.add(port_tcp_game_field);
		table.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.4f);
		table.add(port_udp_game_label);
		table.add(port_udp_game_field);
		table.row().padTop(LINE_DISTANCE_TO_NEXT_ROW);
		table.add(connect_button).padLeft(125).colspan(2);
	}

	@Override
	public void setListeners() {

		connect_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {

				if (connect_button.getState() == State.DISABLED)
					return;

				String ip = ip_field.getText();
				String portTCPString = port_tcp_game_field.getText();
				String portUDPString = port_udp_game_field.getText();

				if (isIPAddressValid(ip) == false) {
					LogicTier.ERROR_MESSAGE = ErrorMessage.invalidIPAddress;
					return;
				}
				if (isInteger(portTCPString) == false) {
					LogicTier.ERROR_MESSAGE = ErrorMessage.invalidPorts;
					return;
				}
				if (isInteger(portUDPString) == false) {
					LogicTier.ERROR_MESSAGE = ErrorMessage.invalidPorts;
					return;
				}

				int portTCPGame = Integer.valueOf(portTCPString);
				int portUDPGame = Integer.valueOf(portUDPString);

//				client_gameserver_connection.connect(ip, portTCPGame, portUDPGame);

			};
		});
	}

	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}

	public static boolean isIPAddressValid(final String ip) {
		String pattern = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";

		return ip.matches(pattern);
	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(connect_button);
		widgets.add(ip_field);
		widgets.add(port_tcp_game_field);
		widgets.add(port_udp_game_field);

		return new HoverHandler(widgets);
	}

	@Override
	public void unfocus() {
		super.unfocus();
		ip_field.unfocusWidget();
		port_tcp_game_field.unfocusWidget();
		port_udp_game_field.unfocusWidget();
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

	public CustomTextfield getIPField() {
		return ip_field;
	}

}
