package com.elementar.gui.modules.ingame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.gui.abstracts.AChildCoverModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.widgets.AStatisticLabel;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomImage;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.map.MapLoader;
import com.elementar.logic.network.protocols.ClientGameserverProtocol;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.Team;

public class PlayerStatsModule extends AChildCoverModule {

	/**
	 * we use buttons to get the internal label padding
	 */
	private CustomTextbutton server_name_button, map_seed_button;
	private CustomLabel kills_label, deaths_label, assists_label, ping_label;

	private VerticalGroup player_group;

	/**
	 * holds all horizontal entries of the statistics
	 */
	private HashMap<Short, HorizontalGroup> statistic_entry_map = new HashMap<>();

	public PlayerStatsModule() {
		super(StaticGraphic.gui_bg_player_statistics, BasicScreen.DRAW_COVERMODULE_1, true);
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		// remove clients
		Iterator<Entry<Short, HorizontalGroup>> itEntries = statistic_entry_map.entrySet().iterator();
		while (itEntries.hasNext() == true) {
			Entry<Short, HorizontalGroup> statisticEntry = itEntries.next();
			HorizontalGroup group = statisticEntry.getValue();

			if (LogicTier.WORLD_PLAYER_MAP.containsKey(statisticEntry.getKey()) == false) {

				player_group.removeActor(group);
				itEntries.remove();
			} else {
				// last element of horizontal group is ping label
				CustomLabel pingLabel = (CustomLabel) group.getChild(group.getChildren().size - 1);
				OmniClient client = LogicTier.WORLD_PLAYER_MAP.get(statisticEntry.getKey());
				if (client != null && client.getMetaClient() != null)
					pingLabel.setText(LogicTier.WORLD_PLAYER_MAP.get(statisticEntry.getKey()).getMetaClient().ping);
			}
		}

	}

	@Override
	public void loadWidgets() {

		super.loadWidgets();

		server_name_button = new CustomTextbutton(
				ClientGameserverProtocol.GAMESERVER_NAME.equals("") == true ? TextData.getWord("testEnvironment")
						: ClientGameserverProtocol.GAMESERVER_NAME,
				StyleContainer.button_bold_18_style, Shared.WIDTH6, Shared.HEIGHT1, Align.left);

		map_seed_button = new CustomTextbutton(TextData.getWord("mapSeed") + ": " + MapLoader.SEED,
				StyleContainer.button_bold_18_style, Shared.WIDTH6, Shared.HEIGHT1, Align.left);

		kills_label = new CustomLabel(TextData.getWord("kills"), StyleContainer.label_bold_whitepure_18_style,
				Shared.WIDTH2, Shared.HEIGHT1, Align.center);
		deaths_label = new CustomLabel(TextData.getWord("deaths"), StyleContainer.label_bold_whitepure_18_style,
				Shared.WIDTH2, Shared.HEIGHT1, Align.center);
		assists_label = new CustomLabel(TextData.getWord("assists"), StyleContainer.label_bold_whitepure_18_style,
				Shared.WIDTH2, Shared.HEIGHT1, Align.center);
		ping_label = new CustomLabel(TextData.getWord("ping"), StyleContainer.label_bold_whitepure_18_style,
				Shared.WIDTH2, Shared.HEIGHT1, Align.center);

		player_group = new VerticalGroup();

	}

	public void setHorizontalEntry(OmniClient client) {

		HorizontalGroup horizontalEntry = new HorizontalGroup();
		Team team = client.getMetaClient().team;
		GameclassName gameclassName = client.getMetaClient().gameclass_name;

		if (team != null && gameclassName != null) {

			CustomIconbutton symbolIcon = new CustomIconbutton(client.getMetaClient().gameclass.getSymbolStatistics(),
					Shared.HEIGHT2, Shared.HEIGHT2);
			CustomImage bgTeamColor = new CustomImage(
					team == Team.TEAM_1 ? StaticGraphic.gui_bg_stats_team1 : StaticGraphic.gui_bg_stats_team2);
			bgTeamColor.setHeight(bgTeamColor.getSprite().getHeight());

			CustomLabel playerNameLabel = new CustomLabel(client.getMetaClient().name,
					StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH4, Shared.HEIGHT1, Align.left);
			CustomLabel gameclassNameLabel = new CustomLabel(client.getMetaClient().gameclass_name.toString(),
					StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH4, Shared.HEIGHT1, Align.left);

			VerticalGroup vGroup = new VerticalGroup().padLeft(35).padRight(35);
			vGroup.addActor(playerNameLabel);
			vGroup.addActor(gameclassNameLabel);

			AStatisticLabel killLabel = new AStatisticLabel(client) {
				@Override
				protected int getStatistic() {
					return this.client.getPhysicalClient().getKillCount();
				}
			};
			AStatisticLabel deathLabel = new AStatisticLabel(client) {
				@Override
				protected int getStatistic() {
					return this.client.getPhysicalClient().getDeathCount();
				}
			};
			AStatisticLabel assistsLabel = new AStatisticLabel(client) {
				@Override
				protected int getStatistic() {
					return this.client.getPhysicalClient().getAssistCount();
				}
			};
			AStatisticLabel crystalLabel = new AStatisticLabel(client) {
				@Override
				protected int getStatistic() {
					return this.client.getPhysicalClient().getCrystalCount();
				}
			};

			CustomLabel pingLabel = new CustomLabel("0", StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH2,
					Shared.HEIGHT2, Align.center);

			horizontalEntry.addActor(symbolIcon);
			horizontalEntry.addActor(bgTeamColor);
			horizontalEntry.addActor(vGroup);
			horizontalEntry.addActor(killLabel);
			horizontalEntry.addActor(deathLabel);
			horizontalEntry.addActor(assistsLabel);
			horizontalEntry.addActor(crystalLabel);
			horizontalEntry.addActor(pingLabel);

			player_group.addActor(horizontalEntry);

			statistic_entry_map.put(client.getMetaClient().connection_id, horizontalEntry);

		}
	}

	@Override
	public void setLayout() {

		super.setLayout();

		table_inner.top().left();
		table_inner.setPosition(inner_group.getX(), inner_group.getY() + inner_image.getSprite().getHeight());

		table_inner.add(server_name_button).colspan(2);
		table_inner.add(map_seed_button).colspan(5);
		table_inner.row();
		table_inner.add();
		table_inner.add();
		table_inner.add(kills_label);
		table_inner.add(deaths_label);
		table_inner.add(assists_label);
		table_inner.add().width(Shared.WIDTH2);
		table_inner.add(ping_label);
		table_inner.row();
		table_inner.add(player_group).colspan(7);

	}

	@Override
	public void setListeners() {

	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return null;
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

	public HashMap<Short, HorizontalGroup> getStatisticEntryMap() {
		return statistic_entry_map;
	}

}
