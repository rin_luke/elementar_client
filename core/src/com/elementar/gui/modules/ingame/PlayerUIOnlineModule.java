package com.elementar.gui.modules.ingame;

import com.elementar.gui.abstracts.ARootWorldModule;

public class PlayerUIOnlineModule extends APlayerUIModule {

	public PlayerUIOnlineModule(ARootWorldModule rootWorldModule) {
		super(rootWorldModule);
	}

	@Override
	protected String handleChatMessage(String chatMessage) {
		return chatMessage;
	}

}
