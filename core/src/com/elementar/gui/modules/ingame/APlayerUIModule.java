package com.elementar.gui.modules.ingame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.AudioData;
import com.elementar.data.container.KeysConfiguration;
import com.elementar.data.container.ParticleContainer;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.KeysConfiguration.WorldAction;
import com.elementar.data.container.util.AdvancedKey;
import com.elementar.data.container.util.Gameclass;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.ARootWorldModule;
import com.elementar.gui.modules.options.KeysModule;
import com.elementar.gui.modules.roots.WorldTrainingRoot;
import com.elementar.gui.widgets.ChatWindow;
import com.elementar.gui.widgets.ComboRose;
import com.elementar.gui.widgets.CustomEffectWidget;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.SkillButton;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.WidgetGroupable;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.ServerPhysicalClient;
import com.elementar.logic.characters.skills.SparseComboStorage;
import com.elementar.logic.network.gameserver.GameserverLogic;
import com.elementar.logic.network.protocols.ClientGameserverProtocol;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.Team;

public abstract class APlayerUIModule extends AChildModule {

	public static float SCALE_UI = 0.75f;

	private SkillButton comboslot1_button, comboslot2_button;
	private Cell<CustomTable> cell_skill_main, cell_skill_combo_feedback, cell_skill_combo1, cell_skill_combo2;

	private CustomLabel skill1_key_label, skill2_key_label;

	private CustomLabel time_label;

	private HorizontalGroup symbol_group_team1, symbol_group_team2;
	private CustomIconbutton symbolbar_bg_image;
	private CustomIconbutton skill_bg_image;

	private ChatWindow chat_window;

	private CustomIconbutton health_bar_UI_image, mana_bar_UI_image;
	private CustomLabel health_slash_separator_label, health_current_value_label, health_absolute_value_label,
			mana_slash_separator_label, mana_current_value_label, mana_absolute_value_label;

	private HashMap<Short, SymbolSlot> symbols_map = new HashMap<>();

	/**
	 * elapsed time within the match or training
	 */
	protected float time;

	protected ARootWorldModule world_module;
	protected PlayerStatsModule player_stats_module;

	private CustomLabel team1_remaining_lives_label, team2_remaining_lives_label;
	private CustomIconbutton team1_remaining_lives_image, team2_remaining_lives_image;
	private CustomIconbutton team1_remaining_lives_bar, team2_remaining_lives_bar;

	private CustomEffectWidget team1_kill_effect, team2_kill_effect;

	private ComboRose combo_rose;

	private String chat_message_to_send = "";

	public APlayerUIModule(ARootWorldModule rootWorldModule) {
		super(true);
		world_module = rootWorldModule;
		super.show();
	}

	@Override
	public void show() {
	}

	private void updateRemainingLives(int remainingLives, CustomIconbutton bar, CustomLabel label,
			CustomEffectWidget killEffect) {
		if (remainingLives >= 0) {
			bar.getIcon().setScale(((float) remainingLives / Shared.INITIAL_REMAINING_LIVES), 1);

			try {
				int lastRemaining = Integer.parseInt(label.getText().toString());
				if (lastRemaining != remainingLives)
					killEffect.getParticleEffect().start();
			} catch (NumberFormatException e) {
			}

			label.setText(remainingLives);
		}

	}

	@Override
	public void update(float delta) {
		super.update(delta);

		// update remaining lives
		updateRemainingLives(LogicTier.TEAM1_REMAINING_LIVES, team1_remaining_lives_bar, team1_remaining_lives_label,
				team1_kill_effect);

		updateRemainingLives(LogicTier.TEAM2_REMAINING_LIVES, team2_remaining_lives_bar, team2_remaining_lives_label,
				team2_kill_effect);

		updateTime();

		chat_window.updateAlpha(delta);

		// update time label
		int seconds = (int) (time % 60);
		// represented as: ['minutes' : '0 (if seconds < 10) + seconds']
		time_label.setText((int) (time / 60f) + ":" + (seconds < 10 ? "0" : "") + "" + seconds);

		// updates key assignments continuously
		skill1_key_label.setText(KeysModule.getKeycodeString(KeysConfiguration.getKeycode(WorldAction.EXECUTE_SKILL1)));
		skill2_key_label.setText(KeysModule.getKeycodeString(KeysConfiguration.getKeycode(WorldAction.EXECUTE_SKILL2)));

		// update combo rose
//		combo_rose.update();
//
//		combo_rose.setSkillSlots();

		for (Entry<Short, OmniClient> clientEntry : LogicTier.WORLD_PLAYER_MAP.entrySet()) {

			chat_window.updateMessage(clientEntry.getValue());

			SymbolSlot symbolSlot = symbols_map.get(clientEntry.getKey());

			// set new game classes for combo rose, which got cleared before
//			if (LogicTier.getMyClient() != null
//					&& LogicTier.getMyClient().getMetaClient().team == clientEntry.getValue().getMetaClient().team
//					&& LogicTier.getMyClient() != clientEntry.getValue())
//				combo_rose.setHeadSlots(clientEntry.getValue());

			if (symbolSlot == null)
				symbolSlot = setSymbolSlot(clientEntry.getValue());

			float deathTime = clientEntry.getValue().getPhysicalClient().death_time_current;
			// update death label
			if (deathTime > 1) {
				symbolSlot.symbol_button.setState(State.DISABLED);
				symbolSlot.death_label.setVisible(true);
				symbolSlot.death_label.setText("" + (int) (deathTime % 60));
			} else {
				symbolSlot.symbol_button.setState(State.NOT_SELECTED);
				symbolSlot.death_label.setVisible(false);
			}

			// handle player statistics
			HorizontalGroup statisticEntry = player_stats_module.getStatisticEntryMap().get(clientEntry.getKey());

			if (statisticEntry == null)
				player_stats_module.setHorizontalEntry(clientEntry.getValue());

		}

		// is client still inside the list?
		Iterator<Entry<Short, SymbolSlot>> itSymbols = symbols_map.entrySet().iterator();
		while (itSymbols.hasNext() == true) {
			Entry<Short, SymbolSlot> symbolEntry = itSymbols.next();
			if (LogicTier.WORLD_PLAYER_MAP.containsKey(symbolEntry.getKey()) == false) {

				SymbolSlot slot = symbolEntry.getValue();
				slot.in_use = false;
				slot.symbol_button.setIcon(null);
				itSymbols.remove();
			}
		}

		// might be null after the esc command is confirmed
		if (LogicTier.getMyClient() != null) {

			DrawableClient drawableClient = LogicTier.getMyClient().getDrawableClient();

			short healthCurrent = drawableClient.health_current;
			short healthAbsolute = drawableClient.getMetaClient().health_absolute;
			short manaCurrent = drawableClient.mana_current;
			short manaAbsolute = drawableClient.getMetaClient().mana_absolute;

			if (healthCurrent < 0)
				healthCurrent = 0;
			if (manaCurrent < 0)
				manaCurrent = 0;

			updateHealthManaUI(healthCurrent, healthAbsolute, health_bar_UI_image);
			health_current_value_label.setText(healthCurrent + "");
			health_absolute_value_label.setText(healthAbsolute + "");
			updateHealthManaUI(manaCurrent, manaAbsolute, mana_bar_UI_image);
			mana_current_value_label.setText(manaCurrent + "");
			mana_absolute_value_label.setText(manaAbsolute + "");

			SkillButton skillButton = null;
			// first reset all combo thumbnails
			skillButton = (SkillButton) cell_skill_combo1.getActor().getChild(0);
			skillButton.setIcon(null);
			skillButton = (SkillButton) cell_skill_combo2.getActor().getChild(0);
			skillButton.setIcon(null);

			comboslot1_button = (SkillButton) cell_skill_combo1.getActor().getChild(0);
			comboslot1_button.setShowComboDuration(true);
			comboslot1_button.setIcon(getAbsorbedSkillThumbnail(drawableClient.sparse_combo_storage1),
					drawableClient.sparse_combo_storage1);

			comboslot2_button = (SkillButton) cell_skill_combo2.getActor().getChild(0);
			comboslot2_button.setShowComboDuration(true);
			comboslot2_button.setIcon(getAbsorbedSkillThumbnail(drawableClient.sparse_combo_storage2),
					drawableClient.sparse_combo_storage2);
		}

	}

	private Sprite getAbsorbedSkillThumbnail(SparseComboStorage comboStorage) {

		if (comboStorage.meta_skill_id < 0)
			return null;

		if (comboStorage.isStoring() == false)
			return null;

		return Gameclass.ALL_META_SKILLS.get((int) comboStorage.meta_skill_id).getThumbnailWorldComboslot();

	}

	/**
	 * Note that the background sprite is the healthbar and icon is the background
	 * 
	 * @param healthOrMana
	 * @param healthOrManaWidget
	 */
	private void updateHealthManaUI(short healthManaCurrent, short healthManaAbsolute,
			CustomIconbutton healthOrManaWidget) {
		Sprite bgSprite = healthOrManaWidget.getBackgroundSprite();
		float width = healthOrManaWidget.getIcon().getWidth();
		bgSprite.setSize(width * ((float) healthManaCurrent / healthManaAbsolute),
				healthOrManaWidget.getIcon().getHeight());

		healthOrManaWidget.setLeftAlign();

	}

	/**
	 * By default the time is fetched from {@link ClientGameserverProtocol}. In
	 * {@link WorldTrainingRoot} the time isn't fetched from network data.
	 */
	protected void updateTime() {
		time = ClientGameserverProtocol.GAMESERVER_TIME_INTERPOLATED;
	}

	@Override
	public void loadWidgets() {
		time_label = new CustomLabel("", StyleContainer.label_medium_whitepure_20_time_style, Shared.WIDTH2 * SCALE_UI,
				Shared.HEIGHT1 * SCALE_UI, Align.center);

		symbolbar_bg_image = new CustomIconbutton(StaticGraphic.gui_bg_infobar, StaticGraphic.gui_bg_infobar.getWidth(),
				StaticGraphic.gui_bg_infobar.getHeight());

		symbol_group_team1 = new HorizontalGroup();
		symbol_group_team2 = new HorizontalGroup();

		loadSlots(symbol_group_team1);
		loadSlots(symbol_group_team2);

		chat_window = new ChatWindow(StaticGraphic.gui_bg_world_chat_input, true);

		chat_window.setActiveAndScope(false, false);

		combo_rose = new ComboRose();

		skill_bg_image = new CustomIconbutton(StaticGraphic.gui_bg_skillbar,
				StaticGraphic.gui_bg_skillbar.getWidth() * SCALE_UI,
				StaticGraphic.gui_bg_skillbar.getHeight() * SCALE_UI);

		// set bars dimensions equal to bg dimensions
		StaticGraphic.gui_skillbar_health.setSize(StaticGraphic.gui_bg_skillbar_health.getWidth(),
				StaticGraphic.gui_bg_skillbar_health.getHeight());
		StaticGraphic.gui_skillbar_mana.setSize(StaticGraphic.gui_bg_skillbar_mana.getWidth(),
				StaticGraphic.gui_bg_skillbar_mana.getHeight());

		float slashSeparatorWidth = 20;
		// health
		health_bar_UI_image = new CustomIconbutton(StaticGraphic.gui_bg_skillbar_health,
				StaticGraphic.gui_skillbar_health);
		float height = StaticGraphic.gui_bg_skillbar_health.getHeight();
		health_slash_separator_label = new CustomLabel(" / ",
				StyleContainer.label_bold_ivory2_18_bordered_healthmana_style, slashSeparatorWidth, height,
				Align.center);
		health_current_value_label = new CustomLabel("", StyleContainer.label_bold_ivory2_18_bordered_healthmana_style,
				Shared.WIDTH2, height, Align.right);
		health_absolute_value_label = new CustomLabel("", StyleContainer.label_bold_ivory2_18_bordered_healthmana_style,
				Shared.WIDTH2, height, Align.left);

		// mana
		mana_bar_UI_image = new CustomIconbutton(StaticGraphic.gui_bg_skillbar_mana, StaticGraphic.gui_skillbar_mana);
		height = StaticGraphic.gui_bg_skillbar_mana.getHeight();
		mana_slash_separator_label = new CustomLabel(" / ",
				StyleContainer.label_bold_ivory2_18_bordered_healthmana_style, slashSeparatorWidth, height,
				Align.center);
		mana_current_value_label = new CustomLabel("", StyleContainer.label_bold_ivory2_18_bordered_healthmana_style,
				Shared.WIDTH2, height, Align.right);
		mana_absolute_value_label = new CustomLabel("", StyleContainer.label_bold_ivory2_18_bordered_healthmana_style,
				Shared.WIDTH2, height, Align.left);

		// tickets
		team1_remaining_lives_label = new CustomLabel("", StyleContainer.label_bold_ivory2_18_bordered_healthmana_style,
				Shared.WIDTH1 * SCALE_UI, Shared.HEIGHT1 * SCALE_UI, Align.center);
		team2_remaining_lives_label = new CustomLabel("", StyleContainer.label_bold_ivory2_18_bordered_healthmana_style,
				Shared.WIDTH1 * SCALE_UI, Shared.HEIGHT1 * SCALE_UI, Align.center);

		if (world_module.getPhysicalClient().getTeam() == Team.TEAM_1) {
			// set origin
			StaticGraphic.gui_ticket_my_team.setOrigin(StaticGraphic.gui_ticket_my_team.getWidth(), 0);
			StaticGraphic.gui_ticket_enemy_team.setOrigin(0, 0);

			team1_remaining_lives_bar = new CustomIconbutton(StaticGraphic.gui_ticket_my_team,
					StaticGraphic.gui_bg_tickets_left_my_team);
			team2_remaining_lives_bar = new CustomIconbutton(StaticGraphic.gui_ticket_enemy_team,
					StaticGraphic.gui_bg_tickets_right_enemy_team);

			team1_remaining_lives_image = new CustomIconbutton(StaticGraphic.gui_icon_ticket_my_team,
					StaticGraphic.gui_icon_ticket_my_team.getWidth(),
					StaticGraphic.gui_icon_ticket_my_team.getHeight());
			team2_remaining_lives_image = new CustomIconbutton(StaticGraphic.gui_icon_ticket_enemy_team,
					StaticGraphic.gui_icon_ticket_enemy_team.getWidth(),
					StaticGraphic.gui_icon_ticket_enemy_team.getHeight());

			team1_kill_effect = new CustomEffectWidget(ParticleContainer.particle_gui_kill_left_my_team);
			team2_kill_effect = new CustomEffectWidget(ParticleContainer.particle_gui_kill_right_enemy_team);
		} else {
			// set origin
			StaticGraphic.gui_ticket_my_team.setOrigin(0, 0);
			StaticGraphic.gui_ticket_enemy_team.setOrigin(StaticGraphic.gui_ticket_enemy_team.getWidth(), 0);

			team1_remaining_lives_bar = new CustomIconbutton(StaticGraphic.gui_ticket_enemy_team,
					StaticGraphic.gui_bg_tickets_left_enemy_team);
			team2_remaining_lives_bar = new CustomIconbutton(StaticGraphic.gui_ticket_my_team,
					StaticGraphic.gui_bg_tickets_right_my_team);
			team1_remaining_lives_image = new CustomIconbutton(StaticGraphic.gui_icon_ticket_enemy_team,
					StaticGraphic.gui_icon_ticket_enemy_team.getWidth(),
					StaticGraphic.gui_icon_ticket_enemy_team.getHeight());
			team2_remaining_lives_image = new CustomIconbutton(StaticGraphic.gui_icon_ticket_my_team,
					StaticGraphic.gui_icon_ticket_my_team.getWidth(),
					StaticGraphic.gui_icon_ticket_my_team.getHeight());
			team1_kill_effect = new CustomEffectWidget(ParticleContainer.particle_gui_kill_left_enemy_team);
			team2_kill_effect = new CustomEffectWidget(ParticleContainer.particle_gui_kill_right_my_team);
		}

		team1_remaining_lives_bar.setTopAlign();
		team1_remaining_lives_bar.setIconShiftY(team1_remaining_lives_bar.getShiftY() - 4 * SCALE_UI);
		team1_remaining_lives_bar.setIconShiftX(40 * SCALE_UI);

		team2_remaining_lives_bar.setTopAlign();
		team2_remaining_lives_bar.setIconShiftY(team2_remaining_lives_bar.getShiftY() - 4 * SCALE_UI);
		team2_remaining_lives_bar.setIconShiftX(-40 * SCALE_UI);

		/*
		 * we take data from ice skill1
		 */
		float widthKeyLabel = Gameclass.ALL_META_SKILLS.get(1).getThumbnailWorldBig().getWidth();
		float heightKeyLabel = Gameclass.ALL_META_SKILLS.get(1).getThumbnailWorldBig().getHeight();

		// add +30 to shift the tooltip a little bit upward
		skill1_key_label = new CustomLabel("", StyleContainer.label_bold_ivory2_25_bordered_blue7_style, widthKeyLabel,
				heightKeyLabel + 30, Align.bottom);
		skill2_key_label = new CustomLabel("", StyleContainer.label_bold_ivory2_25_bordered_blue7_style, widthKeyLabel,
				heightKeyLabel + 30, Align.bottom);

	}

	private void loadSlots(HorizontalGroup symbolGroup) {
		for (int i = 0; i < Shared.MAX_TEAM_CAPACITY; i++)
			symbolGroup.addActor(new SymbolSlot());

	}

	CustomTable table_key_labels;
	
	@Override
	public void setLayout() {

		// kill effects
		CustomTable tableKillEffects = new CustomTable();
		tableKillEffects.top().padTop(25 * SCALE_UI).setFillParent(true);
		tables.add(tableKillEffects);

		tableKillEffects.add(team1_kill_effect).padRight(760 * SCALE_UI);
		tableKillEffects.add(team2_kill_effect);

		// symbol bar
		CustomTable tableSymbolbar = new CustomTable();
		tableSymbolbar.top().setFillParent(true);
		tables.add(tableSymbolbar);

		tableSymbolbar.add(symbolbar_bg_image).width(symbolbar_bg_image.getWidth())
				.height(symbolbar_bg_image.getHeight());

		CustomTable table = new CustomTable();
		table.top().setFillParent(true);
		tables.add(table);

		table.add(symbol_group_team1).padRight(126 * SCALE_UI);
		table.add(symbol_group_team2);

		CustomTable tableMinimap = new CustomTable();
		tableMinimap.left().bottom().setFillParent(true);
		tables.add(tableMinimap);

//		tableMinimap.add(minimap_image);

		CustomTable tableSkillBG = new CustomTable();
		tableSkillBG.bottom().setFillParent(true);
		tables.add(tableSkillBG);

		tableSkillBG.add(skill_bg_image);

		// declare some repeatedly used variables
		Gameclass myGameclass = LogicTier.getMyClient().getMetaClient().gameclass;
		PhysicalClient myPhysClient = LogicTier.getMyClient().getPhysicalClient();

		// skill feedback
		CustomTable tableSkillComboFeedback = new CustomTable();
		tableSkillComboFeedback.setFillParent(true);
		tables.add(tableSkillComboFeedback);
		cell_skill_combo_feedback = tableSkillComboFeedback.add(myGameclass.getSkillComboFeedbackTableWorld());

		// skill main
		CustomTable tableSkillMainWrapper = new CustomTable();
		tableSkillMainWrapper.bottom().setFillParent(true);
		tables.add(tableSkillMainWrapper);
		cell_skill_main = tableSkillMainWrapper.add(myGameclass.getSkillMainTableWorld(myPhysClient));

		// key labels
	    table_key_labels = new CustomTable();
		table_key_labels.bottom().setFillParent(true);
		table_key_labels.padBottom(63 * APlayerUIModule.SCALE_UI).padRight(125 * APlayerUIModule.SCALE_UI);
		tables.add(table_key_labels);

		table_key_labels.add(skill1_key_label);
		table_key_labels.add(skill2_key_label).padLeft(27 * APlayerUIModule.SCALE_UI);

		// skill combo1
		CustomTable tableSkillCombo1 = new CustomTable();
		tableSkillCombo1.bottom().setFillParent(true);
		tables.add(tableSkillCombo1);
		cell_skill_combo1 = tableSkillCombo1.add(myGameclass.getSkillCombo1TableWorld(myPhysClient));
		// skill combo2
		CustomTable tableSkillCombo2 = new CustomTable();
		tableSkillCombo2.bottom().setFillParent(true);
		tables.add(tableSkillCombo2);
		cell_skill_combo2 = tableSkillCombo2.add(myGameclass.getSkillCombo2TableWorld(myPhysClient));

		float yShift = 9f * SCALE_UI;

		CustomTable tableHealthMana = new CustomTable();
		tableHealthMana.bottom().padBottom(yShift).setFillParent(true);
		tables.add(tableHealthMana);

		tableHealthMana.add(health_bar_UI_image).height(health_bar_UI_image.getHeight() * SCALE_UI)
				.width(health_bar_UI_image.getWidth() * SCALE_UI);
		tableHealthMana.row();
		tableHealthMana.add(mana_bar_UI_image).height(mana_bar_UI_image.getHeight() * SCALE_UI)
				.width(mana_bar_UI_image.getWidth() * SCALE_UI);

		CustomTable tableHealthManaNumbers = new CustomTable();
		tableHealthManaNumbers.bottom().padBottom(yShift).setFillParent(true);
		tables.add(tableHealthManaNumbers);

		tableHealthManaNumbers.add(health_current_value_label).height(health_current_value_label.getHeight() * SCALE_UI)
				.width(health_current_value_label.getWidth() * SCALE_UI);
		tableHealthManaNumbers.add(health_slash_separator_label)
				.height(health_slash_separator_label.getHeight() * SCALE_UI)
				.width(health_slash_separator_label.getWidth() * SCALE_UI);
		tableHealthManaNumbers.add(health_absolute_value_label)
				.height(health_absolute_value_label.getHeight() * SCALE_UI)
				.width(health_absolute_value_label.getWidth() * SCALE_UI);

		tableHealthManaNumbers.row();

		tableHealthManaNumbers.add(mana_current_value_label).height(mana_current_value_label.getHeight() * SCALE_UI)
				.width(mana_current_value_label.getWidth() * SCALE_UI);
		tableHealthManaNumbers.add(mana_slash_separator_label).height(mana_slash_separator_label.getHeight() * SCALE_UI)
				.width(mana_slash_separator_label.getWidth() * SCALE_UI);
		tableHealthManaNumbers.add(mana_absolute_value_label).height(mana_absolute_value_label.getHeight() * SCALE_UI)
				.width(mana_absolute_value_label.getWidth() * SCALE_UI);

		CustomTable tableTimebar = new CustomTable();
		tableTimebar.top().setFillParent(true);
		tables.add(tableTimebar);

		tableTimebar.add(time_label);

		CustomTable tableChatWindow = new CustomTable();
		tableChatWindow.setFillParent(true);
		tables.add(tableChatWindow);

		tableChatWindow.add(chat_window.getGroup()).padBottom(-250);

		// bars of remaining lives
		CustomTable tableRemainingLivesBars = new CustomTable();
		tableRemainingLivesBars.top().setFillParent(true);
		tables.add(tableRemainingLivesBars);

		tableRemainingLivesBars.add(team1_remaining_lives_bar).padRight(518 * SCALE_UI);
		tableRemainingLivesBars.add(team2_remaining_lives_bar);

		// labels of remaining lives
		CustomTable tableRemainingLivesLabels = new CustomTable();
		tableRemainingLivesLabels.top().setFillParent(true);
		tables.add(tableRemainingLivesLabels);

		tableRemainingLivesLabels.add(team1_remaining_lives_image);
		tableRemainingLivesLabels.add(team1_remaining_lives_label).padRight(700 * SCALE_UI);
		tableRemainingLivesLabels.add(team2_remaining_lives_image);
		tableRemainingLivesLabels.add(team2_remaining_lives_label);

		tables.add(combo_rose.createTableHeads());
		tables.add(combo_rose.createTableSkill1());
		tables.add(combo_rose.createTableSkill2());

		/*
		 * we need to bind the skill tooltips not to the skill button but to the labels
		 * (Q and E labels) because of the order of the tables
		 */
		SkillButton button1 = myGameclass.getSkill1Button();
		SkillButton button2 = myGameclass.getSkill2Button();

		button1.bindTooltip(tables, skill1_key_label);
		button2.bindTooltip(tables, skill2_key_label);

	}

	@Override
	public void setListeners() {

	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return new HoverHandler(widgets);
	}

	@Override
	public void loadSubModules() {
		player_stats_module = new PlayerStatsModule();
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		subModules.add(player_stats_module);
	}

	@Override
	public void unfocus() {
		super.unfocus();
		chat_window.unfocusWidget();
	}

	@Override
	public void touchDown(int screenX, int screenY, int pointer, int button) {
		super.touchDown(screenX, screenY, pointer, button);

		WorldAction action = KeysConfiguration.getAction(new AdvancedKey(button, false));

		if (action == WorldAction.OPEN_PLAYER_STATISTICS)
			player_stats_module.setVisibleGlobal(true);

		if (action == WorldAction.SWITCH_COMBO_MODE) {
			AudioData.playEffect(AudioData.gui_combo_feedback);
//			popupComboRose();
		}

		if (action == WorldAction.OPEN_CHAT)
			openChat();
	}

	@Override
	public void touchUp(int screenX, int screenY, int pointer, int button) {
		super.touchUp(screenX, screenY, pointer, button);

		WorldAction action = KeysConfiguration.getAction(new AdvancedKey(button, false));

		if (action == WorldAction.SWITCH_COMBO_MODE) {
//			popoutComboRose();
		}

		if (action == WorldAction.OPEN_PLAYER_STATISTICS)
			player_stats_module.setVisibleGlobal(false);

	}

	@Override
	public void keyDown(int keycode) {
		super.keyDown(keycode);

		if (keycode == Keys.DOWN) {
			if (logic_tier.getGameserverLogic() != null) {
				for (ServerPhysicalClient client : GameserverLogic.PLAYER_MAP.values())
					client.health_current = 0;

			} else
				for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {
					client.getPhysicalClient().health_current = 0;
				}
		}

		WorldAction action = KeysConfiguration.getAction(new AdvancedKey(keycode, true));

		if (action == WorldAction.OPEN_PLAYER_STATISTICS)
			player_stats_module.setVisibleGlobal(true);

		if (action == WorldAction.SWITCH_COMBO_MODE) {
			AudioData.playEffect(AudioData.gui_combo_feedback);
//			popupComboRose();
		}

		boolean chatOpened = false;
		if (action == WorldAction.OPEN_CHAT)
			chatOpened = openChat();

		if (keycode == Keys.ENTER && chatOpened == false)
			closeChat();

	}

	@Override
	public void keyUp(int keycode) {
		super.keyUp(keycode);

		WorldAction action = KeysConfiguration.getAction(new AdvancedKey(keycode, true));

		if (action == WorldAction.OPEN_PLAYER_STATISTICS)
			player_stats_module.setVisibleGlobal(false);

		if (action == WorldAction.SWITCH_COMBO_MODE) {
//			popoutComboRose();
		}

	}

	protected boolean openChat() {

		if (chat_window.isActive() == false) {

			/*
			 * if shift left is pressed the text will only be shown to all, otherwise just
			 * to allies
			 */
			chat_window.setActiveAndScope(true, !Gdx.input.isKeyPressed(Keys.SHIFT_LEFT));
			return true;
		}
		return false;
	}

	protected void closeChat() {

		if (chat_window.getTextField().getStage().getKeyboardFocus() == chat_window.getTextField()) {

			if (chat_window.getTextField().getText().equals("") == false) {
				chat_message_to_send = handleChatMessage(
						chat_window.getScopePrefix() + chat_window.getTextField().getText());
			}

			// clear
			chat_window.getTextField().setText("");
			chat_window.setActiveAndScope(false, false);
		}
	}

	protected void popupComboRose() {
		combo_rose.setActive(true);
		combo_rose.applyPopupAnimations();
	}

	protected void popoutComboRose() {

		combo_rose.setActive(false);
		combo_rose.applySelectAnimations();

		if (combo_rose.getIDAllySelected() == -1 || combo_rose.getSkillSelected() == null)
			return;

		DrawableClient drawableClient = LogicTier.getMyClient().getDrawableClient();
		PhysicalClient physicalClient = LogicTier.getMyClient().getPhysicalClient();
		drawableClient.setCurrentAction(combo_rose.getSkillSelected(), true);
		physicalClient.setCurrentAction(combo_rose.getSkillSelected(), true);
		physicalClient.setIDComboAlly(combo_rose.getIDAllySelected());
		world_module.getReleasedKeys()
				.add(new AdvancedKey(combo_rose.getSkillSelected() == WorldAction.EXECUTE_TRANSFER_SKILL1
						? KeysConfiguration.KEY_TRANSFER_SKILL1
						: KeysConfiguration.KEY_TRANSFER_SKILL2, true));

	}

	protected abstract String handleChatMessage(String chatMessage);

	/**
	 * Removes an unused slot from {@link #symbol_group_team1} or
	 * {@link #symbol_group_team2} and assigns a symbol sprite to it. Then the slot
	 * is put to {@link #symbols_map}
	 * 
	 * @param client
	 * @return
	 */
	private SymbolSlot setSymbolSlot(OmniClient client) {

		SymbolSlot slot;
		Team team = client.getMetaClient().team;

		if (team == Team.TEAM_1)
			slot = getNextEmptySlot(symbol_group_team1);
		else
			slot = getNextEmptySlot(symbol_group_team2);

		if (slot != null) {
			slot.in_use = true;

			slot.setSymbolSprite(client.getMetaClient().gameclass);
			symbols_map.put(client.getMetaClient().connection_id, slot);

		}
		return slot;
	}

	private SymbolSlot getNextEmptySlot(HorizontalGroup symbolGroup) {
		for (Actor a : symbolGroup.getChildren()) {
			SymbolSlot slot = (SymbolSlot) a;
			if (slot.in_use == false)
				return slot;
		}
		return null;
	}

	public void setWidgetsAfterChange(MetaClient metaClient) {
		SymbolSlot symbolSlot = symbols_map.get(metaClient.connection_id);
		symbolSlot.setSymbolSprite(metaClient.gameclass);

		cell_skill_combo_feedback.setActor(metaClient.gameclass.getSkillComboFeedbackTableWorld());
		cell_skill_main
				.setActor(metaClient.gameclass.getSkillMainTableWorld(LogicTier.getMyClient().getPhysicalClient()));

		SkillButton button1 = metaClient.gameclass.getSkill1Button();
		SkillButton button2 = metaClient.gameclass.getSkill2Button();

		// key labels
		tables.remove(table_key_labels);
		table_key_labels.clearChildren();
		table_key_labels.remove();

		table_key_labels = new CustomTable();
		table_key_labels.bottom().setFillParent(true);
		table_key_labels.padBottom(63 * APlayerUIModule.SCALE_UI).padRight(125 * APlayerUIModule.SCALE_UI);
		tables.add(table_key_labels);

		table_key_labels.add(skill1_key_label);
		table_key_labels.add(skill2_key_label).padLeft(27 * APlayerUIModule.SCALE_UI);
		
		button1.bindTooltip(tables, skill1_key_label);
		button2.bindTooltip(tables, skill2_key_label);

		cell_skill_combo1
				.setActor(metaClient.gameclass.getSkillCombo1TableWorld(LogicTier.getMyClient().getPhysicalClient()));
		cell_skill_combo2
				.setActor(metaClient.gameclass.getSkillCombo2TableWorld(LogicTier.getMyClient().getPhysicalClient()));
	}

	/**
	 * 
	 */
	private class SymbolSlot extends VerticalGroup implements WidgetGroupable {

		// has no background sprite
		private CustomIconbutton symbol_button;

		// has background sprite
		private CustomLabel death_label;

		/**
		 * if <code>true</code> the slot shows a symbol image of the gameclass
		 */
		private boolean in_use;

		/**
		 * Creates an empty symbol slot placed at the top edge of the screen
		 * 
		 */
		public SymbolSlot() {
			in_use = false;
			symbol_button = new CustomIconbutton(null, 65 * SCALE_UI, 53 * SCALE_UI);
			symbol_button.setVisible(true);
			death_label = new CustomLabel("", StyleContainer.label_medium_ivory3_18_style,
					StaticGraphic.gui_bg_death_timer, Align.center);
			death_label.setVisible(false);

			// add to vertical group structure
			addActor(symbol_button);
			addActor(death_label);

		}

		public void setSymbolSprite(Gameclass gameclass) {
			// create new sprite
			Sprite symbolSprite = new Sprite(gameclass.getSymbol());
			symbolSprite.setScale(0.4f * SCALE_UI);

			symbol_button.setIcon(symbolSprite);
		}

		@Override
		public void setVisibilityWidgets(boolean visible) {
			// symbol button is always visible
			death_label.setVisible(visible);
		}

	}

	public ChatWindow getChatWindow() {
		return chat_window;
	}

	public PlayerStatsModule getStatisticsModule() {
		return player_stats_module;
	}

	public void createAlly(DrawableClient characterModel) {
		// will be overridden by player training ui
	}

	public void createEnemy(DrawableClient characterModel) {
		// will be overridden by player training ui
	}

	public void emptyChatMessageAfterSend() {
		chat_message_to_send = "";
	}

	public String getChatMessageToSend() {
		return chat_message_to_send;
	}

	/**
	 * 
	 * @return
	 */
	public ComboRose getComboRose() {
		return combo_rose;
	}

}
