package com.elementar.gui.modules.ingame;

import java.util.ArrayList;
import java.util.Map.Entry;

import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StyleContainer;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.ARootWorldModule;
import com.elementar.gui.util.CustomSkeleton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomSpineWidget;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.network.gameserver.GameserverLogic.ServerState;
import com.elementar.logic.network.protocols.ClientGameserverProtocol;
import com.elementar.logic.util.Shared;

public class VictoryDefeatModule extends AChildModule {

	private static final float ELAPSED_TIME_TIL_FADE_IN = 1f;
	private static final float ELAPSED_TIME_TIL_STATSMODULE = 4f;

	private CustomTable table_spine;
	private boolean add_spine_widget = true, add_stats_module = true;

	private CustomSpineWidget spine_widget;

	private float elapsed_time;

	private PlayerStatsModule player_stats_module;

	public VictoryDefeatModule() {
		super(false);

	}

	@Override
	public void update(float delta) {
		super.update(delta);

		if (isVisible() == true)
			ARootWorldModule.CURSOR_UI_CHECK.setAtUIModules(true);

		elapsed_time += delta;

		if (spine_widget != null && elapsed_time > ELAPSED_TIME_TIL_FADE_IN) {

			// add stats module
			if (elapsed_time >= ELAPSED_TIME_TIL_STATSMODULE) {

				if (add_stats_module == true) {
					add_stats_module = false;
					table_spine.removeActor(spine_widget);
					table_spine.validate();

					player_stats_module.setVisibleGlobal(true);

					for (Entry<Short, OmniClient> clientEntry : LogicTier.WORLD_PLAYER_MAP.entrySet()) {
						// handle player statistics
						HorizontalGroup statisticEntry = player_stats_module.getStatisticEntryMap()
								.get(clientEntry.getKey());

						if (statisticEntry == null)
							player_stats_module.setHorizontalEntry(clientEntry.getValue());
					}

				}
			}
			// add spine widget
			if (add_spine_widget == true) {
				add_spine_widget = false;

				table_spine.add(spine_widget);
				table_spine.validate();
			}

		}

	}

	@Override
	public void loadWidgets() {
	}

	@Override
	public void setLayout() {

	}

	@Override
	public void setListeners() {
	}

	@Override
	public void loadSubModules() {
		/*
		 * because player stats is a cover module (=gray background behind) we need to
		 * put the time label widget inside of this module. Otherwise it is not drawn in
		 * its full alpha.
		 */
		player_stats_module = new PlayerStatsModule() {
			private CustomLabel time_label;

			@Override
			public void update(float delta) {
				super.update(delta);

				time_label.setText("" + (int) Math.ceil(ClientGameserverProtocol.GAMESERVER_TIME_INTERPOLATED));
			}

			@Override
			public void loadWidgets() {
				super.loadWidgets();
				time_label = new CustomLabel("", StyleContainer.label_bold_white_30_style, Shared.WIDTH5,
						Shared.HEIGHT2, Align.center);

			}

			@Override
			public void setLayout() {
				super.setLayout();

				CustomTable tableTimeButton = new CustomTable();
				tableTimeButton.top().padTop(20).setFillParent(true);
				tables.add(tableTimeButton);

				tableTimeButton.add(time_label);

			}

		};
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		subModules.add(player_stats_module);
	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return new HoverHandler(widgets);
	}

	public void setSkeleton(CustomSkeleton skeleton) {

		spine_widget = new CustomSpineWidget(skeleton, "game_ending", false);

		// init tables
		table_spine = new CustomTable();
		table_spine.setFillParent(true);
		tables.add(table_spine);

		elapsed_time = 0;
	}

	public boolean isDone() {
		return LogicTier.SERVER_STATE != ServerState.POST_MATCH && add_stats_module == false;
	}

}
