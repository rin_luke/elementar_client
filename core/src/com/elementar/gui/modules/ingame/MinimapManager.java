package com.elementar.gui.modules.ingame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.elementar.data.container.StaticGraphic;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.util.IslandFillingManager;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.creeps.OmniCreep;
import com.elementar.logic.map.MapManager;
import com.elementar.logic.map.RandomIsland;
import com.elementar.logic.util.Shared;

public class MinimapManager {

	private Sprite background_sprite, ornament_border_sprite;

	private IslandFillingManager island_filling_manager;

	private MapManager map_manager;

	private ExtendViewport viewport;
	private OrthographicCamera cam;

	private OmniClient my_client;

	// TODO delete in future
	private boolean visible = true;

	public MinimapManager(MapManager mapManager, OmniClient myClient) {
		this.map_manager = mapManager;

		background_sprite = new Sprite(StaticGraphic.gui_bg_minimap);

		background_sprite.setSize(mapManager.getOuterBound().x, mapManager.getOuterBound().y);
		background_sprite.setPosition(-background_sprite.getWidth() * 0.5f, -background_sprite.getHeight() * 0.5f);

		ornament_border_sprite = StaticGraphic.gui_bg_minimap_ornament;
		ornament_border_sprite.setSize(mapManager.getOuterBound().x, mapManager.getOuterBound().y);

		ornament_border_sprite.setPosition(
				-ornament_border_sprite.getWidth() * 0.5f
						+ (ornament_border_sprite.getWidth() - background_sprite.getWidth()) * 0.5f,
				-ornament_border_sprite.getHeight() * 0.5f);

		island_filling_manager = new IslandFillingManager();

		island_filling_manager.place(map_manager.getMapBorder().getLeftFillingMinimapVertices(),
				map_manager.getMapBorder().getLeftFillingMinimapVertices()[0]);
		island_filling_manager.place(map_manager.getMapBorder().getRightFillingMinimapVertices(),
				map_manager.getMapBorder().getRightFillingMinimapVertices()[0]);
		for (RandomIsland island : map_manager.getIslands())
			island_filling_manager.place(island.getGlobalVertices(), island.getStartPoint());

		viewport = new ExtendViewport(Shared.MAP_WIDTH * 7, Shared.MAP_HEIGHT * 7);
		cam = (OrthographicCamera) viewport.getCamera();

		my_client = myClient;
	}

	/**
	 * 
	 * @param batch
	 * @param minimapDrawLeft <code>true</code> by default
	 */
	public void draw(ShaderPolyBatch batch, boolean minimapDrawLeft) {

		if (visible == false)
			return;

		if (minimapDrawLeft == true)
			cam.position.x = cam.viewportWidth * 0.5f - background_sprite.getWidth() * 0.5f;
		else
			cam.position.x = -cam.viewportWidth * 0.5f + background_sprite.getWidth() * 0.5f;

		cam.position.y = cam.viewportHeight * 0.5f - background_sprite.getHeight() * 0.5f;

		viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		batch.setProjectionMatrix(cam.combined);
		batch.begin();
		background_sprite.draw(batch);
		batch.setShader(batch.minimap_shader);
		island_filling_manager.draw(batch);

		batch.end();

		LogicTier.VIEW_HANDLER.renderFogMinimap(cam);

		batch.begin();

		// draw bases
		BasicScreen.SKELETON_RENDERER.draw(batch, map_manager.getBaseLeft().getSkeleton());
		BasicScreen.SKELETON_RENDERER.draw(batch, map_manager.getBaseRight().getSkeleton());
		batch.setShader(batch.sprite_shader);

		// draw character symbols
		for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values())
			if (client != my_client)
				drawClientSymbols(batch, client);

		// draw own client symbol
		drawClientSymbols(batch, my_client);

		Sprite symbol;

		// draw creep symbols
		for (OmniCreep creep : LogicTier.CREEP_LIVING_LIST) {
			if (creep.getPhysicalCreep().isVisible() == true && creep.getPhysicalCreep().isAlive() == true) {
				symbol = StaticGraphic.gui_minimap_creep;
				symbol.setPosition(creep.getDrawableCreep().pos.x - symbol.getWidth() * 0.5f,
						creep.getDrawableCreep().pos.y - symbol.getHeight() * 0.5f);
				symbol.draw(batch);
			}

		}

		ornament_border_sprite.draw(batch);

		batch.end();

	}

	public OrthographicCamera getCam() {
		return cam;
	}

	private void drawClientSymbols(ShaderPolyBatch batch, OmniClient client) {
		Sprite symbol;
		if (client.getPhysicalClient().isVisible() == true && client.getPhysicalClient().isAlive() == true) {

			// set sprites
			if (client.getMetaClient().team == my_client.getMetaClient().team)
				symbol = client.getMetaClient().gameclass.getSymbolMinimapGreen();
			else
				symbol = client.getMetaClient().gameclass.getSymbolMinimapRed();

			symbol.setFlip(client.getDrawableClient().getHeadFlip(), false);

			// set sprites positions
			symbol.setPosition(client.getDrawableClient().pos.x - symbol.getWidth() * 0.5f,
					client.getDrawableClient().pos.y - symbol.getHeight() * 0.5f);

			// draw sprites
			symbol.draw(batch);
		}
	}

	// TODO delete this method
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

}
