package com.elementar.gui.modules.ingame;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AChildCoverModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.ARootMenuModule;
import com.elementar.gui.abstracts.ARootWorldModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.modules.AYesNoConfirmationModule;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;

/**
 * Appears when player clicks the escape button while being ingame.
 * 
 * @author lukassongajlo
 * 
 */
public class ESCModule extends AChildCoverModule {

	private AYesNoConfirmationModule quit_confirm_module;

	private CustomTextbutton options_button, quit_button, resume_button;

	public ESCModule() {
		super(StaticGraphic.gui_bg_esc1, BasicScreen.DRAW_COVERMODULE_1, true);
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		if (isVisible() == true)
			ARootWorldModule.CURSOR_UI_CHECK.setAtUIModules(true);

	}

	@Override
	public void loadWidgets() {
		super.loadWidgets();
		options_button = new CustomTextbutton(TextData.getWord("options"),
				StyleContainer.button_bold_20_style, StaticGraphic.gui_bg_btn_height1_width4,
				Align.center);
		options_button.setSound(ClickSoundType.LIGHT);
		quit_button = new CustomTextbutton(TextData.getWord("quit"),
				StyleContainer.button_bold_20_style, StaticGraphic.gui_bg_btn_height1_width4,
				Align.center);
		quit_button.setSound(ClickSoundType.LIGHT);
		resume_button = new CustomTextbutton(TextData.getWord("resume"),
				StyleContainer.button_bold_20_style, StaticGraphic.gui_bg_btn_height1_width4,
				Align.center);
		resume_button.setSound(ClickSoundType.LIGHT);

	}

	@Override
	public void setLayout() {
		super.setLayout();

		CustomTable table = new CustomTable();
		table.setFillParent(true);
		tables.add(table);

		table.add(resume_button);
		table.row();
		table.add(quit_button).padTop(50);
		table.row();
		table.add(options_button).padTop(50);
	}

	@Override
	public void setListeners() {
		super.setListeners();

		resume_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				clickedEscape();
			}
		});

		quit_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				// TODO beachten dass bei istrainingworld == false auch
				// disconnect werden muss
				quit_confirm_module.setVisibleGlobal(true);
			}
		});

		options_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				AModule.OPTIONS_MODULE.setVisibleGlobal(true);
			}
		});
	}

	@Override
	public void loadSubModules() {
		quit_confirm_module = new AYesNoConfirmationModule(TextData.getWord("reallyWantToLeave?"),
				BasicScreen.DRAW_COVERMODULE_2) {
			@Override
			public void clickedYes() {
				logic_tier.reset();
				if (ARootMenuModule.MAINMENU_NAVIGATION_MODULE != null)
					GUIUtil.clickActor(ARootMenuModule.MAINMENU_NAVIGATION_MODULE.getButtonPlay());
			}
		};
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		subModules.add(quit_confirm_module);
	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(resume_button);
		widgets.add(options_button);
		widgets.add(quit_button);
		return new HoverHandler(widgets);
	}

	@Override
	protected boolean clickedEscape() {
		setVisibleGlobal(false);
		return true;
	}

	public AYesNoConfirmationModule getConfirmationModule() {
		return quit_confirm_module;
	}

}
