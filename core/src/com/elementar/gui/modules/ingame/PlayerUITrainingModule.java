package com.elementar.gui.modules.ingame;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.GameclassContainer;
import com.elementar.data.container.KeysConfiguration;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.data.container.KeysConfiguration.WorldAction;
import com.elementar.data.container.util.AdvancedKey;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.gui.abstracts.ARootWorldModule;
import com.elementar.gui.modules.roots.WorldTrainingRoot;
import com.elementar.gui.util.CursorUICheckListener;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextArea;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.ACreep;
import com.elementar.logic.creeps.MetaCreep;
import com.elementar.logic.creeps.OmniCreep;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.ViewHandler;
import com.elementar.logic.util.Shared.Team;

public class PlayerUITrainingModule extends APlayerUIModule {

	private float firerate = 1;

	private CustomLabel firerate_label;
	private CustomIconbutton gameclass_switch_button;
	private CustomTextbutton free_spells_button, release_zoom_button, create_enemy_button, create_ally_button,
			create_creep_button, delete_dummies_button, plus_firerate_button, minus_firerate_button,
			get_allies_skills_button, let_enemies_combine;
	private CustomTextArea key_assignments_textarea;
	private boolean infinite_mana;

	public PlayerUITrainingModule(ARootWorldModule rootWorldModule) {
		super(rootWorldModule);
	}

	@Override
	protected void updateTime() {
		time = world_module.getTimeLocal();
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		firerate_label.setText("firerate = " + firerate);

		if (infinite_mana == true) {
			for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {
				client.getPhysicalClient().mana_current = client.getPhysicalClient().getMetaClient().mana_absolute;
				client.getDrawableClient().mana_current = client.getDrawableClient().getMetaClient().mana_absolute;
				client.getPhysicalClient().getSkill1().setCooldownCurrent(0);
				client.getPhysicalClient().getSkill2().setCooldownCurrent(0);
			}
		}
		// for(OmniClient client : LogicTier.PLAYER_MAP.values()){
		// if(client.isDummy() == true){
		// client.getPhysicalClient().setCurrentAction(WorldAction.LEFT_MOUSE_CLICK,
		// true);
		// }
		// }

	}

	@Override
	public void loadWidgets() {
		super.loadWidgets();
		gameclass_switch_button = new CustomIconbutton(StaticGraphic.gui_icon_character_switch,
				StaticGraphic.gui_bg_btn_height2_width4);
		gameclass_switch_button.setSound(ClickSoundType.HEAVY);
		gameclass_switch_button.addListener(new CursorUICheckListener(world_module));
		gameclass_switch_button.twowayClicklistener(true);
		create_enemy_button = new CustomTextbutton(TextData.getWord("createEnemy"), StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		create_enemy_button.setSound(ClickSoundType.LIGHT);
		create_enemy_button.addListener(new CursorUICheckListener());
		create_ally_button = new CustomTextbutton(TextData.getWord("createAlly"), StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		create_ally_button.setSound(ClickSoundType.LIGHT);
		create_ally_button.addListener(new CursorUICheckListener());

		create_creep_button = new CustomTextbutton(TextData.getWord("createCreep"), StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		create_creep_button.setSound(ClickSoundType.LIGHT);
		create_creep_button.addListener(new CursorUICheckListener());

		get_allies_skills_button = new CustomTextbutton(TextData.getWord("getAlliesSkills"),
				StyleContainer.button_bold_20_style, StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		get_allies_skills_button.setSound(ClickSoundType.LIGHT);
		get_allies_skills_button.addListener(new CursorUICheckListener());

		let_enemies_combine = new CustomTextbutton(TextData.getWord("letEnemiesCombine"),
				StyleContainer.button_bold_20_style, StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		let_enemies_combine.setSound(ClickSoundType.LIGHT);
		let_enemies_combine.addListener(new CursorUICheckListener());

		delete_dummies_button = new CustomTextbutton(TextData.getWord("deleteDummies"),
				StyleContainer.button_bold_20_style, StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		delete_dummies_button.setSound(ClickSoundType.LIGHT);
		delete_dummies_button.addListener(new CursorUICheckListener());

		free_spells_button = new CustomTextbutton(TextData.getWord("freeSpells"), StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		free_spells_button.setSound(ClickSoundType.LIGHT);
		free_spells_button.addListener(new CursorUICheckListener());
		free_spells_button.twowayClicklistener(true);

		release_zoom_button = new CustomTextbutton(TextData.getWord("releaseZoom"), StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		release_zoom_button.setSound(ClickSoundType.LIGHT);
		release_zoom_button.addListener(new CursorUICheckListener());
		release_zoom_button.twowayClicklistener(true);

		key_assignments_textarea = new CustomTextArea(
				"G = Debug lines\nF = Superman Mode\nHolding right shift = Controlling Dummies\nN = Hiding this window\nM = Screenshot oder Video Mode",
				StaticGraphic.gui_bg_tooltip);

		plus_firerate_button = new CustomTextbutton("+ firerate", StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		plus_firerate_button.addListener(new CursorUICheckListener());
		minus_firerate_button = new CustomTextbutton("- firerate", StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		minus_firerate_button.addListener(new CursorUICheckListener());

		firerate_label = new CustomLabel("", StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH4,
				Shared.HEIGHT1, Align.center);

	}

	@Override
	public void setLayout() {
		super.setLayout();
		CustomTable tableLeft = new CustomTable();
		tableLeft.left().setFillParent(true);
		tables.add(tableLeft);

		tableLeft.add(release_zoom_button);
		tableLeft.row();
		tableLeft.add(free_spells_button);
		tableLeft.row();
		tableLeft.add(gameclass_switch_button);
		tableLeft.row();
		tableLeft.add(create_enemy_button);
		tableLeft.row();
		tableLeft.add(create_ally_button);
		tableLeft.row();
		tableLeft.add(create_creep_button);
		tableLeft.row();
		tableLeft.add(delete_dummies_button);
		tableLeft.row();
		tableLeft.add(get_allies_skills_button);
		tableLeft.row();
		tableLeft.add(let_enemies_combine);
		tableLeft.row();
		tableLeft.add(plus_firerate_button);
		tableLeft.row();
		tableLeft.add(minus_firerate_button);
		tableLeft.row();
		tableLeft.add(firerate_label);

		CustomTable tableKeyAssignments = new CustomTable();
		tableKeyAssignments.right().setFillParent(true);
		tables.add(tableKeyAssignments);

		tableKeyAssignments.add(key_assignments_textarea);

	}

	int numberRays = ViewHandler.RAY_COUNT_FOG_DISPEL;
	final DummyCreator creator = new DummyCreator();

	@Override
	public void setListeners() {
		super.setListeners();

		float firerateStep = 0.5f;

		plus_firerate_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				firerate += firerateStep;
			}
		});

		minus_firerate_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (firerate - firerateStep < 0.1f)
					return;
				firerate -= firerateStep;
			}
		});

		gameclass_switch_button.addListener(new ClickListener() {
			@Override

			public void clicked(InputEvent event, float x, float y) {

				if (gameclass_switch_button.getState() == State.SELECTED)
					world_module.changeTab(((WorldTrainingRoot) world_module).getGameclassSelectionModule());

				if (gameclass_switch_button.getState() == State.NOT_SELECTED)
					world_module.changeTab(PlayerUITrainingModule.this);
			}
		});

		free_spells_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				infinite_mana = free_spells_button.getState() == State.SELECTED;
			}
		});

		release_zoom_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				ARootWorldModule.MAX_ZOOM_OUT = release_zoom_button.getState() == State.SELECTED ? 100f : 1f;
			}
		});

		create_creep_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				creator.createCreep();
			}
		});

		create_enemy_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				world_module.setCreateEnemy(true);
				GUIUtil.clickActor(gameclass_switch_button);
				// creator.createPlayer(Team.TEAM_2, GameclassName.ICE);//
				// Util.randomEnum(GameclassName.class));
			}
		});

		create_ally_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				world_module.setCreateAlly(true);
				GUIUtil.clickActor(gameclass_switch_button);
				// creator.createPlayer(Team.TEAM_1,
				// Util.randomEnum(GameclassName.class));
			}

		});

		delete_dummies_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Iterator<Entry<Short, OmniClient>> it = LogicTier.WORLD_PLAYER_MAP.entrySet().iterator();
				OmniClient client;
				while (it.hasNext() == true) {
					client = it.next().getValue();
					if (client.isDummy() == true) {
						client.getPhysicalClient().getHitbox().destroy();
						it.remove();
					}
				}
				creator.resetCounters();

				// remove creeps
				Iterator<OmniCreep> it2 = LogicTier.CREEP_LIVING_LIST.iterator();
				OmniCreep creep;
				while (it2.hasNext() == true) {
					creep = it2.next();
					creep.getPhysicalCreep().getHitbox().destroy();
					it2.remove();
				}

			}

		});

		get_allies_skills_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {
					if (client.isDummy() == true && client.getMetaClient().team == Team.TEAM_1) {

						client.getDrawableClient().setCurrentAction(WorldAction.EXECUTE_TRANSFER_SKILL1, true);

						client.getPhysicalClient().setCurrentAction(WorldAction.SWITCH_COMBO_MODE, true);
						client.getPhysicalClient().setCurrentAction(WorldAction.EXECUTE_TRANSFER_SKILL1, true);
					}
				}
			}
		});

		let_enemies_combine.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {

				short enemyID = -1;

				for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {
					if (client.isDummy() == true && client.getMetaClient().team == Team.TEAM_2) {

						if (enemyID == -1) {
							enemyID = client.getMetaClient().connection_id;
							continue;
						}

						client.getDrawableClient().setCurrentAction(WorldAction.EXECUTE_TRANSFER_SKILL1, true);

						client.getPhysicalClient().setCurrentAction(WorldAction.SWITCH_COMBO_MODE, true);
						client.getPhysicalClient().setCurrentAction(WorldAction.EXECUTE_TRANSFER_SKILL1, true);
					}
				}
			}
		});
	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(release_zoom_button);
		widgets.add(free_spells_button);
		widgets.add(gameclass_switch_button);
		widgets.add(create_enemy_button);
		widgets.add(create_ally_button);
		widgets.add(create_creep_button);
		widgets.add(delete_dummies_button);
		widgets.add(get_allies_skills_button);
		widgets.add(let_enemies_combine);
		widgets.add(plus_firerate_button);
		widgets.add(minus_firerate_button);
		widgets.add(plus_firerate_button);
		return super.createHoverHandler(widgets);
	}

	@Override
	protected boolean clickedEscape() {
		if (gameclass_switch_button.getState() == State.SELECTED) {
			GUIUtil.clickActor(gameclass_switch_button);
			return true;
		}
		return super.clickedEscape();
	}

	@Override
	public void keyDown(int keycode) {
		super.keyDown(keycode);
		if (keycode == Keys.N)
			key_assignments_textarea.setVisibilityWidgets(!key_assignments_textarea.isVisible());
	}

	@Override
	protected String handleChatMessage(String chatMessage) {
		LogicTier.getMyClient().getMetaClient().chat_message = chatMessage;
		return "";
	}

	public float getFirerate() {
		return firerate;
	}

	public CustomIconbutton getSwitchButton() {
		return gameclass_switch_button;
	}

	@Override
	public void createAlly(DrawableClient characterModel) {
		creator.createPlayer(Team.TEAM_1, characterModel.getGameclass().getNameAsEnum());

	}

	@Override
	public void createEnemy(DrawableClient characterModel) {
		creator.createPlayer(Team.TEAM_2, characterModel.getGameclass().getNameAsEnum());
	}

	private class DummyCreator {

		private int team1_counter = 1; // own player
		private int team2_counter = 0;

		void createPlayer(Team team, GameclassName gameclassName) {

			/*
			 * dummy limit is equal to max team size + 1 (own player)
			 */
			int counter = team == Team.TEAM_1 ? team1_counter : team2_counter;
			if (counter < Shared.MAX_TEAM_CAPACITY) {
				MetaClient metaDummy = new MetaClient("Dummy", GameclassContainer.filterByName(
						// GameclassName.LIGHT
						gameclassName).getNameAsEnum(), 0, team);
				OmniClient dummy = new OmniClient(metaDummy, true, Location.CLIENTSIDE_TRAINING);

				dummy.init(logic_tier.getClientMainWorld(), logic_tier.getClientParaWorld());

				/*
				 * init() would lead to a spawn in the right or left base, so the position has
				 * to be manually set to players position.
				 */
				dummy.setPosition(world_module.getPhysicalClient().pos.x, world_module.getPhysicalClient().pos.y);
				// add dummy
				LogicTier.WORLD_PLAYER_MAP.put(dummy.getPhysicalClient().connection_id, dummy);

				counter++;

				if (team == Team.TEAM_1) {
					LogicTier.VIEW_HANDLER.illuminateDummy(dummy);
					team1_counter = counter;
				} else
					team2_counter = counter;

			}

		}

		void createCreep() {

			MetaCreep metaDummy = new MetaCreep(0, 0);

			OmniCreep dummy = new OmniCreep(metaDummy,

					// CreepCategory.SMALL,
					Util.randomEnum(ACreep.CreepCategory.class), Location.CLIENTSIDE_TRAINING);

			dummy.init(logic_tier.getClientMainWorld());

			/*
			 * init() would lead to a spawn in the right or left base, so the position has
			 * to be manually set to players position.
			 */
			dummy.setPosition(world_module.getPhysicalClient().pos.x, world_module.getPhysicalClient().pos.y);
			// add enemy
			LogicTier.CREEP_MAP.put(dummy.getPhysicalCreep().connection_id, dummy);
			LogicTier.CREEP_LIVING_LIST.add(dummy);

		}

		void resetCounters() {
			team1_counter = 1;
			team2_counter = 0;
		}
	}

}