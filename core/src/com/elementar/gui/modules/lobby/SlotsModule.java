package com.elementar.gui.modules.lobby;

import java.util.ArrayList;

import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.modules.roots.LobbyRoot;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.LobbySlotContainer;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.network.protocols.ClientGameserverProtocol;
import com.elementar.logic.util.Shared.Team;

public class SlotsModule extends AChildModule {

	private Team team;

	private ArrayList<LobbySlotContainer> slots;

	private int number_slots;

	public SlotsModule(boolean visible, Team team) {
		super(visible);
		this.team = team;
		this.number_slots = ClientGameserverProtocol.GAMESERVER_CAPACITY / 2;
		this.slots = new ArrayList<LobbySlotContainer>(number_slots);
		super.show();
	}

	@Override
	public void show() {
	}

	@Override
	public void update(float delta) {

		// clear the slots
		for (LobbySlotContainer slot : slots)
			slot.reset();

		// assign / update
		for (OmniClient client : LogicTier.META_PLAYER_MAP.values())
			for (LobbySlotContainer slot : slots)
				if (client.getMetaClient().team == team
						&& client.getMetaClient().slot_index == slot.getIndex()) {

					slot.setName(client.getMetaClient().name);
					if (client.getMetaClient().pre_selected_gameclass != null
							&& client.getMetaClient().gameclass_name == null) {
						slot.setThumbnail(client.getMetaClient().pre_selected_gameclass);
						slot.setPreview(true);
					} else if (client.getMetaClient().gameclass_name != null) {
						slot.setThumbnail(client.getMetaClient().gameclass_name);
						slot.setPreview(false);
					}
				} else {
					// team assignment not valid
				}

	}

	@Override
	public void loadWidgets() {
		for (int i = 0; i < number_slots; i++)
			slots.add(new LobbySlotContainer(i, team,
					((LobbyRoot) screen.getRootModule()).getContextMenu()));
	}

	@Override
	public void setLayout() {

		CustomTable table = new CustomTable();

		float padRight = team == Team.TEAM_1 ? 800 : -800;
		table.top().padRight(padRight).setFillParent(true);
		tables.add(table);

		for (int i = 0; i < slots.size(); i++)
			table.add(slots.get(i));

	}

	@Override
	public void setListeners() {
	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return null;
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

}
