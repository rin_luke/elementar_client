package com.elementar.gui.modules;

import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.GameclassContainer;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.widgets.CustomSpineWidget;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.MetaClient;

public class PurchaseSuccessModule extends OkConfirmationModule {

	private CustomSpineWidget spine_widget_character;

	public PurchaseSuccessModule() {
		super("", BasicScreen.DRAW_COVERMODULE_2);

	}

	@Override
	public void loadWidgets() {
		super.loadWidgets();

		spine_widget_character = new CustomSpineWidget(new DrawableClient(
				new MetaClient(GameclassContainer.filterByName(GameclassName.ICE).getNameAsEnum(), 1),
				AnimationContainer.makeCharacterAnimation(0.2f), false));
	}

	@Override
	public void setLayout() {
		super.setLayout();

		CustomTable table = new CustomTable();
		tables.add(table);
		table.setFillParent(true);

		table.add(spine_widget_character).padRight(500);

	}

	public DrawableClient getClientModel() {
		return spine_widget_character.getClientModel();
	}

}
