package com.elementar.gui.modules.roots;

import static com.elementar.logic.util.Shared.SEND_AND_UPDATE_RATE_WORLD;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AGameclassSelectionModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.AOrderModule;
import com.elementar.gui.abstracts.ARootMenuModule;
import com.elementar.gui.abstracts.ARootNonWorldModule;
import com.elementar.gui.modules.lobby.SlotsModule;
import com.elementar.gui.modules.selection.ATeamSelectionModule;
import com.elementar.gui.modules.selection.GameclassCollectionModule;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomSpineWidget;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.LobbySlotContextMenu;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;
import com.elementar.gui.widgets.listener.DisabledHandledListener;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.map.MapLoader;
import com.elementar.logic.network.gameserver.GameserverLogic.ServerState;
import com.elementar.logic.network.protocols.ClientGameserverProtocol;
import com.elementar.logic.network.requests.PreMatchRequest;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.ClientState;
import com.elementar.logic.util.Shared.Team;
import com.esotericsoftware.spine.Animation.MixBlend;
import com.esotericsoftware.spine.Animation.MixDirection;

/**
 * lobby is a screen to choose a class
 * 
 * @author lukassongajlo
 * 
 */
public class LobbyRoot extends ARootNonWorldModule {

	private LobbySlotContextMenu context_menu_slot;

	private AChildModule wrapper_module;
	private SlotsModule team1_slots_module, team2_slots_module;
	private AGameclassSelectionModule gameclass_selection_module;
	private ATeamSelectionModule team_selection_module;

	private AChildModule post_match_module;

	private CustomSpineWidget bg_widget;

	private CustomIconbutton options_button, back_to_menu_button;

	/**
	 * switches to <code>true</code> after clicking the selection button.
	 * Gameserver has to validate the selection
	 */
	private boolean send_gameclass_selection = false;

	private boolean post_tab_active = false;

	/**
	 * map loader of previous loaded map
	 */
	private MapLoader map_loader;

	private float accumulator;

	public LobbyRoot(MapLoader mapLoader) {
		super();
		this.map_loader = mapLoader;
		LogicTier.BUILDINGS_MAP = null;
	}

	@Override
	public void update(float delta) {
		// update sub modules
		super.update(delta);

		logic_tier.updateBySnapshots();

		MetaClient myMetaClient = LogicTier.getMyClient().getMetaClient();

		boolean gameclassAlreadyAssigned = false;

		/*
		 * check whether pre-selected game class is already chosen by another
		 * player
		 */
		GameclassCollectionModule.resetCrossedOut();
		for (OmniClient client : LogicTier.META_PLAYER_MAP.values()) {

			if (myMetaClient != client.getMetaClient()
					&& myMetaClient.pre_selected_gameclass == client
							.getMetaClient().gameclass_name) {
				gameclassAlreadyAssigned = true;
			}

			GameclassCollectionModule.setCrossedOut(client.getMetaClient().gameclass_name);

		}

		// determine select button state
		if (myMetaClient.client_state == ClientState.PICKING_GAMECLASS) {

			if (send_gameclass_selection == true) {
				/*
				 * player did selection but game class is already assigned
				 */
				if (gameclassAlreadyAssigned == true) {
					// gameclass_already_assigned_module.setVisibleGlobal(true);
					send_gameclass_selection = false;
				}
			}
		} else
			gameclass_selection_module.getSelectButton().setState(State.DISABLED);

		// handles visibility of all modules
		if (LogicTier.SERVER_STATE == ServerState.POST_MATCH) {
			if (post_match_module.isVisible() == false) {
				post_tab_active = true;
				changeTab(post_match_module);
			}
		}

		/*
		 * if game class is assigned the select button's state is set to disabled
		 * as well. Additionally it is disabled when the server state is in POST
		 */
		if (myMetaClient.gameclass_name != null || gameclassAlreadyAssigned == true)
			gameclass_selection_module.getSelectButton().setState(State.DISABLED);

		if (myMetaClient.client_state == ClientState.MATCH
				&& LogicTier.SERVER_STATE == ServerState.IN_MATCH)
			gui_tier.changeRootModule(new WorldOnlineRoot(map_loader));

		if (post_tab_active == true && LogicTier.SERVER_STATE != ServerState.POST_MATCH)
			gui_tier.changeRootModule(new MaploadingRoot(ClientGameserverProtocol.MAP_SEED, true));

		// check received packets
		accumulator += delta;
		while (accumulator >= SEND_AND_UPDATE_RATE_WORLD) {
			accumulator -= SEND_AND_UPDATE_RATE_WORLD;

			team_selection_module.updateLabels();

			PreMatchRequest req = new PreMatchRequest();
			req.pre_selected_gameclass
					= GameclassCollectionModule.CHARACTER_MODEL.getMetaClient().gameclass_name;

			req.team = team_selection_module.getSelectedTeam();

			if (send_gameclass_selection == true) {
				req.skin_index
						= GameclassCollectionModule.CHARACTER_MODEL.getMetaClient().skin_index;
				req.selected_gameclass
						= GameclassCollectionModule.CHARACTER_MODEL.getMetaClient().gameclass_name;
			}

			ClientGameserverProtocol.RELIABILITY_SYSTEM.update(client_gameserver_connection,
					client_gameserver_connection.getClient().getID());
			ClientGameserverProtocol.RELIABILITY_SYSTEM.preparePacket(req);

			client_gameserver_connection.getClient().sendUDP(req);

		}

	}

	@Override
	public void loadWidgets() {

		context_menu_slot = new LobbySlotContextMenu();

		options_button = new CustomIconbutton(StaticGraphic.gui_icon_options, Shared.WIDTH3,
				Shared.HEIGHT2);
		options_button.setSound(ClickSoundType.LIGHT);

		back_to_menu_button = new CustomIconbutton(StaticGraphic.gui_icon_back,
				StaticGraphic.gui_icon_back.getWidth(), StaticGraphic.gui_icon_back.getHeight());
		back_to_menu_button.setSound(ClickSoundType.HEAVY);

		bg_widget = new CustomSpineWidget(AnimationContainer.skeleton_bg1_menu, "gui_bg_lobby",
				false);
		bg_widget.setMixBehaviour(MixBlend.first, MixDirection.in);

	}

	@Override
	public void setLayout() {

		CustomTable tableSkeleton = new CustomTable();
		tableSkeleton.setFillParent(true);
		tableSkeleton.center();
		tables.add(tableSkeleton);

		tableSkeleton.add(bg_widget);

		CustomTable tableWidgets = new CustomTable();
		tableWidgets.top().left().setFillParent(true);
		tables.add(tableWidgets);

		tableWidgets.add(options_button);
		tableWidgets.row();
		tableWidgets.add(back_to_menu_button);

	}

	@Override
	public void setListeners() {

		options_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				AModule.OPTIONS_MODULE.setVisibleGlobal(true);
			}
		});

		back_to_menu_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				logic_tier.reset();
				if (ARootMenuModule.MAINMENU_NAVIGATION_MODULE != null)
					GUIUtil.clickActor(ARootMenuModule.MAINMENU_NAVIGATION_MODULE.getButtonPlay());
			}
		});

	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(options_button);
		widgets.add(back_to_menu_button);
		return new HoverHandler(widgets);
	}

	@Override
	public void loadSubModules() {

		wrapper_module = new AChildModule(true) {

			private CustomLabel time_label;

			@Override
			public void update(float delta) {
				super.update(delta);

				// set time
				String preTimeText = "";

				if (LogicTier.SERVER_STATE == ServerState.PRE_MATCH)
					preTimeText = TextData.getWord("matchStartsIn") + "\n";

				time_label.setText(preTimeText + ""
						+ (int) Math.ceil(ClientGameserverProtocol.GAMESERVER_TIME_INTERPOLATED));

			}

			@Override
			public void setVisibleGlobal(boolean visible) {
				super.setVisibleGlobal(visible);

				// consider sub modules
				if (visible == false)
					for (AModule subModule : this.sub_modules)
						subModule.setVisibleGlobal(visible);

			}

			@Override
			public void loadWidgets() {
				time_label = new CustomLabel("", StyleContainer.label_bold_white_30_style,
						Shared.WIDTH5, Shared.HEIGHT2, Align.center);
			}

			@Override
			public void setLayout() {
				CustomTable tableTimeLabel = new CustomTable();
				tableTimeLabel.top().padTop(20).setFillParent(true);
				tables.add(tableTimeLabel);

				tableTimeLabel.add(time_label);
			}

			@Override
			public void setListeners() {

			}

			@Override
			public void loadSubModules() {

				boolean postMatch = LogicTier.SERVER_STATE == ServerState.POST_MATCH;
				boolean teamSet = LogicTier.getMyClient().getMetaClient().team != null;

				gameclass_selection_module = new AGameclassSelectionModule(teamSet && !postMatch,
						draw_order, false, true, false) {

					@Override
					public DisabledHandledListener getSelectListener(
							CustomTextbutton selectButton) {
						return new DisabledHandledListener(selectButton) {

							@Override
							public void afterDisabledCheck() {
								send_gameclass_selection = true;
								selectButton.setState(State.DISABLED);
							}
						};
					}
				};

				team1_slots_module = new SlotsModule(!postMatch, Team.TEAM_1);
				team2_slots_module = new SlotsModule(!postMatch, Team.TEAM_2);

				// visible = true if team is not set yet
				team_selection_module = new ATeamSelectionModule(!teamSet && !postMatch) {

					@Override
					public void afterTeamSelection() {
						team_selection_module.setVisibleGlobal(false);
						gameclass_selection_module.setVisibleGlobal(true);
					}
				};

			}

			@Override
			public void addSubModules(ArrayList<AModule> subModules) {
				subModules.add(team1_slots_module);
				subModules.add(team2_slots_module);
				subModules.add(gameclass_selection_module);
				subModules.add(team_selection_module);
			}

			@Override
			protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
				return null;
			}
		};

		post_match_module = new AChildModule(false) {

			private CustomIconbutton bg_image;
			private CustomLabel text_label, time_label;

			@Override
			public void update(float delta) {
				super.update(delta);

				time_label.setText(""
						+ (int) Math.ceil(ClientGameserverProtocol.GAMESERVER_TIME_INTERPOLATED));

			}

			@Override
			public void loadWidgets() {
				bg_image = new CustomIconbutton(StaticGraphic.gui_bg_post_lobby, 0, 0);
				text_label = new CustomLabel(TextData.getWord("matchJustEnded"),
						StyleContainer.label_bold_white_30_style, 0, Shared.HEIGHT1,
						Align.center);
				time_label = new CustomLabel(TextData.getWord(""),
						StyleContainer.label_bold_whitepure_40_style, 0, Shared.HEIGHT2,
						Align.center);
			}

			@Override
			public void setLayout() {

				CustomTable tableBGImage = new CustomTable();
				tableBGImage.setFillParent(true);
				tableBGImage.center();
				tables.add(tableBGImage);

				tableBGImage.add(bg_image);

				CustomTable tableLabels = new CustomTable();
				tableLabels.setFillParent(true);
				tableLabels.center();
				tables.add(tableLabels);

				tableLabels.add(text_label);
				tableLabels.row();
				tableLabels.add(time_label);
			}

			@Override
			public void setListeners() {

			}

			@Override
			public void loadSubModules() {

			}

			@Override
			public void addSubModules(ArrayList<AModule> subModules) {

			}

			@Override
			protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
				return null;
			}

		};

	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {

		subModules.add(wrapper_module);
		subModules.add(post_match_module);
		subModules.add(new AOrderModule() {

			@Override
			public void setLayout() {
				CustomTable tableChatWindow = new CustomTable();
				tableChatWindow.setFillParent(true);
				this.tables.add(tableChatWindow);
			}

		});
	}

	@Override
	protected void addTabs(ArrayList<AModule> tabs) {
		super.addTabs(tabs);

		tabs.add(wrapper_module);
		tabs.add(post_match_module);

	}

	@Override
	protected boolean clickedEscape() {
		client_gameserver_connection.disconnect();

		return true;
	}

	@Override
	public void unfocus() {
		super.unfocus();
		context_menu_slot.unfocusWidget();
	}

	public LobbySlotContextMenu getContextMenu() {
		return context_menu_slot;
	}

}
