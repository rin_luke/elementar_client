package com.elementar.gui.modules.roots;

import java.util.ArrayList;

import com.elementar.gui.abstracts.AGameclassSelectionModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.ARootMenuModule;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.listener.DisabledHandledListener;

public class SpiritsRoot extends ARootMenuModule {

	public static int INDEX_GAMECLASS = 0, INDEX_SKIN = 0;

	private AGameclassSelectionModule gameclass_selection_module;

	public SpiritsRoot() {
		super();

	}

	@Override
	public void loadWidgets() {

	}

	@Override
	public void setLayout() {
	}

	@Override
	public void setListeners() {
	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return null;
	}

	@Override
	public void loadSubModules() {
		super.loadSubModules();

		gameclass_selection_module = new AGameclassSelectionModule(true, draw_order, true, false, true) {

			@Override
			public DisabledHandledListener getSelectListener(CustomTextbutton selectButton) {
				return null;
			}
		};

	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		super.addSubModules(subModules);
		subModules.add(gameclass_selection_module);
	}

	@Override
	protected boolean clickedEscape() {
		return true;
	}

}
