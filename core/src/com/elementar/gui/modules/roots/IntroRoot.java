package com.elementar.gui.modules.roots;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.DataTier;
import com.elementar.data.container.GameclassSkinContainer;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.gui.GUITier;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.modules.selection.GameclassCollectionModule;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.GameclassIconbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.util.Shared;

/**
 * Might be used for game company fade in / fade out at the beginning
 * 
 * @author lukassongajlo
 * 
 */
public class IntroRoot extends AModule {

	private Image leaf_image;

	private CustomLabel loading_label;

	private boolean has_loaded = false;

	public IntroRoot() {
		super(true, true, BasicScreen.DRAW_REGULAR);

	}

	@Override
	public void show() {

		/*
		 * we need a pre-loading step here to get font data to display the progress in %
		 */

		DataTier.preload();
		super.show();
		DataTier.load();
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		if (has_loaded == true) {

			gui_tier.changeRootModule(new HomeRoot());
			return;
		}
		if (DataTier.ASSET_MANAGER.update(16) == true) {

			// set all assets attributes
			DataTier.setFields();

			// invoke loading of gameclass buttons
			GameclassCollectionModule.invoke();

			if (GUITier.STEAM_INTERFACE != null) {
				GameclassSkinContainer.loadSkinsWithoutSteam();
				// update layout of skin sub-section
				for (GameclassIconbutton button : GameclassCollectionModule.GAMECLASS_BUTTONS)
					button.updateSubSection();

//				GUITier.STEAM_INTERFACE.loadItems();
			} else {

				GameclassSkinContainer.loadSkinsWithoutSteam();

				// update layout of skin sub-section
				for (GameclassIconbutton button : GameclassCollectionModule.GAMECLASS_BUTTONS)
					button.updateSubSection();

			}

			has_loaded = true;

		}

		loading_label.setText((int) (DataTier.ASSET_MANAGER.getProgress() * 100) + " %");
	}

	@Override
	public void loadWidgets() {

		/*
		 * the only particle effect which has to be loaded previously, because the
		 * datatier is loaded after this
		 */
		leaf_image = new Image(StaticGraphic.loadLoadingIndicator());

		loading_label = new CustomLabel("", StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.center);
	}

	@Override
	public void setLayout() {
		CustomTable table = new CustomTable();
		table.setFillParent(true);
		tables.add(table);

		table.add(leaf_image);
		table.row();
		table.add(loading_label);

	}

	@Override
	public void setListeners() {

	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return null;
	}

	@Override
	protected boolean clickedEscape() {
		return true;
	}

	@Override
	public void loadSubModules() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		// TODO Auto-generated method stub

	}
}
