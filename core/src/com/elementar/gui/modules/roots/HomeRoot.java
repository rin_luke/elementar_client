package com.elementar.gui.modules.roots;

import static com.elementar.logic.util.Shared.GAME_VERSION;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Graphics;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.GameclassContainer;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.gui.abstracts.ARootMenuModule;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomSpineWidget;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.MetaClient;

public class HomeRoot extends ARootMenuModule {

	private CustomLabel version_label;

	private CustomSpineWidget spine_widget_play, spine_widget_host, spine_widget_discord, spine_widget_platform,
			spine_widget_character, spine_widget_title;
	private CustomTextbutton button_big_play, button_big_host, button_big_discord, button_big_title;

	public HomeRoot() {
		super();
	}

	@Override
	public void show() {
		super.show();

		MAINMENU_NAVIGATION_MODULE.getButtonHome().setState(State.SELECTED);

	}

	@Override
	public void loadWidgets() {
		version_label = new CustomLabel(GAME_VERSION, StyleContainer.label_bold_whitepure_18_style, 150, 50,
				Align.left);

		spine_widget_host = new CustomSpineWidget(AnimationContainer.skeleton_home_screen_host, "inactive", false);
		spine_widget_play = new CustomSpineWidget(AnimationContainer.skeleton_home_screen_play, "inactive", false);
		spine_widget_discord = new CustomSpineWidget(AnimationContainer.skeleton_home_screen_discord, "inactive",
				false);
		spine_widget_platform = new CustomSpineWidget(AnimationContainer.skeleton_home_screen_platform,
				"skin_platform_idle", true);
		spine_widget_character = new CustomSpineWidget(new DrawableClient(
				new MetaClient(GameclassContainer.filterByName(GameclassName.ICE).getNameAsEnum(), 1),
				AnimationContainer.makeCharacterAnimation(0.5f), true));
		spine_widget_title = new CustomSpineWidget(AnimationContainer.skeleton_home_screen_title, "inactive", false);

		button_big_host = new BigButton(spine_widget_host);
		button_big_play = new BigButton(spine_widget_play);
		button_big_discord = new BigButton(spine_widget_discord);
		button_big_title = new BigButton(spine_widget_title);
	}

	@Override
	public void setLayout() {

		float heightMidBtns = 480f * AnimationContainer.SCALE_HOME_SCREEN;
		float heightLeftBtns = 600f * AnimationContainer.SCALE_HOME_SCREEN;
		float widthLeftBtns = 480f * AnimationContainer.SCALE_HOME_SCREEN;
		float widthCenterBtns = 852f * AnimationContainer.SCALE_HOME_SCREEN;
		float padBotBtns = 44 * AnimationContainer.SCALE_HOME_SCREEN;
		float padHorizontal = 1420f * AnimationContainer.SCALE_HOME_SCREEN;

		// skin button
		float widthSkinButton = 500f * AnimationContainer.SCALE_HOME_SCREEN;
		float heightSkinButton = 1050 * AnimationContainer.SCALE_HOME_SCREEN;
		float padSkinButton = 1600f * AnimationContainer.SCALE_HOME_SCREEN;

		CustomTable tableVersion = new CustomTable();
		tableVersion.bottom().setFillParent(true);
		tables.add(tableVersion);
		tableVersion.add(version_label).padBottom(version_label.getHeight() * 0.5f);

		float padTop = 120;

		// spine widgets center
		CustomTable tableSpineCenter = new CustomTable();
		tableSpineCenter/* .center() */.setFillParent(true);
		tableSpineCenter.padTop(padTop);
		tables.add(tableSpineCenter);
		tableSpineCenter.add(spine_widget_play).width(widthCenterBtns).height(heightMidBtns).padBottom(padBotBtns);
		tableSpineCenter.row();
		tableSpineCenter.add(spine_widget_host).width(widthCenterBtns).height(heightMidBtns);
		// spine widget left
		CustomTable tableSpineLeft = new CustomTable();
		tableSpineLeft.padRight(padHorizontal)/* .center() */.setFillParent(true);
		tables.add(tableSpineLeft);
		tableSpineLeft.add(spine_widget_discord).width(widthLeftBtns).height(heightLeftBtns);

		// platform
		CustomTable tablePlatform = new CustomTable();
		tablePlatform.padLeft(padSkinButton)/* .center() */.setFillParent(true);
		tables.add(tablePlatform);

		tablePlatform.add(spine_widget_platform).padTop(400);

		// character
		CustomTable tableCharacter = new CustomTable();
		tableCharacter.padLeft(padSkinButton)/* .center() */.setFillParent(true);
		tables.add(tableCharacter);

		tableCharacter.add(spine_widget_character).padTop(100);

		// title
		CustomTable tableTitle = new CustomTable();
		tableTitle.padLeft(padSkinButton)/* .center() */.setFillParent(true);
		tables.add(tableTitle);

		tableTitle.add(spine_widget_title).padTop(400);

		CustomTable tableButtonsCenter = new CustomTable();
		tableButtonsCenter/* .center() */.setFillParent(true);
		tableButtonsCenter.padTop(padTop);
		tables.add(tableButtonsCenter);
		tableButtonsCenter.add(button_big_play).width(widthCenterBtns).height(heightMidBtns).padBottom(padBotBtns);
		tableButtonsCenter.row();
		tableButtonsCenter.add(button_big_host).width(widthCenterBtns).height(heightMidBtns);
		// spine widgets left
		CustomTable tableButtonsLeft = new CustomTable();
		tableButtonsLeft.padRight(padHorizontal)/* .center() */.setFillParent(true);
		tables.add(tableButtonsLeft);
		tableButtonsLeft.add(button_big_discord).width(widthLeftBtns).height(heightLeftBtns);

		CustomTable tableButtons2 = new CustomTable();
		tableButtons2/* .center() */.setFillParent(true);
		tables.add(tableButtons2);

		tableButtons2.add(button_big_title).width(widthSkinButton).height(heightSkinButton).padLeft(padSkinButton);

	}

	@Override
	public void setListeners() {

		button_big_play.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				GUIUtil.clickActor(MAINMENU_NAVIGATION_MODULE.getButtonPlay());
			}
		});

		button_big_host.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				GUIUtil.clickActor(MAINMENU_NAVIGATION_MODULE.getButtonPlay());
				GUIUtil.clickActor(MAINMENU_NAVIGATION_MODULE.getSubNavigationModule().getHostServerButton());
				MAINMENU_NAVIGATION_MODULE.getSubNavigationModule().getSearchGameButton().setState(State.NOT_SELECTED);
				MAINMENU_NAVIGATION_MODULE.getSubNavigationModule().getHostServerButton().setState(State.SELECTED);
			}
		});

		button_big_discord.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				((Lwjgl3Graphics) Gdx.graphics).getWindow().iconifyWindow();
				Gdx.net.openURI("https://discord.gg/9RxY8Vf5vw");
				
			}
		});

		button_big_title.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				SpiritsRoot.INDEX_GAMECLASS = 1; // = ice
				SpiritsRoot.INDEX_SKIN = 1; // = second skin
				GUIUtil.clickActor(MAINMENU_NAVIGATION_MODULE.getButtonSpirits());
			}
		});
	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(button_big_host);
		widgets.add(button_big_play);
		widgets.add(button_big_discord);
		widgets.add(button_big_title);
		return new HoverHandler(widgets);
	}

	@Override
	protected boolean clickedEscape() {
		return true;
	}

	@Override
	public void unfocus() {
	}

	private class BigButton extends CustomTextbutton {

		private CustomSpineWidget spine_widget;

		public BigButton(CustomSpineWidget spineWidget) {
			this.spine_widget = spineWidget;
		}

		@Override
		public void act(float delta) {
			super.act(delta);

			if (getState() == State.HOVERED && spine_widget.getCurrentAnimation().equals("inactive")) {
				spine_widget.setAnimation("active");
				spine_widget.setAnimationTime(0);

			} else if (getState() != State.HOVERED && spine_widget.getCurrentAnimation().equals("active")) {
				spine_widget.setAnimation("inactive");
				spine_widget.setAnimationTime(0);

			}

		}
	}

}
