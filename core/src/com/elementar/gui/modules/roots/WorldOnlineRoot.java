package com.elementar.gui.modules.roots;

import java.util.ArrayList;

import com.elementar.gui.abstracts.ARootWorldModule;
import com.elementar.gui.modules.ingame.APlayerUIModule;
import com.elementar.gui.modules.ingame.PlayerUIOnlineModule;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.map.MapLoader;

public class WorldOnlineRoot extends ARootWorldModule {

	// private TeamSelectionModule team_selection_module;
	/**
	 * 
	 * @param mapLoader
	 */
	public WorldOnlineRoot(MapLoader mapLoader) {
		super(mapLoader);
	}

	@Override
	protected void drawSpecific() {

	}

	@Override
	public void setListeners() {
	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return null;
	}

	@Override
	public boolean isTrainingsWorld() {
		return false;
	}

	@Override
	protected APlayerUIModule createPlayerUIModule() {
		return new PlayerUIOnlineModule(this);
	}

}
