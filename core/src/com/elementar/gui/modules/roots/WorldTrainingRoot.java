package com.elementar.gui.modules.roots;

import static com.elementar.logic.util.Shared.CHARACTER_MAX_VELOCITY_X;
import static com.elementar.logic.util.Shared.DEBUG_MODE_DRAW_CYAN_CONTOURS_AND_RED_VERTICES_OF_ISLANDS;
import static com.elementar.logic.util.Shared.DEBUG_MODE_SUPERMAN;
import static com.elementar.logic.util.Shared.PPM;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.elementar.data.container.KeysConfiguration;
import com.elementar.data.container.KeysConfiguration.WorldAction;
import com.elementar.data.container.util.AdvancedKey;
import com.elementar.gui.abstracts.AGameclassSelectionModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.ARootWorldModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.modules.ingame.APlayerUIModule;
import com.elementar.gui.modules.ingame.PlayerUITrainingModule;
import com.elementar.gui.modules.selection.WorldTrainingSelectionModule;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.creeps.OmniCreep;
import com.elementar.logic.map.MapBorder;
import com.elementar.logic.map.MapLoader;
import com.elementar.logic.map.RandomIsland;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.ViewCircle;

public class WorldTrainingRoot extends ARootWorldModule {

	private MapBorder map_border;

	private ShapeRenderer ui_renderer;

	private boolean draw_creep_circles = false;

	private AGameclassSelectionModule gameclass_selection_module;

	public WorldTrainingRoot(MapLoader mapLoader) {
		super(mapLoader);
		this.map_border = map_loader.getMapBorder();
		this.ui_renderer = BasicScreen.SHAPE_RENDERER;
		my_omni_client.init(logic_tier.getClientMainWorld(), logic_tier.getClientParaWorld());
	}

	@Override
	protected APlayerUIModule createPlayerUIModule() {
		return new PlayerUITrainingModule(this);
	}

	@Override
	protected void drawSpecific() {
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

		ui_renderer.setProjectionMatrix(world_cam.combined);
		ui_renderer.begin(ShapeType.Line);

		// ui_renderer.rect(-(Shared.MAP_WIDTH * 0.5f) * 0.75f,
		// -(Shared.MAP_HEIGHT * 0.5f),
		// Shared.MAP_WIDTH * 0.75f, Shared.MAP_HEIGHT);
		//
		// ui_renderer.rect(-(Shared.MAP_WIDTH * 0.5f) * 0.75f,
		// -HorizontalSegments.X_AXIS_BAND_WIDTH,
		// Shared.MAP_WIDTH * 0.75f, HorizontalSegments.X_AXIS_BAND_WIDTH * 2);
		if (draw_creep_circles == true)
			for (OmniCreep creep : LogicTier.CREEP_LIVING_LIST) {
				ViewCircle fogDispel = creep.getPhysicalCreep().getViewCircle();

				int circlePoints = 40;
				ViewCircle attackEntryCircle = creep.getPhysicalCreep().getAttackEntryCircle();
				ViewCircle fightStanceEntryCircle = creep.getPhysicalCreep().getFightStanceEntryCircle();

				ui_renderer.setColor(1, 0.5f, 0, 1);
				if (fogDispel.getFogDispelWorld() != null)
					ui_renderer.circle(fogDispel.getX(), fogDispel.getY(), fogDispel.getFogDispelWorld().getDistance(),
							20);
				ui_renderer.setColor(1, 0, 0, 1);
				ui_renderer.circle(attackEntryCircle.getX(), attackEntryCircle.getY(), attackEntryCircle.getRadius(),
						circlePoints);
				ui_renderer.setColor(0.5f, 1f, 0.5f, 1f);
				ui_renderer.circle(fightStanceEntryCircle.getX(), fightStanceEntryCircle.getY(),
						fightStanceEntryCircle.getRadius(), circlePoints);
			}

		ui_renderer.end();

		ui_renderer.begin(ShapeType.Filled);
		ui_renderer.setColor(1, 0, 0, 1);

		// DRAW ISLAND LINES
		if (DEBUG_MODE_DRAW_CYAN_CONTOURS_AND_RED_VERTICES_OF_ISLANDS == true)
			for (

			RandomIsland island : map_manager.getIslands()) {
				if (Util.convertVector2ArrayIntoFloatArray(island.getGlobalVertices()).length >= 6)
					ui_renderer.polygon(Util.convertVector2ArrayIntoFloatArray(island.getGlobalVertices()));

			}
		ui_renderer.end();

		// ui_renderer.begin(ShapeType.Line);
		// for (BGSkeleton skeleton : AnimationContainer.bg4_skeletons) {
		// ui_renderer.setColor(1, 1, 1, 1);
		// ui_renderer
		// .polygon(Util.convertVector2ArrayIntoFloatArray(skeleton.getBoundingBoxData().getGlobalHitbox()));
		// ui_renderer.setColor(0, 1, 0, 1);
		// ui_renderer.rect(skeleton.getPolygon().getBoundingRectangle().x,
		// skeleton.getPolygon().getBoundingRectangle().y,
		// skeleton.getPolygon().getBoundingRectangle().width,
		// skeleton.getPolygon().getBoundingRectangle().height);
		// }

		// ui_renderer.setColor(0, 0, 1, 1);
		//
		// for (Node node : map_manager.getGraph().getNodes()) {
		// if (node.containsObstacle() == true)
		// ui_renderer.setColor(1, 0, 0, 0.1f);
		// else
		// ui_renderer.setColor(0, 1, 0, 0.1f);
		// ui_renderer.rect(node.getPos().x - Graph.CELL_SIDE_LENGTH * 0.5f,
		// node.getPos().y - Graph.CELL_SIDE_LENGTH * 0.5f,
		// Graph.CELL_SIDE_LENGTH, Graph.CELL_SIDE_LENGTH);
		// }
		//
		// ui_renderer.end();
		// ui_renderer.begin(ShapeType.Line);
		//
		// ui_renderer.setColor(0, 0, 0, 1f);
		// for (Node node : map_manager.getGraph().getNodes()) {
		// ui_renderer.rect(node.getPos().x - Graph.CELL_SIDE_LENGTH * 0.5f,
		// node.getPos().y - Graph.CELL_SIDE_LENGTH * 0.5f,
		// Graph.CELL_SIDE_LENGTH, Graph.CELL_SIDE_LENGTH);
		// }
		// ui_renderer.setColor(1f, 1f, 1f, 1f);
		// for (LightCluster cluster :
		// LogicTier.VIEW_HANDLER.getLightClusters()) {
		// ui_renderer.circle(cluster.getPos().x, cluster.getPos().y,
		// cluster.getDistance(), 20);
		// }

		// FOR MAPBORDER HALF-PAGE (BLUE)
		// ui_renderer.setColor(0, 0, 1, 0.2f);
		// ui_renderer.rect(map_border.sampling.x, map_border.sampling.w,
		// map_border.sampling.y,
		// map_border.sampling.z - map_border.sampling.w);
		//
		// ui_renderer.setColor(1, 0, 0, 1f);
		// for (VerticesCorrespondings v :
		// map_border.getGen().getVisualIntersections()) {
		// ui_renderer.circle(v.vertex.x, v.vertex.y, 0.2f);
		// }

		// ui_renderer
		// .rect(map_border.sampingPlusDirac.x,
		// map_border.sampingPlusDirac.w,
		// map_border.sampingPlusDirac.y, map_border.sampingPlusDirac.z
		// - map_border.sampingPlusDirac.w);

		// DRAW VIEWPORTS (vegetation and player viewport)
		if (Shared.DEBUG_MODE_DRAW_VIEWPORTS == true) {
			ui_renderer.rect(vegetation_viewport.x, vegetation_viewport.y, vegetation_viewport.width,
					vegetation_viewport.height);
			ui_renderer.setColor(1, 0, 0, 1);
			ui_renderer.rect(world_cam.position.x - world_cam.viewportWidth * 0.5f,
					world_cam.position.y - world_cam.viewportHeight * 0.5f, world_cam.viewportWidth,
					world_cam.viewportHeight);
		}
		ui_renderer.end();

		// DRAW ISLAND VERTICES
		if (DEBUG_MODE_DRAW_CYAN_CONTOURS_AND_RED_VERTICES_OF_ISLANDS == true) {
			ui_renderer.begin(ShapeType.Filled);
			for (RandomIsland island : map_manager.getIslands()) {

				for (int i = 0; i < island.getGlobalVertices().length; i++) {
					Vector2 globalVertex = island.getGlobalVertices()[i];
					if (i == 1)
						ui_renderer.setColor(1, 1, 0, 1);
					else if (i == 0)
						ui_renderer.setColor(0, 1, 0, 1);
					else
						ui_renderer.setColor(1, 0, 0, 1);

					ui_renderer.circle(globalVertex.x, globalVertex.y, 0.03f, 12);
					// font.draw(stage.getBatch(), " o ", vertex.x, vertex.y);
				}
			}
			for (int i = 0; i < map_border.getLocalVertices().length; i++) {
				Vector2 vec = map_border.getLocalVertices()[i];
				if (i == 1)
					ui_renderer.setColor(1, 1, 0, 1);
				else if (i == 0)
					ui_renderer.setColor(0, 1, 0, 1);
				else
					ui_renderer.setColor(1, 0, 0, 1);
				ui_renderer.circle(vec.x, vec.y, 0.03f, 12);
			}
			ui_renderer.end();
		}

		// DRAW MAPBORDER RED POINTS
		// ui_renderer.begin(ShapeType.Filled);
		//
		// for (int i = 0; i < map_border.getLocalVertices().length; i++) {
		// Vector2 globalVertex = map_border.getLocalVertices()[i];
		// if (i == 1)
		// ui_renderer.setColor(1, 1, 0, 1);
		// else if (i == 0)
		// ui_renderer.setColor(0, 1, 0, 1);
		// else
		// ui_renderer.setColor(1, 0, 0, 1);
		//
		// ui_renderer.circle(globalVertex.x, globalVertex.y, 0.03f, 12);
		// }
		// ui_renderer.end();

	}

	@Override
	public void setListeners() {
	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return null;
	}

	@Override
	public void loadSubModules() {
		super.loadSubModules();

		gameclass_selection_module = new WorldTrainingSelectionModule(draw_order,
				((PlayerUITrainingModule) player_ui_module).getSwitchButton(), this);
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		super.addSubModules(subModules);
		subModules.add(gameclass_selection_module);
	}

	@Override
	protected void addTabs(ArrayList<AModule> tabs) {
		super.addTabs(tabs);
		tabs.add(gameclass_selection_module);

	}

	@Override
	public void touchDown(int screenX, int screenY, int pointer, int button) {
		if (CURSOR_UI_CHECK.isAtUIWidgets() == false && CURSOR_UI_CHECK.isAtUIModules() == false) {
			if (enable_dummy_movement == true) {
				Iterator<OmniClient> it = LogicTier.WORLD_PLAYER_MAP.values().iterator();
				OmniClient client;
				while (it.hasNext() == true) {
					client = it.next();
					if (client.isDummy() == true) {
						WorldAction action = KeysConfiguration.getAction(new AdvancedKey(button, false));
						client.getDrawableClient().setCurrentAction(action, true);
						client.getPhysicalClient().setCurrentAction(action, true);
					}
				}
			}
		}
		super.touchDown(screenX, screenY, pointer, button);
	}

	@Override
	public void touchUp(int screenX, int screenY, int pointer, int button) {
		AdvancedKey advancedKey = new AdvancedKey(button, false);
		released_keys.add(advancedKey);
		super.touchUp(screenX, screenY, pointer, button);
		if (enable_dummy_movement == true) {
			Iterator<OmniClient> it = LogicTier.WORLD_PLAYER_MAP.values().iterator();
			OmniClient client;
			while (it.hasNext() == true) {
				client = it.next();
				if (client.isDummy() == true) {
					WorldAction action = KeysConfiguration.getAction(advancedKey);
					client.getDrawableClient().setCurrentAction(action, false);
					client.getPhysicalClient().setCurrentAction(action, false);
				}
			}
		}
	}

	private int jumpPower = 0;

	@Override
	public void releaseAllKeys() {
		super.releaseAllKeys();

		/*
		 * at some point we need to reset the transfer skill assignment. This method is
		 * a good place for that.
		 */
		for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values())
			if (client.isDummy() == true) {
				client.getDrawableClient().setCurrentAction(WorldAction.EXECUTE_TRANSFER_SKILL1, false);
				client.getPhysicalClient().setCurrentAction(WorldAction.EXECUTE_TRANSFER_SKILL1, false);

				client.getDrawableClient().setCurrentAction(WorldAction.EXECUTE_TRANSFER_SKILL2, false);
				client.getPhysicalClient().setCurrentAction(WorldAction.EXECUTE_TRANSFER_SKILL2, false);
			}

	}

	@Override
	public void keyDown(int keycode) {
		super.keyDown(keycode);

		if (CURSOR_UI_CHECK.isAtUIModules() == false)
			if (enable_dummy_movement == true) {
				Iterator<OmniClient> it = LogicTier.WORLD_PLAYER_MAP.values().iterator();
				OmniClient client;
				while (it.hasNext() == true) {
					client = it.next();
					if (client.isDummy() == true) {
						WorldAction action = KeysConfiguration.getAction(new AdvancedKey(keycode, true));
						client.getDrawableClient().setCurrentAction(action, true);
						client.getPhysicalClient().setCurrentAction(action, true);
					}
				}
			}

		if (keycode == Keys.P)
			draw_creep_circles = !draw_creep_circles;

		if (CURSOR_UI_CHECK.isAtUIModules() == false) {
			if (DEBUG_MODE_SUPERMAN == true) {
				if (keycode == Keys.F) {
					if (CHARACTER_MAX_VELOCITY_X < 100f)
						CHARACTER_MAX_VELOCITY_X = 500f;
					else
						CHARACTER_MAX_VELOCITY_X = 250 / PPM;
					if (jumpPower == 0)
						jumpPower = 300;
					else
						jumpPower = 0;
				}

				if (keycode == Keys.SPACE)
					my_physical_client.getHitbox().getBody().applyForceToCenter(new Vector2(0, jumpPower), true);
			}
		}

	}

	@Override
	public void keyUp(int keycode) {
		super.keyUp(keycode);

		// release key dummy
		Iterator<OmniClient> it = LogicTier.WORLD_PLAYER_MAP.values().iterator();
		OmniClient client;
		while (it.hasNext() == true) {
			client = it.next();
			if (client.isDummy() == true) {
				WorldAction action = KeysConfiguration.getAction(new AdvancedKey(keycode, true));
				client.getDrawableClient().setCurrentAction(action, false);
				client.getPhysicalClient().setCurrentAction(action, false);
			}
		}
	}

	@Override
	public boolean isTrainingsWorld() {
		return true;
	}

	public AGameclassSelectionModule getGameclassSelectionModule() {
		return gameclass_selection_module;
	}

}
