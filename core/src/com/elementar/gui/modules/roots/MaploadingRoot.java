package com.elementar.gui.modules.roots;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.ARootNonWorldModule;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.map.MapLoader;
import com.elementar.logic.util.Shared;

/**
 * In this root module resets the {@link LogicTier#map_loader} object and
 * creates new client worlds. After that the map will be loaded.
 * 
 * @author lukassongajlo
 * 
 */
public class MaploadingRoot extends ARootNonWorldModule {

	/**
	 * For both scenarios (it creates map for training, and creates map for
	 * multiplayer).
	 */
	private MapLoader map_loader;

	private boolean is_multiplayer;

	private CustomLabel loading_map_label;
	private CustomIconbutton loading_bar_icon;

	/**
	 * 
	 * @param seed
	 * @param isMultiplayer
	 */
	public MaploadingRoot(long seed, boolean isMultiplayer) {
		super();

		is_multiplayer = isMultiplayer;

		// reset
		if (is_multiplayer == true)
			logic_tier.softReset();
		
		logic_tier.createNewClientWorlds();

		// initiate mapLoader, pass location
		map_loader = new MapLoader(isMultiplayer == true ? Location.CLIENTSIDE_MULTIPLAYER
				: Location.CLIENTSIDE_TRAINING);
		map_loader.startLoadingOnce(logic_tier.getClientMainWorld(), seed);

	}

	/**
	 * Just for {@link WorldTrainingRoot}
	 * 
	 * @param seed
	 */
	public MaploadingRoot(long seed) {
		this(seed, false);
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		if (is_multiplayer == true)
			// check received packets
			logic_tier.updateBySnapshots();

		// continuous update of loading bar width
		loading_bar_icon.getIcon().setScale(
				((float) map_loader.getCurrentState() / map_loader.getEndState()),
				// height no change
				1);

		// if algorithm has failed it wont go through this 'if'
		if (map_loader.isCompleted() == true) {
			if (is_multiplayer == false)
				gui_tier.changeRootModule(new WorldTrainingRoot(map_loader));
			else if (LogicTier.getMyClient() != null) {
				gui_tier.changeRootModule(new LobbyRoot(map_loader));
			}
		}

		if (map_loader.hasFailed() == true)
			clickedEscape();

	}

	@Override
	public void loadWidgets() {

		loading_bar_icon = new CustomIconbutton(StaticGraphic.gui_progressbar,
				StaticGraphic.gui_bg_progressbar) {
			@Override
			public void draw(Batch batch, float parentAlpha) {
				/*
				 * Dirty! I copied the code from CustomTextbutton and shifted
				 * the drawAdditionalSprites() line above the background
				 * rendering. Otherwise the bar is drawn shifted.
				 */
				ShaderPolyBatch shaderBatch = (ShaderPolyBatch) batch;

				// draw icon first, then bg
				drawAdditionalSprites(shaderBatch);

				if (background != null) {
					if (state == State.DISABLED)
						background.setAlpha(Shared.WIDGET_DISABLED_ALPHA);
					background.setPosition(getX(), getY());
					background.draw(shaderBatch);
					background.setAlpha(1f);
				}
				shaderBatch.flush();
				shaderBatch.reset();
			}
		};
		loading_bar_icon.setTopAlign();
		loading_bar_icon.setIconShiftY(loading_bar_icon.getShiftY() - 3);
		loading_bar_icon.getIcon().setScale(0);

		loading_map_label = new CustomLabel(TextData.getWord("loadingMap"),
				StyleContainer.label_bold_ivory2_18_bordered_style, Shared.WIDTH5,
				Shared.HEIGHT1, Align.center);

	}

	@Override
	public void setLayout() {

		// layout progress bar
		CustomTable progressTable = new CustomTable();
		progressTable.bottom().padBottom(100).setFillParent(true);
		tables.add(progressTable);

		progressTable.add(loading_map_label);
		progressTable.row();
		progressTable.add(loading_bar_icon);

	}

	@Override
	public void setListeners() {
	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return null;
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

	@Override
	protected boolean clickedEscape() {
		if (is_multiplayer == false) {
			map_loader.stopLoading();
			gui_tier.changeRootModule(new SpiritsRoot());
		}

		return true;
	}

}
