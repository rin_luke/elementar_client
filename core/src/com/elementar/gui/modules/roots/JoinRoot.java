package com.elementar.gui.modules.roots;

import static com.elementar.logic.network.protocols.ClientMainserverProtocol.GAMESERVER_LIST;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.ARootMenuModule;
import com.elementar.gui.modules.APasswordModule;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.GameserverContainer;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.TooltipTextArea;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;
import com.elementar.gui.widgets.listener.DisabledHandledListener;
import com.elementar.gui.widgets.listener.TooltipListener;
import com.elementar.logic.LogicTier;
import com.elementar.logic.network.connection.ClientGameserverConnection;
import com.elementar.logic.network.gameserver.MetaDataGameserver;
import com.elementar.logic.network.protocols.ClientGameserverProtocol;
import com.elementar.logic.network.requests.ServerListRequest;
import com.elementar.logic.util.ErrorMessage;
import com.elementar.logic.util.Shared;

public class JoinRoot extends ARootMenuModule {

	public static MetaDataGameserver SELECTED_GAMESERVER;
	public static boolean CONNECT_BY_DOUBLECLICK = false;

//	private DirectConnectModule direct_connect_module;
	private APasswordModule password_module;

	private CustomTextbutton join_button;
	private TooltipTextArea join_tooltip;

	private CustomIconbutton refresh_button, bg_icon;
	private CustomLabel server_label, number_player_label, ping_label, direct_connect_label;
	private CustomIconbutton direct_connect_question_mark;
	private TooltipTextArea direct_connect_tooltip;

	private VerticalGroup gameserver_verticalgroup;
	private ScrollPane scrollpane;

	public JoinRoot() {
		super();
		// will lead to a refresh
		GAMESERVER_LIST.refreshed = true;

	}

	@Override
	public void update(float delta) {

		// handle disconnection if an error arises
		if (LogicTier.ERROR_MESSAGE == ErrorMessage.incorrectPassword
				|| LogicTier.ERROR_MESSAGE == ErrorMessage.serverFull)
			client_gameserver_connection.disconnect();

		super.update(delta);

		refreshGameserverList();

		if (CONNECT_BY_DOUBLECLICK == true) {
			CONNECT_BY_DOUBLECLICK = false;
			GUIUtil.clickActor(join_button);
		}

		if (client_gameserver_connection.getClient().isConnected() == true) 
			sendPlayerJoinRequestToGameserver(new String(password_module.getPassword()), false);

		if (ClientGameserverProtocol.isJoinSuccessful() == true) {
			gui_tier.changeRootModule(new MaploadingRoot(ClientGameserverProtocol.MAP_SEED, true));
		} else {
			// client try to connect
		}
	}

	@Override
	public void loadWidgets() {

		bg_icon = new CustomIconbutton(StaticGraphic.gui_bg_join1, StaticGraphic.gui_bg_join1.getWidth(),
				StaticGraphic.gui_bg_join1.getHeight());

		join_button = new CustomTextbutton(TextData.getWord("join"), StyleContainer.button_bold_30_style,
				StaticGraphic.gui_bg_btn_height2_width5, Align.center) {
			@Override
			public boolean disabledCondition() {
				// return false;
				return SELECTED_GAMESERVER == null;
			}
		};
		join_button.setSound(ClickSoundType.HEAVY);

		join_tooltip = new TooltipTextArea(TextData.getWord("chooseAServer"), Shared.WIDTH4);

		server_label = new CustomLabel(TextData.getWord("server"), StyleContainer.label_bold_ivory1_20_style,
				Shared.WIDTH9, Shared.HEIGHT1, Align.center);

		number_player_label = new CustomLabel(TextData.getWord("player"), StyleContainer.label_bold_ivory1_20_style,
				Shared.WIDTH3, Shared.HEIGHT1, Align.center);

		ping_label = new CustomLabel(TextData.getWord("ping"), StyleContainer.label_bold_ivory1_20_style, Shared.WIDTH3,
				Shared.HEIGHT1, Align.center);

		direct_connect_label = new CustomLabel(TextData.getWord("directConnect"),
				StyleContainer.label_bold_ivory1_20_style, Shared.WIDTH5, Shared.HEIGHT1, Align.center);

		refresh_button = new CustomIconbutton(StaticGraphic.gui_icon_refresh, StaticGraphic.gui_icon_refreshstop,
				StaticGraphic.gui_bg_btn_height2_width5);
		refresh_button.setSound(ClickSoundType.HEAVY);

		gameserver_verticalgroup = new VerticalGroup();
		gameserver_verticalgroup.setFillParent(false);

		scrollpane = new ScrollPane(gameserver_verticalgroup);

		scrollpane.setOverscroll(true, true);
		scrollpane.setFadeScrollBars(false);
		scrollpane.setFillParent(false);

		stage.setScrollFocus(scrollpane);

		direct_connect_tooltip = new TooltipTextArea(TextData.getWord("directConnectDetail"), Shared.WIDTH5);

		direct_connect_question_mark = new CustomIconbutton(StaticGraphic.gui_icon_help,
				StaticGraphic.gui_icon_help.getWidth(), StaticGraphic.gui_icon_help.getHeight());
	}

	@Override
	public void setLayout() {

		// create image table
		CustomTable tableImages = new CustomTable();
		tableImages.setFillParent(true);
		tables.add(tableImages);

		tableImages.add(bg_icon).padTop(45 * 2);

		CustomTable table = new CustomTable();
		table.setFillParent(true);
		tables.add(table);

		table.row().padTop(35);
		table.add(server_label);
		table.add(number_player_label);
		table.add(ping_label);
		table.add(/* direct_connect_label */).width(direct_connect_label.getWidth());
		// .padLeft(480 * 2).padTop(30);
		table.row();
		table.add(scrollpane).colspan(3).width(960).height(680);
		VerticalGroup vertGroup = new VerticalGroup();
		vertGroup.addActor(refresh_button);
		vertGroup.addActor(join_button);
		table.add(vertGroup).padTop(490);

		join_tooltip.setPosition(tables, table, join_button);

//		CustomTable tableTooltip = new CustomTable();
//		tableTooltip.padBottom(645).padLeft(1150).setFillParent(true);
//		tables.add(tableTooltip);
//
//		tableTooltip.add(direct_connect_question_mark);
//		
//		direct_connect_tooltip.setPosition(tables, tableTooltip, direct_connect_question_mark);
	}

	@Override
	public void setListeners() {

		join_button.addListener(new DisabledHandledListener(join_button) {

			@Override
			public void afterDisabledCheck() {

				if (SELECTED_GAMESERVER != null) {
					if (SELECTED_GAMESERVER.has_password == true) {

						password_module.setVisibleGlobal(true);

						return;
					}
				}

				connectToSelectedGameserver();
			}
		});

		join_button.addListener(new TooltipListener(join_tooltip) {
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				if (join_button.getState() == State.DISABLED) {
					super.enter(event, x, y, pointer, fromActor);
				}
			}
		});

		server_label.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
			}
		});

		refresh_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				client_mainserver_connection.getClient().sendTCP(new ServerListRequest());
			}
		});

//		direct_connect_question_mark.addListener(new TooltipListener(direct_connect_tooltip));

	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(join_button);
		widgets.add(refresh_button);
//		widgets.add(direct_connect_question_mark);

		return new HoverHandler(widgets);
	}

	@Override
	public void loadSubModules() {
		super.loadSubModules();
//		direct_connect_module = new DirectConnectModule();
		password_module = new APasswordModule() {

			@Override
			public void clickedYes() {
				connectToSelectedGameserver();
			}
		};
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		super.addSubModules(subModules);
//		subModules.add(direct_connect_module);
		subModules.add(password_module);
	}

	@Override
	protected void addTabs(ArrayList<AModule> tabs) {
	}

	@Override
	protected boolean clickedEscape() {
		return true;
	}

	@Override
	public void unfocus() {
		super.unfocus();

		if (join_button.isHovered() == true)
			return;

		if (password_module.isVisible() == true)
			return;

		for (Actor actor : gameserver_verticalgroup.getChildren()) {
			if (actor instanceof GameserverContainer) {
				GameserverContainer container = (GameserverContainer) actor;

				if (container.isHovered() == false) {

					container.setState(State.NOT_SELECTED);

					if (container.getMDGS() != null && SELECTED_GAMESERVER != null)

						synchronized (SELECTED_GAMESERVER) {
							if (SELECTED_GAMESERVER == container.getMDGS())
								SELECTED_GAMESERVER = null;
						}

				}

			}
		}

//		SELECTED_GAMESERVER = null;

	}

	public static void connectToSelectedGameserver() {

		if (SELECTED_GAMESERVER != null)
			if (SELECTED_GAMESERVER.ip != null)
				ClientGameserverConnection.getInstance().connect(SELECTED_GAMESERVER.ip,
						SELECTED_GAMESERVER.port_tcp_game, SELECTED_GAMESERVER.port_udp_game);

	}

	private void refreshGameserverList() {
		synchronized (GAMESERVER_LIST) {

			if (GAMESERVER_LIST.refreshed == true) {

				/*
				 * The current game server list might be out-dated, so the hover handler have
				 * old game servers and must be deleted. Afterwards it has to be overridden with
				 * the perpetual widgets from the beginning (server_button,
				 * number_player_button,...) to prepare it for incoming game server rows inside
				 * the vertical group.
				 */
				HoverHandler.remove(hover_handler);
				/*
				 * Re-fill hover handler with perpetual widgets.
				 */
				hover_handler = createHoverHandler(new ArrayList<StatePossessable>());

				gameserver_verticalgroup.clear();

				if (GAMESERVER_LIST != null) {
					for (int i = 0; i < GAMESERVER_LIST.size(); i++) {

						new GameserverContainer(hover_handler, GAMESERVER_LIST.get(i), gameserver_verticalgroup);

						// new PingClientConnection(hover_handler,
						// GAMESERVER_LIST.get(i),
						// gameserver_verticalgroup);

					}
				}
				GAMESERVER_LIST.refreshed = false;
			}
		}
	}

}
