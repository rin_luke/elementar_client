package com.elementar.gui.modules.roots;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.gui.abstracts.ARootMenuModule;
import com.elementar.gui.widgets.CustomImage;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.util.Shared;

public class CreditsRoot extends ARootMenuModule {

	private Image bounding_image;
	private CustomImage image_logo;
	private CustomLabel label_prog, label_art, label_comm, label_sound, label_music, label_spec_thanks;
	private CustomLabel label_prog_content, label_art_content, label_comm_content, label_sound_content,
			label_music_content, label_spec_thanks_content;

	public CreditsRoot() {
		super();
	}

	@Override
	public void loadWidgets() {

		bounding_image = new Image(StaticGraphic.getDrawable("gui_bg_screencover"), Scaling.fill);

		image_logo = new CustomImage(StaticGraphic.gui_logo_stonearch);

		// blue label
		label_prog = new CustomLabel(TextData.getWord("programmingCredits"), StyleContainer.label_bold_blue3_25_style,
				Shared.WIDTH4, Shared.HEIGHT1, Align.left);
		label_art = new CustomLabel(TextData.getWord("artCredits"), StyleContainer.label_bold_blue3_25_style,
				Shared.WIDTH4, Shared.HEIGHT1, Align.left);
		label_comm = new CustomLabel(TextData.getWord("commCredits"), StyleContainer.label_bold_blue3_25_style,
				Shared.WIDTH4, Shared.HEIGHT1, Align.left);
		label_sound = new CustomLabel(TextData.getWord("soundCredits"), StyleContainer.label_bold_blue3_25_style,
				Shared.WIDTH4, Shared.HEIGHT1, Align.left);
		label_music = new CustomLabel(TextData.getWord("musicCredits"), StyleContainer.label_bold_blue3_25_style,
				Shared.WIDTH4, Shared.HEIGHT1, Align.left);
		label_spec_thanks = new CustomLabel(TextData.getWord("specThanksCredits"),
				StyleContainer.label_bold_blue3_25_style, Shared.WIDTH3, Shared.HEIGHT1, Align.center);

		// strings
		String stringProg = "Lukas Songajlo";
		String stringArt = "Julian Bauer";
		String stringComm = "Aleks Kovacevic";
		String stringSound = "Rupert Jaud\nJulian Bauer";
		String stringMusic = "Robert Pachaly";
		String stringSpecThanks = "A special thanks goes to Fabian Karolat, Marco Skorzinski, \nMirko Riesterer, Joanna Ryba and our families.";

		// content ivory label
		label_prog_content = new CustomLabel(stringProg, StyleContainer.label_bold_ivory1_20_style, Shared.WIDTH4,
				Shared.HEIGHT1, Align.left);
		label_art_content = new CustomLabel(stringArt, StyleContainer.label_bold_ivory1_20_style, Shared.WIDTH4,
				Shared.HEIGHT1, Align.left);
		label_comm_content = new CustomLabel(stringComm, StyleContainer.label_bold_ivory1_20_style, Shared.WIDTH4,
				Shared.HEIGHT1, Align.left);
		label_sound_content = new CustomLabel(stringSound, StyleContainer.label_bold_ivory1_20_style, Shared.WIDTH4,
				Shared.HEIGHT1, Align.left);
		label_music_content = new CustomLabel(stringMusic, StyleContainer.label_bold_ivory1_20_style, Shared.WIDTH4,
				Shared.HEIGHT1, Align.left);
		label_spec_thanks_content = new CustomLabel(stringSpecThanks, StyleContainer.label_bold_ivory1_20_style,
				Shared.WIDTH4, Shared.HEIGHT1, Align.center);
	}

	@Override
	public void setLayout() {

		float padBotFirstRow = 200, padTopSecRow = 100;
		float distBetween = 700;

		// background filling
		CustomTable tableBounding1 = new CustomTable();
		tableBounding1.setFillParent(true);
		tables.add(tableBounding1);

		tableBounding1.add(bounding_image).expand().fill();

		// stone arch logo
		CustomTable tableLogo = new CustomTable();
		tableLogo.padBottom(padBotFirstRow + 300).setFillParent(true);
		tables.add(tableLogo);

		tableLogo.add(image_logo).width(image_logo.getSprite().getWidth());

		// prog
		CustomTable tableProg = new CustomTable();
		tableProg.padBottom(padBotFirstRow).padRight(distBetween).setFillParent(true);
		tables.add(tableProg);

		tableProg.add(label_prog);
		tableProg.row();
		tableProg.add(label_prog_content);

		// art
		CustomTable tableArt = new CustomTable();
		tableArt.padBottom(padBotFirstRow).setFillParent(true);
		tables.add(tableArt);

		tableArt.add(label_art);
		tableArt.row();
		tableArt.add(label_art_content);

		// comm
		CustomTable tableComm = new CustomTable();
		tableComm.padBottom(padBotFirstRow).padLeft(distBetween).setFillParent(true);
		tables.add(tableComm);

		tableComm.add(label_comm);
		tableComm.row();
		tableComm.add(label_comm_content);

		// sound
		CustomTable tableSound = new CustomTable();
		tableSound.padRight(distBetween).padTop(padTopSecRow).setFillParent(true);
		tables.add(tableSound);

		tableSound.add(label_sound).padTop(10);
		tableSound.row().padTop(10);
		tableSound.add(label_sound_content);

		// music
		CustomTable tableMusic = new CustomTable();
		tableMusic.padTop(padTopSecRow).setFillParent(true);
		tables.add(tableMusic);

		tableMusic.add(label_music);
		tableMusic.row();
		tableMusic.add(label_music_content);

		// special thanks
		CustomTable tableSpecThanks = new CustomTable();
		tableSpecThanks.padTop(550).setFillParent(true);
		tables.add(tableSpecThanks);

		tableSpecThanks.add(label_spec_thanks);
		tableSpecThanks.row();
		tableSpecThanks.add(label_spec_thanks_content);

	}

	@Override
	public void setListeners() {

	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return null;
	}

	@Override
	protected boolean clickedEscape() {
		gui_tier.changeRootModule(new HomeRoot());
		return true;
	}

}
