package com.elementar.gui.modules.roots;

import java.util.ArrayList;

import com.elementar.data.container.StaticGraphic;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.ARootMenuModule;
import com.elementar.gui.modules.host.GameserverConfigModule;
import com.elementar.gui.modules.host.GameserverMonitorModule;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.network.protocols.ClientGameserverProtocol;

public class HostRoot extends ARootMenuModule {

	private GameserverConfigModule config_module;
	private GameserverMonitorModule monitor_module;

	// common widget for config and monitor module
	private CustomIconbutton bg_icon;

	public HostRoot() {
		super();
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		if (logic_tier.getGameserverLogic() != null) {

			/*
			 * after the game server is created and could join successfully the main server,
			 * the host client tries now to connect to the game server
			 */
			if (logic_tier.getGameserverLogic().getGameserverMainserverConnection().getProtocol()
					.isJoinSuccessfulMainserver() == true) {

				// switch to monitor module
				if (monitor_module.isVisible() == false)
					changeTab(monitor_module);

			}
		}
		// switch to config module
		else if (config_module.isVisible() == false)
			changeTab(config_module);

		// host has clicked on the join button...
		if (client_gameserver_connection.getClient().isConnected() == true)
			// send a join request as host
			sendPlayerJoinRequestToGameserver(config_module.getPassword(), true);

		if (ClientGameserverProtocol.isJoinSuccessful() == true) {

			// hide sub navigation bar
			ARootMenuModule.MAINMENU_NAVIGATION_MODULE.getSubNavigationModule().setVisibleGlobal(false);

			gui_tier.changeRootModule(new MaploadingRoot(ClientGameserverProtocol.MAP_SEED, true));
		} else {
			// client try to connect
		}

	}

	@Override
	public void loadWidgets() {
		bg_icon = new CustomIconbutton(StaticGraphic.gui_bg_host1, StaticGraphic.gui_bg_host1.getWidth(),
				StaticGraphic.gui_bg_host1.getHeight());
	}

	@Override
	public void setLayout() {
		CustomTable tableImage = new CustomTable();
		tableImage.setFillParent(true);
		tables.add(tableImage);
		tableImage.add(bg_icon).padTop(150);
	}

	@Override
	public void setListeners() {

	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return new HoverHandler(widgets);
	}

	@Override
	public void loadSubModules() {
		super.loadSubModules();

		boolean isGameserverActive = logic_tier.getGameserverLogic() != null;

		config_module = new GameserverConfigModule(!isGameserverActive);
		monitor_module = new GameserverMonitorModule(isGameserverActive);
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		super.addSubModules(subModules);
		subModules.add(config_module);
		subModules.add(monitor_module);
	}

	@Override
	protected void addTabs(ArrayList<AModule> tabs) {
		super.addTabs(tabs);
		tabs.add(config_module);
		tabs.add(monitor_module);
	}

	@Override
	protected boolean clickedEscape() {
		return true;
	}
}
