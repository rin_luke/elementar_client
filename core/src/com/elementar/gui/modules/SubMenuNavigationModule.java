package com.elementar.gui.modules;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.modules.roots.HostRoot;
import com.elementar.gui.modules.roots.JoinRoot;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.TooltipTextArea;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.listener.DisabledHandledListener;
import com.elementar.gui.widgets.listener.TooltipListener;
import com.elementar.logic.util.Shared;

public class SubMenuNavigationModule extends AChildModule {

	private CustomTextbutton search_game_button, host_server_button, tutorial_button;

	private CustomIconbutton myserver_connection_status_icon;

	private TooltipTextArea tutorial_tooltip_label;

	public SubMenuNavigationModule() {
		super(false);
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		myserver_connection_status_icon
				.setIcon(logic_tier.getGameserverLogic() != null ? StaticGraphic.gui_icon_server_connected
						: StaticGraphic.gui_icon_server_inactive);

	}

	@Override
	public void loadWidgets() {
		search_game_button = new CustomTextbutton(TextData.getWord("searchGame"), StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		search_game_button.setSound(ClickSoundType.LIGHT);
		search_game_button.onewayClicklistener(true);

		host_server_button = new CustomTextbutton(TextData.getWord("hostServer"), StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		host_server_button.setSound(ClickSoundType.LIGHT);
		host_server_button.onewayClicklistener(true);

		tutorial_button = new CustomTextbutton(TextData.getWord("tutorial"), StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		tutorial_button.setSound(ClickSoundType.LIGHT);
		tutorial_button.onewayClicklistener(true);

		tutorial_tooltip_label = new TooltipTextArea(TextData.getWord("learnTheMainMechanics"), Shared.WIDTH5);

		myserver_connection_status_icon = new CustomIconbutton(StaticGraphic.gui_icon_server_inactive,
				StaticGraphic.gui_icon_server_inactive.getWidth(), StaticGraphic.gui_icon_server_inactive.getHeight());
	}

	@Override
	public void setLayout() {

		CustomTable table = new CustomTable();
		table.setFillParent(true);
		table.top().padTop(93);
		tables.add(table);

		table.add(search_game_button);
		table.add(host_server_button);
		table.add(tutorial_button);

		// connection status
		CustomTable tableConnectionStatus = new CustomTable();
		tableConnectionStatus.top().padTop(105).padRight(170).setFillParent(true);
		tables.add(tableConnectionStatus);

		tableConnectionStatus.add(myserver_connection_status_icon);

		tutorial_tooltip_label.setPosition(tables, table, tutorial_button);

	}

	@Override
	public void setListeners() {

		search_game_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				gui_tier.changeRootModule(new JoinRoot());
			}
		});
		host_server_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				gui_tier.changeRootModule(new HostRoot());
			}
		});
		tutorial_button.addListener(new DisabledHandledListener(tutorial_button) {

			@Override
			public void afterDisabledCheck() {
			}
		});
		tutorial_button.addListener(new TooltipListener(tutorial_tooltip_label));

		myserver_connection_status_icon.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				GUIUtil.clickActor(host_server_button);
			}
		});

	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		// priority is important in this case!
		widgets.add(host_server_button);
		widgets.add(tutorial_button);
		widgets.add(search_game_button);
		return new HoverHandler(widgets);
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {

	}

	public CustomTextbutton getSearchGameButton() {
		return search_game_button;
	}

	public CustomTextbutton getHostServerButton() {
		return host_server_button;
	}

}
