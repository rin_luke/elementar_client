package com.elementar.gui.modules;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AChildCoverModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.util.Shared;

public abstract class AYesNoConfirmationModule extends AChildCoverModule {

	private CustomLabel question_label;
	private CustomTextbutton yes_button, cancel_button;

	public AYesNoConfirmationModule(String text, int drawOrder) {
		super(StaticGraphic.gui_bg_confirmation, drawOrder, true);

		question_label.setText(text);

	}

	public abstract void clickedYes();

	@Override
	public void loadWidgets() {
		super.loadWidgets();
		yes_button = new CustomTextbutton(TextData.getWord("yes"), StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		yes_button.setSound(ClickSoundType.LIGHT);

		cancel_button = new CustomTextbutton(TextData.getWord("cancel"), StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		cancel_button.setSound(ClickSoundType.LIGHT);

		question_label = new CustomLabel("", StyleContainer.label_medium_whitepure_20_style,
				background_sprite.getWidth(), Shared.HEIGHT1, Align.center);
	}

	@Override
	public void setLayout() {
		super.setLayout();

		table_clicksafe.padBottom(200);

		CustomTable table = new CustomTable();
		table.padBottom(100).setFillParent(true);
		tables.add(table);

		table.add(question_label).colspan(2);
		table.row().padTop(30).padBottom(20);
		table.add(yes_button).padLeft(50);
		table.add(cancel_button).padRight(50);

	}

	@Override
	public void setListeners() {
		super.setListeners();

		cancel_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				setVisibleGlobal(false);
				/*
				 * the exit method of the HoverListener isn't called after clicking this button,
				 * so we have to set its state manually
				 */
				cancel_button.setHovered(false);
			}
		});

		yes_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				clickedYes();
				setVisibleGlobal(false);
				/*
				 * the exit method of the HoverListener isn't called after clicking this button,
				 * so we have to set its state manually
				 */
				yes_button.setHovered(false);
			}
		});
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(yes_button);
		widgets.add(cancel_button);
		return new HoverHandler(widgets);
	}

	@Override
	protected boolean clickedEscape() {
		GUIUtil.clickActor(cancel_button);
		return true;
	}

}
