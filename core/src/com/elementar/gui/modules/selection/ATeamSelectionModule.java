package com.elementar.gui.modules.selection;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.network.protocols.ClientGameserverProtocol;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.Team;

public abstract class ATeamSelectionModule extends AChildModule {

	private CustomIconbutton team1_select_button, team2_select_button;

	private CustomLabel team1_label, team2_label, choose_your_team_label, team1_amount_label,
			team2_amount_label;

	private Team selected_team;

	public ATeamSelectionModule(boolean visible) {
		super(visible);
	}

	public void updateLabels() {

		int countTeam1 = 0, countTeam2 = 0;
		for (OmniClient client : LogicTier.META_PLAYER_MAP.values()) {
			if (client.getMetaClient().team == Team.TEAM_1)
				countTeam1++;
			if (client.getMetaClient().team == Team.TEAM_2)
				countTeam2++;
		}
		int teamCapacity = ClientGameserverProtocol.GAMESERVER_CAPACITY / 2;
		team1_amount_label.setText(countTeam1 + "/" + teamCapacity);
		team2_amount_label.setText(countTeam2 + "/" + teamCapacity);

		if (countTeam1 >= teamCapacity)
			team1_select_button.setState(State.DISABLED);
		else if (team1_select_button.getState() == State.DISABLED)
			team1_select_button.setState(State.NOT_SELECTED);

		if (countTeam2 >= teamCapacity)
			team2_select_button.setState(State.DISABLED);
		else if (team2_select_button.getState() == State.DISABLED)
			team2_select_button.setState(State.NOT_SELECTED);

	}

	@Override
	public void loadWidgets() {
		float width = 600;
		float height = 800;
		team1_select_button = new CustomIconbutton(StaticGraphic.gui_emblem_team1, width, height);
		team2_select_button = new CustomIconbutton(StaticGraphic.gui_emblem_team2, width, height);

		choose_your_team_label = new CustomLabel(TextData.getWord("chooseYourTeam"),
				StyleContainer.label_bold_whitepure_40_style, width, Shared.HEIGHT1, Align.center);

		team1_amount_label
				= new CustomLabel("1/3", StyleContainer.label_bold_ivory2_40_bordered_blue7_style,
						width, Shared.HEIGHT1, Align.center);

		team2_amount_label
				= new CustomLabel("2/3", StyleContainer.label_bold_ivory2_40_bordered_blue7_style,
						width, Shared.HEIGHT1, Align.center);

		team1_label = new CustomLabel(TextData.getWord("balance"),
				StyleContainer.label_bold_ivory2_50_bordered_blue7_style, width, Shared.HEIGHT1,
				Align.center);

		team2_label = new CustomLabel(TextData.getWord("chaos"),
				StyleContainer.label_bold_ivory2_50_bordered_blue7_style, width, Shared.HEIGHT1,
				Align.center);

	}

	@Override
	public void setLayout() {

		float padding = 200;

		// labels
		CustomTable tableTeamLabels = new CustomTable();
		tableTeamLabels.bottom().padBottom(140).setFillParent(true);
		tables.add(tableTeamLabels);

		tableTeamLabels.add(team1_label).padRight(padding);
		tableTeamLabels.add(team2_label);
		tableTeamLabels.row().padTop(20);
		tableTeamLabels.add(team1_amount_label).padRight(padding);
		tableTeamLabels.add(team2_amount_label);

		// buttons
		CustomTable tableTeamSelectBtns = new CustomTable();
		tableTeamSelectBtns.setFillParent(true);
		tables.add(tableTeamSelectBtns);

		tableTeamSelectBtns.add(team1_select_button).padRight(padding);
		tableTeamSelectBtns.add(team2_select_button);

		// chooseYourTeam
		CustomTable tableChooseTitle = new CustomTable();
		tableChooseTitle.padBottom(600).setFillParent(true);
		tables.add(tableChooseTitle);

		tableChooseTitle.add(choose_your_team_label);

	}

	@Override
	public void setListeners() {

		team1_select_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				selected_team = Team.TEAM_1;
				afterTeamSelection();
			}
		});

		team2_select_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				selected_team = Team.TEAM_2;
				afterTeamSelection();
			}
		});

	}

	public abstract void afterTeamSelection();

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {

	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(team1_select_button);
		widgets.add(team2_select_button);
		return new HoverHandler(widgets);
	}

	public Team getSelectedTeam() {
		return selected_team;
	}

}
