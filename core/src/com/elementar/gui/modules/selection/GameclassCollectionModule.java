package com.elementar.gui.modules.selection;

import java.util.ArrayList;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.GameclassContainer;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.data.container.util.Gameclass;
import com.elementar.data.container.util.GameclassSkin;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.data.container.util.Gameclass.Role;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AGameclassSelectionModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.modules.roots.SpiritsRoot;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.GameclassIconbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.util.Shared;

public class GameclassCollectionModule extends AChildModule {

	public static DrawableClient CHARACTER_MODEL;
	public static boolean SELECTED_IS_CROSSED_OUT;

	private static int MAX_GAMECLASSES_PER_LINE = 4;
	public static ArrayList<GameclassIconbutton> GAMECLASS_BUTTONS;

	private boolean has_buy_section;

	private CustomLabel fighter_label, tank_label, support_label;

	// role filter
	private ArrayList<CustomIconbutton> rolefilter_buttons;
	private ArrayList<Gameclass> filtered_gameclasses;

	private CustomIconbutton rolefilter_bg_image;

	static {
		// flip = false
		CHARACTER_MODEL = new DrawableClient(
				new MetaClient(GameclassContainer.ALL_GAMECLASSES.get(0).getNameAsEnum(), 0),
				AnimationContainer.makeCharacterAnimation(0.5f), false);

		GAMECLASS_BUTTONS = new ArrayList<>();

		// add buttons to structure
		for (Gameclass gameclass : GameclassContainer.ALL_GAMECLASSES)
			GAMECLASS_BUTTONS.add(new GameclassIconbutton(gameclass, CHARACTER_MODEL, true));

	}

	public static void invoke() {

	}

	public static void resetCrossedOut() {
		SELECTED_IS_CROSSED_OUT = false;
		for (GameclassIconbutton iconButton : GAMECLASS_BUTTONS)
			iconButton.setCrossedOut(false);
	}

	public static void setCrossedOut(GameclassName selectedGameclass) {
		for (GameclassIconbutton iconButton : GAMECLASS_BUTTONS)
			if (iconButton.getGameClass().getNameAsEnum() == selectedGameclass) {
				iconButton.setCrossedOut(true);
				if (CHARACTER_MODEL != null
						&& CHARACTER_MODEL.getGameclass().getNameAsEnum() == iconButton.getGameClass().getNameAsEnum())
					SELECTED_IS_CROSSED_OUT = true;

			}
	}

	public GameclassCollectionModule(boolean visible, int drawOrder, boolean hasBuySection) {
		super(visible, drawOrder);
		has_buy_section = hasBuySection;
		super.show();

		/*
		 * before this line is called we set the static fields of the SpiritsRoot.
		 * 
		 */
		GameclassIconbutton iconButton = GAMECLASS_BUTTONS.get(SpiritsRoot.INDEX_GAMECLASS);
		GUIUtil.clickActor(iconButton);
		iconButton.clickShowButton(SpiritsRoot.INDEX_SKIN);

		// set sound after first click
		for (GameclassIconbutton button : GAMECLASS_BUTTONS) {
			button.setCrossedOut(false);
			button.setSound(ClickSoundType.LIGHT);
		}
	}

	@Override
	public void show() {

	}

	@Override
	public void loadWidgets() {
		// widgets will handle as static variables to avoid loading time

		// rolefilter_buttons = new ArrayList<>();
		//
		// for (Role role : Role.values()) {
		// if (role != Role.ALL) {
		//
		// CustomIconbutton roleButton
		// = new CustomIconbutton(TextData.getWord(role.name().toLowerCase()),
		// StyleContainer.button_rolefilter_style, Align.center,
		// getRoleSprite(role), 100, 100);
		// // extra alignment code
		// roleButton.getLabel().setAlignment(Align.bottom, Align.center);
		// roleButton.setSound(ClickSoundType.LIGHT);
		// roleButton.twowayClicklistener(true);
		//
		// rolefilter_buttons.add(roleButton);
		// }
		// }

		// rolefilter_bg_image = new
		// CustomIconbutton(StaticGraphic.gui_bg_rolefilter,
		// StaticGraphic.gui_bg_rolefilter.getWidth(),
		// StaticGraphic.gui_bg_rolefilter.getHeight());

//		rolefilter_bg_image.setColor(SharedColors.IVORY_2, SharedColors.IVORY_2_DIMMED, SharedColors.IVORY_2_DISABLED);

		fighter_label = new CustomLabel(TextData.getWord("fighter"), StyleContainer.label_medium_whitepure_20_style,
				Shared.WIDTH4, Shared.HEIGHT1, Align.left);
		tank_label = new CustomLabel(TextData.getWord("tank"), StyleContainer.label_medium_whitepure_20_style, Shared.WIDTH4,
				Shared.HEIGHT1, Align.left);
		support_label = new CustomLabel(TextData.getWord("support"), StyleContainer.label_medium_whitepure_20_style,
				Shared.WIDTH4, Shared.HEIGHT1, Align.left);

	}

	private Sprite getRoleSprite(Role role) {
		switch (role) {
		case FIGHTER:
			return StaticGraphic.gui_icon_fighter;
		case TANK:
			return StaticGraphic.gui_icon_tank;
		case SUPPORT:
			return StaticGraphic.gui_icon_support;
		}

		return null;
	}

	@Override
	public void setLayout() {
		
		float cellWidth = GAMECLASS_BUTTONS.get(0).getWidth();
		float cellHeight = GAMECLASS_BUTTONS.get(0).getHeight();

		CustomTable table = new CustomTable();
		table.top().setFillParent(true);
		table.padRight(1100).padTop(AGameclassSelectionModule.PAD_TOP_LEFT_SIDE);
		tables.add(table);

		for (Role role : Role.values()) {

			CustomLabel label = null;
			switch (role) {
			case ALL:
				continue;
			case FIGHTER:
				label = fighter_label;
				break;
			case TANK:
				label = tank_label;
				break;
			case SUPPORT:
				label = support_label;
				break;
			}

			// add header
			table.add(label).width(cellWidth).height(cellHeight * 0.5f);
			table.row();
			// filter
			ArrayList<GameclassIconbutton> filteredButtons = filter(GameclassContainer.filterByRole(role));
			// add filteredButtons
			
			addToTable(table, filteredButtons);

			table.row();
			table.add().height(cellHeight*0.5f);
			table.row();

		}

//		for(GameclassIconbutton button: filtered_gameclass_buttons) {
//			button.addToTables(tables);
//			table_collection.add()
//		}
//		
//		
//		
//
//		CustomTable tableTank = new CustomTable();
//		tableTank.top().setFillParent(true);
//		tableTank.padRight(1100).padTop(AGameclassSelectionModule.PAD_TOP_LEFT_SIDE);
//		tables.add(tableTank);
//
//		CustomTable tableSupport = new CustomTable();
//		tableSupport.top().setFillParent(true);
//		tableSupport.padRight(1100).padTop(AGameclassSelectionModule.PAD_TOP_LEFT_SIDE);
//		tables.add(tableSupport);

		// role filter background image
		// CustomTable tableRolefilterBG = new CustomTable();
		// tableRolefilterBG.top().setFillParent(true);
		// tableRolefilterBG.padRight(1100);
		// tableRolefilterBG.padTop(AGameclassSelectionModule.PAD_TOP_LEFT_SIDE);
		// tables.add(tableRolefilterBG);
		//
		// tableRolefilterBG.add(rolefilter_bg_image);
		//
		// // role filter buttons
		// CustomTable tableRolefilterButtons = new CustomTable();
		// tableRolefilterButtons.top().setFillParent(true);
		// tableRolefilterButtons.padRight(1100);
		// tableRolefilterButtons.padTop(AGameclassSelectionModule.PAD_TOP_LEFT_SIDE
		// - 10);
		// tables.add(tableRolefilterButtons);
		//
		// for (CustomIconbutton roleIconButton : rolefilter_buttons)
		// tableRolefilterButtons.add(roleIconButton).padRight(10).padLeft(10);

	}

	@Override
	public void setListeners() {
		for (final GameclassIconbutton gameclassButton : GAMECLASS_BUTTONS)
			gameclassButton.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {

					for (GameclassIconbutton deactivatedButton : GAMECLASS_BUTTONS)
						deactivatedButton.setVisibilityWidgets(false);
					gameclassButton.setVisibilityWidgets(true);

					// find equipped skin and click button
					for (GameclassSkin skin : gameclassButton.getSkins()) {
						if (skin.isEquipped() == true) {
							gameclassButton.clickShowButton(skin.getIndex());
							break;
						}
					}
				}
			});

		// role filter button listener
		// for (int i = 0; i < rolefilter_buttons.size(); i++) {
		// final int index = i;
		// rolefilter_buttons.get(i).addListener(new ClickListener() {
		// @Override
		// public void clicked(InputEvent event, float x, float y) {
		//
		// if (rolefilter_buttons.get(index).getState() == State.NOT_SELECTED)
		// filtered_gameclasses = GameclassContainer.filterByRole(Role.ALL);
		// else {
		// // state == SELECTED
		// filtered_gameclasses = GameclassContainer.filterByRole(Role.valueOf(
		// rolefilter_buttons.get(index).getText().toString().toUpperCase()));
		//
		// // set other buttons to NOT_SELECTED
		// for (CustomIconbutton button : rolefilter_buttons)
		// if (button != rolefilter_buttons.get(index))
		// button.setState(State.NOT_SELECTED);
		//
		// }
		//
		// filter(filtered_gameclasses);
		//
		// }
		// });
		//
		// }

	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.addAll(GAMECLASS_BUTTONS);
		// widgets.addAll(rolefilter_buttons);
		return new HoverHandler(widgets, GAMECLASS_BUTTONS.get(0));
	}

	@Override
	public void loadSubModules() {

	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {

	}

	public ArrayList<GameclassIconbutton> filter(ArrayList<Gameclass> filteredGameclasses) {

		ArrayList<GameclassIconbutton> filteredButtons = new ArrayList<>();

		// filter
		boolean insideFilter;
		int index = 0;
		for (GameclassIconbutton gameclassButton : GAMECLASS_BUTTONS) {
			insideFilter = filteredGameclasses.contains(gameclassButton.getGameClass());
			if (insideFilter == true) {
				gameclassButton.setIndex(index);
				filteredButtons.add(gameclassButton);
				index++;
			}
		}

		return filteredButtons;

	}

	private void addToTable(CustomTable table, ArrayList<GameclassIconbutton> filteredButtons) {

		int spaceBetweenCells = 2;
		int nextLine = MAX_GAMECLASSES_PER_LINE;
		int counter = 0;

		for (GameclassIconbutton button : filteredButtons) {

			table.add(button).spaceRight(spaceBetweenCells).padTop(spaceBetweenCells);

			counter++;

			// new row by adding horizontalgroup
			if (counter == nextLine) {

				counter = 0;

				table.row();

			}

			button.addToTables(tables);
		}

	}

}
