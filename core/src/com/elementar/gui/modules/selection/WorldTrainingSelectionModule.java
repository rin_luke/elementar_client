package com.elementar.gui.modules.selection;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Scaling;
import com.elementar.data.container.StaticGraphic;
import com.elementar.gui.abstracts.AGameclassSelectionModule;
import com.elementar.gui.abstracts.ARootWorldModule;
import com.elementar.gui.modules.ingame.APlayerUIModule;
import com.elementar.gui.modules.roots.SpiritsRoot;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.listener.DisabledHandledListener;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.OmniClient;

/**
 * The player can choose a new character inside the world without switching to
 * the {@link SpiritsRoot}.
 * 
 * @author lukassongajlo
 *
 */
public class WorldTrainingSelectionModule extends AGameclassSelectionModule {

	/**
	 * provides access to the gameclass-switch button, so the tab can switch
	 * from this to {@link APlayerUIModule}
	 */
	private CustomIconbutton gameclass_switch_button;

	/**
	 * covers the whole screen
	 */
	private Image bounding_image;

	private ARootWorldModule world_module;

	public WorldTrainingSelectionModule(int drawOrder, CustomIconbutton gameclassSwitchButton,
			ARootWorldModule worldModule) {
		super(false, drawOrder, false, true, true);
		world_module = worldModule;
		gameclass_switch_button = gameclassSwitchButton;
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		/*
		 * check whether game class is already chosen
		 */
		GameclassCollectionModule.resetCrossedOut();
		for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()){
			GameclassCollectionModule.setCrossedOut(client.getMetaClient().gameclass_name);
		}
		
		if (isVisible() == true)
			ARootWorldModule.CURSOR_UI_CHECK.setAtUIModules(true);
	}

	@Override
	public void loadWidgets() {
		super.loadWidgets();
		bounding_image = new Image(StaticGraphic.getDrawable("gui_bg_selection_world"),
				Scaling.fill);
	}

	@Override
	public void setLayout() {
		CustomTable tableOuterBounding = new CustomTable();
		tableOuterBounding.setFillParent(true);
		tables.add(tableOuterBounding);

		tableOuterBounding.add(bounding_image).expand().fill();

		super.setLayout();

	}

	@Override
	public void setListeners() {
		super.setListeners();
	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return super.createHoverHandler(widgets);
	}

	@Override
	protected boolean clickedEscape() {
		world_module.setCreateAlly(false);
		world_module.setCreateEnemy(false);
		GUIUtil.clickActor(gameclass_switch_button);
		return true;
	}

	@Override
	public DisabledHandledListener getSelectListener(CustomTextbutton selectButton) {
		return new DisabledHandledListener(selectButton) {
			@Override
			public void afterDisabledCheck() {
				world_module.setCharacter(GameclassCollectionModule.CHARACTER_MODEL);
				clickedEscape();
			}
		};
	}

}
