package com.elementar.gui.modules;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Graphics;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.ParticleContainer;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.modules.roots.CreditsRoot;
import com.elementar.gui.modules.roots.HomeRoot;
import com.elementar.gui.modules.roots.JoinRoot;
import com.elementar.gui.modules.roots.SpiritsRoot;
import com.elementar.gui.widgets.CustomEffectWidget;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;
import com.elementar.logic.network.connection.AClientConnection.ConnectionStatus;
import com.elementar.logic.network.util.NetworkUtil;
import com.elementar.logic.util.Shared;

public class MenuNavigationModule extends AChildModule {

	private final float UPDATE_RATE_CONN_STATUS = 2f;

	private CustomTable spirits_play_table;
	private CustomTable connection_status_table;

	private CustomIconbutton button_home, button_options, button_stonearch, button_minimize, button_close;
	private CustomTextbutton button_spirits, button_play;
	private CustomIconbutton nav_bg_icon;
	private CustomEffectWidget home_btn_effect;
	private CustomLabel connection_status_label;

	private CustomIconbutton connection_status_icon;

	private SubMenuNavigationModule sub_nav_module;

	/**
	 * accu is set to RATE so we don't have to wait for the first connection attempt
	 */
	private float accumulator = UPDATE_RATE_CONN_STATUS;

	public MenuNavigationModule() {
		super(true);
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		ConnectionStatus status;

		accumulator += delta;
		while (accumulator >= UPDATE_RATE_CONN_STATUS) {
			accumulator -= UPDATE_RATE_CONN_STATUS;

			// update whether the player has internet connection
			NetworkUtil.isInternetReachable(UPDATE_RATE_CONN_STATUS);

			status = NetworkUtil.INTERNET_CONNECTION_STATUS;

			if (status == ConnectionStatus.FAILED) {
				connection_status_table.setVisible(true);
				connection_status_label.setText(TextData.getWord("offlineMode"));
				connection_status_icon.setIcon(StaticGraphic.gui_icon_offline_mode);
			} else if (status == ConnectionStatus.CONNECTED) {

				// reset status
				status = client_mainserver_connection.getConnectionStatus();

				if (status != ConnectionStatus.TRYING && status != ConnectionStatus.CONNECTED)
					client_mainserver_connection.connect();

				connection_status_table.setVisible(true);
				connection_status_label.setText(TextData.getWord("connecting"));
				connection_status_icon.setIcon(StaticGraphic.gui_icon_server_connecting);

				if (status == ConnectionStatus.CONNECTED) {
					connection_status_label.setText(TextData.getWord("connected"));
					connection_status_icon.setIcon(StaticGraphic.gui_icon_server_connected);
				}

			}

		}

	}

	@Override
	public void loadWidgets() {

		button_options = new CustomIconbutton(StaticGraphic.gui_icon_options, Shared.WIDTH3, Shared.HEIGHT2);
		button_options.setSound(ClickSoundType.HEAVY);

		button_close = new CustomIconbutton(StaticGraphic.gui_icon_close_game,
				StaticGraphic.gui_icon_close_game.getWidth(), StaticGraphic.gui_icon_close_game.getHeight());
		button_minimize = new CustomIconbutton(StaticGraphic.gui_icon_minimize_game,
				StaticGraphic.gui_icon_minimize_game.getWidth(), StaticGraphic.gui_icon_minimize_game.getHeight());
		// TODO SCHATTEN UND BUTTON AUSKOMMENTIERT
		// close_button = new CustomIconbutton(StaticGraphic.gui_icon_decline,
		// StaticGraphic.gui_bg_btn_height2_width2_1);
		button_home = new CustomIconbutton(StaticGraphic.gui_btn_home, 352, Shared.HEIGHT3);
		button_home.setIconShiftY(-20);
		button_home.setSound(ClickSoundType.HEAVY);
		button_home.onewayClicklistener(true);

		button_spirits = new CustomTextbutton(TextData.getWord("spirits"), StyleContainer.button_bold_30_style,
				Shared.WIDTH5, Shared.HEIGHT2, Align.center);
		button_spirits.setSound(ClickSoundType.HEAVY);
		button_spirits.onewayClicklistener(true);

		button_play = new CustomTextbutton(TextData.getWord("play"), StyleContainer.button_bold_30_style, Shared.WIDTH5,
				Shared.HEIGHT2, Align.center);
		button_play.setSound(ClickSoundType.HEAVY);
		button_play.onewayClicklistener(true);

		nav_bg_icon = new CustomIconbutton(StaticGraphic.gui_bg_nav1, StaticGraphic.gui_bg_nav1.getWidth(),
				StaticGraphic.gui_bg_nav1.getHeight());

		home_btn_effect = new CustomEffectWidget(ParticleContainer.particle_gui_btn_home);

		connection_status_label = new CustomLabel("", StyleContainer.label_medium_ivory3_18_style, Shared.WIDTH4,
				Shared.HEIGHT1, Align.right);

		connection_status_icon = new CustomIconbutton(StaticGraphic.gui_icon_server_connected,
				StaticGraphic.gui_icon_server_connected.getWidth(),
				StaticGraphic.gui_icon_server_connected.getHeight());

		button_stonearch = new CustomIconbutton(StaticGraphic.gui_icon_stonearch, Shared.WIDTH3, Shared.HEIGHT2) {
			@Override
			public void act(float delta) {
				super.act(delta);

				if (getState() != State.HOVERED)
					getIcon().setAlpha(Shared.WIDGET_DISABLED_ALPHA);
				else
					getIcon().setAlpha(1f);

			}
		};
		button_stonearch.onewayClicklistener(true);

	}

	@Override
	public void setLayout() {

		// create image table
		CustomTable tableImages = new CustomTable();
		tableImages.setFillParent(true);
		tableImages.top().left();
		tables.add(tableImages);
		tableImages.add(nav_bg_icon);

		// create widgets table
		CustomTable tableWidgets = new CustomTable();
		tableWidgets.setFillParent(true);
		tableWidgets.top().left();
		tables.add(tableWidgets);
		/*
		 * layout widgets, #top() method is necessary because home button increases the
		 * table height. Without #top() the smaller buttons would be placed incorrectly
		 */
		tableWidgets.add(button_options).top();
		tableWidgets.add(button_home);
		tableWidgets.add(button_minimize).expandX().right().top();
		tableWidgets.add(button_close).right().top();

		// create home btn effect table
		CustomTable tableHomeEffect = new CustomTable();
		tableHomeEffect.setFillParent(true);
		tableHomeEffect.top().left();
		tables.add(tableHomeEffect);
		tableHomeEffect.add(home_btn_effect).padLeft(320).padTop(50);

		// create widgets table #2
		spirits_play_table = new CustomTable();
		spirits_play_table.setFillParent(true);
		spirits_play_table.top();
		tables.add(spirits_play_table);
		spirits_play_table.add(button_spirits);
		spirits_play_table.add(button_play);

		// create connection status table
		connection_status_table = new CustomTable();
		connection_status_table.setFillParent(true);
		connection_status_table.top().right().padRight(200).padTop(20);
		connection_status_table.setVisible(false);
		tables.add(connection_status_table);

		connection_status_table.add(connection_status_label).padRight(20);
		connection_status_table.add(connection_status_icon);

		// create table for stonearch icon
		CustomTable tableStonearch = new CustomTable();
		tableStonearch.setFillParent(true);
		tableStonearch.top().right().padRight(400);
		tables.add(tableStonearch);
		tableStonearch.add(button_stonearch);
	}

	@Override
	public void setListeners() {

		button_home.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				sub_nav_module.setVisibleGlobal(false);
				gui_tier.changeRootModule(new HomeRoot());
			}
		});

		button_play.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				sub_nav_module.setVisibleGlobal(true);
				gui_tier.changeRootModule(new JoinRoot());
				sub_nav_module.getSearchGameButton().setState(State.SELECTED);
			}
		});

		button_spirits.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				sub_nav_module.setVisibleGlobal(false);
				gui_tier.changeRootModule(new SpiritsRoot());

			}
		});

		button_stonearch.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				sub_nav_module.setVisibleGlobal(false);
				gui_tier.changeRootModule(new CreditsRoot());

			}
		});

		button_options.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				AModule.OPTIONS_MODULE.setVisibleGlobal(true);
			}
		});

		button_close.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit();
			}
		});

		button_minimize.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				((Lwjgl3Graphics) Gdx.graphics).getWindow().iconifyWindow();
			}
		});

	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(button_options);
		widgets.add(button_close);
		widgets.add(button_minimize);
		widgets.add(button_home);
		widgets.add(button_spirits);
		widgets.add(button_play);
		widgets.add(button_stonearch);
		return new HoverHandler(widgets);
	}

	@Override
	public void loadSubModules() {
		sub_nav_module = new SubMenuNavigationModule();

	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		subModules.add(sub_nav_module);
	}

	public SubMenuNavigationModule getSubNavigationModule() {
		return sub_nav_module;
	}

	public CustomTextbutton getButtonPlay() {
		return button_play;
	}

	public CustomIconbutton getButtonHome() {
		return button_home;
	}

	public CustomTextbutton getButtonSpirits() {
		return button_spirits;
	}

}
