package com.elementar.gui.modules.host;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.modules.AYesNoConfirmationModule;
import com.elementar.gui.widgets.ConsoleWidget;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.network.gameserver.GameserverLogic;
import com.elementar.logic.network.gameserver.MetaDataGameserver;
import com.elementar.logic.util.Shared;

public class GameserverMonitorModule extends AChildModule {

	private AYesNoConfirmationModule confirmation_module;

	private ConsoleWidget console_widget;

	private CustomLabel name_label, ip_label, password_label, players_label, seed_label,
			uptime_label;

	private CustomTextbutton join_server_button;
	private CustomIconbutton close_server_button;

	private CustomIconbutton line_icon;

	private String time_text_parsed;

	public GameserverMonitorModule(boolean visible) {
		super(visible);
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		console_widget.getScrollPane().layout();

		GameserverLogic gameserverLogic = logic_tier.getGameserverLogic();

		if (gameserverLogic != null) {
			// update labels
			name_label.setText(gameserverLogic.getMetaDataGameserver().server_name);
			ip_label.setText(
					TextData.getWord("ip") + ": " + gameserverLogic.getMetaDataGameserver().ip);
			password_label.setText(TextData.getWord("passwordConsole") + ": "
					+ gameserverLogic.getMetaDataGameserver().password);
			seed_label.setText(
					TextData.getWord("seed") + ": " + gameserverLogic.getMetaDataGameserver().seed);

			// update #players
			players_label.setText(
					TextData.getWord("players") + ": " + GameserverLogic.PLAYER_MAP.values().size()
							+ " / " + gameserverLogic.getMetaDataGameserver().capacity);

			// update time label
			float time = gameserverLogic.getTime();
			int seconds = (int) (gameserverLogic.getTime() % 60);
			// represented as: ['minutes' : '0 (if seconds < 10) + seconds']
			time_text_parsed
					= (int) ((time) / 60f) + ":" + (seconds < 10 ? "0" : "") + "" + seconds;
			uptime_label.setText(TextData.getWord("uptime") + ": " + time_text_parsed);

			// update console
			console_widget.update(gameserverLogic, time_text_parsed);
		}
	}

	@Override
	public void loadWidgets() {

		// meta section labels
		name_label = new CustomLabel("", StyleContainer.label_bold_whitepure_18_style,
				Shared.WIDTH9, Shared.HEIGHT1, Align.center);
		ip_label = new CustomLabel("", StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3,
				Shared.HEIGHT1, Align.left);
		password_label = new CustomLabel("", StyleContainer.label_bold_whitepure_18_style,
				Shared.WIDTH3, Shared.HEIGHT1, Align.left);
		// text will be set continuously in update()
		players_label = new CustomLabel("", StyleContainer.label_bold_whitepure_18_style,
				Shared.WIDTH3, Shared.HEIGHT1, Align.center);
		uptime_label = new CustomLabel(TextData.getWord("uptime"),
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.center);
		seed_label = new CustomLabel("", StyleContainer.label_bold_whitepure_18_style,
				Shared.WIDTH3, Shared.HEIGHT1, Align.left);

		line_icon = new CustomIconbutton(StaticGraphic.gui_server_line,
				StaticGraphic.gui_server_line.getWidth(),
				StaticGraphic.gui_server_line.getHeight());

		console_widget = new ConsoleWidget();

		join_server_button = new CustomTextbutton(TextData.getWord("joinServer"),
				StyleContainer.button_bold_30_style, StaticGraphic.gui_bg_btn_custom3,
				Align.center);

		close_server_button = new CustomIconbutton(StaticGraphic.gui_icon_close_server,
				StaticGraphic.gui_icon_close_server.getWidth(),
				StaticGraphic.gui_icon_close_server.getHeight());

	}

	@Override
	public void setLayout() {

		// console
		CustomTable tableConsole = new CustomTable();
		tableConsole.padTop(230).setFillParent(true);
		tables.add(tableConsole);

		tableConsole.add(console_widget.getScrollPane()).width(800).height(300);

		// meta section
		CustomTable tableMetaSection = new CustomTable();
		tableMetaSection.padBottom(380).setFillParent(true);
		tables.add(tableMetaSection);

		tableMetaSection.add(name_label).colspan(4);
		tableMetaSection.row().padTop(25);
		tableMetaSection.add(ip_label).padRight(50);
		tableMetaSection.add(players_label);
		tableMetaSection.add();
		tableMetaSection.add(seed_label);
		tableMetaSection.row().padTop(25);
		tableMetaSection.add(password_label).padRight(50);
		tableMetaSection.add(uptime_label);
		tableMetaSection.row().padTop(25);
		tableMetaSection.add(line_icon).colspan(4);

		// join server button
		CustomTable tableStartbutton = new CustomTable();
		tableStartbutton.padTop(725f).padLeft(1f).setFillParent(true);
		tables.add(tableStartbutton);
		tableStartbutton.add(join_server_button);

		// close server button
		CustomTable tableClosebutton = new CustomTable();
		tableClosebutton.padBottom(545f).padLeft(1146f).setFillParent(true);
		tables.add(tableClosebutton);
		tableClosebutton.add(close_server_button);

	}

	@Override
	public void setListeners() {

		join_server_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				MetaDataGameserver mdgs = logic_tier.getGameserverLogic().getMetaDataGameserver();

				// host-client connects to the game server
				client_gameserver_connection.connect(mdgs.ip, mdgs.port_tcp_game,
						mdgs.port_udp_game);

			};
		});

		close_server_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				confirmation_module.setVisibleGlobal(true);
			}
		});
	}

	@Override
	public void loadSubModules() {
		confirmation_module = new AYesNoConfirmationModule(TextData.getWord("closeServer"),
				BasicScreen.DRAW_COVERMODULE_1) {

			@Override
			public void clickedYes() {
				console_widget.clear();
				logic_tier.disposeGameserverLogic();
			}
		};
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		subModules.add(confirmation_module);
	}

	@Override
	protected boolean clickedEscape() {
		return true;
	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(join_server_button);
		widgets.add(close_server_button);
		return new HoverHandler(widgets);
	}

}
