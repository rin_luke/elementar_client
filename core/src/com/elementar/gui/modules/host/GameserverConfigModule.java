package com.elementar.gui.modules.host;

import static com.elementar.data.container.StyleContainer.slider_enabled_style;
import static com.elementar.logic.util.Shared.MAX_GAMESERVERNAME_LENGTH;
import static com.elementar.logic.util.Shared.MAX_SERVER_SIZE;
import static com.elementar.logic.util.Shared.MIN_SERVER_SIZE;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomSlider;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.CustomTextfield;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.SeedTextfield;
import com.elementar.gui.widgets.TooltipTextArea;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.listener.TooltipListener;
import com.elementar.logic.LogicTier;
import com.elementar.logic.network.connection.AClientConnection.ConnectionStatus;
import com.elementar.logic.network.gameserver.MetaDataGameserver;
import com.elementar.logic.network.requests.CreateGameserverRequest;
import com.elementar.logic.network.util.NetworkUtil;
import com.elementar.logic.util.ErrorMessage;
import com.elementar.logic.util.Shared;

public class GameserverConfigModule extends AChildModule {

	// config
	private CustomSlider number_players_slider;
	private CustomLabel name_label, password_label, map_seed_label, max_players_label;
	private CustomIconbutton question_mark_icon, portframe_icon;
	private CustomTextbutton start_server_button;
	private SeedTextfield map_seed_field;
	private CustomTextfield name_field, password_field;

	// ports
	private CustomLabel ports_label, port_tcp_game_label, port_udp_game_label, how_to_host_label;
	private CustomTextfield port_tcp_game_field, port_udp_game_field;
	private TooltipTextArea how_to_host_tooltip;

	public GameserverConfigModule(boolean visible) {
		super(visible);
	}

	@Override
	public void loadWidgets() {

		// slider
		number_players_slider = new CustomSlider(MIN_SERVER_SIZE, MAX_SERVER_SIZE, 2, slider_enabled_style);

		number_players_slider.setValue(Shared.DEFAULT_SERVER_SIZE);

		// labels
		name_label = new CustomLabel(TextData.getWord("serverName"), StyleContainer.label_bold_whitepure_18_style,
				Shared.WIDTH3, Shared.HEIGHT1, Align.right);
		password_label = new CustomLabel(TextData.getWord("password"), StyleContainer.label_bold_whitepure_18_style,
				Shared.WIDTH3, Shared.HEIGHT1, Align.right);
		ports_label = new CustomLabel(TextData.getWord("ports"), StyleContainer.label_bold_whitepure_18_style,
				Shared.WIDTH3, Shared.HEIGHT1, Align.center);
		port_tcp_game_label = new CustomLabel(TextData.getWord("portTCPGame"),
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1, Align.right);
		port_udp_game_label = new CustomLabel(TextData.getWord("portUDPGame"),
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1, Align.right);
		how_to_host_label = new CustomLabel(TextData.getWord("howToHost"), StyleContainer.label_bold_whitepure_18_style,
				Shared.WIDTH3, Shared.HEIGHT1, Align.right);
		map_seed_label = new CustomLabel(TextData.getWord("mapSeed"), StyleContainer.label_bold_whitepure_18_style,
				Shared.WIDTH3, Shared.HEIGHT1, Align.right);
		max_players_label = new CustomLabel(
				TextData.getWord("maxPlayers") + ": " + (int) (number_players_slider.getValue()),
				StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH3, Shared.HEIGHT1, Align.right);

		Sprite background = StaticGraphic.gui_bg_inputfield_height1_width5;
		name_field = new CustomTextfield(background, MAX_GAMESERVERNAME_LENGTH);

		password_field = new CustomTextfield(background, 20, TextData.getWord("optional"));

		map_seed_field = new SeedTextfield(background, TextData.getWord("optional"));

		Sprite background2 = StaticGraphic.gui_bg_inputfield_height1_width3;
		port_tcp_game_field = new CustomTextfield(background2, 20);

		port_udp_game_field = new CustomTextfield(background2, 20);

		// TODO das muss nachher raus
		port_tcp_game_field.setText("40001");
		port_udp_game_field.setText("40002");

		start_server_button = new CustomTextbutton(TextData.getWord("startServer"), StyleContainer.button_bold_30_style,
				StaticGraphic.gui_bg_btn_custom2, Align.center);
		start_server_button.setSound(ClickSoundType.HEAVY);

		portframe_icon = new CustomIconbutton(StaticGraphic.gui_host_portframe,
				StaticGraphic.gui_host_portframe.getWidth(), StaticGraphic.gui_host_portframe.getHeight());
		question_mark_icon = new CustomIconbutton(StaticGraphic.gui_icon_help, StaticGraphic.gui_icon_help.getWidth(),
				StaticGraphic.gui_icon_help.getHeight());

		how_to_host_tooltip = new TooltipTextArea(TextData.getWord("hostDesc"), Shared.WIDTH6);

	}

	@Override
	public void setLayout() {

		/*
		 * with this variable you can shift the name-label, password-label, etc.
		 */
		float padTopContent = 130;

		// port frame
		CustomTable tablePortframe = new CustomTable();
		tablePortframe.padLeft(620f).padBottom(120f).padTop(padTopContent).setFillParent(true);
		tables.add(tablePortframe);
		tablePortframe.add(portframe_icon);

		// ports
		CustomTable tablePorts = new CustomTable();
		tablePorts.padLeft(540f).padBottom(180f).padTop(padTopContent).setFillParent(true);
		tables.add(tablePorts);
		tablePorts.columnDefaults(1).spaceLeft(DISTANCE_BETWEEN_ACTORS * 0.5f);
		tablePorts.columnDefaults(2).spaceLeft(DISTANCE_BETWEEN_ACTORS * 0.5f);
		tablePorts.add(port_tcp_game_label);
		tablePorts.add(port_tcp_game_field);
		tablePorts.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);
		tablePorts.add(port_udp_game_label);
		tablePorts.add(port_udp_game_field);
		tablePorts.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);

		// config
		CustomTable tableConfig = new CustomTable();
		tableConfig.padRight(500f).padBottom(110).padTop(padTopContent).setFillParent(true);
		tables.add(tableConfig);
		tableConfig.columnDefaults(1).spaceLeft(DISTANCE_BETWEEN_ACTORS * 0.5f);
		tableConfig.columnDefaults(2).spaceLeft(DISTANCE_BETWEEN_ACTORS);
		tableConfig.add(name_label);
		tableConfig.add(name_field);
		tableConfig.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);
		tableConfig.add(password_label);
		tableConfig.add(password_field);
		tableConfig.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);
		tableConfig.add(max_players_label);
		tableConfig.add(number_players_slider);
		tableConfig.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);
		tableConfig.add(map_seed_label);
		tableConfig.add(map_seed_field);

		// start server button
		CustomTable tableStartbutton = new CustomTable();
		tableStartbutton.padTop(725f).padLeft(1f).setFillParent(true);
		tables.add(tableStartbutton);
		tableStartbutton.add(start_server_button);

		// ports header
		CustomTable tableHeaderPorts = new CustomTable();
		tableHeaderPorts.padLeft(640f).padBottom(450f).padTop(padTopContent).setFillParent(true);
		tables.add(tableHeaderPorts);
		tableHeaderPorts.add(ports_label);

		// how to host
		CustomTable tableHowTo = new CustomTable();
		tableHowTo.padLeft(540).padTop(90 + padTopContent).setFillParent(true);
		tables.add(tableHowTo);
		tableHowTo.add(how_to_host_label).padRight(20);
		tableHowTo.add(question_mark_icon).left();
		how_to_host_tooltip.setPosition(tables, tableHowTo, question_mark_icon);

	}

	@Override
	public void setListeners() {

		number_players_slider.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				max_players_label.setText(
						TextData.getWord("maxPlayers") + ": " + (int) (number_players_slider.getVisualValue()));
			}
		});
		start_server_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				// send create-gameserver-request

				String gameserverName = name_field.getText();
				boolean isNameBlank = gameserverName == null || gameserverName.trim().isEmpty();
				
				if (NetworkUtil.INTERNET_CONNECTION_STATUS == ConnectionStatus.FAILED)
					LogicTier.ERROR_MESSAGE = ErrorMessage.noConnectionMainserver;
				else if (isNameBlank == true) {
					LogicTier.ERROR_MESSAGE = ErrorMessage.noServerName;
					name_field.setText("");
				} else if (client_mainserver_connection.isConnected() == true) {
					int serverSize = (int) (number_players_slider.getVisualValue());
					int portTCPGame = Integer.valueOf(port_tcp_game_field.getText());
					int portUDPGame = Integer.valueOf(port_udp_game_field.getText());
					MetaDataGameserver createdGameserver = new MetaDataGameserver(NetworkUtil.getExternalIP(),
							portTCPGame, portUDPGame, name_field.getText(), password_field.getText(), serverSize,
							GUIUtil.getValidSeed(map_seed_field));

					client_mainserver_connection.getClient().sendTCP(new CreateGameserverRequest(createdGameserver));
				} else
					LogicTier.ERROR_MESSAGE = ErrorMessage.noConnectionMainserver;

			};
		});

		question_mark_icon.addListener(new TooltipListener(how_to_host_tooltip));

	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(port_tcp_game_field);
		widgets.add(port_udp_game_field);
		widgets.add(name_field);
		widgets.add(password_field);
		widgets.add(map_seed_field);
		widgets.add(number_players_slider);
		widgets.add(start_server_button);
		return new HoverHandler(widgets);
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

	@Override
	public void unfocus() {
		name_field.unfocusWidget();
		password_field.unfocusWidget();
		map_seed_field.unfocusWidget();
		port_tcp_game_field.unfocusWidget();
		port_udp_game_field.unfocusWidget();
	}

	public String getPassword() {

		if (password_field == null)
			return "";

		return password_field.getText();
	}

}