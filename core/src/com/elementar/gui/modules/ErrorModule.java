package com.elementar.gui.modules;

import java.util.ArrayList;

import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.ARootMenuModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.AlphaTransition;
import com.elementar.logic.characters.AlphaTransitionAccessor;
import com.elementar.logic.util.Shared;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

public class ErrorModule extends AChildModule {

	private static float FADE_IN_OUT_DURATION = 0.125f;
	private static float ERROR_DISPLAY_DURATION = 2.3f;

	private TweenManager tween_alpha_manager = new TweenManager();
	private AlphaTransition alpha_transition = new AlphaTransition();
	private CustomLabel error_label;

	private final TweenCallback fade_out_callback = new TweenCallback() {
		@Override
		public void onEvent(int type, BaseTween<?> source) {
			Tween.to(alpha_transition, AlphaTransitionAccessor.FADE_OUT, FADE_IN_OUT_DURATION).target(0f)
					.ease(TweenEquations.easeInOutSine).delay(ERROR_DISPLAY_DURATION).start(tween_alpha_manager);

		}
	};

	public ErrorModule() {
		super(true, BasicScreen.DRAW_COVERMODULE_2);
	}

	@Override
	public void update(float delta) {

		tween_alpha_manager.update(delta);

		if (LogicTier.ERROR_MESSAGE != null) {

			tween_alpha_manager.killAll();
			alpha_transition.setValue(0);

			error_label.setText(TextData.getWord(LogicTier.ERROR_MESSAGE.name()));

			Tween.to(alpha_transition, AlphaTransitionAccessor.FADE_IN, FADE_IN_OUT_DURATION).target(1f)
					.ease(TweenEquations.easeInOutSine).setCallback(fade_out_callback).start(tween_alpha_manager);

			LogicTier.ERROR_MESSAGE = null;
		}

		error_label.setAlpha(alpha_transition.getValue());

	}

	@Override
	public void loadWidgets() {
		error_label = new CustomLabel("", StyleContainer.label_error_message_style, Shared.WIDTH9, Shared.HEIGHT1,
				Align.center);
	}

	@Override
	public void setLayout() {
		CustomTable table = new CustomTable();
		table.setFillParent(true);
		tables.add(table);
		if (this.screen.getRootModule() instanceof ARootMenuModule) {
			// menu
			table.add(error_label).padBottom(750);
		} else {
			// ingame
			table.bottom();
			table.add(error_label).padBottom(270);
		}

	}

	@Override
	public void setListeners() {
	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return null;
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

}
