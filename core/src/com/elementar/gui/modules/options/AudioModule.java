package com.elementar.gui.modules.options;

import static com.elementar.data.container.StyleContainer.slider_enabled_style;
import static com.elementar.gui.modules.options.OptionsModule.LABEL_WIDTH;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.AudioData;
import com.elementar.data.container.SettingsLoader;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomSlider;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.util.Shared;

public class AudioModule extends AChildModule {

	private CustomSlider general_slider, music_slider, effects_slider, ambient_slider;

	private CustomLabel general_volume_label, music_volume_label, effects_volume_label, ambient_volume_label,
			general_volume_value_label, music_volume_value_label,
			effects_volume_value_label, ambient_volume_value_label;

	public AudioModule(int drawOrder) {
		super(false, drawOrder);
	}

	@Override
	public void loadWidgets() {

		general_slider = new CustomSlider(0, 1, 0.01f, slider_enabled_style);
		music_slider = new CustomSlider(0, 1, 0.01f, slider_enabled_style);
		effects_slider = new CustomSlider(0, 1, 0.01f, slider_enabled_style);
		ambient_slider = new CustomSlider(0, 1, 0.01f, slider_enabled_style);

		// set loaded options
		general_slider.setValue(AudioData.general_volume);
		music_slider.setValue(AudioData.music_volume);
		effects_slider.setValue(AudioData.effects_volume);
		ambient_slider.setValue(AudioData.ambient_volume);

		general_volume_label = new CustomLabel(TextData.getWord("generalInAudio"),
				StyleContainer.label_medium_whitepure_20_style, OptionsModule.LABEL_WIDTH, Shared.HEIGHT1, Align.left);
		music_volume_label = new CustomLabel(TextData.getWord("music"), StyleContainer.label_medium_whitepure_20_style,
				LABEL_WIDTH, Shared.HEIGHT1, Align.left);
		effects_volume_label = new CustomLabel(TextData.getWord("effects"),
				StyleContainer.label_medium_whitepure_20_style, LABEL_WIDTH, Shared.HEIGHT1, Align.left);
		ambient_volume_label = new CustomLabel(TextData.getWord("ambient"),
				StyleContainer.label_medium_whitepure_20_style, LABEL_WIDTH, Shared.HEIGHT1, Align.left);

		general_volume_value_label = new CustomLabel((int) (AudioData.general_volume * 100) + "",
				StyleContainer.label_medium_whitepure_20_style, Shared.WIDTH2, Shared.HEIGHT1, Align.center);
		music_volume_value_label = new CustomLabel((int) (AudioData.music_volume * 100) + "",
				StyleContainer.label_medium_whitepure_20_style, Shared.WIDTH2, Shared.HEIGHT1, Align.center);
		effects_volume_value_label = new CustomLabel((int) (AudioData.effects_volume * 100) + "",
				StyleContainer.label_medium_whitepure_20_style, Shared.WIDTH2, Shared.HEIGHT1, Align.center);
		ambient_volume_value_label = new CustomLabel((int) (AudioData.ambient_volume * 100) + "",
				StyleContainer.label_medium_whitepure_20_style, Shared.WIDTH2, Shared.HEIGHT1, Align.center);
	}

	@Override
	public void setLayout() {

		CustomTable table = new CustomTable();
		table.setFillParent(true);
		tables.add(table);

		table.add(general_volume_label);
		table.add(general_slider);
		table.add(general_volume_value_label);
		table.row().padTop(50);
		table.add(music_volume_label);
		table.add(music_slider);
		table.add(music_volume_value_label);
		table.row().padTop(20);
		table.add(effects_volume_label);
		table.add(effects_slider);
		table.add(effects_volume_value_label);
		table.row().padTop(20);
		table.add(ambient_volume_label);
		table.add(ambient_slider);
		table.add(ambient_volume_value_label);

	}

	@Override
	public void setListeners() {

		general_slider.addListener(new AudioPersistentChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				super.changed(event, actor);
				AudioData.update();
			}

			@Override
			public void setAttributes() {
				float val = general_slider.getVisualValue();
				AudioData.general_volume = val;
				int valPercentage = (int) (val * 100);
				general_volume_value_label.setText(valPercentage + "");
			}
		});
		music_slider.addListener(new AudioPersistentChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				super.changed(event, actor);
				AudioData.update();
			}

			@Override
			public void setAttributes() {
				float val = music_slider.getVisualValue();
				AudioData.music_volume = val;
				int valPercentage = (int) (val * 100);
				music_volume_value_label.setText(valPercentage + "");
			}
		});
		effects_slider.addListener(new AudioPersistentChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				super.changed(event, actor);
				AudioData.update();
			}

			@Override
			public void setAttributes() {
				float val = effects_slider.getVisualValue();
				AudioData.effects_volume = val;
				int valPercentage = (int) (val * 100);
				effects_volume_value_label.setText(valPercentage + "");
			}
		});

		ambient_slider.addListener(new AudioPersistentChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				super.changed(event, actor);
				AudioData.update();
			}

			@Override
			public void setAttributes() {
				float val = ambient_slider.getVisualValue();
				AudioData.ambient_volume = val;
				int valPercentage = (int) (val * 100);
				ambient_volume_value_label.setText(valPercentage + "");
			}
		});

	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(general_slider);
		widgets.add(music_slider);
		widgets.add(effects_slider);
		widgets.add(ambient_slider);

		return new HoverHandler(widgets);
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

	private abstract class AudioPersistentChangeListener extends PersistentChangeListener {

		@Override
		public void save() {
			SettingsLoader.saveAudio();
		}

	}
}
