package com.elementar.gui.modules.options;

import static com.elementar.gui.modules.options.OptionsModule.LABEL_WIDTH;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.KeysConfiguration;
import com.elementar.data.container.SettingsLoader;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.data.container.KeysConfiguration.WorldAction;
import com.elementar.data.container.util.AdvancedKey;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;
import com.elementar.logic.util.Shared;

public class KeysModule extends AChildModule {

	private ArrayList<CustomLabel> action_labels;

	private KeyIndexedHashMap<CustomTextbutton, WorldAction> button_action_map;

	private AdvancedKey last_keycode;

	private CustomTextbutton changing_button;

	private CustomLabel category_skills_label, category_movement_label, category_miscellaneous_label;
	private CustomTextbutton restore_defaults_btn;

	private ScrollPane scrollpane;
	private VerticalGroup vertical_group;

	/**
	 * help variable to transport a signal from keyDown() to clickedEscape()
	 */
	private boolean changing_active = false;

	public KeysModule(int drawOrder) {
		super(false, drawOrder);
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		changing_active = changing_button != null;

	}

	@Override
	public void loadWidgets() {

		vertical_group = new VerticalGroup();
		scrollpane = new ScrollPane(vertical_group);
		scrollpane.setStyle(StyleContainer.scrollpane_client_style);
		scrollpane.setScrollbarsVisible(true);
		scrollpane.setFadeScrollBars(false);
		scrollpane.setOverscroll(false, true);
		scrollpane.setFillParent(true);

		action_labels = new ArrayList<>();
		button_action_map = new KeyIndexedHashMap<>();

		// movement
		createActionLabel(TextData.getWord("moveLeft"));
		createActionLabel(TextData.getWord("moveRight"));
		createActionLabel(TextData.getWord("jump"));
		createActionLabel(TextData.getWord("hold"));
		// skills
		createActionLabel(TextData.getWord("switchComboMode"));
		createActionLabel(TextData.getWord("basicAttack"));
		createActionLabel(TextData.getWord("skill1"));
		createActionLabel(TextData.getWord("skill2"));
		// chat
		createActionLabel(TextData.getWord("openChat"));
		createActionLabel(TextData.getWord("openPlayerStatistics"));

		// movement
		createKeyButton(WorldAction.MOVE_LEFT);
		createKeyButton(WorldAction.MOVE_RIGHT);
		createKeyButton(WorldAction.JUMP);
		createKeyButton(WorldAction.HOLD);
		// skills
		createKeyButton(WorldAction.SWITCH_COMBO_MODE);
		createKeyButton(WorldAction.EXECUTE_SKILL0);
		createKeyButton(WorldAction.EXECUTE_SKILL1);
		createKeyButton(WorldAction.EXECUTE_SKILL2);
		// chat
		createKeyButton(WorldAction.OPEN_CHAT);
		createKeyButton(WorldAction.OPEN_PLAYER_STATISTICS);

		category_miscellaneous_label = new CustomLabel(TextData.getWord("miscellaneous"),
				StyleContainer.label_keymodule_titles_style, LABEL_WIDTH, Shared.HEIGHT1, Align.left);
		category_movement_label = new CustomLabel(TextData.getWord("movement"),
				StyleContainer.label_keymodule_titles_style, LABEL_WIDTH, Shared.HEIGHT1, Align.left);
		category_skills_label = new CustomLabel(TextData.getWord("skills"), StyleContainer.label_keymodule_titles_style,
				LABEL_WIDTH, Shared.HEIGHT1, Align.left);

		restore_defaults_btn = new CustomTextbutton(TextData.getWord("restoreDefaults"),
				StyleContainer.button_bold_20_style, StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		restore_defaults_btn.setSound(ClickSoundType.LIGHT);

	}

	private void createActionLabel(String text) {
		action_labels.add(new CustomLabel(text, StyleContainer.label_medium_whitepure_20_style,
				OptionsModule.LABEL_WIDTH, Shared.HEIGHT1, Align.left));
	}

	private void createKeyButton(WorldAction action) {

		CustomTextbutton keyButton = new CustomTextbutton(getKeycodeString(KeysConfiguration.getKeycode(action)),
				StyleContainer.button_bold_20_style, StaticGraphic.gui_bg_btn_height1_width3, Align.center);
		keyButton.setSound(ClickSoundType.LIGHT);

		button_action_map.put(keyButton, action);
	}

	@Override
	public void setLayout() {

		CustomTable tableSub = new CustomTable();

		// add movement widgets
		tableSub.add(category_movement_label);
		tableSub.row();
		for (int i = 0; i < 4; i++) {
			tableSub.add(action_labels.get(i));
			tableSub.add(button_action_map.get(i)).padLeft(DISTANCE_BETWEEN_ACTORS);
			tableSub.row();
		}

		// add skill widgets
		tableSub.add(category_skills_label).padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);
		tableSub.row();
		for (int i = 4; i < 4 + 4; i++) {
			tableSub.add(action_labels.get(i));
			tableSub.add(button_action_map.get(i)).padLeft(DISTANCE_BETWEEN_ACTORS);
			tableSub.row();
		}

		// add miscellaneous widgets
		tableSub.add(category_miscellaneous_label).padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);
		tableSub.row();
		for (int i = 4 + 4; i < 4 + 4 + 2; i++) {
			tableSub.add(action_labels.get(i));
			tableSub.add(button_action_map.get(i)).padLeft(DISTANCE_BETWEEN_ACTORS);
			tableSub.row();
		}

		vertical_group.addActor(tableSub);

		CustomTable tableMain = new CustomTable();
		tableMain.padTop(100).setFillParent(true);
		tables.add(tableMain);

		// note that vertical group is already part of scrollpane
		WidgetGroup scrollGroup = new WidgetGroup();
		scrollGroup.addActor(scrollpane);
		tableMain.add(scrollGroup).width(1000).height(500);
		tableMain.row();
		tableMain.add(restore_defaults_btn).align(Align.right).padTop(50);
		scrollpane.layout();

	}

	@Override
	public void setListeners() {

		scrollpane.addListener(new InputListener() {
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				if (scrollpane.getStage() == null)
					return;

				scrollpane.getStage().setScrollFocus(scrollpane);
			}

			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				if (scrollpane.getStage() == null)
					return;

				scrollpane.getStage().setScrollFocus(null);
			}
		});

		for (Entry<CustomTextbutton, WorldAction> entry : button_action_map.entrySet()) {
			entry.getKey().addListener(new ClickListener() {

				@Override
				public void clicked(InputEvent event, float x, float y) {
					super.clicked(event, x, y);

					if (touch_down_entered == false) {
						/*
						 * reset current selected button to its previous value if the user hasn't
						 * pressed
						 */
						confirmChangingButton(last_keycode);

						changing_button = entry.getKey();
						last_keycode = KeysConfiguration.getKeycode(button_action_map.get(changing_button));
						changing_button.setBG(StaticGraphic.gui_bg_btn_keyconfig);

						// unselect button and remove from hover-handler
						changing_button.setState(State.NOT_SELECTED);
						hover_handler.remove(changing_button);
						changing_button.setText("");
					}

					touch_down_entered = false;

				}

			});
		}

		restore_defaults_btn.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);

				/*
				 * reset current selected button to its previous value if the user hasn't
				 * pressed a key so far
				 */
				confirmChangingButton(last_keycode);

				for (Entry<CustomTextbutton, WorldAction> entry : button_action_map.entrySet()) {

					CustomTextbutton button = entry.getKey();
					WorldAction action = entry.getValue();

					applyNewConfig(KeysConfiguration.getKeycodeBuiltIn(action), button);

				}
			}

		});

	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.addAll(button_action_map.keySet());
		widgets.add(restore_defaults_btn);
		return new HoverHandler(widgets);
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

	/**
	 * confirms the current button which should be changed if not <code>null</code>
	 * 
	 * @param keycode
	 */
	private void confirmChangingButton(AdvancedKey keycode) {

		if (changing_button != null) {

			changing_button.setBG(StaticGraphic.gui_bg_btn_height1_width3);

			hover_handler.add(changing_button);

			applyNewConfig(keycode, changing_button);

			changing_button = null;
		}

	}

	/**
	 * button gets text determined by passed keycode, {@link KeysConfiguration} gets
	 * updated and {@link SettingsLoader} saves the new config
	 * 
	 * @param keycode
	 * @param button
	 */
	private void applyNewConfig(AdvancedKey keycode, CustomTextbutton button) {
		button.setText(getKeycodeString(keycode));
		KeysConfiguration.setNewConfig(keycode, button_action_map.get(button));
		SettingsLoader.save();
	}

	/**
	 * keycode might be -1 if there is no assignment for the button. Returns an
	 * empty String in that case. Otherwise the regular String provided by
	 * {@link Keys#toString(int)}
	 * 
	 * @param intBoolPair
	 * @return
	 */
	public static String getKeycodeString(AdvancedKey intBoolPair) {
		if (intBoolPair == null)
			return "";
		if (intBoolPair.isFromKeyboard() == true)
			return Keys.toString(intBoolPair.getKey());
		else
			return getMouseString(intBoolPair.getKey());
	}

	private static String getMouseString(int mouseKey) {
		switch (mouseKey) {
		case Buttons.LEFT:
			return "LMB";
		case Buttons.RIGHT:
			return "RMB";
		case Buttons.MIDDLE:
			return "Mouse Middle";
		case Buttons.FORWARD:
			return "Mouse Forward";
		case Buttons.BACK:
			return "Mouse Back";
		}
		return "";
	}

	@Override
	public void keyDown(int keycode) {
		// no sub modules = no need to call superior method
		if (changing_button != null) {

			AdvancedKey advancedKey = new AdvancedKey(keycode, true);

			if (keycode == Keys.ESCAPE) {
				/*
				 * If 'escape' is pressed the last key should be used
				 */
				advancedKey.setKey(last_keycode.getKey());

			} else {
				// is the keycode in use by a button?
				for (Entry<CustomTextbutton, WorldAction> entry : button_action_map.entrySet()) {

					AdvancedKey entryKeycode = KeysConfiguration.getKeycode(entry.getValue());

					if (entryKeycode == null)
						continue;

					if (entryKeycode.equals(advancedKey) == true)
						if (entry.getKey() != changing_button) {
							/*
							 * assign old button with current changing button, which isn't set yet (holding
							 * old value)
							 */
							AdvancedKey oldKey = KeysConfiguration.getKeycode(button_action_map.get(changing_button));
							if (oldKey == null)
								continue;
							applyNewConfig(oldKey, entry.getKey());
						}
				}
			}

			confirmChangingButton(advancedKey);

		}

	}

	boolean touch_down_entered = false;

	@Override
	public void touchDown(int screenX, int screenY, int pointer, int button) {

		// no sub modules = no need to call superior method
		if (changing_button != null) {

			touch_down_entered = true;

			AdvancedKey advancedKey = new AdvancedKey(button, false);

			// is the keycode in use by a button?
			for (Entry<CustomTextbutton, WorldAction> entry : button_action_map.entrySet()) {
				AdvancedKey entryKeycode = KeysConfiguration.getKeycode(entry.getValue());
				if (entryKeycode != null)
					if (entryKeycode.equals(advancedKey) == true)
						if (entry.getKey() != changing_button) {
							/*
							 * assign old button with current changing button, which isn't set yet (holding
							 * old value)
							 */
							AdvancedKey oldKey = KeysConfiguration.getKeycode(button_action_map.get(changing_button));
							if (oldKey == null)
								continue;
							applyNewConfig(oldKey, entry.getKey());
						}
			}
			confirmChangingButton(advancedKey);
		}

	}

	@Override
	public void setVisibleGlobal(boolean visible) {
		if (visible == false)
			confirmChangingButton(last_keycode);
		super.setVisibleGlobal(visible);
	}

	@Override
	protected boolean clickedEscape() {
		// a changing button is active, so don't handle the escape case further
		if (changing_active == true) {
			changing_active = false;
			return true;
		}
		return super.clickedEscape();
	}

	/**
	 * Inserted keys are additionally added to an {@link ArrayList}. With
	 * {@link #get(int)} you get a key by index. Thats useful when the order of
	 * insertion has to be maintained like for the key config buttons.
	 * 
	 * @author lukassongajlo
	 *
	 * @param <K>
	 * @param <V>
	 */
	private class KeyIndexedHashMap<K, V> extends HashMap<K, V> {

		private ArrayList<K> list = new ArrayList<>();

		@Override
		public V put(K key, V value) {
			list.add(key);
			return super.put(key, value);
		}

		public K get(int index) {
			return list.get(index);
		}

	}

}
