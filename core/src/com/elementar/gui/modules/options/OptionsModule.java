package com.elementar.gui.modules.options;

import java.util.ArrayList;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AChildCoverModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.modules.ingame.ESCModule;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;

/**
 * 
 * @author lukassongajlo
 * 
 */
public class OptionsModule extends AChildCoverModule {

	/**
	 * for label-layout
	 */
	public final static float LABEL_WIDTH = 210;

	private GeneralModule general_module;
	private KeysModule keys_module;
	private GraphicsModule graphics_module;
	private AudioModule audio_module;
	private LanguageModule language_module;

	private CustomTextbutton general_tab_button, keys_tab_button, graphics_tab_button,
			audio_tab_button, language_tab_button;

	/**
	 * In the mainmenu screen the drawOrder is
	 * {@link BasicScreen#DRAW_COVERMODULE_1}, otherwise
	 * {@link BasicScreen#DRAW_COVERMODULE_2} (for example: when you are wihtin
	 * the world and you click {@link Keys#ESCAPE}, the {@link ESCModule} occurs
	 * and when you click Optionen from that view, the draw_order will be
	 * {@link BasicScreen#DRAW_COVERMODULE_2}
	 */
	public OptionsModule(int drawOrder) {
		super(StaticGraphic.gui_bg_options1, drawOrder, true);
	}

	@Override
	public void loadWidgets() {
		super.loadWidgets();

		Sprite background = StaticGraphic.gui_bg_btn_height1_width4;

		general_tab_button = new CustomTextbutton(TextData.getWord("general"),
				StyleContainer.button_bold_20_style, background, Align.center);
		general_tab_button.setSound(ClickSoundType.LIGHT);
		general_tab_button.onewayClicklistener(true);

		keys_tab_button = new CustomTextbutton(TextData.getWord("keys"),
				StyleContainer.button_bold_20_style, background, Align.center);
		keys_tab_button.setSound(ClickSoundType.LIGHT);
		keys_tab_button.onewayClicklistener(true);

		graphics_tab_button = new CustomTextbutton(TextData.getWord("graphics"),
				StyleContainer.button_bold_20_style, background, Align.center);
		graphics_tab_button.setSound(ClickSoundType.LIGHT);
		graphics_tab_button.onewayClicklistener(true);

		audio_tab_button = new CustomTextbutton(TextData.getWord("audio"),
				StyleContainer.button_bold_20_style, background, Align.center);
		audio_tab_button.setSound(ClickSoundType.LIGHT);
		audio_tab_button.onewayClicklistener(true);

		language_tab_button = new CustomTextbutton(TextData.getWord("language"),
				StyleContainer.button_bold_20_style, background, Align.center);
		language_tab_button.setSound(ClickSoundType.LIGHT);
		language_tab_button.onewayClicklistener(true);

	}

	@Override
	public void setLayout() {
		super.setLayout();

		table_inner.top().left();
		table_inner.setPosition(inner_group.getX(),
				inner_group.getY() + inner_image.getSprite().getHeight());
		table_inner.add(general_tab_button).padLeft(46);
		table_inner.add(keys_tab_button);
		table_inner.add(graphics_tab_button);
		table_inner.add(audio_tab_button);
//		table_inner.add(language_tab_button);
	}

	@Override
	public void setListeners() {
		super.setListeners();
		general_tab_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				changeTab(general_module);
			}
		});
		keys_tab_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				changeTab(keys_module);
			}
		});
		graphics_tab_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				changeTab(graphics_module);
			}
		});
		audio_tab_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				changeTab(audio_module);
			}
		});
		language_tab_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				changeTab(language_module);
			}
		});

	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(general_tab_button);
		widgets.add(keys_tab_button);
		widgets.add(graphics_tab_button);
		widgets.add(audio_tab_button);
		widgets.add(language_tab_button);
		return new HoverHandler(widgets, general_tab_button);
	}

	@Override
	public void loadSubModules() {
		general_module = new GeneralModule(draw_order);
		keys_module = new KeysModule(draw_order);
		graphics_module = new GraphicsModule(draw_order);
		audio_module = new AudioModule(draw_order);
		language_module = new LanguageModule(draw_order);
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		subModules.add(general_module);
		subModules.add(keys_module);
		subModules.add(graphics_module);
		subModules.add(audio_module);
		subModules.add(language_module);
	}

	@Override
	protected void addTabs(ArrayList<AModule> tabs) {
		tabs.addAll(sub_modules);
	}

	@Override
	protected boolean clickedEscape() {
		setVisibleGlobal(false);
		return true;
	}

	/**
	 * If OptionsModule closes, all submodules should be turn into invisible. If
	 * OptionsModule opens the visibility of the first submodule (here:
	 * {@link OptionsModule#general_module}) is set to true.
	 */
	@Override
	public void setVisibleGlobal(boolean visible) {
		super.setVisibleGlobal(visible);
		if (visible == false) {
			for (AModule subModule : tabs)
				subModule.setVisibleGlobal(false);
		} else {
			general_tab_button.setState(State.SELECTED);
			for (AModule subModule : tabs)
				subModule.getHoverHandler().setActive(true);
			general_module.setVisibleGlobal(true);
		}
	}
}
