package com.elementar.gui.modules.options;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public abstract class PersistentChangeListener extends ChangeListener {

	@Override
	public void changed(ChangeEvent event, Actor actor) {
		setAttributes();
	}

	@Override
	public boolean handle(Event event) {
		if (event instanceof InputEvent) {
			InputEvent inputEvent = (InputEvent) event;
			if (inputEvent.getType() == Type.exit) {
				save();
			}
		}

		return super.handle(event);
	}

	public abstract void setAttributes();
	
	public abstract void save();
}
