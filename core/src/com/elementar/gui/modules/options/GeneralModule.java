package com.elementar.gui.modules.options;

import static com.elementar.data.container.StyleContainer.checkbox_style;
import static com.elementar.data.container.StyleContainer.slider_enabled_style;
import static com.elementar.gui.modules.options.OptionsModule.LABEL_WIDTH;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.OptionsData;
import com.elementar.data.container.SettingsLoader;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.ARootWorldModule;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.widgets.ComboRose;
import com.elementar.gui.widgets.CustomCheckbox;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomSlider;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.RadioButtonFunctionality;
import com.elementar.gui.widgets.TooltipTextArea;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.listener.TooltipListener;
import com.elementar.logic.util.Shared;

public class GeneralModule extends AChildModule {

	private CustomLabel fps_label;
	private CustomCheckbox fps_checkbox;

	private CustomLabel minimap_layout_label, minimap_left_label, minimap_right_label, combo_rose_sensitivity_label,
			combo_rose_sensitivity_value_label;
	private RadioButtonFunctionality minimap_radio_func;
	private CustomIconbutton minimap_left_btn, minimap_right_btn;

	private CustomSlider combo_rose_sensitivity_slider;
	private TooltipTextArea combo_rose_sensitivity_tooltip;
	private CustomIconbutton combo_rose_sensitivity_question_mark;

	public GeneralModule(int drawOrder) {
		super(false, drawOrder);
	}

	@Override
	public void show() {
		super.show();

		// click on one radio button
		if (OptionsData.minimap_left == true)
			GUIUtil.clickActor(minimap_left_btn);
		else
			GUIUtil.clickActor(minimap_right_btn);
	}

	@Override
	public void loadWidgets() {

		fps_label = new CustomLabel(TextData.getWord("fps"), StyleContainer.label_medium_whitepure_20_style,
				LABEL_WIDTH, Shared.HEIGHT1, Align.left);
		fps_checkbox = new CustomCheckbox(checkbox_style);
		fps_checkbox.setChecked(OptionsData.fps_on);

		// minimap labels
		minimap_layout_label = new CustomLabel(TextData.getWord("minimapPosition"),
				StyleContainer.label_medium_whitepure_20_style, LABEL_WIDTH, Shared.HEIGHT1, Align.left);
		minimap_left_label = new CustomLabel(TextData.getWord("minimapLeft"),
				StyleContainer.label_medium_whitepure_20_style, Shared.WIDTH2, Shared.HEIGHT1, Align.left);
		minimap_right_label = new CustomLabel(TextData.getWord("minimapRight"),
				StyleContainer.label_medium_whitepure_20_style, Shared.WIDTH2, Shared.HEIGHT1, Align.left);

		combo_rose_sensitivity_label = new CustomLabel(TextData.getWord("comboRoseSensitivity"),
				StyleContainer.label_medium_whitepure_20_style, LABEL_WIDTH, Shared.HEIGHT1, Align.left);
		combo_rose_sensitivity_slider = new CustomSlider(0f, 1f, 0.01f, slider_enabled_style);
		combo_rose_sensitivity_slider.setValue(OptionsData.combo_rose_sensitivity);
		combo_rose_sensitivity_value_label = new CustomLabel((int) (OptionsData.combo_rose_sensitivity * 100) + "",
				StyleContainer.label_medium_whitepure_20_style, Shared.WIDTH2, Shared.HEIGHT1, Align.center);
		combo_rose_sensitivity_tooltip = new TooltipTextArea(TextData.getWord("comboRoseSensitivityDetail"), Shared.WIDTH6);
		combo_rose_sensitivity_question_mark = new CustomIconbutton(StaticGraphic.gui_icon_help,
				StaticGraphic.gui_icon_help.getWidth(), StaticGraphic.gui_icon_help.getHeight()) {
			@Override
			public float getPrefWidth() {
				return super.getPrefWidth() + 20;
			}
		};

		minimap_radio_func = new RadioButtonFunctionality();
		// minimap radio buttons
		minimap_left_btn = new CustomIconbutton(StaticGraphic.gui_icon_radiobutton_unchecked,
				StaticGraphic.gui_icon_radiobutton_checked, Shared.WIDTH1, Shared.HEIGHT1);
		minimap_left_btn.setSound(ClickSoundType.LIGHT);
		minimap_radio_func.add(minimap_left_btn);

		// right minimap radio button
		minimap_right_btn = new CustomIconbutton(StaticGraphic.gui_icon_radiobutton_unchecked,
				StaticGraphic.gui_icon_radiobutton_checked, Shared.WIDTH1, Shared.HEIGHT1);
		minimap_right_btn.setSound(ClickSoundType.LIGHT);
		minimap_radio_func.add(minimap_right_btn);

	}

	@Override
	public void setLayout() {

		CustomTable table = new CustomTable();
		table.setFillParent(true);
		tables.add(table);

		float preLabelWidth = combo_rose_sensitivity_question_mark.getPrefWidth();

		table.columnDefaults(2).spaceLeft(DISTANCE_BETWEEN_ACTORS);
		table.add().width(preLabelWidth);
		table.add(fps_label);
		table.add(fps_checkbox);
		table.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);
		table.add().width(preLabelWidth);
		table.add(minimap_layout_label);
		table.add(minimap_left_btn);
		table.add(minimap_left_label);
		table.add(minimap_right_btn).padLeft(DISTANCE_BETWEEN_ACTORS * 0.3f);
		table.add(minimap_right_label);
		table.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);
//		table.add(combo_rose_sensitivity_question_mark).width(preLabelWidth);
//		table.add(combo_rose_sensitivity_label);
//		HorizontalGroup group = new HorizontalGroup();
//		group.addActor(combo_rose_sensitivity_slider);
//		group.addActor(combo_rose_sensitivity_value_label);
//		table.add(group).width(fps_checkbox.getWidth());

//		table.add(combo_rose_sensitivity_question_mark).padLeft(DISTANCE_BETWEEN_ACTORS * 0.1f);

//		combo_rose_sensitivity_tooltip.setPosition(tables, table, combo_rose_sensitivity_question_mark);

	}

	@Override
	public void setListeners() {

		fps_checkbox.addListener(new PersistentClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				STATS_MODULE.setVisibleGlobal(fps_checkbox.isChecked());
			}

			@Override
			public void setAttributes() {
				OptionsData.fps_on = fps_checkbox.isChecked();
			}
		});

		minimap_left_btn.addListener(new PersistentClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				minimap_radio_func.check(0);
				ARootWorldModule.MINIMAP_DRAW_LEFT = true;
			}

			@Override
			public void setAttributes() {
				OptionsData.minimap_left = true;
			}
		});

		minimap_right_btn.addListener(new PersistentClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				minimap_radio_func.check(1);
				ARootWorldModule.MINIMAP_DRAW_LEFT = false;
			}

			@Override
			public void setAttributes() {
				OptionsData.minimap_left = false;
			}
		});

		combo_rose_sensitivity_slider.addListener(new PersistentChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				super.changed(event, actor);
			}

			@Override
			public void setAttributes() {
				float val = combo_rose_sensitivity_slider.getVisualValue();
				OptionsData.combo_rose_sensitivity = val;
				ComboRose.setScale(val);
				int valPercentage = (int) (val * 100);
				combo_rose_sensitivity_value_label.setText(valPercentage+"");
			}

			@Override
			public void save() {
				SettingsLoader.save();
			}
		});

		combo_rose_sensitivity_question_mark.addListener(new TooltipListener(combo_rose_sensitivity_tooltip));

	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(fps_checkbox);
		widgets.add(minimap_left_btn);
		widgets.add(minimap_right_btn);
		widgets.add(combo_rose_sensitivity_slider);
		widgets.add(combo_rose_sensitivity_question_mark);
		return new HoverHandler(widgets);
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

}
