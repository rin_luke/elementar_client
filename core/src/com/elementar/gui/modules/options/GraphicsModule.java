package com.elementar.gui.modules.options;

import static com.elementar.data.container.StyleContainer.checkbox_style;
import static com.elementar.data.container.StyleContainer.slider_enabled_style;
import static com.elementar.gui.modules.options.OptionsModule.LABEL_WIDTH;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.OptionsData;
import com.elementar.data.container.SettingsLoader;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.widgets.CustomCheckbox;
import com.elementar.gui.widgets.CustomDropdown;
import com.elementar.gui.widgets.CustomIconbutton;
import com.elementar.gui.widgets.CustomLabel;
import com.elementar.gui.widgets.CustomSlider;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.RadioButtonFunctionality;
import com.elementar.gui.widgets.ResolutionDropdown;
import com.elementar.gui.widgets.TooltipTextArea;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.listener.TooltipListener;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.CategorySize;

public class GraphicsModule extends AChildModule {

	private CustomLabel vegetation_intersection_label, vegetation_intersection_value_label, fullscreen_label,
			aspect_ratio_label, four_to_3_label, sixteen_to_9_label, sixteen_to_10_label, resolution_label,
			v_sync_label;

	private CustomIconbutton vegetation_intersection_question_mark;

	private CustomCheckbox fullscreen_checkbox, v_sync_checkbox;
	private CustomSlider vegetation_intersection_slider;

	private ResolutionDropdown resolution_dropdown;

	/**
	 * this cell contains the current resolution dropdown menu for the resolutions
	 */
	private Cell<CustomDropdown> resolution_cell;

	private TooltipTextArea vegetation_intersection_tooltip;

	private CustomIconbutton aspect_ratio_button_4_to_3, aspect_ratio_button_16_to_9, aspect_ratio_button_16_to_10;

	private CustomTextbutton apply_btn, restore_defaults_btn;

	private RadioButtonFunctionality aspect_ratio_radio_func;

	private String aspect_ratio;

	public GraphicsModule(int drawOrder) {
		super(false, drawOrder);
	}

	@Override
	public void show() {

		super.show();

		aspect_ratio = OptionsData.aspect_ratio;
		clickAspectRatioButton();

		Integer[] resolution = ResolutionDropdown.parseResolution(OptionsData.resolution);
		if (resolution != null)
			for (CustomTextbutton button : resolution_dropdown.getCurrent().getItems()) {
				Integer[] resolutionButton = ResolutionDropdown.parseResolution(button.getText().toString());
				if (resolutionButton != null)
					if (resolutionButton[0].equals(resolution[0]) && resolutionButton[1].equals(resolution[1])) {
						GUIUtil.clickActor(resolution_dropdown.getCurrent());
						GUIUtil.clickActor(button);
					}
			}

	}

	@Override
	public void loadWidgets() {

		resolution_label = new CustomLabel(TextData.getWord("screenResolution"),
				StyleContainer.label_medium_whitepure_20_style, LABEL_WIDTH, Shared.HEIGHT1, Align.left);

		resolution_dropdown = new ResolutionDropdown();

		aspect_ratio_radio_func = new RadioButtonFunctionality();
		aspect_ratio_label = new CustomLabel(TextData.getWord("aspectRatio"),
				StyleContainer.label_medium_whitepure_20_style, LABEL_WIDTH, Shared.HEIGHT1, Align.left);

		float aspectLabelWidth = Shared.WIDTH2;

		// 4:3
		aspect_ratio_button_4_to_3 = new CustomIconbutton(StaticGraphic.gui_icon_radiobutton_unchecked,
				StaticGraphic.gui_icon_radiobutton_checked, Shared.WIDTH1, Shared.HEIGHT1);
		aspect_ratio_button_4_to_3.setSound(ClickSoundType.LIGHT);
		aspect_ratio_radio_func.add(aspect_ratio_button_4_to_3);

		four_to_3_label = new CustomLabel(TextData.getWord("4to3"), StyleContainer.label_medium_whitepure_20_style,
				aspectLabelWidth, Shared.HEIGHT1, Align.left);
		// 16:9
		aspect_ratio_button_16_to_9 = new CustomIconbutton(StaticGraphic.gui_icon_radiobutton_unchecked,
				StaticGraphic.gui_icon_radiobutton_checked, Shared.WIDTH1, Shared.HEIGHT1);
		aspect_ratio_button_16_to_9.setSound(ClickSoundType.LIGHT);
		aspect_ratio_radio_func.add(aspect_ratio_button_16_to_9);

		sixteen_to_9_label = new CustomLabel(TextData.getWord("16to9"), StyleContainer.label_medium_whitepure_20_style,
				aspectLabelWidth, Shared.HEIGHT1, Align.left);
		// 16:10
		aspect_ratio_button_16_to_10 = new CustomIconbutton(StaticGraphic.gui_icon_radiobutton_unchecked,
				StaticGraphic.gui_icon_radiobutton_checked, Shared.WIDTH1, Shared.HEIGHT1);
		aspect_ratio_button_16_to_10.setSound(ClickSoundType.LIGHT);
		aspect_ratio_radio_func.add(aspect_ratio_button_16_to_10);

		sixteen_to_10_label = new CustomLabel(TextData.getWord("16to10"),
				StyleContainer.label_medium_whitepure_20_style, aspectLabelWidth, Shared.HEIGHT1, Align.left);

		fullscreen_label = new CustomLabel(TextData.getWord("fullscreen"),
				StyleContainer.label_medium_whitepure_20_style, LABEL_WIDTH, Shared.HEIGHT1, Align.left);
		fullscreen_checkbox = new CustomCheckbox(checkbox_style);
		fullscreen_checkbox.setChecked(OptionsData.fullscreen_on);

		vegetation_intersection_slider = new CustomSlider(0, CategorySize.values().length, 1, slider_enabled_style);
		vegetation_intersection_slider.setValue(OptionsData.vegetation_intersection_level);

		vegetation_intersection_label = new CustomLabel(TextData.getWord("vegetationIntersection"),
				StyleContainer.label_medium_whitepure_20_style, LABEL_WIDTH, Shared.HEIGHT1, Align.left);
		vegetation_intersection_value_label = new CustomLabel((int) (OptionsData.vegetation_intersection_level) + "",
				StyleContainer.label_medium_whitepure_20_style, Shared.WIDTH2, Shared.HEIGHT1, Align.center);
		vegetation_intersection_tooltip = new TooltipTextArea(TextData.getWord("vegetationIntersectionDetail"), Shared.WIDTH5);

		vegetation_intersection_question_mark = new CustomIconbutton(StaticGraphic.gui_icon_help,
				StaticGraphic.gui_icon_help.getWidth(), StaticGraphic.gui_icon_help.getHeight()) {
			@Override
			public float getPrefWidth() {
				return super.getPrefWidth() + 20;
			}
		};

		v_sync_label = new CustomLabel(TextData.getWord("vsync"), StyleContainer.label_medium_whitepure_20_style,
				LABEL_WIDTH, Shared.HEIGHT1, Align.left);
		v_sync_checkbox = new CustomCheckbox(checkbox_style);
		v_sync_checkbox.setChecked(OptionsData.v_sync_on);

		apply_btn = new CustomTextbutton(TextData.getWord("apply"), StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_btn_height1_width4, Align.center);

		restore_defaults_btn = new CustomTextbutton(TextData.getWord("restoreDefaults"),
				StyleContainer.button_bold_20_style, StaticGraphic.gui_bg_btn_height1_width4, Align.center);
		restore_defaults_btn.setSound(ClickSoundType.LIGHT);

	}

	private void clickAspectRatioButton() {
		switch (aspect_ratio) {
		case "4:3":
			GUIUtil.clickActor(aspect_ratio_button_4_to_3);
			break;
		case "16:9":
			GUIUtil.clickActor(aspect_ratio_button_16_to_9);
			break;
		case "16:10":
			GUIUtil.clickActor(aspect_ratio_button_16_to_10);
			break;
		}
	}

	@Override
	public void setLayout() {

		CustomTable btnsTable = new CustomTable();
		btnsTable.padLeft(600).padTop(500).setFillParent(true);
		tables.add(btnsTable);

		btnsTable.add(apply_btn).padRight(DISTANCE_BETWEEN_ACTORS * 0.5f);
		btnsTable.add(restore_defaults_btn);

		CustomTable table = new CustomTable();
		table.padBottom(100).setFillParent(true);
		tables.add(table);

		float preLabelWidth = vegetation_intersection_question_mark.getPrefWidth();
		table.columnDefaults(2).spaceLeft(DISTANCE_BETWEEN_ACTORS * 0.5f);
		table.add().width(preLabelWidth);
		table.add(fullscreen_label);
		table.add(fullscreen_checkbox).left();
		table.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);
		table.add().width(preLabelWidth);
		table.add(v_sync_label);
		table.add(v_sync_checkbox).left();
		table.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);
		table.add().width(preLabelWidth);
		table.add(aspect_ratio_label);
		CustomTable tableHorizontal = new CustomTable();
		tableHorizontal.add(aspect_ratio_radio_func.getButtons().get(0));
		tableHorizontal.add(four_to_3_label);
		tableHorizontal.add(aspect_ratio_radio_func.getButtons().get(1));
		tableHorizontal.add(sixteen_to_9_label);
		tableHorizontal.add(aspect_ratio_radio_func.getButtons().get(2));
		tableHorizontal.add(sixteen_to_10_label);
		table.add(tableHorizontal);
		table.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);
		table.add().width(preLabelWidth);
		table.add(resolution_label);

		// the following line is necessary to generate a cell
		resolution_cell = table.add(resolution_dropdown.getCurrent()).left();
		resolution_dropdown.addItems(tables, table, resolution_cell);

		table.row().padTop(LINE_DISTANCE_TO_NEXT_ROW * 0.5f);
		table.add(vegetation_intersection_question_mark).width(preLabelWidth);

		table.add(vegetation_intersection_label);
		HorizontalGroup group = new HorizontalGroup();
		group.addActor(vegetation_intersection_slider);
		group.addActor(vegetation_intersection_value_label);
		table.add(group).left().width(0);

		vegetation_intersection_tooltip.setPosition(tables, table, vegetation_intersection_question_mark);

	}

	@Override
	public void setListeners() {

		fullscreen_checkbox.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				clickAspectRatioButton();
			}
		});

		v_sync_checkbox.addListener(new PersistentClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				Gdx.graphics.setVSync(OptionsData.v_sync_on);
			}

			@Override
			public void setAttributes() {
				OptionsData.v_sync_on = v_sync_checkbox.isChecked();
			}
		});

		vegetation_intersection_slider.addListener(new PersistentChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				super.changed(event, actor);
				OptionsData.vegetation_intersection_level = (int) vegetation_intersection_slider.getVisualValue();
				vegetation_intersection_value_label.setText(OptionsData.vegetation_intersection_level + "");
			}

			@Override
			public void setAttributes() {
			}

			@Override
			public void save() {
				SettingsLoader.save();
			}
		});

		vegetation_intersection_question_mark.addListener(new TooltipListener(vegetation_intersection_tooltip));

		aspect_ratio_button_4_to_3.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				aspect_ratio = "4:3";

				// apply the "radio button" functionality
				aspect_ratio_radio_func.check(0);

				resolution_dropdown.setCurrent(aspect_ratio, fullscreen_checkbox.isChecked());
				resolution_cell.setActor(resolution_dropdown.getCurrent());
			}
		});

		aspect_ratio_button_16_to_9.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				aspect_ratio = "16:9";

				// apply the "radio button" functionality
				aspect_ratio_radio_func.check(1);

				resolution_dropdown.setCurrent(aspect_ratio, fullscreen_checkbox.isChecked());
				resolution_cell.setActor(resolution_dropdown.getCurrent());
			}
		});

		aspect_ratio_button_16_to_10.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				aspect_ratio = "16:10";

				// apply the "radio button" functionality
				aspect_ratio_radio_func.check(2);

				resolution_dropdown.setCurrent(aspect_ratio, fullscreen_checkbox.isChecked());
				resolution_cell.setActor(resolution_dropdown.getCurrent());
			}
		});
		apply_btn.addListener(new PersistentClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {

				Integer[] resolution = ResolutionDropdown.parseResolution(resolution_dropdown.getResolution());

				if (resolution != null) {

					// save changes
					super.clicked(event, x, y);

					int resoWidth = resolution[0];
					int resoHeight = resolution[1];

					if (OptionsData.fullscreen_on == false)
						Gdx.graphics.setWindowedMode(resoWidth, resoHeight);
					else {

						for (DisplayMode mode : Gdx.graphics.getDisplayModes())
							if (mode.width == resoWidth && mode.height == resoHeight)
								Gdx.graphics.setFullscreenMode(mode);

					}

					gui_tier.getViewport().update(resoWidth, resoHeight, true);
					
				}

			}

			@Override
			public void setAttributes() {
				OptionsData.fullscreen_on = fullscreen_checkbox.isChecked();
				OptionsData.resolution = resolution_dropdown.getResolution();
				OptionsData.aspect_ratio = aspect_ratio;
			}
		});

		restore_defaults_btn.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				// prepare for optimum
				fullscreen_checkbox.setChecked(true);

				// read optimal aspect ratio
				aspect_ratio = ResolutionDropdown.getAspectRatioString(
						((float) (Gdx.graphics.getDisplayMode().width)) / Gdx.graphics.getDisplayMode().height);

				clickAspectRatioButton();

				// select optimal resolution
				for (CustomTextbutton button : resolution_dropdown.getCurrent().getItems()) {
					Integer[] resolution = ResolutionDropdown.parseResolution(button.getText().toString());
					if (resolution != null)
						if (resolution[0] == Gdx.graphics.getDisplayMode().width
								&& resolution[1] == Gdx.graphics.getDisplayMode().height) {
							GUIUtil.clickActor(resolution_dropdown.getCurrent());
							GUIUtil.clickActor(button);
						}
				}

				// apply the optimum
				GUIUtil.clickActor(apply_btn);

			}
		});
	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		widgets.add(v_sync_checkbox);
		widgets.add(fullscreen_checkbox);
		widgets.add(vegetation_intersection_slider);
		widgets.add(vegetation_intersection_question_mark);

		for (CustomIconbutton radioButton : aspect_ratio_radio_func.getButtons())
			widgets.add(radioButton);

		resolution_dropdown.addToHoverhandler(widgets);

		widgets.add(apply_btn);
		widgets.add(restore_defaults_btn);

		return new HoverHandler(widgets);
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

	@Override
	public void unfocus() {
		super.unfocus();
		resolution_dropdown.unfocusWidget();

	}

}
