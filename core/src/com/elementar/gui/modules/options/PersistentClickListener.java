package com.elementar.gui.modules.options;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.elementar.data.container.SettingsLoader;

public abstract class PersistentClickListener extends ClickListener {
	@Override
	public void clicked(InputEvent event, float x, float y) {
		setAttributes();
		SettingsLoader.save();
	}

	public abstract void setAttributes();
}
