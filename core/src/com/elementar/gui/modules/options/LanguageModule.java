package com.elementar.gui.modules.options;

import java.util.ArrayList;

import com.elementar.gui.abstracts.AChildModule;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;

public class LanguageModule extends AChildModule {

	public LanguageModule(int drawOrder) {
		super(false, drawOrder);
	}

	@Override
	public void loadWidgets() {

	}

	@Override
	public void setLayout() {

	}

	@Override
	public void setListeners() {

	}

	@Override
	public HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return null;
	}

	@Override
	public void loadSubModules() {
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
	}

}
