package com.elementar.gui.util;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.elementar.data.container.KeysConfiguration;
import com.elementar.data.container.KeysConfiguration.WorldAction;
import com.elementar.gui.abstracts.ARootWorldModule;

public class CursorUICheckListener extends InputListener {

	private ARootWorldModule world_module;

	/**
	 * 
	 * @param worldModule,
	 *            might be <code>null</code> if the player's keys shouldn't be
	 *            released after clicking the widget
	 */
	public CursorUICheckListener(ARootWorldModule worldModule) {
		world_module = worldModule;
	}

	public CursorUICheckListener() {
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		if (world_module != null) {
			// one time action after entering module
			for (WorldAction action : WorldAction.values())
				world_module.getReleasedKeys().add(KeysConfiguration.getKeycode(action));

			world_module.releaseAllKeys();
		}

		return super.touchDown(event, x, y, pointer, button);
	}

	@Override
	public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
		/*
		 * for further information look at:
		 * https://stackoverflow.com/questions/36336111/libgdx-listener-enter-
		 * and-exit-fires-multiple-times
		 */
		if (pointer == -1)
			ARootWorldModule.CURSOR_UI_CHECK.setAtUIWidgets(true);
	}

	@Override
	public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
		if (pointer == -1)
			ARootWorldModule.CURSOR_UI_CHECK.setAtUIWidgets(false);
	}
}
