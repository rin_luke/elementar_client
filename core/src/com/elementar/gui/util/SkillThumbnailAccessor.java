package com.elementar.gui.util;

import aurelienribon.tweenengine.TweenAccessor;

public class SkillThumbnailAccessor implements TweenAccessor<SkillThumbnailMovement> {

	public static final int MOVEMENT = 1;
	
	@Override
	public int getValues(SkillThumbnailMovement target, int tweenType, float[] returnValues) {
		switch (tweenType) {
		case MOVEMENT:
			returnValues[0] = 0;
			return 1;
		default:
			return 0;
		}
	}

	@Override
	public void setValues(SkillThumbnailMovement target, int tweenType, float[] newValues) {
		switch (tweenType) {
		case MOVEMENT:
			target.setOffset(newValues[0]);
			break;
		}
	}

}