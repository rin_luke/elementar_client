package com.elementar.gui.util;

import com.elementar.gui.abstracts.ARootWorldModule;
import com.elementar.gui.modules.ingame.ESCModule;
import com.elementar.gui.modules.ingame.VictoryDefeatModule;
import com.elementar.gui.modules.roots.WorldTrainingRoot;

/**
 * Relevant if player is inside {@link ARootWorldModule}. Provides methods to
 * check whether the cursor operates currently at the UI or inside the world.
 * <p>
 * There are two cases for operating at the UI. First opening a new module which
 * covers the world, for example {@link ESCModule} or
 * {@link VictoryDefeatModule}. In this case all inputs by the player has no
 * effect. Second hovering over widgets on the same layer as the world. In this
 * case all inputs are valid expect the left mouse click. For example skill tree
 * button or character switch button in {@link WorldTrainingRoot}.
 * <p>
 * If the player switched from UI to world or vice versa, the cursor image has
 * to be changed. In case of modules all keys has to be released (see
 * {@link CursorUICheckListener}).
 * 
 * @author lukassongajlo
 *
 */
public class CursorUICheck {

	private boolean at_UI_modules;
	private boolean at_UI_widgets;

	public void setAtUIModules(boolean atUIModules) {
		at_UI_modules = atUIModules;
	}

	public void setAtUIWidgets(boolean atUIWidgets) {
		at_UI_widgets = atUIWidgets;
	}

	/**
	 * Returns whether the player operates on the UI in a module.
	 * 
	 * @return
	 */
	public boolean isAtUIModules() {
		return at_UI_modules;
	}

	/**
	 * Returns whether the player operates on the UI with a widget.
	 * 
	 * @return
	 */
	public boolean isAtUIWidgets() {
		return at_UI_widgets;
	}
}
