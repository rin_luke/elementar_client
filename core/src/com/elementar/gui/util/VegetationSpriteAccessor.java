package com.elementar.gui.util;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import aurelienribon.tweenengine.TweenAccessor;

/**
 * The tweening works with two sensors, one plant-side and one player-side. By
 * contact the plant gets the velocity information of the player, so we can
 * decide where the impact occured. Für die Berechnung des Vorzeichens gehen wir
 * immer von unrotierten sprites aus. Diese sind aber schon rotiert.Weil wir
 * eben von unrotierten sprites ausgehen, müssen wir den vektor entsprechend
 * zurück rotieren, in eine lage bei dem diese bedingungen vorherrschen. dann
 * können wir das vorzeichen berechnen. So we need to set the rotation angle of
 * the velocity firstly to the same angle as the sprite.
 * 
 * @author lukassongajlo
 * 
 */
public class VegetationSpriteAccessor implements TweenAccessor<VegetationSprite> {
	public static final int SKEW_X2_BEGIN = 1;
	public static final int SKEW_X3_BEGIN = 2;
	public static final int CORRECTION_AFTER_CONTACT = 3;
	/**
	 * CONTACT describes the situation when a player walk through a plant. First
	 * an amplitude occurs depending on the player velocity. After this
	 * amplitude the plant moves back with CORRECTION_AFTER_CONTACT.
	 */
	public static final int CONTACT = 4;

	@Override
	public int getValues(VegetationSprite target, int tweenType, float[] returnValues) {

		float sign;

		Vector2 normalizedVector = target.getNormalizedClientVelocityVector();
		
		if (normalizedVector != null)
			sign = normalizedVector.x > 0 ? 1f : -1f;
		else
			sign = 1f;
		
		switch (tweenType) {
		case SKEW_X2_BEGIN: // im moment left
		case SKEW_X3_BEGIN:
			returnValues[0] = -sign
					* GUIUtil.getIdleAmplitude(target.getFunctionality(), target.getSize());
			return 1;

		case CORRECTION_AFTER_CONTACT:
			float d1, d2;
			float[] vs = target.getVertices();

			Vector2 currentX2Y2AfterRunThrough = new Vector2(vs[Batch.X2], vs[Batch.Y2]),
					currentX3Y3AfterRunThrough = new Vector2(vs[Batch.X3], vs[Batch.Y3]);
			/*
			 * if sign < 0 the player comes from the right side, so the
			 * correction should be going to idle-right points
			 */
			if (sign < 0) {

				d1 = -sign * currentX2Y2AfterRunThrough.dst(target.getIdleRightPoint1());
				d2 = -sign * currentX3Y3AfterRunThrough.dst(target.getIdleRightPoint2());
			} else {
				d1 = -sign * currentX2Y2AfterRunThrough.dst(target.getIdleLeftPoint1());
				d2 = -sign * currentX3Y3AfterRunThrough.dst(target.getIdleLeftPoint2());
			}
			target.setLastInterpolationValue1(d1);
			target.setLastInterpolationValue2(d2);

			returnValues[0] = d1;
			returnValues[1] = d2;
			return 2;
		case CONTACT:
			float shiftAmount = sign
					* GUIUtil.getRunThroughAmplitude(target.getFunctionality(), target.getSize());
			returnValues[0] = shiftAmount;
			target.setLastInterpolationValue1(shiftAmount);
			return 1;
		}

		assert false;
		return -1;
	}

	@Override
	public void setValues(VegetationSprite target, int tweenType, float[] newValues) {
		float dxForX2, dyForY2, dxForX3, dyForY3;
		float[] vs = target.getVertices();
		switch (tweenType) {
		case SKEW_X2_BEGIN:

			dxForX2 = newValues[0] * MathUtils.cosDeg(target.getRotation());
			dyForY2 = newValues[0] * MathUtils.sinDeg(target.getRotation());
			vs[Batch.X2] = target.getX2Y2().x + dxForX2;
			vs[Batch.Y2] = target.getX2Y2().y + dyForY2;

			break;
		case SKEW_X3_BEGIN:

			dxForX3 = newValues[0] * MathUtils.cosDeg(target.getRotation());
			dyForY3 = newValues[0] * MathUtils.sinDeg(target.getRotation());
			vs[Batch.X3] = target.getX3Y3().x + dxForX3;
			vs[Batch.Y3] = target.getX3Y3().y + dyForY3;

			break;

		case CORRECTION_AFTER_CONTACT:
			dxForX2 = (target.getLastInterpolationValue1() - newValues[0])
					* MathUtils.cosDeg(target.getRotation());
			dyForY2 = (target.getLastInterpolationValue1() - newValues[0])
					* MathUtils.sinDeg(target.getRotation());
			dxForX3 = (target.getLastInterpolationValue2() - newValues[1])
					* MathUtils.cosDeg(target.getRotation());
			dyForY3 = (target.getLastInterpolationValue2() - newValues[1])
					* MathUtils.sinDeg(target.getRotation());
			vs[Batch.X2] += dxForX2;
			vs[Batch.Y2] += dyForY2;
			vs[Batch.X3] += dxForX3;
			vs[Batch.Y3] += dyForY3;

			target.setLastInterpolationValue1(newValues[0]);
			target.setLastInterpolationValue2(newValues[1]);
			break;

		case CONTACT:
			// Note dxForX2 is in that case also for X3 becuase the shift is
			// identical, same for dxForY2
			dxForX2 = (target.getLastInterpolationValue1() - newValues[0])
					* MathUtils.cosDeg(target.getRotation());
			dyForY2 = (target.getLastInterpolationValue1() - newValues[0])
					* MathUtils.sinDeg(target.getRotation());

			vs[Batch.X2] += dxForX2;
			vs[Batch.Y2] += dyForY2;
			vs[Batch.X3] += dxForX2;
			vs[Batch.Y3] += dyForY2;
			target.setLastInterpolationValue1(newValues[0]);
			break;
		}
	}

}
