package com.elementar.gui.util;

/**
 * Implementing class has to provide some fields to show a damage difference
 * bar.
 * 
 * @author lukassongajlo
 *
 */
public interface HasDamageDifference {

	/**
	 * 
	 * @return
	 */
	public DamageDiffbar getDamageDiffBar();

	/**
	 * for drawing the damage difference bar
	 * 
	 * @return
	 */
	public short getLastHealth();

	/**
	 * for drawing the damage difference bar
	 */
	public void setLastHealth(short lastHealth);

}
