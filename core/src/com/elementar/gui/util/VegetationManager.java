package com.elementar.gui.util;

import static com.elementar.logic.util.Shared.NEARLY_ZERO;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.elementar.data.container.StaticGraphic;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.Shared.CategoryFunctionality;
import com.elementar.logic.util.Shared.CategorySize;
import com.elementar.logic.util.lists.NotNullArrayList;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

public class VegetationManager {

	public static final float OVERLAPPING_VEGETATION_IN_PERCENT = .20f;

	private NotNullArrayList<VegetationSprite> vegetation_sprites;

	private TweenManager tween_manager;

	private float full_width, platform_length, taken_width;
	private int angle;

	private Vector2 offset_vector, position, platform_direction,
			platform_begin, platform_end;
	private CategoryFunctionality current_functionality;

	/**
	 * This data structure assigns each section on a platform a boolean value
	 * whether there is already a plant or not. Each vegetation sprite has the
	 * same width, so one entry in this list corresponds with one sprite with
	 * this width.
	 */
	private ArrayList<Boolean> plants_assignment;

	public VegetationManager(TweenManager tweenManager) {
		vegetation_sprites = new NotNullArrayList<VegetationSprite>();
		this.tween_manager = tweenManager;

	}

	public void update(float delta) {
		tween_manager.update(delta);
	}

	/**
	 * 
	 * @param globalVertices
	 */
	public void place(Vector2[] globalVertices) {
		place(globalVertices, -1);
	}

	/**
	 * Places vegetation sprites around the global vertices, except the platform
	 * defined by the passed index. Pass -1 for index if there should be no
	 * exception.
	 * 
	 * @param globalVertices
	 * @param index
	 */
	public void place(Vector2[] globalVertices, int index) {

		full_width = StaticGraphic.vegetation_width;
		taken_width = full_width * (1 - OVERLAPPING_VEGETATION_IN_PERCENT);
		for (int i = 1; i <= globalVertices.length; i++) {
			if (i - 1 != index) {
				platform_begin = globalVertices[i - 1];
				platform_end = null;
				if (i == globalVertices.length)
					platform_end = globalVertices[0];
				else
					platform_end = globalVertices[i];
				platform_direction = Util.subVectors(platform_end,
						platform_begin);
				platform_length = platform_direction.len();

				int numberPlants = (int) ((platform_length - full_width
						* OVERLAPPING_VEGETATION_IN_PERCENT) / taken_width);
				angle = Math.round(platform_direction.angle()); // save

				// define functionality
				current_functionality = Util.getPlatformFunctionality(angle);
				offset_vector = new Vector2(platform_direction.x,
						platform_direction.y);
				if (numberPlants == 0) {
					if (Math.random() < getOccurrenceProbalility()) {
						offset_vector.setLength(platform_length * 2
								* OVERLAPPING_VEGETATION_IN_PERCENT);
						position = Util.subVectors(platform_begin,
								offset_vector);

						vegetation_sprites
								.add(StaticGraphic
										.makeVegetationSprite(
												current_functionality,
												position,
												angle,
												platform_length
														+ 2
														* platform_length
														* OVERLAPPING_VEGETATION_IN_PERCENT));
					}
				} else {

					// neu: frage, wieviele vegetations sprites passen auf eine
					// platform?
					plants_assignment = new ArrayList<Boolean>(numberPlants);
					for (int j = 0; j < numberPlants; j++)
						plants_assignment.add(false);

					for (int k = 0; k < 3; k++) {
						switch (k) {
						case 0:
							// Medium
							placePlant(CategorySize.MEDIUM, 0.15f);
							break;
						case 1:
							// Smadium
							// placePlant(CategorySize.SMADIUM, 0.25f);
							break;
						case 2:
							// Small
							for (int j = 0; j < plants_assignment.size(); j++)
								plants_assignment.set(j, false);
							placePlant(CategorySize.SMALL,
									getOccurrenceProbalility());
							break;
						}

					}
					// Schlussteil:
					if (Math.random() < getOccurrenceProbalility()) {
						offset_vector.setLength(full_width
								* OVERLAPPING_VEGETATION_IN_PERCENT);
						position = Util.addVectors(platform_end, offset_vector);
						offset_vector.setLength(full_width);
						position = Util.subVectors(platform_end, offset_vector);

						vegetation_sprites
								.add(StaticGraphic.makeVegetationSprite(
										CategorySize.SMALL,
										current_functionality, position, angle));
					}

					plants_assignment.clear();
				}
			}
		}
	}

	/**
	 * Each sort of vegetation has a probability of occurrence for one section
	 * (each section has the width of one scaled sprite) in percent.
	 * 
	 * @param medium
	 * @param occurrenceProbalitiy
	 */
	private void placePlant(CategorySize size, float occurrenceProbalitiy) {
		offset_vector.setLength(NEARLY_ZERO);
		for (int i = 0; i < plants_assignment.size(); i++) {
			if (plants_assignment.get(i) == false)
				// when 'false' it wasn't be assigned
				if (Math.random() < occurrenceProbalitiy) {

					position = Util.addVectors(platform_begin, offset_vector);
					vegetation_sprites.add(StaticGraphic.makeVegetationSprite(
							size, current_functionality, position, angle));

					plants_assignment.set(i, true);
				}

			offset_vector.setLength(offset_vector.len() + taken_width);
		}
	}

	/**
	 * If sprite is still not tweening, then it will apply the start-animation
	 * once (useful in loops)
	 * 
	 * @param tweenManager
	 * @param sprite
	 */
	public static void applyStartAnimationOnce(TweenManager tweenManager,
			VegetationSprite sprite) {

		if (sprite.isTweening() == false) {
			applyStartAnimation(tweenManager, sprite);
			/*
			 * is_tweening is necessary for the update loop. Without that, the
			 * start-animation would be applied continuously.
			 */
			sprite.setTweening(true);
		}
	}

	/**
	 * Applies start-animation regardless of anything (see
	 * {@link #applyStartAnimationOnce(TweenManager, VegetationSprite)})
	 * 
	 * @param tweenManager
	 * @param sprite
	 */
	public static void applyStartAnimation(TweenManager tweenManager,
			VegetationSprite sprite) {

		float duration, swingAmplitude, delayInPercentOfDuration;

		duration = GUIUtil.getRndSwingDuration(sprite.getFunctionality(),
				sprite.getSize());
		swingAmplitude = GUIUtil.getIdleAmplitude(sprite.getFunctionality(),
				sprite.getSize());
		delayInPercentOfDuration = GUIUtil.getDelayInPercentOfDuration(
				sprite.getFunctionality(), sprite.getSize());

		// this is for case after correction (after a walk through)
		if (sprite.getNormalizedClientVelocityVector() != null)
			swingAmplitude *= sprite.getNormalizedClientVelocityVector().x > 0 ? 1
					: -1;

		// x2 startet früher als x3
		Tween.to(sprite, VegetationSpriteAccessor.SKEW_X2_BEGIN, duration)
				.target(swingAmplitude).ease(TweenEquations.easeInOutSine)
				.repeatYoyo(-1, 0).start(tweenManager);

		Tween.to(sprite, VegetationSpriteAccessor.SKEW_X3_BEGIN, duration)
				.target(swingAmplitude).ease(TweenEquations.easeInOutSine)
				.repeatYoyo(-1, 0).delay(duration * delayInPercentOfDuration)
				.start(tweenManager);

	}

	/**
	 * Returns the occurrenceProbalitiy for small sprites.
	 * 
	 * @return
	 */
	private float getOccurrenceProbalility() {
		if (current_functionality == CategoryFunctionality.NON_WALK)
			return 0f;
		else if (current_functionality == CategoryFunctionality.SLIDE)
			return 0.7f;
		// walkable = 1;
		return 0.9f;
	}

	public void draw(ShaderPolyBatch batch) {
		for (VegetationSprite vegetationSprite : vegetation_sprites)
			vegetationSprite.draw(batch);
	}

	public ArrayList<VegetationSprite> getSprites() {
		return vegetation_sprites;
	}

	public TweenManager getTweenManager() {
		return tween_manager;
	}
}
