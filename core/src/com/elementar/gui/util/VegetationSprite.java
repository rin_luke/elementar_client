package com.elementar.gui.util;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.characters.abstracts.APhysicalElement;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.Shared.CategoryFunctionality;
import com.elementar.logic.util.Shared.CategorySize;

/**
 * This sprite represents a vegetation sprite
 * 
 * @author lukassongajlo
 * 
 */
public class VegetationSprite extends Sprite {

	private Vector2 client_velocity_vector;
	private APhysicalElement physical_element;

	private CategoryFunctionality functionality;
	private CategorySize size;

	private boolean is_tweening;

	private Vector2 x2y2, x3y3, idle_right_point1, idle_right_point2, idle_left_point1, idle_left_point2;

	private float interpolation_value1, interpolation_value2;

	public VegetationSprite(Sprite sprite, CategoryFunctionality functionality, CategorySize size) {
		super(sprite);
		this.functionality = functionality;
		this.size = size;
		this.is_tweening = false;
		this.client_velocity_vector = null;// new Vector2(1, 0);
	}

	public void calcX2Y2AndX3Y3(float vegetationWidth, float vegetationHeight) {

		Vector2 temp = new Vector2();

		temp.set(vegetationHeight * MathUtils.cosDeg(getRotation() - 270),
				vegetationHeight * MathUtils.sinDeg(getRotation() - 270));
		x2y2 = new Vector2(getX(), getY()).add(temp);

		temp.set(vegetationWidth * MathUtils.cosDeg(getRotation()), vegetationWidth * MathUtils.sinDeg(getRotation()));
		x3y3 = Util.addVectors(x2y2, temp);

		// plant amplitude during idle animation
		float amplitude = GUIUtil.getIdleAmplitude(functionality, size);
		temp.set(amplitude * MathUtils.cosDeg(getRotation()), amplitude * MathUtils.sinDeg(getRotation()));
		idle_right_point1 = Util.addVectors(x2y2, temp);
		idle_right_point2 = Util.addVectors(x3y3, temp);
		idle_left_point1 = Util.subVectors(x2y2, temp);
		idle_left_point2 = Util.subVectors(x3y3, temp);

	}

	public void setClientVelocityVector(Vector2 vel) {
		this.client_velocity_vector = vel;
	}

	public Vector2 getClientVelocityVector() {
		return client_velocity_vector;
	}

	/**
	 * The impact from client velocity vector to the sprite is described in a
	 * case where the sprite isn't rotated. So when we use the non-normalized
	 * resp. non-rotated velocity vector and touch a non-walkable sprite (at the
	 * ceiling) we see that the contact behaviour is the wrong way out
	 * (mirror-inverted). That's because our decision is based on a non rotated
	 * image! But the image could be rotated, so we need to rotate the vector
	 * instead.
	 * 
	 * @return
	 */
	public Vector2 getNormalizedClientVelocityVector() {
		if (client_velocity_vector != null) {
			Vector2 v = new Vector2(client_velocity_vector);
			v.rotate(-getRotation());
			return v;
		}
		return null;
	}

	public Vector2 getX2Y2() {
		return x2y2;
	}

	public Vector2 getX3Y3() {
		return x3y3;
	}

	public CategoryFunctionality getFunctionality() {
		return functionality;
	}

	public CategorySize getSize() {
		return size;
	}

	public void setLastInterpolationValue1(float value) {
		this.interpolation_value1 = value;
	}

	public void setLastInterpolationValue2(float value) {
		this.interpolation_value2 = value;
	}

	public float getLastInterpolationValue1() {
		return this.interpolation_value1;
	}

	public float getLastInterpolationValue2() {
		return this.interpolation_value2;
	}

	public Vector2 getIdleRightPoint1() {
		return idle_right_point1;
	}

	public Vector2 getIdleRightPoint2() {
		return idle_right_point2;
	}

	public Vector2 getIdleLeftPoint1() {
		return idle_left_point1;
	}

	public Vector2 getIdleLeftPoint2() {
		return idle_left_point2;
	}

	public void setTweening(boolean tweening) {
		this.is_tweening = tweening;
	}

	/**
	 * is_tweening is necessary for the update loop. Without that, the
	 * start-animation would be applied continuously.
	 * 
	 * @return
	 */
	public boolean isTweening() {
		return is_tweening;
	}

	public void setPhysicalElement(APhysicalElement element) {
		this.physical_element = element;
	}

	/**
	 * plants could be outside of the viewport. So they should be inactive to
	 * improve performance.
	 * 
	 * @param active
	 */
	public void setActive(boolean active) {
		if (physical_element != null)
			physical_element.getBody().setActive(active);
	}

	public static boolean hasPhysicalBody(int vegetationInteractionLevel, CategorySize size) {
		boolean has = false;
		switch (vegetationInteractionLevel) {
		case 3:
			has = true;
			break;
		case 2:
			if (size != CategorySize.SMALL)
				has = true;
			break;
		case 1:
			if (size != CategorySize.SMALL && size != CategorySize.MEDIUM)
				has = true;
			break;

		}
		return has;
	}
}
