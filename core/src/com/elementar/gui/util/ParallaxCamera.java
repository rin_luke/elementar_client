package com.elementar.gui.util;

import static com.elementar.logic.util.Shared.WORLD_VIEWPORT_HEIGHT;
import static com.elementar.logic.util.Shared.WORLD_VIEWPORT_WIDTH;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.elementar.gui.abstracts.ARootWorldModule;

/**
 * Singleton
 * <p>
 * This camera is used as worldcam ({@link ARootWorldModule})
 * 
 * @author lukassongajlo
 * 
 */
public class ParallaxCamera extends OrthographicCamera {
	Matrix4 parallaxView = new Matrix4();
	Matrix4 parallaxCombined = new Matrix4();
	Vector3 tmp = new Vector3();
	Vector3 tmp2 = new Vector3();

	private static ParallaxCamera instance;

	public static synchronized ParallaxCamera getInstance() {
		if (instance == null)
			instance = new ParallaxCamera();

		return instance;
	}

	private ParallaxCamera() {
		super(WORLD_VIEWPORT_WIDTH, WORLD_VIEWPORT_HEIGHT);
	}

	public Matrix4 calculateParallaxMatrix(float parallaxX, float parallaxY) {
		update();
		tmp.set(position);

		tmp.x *= parallaxX;
		tmp.y *= parallaxY;

		parallaxView.setToLookAt(tmp, tmp2.set(tmp).add(direction), up);
		parallaxCombined.set(projection);
		Matrix4.mul(parallaxCombined.val, parallaxView.val);
		return parallaxCombined;
	}

}
