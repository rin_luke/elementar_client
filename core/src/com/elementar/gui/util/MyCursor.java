package com.elementar.gui.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import com.elementar.data.container.OptionsData;
import com.elementar.gui.GUITier;

public class MyCursor {

	private TextureRegion cursor_sprite;
	private float x, y;

	private TextureRegion cursor_sprite_regular, cursor_sprite_world;

	public MyCursor() {
		cursor_sprite_world = new TextureRegion(
				new Texture(Gdx.files.internal("graphic/gui_cursor_world_regular1.png")));
		cursor_sprite_regular = new TextureRegion(new Texture(Gdx.files.internal("graphic/gui_cursor1.png")));

		setSpriteRegular();

	}

	public void render(ShaderPolyBatch batch, Camera rootCam) {

		if (cursor_sprite == null)
			// cursor sprite = invisible
			return;

		// position cursor sprite
		if (rootCam != null) {

			int xCurrent = Gdx.input.getX(), yCurrent = Gdx.input.getY(), width = Gdx.graphics.getWidth(),
					height = Gdx.graphics.getHeight();

			if (OptionsData.fullscreen_on == true) {

				if (Gdx.input.isCursorCatched() == false)
					Gdx.input.setCursorCatched(true);

				boolean outside = false;
				if (xCurrent < 0) {
					xCurrent = 0;
					outside = true;
				}
				if (xCurrent > width) {
					xCurrent = width;
					outside = true;
				}
				if (yCurrent < 0) {
					yCurrent = 0;
					outside = true;
				}
				if (yCurrent > height) {
					yCurrent = height;
					outside = true;
				}

				if (outside == true)
					Gdx.input.setCursorPosition(xCurrent, yCurrent);

			} else {
				if (Gdx.input.isCursorCatched() == true) {
					Gdx.input.setCursorCatched(false);
				}
			}

//			System.out.println("MyCursor.render()			isCatched = " + Gdx.input.isCursorCatched());

			Vector3 temp = rootCam.unproject(new Vector3(xCurrent, yCurrent, 0));
			x = temp.x - cursor_sprite.getRegionWidth() * 0.5f;
			y = temp.y - cursor_sprite.getRegionHeight() * 0.5f;

		}

		// draw cursor sprite
		batch.begin();
		batch.draw(cursor_sprite, x, y);
		batch.end();

	}

	public void setSpriteRegular() {
		cursor_sprite = cursor_sprite_regular;
	}

	public void setSpriteWorld() {
		cursor_sprite = cursor_sprite_world;
	}

	public void setSpriteInvisible() {
		cursor_sprite = null;
	}

}
