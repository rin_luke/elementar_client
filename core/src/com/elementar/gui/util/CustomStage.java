package com.elementar.gui.util;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.elementar.gui.widgets.AContextMenu;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.TooltipTextArea;

/**
 * Extends {@link Stage}. Contains a second stage for overlapping widgets like
 * {@link TooltipTextArea} or {@link AContextMenu}.
 * 
 * @author lukassongajlo
 *
 */
public class CustomStage extends Stage {
	private Stage overlapping_stage;

	public CustomStage(Viewport viewport, Batch batch) {
		super(viewport, batch);
		overlapping_stage = new Stage(viewport, batch);
	}

	@Override
	public void clear() {
		super.clear();
		overlapping_stage.clear();
	}

	/**
	 * If an actor is an overlapping widget like a tooltip or context-menu then
	 * those widgets will be added to a separated stage (
	 * {@link #overlapping_stage} )
	 */
	@Override
	public void addActor(Actor actor) {
		CustomTable table = (CustomTable) actor;

		if (table.containsOverlapping() == true) {
			overlapping_stage.addActor(actor);
		} else {
			super.addActor(actor);
		}

	}

	public void drawOverlap() {
		overlapping_stage.draw();
	}

	@Override
	public void setDebugAll(boolean debugAll) {
		super.setDebugAll(debugAll);
		overlapping_stage.setDebugAll(debugAll);
	}
}
