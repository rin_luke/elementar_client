package com.elementar.gui.util;

import com.esotericsoftware.spine.Bone;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;

public class CustomSkeleton extends Skeleton {

	private float scale;

	public CustomSkeleton(SkeletonData data, float scale) {
		super(data);
		this.scale = scale;
	}

	public float getScale() {
		return scale;
	}

	public void setFlipX(boolean flip) {
		if (flip == true) {
			if (getScaleX() > 0)
				setScaleX(getScaleX() * -1);
		} else if (getScaleX() < 0)
			setScaleX(getScaleX() * -1);
	}

	public boolean getFlipX() {
		return getScaleX() < 0 ? true : false;
	}

	public static void setBoneFlipX(boolean flip, Bone bone) {
		if (flip == true) {
			if (bone.getScaleX() > 0)
				bone.setScaleX(bone.getScaleX() * -1);
		} else if (bone.getScaleX() < 0)
			bone.setScaleX(bone.getScaleX() * -1);
	}

	public static boolean getBoneFlipX(Bone bone) {
		return bone.getScaleX() < 0 ? true : false;
	}

}
