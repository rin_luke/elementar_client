package com.elementar.gui.util;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.codedisaster.steamworks.SteamAPI;
import com.codedisaster.steamworks.SteamAPICall;
import com.codedisaster.steamworks.SteamApps;
import com.codedisaster.steamworks.SteamAuth;
import com.codedisaster.steamworks.SteamAuthTicket;
import com.codedisaster.steamworks.SteamException;
import com.codedisaster.steamworks.SteamFriends;
import com.codedisaster.steamworks.SteamFriendsCallback;
import com.codedisaster.steamworks.SteamID;
import com.codedisaster.steamworks.SteamInventory;
import com.codedisaster.steamworks.SteamInventoryCallback;
import com.codedisaster.steamworks.SteamInventoryHandle;
import com.codedisaster.steamworks.SteamLeaderboardEntriesHandle;
import com.codedisaster.steamworks.SteamLeaderboardEntry;
import com.codedisaster.steamworks.SteamLeaderboardHandle;
import com.codedisaster.steamworks.SteamNativeIntHandle;
import com.codedisaster.steamworks.SteamPublishedFileID;
import com.codedisaster.steamworks.SteamRemoteStorage;
import com.codedisaster.steamworks.SteamRemoteStorageCallback;
import com.codedisaster.steamworks.SteamResult;
import com.codedisaster.steamworks.SteamUGC;
import com.codedisaster.steamworks.SteamUGCCallback;
import com.codedisaster.steamworks.SteamUGCDetails;
import com.codedisaster.steamworks.SteamUGCHandle;
import com.codedisaster.steamworks.SteamUGCQuery;
import com.codedisaster.steamworks.SteamUser;
import com.codedisaster.steamworks.SteamUserCallback;
import com.codedisaster.steamworks.SteamUserStats;
import com.codedisaster.steamworks.SteamUserStatsCallback;
import com.codedisaster.steamworks.SteamUtils;
import com.codedisaster.steamworks.SteamUtilsCallback;
import com.elementar.data.container.GameclassSkinContainer;
import com.elementar.data.container.PlayerData;
import com.elementar.data.container.SettingsLoader;
import com.elementar.data.container.util.GameclassSkin;
import com.elementar.data.container.util.GameclassSkin.SkinAvailability;
import com.elementar.gui.modules.selection.GameclassCollectionModule;
import com.elementar.gui.widgets.GameclassIconbutton;

public class SteamInterface {

	public SteamUser user;
	public SteamUserStats userStats;
	public SteamRemoteStorage remoteStorage;
	public SteamUGC ugc;
	public SteamUtils utils;
	public SteamApps apps;
	public SteamFriends friends;
	public SteamInventory inventory;

	public SteamLeaderboardHandle currentLeaderboard = null;

	public boolean purchase_has_started = false;
	public boolean purchase_successful = false;

	public SteamInterface() {

		try {

			SteamAPI.loadLibraries();
			/*
			 * if the following returns true, the game was started outside of the Steam
			 * client.
			 */
			SteamAPI.restartAppIfNecessary(1117130);

			if (SteamAPI.init() == false) {
				SteamAPI.printDebugInfo(System.err);
				System.exit(0);
			}

			SteamAPI.printDebugInfo(System.out);

			registerInterfaces();

		} catch (SteamException e) {
			e.printStackTrace();
		}

	}

	private SteamUserCallback userCallback = new SteamUserCallback() {

		public void onAuthSessionTicket(SteamAuthTicket authTicket, SteamResult result) {

		}

		@Override
		public void onValidateAuthTicket(SteamID steamID, SteamAuth.AuthSessionResponse authSessionResponse,
				SteamID ownerSteamID) {

		}

		@Override
		public void onMicroTxnAuthorization(int appID, long orderID, boolean authorized) {

		}

		@Override
		public void onEncryptedAppTicket(SteamResult result) {

		}
	};

	private SteamUserStatsCallback userStatsCallback = new SteamUserStatsCallback() {
		@Override
		public void onUserStatsReceived(long gameId, SteamID steamIDUser, SteamResult result) {
			System.out.println("User stats received: gameId=" + gameId + ", userId=" + steamIDUser.getAccountID()
					+ ", result=" + result.toString());

			int numAchievements = userStats.getNumAchievements();
			System.out.println("Num of achievements: " + numAchievements);

			for (int i = 0; i < numAchievements; i++) {
				String name = userStats.getAchievementName(i);
				boolean achieved = userStats.isAchieved(name, false);
				System.out.println("# " + i + " : name=" + name + ", achieved=" + (achieved ? "yes" : "no"));
			}
		}

		@Override
		public void onUserStatsStored(long gameId, SteamResult result) {
			System.out.println("User stats stored: gameId=" + gameId + ", result=" + result.toString());
		}

		@Override
		public void onUserStatsUnloaded(SteamID steamIDUser) {
			System.out.println("User stats unloaded: userId=" + steamIDUser.getAccountID());
		}

		@Override
		public void onUserAchievementStored(long gameId, boolean isGroupAchievement, String achievementName,
				int curProgress, int maxProgress) {
			System.out.println("User achievement stored: gameId=" + gameId + ", name=" + achievementName + ", progress="
					+ curProgress + "/" + maxProgress);
		}

		@Override
		public void onLeaderboardFindResult(SteamLeaderboardHandle leaderboard, boolean found) {
			System.out.println(
					"Leaderboard find result: handle=" + leaderboard.toString() + ", found=" + (found ? "yes" : "no"));

			if (found) {
				System.out.println("Leaderboard: name=" + userStats.getLeaderboardName(leaderboard) + ", entries="
						+ userStats.getLeaderboardEntryCount(leaderboard));

				currentLeaderboard = leaderboard;
			}
		}

		@Override
		public void onLeaderboardScoresDownloaded(SteamLeaderboardHandle leaderboard,
				SteamLeaderboardEntriesHandle entries, int numEntries) {

			System.out.println("Leaderboard scores downloaded: handle=" + leaderboard.toString() + ", entries="
					+ entries.toString() + ", count=" + numEntries);

			int[] details = new int[16];

			for (int i = 0; i < numEntries; i++) {

				SteamLeaderboardEntry entry = new SteamLeaderboardEntry();
				if (userStats.getDownloadedLeaderboardEntry(entries, i, entry, details)) {

					int numDetails = entry.getNumDetails();

					System.out.println("Leaderboard entry #" + i + ": accountID="
							+ entry.getSteamIDUser().getAccountID() + ", globalRank=" + entry.getGlobalRank()
							+ ", score=" + entry.getScore() + ", numDetails=" + numDetails);

					for (int detail = 0; detail < numDetails; detail++) {
						System.out.println("  ... detail #" + detail + "=" + details[detail]);
					}

					if (friends.requestUserInformation(entry.getSteamIDUser(), false)) {
						System.out.println("  ... requested user information for entry");
					} else {
						System.out.println(
								"  ... user name is '" + friends.getFriendPersonaName(entry.getSteamIDUser()) + "'");

						int smallAvatar = friends.getSmallFriendAvatar(entry.getSteamIDUser());
						if (smallAvatar != 0) {
							int w = utils.getImageWidth(smallAvatar);
							int h = utils.getImageHeight(smallAvatar);
							System.out.println("  ... small avatar size: " + w + "x" + h + " pixels");

							ByteBuffer image = ByteBuffer.allocateDirect(w * h * 4);

							try {
								if (utils.getImageRGBA(smallAvatar, image)) {
									System.out.println("  ... small avatar retrieve avatar image successful");

									int nonZeroes = w * h;
									for (int y = 0; y < h; y++) {
										for (int x = 0; x < w; x++) {
											// System.out.print(String.format(" %08x", image.getInt(y * w + x)));
											if (image.getInt(y * w + x) == 0) {
												nonZeroes--;
											}
										}
										// System.out.println();
									}

									if (nonZeroes == 0) {
										System.err.println("Something's wrong! Avatar image is empty!");
									}

								} else {
									System.out.println("  ... small avatar retrieve avatar image failed!");
								}
							} catch (SteamException e) {
								e.printStackTrace();
							}
						} else {
							System.out.println("  ... small avatar image not available!");
						}

					}
				}

			}
		}

		@Override
		public void onLeaderboardScoreUploaded(boolean success, SteamLeaderboardHandle leaderboard, int score,
				boolean scoreChanged, int globalRankNew, int globalRankPrevious) {

			System.out.println("Leaderboard score uploaded: " + (success ? "yes" : "no") + ", handle="
					+ leaderboard.toString() + ", score=" + score + ", changed=" + (scoreChanged ? "yes" : "no")
					+ ", globalRankNew=" + globalRankNew + ", globalRankPrevious=" + globalRankPrevious);
		}

		@Override
		public void onGlobalStatsReceived(long gameId, SteamResult result) {
			System.out.println("Global stats received: gameId=" + gameId + ", result=" + result.toString());
		}
	};

	private SteamRemoteStorageCallback remoteStorageCallback = new SteamRemoteStorageCallback() {
		@Override
		public void onFileWriteAsyncComplete(SteamResult result) {

		}

		@Override
		public void onFileReadAsyncComplete(SteamAPICall fileReadAsync, SteamResult result, int offset, int read) {

		}

		@Override
		public void onFileShareResult(SteamUGCHandle fileHandle, String fileName, SteamResult result) {
			System.out.println("Remote storage file share result: handle='" + fileHandle.toString() + ", name="
					+ fileName + "', result=" + result.toString());
		}

		@Override
		public void onDownloadUGCResult(SteamUGCHandle fileHandle, SteamResult result) {
			System.out.println("Remote storage download UGC result: handle='" + fileHandle.toString() + "', result="
					+ result.toString());

			ByteBuffer buffer = ByteBuffer.allocateDirect(1024);
			int offset = 0, bytesRead;

			do {
				bytesRead = remoteStorage.ugcRead(fileHandle, buffer, buffer.limit(), offset,
						SteamRemoteStorage.UGCReadAction.ContinueReadingUntilFinished);
				offset += bytesRead;
			} while (bytesRead > 0);

			System.out.println("Read " + offset + " bytes from handle=" + fileHandle.toString());
		}

		@Override
		public void onPublishFileResult(SteamPublishedFileID publishedFileID, boolean needsToAcceptWLA,
				SteamResult result) {
			System.out.println("Remote storage publish file result: publishedFileID=" + publishedFileID.toString()
					+ ", needsToAcceptWLA=" + needsToAcceptWLA + ", result=" + result.toString());
		}

		@Override
		public void onUpdatePublishedFileResult(SteamPublishedFileID publishedFileID, boolean needsToAcceptWLA,
				SteamResult result) {
			System.out.println(
					"Remote storage update published file result: publishedFileID=" + publishedFileID.toString()
							+ ", needsToAcceptWLA=" + needsToAcceptWLA + ", result=" + result.toString());
		}

		@Override
		public void onPublishedFileSubscribed(SteamPublishedFileID publishedFileID, int appID) {

		}

		@Override
		public void onPublishedFileUnsubscribed(SteamPublishedFileID publishedFileID, int appID) {

		}

		@Override
		public void onPublishedFileDeleted(SteamPublishedFileID publishedFileID, int appID) {

		}
	};

	private SteamUGCCallback ugcCallback = new SteamUGCCallback() {
		@Override
		public void onUGCQueryCompleted(SteamUGCQuery query, int numResultsReturned, int totalMatchingResults,
				boolean isCachedData, SteamResult result) {
			System.out.println("UGC query completed: handle=" + query.toString() + ", " + numResultsReturned + " of "
					+ totalMatchingResults + " results returned, result=" + result.toString());

			for (int i = 0; i < numResultsReturned; i++) {
				SteamUGCDetails details = new SteamUGCDetails();
				ugc.getQueryUGCResult(query, i, details);
				printUGCDetails("UGC details #" + i, details);
			}

			ugc.releaseQueryUserUGCRequest(query);
		}

		@Override
		public void onSubscribeItem(SteamPublishedFileID publishedFileID, SteamResult result) {
			System.out.println("Subscribe item result: publishedFileID=" + publishedFileID + ", result=" + result);
		}

		@Override
		public void onUnsubscribeItem(SteamPublishedFileID publishedFileID, SteamResult result) {
			System.out.println("Unsubscribe item result: publishedFileID=" + publishedFileID + ", result=" + result);
		}

		@Override
		public void onRequestUGCDetails(SteamUGCDetails details, SteamResult result) {
			System.out.println("Request details result: result=" + result);
			printUGCDetails("UGC details ", details);
		}

		@Override
		public void onCreateItem(SteamPublishedFileID publishedFileID, boolean needsToAcceptWLA, SteamResult result) {

		}

		@Override
		public void onSubmitItemUpdate(SteamPublishedFileID publishedFileID, boolean needsToAcceptWLA,
				SteamResult result) {

		}

		@Override
		public void onDownloadItemResult(int appID, SteamPublishedFileID publishedFileID, SteamResult result) {

		}

		@Override
		public void onUserFavoriteItemsListChanged(SteamPublishedFileID publishedFileID, boolean wasAddRequest,
				SteamResult result) {

		}

		@Override
		public void onSetUserItemVote(SteamPublishedFileID publishedFileID, boolean voteUp, SteamResult result) {

		}

		@Override
		public void onGetUserItemVote(SteamPublishedFileID publishedFileID, boolean votedUp, boolean votedDown,
				boolean voteSkipped, SteamResult result) {

		}

		private void printUGCDetails(String prefix, SteamUGCDetails details) {
			System.out.println(prefix + ": publishedFileID=" + details.getPublishedFileID().toString() + ", result="
					+ details.getResult().name() + ", type=" + details.getFileType().name() + ", title='"
					+ details.getTitle() + "'" + ", description='" + details.getDescription() + "'" + ", tags='"
					+ details.getTags() + "'" + ", fileName=" + details.getFileName() + ", fileHandle="
					+ details.getFileHandle().toString() + ", previewFileHandle="
					+ details.getPreviewFileHandle().toString() + ", url=" + details.getURL());
		}

		@Override
		public void onStartPlaytimeTracking(SteamResult result) {

		}

		@Override
		public void onStopPlaytimeTracking(SteamResult result) {

		}

		@Override
		public void onStopPlaytimeTrackingForAllItems(SteamResult result) {

		}

		@Override
		public void onDeleteItem(SteamPublishedFileID publishedFileID, SteamResult result) {

		}
	};

	private SteamFriendsCallback friendsCallback = new SteamFriendsCallback() {
		@Override
		public void onSetPersonaNameResponse(boolean success, boolean localSuccess, SteamResult result) {

		}

		@Override
		public void onPersonaStateChange(SteamID steamID, SteamFriends.PersonaChange change) {

			switch (change) {

			case Name:
				System.out.println("Persona name received: " + "accountID=" + steamID.getAccountID() + ", name='"
						+ friends.getFriendPersonaName(steamID) + "'");
				break;

			default:
				System.out.println("Persona state changed (unhandled): " + "accountID=" + steamID.getAccountID()
						+ ", change=" + change.name());
				break;
			}
		}

		@Override
		public void onGameOverlayActivated(boolean active) {

		}

		@Override
		public void onGameLobbyJoinRequested(SteamID steamIDLobby, SteamID steamIDFriend) {

		}

		@Override
		public void onAvatarImageLoaded(SteamID steamID, int image, int width, int height) {

		}

		@Override
		public void onFriendRichPresenceUpdate(SteamID steamIDFriend, int appID) {

		}

		@Override
		public void onGameRichPresenceJoinRequested(SteamID steamIDFriend, String connect) {

		}

		@Override
		public void onGameServerChangeRequested(String server, String password) {

		}
	};

	private SteamUtilsCallback utilsCallback = new SteamUtilsCallback() {
		@Override
		public void onSteamShutdown() {
			System.out.println("Steam client wants to shut down!");
		}
	};

	private SteamInventoryCallback inventoryCallback = new SteamInventoryCallback() {

		@Override
		public void onSteamInventoryResultReady(SteamInventoryHandle inventoryHandle, SteamResult result) {
			SteamNativeIntHandle.getNativeHandle(inventoryHandle);
			System.out.println(
					"Inventory Result ready: " + result + ", " + SteamNativeIntHandle.getNativeHandle(inventoryHandle));
			System.out.println(inventory.getResultStatus(inventoryHandle) + ": result of getResultStatus");

			if (purchase_has_started == true) {
				final List<SteamInventoryHandle> inventories = new ArrayList<>();
				inventory.getAllItems(inventories);
				SteamNativeIntHandle.getNativeHandle(inventories.get(0));
			}
		}

		@Override
		public void onSteamInventoryDefinitionUpdate() {
			System.out.println("Inventory definition update");

			final List<Integer> itemDefs = new ArrayList<>();

			System.out.println(inventory.getItemDefinitionIDs(itemDefs) + ": result of getItemDefinitionIDs, itemDefs "
					+ itemDefs);

			GameclassSkinContainer.loadSkins(itemDefs, inventory);

			// update layout of skin sub-section
			for (GameclassIconbutton button : GameclassCollectionModule.GAMECLASS_BUTTONS)
				button.updateSubSection();

		}

		@Override
		public void onSteamInventoryFullUpdate(SteamInventoryHandle inventoryHandle) {
			System.out.println("Inventory full update");
			final List<SteamInventory.SteamItemDetails> itemDetails = new ArrayList<>();
			System.out.println(inventory.getResultStatus(inventoryHandle) + ": result of getResultStatus");
			System.out.println(inventory.getResultItems(inventoryHandle, itemDetails) + ": result of getResultItems");

			for (int i = 0; i < inventory.getResultItemsLength(inventoryHandle); i++) {

				final List<String> values = new ArrayList<>();

				System.out.println(inventory.getResultItemProperty(inventoryHandle, i, "itemdefid", values)
						+ ": result of getResultItemProperty for " + "itemdefid" + ": " + values.get(0));

				for (GameclassSkin skin : GameclassSkinContainer.SKINS.values()) {

					// find corresponding skin
					if (skin.getItemDefID() == Integer.valueOf(values.get(0))) {

						if (purchase_has_started == true) {

							// purchase was successful
							if (skin == GameclassIconbutton.LAST_PURCHASED_SKIN) {

								purchase_has_started = false;
								purchase_successful = true;

							}
						}

						skin.setAvailability(SkinAvailability.AVAILABLE);

					}
				}
			}

			/*
			 * handle equipped flag
			 */
			for (GameclassIconbutton button : GameclassCollectionModule.GAMECLASS_BUTTONS) {

				ArrayList<GameclassSkin> skins = button.getSkins();

				// reset equipped flag
				for (GameclassSkin skin : skins) {
					skin.setEquipped(false);
				}

				boolean hasEquippedSkin = false;
				for (GameclassSkin skin : skins) {

					boolean equipped = SettingsLoader.loadSkinEquipped(skin.getTechnicalDescription())
							&& skin.getAvailability() == SkinAvailability.AVAILABLE;
					skin.setEquipped(equipped);

					if (equipped == true)
						hasEquippedSkin = true;

				}
				/*
				 * if no skin is marked as 'equipped' so we have to set the first skin as
				 * 'equipped'
				 */
				if (hasEquippedSkin == false)
					skins.get(0).setEquipped(true);

			}

			inventory.requestPrices();

		}

		@Override
		public void onSteamInventoryEligiblePromoItemDefIDs(SteamResult result, SteamID steamID,
				int eligiblePromoItemDefs, boolean cachedData) {
			System.out.println(result + " Inventory Eligible Promo Items for user: " + steamID.getAccountID()
					+ ", Count: " + eligiblePromoItemDefs + ", cached: " + cachedData);
			final List<Integer> eligiblePromoItemDefIDs = new ArrayList<>();
			System.out.println(inventory.getEligiblePromoItemDefinitionIDs(user.getSteamID(), eligiblePromoItemDefIDs,
					eligiblePromoItemDefs) + ": result of getEligiblePromoItemDefinitionIDs, itemIds: "
					+ eligiblePromoItemDefs);
		}

		@Override
		public void onSteamInventoryStartPurchaseResult(SteamResult result, long orderID, long transactionID) {

			if (result == SteamResult.OK) {
				// purchase successful
				purchase_has_started = true;
			}

			System.out.println(
					result + " Inventory Start Purchase, OrderID: " + orderID + ", transactionID: " + transactionID);
		}

		@Override
		public void onSteamInventoryRequestPricesResult(SteamResult result, String currency) {

			System.out.println(result + " Inventory Request Prices: " + currency);

			int size = inventory.getNumItemsWithPrices();
			int[] itemDefs = new int[size];
			long[] itemCurrentPrices = new long[size];
			long[] itemBasePrices = new long[size];
			Arrays.fill(itemDefs, -1);
			Arrays.fill(itemCurrentPrices, -1);
			Arrays.fill(itemBasePrices, -1);
			System.out.println(inventory.getItemsWithPrices(itemDefs, itemCurrentPrices, itemBasePrices)
					+ ": result of getNumItemsWithPrices");

			System.out.println(Arrays.toString(itemDefs) + ": itemDefs");
			System.out.println(Arrays.toString(itemCurrentPrices) + ": itemCurrentPrices");
			System.out.println(Arrays.toString(itemBasePrices) + ": itemBasePrices");

			for (int i = 0; i < itemDefs.length; i++) {

				int itemDefID = itemDefs[i];
				int itemPrice = (int) itemCurrentPrices[i];

				GameclassSkin skin = GameclassSkinContainer.SKINS.get(itemDefID);

				if (skin == null)
					continue;

				skin.setPrice(currency, itemPrice);

			}

		}

	};

	protected void registerInterfaces() {

		System.out.println("Register user ...");
		user = new SteamUser(userCallback);

		System.out.println("Register user stats callback ...");
		userStats = new SteamUserStats(userStatsCallback);

		System.out.println("Register remote storage ...");
		remoteStorage = new SteamRemoteStorage(remoteStorageCallback);

		System.out.println("Register UGC ...");
		ugc = new SteamUGC(ugcCallback);

		System.out.println("Register Utils ...");
		utils = new SteamUtils(utilsCallback);

		System.out.println("Register Apps ...");
		apps = new SteamApps();

		System.out.println("Register Friends ...");
		friends = new SteamFriends(friendsCallback);

		System.out.println("Local user account ID: " + user.getSteamID().getAccountID());
		System.out.println("Local user steam ID: " + SteamID.getNativeHandle(user.getSteamID()));
		System.out.println("Local user friends name: " + friends.getPersonaName());

		PlayerData.name = friends.getPersonaName();
		if(PlayerData.name.length() > 12) 
			PlayerData.name = PlayerData.name.substring(0,12)+"...";

		System.out.println("App ID: " + utils.getAppID());

		System.out.println("App build ID: " + apps.getAppBuildId());
		System.out.println("App owner: " + apps.getAppOwner().getAccountID());

		System.out.println("Current game language: " + apps.getCurrentGameLanguage());
		System.out.println("Available game languages: " + apps.getAvailableGameLanguages());

		System.out.println("Register Inventory ...");

		inventory = new SteamInventory(inventoryCallback);

	}

	public void loadItems() {

		final List<SteamInventoryHandle> inventories = new ArrayList<>();
		System.out.println(inventory.getAllItems(inventories) + ": result of getAllItems, Handle: "
				+ SteamNativeIntHandle.getNativeHandle(inventories.get(0)));
	}

}
