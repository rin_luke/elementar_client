package com.elementar.gui.util;

import aurelienribon.tweenengine.TweenAccessor;

public class WorldCamAccessor implements TweenAccessor<ParallaxCamera> {

	public static final int TO_CORE_EXPLOSION = 1;

	@Override
	public int getValues(ParallaxCamera cam, int tweenType, float[] returnValues) {
		switch (tweenType) {
		case TO_CORE_EXPLOSION:
			returnValues[0] = cam.position.x;
			returnValues[1] = cam.position.y;
			returnValues[2] = cam.zoom;
			return 3;
		}

		assert false;
		return -1;
	}

	@Override
	public void setValues(ParallaxCamera cam, int tweenType, float[] newValues) {
		switch (tweenType) {
		case TO_CORE_EXPLOSION:
			cam.position.x = newValues[0];
			cam.position.y = newValues[1];
			cam.zoom = newValues[2];
			break;
		}
	}

}
