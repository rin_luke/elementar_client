package com.elementar.gui.util;

import com.badlogic.gdx.graphics.Color;
import com.elementar.logic.util.Shared;

public class SharedColors {

	public static Color GREY = new Color(16 / 255f, 22 / 255f, 22 / 255f, 1f);

	/**
	 * health & mana bar background
	 */
	public static Color BLACK = new Color(7 / 255f, 12 / 255f, 12 / 255f, 1f);
	public static Color BLACK_DIMMED = new Color(7 / 255f, 12 / 255f, 12 / 255f, 0.7f);

	// BLUE

	public static Color BLUE_1 = new Color(74 / 255f, 238 / 255f, 255f / 255f, 1f);
	/**
	 * 
	 * mana bar
	 */
	public static Color BLUE_2 = new Color(28 / 255f, 211 / 255f, 211f / 255f, 1f);
	/**
	 * receive mana
	 */
	public static Color BLUE_3 = new Color(46 / 255f, 175 / 255f, 181f / 255f, 1f);
	public static Color BLUE_4 = new Color(53 / 255f, 122 / 255f, 122f / 255f, 1f);
	public static Color BLUE_5 = new Color(36 / 255f, 38 / 255f, 91f / 255f, 1f);
	public static Color BLUE_6 = new Color(28 / 255f, 49 / 255f, 61f / 255f, 1f);
	public static Color BLUE_7 = new Color(14 / 255f, 23 / 255f, 34f / 255f, 1f);
	public static Color BLUE_8 = new Color(8 / 255f, 16 / 255f, 22f / 255f, 1f);

	// IVORY

	/**
	 * highlighted font
	 */
	public static Color IVORY_1 = new Color(255 / 255f, 249 / 255f, 222f / 255f, 1f);
	/**
	 * primary font
	 */
	public static Color IVORY_2 = new Color(224 / 255f, 212 / 255f, 184f / 255f, 1f);
	public static Color IVORY_2_DIMMED = new Color(224 / 255f, 212 / 255f, 184f / 255f, 1f);//Shared.ALPHA_HOVER);
	public static Color IVORY_2_DISABLED = new Color(224 / 255f, 212 / 255f, 184f / 255f, Shared.ALPHA_DISABLED);

	/**
	 * deal dmg, secondary font
	 */
	public static Color IVORY_3 = new Color(163 / 255f, 146 / 255f, 130f / 255f, 1f);
	public static Color IVORY_4 = new Color(109 / 255f, 102 / 255f, 94f / 255f, 1f);
	public static Color IVORY_5 = new Color(66 / 255f, 64 / 255f, 58f / 255f, 1f);
	public static Color IVORY_6 = new Color(45 / 255f, 43 / 255f, 39f / 255f, 1f);
	public static Color IVORY_7 = new Color(22 / 255f, 25 / 255f, 25f / 255f, 1f);

	// GREEN

	public static Color GREEN_1 = new Color(214 / 255f, 242 / 255f, 97 / 255f, 1f);
	/**
	 * health allies
	 */
	public static Color GREEN_2 = new Color(176 / 255f, 196 / 255f, 91 / 255f, 1f);
	/**
	 * receive health, buff frames
	 */
	public static Color GREEN_3 = new Color(124 / 255f, 153 / 255f, 79 / 255f, 1f);

	public static Color GREEN_4 = new Color(62 / 255f, 96 / 255f, 62 / 255f, 1f);
	public static Color GREEN_5 = new Color(30 / 255f, 58 / 255f, 43 / 255f, 1f);
	public static Color GREEN_6 = new Color(21 / 255f, 38 / 255f, 35 / 255f, 1f);
	public static Color GREEN_7 = new Color(15 / 255f, 27 / 255f, 25 / 255f, 1f);

	/**
	 * warning notification
	 */
	public static Color RED_1 = new Color(255 / 255f, 113 / 255f, 113 / 255f, 1f);
	/**
	 * health enemies
	 */
	public static Color RED_2 = new Color(249 / 255f, 61 / 255f, 97 / 255f, 1f);
	/**
	 * receive damage, debuff frames
	 */
	public static Color RED_3 = new Color(209 / 255f, 40 / 255f, 73 / 255f, 1f);
	public static Color RED_4 = new Color(153 / 255f, 31 / 255f, 60 / 255f, 1f);
	public static Color RED_5 = new Color(94 / 255f, 28 / 255f, 46 / 255f, 1f);
	public static Color RED_6 = new Color(38 / 255f, 20 / 255f, 29 / 255f, 1f);

}
