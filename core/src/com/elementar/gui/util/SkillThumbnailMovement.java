package com.elementar.gui.util;

/**
 * This class is needed to show an up going movement of the skill thumbnail
 * after an absorption. Furthermore there is an alpha transition, fade-in at the
 * beginning and fade-out at the end
 * 
 * @author lukassongajlo
 *
 */
public class SkillThumbnailMovement {

	private float offset;

	public void setOffset(float offset) {
		this.offset = offset;
	}

	public float getOffset() {
		return offset;
	}

}
