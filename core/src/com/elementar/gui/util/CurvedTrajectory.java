package com.elementar.gui.util;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.emission.IEmitter;
import com.elementar.logic.util.Util;

public class CurvedTrajectory {

	public Vector2 player_pos, B;

	public IEmitter target;

	public float w1, w2;

	public CurvedTrajectory(Vector2 playerPos, IEmitter target) {
		this.player_pos = playerPos;
		this.target = target;

		Vector2 mid = Util.addVectors(playerPos,
				Util.subVectors(target.getPos(), playerPos).scl(MathUtils.random(0.3f, 0.7f)));
		Vector2 vertical = new Vector2(0, MathUtils.random(1f, 2f))
				.setAngleDeg(mid.angleDeg() + (MathUtils.randomBoolean() ? 90 : -90));
		B = Util.addVectors(mid, vertical);

	}

	public Vector2 calcVel(Vector2 posProjectile) {

		Vector2 CB = Util.subVectors(target.getPos(), player_pos);
		CB.scl(w1);

		// find B
		Vector2 BC = Util.subVectors(B, player_pos);
		BC.scl(w2);

		// barycentric coords: trajPos = A + w1*B + w2*C
		Vector2 nextTrajPos = Util.addVectors(player_pos, Util.addVectors(CB, BC));

		// new velocity
		return Util.subVectors(nextTrajPos, posProjectile);

	}

}
