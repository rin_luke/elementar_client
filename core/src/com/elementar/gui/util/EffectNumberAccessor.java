package com.elementar.gui.util;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.elementar.data.container.FontData;
import com.elementar.logic.emission.effect.AEffectWithNumber;

import aurelienribon.tweenengine.TweenAccessor;

public class EffectNumberAccessor implements TweenAccessor<AEffectWithNumber> {

	public static final int RECEIVE_HEALING = 0, RECEIVE_DAMAGE = 1, DEAL_DAMAGE = 2, RECEIVE_MANA = 3;

	@Override
	public int getValues(AEffectWithNumber target, int tweenType, float[] returnValues) {
		returnValues[0] = 0;
		target.setTweenedPos(new Vector2());

		float targetX = target.getTweenTargetPos().x;
		float targetY = target.getTweenTargetPos().y;

		float vertexX = 0, vertexY = 0;
		BitmapFont font = null;

		switch (tweenType) {
		case RECEIVE_HEALING:
			vertexX = targetX;
			vertexY = 0.5f;
			font = FontData.font_bold_22_green3_bordered;
			break;
		case RECEIVE_DAMAGE:
			vertexX = targetX * 0.33f;
			vertexY = 0.1f;
			font = FontData.font_bold_22_red2_bordered;
			break;
		case RECEIVE_MANA:
			vertexX = targetX;
			vertexY = 0.5f;
			font = FontData.font_bold_22_blue1_bordered;
			break;
		case DEAL_DAMAGE:
			vertexX = targetX;
			vertexY = 0.5f;
			font = FontData.font_bold_22_ivory3_bordered;
			break;
		}

		target.setVertex(vertexX, vertexY + targetY);
		target.setFont(font);

		return 1;
	}

	@Override
	public void setValues(AEffectWithNumber target, int tweenType, float[] newValues) {
		float x = newValues[0];
		float y = 0;

		y = calcParabola(x, target.getVertex());

		if (target.isFlipY() == true)
			x *= (-1);

		// set position
		target.getTweenedPos().set(x, y);

	}

	/**
	 * 
	 * @param x
	 * @param vertex
	 * @return
	 */
	private float calcParabola(float x, Vector2 vertex) {
		float a = (-vertex.y / (vertex.x * vertex.x));
		return a * (x - vertex.x) * (x - vertex.x) + vertex.y;
	}

}