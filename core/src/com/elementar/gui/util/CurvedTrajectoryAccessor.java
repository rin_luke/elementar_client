package com.elementar.gui.util;

import com.elementar.logic.characters.skills.TransferSkillUpdated;

import aurelienribon.tweenengine.TweenAccessor;

public class CurvedTrajectoryAccessor implements TweenAccessor<CurvedTrajectory> {

	public static final int W1 = 0, W2 = 1, W2_BACK = 2;

	@Override
	public int getValues(CurvedTrajectory target, int tweenType, float[] returnValues) {

		// start value.
		returnValues[0] = 0;

		switch (tweenType) {
		case W1:
			return 1;
		case W2:
			return 1;
		case W2_BACK:
			returnValues[0] = TransferSkillUpdated.W2_AMPLITUDE;
			return 1;
		}
		return 0;
	}

	@Override
	public void setValues(CurvedTrajectory target, int tweenType, float[] newValues) {

		switch (tweenType) {
		case W1:
			target.w1 = newValues[0];
			break;
		case W2:
			target.w2 = newValues[0];
			break;
		case W2_BACK:
			target.w2 = newValues[0];
			break;
		}

	}

}