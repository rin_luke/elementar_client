package com.elementar.gui.util;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.map.util.BoundingBoxData;
import com.elementar.logic.util.Util;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.Slot;
import com.esotericsoftware.spine.attachments.Attachment;
import com.esotericsoftware.spine.attachments.BoundingBoxAttachment;

public class BGSkeleton extends CustomSkeleton {

	private Animation animation;

	private BoundingBoxData bounding_box_data;

	private Polygon polygon;

	public BGSkeleton(SkeletonData data, float scale, String animationName) {
		super(data, scale);
		if (animationName.equals("") == false)
			animation = data.findAnimation(animationName);

	}

	public void setBoundingBox(float x, float y) {
		Vector2 startPoint = new Vector2(x, y);
		for (Slot slot : getSlots()) {
			Attachment attachment = slot.getAttachment();
			if (attachment instanceof BoundingBoxAttachment) {
				BoundingBoxAttachment bb = (BoundingBoxAttachment) attachment;
				bounding_box_data = new BoundingBoxData(slot.getBone(), bb);
				Vector2[] local = Util.convertFloatArrayIntoVector2Array(bb.getVertices());
				Vector2[] global = Util.shiftVerticesTo(startPoint, local);
				bounding_box_data.setLocalHitbox(local);
				bounding_box_data.setGlobalHitbox(global);
				polygon = Util.getPolygon(Util.convertVector2ArrayIntoVector2ArrayList(global));
				break;
			}
		}
	}

	public Polygon getPolygon() {
		return polygon;
	}

	public BoundingBoxData getBoundingBoxData() {
		return bounding_box_data;
	}

	/**
	 * Might be <code>null</code>
	 * 
	 * @return
	 */
	public Animation getAnimation() {
		return animation;
	}

}
