package com.elementar.gui.util;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.elementar.data.container.FontData;
import com.elementar.data.container.util.Gameclass;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.AlphaTransition;
import com.elementar.logic.characters.AlphaTransitionAccessor;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.characters.skills.SparseComboStorage;
import com.elementar.logic.creeps.DrawableCreep;
import com.elementar.logic.creeps.OmniCreep;
import com.elementar.logic.emission.effect.AEffectTimed;
import com.elementar.logic.emission.effect.AEffectWithNumber;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.emission.effect.EffectHeal;
import com.elementar.logic.emission.effect.EffectMana;
import com.elementar.logic.map.buildings.Base;
import com.elementar.logic.util.Shared.Team;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

public class BarDisplayManager {

	private boolean prev_flip_recv_healing = false, prev_flip_recv_dmg = false, prev_flip_dealt_dmg = false,
			prev_flip_recv_mana = false;

	private final float BAR_SCALE = 1.1f;

	private final float Y_SHIFT_PLAYER_BAR = 0.77f;
	private final float Y_SHIFT_CREEP_BAR = 0.55f;

	private final Vector2 OFFSET_BAR_TO_BACKGROUND = new Vector2(2 * BAR_SCALE, 2 * BAR_SCALE);
	private final int OFFSET_FONT_TO_CCBAR = 6;
	private final int HEALTH_INTERVAL = 50;
	private final float HEALTH_MARKER_WIDTH_SHORT = 2f, HEALTH_MARKER_WIDTH_WIDE = 3f;

	private float offset_x_to_corner;

	private ShapeRenderer shape_renderer;

	private OmniClient my_omni_client;

	private TweenManager tween_manager = new TweenManager();
	private TweenManager tween_manager2 = new TweenManager();

	private Camera stage_cam, world_cam;
	private ShaderPolyBatch batch;
	private Vector3 projected_screen_coords = new Vector3();

	private GlyphLayout name_glyph_layout = new GlyphLayout();
	private GlyphLayout number_glyph_layout = new GlyphLayout();

	private boolean visible = true;

	private final TweenCallback fade_out_callback = new TweenCallback() {
		@Override
		public void onEvent(int type, BaseTween<?> source) {
			AlphaTransition alphaTransition = (AlphaTransition) source.getUserData();
			Tween.to(alphaTransition, AlphaTransitionAccessor.FADE_OUT, 1f).target(0f)
					.ease(TweenEquations.easeInOutSine).start(tween_manager);
		}
	};

	private final TweenCallback kill_effect_callback = new TweenCallback() {
		@Override
		public void onEvent(int type, BaseTween<?> source) {
			AEffectWithNumber effect = (AEffectWithNumber) source.getUserData();
			effect.setTweenFinished();
		}
	};

	public BarDisplayManager(OmniClient myClient, Camera stageCam, ShaderPolyBatch batch) {
		my_omni_client = myClient;
		shape_renderer = BasicScreen.SHAPE_RENDERER;
		stage_cam = stageCam;
		world_cam = ParallaxCamera.getInstance();
		this.batch = batch;

	}

	public void update(float delta) {
		tween_manager.update(delta);
		tween_manager2.update(delta);
	}

	public void draw() {

		if (visible == false)
			return;

		/*
		 * draw upcoming skill thumbnails and hover effects during an active combo mode
		 */
		DrawableClient drawableClient;
		for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {

			drawableClient = client.getDrawableClient();

			// has player an absorbed skill?
			initAbsorptionTweens(drawableClient.isAbsorptionChange1(), drawableClient.getSkillThumbnailMovement1(),
					drawableClient.getSkillThumbnailAlphaTransition1());
			drawableClient.setAbsorptionChange1(false);
			initAbsorptionTweens(drawableClient.isAbsorptionChange2(), drawableClient.getSkillThumbnailMovement2(),
					drawableClient.getSkillThumbnailAlphaTransition2());
			drawableClient.setAbsorptionChange2(false);

			batch.setProjectionMatrix(stage_cam.combined);

			boolean drawingStorage1 = drawableClient.sparse_combo_storage1.meta_skill_id != -1
					&& drawableClient.sparse_combo_storage1.isStoring() == true
					&& MathUtils.isZero(drawableClient.getSkillThumbnailAlphaTransition1().getValue()) == false;
			boolean drawingStorage2 = drawableClient.sparse_combo_storage2.meta_skill_id != -1
					&& drawableClient.sparse_combo_storage2.isStoring() == true
					&& MathUtils.isZero(drawableClient.getSkillThumbnailAlphaTransition2().getValue()) == false;

			if (drawingStorage1 == true)
				drawMovingComboSkillThumbnail(drawableClient.sparse_combo_storage1, client,
						drawableClient.getSkillThumbnailMovement1(),
						drawableClient.getSkillThumbnailAlphaTransition1().getValue(), drawingStorage2, false);
			if (drawingStorage2 == true)
				drawMovingComboSkillThumbnail(drawableClient.sparse_combo_storage2, client,
						drawableClient.getSkillThumbnailMovement2(),
						drawableClient.getSkillThumbnailAlphaTransition2().getValue(), false, drawingStorage1);

		}

		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		shape_renderer.setProjectionMatrix(stage_cam.combined);
		shape_renderer.begin(ShapeType.Filled);

		for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {
			drawableClient = client.getDrawableClient();

			if (drawableClient.getMetaClient().team == my_omni_client.getMetaClient().team) {
				// ally
				if (client != my_omni_client) {
					drawPlayerBarsAndNumbers(client, 1f, true);
				}
			} else {
				// is enemy detected in one view circle?
				drawPlayerBarsAndNumbers(client, 1f, client.getPhysicalClient().isVisible());
			}

		}

		drawPlayerBarsAndNumbers(my_omni_client, 1f, true);

		for (OmniCreep creep : LogicTier.CREEP_LIVING_LIST) {
			// is visible?
			if (creep.getPhysicalCreep().isVisible() == true)
				drawCreepBar(creep, 1f);
		}

		shape_renderer.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);

	}

	private void initAbsorptionTweens(boolean isAbsorptionChange, SkillThumbnailMovement movement,
			AlphaTransition alpha) {
		if (isAbsorptionChange == true) {

			Tween.to(movement, SkillThumbnailAccessor.MOVEMENT, 2f).target(0.3f).ease(TweenEquations.easeNone)
					.start(tween_manager);

			Tween.to(alpha, AlphaTransitionAccessor.FADE_IN, 1f).target(1f).ease(TweenEquations.easeInOutSine)
					.setUserData(alpha).setCallback(fade_out_callback).start(tween_manager);
		}

	}

	private void drawMovingComboSkillThumbnail(SparseComboStorage comboStorage, OmniClient client,
			SkillThumbnailMovement movement, float alpha, boolean leftShift, boolean rightShift) {

		/*
		 * depending on client's effects we might need an additional offset
		 */
		float additionalYOffset = getYThumbnailOffsetByEffects(client.getPhysicalClient().getEffectsTimed());

		// do projection to screen coords
		doProjection(client.getDrawableClient().pos.x,
				client.getDrawableClient().pos.y + movement.getOffset() + additionalYOffset);
		batch.begin();
		// draw
		Sprite skillThumbnail = Gameclass.ALL_META_SKILLS.get((int) comboStorage.meta_skill_id)
				.getThumbnailAfterAbsorption();
		skillThumbnail.setPosition(projected_screen_coords.x - skillThumbnail.getWidth() * 0.5f
				- (leftShift == true ? skillThumbnail.getWidth() * 0.5f : 0)
				+ (rightShift == true ? skillThumbnail.getWidth() * 0.5f : 0), projected_screen_coords.y);
		skillThumbnail.setAlpha(alpha);
		skillThumbnail.draw(batch);
		batch.end();
	}

	private float getYThumbnailOffsetByEffects(ArrayList<AEffectTimed> effectsTimed) {
		if (effectsTimed.isEmpty() == true)
			return Y_SHIFT_PLAYER_BAR + 0.35f;

		for (AEffectTimed effect : effectsTimed)
			if (effect.hasUpperParticleEffect() == true)
				return Y_SHIFT_PLAYER_BAR + 0.75f;

		return Y_SHIFT_PLAYER_BAR + 0.35f;
	}

	/**
	 * Updates positions of the health/mana + bg sprites of the passed player and
	 * draws them depending on the team
	 * 
	 * @param omniClient
	 * @param alpha
	 * @param visible
	 */
	private void drawPlayerBarsAndNumbers(OmniClient omniClient, float alpha, boolean visible) {

		DrawableClient drawableClient = omniClient.getDrawableClient();

		if (drawableClient.isAlive() == false)
			return;

		float healthBarX, healthBarY;
		float bgWidth = 90f * BAR_SCALE, bgHeight, healthBarHeight = 9f * BAR_SCALE,
				maxHealthBarWidth = bgWidth - 2 * OFFSET_BAR_TO_BACKGROUND.x, healthBarWidth;

		// depends on healthbar height (height depends on team)
		float yShiftCCBar = 0;

		// adjust shift vectors for clients
		offset_x_to_corner = -bgWidth * 0.5f;

		Color barColor = drawableClient.getMetaClient().team != my_omni_client.getMetaClient().team ? SharedColors.RED_2
				: SharedColors.GREEN_2;

		float manaBarHeight = 4f * BAR_SCALE;
		bgHeight = 19f * BAR_SCALE;

		// define shift for later drawing of potential cc bar
		yShiftCCBar = Y_SHIFT_PLAYER_BAR + bgHeight;

		doProjection(drawableClient.pos.x, drawableClient.pos.y + Y_SHIFT_PLAYER_BAR);

		// draw bg
		if (visible == true) {
			shape_renderer.setColor(SharedColors.BLACK);
			shape_renderer.getColor().a = alpha;
			shape_renderer.rect(projected_screen_coords.x + offset_x_to_corner,
					projected_screen_coords.y + Y_SHIFT_PLAYER_BAR, bgWidth, bgHeight);

			// draw mana bar
			shape_renderer.setColor(SharedColors.BLUE_2);
			shape_renderer.getColor().a = alpha;
			shape_renderer.rect(projected_screen_coords.x + OFFSET_BAR_TO_BACKGROUND.x + offset_x_to_corner,
					projected_screen_coords.y + OFFSET_BAR_TO_BACKGROUND.y,
					(bgWidth - 2 * OFFSET_BAR_TO_BACKGROUND.x)
							* ((float) drawableClient.mana_current / drawableClient.getMetaClient().mana_absolute),
					manaBarHeight);
		}
		// draw ally health bar
		healthBarX = projected_screen_coords.x + OFFSET_BAR_TO_BACKGROUND.x + offset_x_to_corner;
		healthBarY = projected_screen_coords.y + 2 * OFFSET_BAR_TO_BACKGROUND.y + manaBarHeight;
		healthBarWidth = maxHealthBarWidth
				* ((float) drawableClient.health_current / drawableClient.getMetaClient().health_absolute);
		if (visible == true) {
			shape_renderer.setColor(barColor);
			shape_renderer.getColor().a = alpha;
			shape_renderer.rect(healthBarX, healthBarY, healthBarWidth, healthBarHeight);
		}
		float maxAmountMarkers = ((float) drawableClient.getMetaClient().health_absolute / HEALTH_INTERVAL);
		short truncMaxAmountMarkers = (short) maxAmountMarkers;
		float remainingHealth = maxAmountMarkers - truncMaxAmountMarkers;
		float fraction = maxHealthBarWidth * (remainingHealth / maxAmountMarkers);
		float drawingInterval = (maxHealthBarWidth - fraction) / truncMaxAmountMarkers;

		short amountHealthMarker = (short) (drawableClient.health_current / HEALTH_INTERVAL);
		if (drawableClient.health_current % HEALTH_INTERVAL == 0)
			amountHealthMarker--;

		shape_renderer.setColor(SharedColors.BLACK);
		float markerWidth = 0;
		for (int i = 1; i <= amountHealthMarker; i++) {

			if (i % 4 == 0) {
				shape_renderer.getColor().a = 1;
				markerWidth = HEALTH_MARKER_WIDTH_WIDE;
			} else {
				shape_renderer.getColor().a = 0.3f;
				markerWidth = HEALTH_MARKER_WIDTH_SHORT;
			}
			if (visible == true)
				shape_renderer.rect(healthBarX + i * drawingInterval - markerWidth * 0.5f, healthBarY, markerWidth,
						healthBarHeight);
			shape_renderer.flush();
		}

		// white damage difference bar
		if (drawableClient.getLastHealth() > 0 && drawableClient.health_current > 0) {

			DamageDiffbar dmgDiffBar = drawableClient.getDamageDiffBar();
			short diffHealth = (short) (drawableClient.getLastHealth() - drawableClient.health_current);

			if (diffHealth > 0) {

				dmgDiffBar.incDamage(diffHealth);

				// dmg diff bar shouldn't be larger than the absolute health.
				if (dmgDiffBar.getDamageAmount()
						+ drawableClient.health_current > drawableClient.getMetaClient().health_absolute)
					dmgDiffBar.setDamageAmount(
							drawableClient.getMetaClient().health_absolute - drawableClient.health_current);

				tween_manager.killTarget(dmgDiffBar);
				Tween.to(dmgDiffBar, DamageDiffbarAccessor.DAMAGE_INTERPOLATION, 0.5f).target(0)
						.ease(TweenEquations.easeOutCubic).delay(0.1f).start(tween_manager);

				// DamageHealthNumber number = new DamageHealthNumber();
				// dmg_health_numbers.add(number);

			}
			// batch.begin();
			//
			// GlyphLayout damageLayout
			// = new GlyphLayout(FontData.font_bold_18, "" +
			// last_diffhealth);
			// FontData.font_bold_18.draw(batch, damageLayout,
			// projection_vector.x - name_glyph_layout.width * 0.5f,
			// projection_vector.y + yShiftCCBar * 7);
			//
			// batch.end();

			healthBarX = healthBarX + healthBarWidth;
			healthBarWidth = (bgWidth - 2 * OFFSET_BAR_TO_BACKGROUND.x)
					* ((float) dmgDiffBar.getDamageAmount() / drawableClient.getMetaClient().health_absolute);
			shape_renderer.setColor(SharedColors.IVORY_2);
			shape_renderer.getColor().a = alpha;
			if (visible == true)
				shape_renderer.rect(healthBarX, healthBarY, healthBarWidth, healthBarHeight);

		}

		drawableClient.setLastHealth(drawableClient.health_current);

		shape_renderer.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);

		batch.begin();

		if (visible == true) {
			// set name
			name_glyph_layout.setText(FontData.font_bold_18_bordered, drawableClient.getMetaClient().name);
			// draw name
			FontData.font_bold_18_bordered.draw(batch, name_glyph_layout,
					projected_screen_coords.x - name_glyph_layout.width * 0.5f,
					projected_screen_coords.y + yShiftCCBar + 20);
		}

		// draw damage and healing numbers
		Iterator<AEffectWithNumber> it = omniClient.getPhysicalClient().getEffectsWithNumber().iterator();

		while (it.hasNext() == true) {
			AEffectWithNumber effect = it.next();

			if (effect.isTweenFinished() == true) {
				it.remove();
				continue;
			}

			if (effect.getTweenedPos() == null) {

				// basically 4 cases:
				// case #1: I see damage at me (red)
				// case #2: I see healing at me (green)
				// case #3: I see mana at me (blue)
				// case #4: I see damage at others through me (ivory)
				// case #5: I see healing at others through me (green)

				// find tween type
				int tweenType = -1;

				float targetX = MathUtils.random(0.05f, 0.3f);
				float targetY = 0;

				if (omniClient == my_omni_client) {
					// cases 1 and 2
					if (effect instanceof EffectDamage) {
						tweenType = EffectNumberAccessor.RECEIVE_DAMAGE;
						effect.setFlipY(prev_flip_recv_dmg);
						prev_flip_recv_dmg = !prev_flip_recv_dmg;
					} else if (effect instanceof EffectHeal) {
						tweenType = EffectNumberAccessor.RECEIVE_HEALING;
						effect.setFlipY(prev_flip_recv_healing);
						prev_flip_recv_healing = !prev_flip_recv_healing;
					} else if (effect instanceof EffectMana) {
						tweenType = EffectNumberAccessor.RECEIVE_MANA;
						effect.setFlipY(prev_flip_recv_mana);
						prev_flip_recv_mana = !prev_flip_recv_mana;
					}
				} else {
					// target = other
					if (effect.getEmitter() == my_omni_client.getPhysicalClient()) {
						// cases 3 and 4 (through me)
						if (effect instanceof EffectDamage) {
							tweenType = EffectNumberAccessor.DEAL_DAMAGE;
							effect.setFlipY(prev_flip_dealt_dmg);
							prev_flip_dealt_dmg = !prev_flip_dealt_dmg;
						} else if (effect instanceof EffectHeal) {
							tweenType = EffectNumberAccessor.RECEIVE_HEALING;
							effect.setFlipY(prev_flip_recv_healing);
							prev_flip_recv_healing = !prev_flip_recv_healing;
						}
					}

				}

				effect.setTweenTargetPos(targetX, targetY);

				Tween.to(effect, tweenType, 0.9f).target(targetX).ease(TweenEquations.easeOutCubic).setUserData(effect)
						.setCallback(kill_effect_callback).start(tween_manager2);
			}

			if (visible == true)
				if (effect.getFont() != null) {
					// set text
					number_glyph_layout.setText(effect.getFont(), effect.getNumberAsString());
					// draw text
					Vector3 projectedScreenCoords = doProjection2(drawableClient.pos.x + effect.getTweenedPos().x,
							drawableClient.pos.y + effect.getTweenedPos().y);
					effect.getFont().draw(batch, number_glyph_layout,
							projectedScreenCoords.x - number_glyph_layout.width * 0.5f, projectedScreenCoords.y);
				}
		}

		batch.end();

		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		shape_renderer.begin(ShapeType.Filled);

		// cc bar
		if (omniClient.getPhysicalClient().getEffectsTimed().isEmpty() == false) {

			/*
			 * the prioritized effect is equal to the last element in the list
			 */
			AEffectTimed prioritizedEffect = omniClient.getPhysicalClient().getEffectsTimed()
					.get(omniClient.getPhysicalClient().getEffectsTimed().size() - 1);

			if (prioritizedEffect.hasUpperParticleEffect() == true && prioritizedEffect.getDelayMS() < 0) {

				bgHeight = 4f;
				// adding small offset
				yShiftCCBar += 25;

				// draw cc bg
				if (visible == true) {
					shape_renderer.setColor(SharedColors.BLACK);
					shape_renderer.rect(projected_screen_coords.x + offset_x_to_corner,
							projected_screen_coords.y + yShiftCCBar, bgWidth, bgHeight);

					// draw cc bar
					shape_renderer.setColor(SharedColors.IVORY_2);
					shape_renderer.rect(projected_screen_coords.x + offset_x_to_corner,
							projected_screen_coords.y + yShiftCCBar, bgWidth * prioritizedEffect.getDurationRatio(),
							bgHeight);
				}
				shape_renderer.end();
				Gdx.gl.glDisable(GL20.GL_BLEND);

				// draw font + upper particle effect
				batch.setProjectionMatrix(stage_cam.combined);
				batch.begin();

				GlyphLayout layout = new GlyphLayout(FontData.font_semibold_italic_17_bordered,
						prioritizedEffect.getText());
				float yFont = projected_screen_coords.y + yShiftCCBar + bgHeight + layout.height + OFFSET_FONT_TO_CCBAR;

				if (visible == true)
					FontData.font_semibold_italic_17_bordered.draw(batch, layout,
							projected_screen_coords.x - layout.width * 0.5f, yFont);

				/*
				 * recognize cc duplicates to ignore them in the drawing process
				 */
				ArrayList<AEffectTimed> drawList = new ArrayList<AEffectTimed>();

				for (int i = omniClient.getPhysicalClient().getEffectsTimed().size(); i-- > 0;) {

					AEffectTimed ccEffect1 = omniClient.getPhysicalClient().getEffectsTimed().get(i);

					boolean duplicateFound = false;

					for (int j = omniClient.getPhysicalClient().getEffectsTimed().size(); j-- > 0;) {
						AEffectTimed ccEffect2 = omniClient.getPhysicalClient().getEffectsTimed().get(j);
						if (ccEffect1 == ccEffect2)
							continue;

						if (ccEffect1.getClass().equals(ccEffect2.getClass()) == true)
							if (drawList.contains(ccEffect1) == true || drawList.contains(ccEffect2) == true)
								duplicateFound = true;
					}

					if (ccEffect1.hasUpperParticleEffect() == false)
						continue;

					if (duplicateFound == true)
						continue;

					if (ccEffect1.getDelayMS() > 0)
						continue;

					drawList.add(ccEffect1);

				}

				float stepSize = 44f;
				float startX = -drawList.size() * 0.5f * stepSize + 0.5f * stepSize;
				int i = 0;

				for (AEffectTimed ccEffect : drawList) {

//						if(prioritizedEffect.getPriority() < ccEffect.getPriority())
//							// stun > remaining cc effects
//							continue;

					// small y offset
					if (visible == true)
						ccEffect.drawUpperParticle(batch, projected_screen_coords.x + startX + i * stepSize,
								yFont + 27);
					i++;

				}
				batch.end();

				Gdx.gl.glEnable(GL20.GL_BLEND);
				Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
				// continue
				shape_renderer.begin(ShapeType.Filled);
			}

		}

	}

	private void drawCreepBar(OmniCreep creep, float alpha) {
		DrawableCreep drawableCreep = creep.getDrawableCreep();

		if (drawableCreep.isAlive() == true) {

			float healthBarX, healthBarY;
			float bgWidth = 70f, bgHeight = 12, healthBarHeight = 8f, healthBarWidth;

			// depends on healthbar height (height depends on team)
			float yShiftCCBar = 0;

			// adjust shift vectors for clients
			offset_x_to_corner = -bgWidth * 0.5f;

			// define shift for later drawing of potential cc bar
			yShiftCCBar = Y_SHIFT_CREEP_BAR + bgHeight;

			doProjection(drawableCreep.pos.x, drawableCreep.pos.y + Y_SHIFT_CREEP_BAR);

			// draw bg
			shape_renderer.setColor(SharedColors.BLACK);
			shape_renderer.getColor().a = alpha;
			shape_renderer.rect(projected_screen_coords.x + offset_x_to_corner,
					projected_screen_coords.y + Y_SHIFT_CREEP_BAR, bgWidth, bgHeight);

			// draw health bar
			healthBarX = projected_screen_coords.x + OFFSET_BAR_TO_BACKGROUND.x + offset_x_to_corner;
			healthBarY = projected_screen_coords.y + OFFSET_BAR_TO_BACKGROUND.y;
			healthBarWidth = (bgWidth - 2 * OFFSET_BAR_TO_BACKGROUND.x)
					* ((float) drawableCreep.health_current / drawableCreep.health_absolute);

			shape_renderer.setColor(SharedColors.GREEN_2);
			shape_renderer.getColor().a = alpha;
			shape_renderer.rect(healthBarX, healthBarY, healthBarWidth, healthBarHeight);

			// white damage difference bar
			if (drawableCreep.getLastHealth() > 0 && drawableCreep.health_current > 0) {

				DamageDiffbar dmgDiffBar = drawableCreep.getDamageDiffBar();
				short diffHealth = (short) (drawableCreep.getLastHealth() - drawableCreep.health_current);

				if (diffHealth > 0) {

					dmgDiffBar.incDamage(diffHealth);
					tween_manager.killTarget(dmgDiffBar);
					Tween.to(dmgDiffBar, DamageDiffbarAccessor.DAMAGE_INTERPOLATION, 0.5f).target(0)
							.ease(TweenEquations.easeOutCubic).delay(0.1f).start(tween_manager);

				}

				healthBarX = healthBarX + healthBarWidth;
				healthBarWidth = (bgWidth - 2 * OFFSET_BAR_TO_BACKGROUND.x)
						* ((float) dmgDiffBar.getDamageAmount() / drawableCreep.health_absolute);
				shape_renderer.setColor(SharedColors.IVORY_2);
				shape_renderer.getColor().a = alpha;
				shape_renderer.rect(healthBarX, healthBarY, healthBarWidth, healthBarHeight);

			}

			drawableCreep.setLastHealth(drawableCreep.health_current);

			// // cc bar
			// if (creep.getPhysicalCreep().getEffectsTimed().isEmpty() ==
			// false) {
			//
			// bgHeight = 4f;
			// // the prioritized effect is equal to the last element in the
			// // list
			// AEffectTimed prioritizedEffect =
			// creep.getPhysicalCreep().getEffectsTimed()
			// .get(creep.getPhysicalCreep().getEffectsTimed().size() - 1);
			//
			// yShiftCCBar += 5;
			//
			// if (prioritizedEffect.isDrawableInWorld() == true) {
			//
			// // draw cc bg
			// shape_renderer.setColor(bg_cc_black);
			// shape_renderer.rect(projection_vector.x + offset_x_to_center,
			// projection_vector.y + yShiftCCBar, bgWidth, bgHeight);
			//
			// // draw cc bar
			// shape_renderer.setColor(cc_white);
			// shape_renderer.rect(projection_vector.x + offset_x_to_center,
			// projection_vector.y + yShiftCCBar,
			// bgWidth * prioritizedEffect.getDurationRatio(), bgHeight);
			//
			// // draw font + upper particle effect
			// shape_renderer.end();
			// batch.setProjectionMatrix(stage_cam.combined);
			// batch.begin();
			// // + small y offset
			// prioritizedEffect.drawInWorld(batch, projection_vector.x,
			// projection_vector.y
			// + yShiftCCBar + bgHeight + OFFSET_FONT_TO_CCBAR + 27);
			// batch.end();
			//
			// // continue
			// shape_renderer.begin(ShapeType.Filled);
			// }
			//
			// }
		}

	}

	private void drawBaseHealth(Base base) {

		if (base.isAlive() == true) {
			float healthBarX, healthBarY;
			float bgWidth = 175f, bgHeight = 42f, healthBarWidth;

			float offsetXByTeam = 127;
			if (base.getTeam() == Team.TEAM_1)
				offsetXByTeam = offsetXByTeam * (-1) - bgWidth;

			// draw bg
			shape_renderer.setColor(SharedColors.BLACK);
			shape_renderer.getColor().a = 1;

			shape_renderer.rect(stage_cam.position.x + offsetXByTeam,
					stage_cam.position.y + stage_cam.viewportHeight * 0.5f - bgHeight, bgWidth, bgHeight);

			// draw health bar
			if (base.getTeam() == my_omni_client.getMetaClient().team)
				shape_renderer.setColor(SharedColors.GREEN_2);
			else
				shape_renderer.setColor(SharedColors.RED_2);
			shape_renderer.getColor().a = 1;

			float healthPercentage = (float) base.getHealthCurrent() / base.getHealthAbsolute();

			if (base.getTeam() == Team.TEAM_2) {
				healthBarX = stage_cam.position.x + offsetXByTeam;
				healthBarY = stage_cam.position.y + stage_cam.viewportHeight * 0.5f - bgHeight;
				healthBarWidth = bgWidth * healthPercentage;
				shape_renderer.rect(healthBarX, healthBarY, healthBarWidth, bgHeight);
			} else {
				// team 1
				float newWidth = bgWidth * healthPercentage;

				healthBarX = stage_cam.position.x + offsetXByTeam + (bgWidth - newWidth);
				healthBarY = stage_cam.position.y + stage_cam.viewportHeight * 0.5f - bgHeight;
				shape_renderer.rect(healthBarX, healthBarY, newWidth, bgHeight);
			}

			// draw font + upper particle effect
			shape_renderer.end();
			batch.setProjectionMatrix(stage_cam.combined);
			batch.begin();
			GlyphLayout layout = new GlyphLayout(FontData.font_bold_18_bordered,
					"" + (short) (healthPercentage * 100) + "%");
			// float yFont = projection_vector.y + yShiftCCBar + bgHeight +
			// layout.height
			// + OFFSET_FONT_TO_CCBAR;
			FontData.font_bold_18_bordered.draw(batch, layout,
					stage_cam.position.x + offsetXByTeam + bgWidth * 0.5f - layout.width * 0.5f,
					healthBarY + layout.height * 0.5f + bgHeight * 0.5f);
			batch.end();

			// continue
			shape_renderer.begin(ShapeType.Filled);

		}
	}

	/**
	 * Help method to get screen coordinates
	 * 
	 * @param worldX
	 * @param worldY
	 */
	private void doProjection(float worldX, float worldY) {
		projected_screen_coords.set(worldX, worldY, 0);
		world_cam.project(projected_screen_coords, 0, 0, stage_cam.viewportWidth, stage_cam.viewportHeight);
	}

	private Vector3 doProjection2(float worldX, float worldY) {
		Vector3 projectedVec = new Vector3(worldX, worldY, 0);
		world_cam.project(projectedVec, 0, 0, stage_cam.viewportWidth, stage_cam.viewportHeight);
		return projectedVec;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

}
