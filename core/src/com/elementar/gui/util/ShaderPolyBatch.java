package com.elementar.gui.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ShaderPolyBatch extends PolygonSpriteBatch {

	public static final String vertexShader = "attribute vec4 " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
			+ "attribute vec4 " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
			+ "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
			+ "uniform mat4 u_projTrans;\n" //
			+ "varying vec4 v_color;\n" //
			+ "varying vec2 v_texCoords;\n" //
			+ "\n" //
			+ "void main()\n" //
			+ "{\n" //
			+ "   v_color = " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
			+ "   v_texCoords = " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
			+ "   gl_Position =  u_projTrans * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
			+ "}\n";

	public static final String fragmentShader = "#ifdef GL_ES\n" //
			+ "#define LOWP lowp\n" //
			+ "precision mediump float;\n" //
			+ "#else\n" //
			+ "#define LOWP \n" //
			+ "#endif\n" //
			+ "varying LOWP vec4 v_color;\n" //
			+ "varying vec2 v_texCoords;\n" //
			+ "uniform sampler2D u_texture;\n" //
			+ "uniform float brightness;\n" //
			+ "uniform float contrast;\n" //
			+ "void main()\n"//
			+ "{\n" //
			+ "  vec4 color = v_color * texture2D(u_texture, v_texCoords);\n"
			// + " color.rgb /= color.a;\n" // ignore alpha
			+ "  color.rgb = ((color.rgb - 0.5) * max(contrast, 0.0)) + 0.5;\n" // apply
																				// contrast
			+ "  color.rgb += brightness;\n" // apply brightness
			// + " color.rgb *= color.a;\n" // return alpha
			+ "  gl_FragColor = color;\n" + "}";

	/**
	 * das auskommentieren des obigen codes (color.rgb *= color.a ) entfernt
	 * schwarze ränder beim parallax effekt
	 */

	protected int brightnessLoc = -1, contrastLoc = -1;

	// ideally use getters/setters here...
	public float brightness = 0f, contrast = 1f;

	public ShaderProgram sprite_shader;

	public ShaderProgram outline_shader;

	public ShaderProgram island_darkening_shader;

	/**
	 * for unfulfilled quest-skins
	 */
	public ShaderProgram black_shader;

	public ShaderProgram minimap_shader;

	public ShaderPolyBatch() {
		super();

		ShaderProgram.pedantic = false;

		sprite_shader = new ShaderProgram(vertexShader, fragmentShader);
		if (sprite_shader.isCompiled() == true) {
			brightnessLoc = sprite_shader.getUniformLocation("brightness");
			contrastLoc = sprite_shader.getUniformLocation("contrast");
		}

		black_shader = new ShaderProgram(Gdx.files.internal("shader/black_vertex_shader.vert").readString(),
				Gdx.files.internal("shader/black_fragment_shader.frag").readString());
		if (black_shader.isCompiled() == false)
			throw new GdxRuntimeException("Couldn't compile shader: " + black_shader.getLog());

		minimap_shader = new ShaderProgram(Gdx.files.internal("shader/minimap_vertex_shader.vert").readString(),
				Gdx.files.internal("shader/minimap_fragment_shader.frag").readString());
		if (minimap_shader.isCompiled() == false)
			throw new GdxRuntimeException("Couldn't compile shader: " + minimap_shader.getLog());

		outline_shader = new ShaderProgram(Gdx.files.internal("shader/outline_vertex_shader.vert").readString(),
				Gdx.files.internal("shader/outline_fragment_shader.frag").readString());
		if (outline_shader.isCompiled() == false)
			throw new GdxRuntimeException("Couldn't compile shader: " + outline_shader.getLog());

		island_darkening_shader = new ShaderProgram(
				Gdx.files.internal("shader/island_darkening_shader.vert").readString(),
				Gdx.files.internal("shader/island_darkening_shader.frag").readString());
		if (island_darkening_shader.isCompiled() == false)
			throw new GdxRuntimeException("Couldn't compile shader: " + island_darkening_shader.getLog());

	}

	@Override
	public void begin() {
		super.begin();

		/*
		 * Aus irgendeinem Grund muss hier an dieser Stelle die beiden Felder
		 * 'brightness' und 'contrast' überschrieben werden. Wenn nicht, wird
		 * beim drüber-hovern des zuletzt ge-addeten buttons (Beispiel: Credits
		 * button) über den gesamten Screen ein brightness effekt gelegt. Ich
		 * vermute, dass das an einem Fehler in Stage#draw() liegt.
		 */
		if (brightnessLoc != -1 && sprite_shader != null)
			sprite_shader.setUniformf(brightnessLoc, brightness);
		if (contrastLoc != -1 && sprite_shader != null)
			sprite_shader.setUniformf(contrastLoc, contrast);
	}

	public void setBrightness(float brightness) {
		this.brightness = brightness;
		if (brightnessLoc != -1 && sprite_shader != null)
			sprite_shader.setUniformf(brightnessLoc, this.brightness);
	}

	public void setContrast(float contrast) {
		this.contrast = contrast;
		if (contrastLoc != -1 && sprite_shader != null) {
			sprite_shader.setUniformf(contrastLoc, this.contrast);
		}
	}

	/**
	 * Resets brightness to 0f and contrast to 1f.
	 */
	public void reset() {
		setBrightness(0f);
		setContrast(1f);
	}

}