package com.elementar.gui.util;

import java.util.Random;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.elementar.gui.widgets.CustomTextfield;
import com.elementar.gui.widgets.SeedTextfield;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;
import com.elementar.gui.widgets.listener.SoundClickListener;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.CategoryFunctionality;
import com.elementar.logic.util.Shared.CategorySize;

public class GUIUtil {

	/**
	 * 
	 * @param functionality
	 * @param size
	 * @return
	 */
	public static float getRndSwingDuration(CategoryFunctionality functionality,
			CategorySize size) {
		switch (functionality) {
		case WALK:
		case SLIDE:
		case NON_WALK:
			switch (size) {
			case SMALL:
				return MathUtils.random(1f) + 1.5f;
			case MEDIUM:
				return MathUtils.random(1f) + 1.5f;
			}
		}

		return 0;
	}

	/**
	 * Called when a plant starts swinging. Returns a percentage. After
	 * percentage*duration of an idle animation, the second upper sprite corner
	 * starts swinging. So unlike the first upper sprite corner the second one
	 * has a little offset.During swinging it gets a more dynamical look.
	 * 
	 * @param functionality
	 * @param size
	 * @return
	 */
	public static float getDelayInPercentOfDuration(CategoryFunctionality functionality,
			CategorySize size) {
		switch (functionality) {

		case WALK:
		case SLIDE:
		case NON_WALK:
			switch (size) {
			case SMALL:
				return 0.3f;
			case MEDIUM:
				return 0.15f;
			}
		}

		return 0;
	}

	/**
	 * Returns the amplitude at vegetations idle animation
	 * 
	 * @param functionality
	 * @param size
	 * @return
	 */
	public static float getIdleAmplitude(CategoryFunctionality functionality, CategorySize size) {
		switch (functionality) {

		case WALK:
		case SLIDE:
		case NON_WALK:
			switch (size) {
			case SMALL:
				return 0.15f;
			case MEDIUM:
				return 0.07f;
			}
		}
		return 0;
	}

	/**
	 * Returns the amplitude at a "run-through" animation of vegetation
	 * 
	 * @param functionality
	 * @param size
	 * @return
	 */
	public static float getRunThroughAmplitude(CategoryFunctionality functionality,
			CategorySize size) {
		switch (functionality) {

		case WALK:
		case SLIDE:
		case NON_WALK:
			switch (size) {
			case SMALL:
				return 0.6f;
			case MEDIUM:
				return 0.2f;
			}
		}

		return 0;
	}

	/**
	 * Calls the {@link ClickListener#clicked(InputEvent, float, float)} method
	 * of all {@link ClickListener} the actor has.
	 * 
	 * @param actor
	 */
	public static void clickActor(Actor actor) {
		if (actor != null) {
			for (EventListener listener : actor.getListeners())
				if (listener instanceof SoundClickListener == false)
					if (listener instanceof ClickListener)
						((ClickListener) listener).clicked(null, 0, 0);
		}
	}

	/**
	 * This method makes changes to the spriteBatch in a way that background
	 * images of buttons get brighter and get contrast changes. So it looks like
	 * the button got hovered.
	 * 
	 * @param batch
	 * @param state
	 */
	public static void determineHoverEffect(ShaderPolyBatch batch, State state) {
		if (state == State.HOVERED || state == State.SELECTED) {
			batch.setBrightness(Shared.WIDGET_HOVER_BRIGHTNESS);
			batch.setContrast(Shared.WIDGET_HOVER_CONTRAST);
		} else
			batch.reset();
	}

	/**
	 * Non-digits will be parsed into digits
	 * 
	 * @param seedField
	 * @return
	 */
	public static long getValidSeed(CustomTextfield seedField) {
		String unparsedSeed = seedField.getText();
		String parsedSeed = "";
		if (unparsedSeed.equals("") || unparsedSeed.equals(seedField.getUnfocusedText())
				|| unparsedSeed.length() < 2) {
			parsedSeed = (new Random()).nextLong() + "";
			if (parsedSeed.charAt(0) == '-')
				parsedSeed = parsedSeed.substring(1);
		} else {
			char c;
			int numericalVal;
			for (int i = 0; i < unparsedSeed.length(); i++) {
				c = unparsedSeed.charAt(i);
				numericalVal = Character.getNumericValue(c);
				if (numericalVal == -1 || numericalVal == -2)
					parsedSeed += "" + Math.abs(numericalVal);
				else
					parsedSeed += numericalVal;
			}
		}

		// the resulting long must be in an appropriate range
		if (parsedSeed.length() >= SeedTextfield.SEED_LENGTH)
			parsedSeed = parsedSeed.substring(0, SeedTextfield.SEED_LENGTH);

		return Long.parseLong(parsedSeed);
	}

}
