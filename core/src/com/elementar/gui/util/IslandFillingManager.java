package com.elementar.gui.util;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Vector2;
import com.elementar.data.container.StaticGraphic;
import com.elementar.logic.util.Util;

public class IslandFillingManager {

	private ArrayList<PolygonSprite> island_filling_sprites;
	private Texture island_filling_texture;
	private EarClippingTriangulator earclipping_triangulator;

	public IslandFillingManager() {
		island_filling_sprites = new ArrayList<PolygonSprite>();

		island_filling_texture = StaticGraphic.island_filling_texture;

		earclipping_triangulator = new EarClippingTriangulator();

	}

	public void place(Vector2[] globalVertices, Vector2 startPoint) {

		/*
		 * If I would directly create a polygon sprite out of the globalVertices
		 * parameter, the resulting picture would be very small, because the
		 * globalVertices represents an island in world space. It might be an
		 * island with 10x10 pixel. So our approach is to scale the island's
		 * vertices up to screen coords, create a polygon sprite of it and scale
		 * the resulting sprite down to world coords.
		 */
		float scaleFactor = 250;
		// up scale
		PolygonRegion polyReg = new PolygonRegion(new TextureRegion(island_filling_texture),
				Util.convertVector2ArrayIntoFloatArray(
						Util.scaleVertices(globalVertices, scaleFactor)),
				earclipping_triangulator
						.computeTriangles(Util.convertVector2ArrayIntoFloatArray(globalVertices))
						.toArray());

		PolygonSprite islandFillingSprite = new PolygonSprite(polyReg);
		// down scale
		islandFillingSprite.setScale(1 / scaleFactor);
		islandFillingSprite.setOrigin(startPoint.x, startPoint.y);
		island_filling_sprites.add(islandFillingSprite);

	}

	public void draw(ShaderPolyBatch batch) {

		// batch.setShader(batch.island_darkening_shader);

		// Vector2 resolution = new
		// Vector2(GUITier.getInstance().getViewport().getScreenWidth(),
		// GUITier.getInstance().getViewport().getScreenHeight());
		// batch.island_darkening_shader.setUniformf("resolution", resolution);

		for (PolygonSprite islandFillingSprite : island_filling_sprites)
			islandFillingSprite.draw(batch);

		// batch.setShader(batch.sprite_shader);
	}

	public ArrayList<PolygonSprite> getSprites() {
		return island_filling_sprites;
	}

}
