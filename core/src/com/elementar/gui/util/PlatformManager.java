package com.elementar.gui.util;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.elementar.data.container.StaticGraphic;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.Shared.CategoryFunctionality;
import com.elementar.logic.util.Shared.CategorySize;

public class PlatformManager {

	/**
	 * piece of platform-sprite overlaps, so it interfere with the next resp.
	 * prev platform-sprite.
	 */
	private final float OVERLAPPING_PERCENT = 0.25f;

	private ArrayList<Sprite> platform_sprites;

	private float sprite_width = 0, sprite_height, platform_length;

	/**
	 * vector to shift sprite a little bit outside (to overlap with another
	 * sprites for continuous looking)
	 */
	private Vector2 offset_vector;
	/**
	 * position of platformsprite. This vector is influenced by offset_vector.
	 */
	private Vector2 position;
	private Vector2 platform_direction;
	private CategoryFunctionality current_functionality;

	private ArrayList<Sprite> non_walkables = new ArrayList<>();
	private ArrayList<Sprite> slidables = new ArrayList<>();
	private ArrayList<Sprite> walkables = new ArrayList<>();

	public PlatformManager() {
		platform_sprites = new ArrayList<Sprite>();
	}

	public void place(Vector2[] globalVertices) {

		// To determine platforms
		Vector2 posBegin = null;
		Vector2 posEnd = null;
		float scaleX = 1f, scaleY = 1f;
		float overlappingLength;
		CategorySize size = null;
		boolean intersecting;
		boolean enoughSpace;
		ArrayList<MyIntersection> intersections = new ArrayList<>();
		for (int i = 1; i <= globalVertices.length; i++) {

			// define posBegin
			posBegin = globalVertices[i - 1];

			// define posEnd
			if (i == globalVertices.length)
				posEnd = globalVertices[0];
			else
				posEnd = globalVertices[i];
			platform_direction = Util.subVectors(posEnd, posBegin);

			platform_length = platform_direction.len();
			int angle = Math.round(platform_direction.angle());

			// define functionality
			current_functionality = Util.getPlatformFunctionality(angle);

			offset_vector = new Vector2(platform_direction.x, platform_direction.y);

			if (platform_length < StaticGraphic.platform_small_width) {
				// TODO warum mach ich *2 und *4?
				offset_vector.setLength(platform_length * 2 * OVERLAPPING_PERCENT);
				position = Util.subVectors(posBegin, offset_vector);

				scaleX = (platform_length + 4 * platform_length * OVERLAPPING_PERCENT)
						/ StaticGraphic.platform_small_width;
				add(StaticGraphic.makePlatformSprite(CategorySize.SMALL, current_functionality, position, angle, scaleX,
						scaleY), current_functionality);

			} else {

				// set start position
				position = new Vector2(posBegin);

				while (true) {

					sprite_width = StaticGraphic.platform_large_width;
					overlappingLength = sprite_width * OVERLAPPING_PERCENT;
					sprite_height = StaticGraphic.platform_large_height;

					offset_vector.setLength(overlappingLength);
					position = Util.subVectors(position, offset_vector);
					platform_length += overlappingLength;

					enoughSpace = isEnoughSpaceX(sprite_width, overlappingLength);
					intersecting = isIntersecting(sprite_width, sprite_height, globalVertices, intersections);
					if (enoughSpace == false || intersecting == true) {
						// large is not valid

						/*
						 * undo offset to have same start conditions for new
						 * offset
						 */
						position = Util.addVectors(position, offset_vector);
						platform_length -= overlappingLength;

						// continue with new dimensions
						sprite_width = StaticGraphic.platform_small_width;
						overlappingLength = sprite_width * OVERLAPPING_PERCENT;
						sprite_height = StaticGraphic.platform_small_height;

						/*
						 * old formula, seems to be wrong in my eyes
						 */
						scaleX = (platform_length + overlappingLength) / sprite_width;
						/*
						 * following is the deriviation of: scaleX =
						 * (platform_length +
						 * scaleX*overLappingLength)/sprite_width;
						 */
						// scaleX = platform_length / (sprite_width -
						// overlappingLength);

						if (scaleX < 1f)
							scaleX = 1f;
						if (scaleX > 2f)
							scaleX = 2f;

						sprite_width *= scaleX;
						overlappingLength = sprite_width * OVERLAPPING_PERCENT;

						offset_vector.setLength(overlappingLength);
						position = Util.subVectors(position, offset_vector);
						platform_length += overlappingLength;

						size = CategorySize.SMALL;

						enoughSpace = isEnoughSpaceX(sprite_width, overlappingLength);

					} else {
						scaleX = 1f;
						size = CategorySize.LARGE;
					}

					if (enoughSpace == false) {
						// put a last perfectly fitting sprite there
						sprite_width = StaticGraphic.platform_small_width;
						overlappingLength = sprite_width * (1 - OVERLAPPING_PERCENT);

						offset_vector.setLength(overlappingLength);
						position = Util.subVectors(posEnd, offset_vector);

						size = CategorySize.SMALL;
						scaleX = 1f;
						add(StaticGraphic.makePlatformSprite(size, current_functionality, position, angle, scaleX,
								scaleY), current_functionality);
						break;
					}

					add(StaticGraphic.makePlatformSprite(size, current_functionality, position, angle, scaleX, scaleY),
							current_functionality);

					platform_length -= sprite_width;
					// set next position
					offset_vector.setLength(sprite_width);
					position = Util.addVectors(position, offset_vector);

				}

			}
		}

		platform_sprites = getSortedList();

	}

	private void add(Sprite sprite, CategoryFunctionality func) {
		switch (func) {
		case NON_WALK:
			non_walkables.add(sprite);
			break;
		case SLIDE:
			slidables.add(sprite);
			break;
		case WALK:
			walkables.add(sprite);
			break;
		}
	}

	private ArrayList<Sprite> getSortedList() {
		ArrayList<Sprite> sprites = new ArrayList<>();

		sprites.addAll(non_walkables);
		sprites.addAll(walkables);
		sprites.addAll(slidables);

		return sprites;
	}

	/**
	 * Checks whether a given platformSprite-size fits onto platform
	 * 
	 * @param spriteWidth
	 * @param overlapping
	 * @return
	 */
	private boolean isEnoughSpaceX(float spriteWidth, float overlapping) {
		return platform_length + overlapping > spriteWidth;
	}

	/**
	 * checks intersecting
	 * 
	 * @param spriteWidth
	 * @param spriteHeight
	 * @param globalVertices
	 * @param intersections
	 * @return
	 */
	private boolean isIntersecting(float spriteWidth, float spriteHeight, Vector2[] globalVertices,
			ArrayList<MyIntersection> intersections) {

		intersections.clear();

		Vector2 cpyPlatformDirection = new Vector2(platform_direction.x, platform_direction.y);
		// CHECKEN OB TAKENWIDTH RAUSGUCKT AUS PLATFORM, idee: restliche
		// platformlength mitgeben um zu testen
		cpyPlatformDirection.setLength(spriteWidth);
		Vector2 orthoPlatformDirection = new Vector2(platform_direction.x, platform_direction.y);
		orthoPlatformDirection.rotate(-90f);
		orthoPlatformDirection.setLength(spriteHeight);
		Vector2 preSensor1 = Util.addVectors(position, cpyPlatformDirection, 0.3f);
		Vector2 preSensor2 = Util.subVectors(Util.addVectors(position, cpyPlatformDirection), cpyPlatformDirection,
				0.3f);
		Vector2 sensorStart1 = Util.addVectors(preSensor1, orthoPlatformDirection, 0.1f);
		Vector2 sensorStart2 = Util.addVectors(preSensor2, orthoPlatformDirection, 0.1f);
		Vector2 sensorEnd1 = Util.addVectors(sensorStart1, orthoPlatformDirection, 1f);
		Vector2 sensorEnd2 = Util.addVectors(sensorStart2, orthoPlatformDirection, 1f);
		Vector2 platformStart = null, platformEnd = null;
		MyIntersection myIntersection = null;
		for (int i = 1; i <= globalVertices.length; i++) {
			platformStart = globalVertices[i - 1];
			if (i == globalVertices.length)
				platformEnd = globalVertices[0];
			else
				platformEnd = globalVertices[i];

			myIntersection = new MyIntersection();
			if (Intersector.intersectSegments(platformStart, platformEnd, sensorStart1, sensorEnd1,
					myIntersection.intersection_point)) {
				myIntersection.distance_to_platform = myIntersection.intersection_point.dst(preSensor1);
				intersections.add(myIntersection);
			}
			myIntersection = new MyIntersection();
			if (Intersector.intersectSegments(platformStart, platformEnd, sensorStart2, sensorEnd2,
					myIntersection.intersection_point)) {
				myIntersection.distance_to_platform = myIntersection.intersection_point.dst(preSensor2);
				intersections.add(myIntersection);
			}
		}

		return intersections.size() != 0;
	}

	public void draw(ShaderPolyBatch batch) {
		for (Sprite platform : platform_sprites)
			platform.draw(batch);

	}

	public ArrayList<Sprite> getPlatformSprites() {
		return platform_sprites;
	}

	private class MyIntersection implements Comparable<MyIntersection> {

		private float distance_to_platform;
		private Vector2 intersection_point = new Vector2();
		private Vector2 platform_point;

		public MyIntersection() {
		}

		@Override
		public int compareTo(MyIntersection o) {
			if (distance_to_platform < o.distance_to_platform)
				return -1;
			return 1;
		}

	}

}
