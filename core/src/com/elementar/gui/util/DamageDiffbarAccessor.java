package com.elementar.gui.util;

import aurelienribon.tweenengine.TweenAccessor;

public class DamageDiffbarAccessor implements TweenAccessor<DamageDiffbar> {

	public static final int DAMAGE_INTERPOLATION = 1;

	@Override
	public int getValues(DamageDiffbar target, int tweenType, float[] returnValues) {

		switch (tweenType) {
		case DAMAGE_INTERPOLATION:
			returnValues[0] = target.getDamageAmount();
			return 1;
		}

		return 0;
	}

	@Override
	public void setValues(DamageDiffbar target, int tweenType, float[] newValues) {

		switch (tweenType) {
		case DAMAGE_INTERPOLATION:
			target.setDamageAmount(newValues[0]);
		}

	}
}
