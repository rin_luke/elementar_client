package com.elementar.gui.util;

import aurelienribon.tweenengine.TweenManager;

/**
 * Wrapper class to interpolate the damage amount with a {@link TweenManager}
 * 
 * @author lukassongajlo
 *
 */
public class DamageDiffbar{

	private short damage_amount;

	public DamageDiffbar() {
	}

	public void incDamage(short damage) {
		this.damage_amount += damage;
	}

	public short getDamageAmount() {
		return damage_amount;
	}

	public void setDamageAmount(float damage) {
		damage_amount = (short) damage;
	}

}
