package com.elementar.gui;

import java.util.ArrayList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Cursor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.util.CustomStage;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.logic.LogicTier;

public abstract class AGUITier extends Game {
	public static int SCREEN_HEIGHT, SCREEN_WIDTH;

	{

		// Log.set(Log.LEVEL_DEBUG);
		// DisplayMode displayMode =
		// Lwjgl3ApplicationConfiguration.getDisplayMode();
		//
		// SCREEN_HEIGHT = displayMode.height;
		// SCREEN_WIDTH = displayMode.width;

		// SCREEN_HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height;
		// SCREEN_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width;

	}

	protected final int OFFSET_CURSOR = 32;

	protected LogicTier logic_tier;

	protected BasicScreen screen;

	protected ArrayList<CustomStage> STAGES = new ArrayList<>();

	protected static AGUITier instance;

	protected Viewport stage_viewport;

	protected Cursor current_cursor;
	public Cursor cursor_invisible, cursor_regular, cursor_world;

	public AGUITier() {
		if (instance == null)
			instance = this;

	}

	public static AGUITier getInstance() {
		return instance;
	}

	private void clearStages() {
		for (CustomStage stage : STAGES)
			stage.clear();
	}

	/**
	 * Changes the root by clearing the stages, deleting all {@link HoverHandler}
	 * objects, setting screen's rootmodule with
	 * {@link BasicScreen#setRootModule(AModule)} and call {@link AModule#show()} at
	 * the end of the method. Note that this method is called in a root module's
	 * update method. The update method will run out with the old root module even
	 * the root module is changed in this method.
	 * 
	 * @param rootModule
	 */
	public void changeRootModule(AModule rootModule) {
		clearStages();
		HoverHandler.clear();
		// at the beginning the screen.getRootModule() returns null
		if (screen.getRootModule() != null)
			screen.getRootModule().dispose();
		screen.setRootModule(rootModule);
		rootModule.show();
	}

	@Override
	public void dispose() {
		super.dispose();
		if (screen != null)
			screen.dispose();
		clearStages();
		for (Stage stage : STAGES)
			stage.dispose();

	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public BasicScreen getScreen() {
		return screen;
	}

	public LogicTier getLogicTier() {
		return logic_tier;
	}

	public Viewport getViewport() {
		return stage_viewport;
	}

	/**
	 * Take as parameter one static integer of the {@link BasicScreen} class.
	 * 
	 * @param drawOrder
	 * @return
	 */
	public CustomStage getStageByDrawOrder(int drawOrder) {
		return STAGES.get(drawOrder);
	}

	public int getNumberStages() {
		return STAGES.size();
	}

	/**
	 * Sets the debug-mode of all stages
	 * 
	 * @param debug
	 */
	public void setDebugStages(boolean debug) {
		for (Stage stage : STAGES)
			stage.setDebugAll(debug);
	}

	public void setCursor(Cursor cursor) {
		if (current_cursor != cursor) {
			current_cursor = cursor;
			Gdx.graphics.setCursor(cursor);
		}
	}

}
