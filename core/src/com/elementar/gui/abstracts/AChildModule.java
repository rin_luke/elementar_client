package com.elementar.gui.abstracts;

/**
 * No child could be a root.
 * 
 * @author lukassongajlo
 * 
 */
public abstract class AChildModule extends AModule {

	/**
	 * is_root = false
	 * 
	 * @param visible
	 * @param drawOrder
	 */
	public AChildModule(boolean visible, int drawOrder) {
		super(visible, false, drawOrder);
		show();
	}

	/**
	 * With regular drawOrder
	 * 
	 * @param visible
	 */
	public AChildModule(boolean visible) {
		this(visible, BasicScreen.DRAW_REGULAR);
	}

	/**
	 * child modules don't have to handle this method.
	 */
	@Override
	protected boolean clickedEscape() {
		return false;
	}
}
