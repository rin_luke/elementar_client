package com.elementar.gui.abstracts;

import java.util.ArrayList;

/**
 * Each {@link Modular} implementation (ergo modules) might has widgets and
 * sub-modules. These methods help to coordinate them and will be called in
 * {@link AModule#show()}.
 * 
 * @author lukassongajlo
 * 
 */
public interface Modular {

	abstract void loadWidgets();

	/**
	 * This method is responsible to layout all widgets and add all needed
	 * tables to {@link AModule#tables}.
	 */
	abstract void setLayout();

	abstract void setListeners();

	/**
	 * use this method to load modules
	 */
	abstract void loadSubModules();

	abstract void addSubModules(ArrayList<AModule> subModules);
}
