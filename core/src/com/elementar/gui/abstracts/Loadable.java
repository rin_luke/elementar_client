package com.elementar.gui.abstracts;

import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.elementar.logic.map.MapLoader;
import com.elementar.logic.map.MapManager;

/**
 * Each implementation of {@link Loadable} has a kind of progress which can be
 * shown in a {@link ProgressBar} widget. Let implement classes this interface
 * when they are part of a progress process, e.g. {@link MapManager} or
 * {@link MapLoader}.
 * <p>
 * This interface has a {@link #getEndState()} but no 'getBeginState()' because
 * the loading process starts always at 0.
 * 
 * @author lukassongajlo
 * 
 */
public interface Loadable {

	/**
	 * Returns the number of elements when the progress has to be finished.
	 * elements could be the number of generated islands in {@link MapManager}
	 * for example.
	 * 
	 * @return
	 */
	public int getEndState();

	/**
	 * Returns the current number of elements in the progress.
	 * 
	 * @return
	 */
	public int getCurrentState();

	public boolean isCompleted();

}
