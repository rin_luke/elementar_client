package com.elementar.gui.abstracts;

import java.util.ArrayList;

import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.PlayerData;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.modules.roots.MaploadingRoot;
import com.elementar.gui.modules.selection.GameclassCollectionModule;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.SeedTextfield;
import com.elementar.gui.widgets.TooltipTextArea;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;
import com.elementar.gui.widgets.listener.DisabledHandledListener;
import com.elementar.gui.widgets.listener.TooltipListener;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.Team;

/**
 * Abstract module where the concretes have to decide whether there is a
 * select-button. If yes the functionality has also to be implemented.
 * 
 * @author lukassongajlo
 *
 */
public abstract class AGameclassSelectionModule extends AChildModule {

	public static float PAD_TOP_LEFT_SIDE = 220;
	public static float PAD_TOP_RIGHT_SIDE = 190;

	private GameclassCollectionModule collection_module;

	private AOrderModule select_module;

	protected CustomTextbutton select_button;

	private SeedTextfield map_seed_field;
	private TooltipTextArea map_seed_tooltip_label;
	private CustomTextbutton demo_spirit_button;

	private boolean has_enter_world, has_select_button, has_buy_section;

	/**
	 * 
	 * @param visible
	 * @param drawOrder
	 * @param hasEnterWorld
	 * @param hasSelectButton
	 * @param hasBuySection
	 */
	public AGameclassSelectionModule(boolean visible, int drawOrder, boolean hasEnterWorld, boolean hasSelectButton,
			boolean hasBuySection) {
		super(visible, drawOrder);
		has_enter_world = hasEnterWorld;
		has_select_button = hasSelectButton;
		has_buy_section = hasBuySection;
		super.show();

	}

	@Override
	public void show() {
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		// handle disable buttons when quest is not fulfilled
		DrawableClient characterModel = GameclassCollectionModule.CHARACTER_MODEL;
		if (characterModel != null) {
			if (demo_spirit_button != null && demo_spirit_button.getState() == State.DISABLED)
				demo_spirit_button.setState(State.NOT_SELECTED);
			if (select_button != null && select_button.getState() == State.DISABLED)
				select_button.setState(State.NOT_SELECTED);
		}

		/*
		 * upper modules like Lobby and World training contain code to cross-out
		 * selected game classes. To determine which game class is already selected
		 * access is needed to several player lists. In World training we use
		 * LogicTier.WORLD, in Lobby we use LogicTier.META
		 */
		if (GameclassCollectionModule.SELECTED_IS_CROSSED_OUT == true && select_button != null)
			select_button.setState(State.DISABLED);

	}

	@Override
	public void setVisibleGlobal(boolean visible) {
		super.setVisibleGlobal(visible);
		collection_module.setVisibleGlobal(visible);
		select_module.setVisibleGlobal(visible);
	}

	@Override
	public void loadWidgets() {
		if (has_select_button == true) {
			select_button = new CustomTextbutton(TextData.getWord("select"), StyleContainer.button_bold_30_style,
					StaticGraphic.gui_bg_btn_custom1, Align.center);
			select_button.setSound(ClickSoundType.HEAVY);
		}
		if (has_enter_world == true) {
			map_seed_field = new SeedTextfield(StaticGraphic.gui_bg_demospirit_inputfield, "Seed");
			map_seed_field.setStyle(StyleContainer.textfield_bold_18_style);
			map_seed_field.setAlignment(Align.center);
			map_seed_tooltip_label = new TooltipTextArea(
					"Type in a map seed, a combination of numbers and letters to generate a prearranged map.", Shared.WIDTH5);
			demo_spirit_button = new CustomTextbutton(TextData.getWord("demoSpirit"),
					StyleContainer.button_bold_30_style, StaticGraphic.gui_bg_demospirit, Align.center);
			demo_spirit_button.setSound(ClickSoundType.HEAVY);
		}
	}

	@Override
	public void setLayout() {

		float padTop = has_buy_section == true ? 350 : 100;

		if (has_enter_world == true) {
			CustomTable table = new CustomTable();
			table.setFillParent(true);
			tables.add(table);

			table.padLeft(1025).padTop(padTop);
			table.add(demo_spirit_button);
			table.row();
			table.add(map_seed_field);

			map_seed_tooltip_label.setPosition(tables, table, map_seed_field.getBoundingButton());
		}

	}

	@Override
	public void setListeners() {
		if (has_enter_world == true) {
			demo_spirit_button.addListener(new DisabledHandledListener(demo_spirit_button) {
				@Override
				public void afterDisabledCheck() {

					logic_tier.reset();

					DrawableClient characterModel = GameclassCollectionModule.CHARACTER_MODEL;
					MetaClient metaClient = new MetaClient(PlayerData.name,
							characterModel.getGameclass().getNameAsEnum(), characterModel.getMetaClient().skin_index,
							Team.TEAM_1);

					OmniClient myOmniClient;

					logic_tier.setMyClient(
							myOmniClient = new OmniClient(metaClient, false, Location.CLIENTSIDE_TRAINING));

					// add to world realm
					LogicTier.WORLD_PLAYER_MAP.put(myOmniClient.getMetaClient().connection_id, myOmniClient);

					gui_tier.changeRootModule(new MaploadingRoot(GUIUtil.getValidSeed(map_seed_field)));
				}
			});

			map_seed_field.addListener(new TooltipListener(map_seed_tooltip_label));
			map_seed_field.getBoundingButton().addListener(new TooltipListener(map_seed_tooltip_label));
		}

		if (has_select_button == true)
			select_button.addListener(getSelectListener(select_button));

	}

	/**
	 * Only matters if {@link #has_select_button} is <code>true</code>
	 * 
	 * @param selectButton
	 * 
	 * @return
	 */
	public abstract DisabledHandledListener getSelectListener(CustomTextbutton selectButton);

	@Override
	public void loadSubModules() {

		collection_module = new GameclassCollectionModule(visible, draw_order, has_buy_section);
		select_module = new AOrderModule() {

			@Override
			public void setLayout() {
				if (has_select_button == true) {
					CustomTable tableSelectBtn = new CustomTable();
					tableSelectBtn.setFillParent(true);
					tables.add(tableSelectBtn);

					tableSelectBtn.padTop(350);
					tableSelectBtn.add(select_button);
				}
			}
		};
	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		subModules.add(collection_module);
		subModules.add(select_module);
	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		if (has_enter_world == true) {
			widgets.add(map_seed_field);
			widgets.add(demo_spirit_button);
		}

		if (has_select_button == true)
			widgets.add(select_button);

		return new HoverHandler(widgets);
	}

	@Override
	public void unfocus() {
		super.unfocus();
		if (has_enter_world == true)
			map_seed_field.unfocusWidget();
	}

	public CustomTextbutton getSelectButton() {
		return select_button;
	}

}
