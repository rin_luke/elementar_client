package com.elementar.gui.abstracts;

import static com.elementar.logic.util.Shared.CLEAR_BLUE;
import static com.elementar.logic.util.Shared.CLEAR_GREEN;
import static com.elementar.logic.util.Shared.CLEAR_RED;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Graphics;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.codedisaster.steamworks.SteamAPI;
import com.elementar.gui.AGUITier;
import com.elementar.gui.GUITier;
import com.elementar.gui.util.CustomStage;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.widgets.TooltipTextArea;
import com.elementar.logic.util.Shared;
import com.esotericsoftware.spine.SkeletonRenderer;

public class BasicScreen extends InputAdapter implements Screen {

	private AModule root_module;

	public static final ShapeRenderer SHAPE_RENDERER = new ShapeRenderer();
	public static final SkeletonRenderer SKELETON_RENDERER = new SkeletonRenderer();

	public static float ANIMATION_TIME;
	public static TooltipTextArea TOOLTIP_DELAYED;

	/**
	 * 1 - all regular stuff
	 * <p>
	 * 2 - CoverModules like OptionsScreen
	 * <p>
	 * 3 - Tooltips and notifications
	 */
	public static int DRAW_ORDER;

	public static final int DRAW_REGULAR = 0;
	public static final int DRAW_NOTIFICATIONS = 1;
	public static final int DRAW_COVERMODULE_1 = 2;
	public static final int DRAW_COVERMODULE_2 = 3;

	private ShaderPolyBatch batch;
	private InputMultiplexer multiplexer;

	private AGUITier gui_tier;

	public BasicScreen() {

		gui_tier = AGUITier.getInstance();

		// not important which batch
		this.batch = (ShaderPolyBatch) gui_tier.getStageByDrawOrder(0).getBatch();

		multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(this);
		multiplexer.addProcessor(gui_tier.getStageByDrawOrder(DRAW_COVERMODULE_2));
		multiplexer.addProcessor(gui_tier.getStageByDrawOrder(DRAW_COVERMODULE_1));
		multiplexer.addProcessor(gui_tier.getStageByDrawOrder(DRAW_NOTIFICATIONS));
		multiplexer.addProcessor(gui_tier.getStageByDrawOrder(DRAW_REGULAR));

		Gdx.input.setInputProcessor(multiplexer);

	}

	@Override
	public void render(float delta) {

		if (Shared.STEAM_ON == true)
			SteamAPI.runCallbacks();

		Gdx.graphics.getGL20().glClearColor(CLEAR_RED / 255f, CLEAR_GREEN / 255f, CLEAR_BLUE / 255f, 1);
		Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		ANIMATION_TIME += delta;

		if (TOOLTIP_DELAYED != null)
			TOOLTIP_DELAYED.updateDelay(delta);

		root_module.update(delta);

		// draw world-layer
		root_module.drawWorld();

		// GUI
		// 0 = regular, 1 = notifications, 2 = cover1, 3 = cover2
		batch.setShader(batch.sprite_shader);
		for (int i = 0; i < gui_tier.getNumberStages(); i++)
			if (root_module.isLayerVisible(i) == true)
				drawRoutine(i, delta);

		// position and draw cursor sprite
//		if (root_module != null && root_module.stage != null)
//			GUITier.MY_CURSOR.render(batch, root_module.stage.getCamera());

	}

	private void drawRoutine(int drawOrder, float delta) {
		CustomStage stage = gui_tier.getStageByDrawOrder(drawOrder);

		stage.act(delta);

		DRAW_ORDER = drawOrder;

		stage.draw();
		batch.reset();

		stage.drawOverlap();
		// batch.brightness = 0;
		// batch.contrast = 1;
		batch.reset();

	}

	@Override
	public void resize(int width, int height) {
		// Note: same viewport obj in all stages

		gui_tier.getStageByDrawOrder(0).getViewport().update(width, height, true);
		root_module.resize(width, height);

	}

	@Override
	public void show() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		root_module.dispose();
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		root_module.touchUp(screenX, screenY, pointer, button);
		return super.touchUp(screenX, screenY, pointer, button);
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		root_module.touchDown(screenX, screenY, pointer, button);
		if (button == Input.Buttons.LEFT)
			root_module.unfocus();

		return super.touchDown(screenX, screenY, pointer, button);
	}

	boolean debug_lines = false;

	@Override
	public boolean keyDown(int keycode) {

		root_module.keyDown(keycode);

		if (keycode == Keys.ESCAPE)
			root_module.clickedEscapeComposite();

		/*
		 * TEST START
		 */
		if (keycode == Keys.G && Shared.DEBUG_MODE_DEBUGLINES == true) {
			debug_lines = !debug_lines;
			gui_tier.setDebugStages(debug_lines);
			Shared.BOX2D_DEBUGLINES = !Shared.BOX2D_DEBUGLINES;
		}

		return super.keyDown(keycode);
	}

	@Override
	public boolean keyUp(int keycode) {
		root_module.keyUp(keycode);

		if (Gdx.input.isKeyPressed(Keys.SYM)) {
			String OS = System.getProperty("os.name").toLowerCase();
			if (OS.contains("win") == true)
				// minimize
				((Lwjgl3Graphics) Gdx.graphics).getWindow().iconifyWindow();

		}

		return super.keyUp(keycode);
	}

	@Override
	public boolean scrolled(float amountX, float amountY) {
		root_module.scrolledComposite(amountY);
		return super.scrolled(amountX, amountY);
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {

		root_module.mouseMoved(screenX, screenY);
		return super.mouseMoved(screenX, screenY);
	}

	public void setRootModule(AModule rootModule) {
		this.root_module = rootModule;
	}

	public AModule getRootModule() {
		return root_module;
	}

}
