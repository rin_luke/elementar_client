package com.elementar.gui.abstracts;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.elementar.data.container.AudioData;
import com.elementar.data.container.PlayerData;
import com.elementar.gui.modules.MenuNavigationModule;
import com.elementar.logic.network.requests.PlayerJoinRequest;

/**
 * Root module
 * 
 * @author lukassongajlo
 * 
 */
public abstract class ARootMenuModule extends ARootNonWorldModule {

	private float timer_joining = 0;

	public static MenuNavigationModule MAINMENU_NAVIGATION_MODULE;

	public ARootMenuModule() {
		// using the convenience constructor
		super();

		AudioData.set(AudioData.music_gui2, null);

		client_gameserver_connection.disconnect();
	}
	
	@Override
	public void update(float delta) {
		super.update(delta);
		

		// update timer
		timer_joining -= Gdx.graphics.getDeltaTime();

		
	}

	@Override
	public void loadSubModules() {
		/*
		 * If we wouldn't integrate an existing MAINMENU_NAVIGATION module, all previous
		 * selected buttons in the sub nav bar would be reseted by re-creating the
		 * MAINMENU_NAVIGATION module.
		 * 
		 * By integrating the MAINMENU_NAVIGATION we maintain the selection of the
		 * nav-buttons, so a root change won't affect the selected buttons of the
		 * sub-navigation bar.
		 */
		if (integrateModule(MAINMENU_NAVIGATION_MODULE) == false)
			MAINMENU_NAVIGATION_MODULE = new MenuNavigationModule();

	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		subModules.add(MAINMENU_NAVIGATION_MODULE);
	}

	public void sendPlayerJoinRequestToGameserver(String password, boolean isHost) {

		// every second we try to join.
		if (timer_joining <= 0) {
			client_gameserver_connection.getClient().sendUDP(new PlayerJoinRequest(PlayerData.name, password, isHost));
			timer_joining = 1;
		}

	}

}
