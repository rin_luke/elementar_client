package com.elementar.gui.abstracts;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.elementar.data.container.AudioData;
import com.elementar.data.container.OptionsData;
import com.elementar.data.container.TextData;
import com.elementar.data.container.util.GameclassSkin;
import com.elementar.gui.AGUITier;
import com.elementar.gui.GUITier;
import com.elementar.gui.modules.ErrorModule;
import com.elementar.gui.modules.PurchaseSuccessModule;
import com.elementar.gui.modules.StatsModule;
import com.elementar.gui.modules.host.GameserverConfigModule;
import com.elementar.gui.modules.options.AudioModule;
import com.elementar.gui.modules.options.OptionsModule;
import com.elementar.gui.modules.roots.HostRoot;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.CustomTextfield;
import com.elementar.gui.widgets.GameclassIconbutton;
import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.Unfocusable;
import com.elementar.logic.LogicTier;
import com.elementar.logic.network.connection.ClientGameserverConnection;
import com.elementar.logic.network.connection.ClientMainserverConnection;

/**
 * Each screen consists of a root-module. That root consists of several children
 * (named {@link #sub_modules} ). Each children have also children, so the whole
 * screen has a hierarchical structure beginning with the root.
 * <p>
 * Furthermore each module consists of several widgets - created in
 * {@link #loadWidgets()} - and which should be add in
 * {@link #addWidgets(ArrayList)}. Both methods must be implemented, so don't
 * worry about them (there are some more but all of them have a describing
 * comment). The backend will add these widgets to the stage.
 * <p>
 * In {@link #createHoverHandler(ArrayList)} you have the chance to add all
 * widgets to a list and create a hover-handler, so that handler can manage the
 * hover effects.
 * <p>
 * If you have widgets which have a certain behavior when they get unfocused,
 * then they should implement the {@link Unfocusable} interface. Note that you
 * shall call their {@link Unfocusable#unfocusWidget()} method in
 * {@link #unfocus()}
 * <p>
 * Each module has several render steps per loop. If you want to draw something
 * you can decide one of the following method: {@link #drawWorld()} for ingame
 * stuff like characters in the world, {@link #drawBeforeStage()} and
 * {@link #drawAfterStage()}. Each method represents a certain step in the
 * draw-order so you can draw the right sprite in right place at the right time.
 * 
 * @author lukassongajlo
 * 
 */
public abstract class AModule implements Modular, Disposable {

	/**
	 * Used to layout options listing in several screens. For example
	 * {@link GameserverConfigModule} in {@link HostRoot} or {@link AudioModule} in
	 * {@link OptionsModule}
	 */
	public final static float LINE_DISTANCE_TO_NEXT_ROW = 60;
	/**
	 * Used to layout label-button- or label-inputfield-pairs in options listing
	 */
	public final static float DISTANCE_BETWEEN_ACTORS = 80;

	protected ArrayList<AModule> sub_modules = new ArrayList<>();
	protected ArrayList<AModule> tabs = new ArrayList<>();

	protected ArrayList<CustomTable> tables = new ArrayList<>();

	protected HoverHandler hover_handler;

	/**
	 * to detect whether a layer has to be drawn, see for example
	 * {@link #drawSecond()} and {@link #drawBeforeStage()}.
	 */
	protected boolean visible;

	protected static OptionsModule OPTIONS_MODULE;
	protected static StatsModule STATS_MODULE;
	protected static ErrorModule ERROR_MODULE;
	protected static PurchaseSuccessModule PURCHASE_SUCCESS_MODULE;

	private boolean is_root;

	/**
	 * the {@link GUITier#changeRootModule(AModule)} method is called inside
	 * {@link #update(float)} and changes the root module but note that the old root
	 * module finishes its last {@link #update(float)}. If {@link #disposed} is set
	 * to <code>true</code> you know that the old root is in its last cycle.
	 */
	protected boolean disposed = false;

	/**
	 * draw_order describes when the this module should be drawed. One of these:
	 * {@link BasicScreen#DRAW_REGULAR}, {@link BasicScreen#DRAW_COVERMODULE_1} or
	 * {@link BasicScreen#DRAW_COVERMODULE_2}. Notifications aren't handled as
	 * module.
	 */
	protected int draw_order;

	protected boolean intro;

	// für besseren Zugriff
	protected ClientMainserverConnection client_mainserver_connection;
	protected ClientGameserverConnection client_gameserver_connection;
	protected BasicScreen screen;
	protected AGUITier gui_tier;
	protected LogicTier logic_tier;
	protected Stage stage;
	protected ShaderPolyBatch batch;

	/**
	 * 
	 * @param visible
	 * @param isRoot
	 * @param drawOrder
	 */
	public AModule(boolean visible, boolean isRoot, int drawOrder) {
		this.gui_tier = GUITier.getInstance();
		this.visible = visible;
		this.is_root = isRoot;
		this.draw_order = drawOrder;
		this.logic_tier = gui_tier.getLogicTier();

		this.screen = gui_tier.getScreen();
		this.stage = gui_tier.getStageByDrawOrder(drawOrder);
		this.batch = (ShaderPolyBatch) stage.getBatch();

		this.client_mainserver_connection = ClientMainserverConnection.getInstance();

		// no connection established until selecting new server
		this.client_gameserver_connection = ClientGameserverConnection.getInstance();

	}

	public void show() {

		// _______Create__Own__Widgets______
		loadWidgets();
		setLayout();
		setListeners();
		hover_handler = createHoverHandler(new ArrayList<StatePossessable>());
		// without watching submodules
		setVisibleLocal();

		if (is_root == true && intro == false) {
			OPTIONS_MODULE = new OptionsModule(getOptionsDrawOrder());
			sub_modules.add(OPTIONS_MODULE);

			if (integrateModule(STATS_MODULE) == false)
				STATS_MODULE = new StatsModule(OptionsData.fps_on);
			sub_modules.add(STATS_MODULE);

			ERROR_MODULE = new ErrorModule();
			sub_modules.add(ERROR_MODULE);

			PURCHASE_SUCCESS_MODULE = new PurchaseSuccessModule();
			sub_modules.add(PURCHASE_SUCCESS_MODULE);

		}

		// ________Handle__Submodules________
		loadSubModules();
		addSubModules(sub_modules);

		addTabs(tabs);
		// might be overriden and looks also at submodules
		setVisibleGlobal(visible);

	}

	/**
	 * the options draw order is {@link BasicScreen#DRAW_COVERMODULE_2}, when
	 * ingame-screen and otherwise {@link BasicScreen#DRAW_COVERMODULE_1}
	 */

	private int getOptionsDrawOrder() {
		// determine options draw order
		if (this instanceof ARootWorldModule)
			return BasicScreen.DRAW_COVERMODULE_2;
		return BasicScreen.DRAW_COVERMODULE_1;
	}

	/**
	 * Existing module will be integrate into the scene. Otherwise the widgets won't
	 * be added to stages and the hoverhandler won't be added to the hover-handler
	 * pool
	 * <p>
	 * Note if param is null this method will return false -> Then you have to
	 * create the module (new Module()...)
	 * <p>
	 * For an example please read {@link ARootMenuModule#ARootMenuModule()}.
	 * 
	 * @param module
	 * 
	 * @return
	 */
	public static boolean integrateModule(AModule module) {
		if (module == null)
			return false;
		module.setVisibleGlobal(module.visible);
		HoverHandler.add(module.getHoverHandler());
		for (AModule subModule : module.sub_modules) {
			subModule.setVisibleGlobal(subModule.visible);
			HoverHandler.add(subModule.getHoverHandler());
		}

		return true;
	}

	/**
	 * Adds all {@link CustomTable} of {@link #tables} to the {@link #stage} (always
	 * visible = true). 'Local' because this method considers only own tables and
	 * none of the sub-modules
	 */
	private void setVisibleLocal() {
		for (Table table : tables) {
			stage.addActor(table);
			table.validate();
		}
	}

	/**
	 * By default this method considers only its own {@link CustomTable} objects.
	 * Might be overridden to consider also sub-modules
	 * 
	 * @param visible
	 */
	public void setVisibleGlobal(boolean visible) {
		this.visible = visible;
		if (visible == false)
			for (CustomTable table : tables)
				table.remove();
		else
			setVisibleLocal();
	}

	/**
	 * Call super-method to consider sub_modules
	 */
	public void update(float delta) {

		// update game server logic if you are host
		if (logic_tier != null && logic_tier.getGameserverLogic() != null)
			logic_tier.getGameserverLogic().update();

		if (is_root == true) {
			HoverHandler.update();
			AudioData.updateTweenManager(delta);

			// update steam purchases
			if (GUITier.STEAM_INTERFACE != null && GUITier.STEAM_INTERFACE.purchase_successful == true
					&& GameclassIconbutton.LAST_PURCHASED_SKIN != null) {

				GUITier.STEAM_INTERFACE.purchase_successful = false;

				GameclassSkin skin = GameclassIconbutton.LAST_PURCHASED_SKIN;

				// get skin data
				String name = skin.getGameclassName().toString().toLowerCase();
				int skinIndex = skin.getIndex();

				// prepare module
				PURCHASE_SUCCESS_MODULE.getClientModel().setSkin(name, skinIndex, false);
				PURCHASE_SUCCESS_MODULE.getStatementLabel().setText(TextData.getWord("congratsUnlock") + " '"
						+ GameclassIconbutton.LAST_PURCHASED_SKIN.getDescription() + "'");

				// set visible true
				PURCHASE_SUCCESS_MODULE.setVisibleGlobal(true);

			}

		}

		for (AModule subModule : sub_modules)
			subModule.update(delta);

	}

	/**
	 * used in {@link BasicScreen} to reduce batch.begin() / end() calls
	 * 
	 * @param drawOrder
	 * @return
	 */
	public boolean isLayerVisible(int drawOrder) {
		if (drawOrder == draw_order && visible == true)
			return visible;
		for (AModule subModule : sub_modules)
			if (subModule.isLayerVisible(drawOrder) == true)
				return true;
		return false;
	}

	public boolean isVisible() {
		return visible;
	}

	/**
	 * This is the first draw method that is called in a render loop. Reserved for
	 * Ingamestuff like backgrounds of the world, lights and characters
	 */
	public void drawWorld() {

	}

	/**
	 * /** Call super-method to consider sub_modules
	 */
	public void keyDown(int keycode) {
		for (AModule subModule : sub_modules)
			if (subModule.visible == true)
				subModule.keyDown(keycode);

	}

	/**
	 * This method calls {@link #clickedEscape()} of submodules and of this module.
	 * 
	 * If one submodule handled the escape-event, the clickedEscape-code of this
	 * module won't be touched.
	 * 
	 * @return true if event is handled by this module, false when
	 */
	public boolean clickedEscapeComposite() {
		boolean eventHandledBySubmodule = false;
		boolean temp = false;
		for (AModule subModule : sub_modules)
			if (subModule.visible == true) {
				temp = subModule.clickedEscapeComposite();
				if (temp == true) {
					// once handled, ever handled (can't be changed by following
					// submodules)
					eventHandledBySubmodule = true;
					/*
					 * break to avoid handle other clickedEscaped of /modules that are on the same
					 * hierarchy level
					 */
					break;
				}
			}

		if (eventHandledBySubmodule == false)
			return clickedEscape();

		return true;

	}

	/**
	 * 
	 * @return {@code false} if the event is not fully handled by this method.
	 *         Higher modules should also handle the escape event.
	 *         <p>
	 *         {@code true} means the event is fully handled by this modules. No
	 *         other higher module should handle this event. If higher (
	 *         hierarchically spoken) modules' escape methods should be called a
	 *         second click on escape is needed.
	 */
	protected abstract boolean clickedEscape();

	/**
	 * Call super-method to consider sub_modules
	 */
	public void keyUp(int keycode) {
		for (AModule subModule : sub_modules)
			if (subModule.visible == true)
				subModule.keyUp(keycode);
	}

	/**
	 * Call super-method to consider sub_modules
	 */
	public void touchDown(int screenX, int screenY, int pointer, int button) {
		for (AModule subModule : sub_modules)
			if (subModule.visible == true)
				subModule.touchDown(screenX, screenY, pointer, button);

	}

	public void touchUp(int screenX, int screenY, int pointer, int button) {
		for (AModule subModule : sub_modules)
			if (subModule.visible == true)
				subModule.touchUp(screenX, screenY, pointer, button);

	}

	/**
	 * Call super-method to consider sub_modules. Use this method to call the
	 * {@link Unfocusable#unfocusWidget()} method of your widgets which implements
	 * the {@link Unfocusable} interface, for example the {@link CustomTextfield}.
	 */
	public void unfocus() {
		for (AModule subModule : sub_modules)
			if (subModule.visible == true)
				subModule.unfocus();
	}

	@Override
	public void dispose() {
		disposed = true;
		for (AModule subModule : sub_modules)
			if (subModule.visible == true)
				subModule.dispose();
	}

	/**
	 * 
	 * If a subModule handled Returns {@code true} when the scrolled event is
	 * handled by {@link #sub_modules}.
	 * 
	 * @param amount
	 * @return
	 */
	public boolean scrolledComposite(float amount) {
		boolean eventHandledBySubmodule = false;
		boolean temp = false;
		for (AModule subModule : sub_modules)
			if (subModule.visible == true) {
				temp = subModule.scrolledComposite(amount);
				if (temp == true) {
					// once handled, ever handled (can't be changed by following
					// submodules)
					eventHandledBySubmodule = true;
					/*
					 * break to avoid handle other clickedEscaped of /modules that are on the same
					 * hierarchy level
					 */
					break;
				}
			}

		if (eventHandledBySubmodule == false)
			return scrolled(amount);

		return true;
	}

	/**
	 * Override this method if you want to that this module should handle the
	 * scrolled event.
	 * 
	 * @param amount
	 * @return {@code false} if the event is not fully handled by this method.
	 *         Higher modules should also handle the scrolled event.
	 *         <p>
	 *         {@code true} means the event is fully handled by this modules. No
	 *         other higher module should handle this event.
	 */
	protected boolean scrolled(float amount) {
		return false;
	}

	public void mouseMoved(int screenX, int screenY) {

	}

	public void resize(int width, int height) {
		// for (AbstractModule subModule : sub_modules)
		// if (subModule.visible == true)
		// subModule.resize(width, height);
	}

	/**
	 * 
	 * @param toTab
	 */
	public void changeTab(AModule toTab) {
		for (AModule module : tabs)
			module.setVisibleGlobal(false);
		toTab.setVisibleGlobal(true);
	}

	/**
	 * Optional. Use this method to add tabs for {@link #changeTab(AModule)}-
	 * functionality
	 * 
	 * @param tabs
	 */
	protected void addTabs(ArrayList<AModule> tabs) {
	}

	/**
	 * The parameter is a list to group all widgets.
	 * <p>
	 * The returned {@link HoverHandler}-object is necessary for only one case (see
	 * {@link #getHoverHandler()})
	 * 
	 * <p>
	 * If two widgets lie on top of each other the adding-order of these widgets
	 * determines the hover-priority (like draw-order, last one > previous one).
	 * <p>
	 * NOTE: If the state of two or more widgets are changed to SELECTED in one
	 * frame, the adding-order determines which will be selected (like draw-order,
	 * last one > previous one).
	 * 
	 * @param widgets
	 * @return
	 */
	protected abstract HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets);

	/**
	 * At the moment just needed to integrate an existing mainmenu-module in the new
	 * root
	 * 
	 * @return
	 */
	public HoverHandler getHoverHandler() {
		return hover_handler == null ? HoverHandler.NOT_NULL_HOVERHANDLER : hover_handler;
	}

	public ArrayList<AModule> getTabs() {
		return tabs;
	}

	public ArrayList<AModule> getSubModules() {
		return sub_modules;
	}

	public void setIntro() {
		intro = true;
	}

}
