package com.elementar.gui.abstracts;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Scaling;
import com.elementar.data.container.StaticGraphic;
import com.elementar.gui.modules.options.OptionsModule;
import com.elementar.gui.widgets.CustomImage;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.HoverHandler;

/**
 * 
 * @author lukassongajlo
 * 
 */
public abstract class AChildCoverModule extends AChildModule {

	/**
	 * covers the whole screen
	 */
	private Image bounding_image;

	/**
	 * This sprite covers a click-safe area like in {@link OptionsModule}
	 */
	protected Sprite background_sprite;

	/**
	 * Consists of 2 actors, the {@link #inner_image} and {@link #table_inner}
	 */
	protected WidgetGroup inner_group;
	protected CustomTable table_clicksafe, table_inner;
	/**
	 * clicks from player inside this image won't be considered. I name this a
	 * "clicksafe" area. Note that the dimensions of this image are derived by
	 * {@link #background_sprite} dimensions.
	 */
	protected CustomImage inner_image;

	protected boolean escape_enabled;

	/**
	 * Use a not null background sprite to determine a click-safe area where the
	 * player is allowed to click without closing the covermodule.
	 * 
	 * @param backgroundSprite
	 *            , not the grey one, this sprite is in front and is used for
	 *            putting widget on it. The sprite is used to determine
	 *            dimensions of the area where the player can click without
	 *            closing the cover-module
	 * @param drawOrder
	 *            , use therefore {@link BasicScreen#DRAW_COVERMODULE_1} or
	 *            {@link BasicScreen#DRAW_COVERMODULE_2}
	 * @param escapeEnabled ,
	 *            if <code>true</code> the escape functionality will be applied
	 *            by clicking on non-clicksafe spaces
	 */
	public AChildCoverModule(Sprite backgroundSprite, int drawOrder, boolean escapeEnabled) {
		super(false, drawOrder);
		background_sprite = backgroundSprite;
		escape_enabled = escapeEnabled;
		super.show();
	}

	@Override
	public void show() {
	}
	
	@Override
	public void update(float delta) {
		super.update(delta);
		
		if (isVisible() == true)
			ARootWorldModule.CURSOR_UI_CHECK.setAtUIModules(true);
		
	}

	@Override
	public void loadWidgets() {

		bounding_image = new Image(StaticGraphic.getDrawable("gui_bg_screencover"), Scaling.fill);
		inner_image = new CustomImage(background_sprite);

	}

	@Override
	public void setLayout() {

		CustomTable tableOuterBounding = new CustomTable();
		tableOuterBounding.setFillParent(true);
		tables.add(tableOuterBounding);

		tableOuterBounding.add(bounding_image).expand().fill();

		table_clicksafe = new CustomTable();
		table_clicksafe.setFillParent(true);
		tables.add(table_clicksafe);

		table_inner = new CustomTable();

		// holds clicksafe image + table (for widgets inside the clicksafe area)
		inner_group = new WidgetGroup();
		inner_group.addActor(inner_image);
		inner_group.addActor(table_inner);

		table_clicksafe.add(inner_group).width(inner_image.getSprite().getWidth())
				.height(inner_image.getSprite().getHeight()).padTop(100);

	}

	@Override
	public void setListeners() {
		bounding_image.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				if (escape_enabled == true)
					clickedEscapeComposite();
				return true;
			}

			// needed to avoid that underlying overlapping widgets like tooltips
			// will be displayed.
			@Override
			public boolean mouseMoved(InputEvent event, float x, float y) {
				return true;
			}
		});

		// needed to avoid clicking on buttons behind the inner image background
		table_clicksafe.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}
		});

	}

	/**
	 * If visible is <code>false</code> the global hover handlers can be
	 * re-activated, otherwise if the module's visibility is set to
	 * <code>true</code> all hover handlers should be deactivated except the
	 * hover handler of this module.
	 */
	@Override
	public void setVisibleGlobal(boolean visible) {
		super.setVisibleGlobal(visible);
		if (visible == false)
			HoverHandler.setActiveGlobally(true);
		else {
			HoverHandler.setActiveGlobally(false);
			if (hover_handler != null)
				hover_handler.setActive(true);
		}
	}
}
