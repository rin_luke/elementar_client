package com.elementar.gui.abstracts;

import com.elementar.data.container.AnimationContainer;
import com.elementar.gui.util.CustomSkeleton;
import com.elementar.gui.widgets.CustomSpineWidget;
import com.elementar.gui.widgets.CustomTable;

/**
 * This class belongs to the root modules family and has an animated background
 * which is changeable.
 * 
 * @author lukassongajlo
 * 
 */
public abstract class ARootNonWorldModule extends AModule {

	/**
	 * 
	 * @param skeleton
	 *            , background skeleton
	 * @param animation
	 *            , background animation
	 * @param drawOrder
	 */
	public ARootNonWorldModule(CustomSkeleton skeleton, String animationString) {
		super(true, true, BasicScreen.DRAW_REGULAR);

		CustomSpineWidget bgSpineActor = new CustomSpineWidget(skeleton, animationString, true);

		bgSpineActor.setAnimationTime(BasicScreen.ANIMATION_TIME);

		// loadWidgets + setLayout
		CustomTable tableSkeleton = new CustomTable();
		tableSkeleton.setFillParent(true);
		tableSkeleton.center();
		tables.add(tableSkeleton);

		tableSkeleton.add(bgSpineActor);

	}

	/**
	 * convenience constructor: use this constructor for a standard background
	 * animation
	 */
	public ARootNonWorldModule() {
		this(AnimationContainer.skeleton_bg1_menu, "gui_bg_mainmenu");
	}

}
