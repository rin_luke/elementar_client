package com.elementar.gui.abstracts;

import java.util.ArrayList;

import com.elementar.gui.widgets.HoverHandler;
import com.elementar.gui.widgets.interfaces.StatePossessable;

/**
 * All abstract methods except {@link #setLayout()} are implemented. We use this
 * class to change the draw order of certain widgets.
 * 
 * @author lukassongajlo
 *
 */
public abstract class AOrderModule extends AChildModule {

	public AOrderModule() {
		super(true);
	}

	@Override
	public void loadWidgets() {

	}

	@Override
	public void setListeners() {

	}

	@Override
	public void loadSubModules() {

	}

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {

	}

	@Override
	protected HoverHandler createHoverHandler(ArrayList<StatePossessable> widgets) {
		return null;
	}

}
