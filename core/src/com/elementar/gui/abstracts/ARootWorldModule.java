package com.elementar.gui.abstracts;

import static com.elementar.logic.util.Shared.BOX2D_DEBUGLINES;
import static com.elementar.logic.util.Shared.DEBUG_MODE_RENDER_LIGHT;
import static com.elementar.logic.util.Shared.SEND_AND_UPDATE_RATE_WORLD;
import static com.elementar.logic.util.Shared.WORLD_VIEWPORT_HEIGHT;
import static com.elementar.logic.util.Shared.WORLD_VIEWPORT_WIDTH;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.AudioData;
import com.elementar.data.container.KeysConfiguration;
import com.elementar.data.container.OptionsData;
import com.elementar.data.container.ParticleContainer;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.KeysConfiguration.WorldAction;
import com.elementar.data.container.util.AdvancedKey;
import com.elementar.gui.modules.ingame.APlayerUIModule;
import com.elementar.gui.modules.ingame.ESCModule;
import com.elementar.gui.modules.ingame.MinimapManager;
import com.elementar.gui.modules.ingame.PlayerUITrainingModule;
import com.elementar.gui.modules.ingame.VictoryDefeatModule;
import com.elementar.gui.modules.roots.MaploadingRoot;
import com.elementar.gui.util.BGSkeleton;
import com.elementar.gui.util.BarDisplayManager;
import com.elementar.gui.util.CursorUICheck;
import com.elementar.gui.util.IslandFillingManager;
import com.elementar.gui.util.ParallaxCamera;
import com.elementar.gui.util.PlatformManager;
import com.elementar.gui.util.VegetationManager;
import com.elementar.gui.util.VegetationSprite;
import com.elementar.gui.widgets.CustomSpineWidget;
import com.elementar.gui.widgets.SkillButton.ComboFeedbackAnimation;
import com.elementar.logic.ClientHistory;
import com.elementar.logic.ClientHistoryEntry;
import com.elementar.logic.LogicTier;
import com.elementar.logic.WorldCycleManager;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.SparseClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.skills.ASkill;
import com.elementar.logic.creeps.OmniCreep;
import com.elementar.logic.creeps.PhysicalCreep;
import com.elementar.logic.creeps.SparseCreep;
import com.elementar.logic.emission.effect.AEffectTimed;
import com.elementar.logic.map.MapBorder;
import com.elementar.logic.map.MapLoader;
import com.elementar.logic.map.MapManager;
import com.elementar.logic.map.Plant;
import com.elementar.logic.map.RandomIsland;
import com.elementar.logic.map.buildings.ABuilding;
import com.elementar.logic.map.buildings.Base;
import com.elementar.logic.map.buildings.Shrine;
import com.elementar.logic.map.buildings.Tower;
import com.elementar.logic.network.protocols.ClientGameserverProtocol;
import com.elementar.logic.network.requests.InputRequest;
import com.elementar.logic.network.util.SnapshotInterpolation;
import com.elementar.logic.util.MyRandom;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.ViewHandler;
import com.esotericsoftware.spine.Animation.MixBlend;
import com.esotericsoftware.spine.Animation.MixDirection;
import com.esotericsoftware.spine.Skeleton;

import box2dLight.Light;

/**
 * for root modules
 * 
 * @author lukassongajlo
 * 
 */
public abstract class ARootWorldModule extends AModule {

	public static CursorUICheck CURSOR_UI_CHECK = new CursorUICheck();

	public static float MAX_ZOOM_OUT = 0.95f, MAX_ZOOM_IN = 0.3f;

	public static boolean MINIMAP_DRAW_LEFT = true;

	private AtomicInteger bytes_sent = new AtomicInteger();

	protected APlayerUIModule player_ui_module;
	private ESCModule esc_module;
	private VictoryDefeatModule vic_def_module;

	protected ParallaxCamera world_cam;
	private Viewport world_viewport;

	protected Box2DDebugRenderer box2d_debug_renderer = new Box2DDebugRenderer();

	private PlatformManager platforms_manager;
	private IslandFillingManager island_filling_manager;

	private VegetationManager vegetation_manager;

	protected MapManager map_manager;
	private ArrayList<Plant> plants;

	private Vector2 left_bottom_vegetation_viewport;

	/**
	 * This viewport is bigger than the camera one, plants that are outside of it,
	 * get inactive because the client doesnt see them as well!
	 */
	protected Rectangle vegetation_viewport;

	/**
	 * Because the physic won't be checked each frame, it might be the case that a
	 * player press a key down and up without any physic interaction. So any
	 * key-release will be stored in this data structure and released just after the
	 * physic checks its state.
	 */
	protected ArrayList<AdvancedKey> released_keys = new ArrayList<>();

	protected OmniClient my_omni_client;

	protected DrawableClient my_drawable_client;
	protected PhysicalClient my_physical_client, server_sync_client;

	protected MapLoader map_loader;

	private ClientHistoryEntry current_history_entry = new ClientHistoryEntry();

	private Vector2 cam_pos_with_inertia = new Vector2();
	private Vector2 view_shift = new Vector2();

	protected boolean enable_dummy_movement = false;

	private float time_local;

	private Light light_cursor;

	private float accumulator;

	private Vector3 cursor_pos_vec3 = new Vector3();

	public Vector2 cursor_pos_vec2 = new Vector2();

	private BarDisplayManager bar_display_manager;

	private MinimapManager minimap_manager;

	protected WorldCycleManager world_cycle_manager;

	protected AtomicInteger correction_count = new AtomicInteger(0);

	public ARootWorldModule(MapLoader mapLoader) {
		super(true, true, BasicScreen.DRAW_REGULAR);

		// Log.set(Log.LEVEL_DEBUG);

		gui_tier.setCursor(gui_tier.cursor_world);

		map_loader = mapLoader;
		map_manager = map_loader.getMapManager();

		LogicTier.BUILDINGS_MAP = map_manager.getBuildings();

		// create bases and islands for para world (lights and server sync)
		MapManager.createMapWithGivenParameters(map_manager, logic_tier.getClientParaWorld());

		my_omni_client = LogicTier.getMyClient();
		// set fields
		my_drawable_client = my_omni_client.getDrawableClient();
		my_physical_client = my_omni_client.getPhysicalClient();
		server_sync_client = my_omni_client.getParaPhysicalClient();

		// create new view handler
		LogicTier.VIEW_HANDLER = new ViewHandler(map_manager, my_physical_client.getMetaClient().team,
				logic_tier.getClientParaWorld());

		initCameras();

		generateSprites();

		left_bottom_vegetation_viewport = new Vector2();
		vegetation_viewport = new Rectangle();

		minimap_manager = new MinimapManager(map_manager, my_omni_client);

		bar_display_manager = new BarDisplayManager(my_omni_client, stage.getCamera(), batch);

		world_cycle_manager = new WorldCycleManager(map_manager, LogicTier.CREEP_MAP, LogicTier.CREEP_LIVING_LIST);

		LogicTier.ENTERED_WORLD = true;

	}

	public abstract boolean isTrainingsWorld();

	protected void initCameras() {
		world_cam = ParallaxCamera.getInstance();

		world_cam.zoom = MAX_ZOOM_OUT;

		world_viewport = new ExtendViewport(WORLD_VIEWPORT_WIDTH, WORLD_VIEWPORT_HEIGHT, world_cam);
		// world_viewport.apply();

		// update window
		resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

	}

	protected void setCameraToPlayer() {

		cam_pos_with_inertia.lerp(my_drawable_client.pos, 0.1f);

		world_cam.position.set(cam_pos_with_inertia, 0);

		world_cam.update();
	}

	private void generateSprites() {

		island_filling_manager = new IslandFillingManager();
		platforms_manager = new PlatformManager();

		vegetation_manager = new VegetationManager(logic_tier.getTweenManagerVegetation());

		MapBorder mapBorder = map_manager.getMapBorder();

		map_manager.createRandom();
		for (BGSkeleton bg : AnimationContainer.bg1_skeletons) {
			/*
			 * note that bg1 has no random position, no rotation and just one skin
			 */
			bg.setPosition(0, 0);
			bg.setSkin("world_bg1_" + map_manager.getRandom().random(1, 2));
		}
		setBGPositions(AnimationContainer.bg2_skeletons, "world_bg2_", 0f, 360f);
		setBGPositions(AnimationContainer.bg3_skeletons, "world_bg3_", -30f, 30f);
		setBGPositions(AnimationContainer.bg4_skeletons, "world_bg4_", 0f, 360f);
		map_manager.disposeRandom();

		// filling and vegetation for mapborder
		island_filling_manager.place(mapBorder.getLeftFillingVertices(), mapBorder.getLeftFillingVertices()[0]);
		island_filling_manager.place(mapBorder.getRightFillingVertices(), mapBorder.getRightFillingVertices()[0]);

		platforms_manager.place(mapBorder.getLocalVertices());

		vegetation_manager.place(mapBorder.getLocalVertices());

		for (

		RandomIsland island : map_manager.getIslands()) {
			// foreground
			platforms_manager.place(island.getGlobalVertices());
			island_filling_manager.place(island.getGlobalVertices(), island.getStartPoint());
			vegetation_manager.place(island.getGlobalVertices());
		}

		// set plants hitboxes
		plants = new ArrayList<Plant>(vegetation_manager.getSprites().size());
		for (VegetationSprite sprite : vegetation_manager.getSprites()) {
			sprite.calcX2Y2AndX3Y3(StaticGraphic.vegetation_width, StaticGraphic.vegetation_height);
			if (VegetationSprite.hasPhysicalBody(OptionsData.vegetation_intersection_level, sprite.getSize()) == true) {
				Plant plant = new Plant(logic_tier.getClientMainWorld(), sprite);
				sprite.setPhysicalElement(plant);
				plants.add(plant);
			}
		}
	}

	private void setBGPositions(ArrayList<BGSkeleton> skeletons, String skinName, float startAngle, float endAngle) {

		MapBorder mapBorder = map_manager.getMapBorder();
		MyRandom random = map_manager.getRandom();

		for (BGSkeleton bg : skeletons) {
			bg.setSkin(skinName + "" + random.random(1, 2));
			bg.getRootBone().setRotation(random.random(startAngle, endAngle));

			boolean intersected;
			do {

				intersected = false;
				float x = random.random(mapBorder.sampling.x, // +
																// Shared.WORLD_VIEWPORT_WIDTH
																// * 0.5f,
						mapBorder.sampling.y);// - Shared.WORLD_VIEWPORT_WIDTH *
												// 0.5f);
				float y = random.random(mapBorder.sampling.w, // +
																// Shared.WORLD_VIEWPORT_HEIGHT
																// * 0.5f,
						mapBorder.sampling.z);// - Shared.WORLD_VIEWPORT_HEIGHT
												// * 0.5f);
				bg.setPosition(x, y);
				bg.setBoundingBox(x, y);
				for (BGSkeleton bgOther : skeletons)
					if (bgOther != bg && bgOther.getPolygon() != null)
						if (bg.getPolygon().getBoundingRectangle()
								.overlaps(bgOther.getPolygon().getBoundingRectangle()) == true) {
							intersected = true;
							break;
						}

			} while (intersected == true);

		}
	}

	@Override
	public void show() {
		super.show();

		// set and update audio by calling set(...,...)
		AudioData.set(AudioData.music_world_calm1, AudioData.global_ambient_tweenable);

		STATS_MODULE.setBytesSent(bytes_sent);
		STATS_MODULE.setCorrectionCount(correction_count);

	}

	@Override
	public void update(float delta) {

		// update cursor position
		cursor_pos_vec3.set(Gdx.input.getX(), Gdx.input.getY(), 0);
		ParallaxCamera.getInstance().unproject(cursor_pos_vec3);
		// convert vec3 into vec2
		cursor_pos_vec2.set(cursor_pos_vec3.x, cursor_pos_vec3.y);

		if (enable_dummy_movement == false) {
			my_drawable_client.cursor_pos.set(cursor_pos_vec2);
			my_physical_client.cursor_pos.set(cursor_pos_vec2);
		} else
			for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values())
				if (client.isDummy() == true) {
					client.getDrawableClient().cursor_pos.set(cursor_pos_vec2);
					client.getPhysicalClient().cursor_pos.set(cursor_pos_vec2);
				}

		CURSOR_UI_CHECK.setAtUIModules(false);

		if (isTrainingsWorld() == true) {
			PlayerUITrainingModule trainingUI = (PlayerUITrainingModule) player_ui_module;
			my_physical_client.setFirerate(trainingUI.getFirerate());
			my_drawable_client.setFirerate(trainingUI.getFirerate());
		}

		// update submodules, checks whether cursor is at UI (modules)
		super.update(delta);

		// set cursor
		view_shift = my_drawable_client.flipByCursor();

		if (player_ui_module.getComboRose().isActive() == true) {

			gui_tier.setCursor(gui_tier.cursor_invisible);
		} else {

			if (CURSOR_UI_CHECK.isAtUIModules() == true || CURSOR_UI_CHECK.isAtUIWidgets() == true) {
				gui_tier.setCursor(gui_tier.cursor_regular);
			} else {
				gui_tier.setCursor(gui_tier.cursor_world);
			}
		}

		vegetation_manager.update(delta);

		for (Plant plant : plants)
			plant.decrementAnimationTime(delta);

		/*
		 * UPDATE DATA: following lines provide an update by network for all drawable
		 * clients.
		 */
		if (isTrainingsWorld() == false) {
			logic_tier.updateBySnapshots();

			/*
			 * if the server isn't running anymore or the client lost its connection, the
			 * screen changes to the main menu
			 */
			if (client_gameserver_connection.getClient().isConnected() == false && disposed == false) {
				clickedEscape();
			}
		}

		/**
		 * world update 25 times per second
		 */
		accumulator += delta;
		while (accumulator >= SEND_AND_UPDATE_RATE_WORLD) {
			accumulator -= SEND_AND_UPDATE_RATE_WORLD;
			time_local += Shared.SEND_AND_UPDATE_RATE_WORLD;

			/*
			 * Set previous client with data from the last updated client to interpolate
			 * afterwards, have no effects on network players (except own player)
			 */
			for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {

				LogicTier.updateNonMovementData(client.getPhysicalClient(), client.getPrevClient());
				LogicTier.updatePosition(client.getPhysicalClient(), client.getPrevClient());
			}

			for (OmniCreep creep : LogicTier.CREEP_LIVING_LIST) {

				// calibrate creep...
				if (creep.getPhysicalCreep().getCurrentTarget() != null)
					// ... by target
					creep.getDrawableCreep().flipByTarget(creep.getPhysicalCreep().getCurrentTarget().getPos());
				else if (creep.getPhysicalCreep().getCurrentPotentialTarget() != null
						&& creep.getPhysicalCreep().isAtOrigin() == true)
					// ... by potential target
					creep.getDrawableCreep()
							.flipByTarget(creep.getPhysicalCreep().getCurrentPotentialTarget().getPos());
				else
					// ... by path
					creep.getDrawableCreep().flipByTarget(creep.getPhysicalCreep().getOrigin());

				LogicTier.updateProjectiles(creep.getPhysicalCreep().sparse_projectiles,
						creep.getPrevCreep().sparse_projectiles);
				LogicTier.updatePosition(creep.getPhysicalCreep(), creep.getPrevCreep());

			}

			if (isTrainingsWorld() == false) {

				// client prediction
				if (Shared.DEBUG_MODE_SYSOS_NETCODE_CLIENT == true)
					System.out.println("AbstractIngameScreen.update()			1. last data from network = "
							+ my_drawable_client.pos + " with id = " + LogicTier.MOST_RECENT_PROCESSED_ID);

				correctClientPredicition();
			} else {
				// update world cycles, e.g. creep waves
				world_cycle_manager.generate(logic_tier.getClientMainWorld());
			}

			/*
			 * TODO evtl. kann man hier performance sparen indem man schreine grundsätzlich
			 * ausschließt und gar nicht erst als "Building" implementiert (hierarchie
			 * ändern)
			 */

			for (ABuilding building : LogicTier.BUILDINGS_MAP.values())
				building.update();

			/*
			 * problem, insgesamt fällt mir auf, dass die player list gefüllt sein kann aber
			 * es immer wieder sein kann, dass noch keine physikalischen eigenschaften
			 * vorhanden sein können, daher auch die nullpointer exception . die player list
			 * wird geupdated in Logictier, aber es kann sein, dass es ein inkrementen gab
			 * der player List aber noch keine initierung.
			 */
			updateWorld();

			// play recv damage sound effect
			if (my_omni_client.getPhysicalClient().info_animations.track_2 == AnimationName.RECEIVE_DAMAGE)
				AudioData.playEffect(AudioData.global_receive_damage);

			if (isTrainingsWorld() == false) {

				logic_tier.getClientHistory().add(new ClientHistoryEntry(current_history_entry));

				if (Shared.DEBUG_MODE_SYSOS_NETCODE_CLIENT == true)
					System.out.println(
							"ARootWorldModule.update()	AFTER CLIENT STEP, ID = " + ClientHistory.MOST_RECENT_ID);

				/*
				 * create new package with combo ally id, cursor pos, current actions
				 */
				InputRequest req = new InputRequest(ClientHistory.MOST_RECENT_ID,
						my_physical_client.getCurrentActions(), cursor_pos_vec2,
						player_ui_module.getComboRose().getIDAllySelected(), player_ui_module.getChatMessageToSend());

				// send to game server
				// update reliability system & tag packet with meta data
				ClientGameserverProtocol.RELIABILITY_SYSTEM.update(client_gameserver_connection,
						client_gameserver_connection.getClient().getID());
				ClientGameserverProtocol.RELIABILITY_SYSTEM.preparePacket(req);

				bytes_sent.addAndGet(client_gameserver_connection.getClient().sendUDP(req));

			} else {
			}
			LogicTier.ERROR_MESSAGE = my_omni_client.getPhysicalClient().getErrorMessage();
			my_omni_client.getPhysicalClient().setErrorMessage(null);

			player_ui_module.emptyChatMessageAfterSend();
			releaseAllKeys();

			// update view circles
			LogicTier.VIEW_HANDLER.update();

		}

		/*
		 * 1.Überschreibung drawable durch Netzwerk für clientprediction (pos usw.)
		 * 2.Nutzung der Daten im update-step 3.Jetzt (unterhalb des Kommentars)
		 * nochmalige Überschreibung mit eigenen AnimationsDaten und eigener Physik
		 */

		float alpha = accumulator / SEND_AND_UPDATE_RATE_WORLD;

		if (isTrainingsWorld() == true) {

			SparseClient interpolatedClient;

			for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {

				// update flip, the flip update of the own player is in
				// mouseMoved
				if (client.isDummy() == true)
					client.getDrawableClient().flipByCursor();

				// update world time for skill cooldowns
				client.getPhysicalClient().setTime(time_local);

				// define interpolated client
				interpolatedClient = SnapshotInterpolation.interpolateClients(client.getPrevClient(),
						client.getPhysicalClient(), alpha);

				// update by interpolated client
				LogicTier.updateNonMovementData(interpolatedClient, client.getDrawableClient());

				// update health / mana absolute values
				client.getDrawableClient().getMetaClient().health_absolute = client.getPhysicalClient()
						.getMetaClient().health_absolute;
				client.getDrawableClient().getMetaClient().mana_absolute = client.getPhysicalClient()
						.getMetaClient().mana_absolute;

				LogicTier.updatePosition(interpolatedClient, client.getDrawableClient());
				// updated directly by physical client

				LogicTier.updateAnimations(client.getPhysicalClient().info_animations, client.getDrawableClient(), true,
						true);
				LogicTier.confirmAnimations(client.getDrawableClient());
				// reset animation tracks
				client.getPhysicalClient().resetOutsidePhysics();
			}

			for (ABuilding building : LogicTier.BUILDINGS_MAP.values()) {
				// interpolate sparse projectiles
				SnapshotInterpolation.interpolateProjectiles(building.getPrevSparseProjectiles(),
						building.getSparseProjectiles(), alpha);
				LogicTier.updateProjectiles(building.getPrevSparseProjectiles(),
						building.getDrawableSparseProjectiles());
			}

			SparseCreep interpolatedCreep;
			for (OmniCreep creep : LogicTier.CREEP_LIVING_LIST) {
				interpolatedCreep = SnapshotInterpolation.interpolateCreeps(creep.getPrevCreep(),
						creep.getPhysicalCreep(), alpha);

				LogicTier.updateProjectiles(interpolatedCreep.sparse_projectiles,
						creep.getDrawableCreep().sparse_projectiles);

				// interpolate rotation
				creep.getDrawableCreep().interpolateRotation(delta);

				creep.getDrawableCreep().health_current = creep.getPhysicalCreep().health_current;
				creep.getDrawableCreep().health_absolute = creep.getPhysicalCreep().health_absolute;

				LogicTier.updatePosition(interpolatedCreep, creep.getDrawableCreep());
				LogicTier.updateAnimations(creep.getPhysicalCreep().info_animations, creep.getDrawableCreep());
				LogicTier.confirmAnimations(creep.getDrawableCreep());
				creep.getPhysicalCreep().resetOutsidePhysics();
			}

		} else {
			// training == false

			SparseClient interpolatedClient;
			for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {
				if (client == my_omni_client) {

					// it's a me
					interpolatedClient = SnapshotInterpolation.interpolateClients(my_omni_client.getPrevClient(),
							my_physical_client, alpha);
					/*
					 * for my own client the position must be updated because of client prediction.
					 */
					LogicTier.updatePosition(interpolatedClient, my_drawable_client);

					/*
					 * The animations are calculated by server for the most tracks, just track0/1
					 * will updated by own physics
					 */
					my_drawable_client.info_animations.current_table_skill_index = my_physical_client.info_animations.current_table_skill_index;
					my_drawable_client.setCurrentAnimation(my_physical_client.info_animations.track_0);
					my_drawable_client.setCurrentAnimation(my_physical_client.info_animations.track_1);
					updateSkillCooldown(PhysicalClient.SKILL_0_INDEX, client);
					updateSkillCooldown(PhysicalClient.SKILL_1_INDEX, client);
					updateSkillCooldown(PhysicalClient.SKILL_2_INDEX, client);
				} else {
					// other player

					/*
					 * apply keys by game server to see when a client is flipping.
					 */
					client.getDrawableClient().setCurrentActions(client.getPhysicalClient().getCurrentActions());

					/*
					 * here we update track 0 of the physical client with the received track 0 by
					 * the game server to show the correct movement particle effects.
					 */
					client.getPhysicalClient().info_animations.track_0 = client
							.getDrawableClient().info_animations.track_0;

					interpolatedClient = SnapshotInterpolation.interpolateProjectiles(client.getPrevClient(),
							client.getPhysicalClient(), alpha);

				}

				// common part for all clients.

				/*
				 * update health so effects like slow and stun can run out after death.
				 */
				client.getPhysicalClient().health_current = client.getDrawableClient().health_current;
				client.getPhysicalClient().mana_current = client.getDrawableClient().mana_current;

				/*
				 * we confirm the animations given by the game server. For the particle effects
				 * we use the current actions and apply it to the Box2D physics.
				 */
				LogicTier.confirmAnimations(client.getDrawableClient());

				/*
				 * all clients need the time to know when a skill is ready, so we see the charge
				 * effect.
				 */
				client.getPhysicalClient().setTime(ClientGameserverProtocol.GAMESERVER_TIME_INTERPOLATED);

				/*
				 * movement particle effects basically
				 */
				client.getPhysicalClient().resetOutsidePhysics();

				// add decorative projectiles to the projectile list
				client.getDrawableClient().sparse_projectiles.addAll(interpolatedClient.sparse_projectiles);

			}
			for (ABuilding building : LogicTier.BUILDINGS_MAP.values()) {
				// interpolate
				SnapshotInterpolation.interpolateProjectiles(building.getPrevSparseProjectiles(),
						building.getSparseProjectiles(), alpha);
				/*
				 * so sparse projectiles are now interpolated projectiles from decorative
				 * emissions
				 */
				building.getDrawableSparseProjectiles().addAll(building.getPrevSparseProjectiles());
			}

		}

		LogicTier.VIEW_HANDLER.updatePlayerFogDispel();

		LogicTier.VIEW_HANDLER.updateCreepFogDispel();

		ParticleContainer.updateEmissionEffects(delta);

		/*
		 * para world has no gravity, so the para-client made no moves
		 */
		for (OmniClient c : LogicTier.WORLD_PLAYER_MAP.values()) {
			// update with interpolated position

			c.getParaPhysicalClient().setPosition(c.getDrawableClient().pos.x, c.getDrawableClient().pos.y);
		}

		if (light_cursor != null)
			light_cursor.setPosition(cursor_pos_vec2);

		// update para world
		logic_tier.getClientParaWorld().setGravity(new Vector2());
		logic_tier.getClientParaWorld().step(Gdx.graphics.getDeltaTime(), Shared.VELOCITY_ITERATIONS,
				Shared.POSITION_ITERATIONS);
		logic_tier.getClientParaWorld().setGravity(new Vector2(0, Shared.GRAVITY));

		handleMatchFinished();

		setCameraToPlayer();

		left_bottom_vegetation_viewport.set(my_drawable_client.pos.x - (WORLD_VIEWPORT_WIDTH * .6f),
				my_drawable_client.pos.y - (WORLD_VIEWPORT_HEIGHT * .6f));

		vegetation_viewport.set(left_bottom_vegetation_viewport.x, left_bottom_vegetation_viewport.y,
				WORLD_VIEWPORT_WIDTH * 1.2f, WORLD_VIEWPORT_HEIGHT * 1.2f);

		bar_display_manager.update(delta);

		for (VegetationSprite sprite : vegetation_manager.getSprites()) {
			if (sprite.getBoundingRectangle().overlaps(vegetation_viewport) == false) {
				if (sprite.isTweening() == true) {
					vegetation_manager.getTweenManager().killTarget(sprite);
					sprite.setTweening(false);
				}
				sprite.setActive(false);
			} else {
				VegetationManager.applyStartAnimationOnce(vegetation_manager.getTweenManager(), sprite);
				sprite.setActive(true);
			}
		}

	}

	public void releaseAllKeys() {
		Iterator<AdvancedKey> it = released_keys.iterator();
		AdvancedKey releasedKey;
		while (it.hasNext() == true) {
			releasedKey = it.next();
			WorldAction action = KeysConfiguration.getAction(releasedKey);
			my_drawable_client.setCurrentAction(action, false);
			my_physical_client.setCurrentAction(action, false);
			it.remove();
		}
	}

	private void handleMatchFinished() {

		// first time recognition
		if (LogicTier.WINNER_TEAM != null) {

			if (vic_def_module.isVisible() == false) {
				for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {
					client.getPhysicalClient().setMatchFinished();
					client.getDrawableClient().setMatchFinished();
				}

				for (OmniCreep creep : LogicTier.CREEP_LIVING_LIST) {
					creep.getPhysicalCreep().setMatchFinished();
					creep.getDrawableCreep().setMatchFinished();
				}

				if (LogicTier.WINNER_TEAM == my_omni_client.getMetaClient().team)
					vic_def_module.setSkeleton(AnimationContainer.skeleton_defeat);
				else
					vic_def_module.setSkeleton(AnimationContainer.skeleton_victory);

				changeTab(vic_def_module);
			}

		}

	}

	private void updateWorld() {
		synchronized (logic_tier.getClientMainWorld()) {

			if (isTrainingsWorld() == false) {
				current_history_entry.setSnapshot(my_physical_client);
			}

			// update before step
			for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {

				client.getPhysicalClient().update();
			}
			for (OmniCreep creep : LogicTier.CREEP_LIVING_LIST)
				creep.getPhysicalCreep().update();

			LogicTier.updateWorld(logic_tier.getClientMainWorld());

			// update after step
			for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values())
				client.getPhysicalClient().updateAfterStep();

			OmniCreep omniCreep;
			PhysicalCreep physicalCreep;
			for (Iterator<OmniCreep> it = LogicTier.CREEP_LIVING_LIST.iterator(); it.hasNext();) {

				omniCreep = it.next();

				physicalCreep = omniCreep.getPhysicalCreep();

				physicalCreep.updateAfterStep();

				if (physicalCreep.hasToBeRemoved() == true) {
					// free skeleton back to the pool.
					AnimationContainer.freeCreepSkeleton(omniCreep.getDrawableCreep().getSkeleton());
					it.remove();
				}
			}

			if (Shared.DEBUG_MODE_SYSOS_NETCODE_CLIENT == true)
				System.out.println("AbstractIngameScreen.updateWorld()		client-step, resulting pos = "
						+ my_physical_client.getHitbox().getPosition());

			if (isTrainingsWorld() == false)
				current_history_entry
						.setResultingPosition(new Vector2(my_physical_client.pos.x, my_physical_client.pos.y));
		}

	}

	private void correctClientPredicition() {

		if (my_physical_client.isUnderImpulse() == true)
			return;

		Vector2 networkPos = new Vector2(my_drawable_client.pos);
		Vector2 networkVel = new Vector2(my_drawable_client.vel);
		ClientHistoryEntry lastCalculatedEntry = logic_tier.getClientHistory()
				.getHistoryEntityByID(LogicTier.MOST_RECENT_PROCESSED_ID);

		if (lastCalculatedEntry == null)
			return;

		Vector2 myCalculatedPos = lastCalculatedEntry.getResultingPosition();
		if (Shared.DEBUG_MODE_SYSOS_NETCODE_CLIENT == true) {
			System.out.println("AbstractIngameScreen.handleClientPredicition()	2. my calculated pos = ( "
					+ myCalculatedPos.x + " / " + myCalculatedPos.y + " ),    network pos = ( " + networkPos.x + " / "
					+ networkPos.y + " ), network vel = " + networkVel);

			System.out.println(
					"ARootWorldModule.correctClientPredicition()	clienthistory = " + logic_tier.getClientHistory());
		}

		if (MathUtils.isZero(myCalculatedPos.dst2(networkPos)) == true)
			// no difference
			return;

		correction_count.addAndGet(1);

		/*
		 * refresh current history entry. It is possible, that there is no new entry so
		 * this method would correct the same processed id again, which is not necessary
		 */
		lastCalculatedEntry.setResultingPosition(networkPos);

		// setting up values by network before rewinding
		server_sync_client.setPosition(networkPos.x, networkPos.y);
		server_sync_client.getHitbox().setVelocity(networkVel);
		server_sync_client.health_current = my_physical_client.health_current;
		server_sync_client.mana_current = my_physical_client.mana_current;

		ArrayList<ClientHistoryEntry> predictedHistory = logic_tier.getClientHistory()
				.getHistoryNewerThanID(LogicTier.MOST_RECENT_PROCESSED_ID);

		if (Shared.DEBUG_MODE_SYSOS_NETCODE_CLIENT == true)
			System.out.println("ARootWorldModule.handleClientPredicition()			START REWINDING #"
					+ correction_count + " of " + predictedHistory.size() + " predicted entries");

		for (ClientHistoryEntry entry : predictedHistory) {

			if (Shared.DEBUG_MODE_SYSOS_NETCODE_CLIENT == true) {
				System.out.println("AbstractIngameScreen.handleClientPrediction()				...prev_keys = "
						+ entry.getSnapshot().prev_actions + ", ID = " + entry.getID());
				System.out.println("AbstractIngameScreen.handleClientPrediction()				...active_keys = "
						+ entry.getSnapshot().current_actions + ", ID = " + entry.getID());
			}

			// setting up values of snapshot before rewinding
			server_sync_client.setupBySnapshot(entry.getSnapshot());

			server_sync_client.update();

			// set server_sync_client members like stunned and enabled_movement

			LogicTier.updateWorld(logic_tier.getClientParaWorld());

			if (Shared.DEBUG_MODE_SYSOS_NETCODE_CLIENT == true)
				System.out.println(
						"AbstractIngameScreen.handleClientPrediction()				... -> after correction pos (new) = "
								+ server_sync_client.getHitbox().getPosition() + ", saved position (old) = "
								+ entry.getResultingPosition());

			server_sync_client.updateAfterStep();
			// refresh predicted entry with updated data
			entry.setResultingPosition(new Vector2(server_sync_client.getHitbox().getPosition()));
		}

		if (Shared.DEBUG_MODE_SYSOS_NETCODE_CLIENT == true)
			System.out.println(
					"AbstractIngameModule.handleClientPredicition()	5. wie groß war der unterschied? physical = "
							+ my_physical_client.pos + ", server_sync = " + server_sync_client.pos);

		// apply to position and velocity of physical client
		my_physical_client.setPosition(server_sync_client.getHitbox().getPosition().x,
				server_sync_client.getHitbox().getPosition().y);

		my_physical_client.getHitbox().setVelocity(server_sync_client.getHitbox().getVelocity());

	}

	private void updateSkillCooldown(int skillIndex, OmniClient client) {
		PhysicalClient physClient = client.getPhysicalClient();
		ASkill skill = physClient.getSkills().get(skillIndex);
		float currentCooldown = skill.getCooldownCurrent();
		float serverCooldown = 0;

		if (skillIndex == PhysicalClient.SKILL_0_INDEX) {

			physClient.cooldown_skill0 = client.getDrawableClient().cooldown_skill0;
			serverCooldown = client.getDrawableClient().cooldown_skill0;
			System.out.println("ARootWorldModule.updateSkillCooldown()			SKILL0: currentCooldown (Client) = "
					+ currentCooldown + ", servercooldiwn = " + serverCooldown + ", isZero(current) = "
					+ MathUtils.isZero(currentCooldown) + ", isZero(server) = " + MathUtils.isZero(serverCooldown));
		}
		if (skillIndex == PhysicalClient.SKILL_1_INDEX) {
			physClient.cooldown_skill1 = client.getDrawableClient().cooldown_skill1;
			serverCooldown = client.getDrawableClient().cooldown_skill1;
		}
		if (skillIndex == PhysicalClient.SKILL_2_INDEX) {
			physClient.cooldown_skill2 = client.getDrawableClient().cooldown_skill2;
			serverCooldown = client.getDrawableClient().cooldown_skill2;
		}

		if (currentCooldown > 0)
			return;

		if (skillIndex == PhysicalClient.SKILL_0_INDEX) {
			System.out.println("ARootWorldModule.updateSkillCooldown()			AFTER SKILL0");
		}
		/*
		 * if the current cooldown is lower than 0 the cooldown by the server might be
		 * still higher so we take rather the server cooldown to avoid skill initiation
		 * by accident.
		 */
		/*
		 * that's also the reason the firerate with client/server is lower.
		 */
		skill.setCooldownCurrent(serverCooldown);

	}

	@Override
	public void drawWorld() {

		float delta = Gdx.graphics.getDeltaTime();

		BasicScreen.SHAPE_RENDERER.setProjectionMatrix(world_cam.combined);

		// BACKGROUND:
		// if (DEBUG_MODE_BACKGROUND_ISLAND_POST_PROCESSING_BLURRING == true)
		// gui_tier.getPostProcessorBackground().capture();
		batch.begin();

		world_cam.position.x += view_shift.x;
		world_cam.position.y += view_shift.y;
		world_cam.update();

		// draw background
		// batch.disableBlending();
		// drawBackgroundImage(StaticGraphic.world_background1,
		// WORLD_BG_PARALLAX_1);
		// batch.enableBlending();
		//
		// drawBackgroundImage(StaticGraphic.world_background2,
		// WORLD_BG_PARALLAX_2);
		// drawBackgroundImage(StaticGraphic.world_background3,
		// WORLD_BG_PARALLAX_3);
		// drawBackgroundImage(StaticGraphic.world_background4,
		// WORLD_BG_PARALLAX_4);

		drawBG(AnimationContainer.bg1_skeletons, 0.7f);// WORLD_BG_PARALLAX_1);
		drawBG(AnimationContainer.bg2_skeletons, 0.8f);// WORLD_BG_PARALLAX_2);
		drawBG(AnimationContainer.bg3_skeletons, 0.9f);// WORLD_BG_PARALLAX_3);
		drawBG(AnimationContainer.bg4_skeletons, 0.995f);// WORLD_BG_PARALLAX_4);
	
		batch.setProjectionMatrix(world_cam.calculateParallaxMatrix(1f, 1f));

		// if (DEBUG_MODE_BACKGROUND_ISLAND_POST_PROCESSING_BLURRING == true)
		// gui_tier.getPostProcessorBackground().render();
		//
		// if (DEBUG_MODE_BACKGROUND_ISLAND_POST_PROCESSING_BLURRING == true)
		// gui_tier.getPostProcessorForeground().captureNoClear();

		// FOREGROUND
		island_filling_manager.draw(batch);

		drawBase(map_manager.getBaseLeft());
		drawBase(map_manager.getBaseRight());

		// draw platforms
		platforms_manager.draw(batch);

		// draw plants
		vegetation_manager.draw(batch);

		// drawTowers();
		// drawShrines();

		ParticleContainer.drawWORLDParticles(batch, delta, map_manager);

		// draw behind effects first
		ParticleContainer.drawEmissionEffects(batch, delta, true);

		// draw creeps
		for (OmniCreep creep : LogicTier.CREEP_LIVING_LIST) {
			if (creep.getPhysicalCreep().isVisible() == true) {
				creep.getDrawableCreep().animationRoutine(delta);
				BasicScreen.SKELETON_RENDERER.draw(batch, creep.getDrawableCreep());
			}
		}
		// draw players
		DrawableClient drawableClient;
		PhysicalClient physicalClient;

		for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {
			// was player detected in one view circle?
			if (client != my_omni_client) {
				drawableClient = client.getDrawableClient();
				physicalClient = client.getPhysicalClient();

				boolean isVisible = physicalClient.isVisible();

				drawableClient.animationRoutine(delta, isVisible);

				if (isVisible == true) {

					CustomSpineWidget spineComboFeedback = drawableClient.getSpineComboFeedback();

					if (spineComboFeedback != null) {
						// act combo feedback
						spineComboFeedback.act(delta);

						// set position
						spineComboFeedback.setPosition(drawableClient.pos.x, drawableClient.pos.y);

						// set animation
						if (physicalClient == my_physical_client.getPotentialComboAlly()) {
							if (spineComboFeedback.getCurrentAnimation()
									.equals(ComboFeedbackAnimation.POPUP.getName()) == false) {
								spineComboFeedback.setAnimationTime(0);
								spineComboFeedback.setAnimation(ComboFeedbackAnimation.POPUP.getName());
							}
						} else {
							if (spineComboFeedback.getCurrentAnimation()
									.equals(ComboFeedbackAnimation.POPOUT.getName()) == false) {
								spineComboFeedback.setAnimationTime(0);
								spineComboFeedback.setAnimation(ComboFeedbackAnimation.POPOUT.getName());
							}
						}

						// draw combo feedback
						spineComboFeedback.draw(batch, 1f);
					}
					// draw client
					BasicScreen.SKELETON_RENDERER.draw(batch, drawableClient);
				}

			}

		}

		// draw own player
		my_drawable_client.animationRoutine(delta, true);
		BasicScreen.SKELETON_RENDERER.draw(batch, my_drawable_client);

		// draw foreground effects
		ParticleContainer.drawEmissionEffects(batch, delta, false);

		batch.end();

		// if (DEBUG_MODE_BACKGROUND_ISLAND_POST_PROCESSING_BLURRING == true)
		// gui_tier.getPostProcessorForeground().render();

		if (BOX2D_DEBUGLINES == true) {
			// box2d_debug_renderer.render(logic_tier.getClientParaWorld(),
			// world_cam.combined);
			if (logic_tier.getGameserverLogic() != null)
				box2d_debug_renderer.render(logic_tier.getGameserverLogic().getWorld(), world_cam.combined);
			box2d_debug_renderer.render(logic_tier.getClientMainWorld(), world_cam.combined);
		}

		// if (ray_handler_lights != null) {
		// ray_handler_lights.setCombinedMatrix(world_cam);
		// ray_handler_lights.updateAndRender();
		// }

		// e.g. drawing dummies in training
		drawSpecific();

		if (DEBUG_MODE_RENDER_LIGHT == true) {

			// // stencilling
			// Gdx.gl.glEnable(GL30.GL_STENCIL_TEST);
			// Gdx.gl.glColorMask(false, false, false, false);
			// Gdx.gl.glDepthMask(false);
			// Gdx.gl.glStencilFunc(GL30.GL_NEVER, 1, 0xFF);// if(test
			// bestanden?)
			// Gdx.gl.glStencilOp(GL30.GL_REPLACE, GL30.GL_KEEP, GL30.GL_KEEP);
			// // {
			// //  }
			// // draw stencil pattern
			// Gdx.gl.glStencilMask(0xFF);
			// Gdx.gl.glClear(GL30.GL_STENCIL_BUFFER_BIT); // needs mask=0xFF
			//
			// // kreis
			//
			// float width = world_cam.viewportWidth * 1.2f;
			// float height = world_cam.viewportHeight * 1.2f;
			// float x = drawable_client.pos.x
			// + (drawable_client.pos.x - world_cam.position.x);
			// float y = drawable_client.pos.y
			// + (drawable_client.pos.y - world_cam.position.y);
			//
			//
			// // BasicScreen.SHAPE_RENDERER.begin(ShapeType.Filled);
			// // BasicScreen.SHAPE_RENDERER.ellipse(x - width * 0.5f, y -
			// height
			// // * 0.5f, width, height, 60);
			// // BasicScreen.SHAPE_RENDERER.end();
			//
			// BasicScreen.SHAPE_RENDERER.begin(ShapeType.Filled);
			// BasicScreen.SHAPE_RENDERER.rect(x - width * 0.5f, y - height
			// * 0.5f, width, height);
			// BasicScreen.SHAPE_RENDERER.end();
			//
			//
			//
			// Gdx.gl.glColorMask(true, true, true, true);
			// Gdx.gl.glDepthMask(true);
			// Gdx.gl.glStencilMask(0x00);
			// // draw only where stencil's value is 1
			// Gdx.gl.glStencilFunc(GL30.GL_EQUAL, 0, 0xFF);
			//
			// // dunkel überall

			LogicTier.VIEW_HANDLER.render(world_cam);
			//
			// Gdx.gl.glDisable(GL30.GL_STENCIL_TEST);
		}

		// draw effect frames in UI
		if (my_physical_client.isAlive() == true) {

			BasicScreen.SHAPE_RENDERER.setProjectionMatrix(stage.getCamera().combined);

			// TODO delete
//			BasicScreen.SHAPE_RENDERER.begin(ShapeType.Line);
//			player_ui_module.getComboRose().draw(BasicScreen.SHAPE_RENDERER);
//			BasicScreen.SHAPE_RENDERER.end();

			BasicScreen.SHAPE_RENDERER.begin(ShapeType.Filled);
			batch.setProjectionMatrix(stage.getCamera().combined);
			batch.begin();

			int i = 0;
			HashMap<Short, ArrayList<AEffectTimed>> mapStackables = new HashMap<>();
			for (AEffectTimed effect : my_omni_client.getPhysicalClient().getEffectsTimed()) {

				if (effect.isStackable() == true) {
					ArrayList<AEffectTimed> list = mapStackables.get(effect.getPoolID());
					if (list == null) {
						list = new ArrayList<>();
						mapStackables.put(effect.getPoolID(), list);
					}
					list.add(effect);
					continue;
				}

				if (effect.getDurationMS() < 0)
					continue;
				if (effect.getThumbnailSprite() == null)
					continue;

				effect.drawFrame(batch, i, stage.getCamera(), BasicScreen.SHAPE_RENDERER, 0);
				i++;

			}

			for (Entry<Short, ArrayList<AEffectTimed>> entry : mapStackables.entrySet()) {

				if (entry.getValue() == null)
					continue;
				if (entry.getValue().size() == 0)
					continue;
				if (entry.getValue().get(0) == null)
					continue;

				entry.getValue().get(0).drawFrame(batch, i, stage.getCamera(), BasicScreen.SHAPE_RENDERER,
						entry.getValue().size());
				i++;

			}

			mapStackables.clear();

			batch.end();
			BasicScreen.SHAPE_RENDERER.end();
		}
	
		if (LogicTier.WINNER_TEAM == null) {
			// shapeRenderer used here
			bar_display_manager.draw();

			minimap_manager.draw(batch, MINIMAP_DRAW_LEFT);

		}

		// float diffX = (world_cam.position.x +
		// client.getDrawableClient().pos.x)
		// * world_cam.zoom;
		// float diffY = (world_cam.position.y +
		// client.getDrawableClient().pos.y)
		// * world_cam.zoom;

		// System.out.println("AbstractIngameModule.drawFirst() 5. bindings: "
		// + GLProfiler.textureBindings + ", drawCalls = "
		// + GLProfiler.drawCalls + ", shaderSwitches = "
		// + GLProfiler.shaderSwitches);

		if (vic_def_module.isDone() == true)
			gui_tier.changeRootModule(new MaploadingRoot(ClientGameserverProtocol.MAP_SEED, true));

	}

	protected void drawBackgroundImage(Sprite bg, float parallaxVel) {
		batch.setProjectionMatrix(world_cam.calculateParallaxMatrix(parallaxVel, parallaxVel));

		// if (base_exploding == null)
		// bg.setCenter(drawable_client.pos.x, drawable_client.pos.y);
		// else

		bg.setCenter(world_cam.position.x, world_cam.position.y);
		bg.draw(batch);

		// drawShrines();

	}

	private void drawBG(ArrayList<BGSkeleton> skeletons, float parallaxVel) {
		batch.setProjectionMatrix(world_cam.calculateParallaxMatrix(parallaxVel, parallaxVel));

		for (BGSkeleton skeleton : skeletons) {
			if (skeleton.getAnimation() != null)
				skeleton.getAnimation().apply(skeleton, 0, BasicScreen.ANIMATION_TIME, true, null, 1, MixBlend.first,
						MixDirection.in);
			skeleton.updateWorldTransform();
			BasicScreen.SKELETON_RENDERER.draw(batch, skeleton);
		}
	}

	private void drawTowers() {
		for (Tower tower : map_manager.getTowers()) {
			tower.incAnimationTime(Gdx.graphics.getDeltaTime());
			if (tower.isAlive() == true)
				// normal idle animation
				tower.getAnimation().apply(tower.getSkeleton(), 0, tower.getAnimationTime(), true, null, 1,
						MixBlend.first, MixDirection.in);
			else if (tower.getAnimation().getDuration() > tower.getAnimationTime())
				// apply death animation only once
				tower.getAnimation().apply(tower.getSkeleton(), 0, tower.getAnimationTime(), true, null, 1,
						MixBlend.first, MixDirection.in);

			tower.getSkeleton().updateWorldTransform();
			BasicScreen.SKELETON_RENDERER.draw(batch, tower);

		}

	}

	private void drawShrines() {
		for (Shrine shrine : map_manager.getShrines()) {
			shrine.getAnimation().apply(shrine.getSkeleton(), 0, BasicScreen.ANIMATION_TIME, true, null, 1,
					MixBlend.first, MixDirection.in);

			shrine.getSkeleton().updateWorldTransform();
			BasicScreen.SKELETON_RENDERER.draw(batch, shrine);

		}
	}

	private void drawBase(Base base) {
		// spine animation
		Skeleton skeleton = base.getSkeleton();

		base.getAnimationCore().apply(skeleton, 0, BasicScreen.ANIMATION_TIME, true, null, 1, MixBlend.first,
				MixDirection.in);

		skeleton.updateWorldTransform();
		BasicScreen.SKELETON_RENDERER.draw(batch, skeleton);

	}

	/**
	 * Use this method for render specific things inside the game loop.
	 * <p>
	 * Differentiate whether you are in training or multiplayer scenario.
	 * 
	 */
	protected abstract void drawSpecific();

	public void setCharacter(DrawableClient characterModel) {

		if (create_ally == true) {
			setCreateAlly(false);

			player_ui_module.createAlly(characterModel);

		} else if (create_enemy == true) {
			setCreateEnemy(false);

			player_ui_module.createEnemy(characterModel);

		} else {

			my_omni_client.getMetaClient().skin_index = characterModel.getMetaClient().skin_index;
			my_omni_client.getMetaClient().setGameclass(characterModel.getGameclass().getNameAsEnum());

			my_drawable_client.updateByMetaData(false);
			my_physical_client.updateByMetaData(my_physical_client.getHitbox(), my_drawable_client);
			player_ui_module.setWidgetsAfterChange(my_drawable_client.getMetaClient());
		}

	}

	// @Override
	// protected void drawAfterStage() {
	//
	// batch.setProjectionMatrix(world_cam.combined);
	//
	// // 1. determine skill preview sprite
	// SkillPreviewSprite skillPreviewSprite;
	// if (physical_client.getPreparingSkill() != null)
	// skillPreviewSprite =
	// physical_client.getPreparingSkill().getPreviewSprite();
	// else
	// // 3 = basic skill
	// skillPreviewSprite =
	// physical_client.getSkills().get(3).getPreviewSprite();
	//
	// // 2. set sprite for updating rotation
	// drawable_client.setSkillPreviewSprite(skillPreviewSprite);
	//
	// // 3. update flip and view shift + rotation of skill preview
	// if (CURSOR_UI_CHECK.isAtUIModules() == false)
	// view_shift = drawable_client.flipByCursor();
	//
	// // 4. position previewSprite depending on skill type
	// skillPreviewSprite.setPosition(drawable_client);
	//
	// // 5. draw
	// // skillPreviewSprite.draw(batch);
	//
	// }

	@Override
	public void resize(int width, int height) {
		// TODO this method will not be called. ...and it isn't important
		world_viewport.update(width, height, true);

		// if (DEBUG_MODE_BACKGROUND_ISLAND_POST_PROCESSING_BLURRING)
		// gui_tier.getPostProcessorBackground().setViewport(
		// new Rectangle(world_viewport.getScreenX(), world_viewport
		// .getScreenY(), world_viewport.getScreenWidth(),
		// world_viewport.getScreenHeight()));
		setCameraToPlayer();

	}

	@Override
	public void dispose() {
		super.dispose();
		LogicTier.VIEW_HANDLER.dispose();
	}

	@Override
	public void loadWidgets() {

	}

	@Override
	public void setLayout() {

	}

	@Override
	public void touchDown(int screenX, int screenY, int pointer, int button) {
		if (CURSOR_UI_CHECK.isAtUIWidgets() == false && CURSOR_UI_CHECK.isAtUIModules() == false
				&& player_ui_module.getChatWindow().hasKeyboardFocus() == false) {
			if (enable_dummy_movement == false) {
				WorldAction action = KeysConfiguration.getAction(new AdvancedKey(button, false));
				my_drawable_client.setCurrentAction(action, true);
				my_physical_client.setCurrentAction(action, true);
			}
		}

		super.touchDown(screenX, screenY, pointer, button);
	}

	@Override
	public void touchUp(int screenX, int screenY, int pointer, int button) {
		released_keys.add(new AdvancedKey(button, false));
		super.touchUp(screenX, screenY, pointer, button);
	}

	private boolean visibleHelp = false;

	@Override
	public void keyDown(int keycode) {
		super.keyDown(keycode);

		if (Gdx.input.isKeyPressed(Keys.APOSTROPHE) == true && keycode == Keys.RIGHT_BRACKET) {
			for (AModule subModule : sub_modules)
				subModule.setVisibleGlobal(visibleHelp);

			minimap_manager.setVisible(visibleHelp);

			bar_display_manager.setVisible(visibleHelp);

			setVisibleGlobal(visibleHelp);

			visibleHelp = !visibleHelp;
		}

		if (keycode == Keys.SHIFT_RIGHT)
			enable_dummy_movement = true;

		if (CURSOR_UI_CHECK.isAtUIModules() == false && player_ui_module.getChatWindow().hasKeyboardFocus() == false)
			if (enable_dummy_movement == false) {

				WorldAction action = KeysConfiguration.getAction(new AdvancedKey(keycode, true));

				if (my_physical_client.isCurrentActionActive(WorldAction.SWITCH_COMBO_MODE) == true) {

					if (action == WorldAction.EXECUTE_SKILL1) {

						my_drawable_client.setCurrentAction(WorldAction.EXECUTE_TRANSFER_SKILL1, true);
						my_physical_client.setCurrentAction(WorldAction.EXECUTE_TRANSFER_SKILL1, true);

						released_keys.add(new AdvancedKey(KeysConfiguration.KEY_TRANSFER_SKILL1, true));

					} else if (action == WorldAction.EXECUTE_SKILL2) {

						my_drawable_client.setCurrentAction(WorldAction.EXECUTE_TRANSFER_SKILL2, true);
						my_physical_client.setCurrentAction(WorldAction.EXECUTE_TRANSFER_SKILL2, true);

						released_keys.add(new AdvancedKey(KeysConfiguration.KEY_TRANSFER_SKILL2, true));

					} else {
						my_drawable_client.setCurrentAction(action, true);
						my_physical_client.setCurrentAction(action, true);

					}

				} else {

					my_drawable_client.setCurrentAction(action, true);
					my_physical_client.setCurrentAction(action, true);

				}
			}
	}

	@Override
	public void keyUp(int keycode) {
		super.keyUp(keycode);
		if (enable_dummy_movement == false)
			released_keys.add(new AdvancedKey(keycode, true));
		if (keycode == Keys.SHIFT_RIGHT)
			enable_dummy_movement = false;

	}

	@Override
	public void loadSubModules() {
		esc_module = new ESCModule();
		player_ui_module = createPlayerUIModule();
		vic_def_module = new VictoryDefeatModule();
	}

	protected abstract APlayerUIModule createPlayerUIModule();

	@Override
	public void addSubModules(ArrayList<AModule> subModules) {
		subModules.add(vic_def_module);
		subModules.add(esc_module);
		subModules.add(player_ui_module);
	}

	@Override
	protected void addTabs(ArrayList<AModule> tabs) {
		tabs.add(player_ui_module);
		tabs.add(vic_def_module);
	}

	@Override
	protected boolean clickedEscape() {
		esc_module.setVisibleGlobal(true);
		return true;
	}

	@Override
	public boolean scrolled(float amount) {

		if (CURSOR_UI_CHECK.isAtUIWidgets() == true)
			return true;
		if (CURSOR_UI_CHECK.isAtUIModules() == true)
			return true;

		world_cam.zoom += amount / 20f;

		if (world_cam.zoom < MAX_ZOOM_IN)
			world_cam.zoom = MAX_ZOOM_IN;

		if (world_cam.zoom > MAX_ZOOM_OUT)
			world_cam.zoom = MAX_ZOOM_OUT;
		return true;
	}

	public float getTimeLocal() {
		return time_local;
	}

	public ArrayList<AdvancedKey> getReleasedKeys() {
		return released_keys;
	}

	public PhysicalClient getPhysicalClient() {
		return my_physical_client;
	}

	public APlayerUIModule getPlayerUIModule() {
		return player_ui_module;
	}

	private boolean create_ally = false, create_enemy = false;

	public void setCreateAlly(boolean createAlly) {
		create_ally = createAlly;
	}

	public void setCreateEnemy(boolean createEnemy) {
		create_enemy = createEnemy;
	}

}
