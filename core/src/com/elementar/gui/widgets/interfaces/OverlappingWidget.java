package com.elementar.gui.widgets.interfaces;

/**
 * I use this interface to differentiate normal widgets and overlapping-Widgets
 * like tooltips or the contextmenus
 * 
 * @author lukassongajlo
 * 
 */
public interface OverlappingWidget {

}