package com.elementar.gui.widgets.interfaces;

import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.widgets.CustomTextfield;

/**
 * Let widgets implement this interface which should be unfocused when player
 * clicks outside their bounds, see for example {@link CustomTextfield}. You are
 * responsible that {@link #unfocusWidget()} of your widget is called in
 * {@link AModule#unfocus()}.
 * 
 * @author lukassongajlo
 * 
 */
public interface Unfocusable {

	/**
	 * This method is called when Buttons.LEFT (left mouse button) is pressed.
	 * Let implement this method by widgets that should be unfocused, when the
	 * click isn't in bounds of this widget. Note that you have to check whether
	 * the widget is in bounds or not. Furthermore you have to call this method
	 * in {@link AModule#unfocus()} in the module where this widget lies.
	 */
	public void unfocusWidget();
}
