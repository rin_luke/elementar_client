package com.elementar.gui.widgets.interfaces;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.elementar.gui.widgets.HoverHandler;

/**
 * Gives every widget, which implements this interface a state ( {@link State}
 * ). Each widget has to have a state-object (your responsibility to add one).
 * <p>
 * To change the state you can use one of the setter methods that you have to
 * implement firstly {@link #setDisabled()}, {@link #setNotSelected()},
 * {@link #setSelected()}. I came up with 3 setters instead of just saying
 * "setState(State s)". That's because with this solution someone might be wanna
 * change in {@link State#HOVERED} state, which isn't allowed. Just the
 * {@link HoverHandler} is allowed to change into that.
 * 
 * 
 * @author lukassongajlo
 * 
 */
public interface StatePossessable {

	public State getState();

	/**
	 * Sets the state of this widget. Do not pass {@link State#HOVERED} as
	 * parameter. Just the {@link HoverHandler} can decide whether a widget got
	 * hovered or not.
	 * 
	 * @param state
	 */
	public void setState(State state);

	public void setHovered(boolean hovered);

	public boolean isHovered();

	public boolean isVisible();

	default public ClickListener getHoverListener() {
		return new ClickListener() {
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				if (pointer == -1) {
					super.enter(event, x, y, pointer, fromActor);
					setHovered(true);
				}
			}

			/**
			 * Note that buttons that disappear after clicking 'em won't enter
			 * this method, so you have to call setHovered(bool) manually, for
			 * example the confirmation modules.
			 */
			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				if (pointer == -1) {
					super.exit(event, x, y, pointer, toActor);
					setHovered(false);
				}
			}
		};
	}

	public static enum State {
		NOT_SELECTED, HOVERED, SELECTED, DISABLED
	}

}
