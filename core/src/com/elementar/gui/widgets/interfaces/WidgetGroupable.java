package com.elementar.gui.widgets.interfaces;

/**
 * Let implement composed widgets this interface.
 * 
 * @author lukassongajlo
 * 
 */
public interface WidgetGroupable {

	public abstract void setVisibilityWidgets(boolean visible);

}
