package com.elementar.gui.widgets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.KeysConfiguration.WorldAction;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.util.CustomSkeleton;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.util.SharedColors;
import com.elementar.gui.widgets.listener.TooltipListener;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.characters.skills.MetaFeedback;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.characters.skills.SparseComboStorage;
import com.elementar.logic.characters.util.SkillStatistic;
import com.elementar.logic.util.Shared;

/**
 * Extends {@link CustomIconbutton}. Icon = cooldown sprite
 * 
 * @author lukassongajlo
 * 
 */
public class SkillButton extends CustomIconbutton {

	private byte skill_index;
	private float mana_cost;
	private float cooldown;

	private PhysicalClient client;

	private boolean show_cooldown = true;
	private boolean show_combo_duration = false;

	private SparseComboStorage scs;

	private CustomSpineWidget spine_combo_widget;
	private boolean temp_combomode_active;

	private MetaSkill meta_skill;

	private SkillTooltip tooltip;
	private CustomTable table_tooltip;
	private CustomTable table_mana_cooldown;
	private CustomTable table_stats;

	public enum ComboFeedbackAnimation {

		POPUP("combo_feedback_popup"), POPOUT("combo_feedback_popout");

		private String name;

		private ComboFeedbackAnimation(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	public SkillButton(MetaSkill metaSkill, int skillIndex, Sprite icon, float displayDelayTooltip) {
		super(icon, null);

		setStyle(StyleContainer.button_bold_ivory1_30_bordered_style);

		meta_skill = metaSkill;

		if (meta_skill != null) {

			tooltip = new SkillTooltip(displayDelayTooltip);
		}

		skill_index = (byte) skillIndex;
		setState(State.SELECTED);

		CustomSkeleton skeleton = AnimationContainer.makeAnimation(AnimationContainer.atlas_characters,
				Gdx.files.internal("graphic/animation_combo_feedback_skill.json"), 0.6f);
		skeleton.setSkin("combo_feedback_skill");
		spine_combo_widget = new CustomSpineWidget(skeleton, ComboFeedbackAnimation.POPOUT.name, false);

	}

	public void setClient(PhysicalClient client) {
		this.client = client;
	}

	public void setShowComboDuration(boolean showComboDuration) {
		this.show_combo_duration = showComboDuration;
	}

	public void setIcon(Sprite absorbedSkillThumbnail, SparseComboStorage sparseComboStorage) {
		setIcon(absorbedSkillThumbnail);
		this.scs = sparseComboStorage;
	}

	public void setShowCooldown(boolean showCooldown) {
		this.show_cooldown = showCooldown;
	}

	@Override
	protected void drawAdditionalSprites(ShaderPolyBatch batch) {

		// update cooldown
		if (client != null)
			if (skill_index >= 0 && skill_index < client.getSkills().size()) {

				cooldown = getCooldownByLocation();
				mana_cost = client.getSkills().get(skill_index).getManaCost();
				if (mana_cost > client.mana_current)
					state = State.DISABLED;
				else
					state = State.NOT_SELECTED;
			}
		if (show_combo_duration == true) {

			if (scs != null)
				if (scs.isStoring() == true)
					if (getIcon() != null) {
						batch.end();

						ShapeRenderer shapeRenderer = BasicScreen.SHAPE_RENDERER;

						Gdx.gl.glEnable(GL20.GL_BLEND);
						Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

						shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());

						shapeRenderer.begin(ShapeType.Filled);

						shapeRenderer.setColor(SharedColors.GREEN_1);

						float ringWidth = 3f;

						shapeRenderer.arc(getX() + getWidth() * 0.5f, getY() + getHeight() * 0.5f,
								getIcon().getHeight() * 0.5f + ringWidth, 90, scs.duration_ratio * 360, 30);
						shapeRenderer.end();

						batch.begin();
					}

		}

		super.drawAdditionalSprites(batch);

		// only draw cooldown sprite when cooldown is active
		setText("");

		if (client == null)
			return;

		boolean cooldownActive = cooldown > 0;

		if (show_combo_duration == false)
			if (cooldownActive == true && show_cooldown == true) {

				setText("" + (int) Math.ceil(cooldown));
				batch.end();

				ShapeRenderer shapeRenderer = BasicScreen.SHAPE_RENDERER;

				Gdx.gl.glEnable(GL20.GL_BLEND);
				Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

				shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());

				shapeRenderer.begin(ShapeType.Filled);
				shapeRenderer.setColor(SharedColors.BLACK_DIMMED);
				shapeRenderer.arc(getX() + getWidth() * 0.5f, getY() + getHeight() * 0.5f, icon_up.getHeight() * 0.5f,
						90, client.getSkills().get(skill_index).getCooldownRatio(cooldown) * 360, 30);
				shapeRenderer.end();

				batch.begin();
			}

		if (scs == null) {

			if (cooldownActive == true || mana_cost > client.mana_current) {

				setSkillAnimation(ComboFeedbackAnimation.POPOUT);
				temp_combomode_active = false;
			} else {

				if (temp_combomode_active == false
						&& client.isCurrentActionActive(WorldAction.SWITCH_COMBO_MODE) == true) {

					setSkillAnimation(ComboFeedbackAnimation.POPUP);

				}

				if (temp_combomode_active == true
						&& client.isCurrentActionActive(WorldAction.SWITCH_COMBO_MODE) == false) {
					setSkillAnimation(ComboFeedbackAnimation.POPOUT);

				}
				temp_combomode_active = client.isCurrentActionActive(WorldAction.SWITCH_COMBO_MODE);
			}

		}

	}

	public void bindTooltip(ArrayList<CustomTable> tables, CustomLabel keyLabel) {

		if (tooltip == null)
			return;

		if (keyLabel.getParent() instanceof CustomTable == false)
			return;

		clearTable(table_tooltip, tables);
		clearTable(table_mana_cooldown, tables);
		clearTable(table_stats, tables);

		CustomTable labelTable = (CustomTable) keyLabel.getParent();
		tooltip.setPosition(tables, labelTable, keyLabel);

		for (EventListener listener : keyLabel.getListeners())
			if (listener instanceof TooltipListener) {
				keyLabel.removeListener(listener);
				break;
			}

		keyLabel.addListener(new TooltipListener(tooltip));

	}

	public void addTooltip(ArrayList<CustomTable> tables, CustomTable parentTable) {

		if (tooltip == null)
			return;

		clearTable(table_tooltip, tables);
		clearTable(table_mana_cooldown, tables);
		clearTable(table_stats, tables);

		tooltip.setPosition(tables, parentTable, this);

		for (EventListener listener : getListeners())
			if (listener instanceof TooltipListener) {
				removeListener(listener);
				break;
			}

		addListener(new TooltipListener(tooltip));

	}

	private void clearTable(CustomTable table, ArrayList<CustomTable> tables) {
		if (table != null) {

			tables.remove(table);
			table.clear();
			table.remove();
			table = null;
		}

	}

	public void setSkillAnimation(ComboFeedbackAnimation animation) {

		if (animation.name.equals(spine_combo_widget.animation.getName()) == true)
			return;

		spine_combo_widget.setAnimationTime(0);
		spine_combo_widget.setAnimation(animation.name);
	}

	public CustomSpineWidget getSpineComboWidget() {
		return spine_combo_widget;
	}

	private float getCooldownByLocation() {

		if (client.getLocation() == Location.CLIENTSIDE_MULTIPLAYER) {
			if (skill_index == PhysicalClient.SKILL_0_INDEX)
				return client.cooldown_skill0;

			if (skill_index == PhysicalClient.SKILL_1_INDEX)
				return client.cooldown_skill1;

			if (skill_index == PhysicalClient.SKILL_2_INDEX)
				return client.cooldown_skill2;
		}

		// otherwise the location is Training world
		return client.getSkills().get(skill_index).getCooldownCurrent();

	}

	private class SkillTooltip extends TooltipTextArea {

		private final float THICKNESS_BORDER = 8f;
		private final float THICKNESS_SEPARATION_LINES = 1.5f;
		private final float PADTOP_COMBO_LABEL = PADDING_VERTICAL;
		private final float PADTOP_STATS_LABELS = PADDING_VERTICAL;
		private final float PADDING_END_PART = 18;// PADDING_VERTICAL;

		private final Color LINE_COLOR = SharedColors.IVORY_5;

		private boolean bot_aligned;

		private CustomIconbutton background_image;

		private CustomIconbutton mana_icon;
		private CustomLabel mana_label;
		private CustomIconbutton cooldown_icon;
		private CustomLabel cooldown_label;
		private TooltipTextArea name_area, desc_area;

		private float height_total;
		private float height_name_desc;
		private float height_desc_stats;
		private float height_combo_label;

		private HashMap<CustomLabel, CustomLabel> stats_map;
		private CustomLabel combo_label;
		private CustomLabel accept_label, accept_value_label, share_label, share_value_label;

		public SkillTooltip(float displayDelayTooltip) {
			super("", Shared.WIDTH6 + 20, displayDelayTooltip);

			float width = Shared.WIDTH6 + 20;

			String name = TextData.getWord(meta_skill.getNameKey()).toUpperCase();
			String desc = TextData.getWord(meta_skill.getDescriptionKey());

			name_area = new TooltipTextArea(name, StyleContainer.textfield_bold_ivory1_22_style, width) {
				@Override
				public void draw(Batch batch, float parentAlpha) {

					drawBorderLine(batch);

					super.draw(batch, parentAlpha);
				}

			};
			desc_area = new TooltipTextArea(desc, StyleContainer.textfield_medium_ivory2_20_style, width);

			stats_map = new HashMap<>();
			float statRowHeight = 30;
			for (MetaFeedback feedback : meta_skill.getFeedbacks())
				for (SkillStatistic stat : feedback.getStats()) {
					CustomLabel statKeyLabel = new CustomLabel(TextData.getWord(stat.getKey()) + ":  ",
							StyleContainer.label_bold_ivory3_20_style, width * 0.5f, statRowHeight, Align.right);
					statKeyLabel.setVisible(false);
					CustomLabel statValueLabel = new CustomLabel(stat.getValue().toString() + "" + stat.getUnit(),
							StyleContainer.label_bold_ivory1_20_style, width * 0.5f, statRowHeight, Align.left);
					statValueLabel.setVisible(false);
					stats_map.put(statKeyLabel, statValueLabel);
				}

			combo_label = new CustomLabel(TextData.getWord("fusion"), StyleContainer.label_bold_ivory2_20_style,
					width * 0.5f, Shared.HEIGHT1, Align.center) {
				@Override
				public void draw(Batch batch, float parentAlpha) {
					super.draw(batch, parentAlpha);

					drawComboSeparationLine(batch);
					drawSeparationLine(batch, height_name_desc);
					drawSeparationLine(batch, height_desc_stats);

				}
			};
			combo_label.setVisible(false);
			accept_label = new CustomLabel(TextData.getWord("comboFactor") + ":  ",
					StyleContainer.label_bold_ivory3_20_style, width * 0.4f, Shared.HEIGHT1, Align.right);
			accept_label.setVisible(false);
			accept_value_label = new CustomLabel((int) (meta_skill.getComboFactorPrim()) + "",
					StyleContainer.label_bold_ivory1_20_style, width * 0.1f, Shared.HEIGHT1, Align.left);
			accept_value_label.setVisible(false);
			share_label = new CustomLabel(TextData.getWord("shareFactor") + ":  ",
					StyleContainer.label_bold_ivory3_20_style, width * 0.4f, Shared.HEIGHT1, Align.right);
			share_label.setVisible(false);
			share_value_label = new CustomLabel((int) (meta_skill.getComboFactorAdded()) + "",
					StyleContainer.label_bold_ivory1_20_style, width * 0.1f, Shared.HEIGHT1, Align.left);
			share_value_label.setVisible(false);

			// begin calculate heights
			height_name_desc = name_area.background.getHeight();
			height_desc_stats = height_name_desc + desc_area.background.getHeight();

			float heightBelowDesc = PADTOP_STATS_LABELS + stats_map.values().size() * statRowHeight + PADTOP_COMBO_LABEL
					+ combo_label.getHeight() + accept_label.getHeight() + PADDING_END_PART;

			height_total = height_desc_stats + heightBelowDesc;
			height_combo_label = height_total - accept_label.getHeight() - 0.5f * combo_label.getHeight()
					- PADDING_END_PART;
			// end calculate heights

			Sprite background = new Sprite(StaticGraphic.gui_bg_tooltip);
			background.setSize(width + 2 * PADDING_HORIZONTAL, heightBelowDesc);
			background_image = new CustomIconbutton(background, background.getWidth(), background.getHeight());

			mana_icon = new CustomIconbutton(StaticGraphic.gui_icon_mana,
					StaticGraphic.gui_icon_mana.getWidth() + PADDING_HORIZONTAL * 0.5f,
					StaticGraphic.gui_icon_mana.getHeight());
			mana_label = new CustomLabel(meta_skill.getManaCost() + "", StyleContainer.label_bold_blue1_20_style,
					Shared.WIDTH1, Shared.HEIGHT1, Align.left);

			cooldown_icon = new CustomIconbutton(StaticGraphic.gui_icon_cooldown,
					StaticGraphic.gui_icon_cooldown.getWidth() + PADDING_HORIZONTAL * 0.5f,
					StaticGraphic.gui_icon_cooldown.getHeight());
			cooldown_label = new CustomLabel((int) (meta_skill.getCooldown()) + "",
					StyleContainer.label_bold_ivory1_20_style, Shared.WIDTH1, Shared.HEIGHT1, Align.left);
		}

		@Override
		public <T extends Actor> void setPosition(ArrayList<CustomTable> tables, Table tableLessPrioritized,
				Actor origWidget) {

			setVisible(false);

			/*
			 * confirm preceding layout of the less prioritized for positioning the
			 * origWidget (else we would get 0/0 for origWidget position)
			 */
			tableLessPrioritized.validate();

			float width = background.getWidth();

			/*
			 * the following if-statement solves an issue with changing parent coordinates
			 * where I couldn't find any rationales for it.
			 */
			if (last_parent == null) {
				// first time
				last_parent_x = origWidget.getParent().getX();
				last_parent_y = origWidget.getParent().getY();
				last_parent = origWidget.getParent();
			}

			float origX = origWidget.getX() + last_parent_x, origY = origWidget.getY() + last_parent_y,
					origHeight = origWidget.getHeight();

			table_tooltip = new CustomTable();
			table_tooltip.setFillParent(true);
			table_tooltip.align(tableLessPrioritized.getAlign());
			tables.add(table_tooltip);

			table_mana_cooldown = new CustomTable();
			table_mana_cooldown.setFillParent(true);
			table_mana_cooldown.align(tableLessPrioritized.getAlign());
			// setting flag manual to keep draw order
			table_mana_cooldown.setContainsOverlapping();
			float heightManaCooldown = Shared.HEIGHT1;
			tables.add(table_mana_cooldown);

			table_stats = new CustomTable();
			table_stats.setFillParent(true);
			table_stats.align(tableLessPrioritized.getAlign());
			table_stats.setContainsOverlapping();
			tables.add(table_stats);

			// find offsetX and apply it
			float offsetX = origX * 2 + width;
			table_tooltip.padLeft(offsetX);
			table_mana_cooldown.padLeft(offsetX - PADDING_HORIZONTAL);
			table_stats.padLeft(offsetX);

			float offsetY;

			// find offsetY and apply it
			if ((table_tooltip.getAlign() & Align.top) != 0) {

				bot_aligned = false;

				offsetY = Math.abs(origY) + THICKNESS_BORDER * 0.5f;
				table_tooltip.padTop(offsetY);
				table_mana_cooldown.padTop(offsetY + PADDING_VERTICAL * 0.5f);
				table_stats.padTop(offsetY + height_desc_stats);

			} else if ((table_tooltip.getAlign() & Align.bottom) != 0) {

				bot_aligned = true;

				// e.g. skill thumbnails in world
				offsetY = origY + origHeight + THICKNESS_BORDER * 0.5f;
				table_tooltip.padBottom(offsetY);
				table_mana_cooldown.padBottom(offsetY + height_total - heightManaCooldown - PADDING_VERTICAL * 0.5f);
				table_stats.padBottom(offsetY + PADDING_VERTICAL * 0.5f);
			}

			// layout name and description
			table_tooltip.add(name_area);
			table_tooltip.row();
			table_tooltip.add(desc_area);
			table_tooltip.row();
			table_tooltip.add(background_image);

			// layout mana cost and cooldown
			HorizontalGroup hGroup = new HorizontalGroup();
			hGroup.setWidth(width);
			hGroup.right();
			hGroup.addActor(mana_icon);
			hGroup.addActor(mana_label);
			hGroup.addActor(cooldown_icon);
			hGroup.addActor(cooldown_label);

			table_mana_cooldown.add(hGroup).width(width).height(heightManaCooldown);

			table_stats.row().padTop(PADTOP_STATS_LABELS);
			// layout stats
			for (Entry<CustomLabel, CustomLabel> entry : stats_map.entrySet()) {

				HorizontalGroup hGroup2 = new HorizontalGroup();
				hGroup2.setWidth(width);
				hGroup2.addActor(entry.getKey());
				hGroup2.addActor(entry.getValue());
				table_stats.add(hGroup2).padLeft(100);
				table_stats.row();
			}
			
			if(stats_map.entrySet().isEmpty() == true) {
				table_stats.add().padLeft(100);
				table_stats.row();
			}

			table_stats.add(combo_label)/* .colspan(4) */.padTop(PADTOP_COMBO_LABEL);
			table_stats.row();

			HorizontalGroup hGroup2 = new HorizontalGroup();
			hGroup2.setWidth(width);
			hGroup2.addActor(accept_label);
			hGroup2.addActor(accept_value_label);
			hGroup2.addActor(share_label);
			hGroup2.addActor(share_value_label);

			table_stats.add(hGroup2).padLeft(30);

		}

		private void drawComboSeparationLine(Batch batch) {
			batch.end();

			ShapeRenderer shapeRenderer = BasicScreen.SHAPE_RENDERER;

			Gdx.gl.glEnable(GL20.GL_BLEND);
			Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

			shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());

			shapeRenderer.begin(ShapeType.Filled);

			shapeRenderer.setColor(LINE_COLOR);

			float x = SkillButton.this.getX() + SkillButton.this.getParent().getX();
			x -= THICKNESS_BORDER * 0.5f;
			float y = 0;

			if (bot_aligned == false) {

				y = SkillButton.this.getY() + SkillButton.this.getParent().getY();
				y = y - height_combo_label - THICKNESS_BORDER;
			} else {
				y = -THICKNESS_BORDER * 0.5f + table_tooltip.getPadBottom() + (height_total - height_combo_label);
			}

			shapeRenderer.rectLine(x, y, combo_label.getX(), y, THICKNESS_SEPARATION_LINES);
			shapeRenderer.rectLine(combo_label.getX() + combo_label.getWidth(), y,
					x + name_area.getWidth() + THICKNESS_BORDER + PADDING_HORIZONTAL * 2, y,
					THICKNESS_SEPARATION_LINES);

			shapeRenderer.end();

			batch.begin();
		}

		private void drawSeparationLine(Batch batch, float height) {
			batch.end();

			ShapeRenderer shapeRenderer = BasicScreen.SHAPE_RENDERER;

			Gdx.gl.glEnable(GL20.GL_BLEND);
			Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

			shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());

			shapeRenderer.begin(ShapeType.Filled);

			shapeRenderer.setColor(LINE_COLOR);

			float x = SkillButton.this.getX() + SkillButton.this.getParent().getX();
			x -= THICKNESS_BORDER * 0.5f;
			float y = 0;

			if (bot_aligned == false) {

				y = SkillButton.this.getY() + SkillButton.this.getParent().getY();
				y = y - height - THICKNESS_BORDER;
			} else {
				y = -THICKNESS_BORDER * 0.5f + table_tooltip.getPadBottom() + (height_total - height);
			}

			shapeRenderer.rectLine(x, y, x + name_area.getWidth() + THICKNESS_BORDER + PADDING_HORIZONTAL * 2, y,
					THICKNESS_SEPARATION_LINES);

			shapeRenderer.end();

			batch.begin();
		}

		private void drawBorderLine(Batch batch) {
			batch.end();

			ShapeRenderer shapeRenderer = BasicScreen.SHAPE_RENDERER;

			Gdx.gl.glEnable(GL20.GL_BLEND);
			Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

			shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());

			shapeRenderer.begin(ShapeType.Filled);

			shapeRenderer.setColor(LINE_COLOR);

			float x = SkillButton.this.getX() + SkillButton.this.getParent().getX();
			x -= THICKNESS_BORDER * 0.5f;
			float y = 0;

			if (bot_aligned == false) {

				y = SkillButton.this.getY() + SkillButton.this.getParent().getY();

				y = y - height_total - THICKNESS_BORDER;
			} else {
				y = -THICKNESS_BORDER * 0.5f + table_tooltip.getPadBottom();
			}

			shapeRenderer.rect(x, y, name_area.getWidth() + THICKNESS_BORDER + PADDING_HORIZONTAL * 2,
					height_total + THICKNESS_BORDER);

			shapeRenderer.end();

			batch.begin();
		}

		@Override
		public void setVisible(boolean visible) {
			super.setVisible(visible);

			name_area.setVisible(visible);
			desc_area.setVisible(visible);
			background_image.setVisible(visible);

			mana_icon.setVisible(visible);
			mana_label.setVisible(visible);
			cooldown_icon.setVisible(visible);
			cooldown_label.setVisible(visible);

			for (Entry<CustomLabel, CustomLabel> entry : stats_map.entrySet()) {
				entry.getKey().setVisible(visible);
				entry.getValue().setVisible(visible);
			}

			combo_label.setVisible(visible);
			accept_label.setVisible(visible);
			accept_value_label.setVisible(visible);
			share_label.setVisible(visible);
			share_value_label.setVisible(visible);
		}

	}

}
