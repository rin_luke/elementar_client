package com.elementar.gui.widgets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.logic.util.Shared;

public class CustomSlider extends Slider implements StatePossessable {

	protected State state;

	private boolean hovered;

	public CustomSlider(float min, float max, float stepSize, SliderStyle style) {
		super(min, max, stepSize, false, style);
		setWidth(style.background.getMinWidth());
		setHeight(Shared.HEIGHT1);
		state = State.NOT_SELECTED;
		addListener(getHoverListener());
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		ShaderPolyBatch shaderBatch = (ShaderPolyBatch) batch;

		GUIUtil.determineHoverEffect(shaderBatch, state);

		super.draw(shaderBatch, parentAlpha);

		// have to flush here to avoid weird behavior between widgets...
		shaderBatch.flush();

	}

	@Override
	public float getPrefWidth() {
		if (getStyle() != null)
			if (getStyle().background != null)
				return getStyle().background.getMinWidth();
		return super.getPrefWidth();
	}

	@Override
	public State getState() {
		return state;
	}

	@Override
	public void setState(State state) {
		this.state = state;
	}

	@Override
	public void setHovered(boolean hovered) {
		this.hovered = hovered;
	}

	@Override
	public boolean isHovered() {
		return hovered;
	}
}
