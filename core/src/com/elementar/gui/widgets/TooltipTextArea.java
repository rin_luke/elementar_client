package com.elementar.gui.widgets;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.gui.widgets.interfaces.OverlappingWidget;

public class TooltipTextArea extends CustomTextArea implements OverlappingWidget {

	protected static final int PADDING_VERTICAL = 33;

	protected Actor last_parent;
	protected float last_parent_x, last_parent_y;

	/**
	 * in seconds, 0 by default.
	 */
	protected float display_delay = 0;
	protected float elapsed_time = 0;

	/**
	 * 
	 * {@link #display_delay} = 0.
	 * 
	 * @param text
	 * @param style
	 * @param width
	 */
	public TooltipTextArea(String text, TextFieldStyle style, float width) {
		super(text, style);
		setWidth(width);

		float lineHeight = getStyle().font.getData().lineHeight;

		calculateOffsets();
		setDisabled(true);

		float height = lineHeight * getLines() + 2 * PADDING_VERTICAL;

		background = new Sprite(StaticGraphic.gui_bg_tooltip);
		background.setSize(getWidth() + 2 * PADDING_HORIZONTAL, height);

		bounding_button = new CustomTextbutton("", StyleContainer.button_bold_30_style, background.getWidth(),
				background.getHeight(), Align.center);
	}

	/**
	 * 
	 * style: {@link StyleContainer#tooltip_style}
	 * 
	 * @param text
	 * @param width
	 * @param displayDelay
	 */
	public TooltipTextArea(String text, float width, float displayDelay) {
		this(text, width);
		display_delay = displayDelay;
	}

	/**
	 * 
	 * @param text
	 * @param width
	 */
	public TooltipTextArea(String text, float width) {
		this(text, StyleContainer.tooltip_style, width);
	}

	@Override
	protected void drawText(Batch batch, BitmapFont font, float x, float y) {
		super.drawText(batch, font, x, y - PADDING_VERTICAL);// + getStyle().font.getDescent()));
	}

	public void updateDelay(float delta) {

		elapsed_time += delta;

		if (elapsed_time > display_delay && isVisible() == false)
			setVisible(true);
	}

	public <T extends Actor> void setPosition(ArrayList<CustomTable> tables, Table tableLessPrioritized,
			Actor origWidget) {

		setVisible(false);

		/*
		 * confirm preceding layout of the less prioritized for positioning the
		 * origWidget (else we would get 0/0 for origWidget position)
		 */
		tableLessPrioritized.validate();

		// help variables
		float width = background.getWidth(), height = background.getHeight();
		// coords and size

		/*
		 * the following if-statement solves an issue with changing parent coordinates
		 * where I couldn't find any rationales for it.
		 */
		if (last_parent == null) {
			// first time
			last_parent_x = origWidget.getParent().getX();
			last_parent_y = origWidget.getParent().getY();
			last_parent = origWidget.getParent();
		}

		float origX = origWidget.getX() + last_parent_x, origY = origWidget.getY() + last_parent_y,
				origWidth = origWidget.getWidth(), origHeight = origWidget.getHeight();

		CustomTable tableTooltip = new CustomTable();
		tableTooltip.setFillParent(true);
		tableTooltip.align(tableLessPrioritized.getAlign());
		tables.add(tableTooltip);

		float shiftRightAligned;

		// setting values depending on in wich quadrant the origWidget lies
		if (origX > 0 && width > origWidth || origX < 0 && width < origWidth)
			shiftRightAligned = 1f;
		else
			shiftRightAligned = 0f;

		float offsetX = width + (origWidth - width) * 2 * shiftRightAligned;
		float offsetY;

		if ((tableTooltip.getAlign() & Align.top) != 0) {
			/*
			 * if table.getAlign() is top aligned (topLeft, topCenter, topRight), the origin
			 * is not in the center of the screen, so the tooltip should always be displayed
			 * beneath the origWidget (-height). The origWidget.y is also negative, so
			 * padBottom is negative.
			 */
			offsetY = -height + (origY * 2);
		} else if ((tableTooltip.getAlign() & Align.bottom) != 0) {
			// e.g. skill thumbnails in world
			offsetY = origY + origHeight;
		} else {
			// center, e.g. settings- intersection plant slider
			offsetY = origY * 2 + height + origHeight * 2;
		}

		// offsetX/Y
		Cell<TooltipTextArea> cell = tableTooltip.add(this);

		cell.padLeft(origX * 2 + offsetX);
		cell.padBottom(offsetY);

	}

	public void setElapsedTime(int elapsedTime) {
		elapsed_time = elapsedTime;
	}

	public float getDisplayDelay() {
		return display_delay;
	}

}
