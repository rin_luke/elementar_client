package com.elementar.gui.widgets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.gui.modules.roots.JoinRoot;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.listener.DisabledHandledListener;
import com.elementar.logic.network.gameserver.MetaDataGameserver;
import com.elementar.logic.network.util.PingStarter;
import com.elementar.logic.util.Shared;

public class GameserverContainer extends HorizontalGroup implements StatePossessable {

	private MetaDataGameserver mdgs;

	private CustomIconbutton password_button;
	private CustomTextbutton name_button, capacity_button, ping_button;

	private Sprite background;

	private boolean hovered;

	public GameserverContainer(HoverHandler hoverHandler, final MetaDataGameserver mdgs, VerticalGroup list) {
		// style arbitrary
		super();
		setWidth(960);
		setHeight(Shared.HEIGHT1);

		this.mdgs = mdgs;

		background = StaticGraphic.gui_bg_gameserver_row;
		password_button = new CustomIconbutton(mdgs.has_password == true ? StaticGraphic.gui_icon_locked : null,
				Shared.WIDTH1, Shared.HEIGHT1);
		password_button.setIconShiftX(10);

		name_button = new CustomTextbutton(mdgs.server_name, StyleContainer.button_bold_20_style,
				Shared.WIDTH9 - Shared.WIDTH1, Shared.HEIGHT1, Align.left);

		capacity_button = new CustomTextbutton("0 / " + mdgs.capacity, StyleContainer.button_bold_20_style,
				Shared.WIDTH3, Shared.HEIGHT1, Align.center);

		// ping is set later in PingClientConnection
		ping_button = new CustomTextbutton("-", StyleContainer.button_bold_20_style, Shared.WIDTH3, Shared.HEIGHT1,
				Align.center);

		addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (getTapCount() == 1)
					JoinRoot.SELECTED_GAMESERVER = mdgs;

				if (getTapCount() == 2)
					JoinRoot.CONNECT_BY_DOUBLECLICK = true;

			}

		});

		addListener(getHoverListener());

		addListener(new DisabledHandledListener(this) {
			@Override
			public void afterDisabledCheck() {
				setState(State.SELECTED);
			}
		});

		addActor(password_button);
		addActor(name_button);
		addActor(capacity_button);
		addActor(ping_button);

		list.addActor(this);
		hoverHandler.add(this);

		/*
		 * provides ping and number players in server and sets the widgets immediately.
		 * Starts a process of multiple pings if necessary in a seperate thread.
		 */
		new PingStarter(ping_button, capacity_button, mdgs.capacity, mdgs.ip, mdgs.port_tcp_game, mdgs.port_udp_game);

	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		ShaderPolyBatch shaderBatch = (ShaderPolyBatch) batch;
		GUIUtil.determineHoverEffect(shaderBatch, getState());
		if (background != null) {
			if (getState() == State.SELECTED || getState() == State.HOVERED)
				background.setAlpha(0.12f);
			else
				background.setAlpha(0);

			background.setPosition(getX(), getY());
			background.draw(shaderBatch);
		}
		super.draw(shaderBatch, parentAlpha);
	}

	@Override
	public void setState(State state) {
		password_button.setState(state);
		name_button.setState(state);
		capacity_button.setState(state);
		ping_button.setState(state);
	}

	@Override
	public State getState() {
		return name_button.getState();
	}

	@Override
	public void setHovered(boolean hovered) {
		this.hovered = hovered;
	}

	@Override
	public boolean isHovered() {
		return hovered;
	}

	@Override
	public float getWidth() {
		return 960;
	}

	@Override
	public float getPrefWidth() {
		return getWidth();
	}

	@Override
	public float getHeight() {
		return Shared.HEIGHT1;
	}

	@Override
	public float getPrefHeight() {
		return getHeight();
	}

	@Override
	public String toString() {
		return "GameserverContainer";
	}

	public MetaDataGameserver getMDGS() {
		return mdgs;
	}
}
