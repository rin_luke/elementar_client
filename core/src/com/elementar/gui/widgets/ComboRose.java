package com.elementar.gui.widgets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.elementar.data.container.OptionsData;
import com.elementar.data.container.KeysConfiguration.WorldAction;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.gui.AGUITier;
import com.elementar.gui.util.CustomStage;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.util.Util;

public class ComboRose {

	private static float HALF_WIDTH_OUTER_RECT;
	private static float HALF_WIDTH_INNER_RECT;
	private static float HALF_HEIGHT_BOTH_RECTS;

	private final float PAD_HEADS_HORIZONTAL_PERCENT = 0.145f;
	private final float PAD_SKILLS_HORIZONTAL_PERCENTAGE = 0.09f;
	private final float PAD_SKILLS_VERTICAL_PERCENTAGE = 0.15f;

	public enum CursorState {
		LEFT_TOP, LEFT_BOT, MID, RIGHT_TOP, RIGHT_BOT
	}

	public enum HeadAnimation {

		ACTIVE("comborose_active"), INACTIVE("comborose_inactive"), POPUP("comborose_popup"),
		POPOUT("comborose_popout"), SELECTED("comborose_selected");

		private String name;

		private HeadAnimation(String name) {
			this.name = name;
		}
	}

	public enum SkillAnimation {

		ACTIVE("comborose_active"), INACTIVE("comborose_inactive"), POPUP("comborose_popup"),
		POPOUT("comborose_popout"), SELECTED("comborose_selected");

		private String name;

		private SkillAnimation(String name) {
			this.name = name;
		}
	}

	private CursorState cursor_state;

	/**
	 * {@link WorldAction#EXECUTE_TRANSFER_SKILL1} or
	 * {@link WorldAction#EXECUTE_TRANSFER_SKILL2}
	 */
	private WorldAction skill_selected;
	private short id_ally_left, id_ally_right, id_ally_selected;

	private boolean active;

	private Vector2 origin_pos, old_cursor_pos, new_cursor_pos, joy_stick_pos;

	private CustomStage stage;
	private CustomSpineWidget spine_head_left, spine_head_right, spine_skill1_left, spine_skill2_left,
			spine_skill1_right, spine_skill2_right;

	public ComboRose() {

		setScale(OptionsData.combo_rose_sensitivity);

		// create spine widgets
//		spine_head_left = new CustomSpineWidget(AnimationContainer.skeleton_comborose_head_left,
//				HeadAnimation.POPOUT.name, false);
//		spine_head_left.setAnimationTime(Float.MAX_VALUE);
//
//		spine_head_right = new CustomSpineWidget(AnimationContainer.skeleton_comborose_head_right,
//				HeadAnimation.POPOUT.name, false);
//		spine_head_right.setAnimationTime(Float.MAX_VALUE);
//		spine_head_right.getSkeleton().setFlipX(true);
//		CustomSkeleton.setBoneFlipX(false, spine_head_right.getSkeleton().findBone("character_symbol_flip"));
//
//		spine_skill1_left = new CustomSpineWidget(AnimationContainer.skeleton_combo_rose_skill1_left,
//				SkillAnimation.POPOUT.name, false);
//		spine_skill2_left = new CustomSpineWidget(AnimationContainer.skeleton_combo_rose_skill2_left,
//				SkillAnimation.POPOUT.name, false);
//		spine_skill1_right = new CustomSpineWidget(AnimationContainer.skeleton_combo_rose_skill1_right,
//				SkillAnimation.POPOUT.name, false);
//		spine_skill1_right.getSkeleton().setFlipX(true);
//		CustomSkeleton.setBoneFlipX(true, spine_skill1_right.getSkeleton().findBone("skill_thumbnail_flip"));
//		spine_skill2_right = new CustomSpineWidget(AnimationContainer.skeleton_combo_rose_skill2_right,
//				SkillAnimation.POPOUT.name, false);
//		spine_skill2_right.getSkeleton().setFlipX(true);
//		CustomSkeleton.setBoneFlipX(true, spine_skill2_right.getSkeleton().findBone("skill_thumbnail_flip"));

		// end spine widgets

		origin_pos = new Vector2();
		old_cursor_pos = new Vector2();
		new_cursor_pos = new Vector2();
		joy_stick_pos = new Vector2();

		stage = AGUITier.getInstance().getStageByDrawOrder(0);

	}

	public static void setScale(float percent) {
		// minimum + slider-value
		HALF_HEIGHT_BOTH_RECTS = Gdx.graphics.getWidth() * 0.03f + Gdx.graphics.getWidth() * 0.2f * (1 - percent);

		HALF_WIDTH_OUTER_RECT = Gdx.graphics.getWidth() * 0.03f + Gdx.graphics.getWidth() * 0.2f * (1 - percent);
		HALF_WIDTH_INNER_RECT = Gdx.graphics.getWidth() * 0.015f + Gdx.graphics.getWidth() * 0.1f * (1 - percent);

	}

	public void draw(ShapeRenderer shapeRenderer) {

		shapeRenderer.rect(origin_pos.x - HALF_WIDTH_OUTER_RECT, origin_pos.y - HALF_HEIGHT_BOTH_RECTS,
				HALF_WIDTH_OUTER_RECT * 2, HALF_HEIGHT_BOTH_RECTS * 2);

		shapeRenderer.rect(origin_pos.x - HALF_WIDTH_INNER_RECT, origin_pos.y - HALF_HEIGHT_BOTH_RECTS,
				HALF_WIDTH_INNER_RECT * 2, HALF_HEIGHT_BOTH_RECTS * 2);

		shapeRenderer.end();
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.circle(joy_stick_pos.x, joy_stick_pos.y, 5);

	}

	public void update() {

		if (active == false)
			return;

		CursorState newCursorState = null;

		Vector2 cursorCoords = new Vector2(Gdx.input.getX(), Gdx.input.getY());
		stage.getViewport().unproject(cursorCoords);
		new_cursor_pos.set(cursorCoords);

		Vector2 diffVec = Util.subVectors(new_cursor_pos, old_cursor_pos);
		Vector2 newPoint = Util.addVectors(joy_stick_pos, diffVec);
		Vector2 diffVec2 = Util.subVectors(newPoint, origin_pos);

		if (diffVec2.y < 0 && diffVec2.y < -HALF_HEIGHT_BOTH_RECTS)
			diffVec2.y = -HALF_HEIGHT_BOTH_RECTS;
		if (diffVec2.y > 0 && diffVec2.y > HALF_HEIGHT_BOTH_RECTS)
			diffVec2.y = HALF_HEIGHT_BOTH_RECTS;
		if (diffVec2.x < 0 && diffVec2.x < -HALF_WIDTH_OUTER_RECT)
			diffVec2.x = -HALF_WIDTH_OUTER_RECT;
		if (diffVec2.x > 0 && diffVec2.x > HALF_WIDTH_OUTER_RECT)
			diffVec2.x = HALF_WIDTH_OUTER_RECT;

		/*
		 * following lines can be used instead of the line above to have an outer
		 * circle.
		 */
//		if (diffVec2.len() > radiusOuter)
//			diffVec2.setLength(radiusOuter);

		joy_stick_pos.set(Util.addVectors(origin_pos, diffVec2));

		old_cursor_pos.set(new_cursor_pos);

		// is cursor outside inner rectangle?
		if (diffVec2.x > HALF_WIDTH_INNER_RECT || diffVec2.x < -HALF_WIDTH_INNER_RECT) {
			float angle = Util.subVectors(joy_stick_pos, origin_pos).angleDeg();
			if (angle >= 0)
				newCursorState = CursorState.RIGHT_TOP;
			if (angle >= 90)
				newCursorState = CursorState.LEFT_TOP;
			if (angle >= 180)
				newCursorState = CursorState.LEFT_BOT;
			if (angle >= 270)
				newCursorState = CursorState.RIGHT_BOT;
		} else
			newCursorState = CursorState.MID;

		if (id_ally_left != -1) {
			if (newCursorState == CursorState.LEFT_TOP) {
				if (cursor_state != CursorState.LEFT_TOP) {
					// one time recognition
					setHeadAnimation(spine_head_left, HeadAnimation.ACTIVE);
					setSkillAnimation(spine_skill1_left, SkillAnimation.ACTIVE);

					setHeadAnimation(spine_head_right, HeadAnimation.INACTIVE);
					setSkillAnimation(spine_skill2_left, SkillAnimation.INACTIVE);

					setSkillAnimation(spine_skill1_right, SkillAnimation.POPOUT);
					setSkillAnimation(spine_skill2_right, SkillAnimation.POPOUT);
					cursor_state = CursorState.LEFT_TOP;
					skill_selected = WorldAction.EXECUTE_TRANSFER_SKILL1;
				}
				id_ally_selected = id_ally_left;
			} else if (newCursorState == CursorState.LEFT_BOT) {
				if (cursor_state != CursorState.LEFT_BOT) {
					// one time recognition
					setHeadAnimation(spine_head_left, HeadAnimation.ACTIVE);
					setSkillAnimation(spine_skill2_left, SkillAnimation.ACTIVE);

					setHeadAnimation(spine_head_right, HeadAnimation.INACTIVE);
					setSkillAnimation(spine_skill1_left, SkillAnimation.INACTIVE);

					setSkillAnimation(spine_skill1_right, SkillAnimation.POPOUT);
					setSkillAnimation(spine_skill2_right, SkillAnimation.POPOUT);
					cursor_state = CursorState.LEFT_BOT;
					skill_selected = WorldAction.EXECUTE_TRANSFER_SKILL2;
				}
				id_ally_selected = id_ally_left;
			}
		}

		if (id_ally_right != -1) {
			if (newCursorState == CursorState.RIGHT_TOP) {
				if (cursor_state != CursorState.RIGHT_TOP) {
					// one time recognition
					setHeadAnimation(spine_head_right, HeadAnimation.ACTIVE);
					setSkillAnimation(spine_skill1_right, SkillAnimation.ACTIVE);

					setHeadAnimation(spine_head_left, HeadAnimation.INACTIVE);
					setSkillAnimation(spine_skill2_right, SkillAnimation.INACTIVE);

					setSkillAnimation(spine_skill1_left, SkillAnimation.POPOUT);
					setSkillAnimation(spine_skill2_left, SkillAnimation.POPOUT);
					cursor_state = CursorState.RIGHT_TOP;
					skill_selected = WorldAction.EXECUTE_TRANSFER_SKILL1;
				}
				id_ally_selected = id_ally_right;
			} else if (newCursorState == CursorState.RIGHT_BOT) {
				if (cursor_state != CursorState.RIGHT_BOT) {
					// one time recognition
					setHeadAnimation(spine_head_right, HeadAnimation.ACTIVE);
					setSkillAnimation(spine_skill2_right, SkillAnimation.ACTIVE);

					setHeadAnimation(spine_head_left, HeadAnimation.INACTIVE);
					setSkillAnimation(spine_skill1_right, SkillAnimation.INACTIVE);

					setSkillAnimation(spine_skill1_left, SkillAnimation.POPOUT);
					setSkillAnimation(spine_skill2_left, SkillAnimation.POPOUT);
					cursor_state = CursorState.RIGHT_BOT;
					skill_selected = WorldAction.EXECUTE_TRANSFER_SKILL2;
				}
				id_ally_selected = id_ally_right;
			}
		}

		if (newCursorState == CursorState.MID) {
			// = cursor is in the middle
			if (cursor_state != CursorState.MID) {
				// one time recognition
				setHeadAnimation(spine_head_left, HeadAnimation.INACTIVE);
				setHeadAnimation(spine_head_right, HeadAnimation.INACTIVE);

				setSkillAnimation(spine_skill1_left, SkillAnimation.POPOUT);
				setSkillAnimation(spine_skill2_left, SkillAnimation.POPOUT);
				setSkillAnimation(spine_skill1_right, SkillAnimation.POPOUT);
				setSkillAnimation(spine_skill2_right, SkillAnimation.POPOUT);

			}

			id_ally_selected = (short) -1;
			cursor_state = CursorState.MID;
			skill_selected = null;
		}

		// clear, will be filled in #setHeadSlots
		id_ally_left = id_ally_right = (short) -1;
		emptySlot(spine_head_left);
		emptySlot(spine_head_right);
	}

	public void setSkillSlots() {

		if (LogicTier.getMyClient() == null)
			return;

		if (LogicTier.getMyClient().getMetaClient().gameclass == null)
			return;

		GameclassName gameclassName = LogicTier.getMyClient().getMetaClient().gameclass.getNameAsEnum();
		setSkill1Slot(spine_skill1_left, gameclassName);
		setSkill1Slot(spine_skill1_right, gameclassName);
		setSkill2Slot(spine_skill2_left, gameclassName);
		setSkill2Slot(spine_skill2_right, gameclassName);
	}

	private void setSkill1Slot(CustomSpineWidget spineActor, GameclassName name) {

		spineActor.getSkeleton().setAttachment("skill_thumbnail_flip",
				"characters/particle_sprites/gui_thumbnail_char_" + name.toString().toLowerCase() + "_skill1");
	}

	private void setSkill2Slot(CustomSpineWidget spineActor, GameclassName name) {

		spineActor.getSkeleton().setAttachment("skill_thumbnail_flip",
				"characters/particle_sprites/gui_thumbnail_char_" + name.toString().toLowerCase() + "_skill2");
	}

	public void setHeadSlots(OmniClient client) {

		GameclassName gameclassName = client.getMetaClient().gameclass.getNameAsEnum();
		if (id_ally_left == -1) {
			// set left slot
			setHeadSlot(spine_head_left, gameclassName);
			id_ally_left = client.getMetaClient().connection_id;
		} else if (id_ally_right == -1) {
			// set right slot
			if (id_ally_left != client.getMetaClient().connection_id) {
				setHeadSlot(spine_head_right, gameclassName);
				id_ally_right = client.getMetaClient().connection_id;
			}
		}
	}

	private void emptySlot(CustomSpineWidget spineActor) {

		spineActor.getSkeleton().setAttachment("character_symbol_flip", null);
	}

	private void setHeadSlot(CustomSpineWidget spineActor, GameclassName name) {

		spineActor.getSkeleton().setAttachment("character_symbol_flip",
				"characters/particle_sprites/gui_thumbnail_char_" + name.toString().toLowerCase() + "_symbol");
	}

	/**
	 * 
	 * @param active
	 */
	public void setActive(boolean active) {
		this.active = active;
		if (active == true) {
			cursor_state = CursorState.MID;
			Vector2 cursorCoords = new Vector2(Gdx.input.getX(), Gdx.input.getY());
			stage.getViewport().unproject(cursorCoords);
			old_cursor_pos.set(cursorCoords);
			origin_pos.set(cursorCoords);
			joy_stick_pos.set(cursorCoords);
		}
		update();
	}

	public void applySelectAnimations() {
		switch (cursor_state) {
		case LEFT_TOP:
			selectLeftTop();
			break;
		case LEFT_BOT:
			selectLeftBot();
			break;
		case RIGHT_TOP:
			selectRightTop();
			break;
		case RIGHT_BOT:
			selectRightBot();
			break;
		case MID:
			popoutAll();
			break;
		}
	}

	public void applyPopupAnimations() {
		setHeadAnimation(spine_head_left, HeadAnimation.POPUP);
		setHeadAnimation(spine_head_right, HeadAnimation.POPUP);
//		setSkillAnimation(spine_skill1_left, SkillAnimation.POPUP_AVAIL);
//		setSkillAnimation(spine_skill2_left, SkillAnimation.POPUP_AVAIL);
//		setSkillAnimation(spine_skill1_right, SkillAnimation.POPUP_AVAIL);
//		setSkillAnimation(spine_skill2_right, SkillAnimation.POPUP_AVAIL);
	}

	private void selectLeftTop() {
		// select
		setSkillAnimation(spine_skill1_left, SkillAnimation.SELECTED);

		// popout
		setHeadAnimation(spine_head_left, HeadAnimation.POPOUT);
		setSkillAnimation(spine_skill2_left, SkillAnimation.POPOUT);
		popoutRight();
	}

	private void selectRightTop() {
		// select
		setSkillAnimation(spine_skill1_right, SkillAnimation.SELECTED);

		// popout
		setHeadAnimation(spine_head_right, HeadAnimation.POPOUT);
		setSkillAnimation(spine_skill2_right, SkillAnimation.POPOUT);
		popoutLeft();
	}

	private void selectLeftBot() {
		// select
		setSkillAnimation(spine_skill2_left, SkillAnimation.SELECTED);

		// popout
		setHeadAnimation(spine_head_left, HeadAnimation.POPOUT);
		setSkillAnimation(spine_skill1_left, SkillAnimation.POPOUT);
		popoutRight();
	}

	private void selectRightBot() {
		// select
		setSkillAnimation(spine_skill2_right, SkillAnimation.SELECTED);

		// popout
		setHeadAnimation(spine_head_right, HeadAnimation.POPOUT);
		setSkillAnimation(spine_skill1_right, SkillAnimation.POPOUT);
		popoutLeft();
	}

	public short getIDAllySelected() {
		return id_ally_selected;
	}

	public WorldAction getSkillSelected() {
		return skill_selected;
	}

	public boolean isActive() {
		return active;
	}

	private void setHeadAnimation(CustomSpineWidget spineActor, HeadAnimation animation) {

		// animation rules

		if (animation.name.equals(spineActor.animation.getName()) == true)
			return;

		// has to be active before inactive can be applied
		if (animation.name.equals(HeadAnimation.INACTIVE.name) == true)
			if (spineActor.animation.getName().equals(HeadAnimation.ACTIVE.name) == false)
				return;

		if (animation.name.equals(HeadAnimation.POPUP.name) == true)
			if (spineActor.animation.getName().equals(HeadAnimation.POPOUT.name) == false
					&& spineActor.animation.getName().equals(HeadAnimation.SELECTED.name) == false)
				return;

		spineActor.setAnimationTime(0);
		spineActor.setAnimation(animation.name);
	}

	private void setSkillAnimation(CustomSpineWidget spineActor, SkillAnimation animation) {

		// animation rules

		if (animation.name.equals(spineActor.animation.getName()) == true)
			return;

		/*
		 * ignore popout if actor had popout previously (if-clause above) or if actor
		 * has been selected (if-clause below)
		 * 
		 */
		if (animation.name.equals(SkillAnimation.POPOUT.name) == true)
			if (spineActor.animation.getName().equals(SkillAnimation.SELECTED.name) == true)
				return;

		if (animation.name.equals(SkillAnimation.POPUP.name) == true)
			if (spineActor.animation.getName().equals(SkillAnimation.POPOUT.name) == false
					&& spineActor.animation.getName().equals(SkillAnimation.SELECTED.name) == false)
				return;

		spineActor.setAnimationTime(0);
		spineActor.setAnimation(animation.name);
	}

	private void popoutAll() {
		popoutLeft();
		popoutRight();
	}

	private void popoutRight() {
		setHeadAnimation(spine_head_right, HeadAnimation.POPOUT);
		setSkillAnimation(spine_skill1_right, SkillAnimation.POPOUT);
		setSkillAnimation(spine_skill2_right, SkillAnimation.POPOUT);
	}

	private void popoutLeft() {
		setHeadAnimation(spine_head_left, HeadAnimation.POPOUT);
		setSkillAnimation(spine_skill1_left, SkillAnimation.POPOUT);
		setSkillAnimation(spine_skill2_left, SkillAnimation.POPOUT);
	}

	/**
	 * layout-method
	 * 
	 * @return
	 */
	public CustomTable createTableHeads() {

		CustomTable tableHead = new CustomTable();
		tableHead.setFillParent(true);
		tableHead.center();

		float stageWidth = stage.getWidth();

		tableHead.add(spine_head_left).padRight(stageWidth * 2 * PAD_HEADS_HORIZONTAL_PERCENT);
		tableHead.add(spine_head_right);

		return tableHead;
	}

	public CustomTable createTableSkill1() {
		float stageWidth = stage.getWidth();
		float stageHeight = stage.getHeight();

		CustomTable tableSkill1 = new CustomTable();
		tableSkill1.setFillParent(true);
		tableSkill1.center().padBottom(stageHeight * PAD_SKILLS_VERTICAL_PERCENTAGE);

		tableSkill1.add(spine_skill1_left).padRight(stageWidth * 2 * PAD_SKILLS_HORIZONTAL_PERCENTAGE);
		tableSkill1.add(spine_skill1_right);

		return tableSkill1;
	}

	public CustomTable createTableSkill2() {
		float stageWidth = stage.getWidth();
		float stageHeight = stage.getHeight();

		CustomTable tableSkill2 = new CustomTable();
		tableSkill2.setFillParent(true);
		tableSkill2.center().padTop(stageHeight * PAD_SKILLS_VERTICAL_PERCENTAGE);

		tableSkill2.add(spine_skill2_left).padRight(stageWidth * 2 * PAD_SKILLS_HORIZONTAL_PERCENTAGE);
		tableSkill2.add(spine_skill2_right);

		return tableSkill2;

	}
}
