package com.elementar.gui.widgets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;

public class CustomLabel extends Label {

	protected Sprite background_sprite;

	/**
	 * Don't use null parameters
	 * 
	 * @param text
	 * @param style
	 * @param backgroundSprite
	 * @param align
	 */
	public CustomLabel(CharSequence text, LabelStyle style, Sprite backgroundSprite, int align) {
		this(text, style, backgroundSprite.getWidth(), backgroundSprite.getHeight(), align);
		this.background_sprite = new Sprite(backgroundSprite);
	}

	/**
	 * Label with no background-images. Don't use null parameters
	 * 
	 * @param text
	 * @param style
	 * @param width
	 * @param height
	 * @param align
	 */
	public CustomLabel(CharSequence text, LabelStyle style, float width, float height, int align) {
		super(text, style);
		setAlignment(align);
		setSize(width, height);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {

		ShaderPolyBatch shaderBatch = (ShaderPolyBatch) batch;

		GUIUtil.determineHoverEffect(shaderBatch, State.NOT_SELECTED);

		if (background_sprite != null) {
			background_sprite.setPosition(getX(),
					getY() - 0.5f * Math.abs(background_sprite.getHeight() - getHeight()));
			background_sprite.draw(shaderBatch);
		}

		super.draw(shaderBatch, parentAlpha);
		shaderBatch.flush();
	}

	@Override
	public void setPosition(float x, float y) {
		super.setPosition(x, y);
	}

	@Override
	public float getPrefWidth() {
		return getWidth();
	}

	@Override
	public float getPrefHeight() {
		return getHeight();
	}

	public void setAlpha(float alpha) {
		getColor().a = alpha;
		getColor().clamp();
	}

	public Sprite getBGSprite() {
		return background_sprite;
	}

}
