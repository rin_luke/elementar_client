package com.elementar.gui.widgets;

import java.util.ArrayList;

import com.elementar.data.container.TextData;
import com.elementar.logic.util.Shared;

/**
 * 
 * @author lukassongajlo
 * 
 */
public class LobbySlotContextMenu extends AContextMenu {

	private CustomTextbutton send_friend_request_button, report_player_button, invite_to_lobby;

	public LobbySlotContextMenu() {
		super();
	}

	@Override
	protected void loadButtons() {
		float width = Shared.WIDTH4, height = Shared.HEIGHT1;

		send_friend_request_button = new CustomTextbutton(TextData.getWord("sendFriendRequest"),
				button_style, width, height, align);

		report_player_button = new CustomTextbutton(TextData.getWord("reportPlayer"), button_style,
				width, height, align);
		invite_to_lobby = new CustomTextbutton(TextData.getWord("inviteToLobby"), button_style,
				width, height, align);
		
		

	}

	@Override
	protected void setListeners() {

	}

	@Override
	protected void addButtons(ArrayList<CustomTextbutton> items) {
		items.add(send_friend_request_button);
		items.add(report_player_button);

	}

}
