package com.elementar.gui.widgets;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.GameclassSkinContainer;
import com.elementar.data.container.ParticleContainer;
import com.elementar.data.container.SettingsLoader;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.data.container.util.CustomParticleEffect;
import com.elementar.data.container.util.Gameclass;
import com.elementar.data.container.util.GameclassSkin;
import com.elementar.data.container.util.GameclassSkin.SkinAvailability;
import com.elementar.gui.GUITier;
import com.elementar.gui.abstracts.AGameclassSelectionModule;
import com.elementar.gui.modules.selection.GameclassCollectionModule;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.WidgetGroupable;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.util.Shared;

/**
 * Represents a gameclass with purchaseable skins, a model in the center, skill
 * thumbnails, name, rareness. Used in {@link GameclassCollectionModule}.
 * 
 * @author lukassongajlo
 * 
 */
public class GameclassIconbutton extends CustomIconbutton implements WidgetGroupable {

	public static GameclassSkin LAST_PURCHASED_SKIN = null;

	public static float SKIN_CELL_WIDTH = 280f;
	public static float SKIN_CELL_HEIGHT = 280f;
	// -1 bis 1 0 bis 1
	private static final float DIMMING_BRIGHTNESS = -0.25f, DIMMING_CONTRAST = 0.5f;

	private ArrayList<GameclassSkin> skins = new ArrayList<GameclassSkin>();

	private Gameclass gameclass;

	private CustomTable model_table, desc_table, skins_table, purchase_buttons_at_skin_table, equip_buttons_table;

	/**
	 * label which shows the item description
	 */
	private CustomLabel description_label;

	private Cell<?> purchase_button_cell_at_desc;

	/**
	 * Displayed in the screen center
	 */
	private DrawableClient character_model;

	/**
	 * Contains visual widgets for displaying the skins
	 */
	private HorizontalGroup skin_group;

	/**
	 * Each child is a {@link CustomIconbutton} with a thumbnail of the skill
	 */
	private HorizontalGroup skill_group;

	private ArrayList<CustomTextbutton> show_buttons = new ArrayList<>();
	private ArrayList<CustomTextbutton> equip_buttons = new ArrayList<>();
	private ArrayList<CustomTextbutton> purchase_buttons = new ArrayList<>();

	/**
	 * index of this button in context of all filtered buttons
	 */
	private int index;

	private boolean crossedout;

	private boolean has_purchase_section;

	/**
	 * 
	 * @param gameclass
	 * @param characterModel
	 * @param hasPurchaseSection
	 */
	public GameclassIconbutton(final Gameclass gameclass, DrawableClient characterModel, boolean hasPurchaseSection) {
		super(gameclass.getSymbol(), StaticGraphic.gui_bg_circle);
		onewayClicklistener(true);
		this.gameclass = gameclass;
		this.has_purchase_section = hasPurchaseSection;
		character_model = characterModel;

		// model table
		model_table = new CustomTable();
		model_table.setFillParent(true);

		CustomSpineWidget modelSpineWidget = new CustomSpineWidget(character_model) {
			@Override
			public void draw(Batch batch, float parentAlpha) {
				ShaderPolyBatch shaderBatch = (ShaderPolyBatch) batch;

				super.draw(batch, parentAlpha);

				shaderBatch.setShader(shaderBatch.sprite_shader);
			}
		};
		model_table.add(modelSpineWidget);
		// desc table
		desc_table = new CustomTable();
		desc_table.top().setFillParent(true);
		desc_table.padLeft(1100).padTop(AGameclassSelectionModule.PAD_TOP_RIGHT_SIDE);

		// description
		Sprite symbolSprite = new Sprite(gameclass.getSymbol());
		CustomIconbutton symbolImage = new CustomIconbutton(symbolSprite, 110, 110);
		CustomLabel gameclassLabel = new CustomLabel(gameclass.getName().toUpperCase(),
				StyleContainer.label_bold_whitepure_40_style, Shared.WIDTH7, Shared.HEIGHT1, Align.left);
		CustomLabel roleLabel = new CustomLabel(TextData.getWord(gameclass.getRole().toString().toLowerCase()),
				StyleContainer.label_medium_whitepure_20_style, Shared.WIDTH7, 25f, Align.left);
		description_label = new CustomLabel("", StyleContainer.label_medium_whitepure_20_style, Shared.WIDTH7, 35,
				Align.left);

		desc_table.add(symbolImage);
		VerticalGroup vGroup = new VerticalGroup();
		vGroup.addActor(gameclassLabel);
		vGroup.addActor(roleLabel);
		desc_table.add(vGroup);
		desc_table.row();
		desc_table.add();

		// skills
		skill_group = gameclass.getSkillGroupSelection();

		desc_table.add(skill_group).left();
		desc_table.row().padTop(20);

		if (hasPurchaseSection == true) {
			desc_table.add();
			vGroup = new VerticalGroup();
			vGroup.addActor(description_label);
			desc_table.add(vGroup);
		}

		desc_table.row().padTop(10);
		desc_table.add();
		purchase_button_cell_at_desc = desc_table.add().left();

		// skin section
		skins_table = new CustomTable();
		skins_table.setFillParent(true);
		skins_table.bottom();// .padBottom(100);

		skin_group = new HorizontalGroup();

		purchase_buttons_at_skin_table = new CustomTable();
		purchase_buttons_at_skin_table.bottom().padBottom(50).setFillParent(true);

		equip_buttons_table = new CustomTable();
		equip_buttons_table.bottom().padBottom(75).setFillParent(true);

		skins_table.add(skin_group);

	}

	public void updateSubSection() {

		// Has a new skin loaded via Steam callbacks?
		for (GameclassSkin skinSteam : GameclassSkinContainer.SKINS.values()) {

			if (skinSteam.getGameclassName() == gameclass.getNameAsEnum()) {
				boolean skinInPlace = false;
				for (GameclassSkin skin : skins)
					if (skin == skinSteam)
						skinInPlace = true;

				// layout new skin
				if (skinInPlace == false) {

					layoutSkinWidgets(skinSteam);
					// add new skin by Steam.
					skins.add(skinSteam);

				}

			}

		}

	}

	private void layoutSkinWidgets(GameclassSkin skin) {

		float yShift = -60;

		// shine pedestal
		CustomEffectWidget shinePedestalEffect = new CustomEffectWidget(
				new CustomParticleEffect(ParticleContainer.particle_skin_pedestal));
		// shine purchase
		CustomEffectWidget shinePurchaseEffect = new CustomEffectWidget(
				new CustomParticleEffect(ParticleContainer.particle_skin_buy));
		// shine equipped
		CustomIconbutton equippedImage = new CustomIconbutton(StaticGraphic.gui_equipped_indicator,
				StaticGraphic.gui_equipped_indicator.getWidth(), StaticGraphic.gui_equipped_indicator.getHeight());
		equippedImage.setIconShiftY(yShift);
		shinePedestalEffect.setShiftY(yShift + 20);
		shinePurchaseEffect.setShiftY(yShift + 20);

		// buy buttons
		CustomTextbutton purchaseButtonAtDesc = new CustomTextbutton("", StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_buy_big, Align.center) {

			@Override
			public void act(float delta) {
				super.act(delta);

				this.setText(TextData.getWord("purchase") + " " + skin.getCurrency() + " " + skin.getPrice());

			}
		};

		purchaseButtonAtDesc.setSound(ClickSoundType.HEAVY);
		purchaseButtonAtDesc.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				GUITier.STEAM_INTERFACE.inventory.startPurchase(/* ids */new int[] { skin.getItemDefID() },
						/* quantity */ new int[] { 1 });

				LAST_PURCHASED_SKIN = skin;

			}
		});

		CustomTextbutton purchaseButtonAtSkin = new CustomTextbutton("", StyleContainer.button_bold_20_style,
				StaticGraphic.gui_bg_buy, Align.center) {

			@Override
			public void act(float delta) {
				super.act(delta);

				this.setText(skin.getCurrency() + " " + skin.getPrice());

			}
		};

		purchaseButtonAtSkin.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);

				GUITier.STEAM_INTERFACE.inventory.startPurchase(/* ids */new int[] { skin.getItemDefID() },
						/* quantity */ new int[] { 1 });

				LAST_PURCHASED_SKIN = skin;
			}
		});

		purchaseButtonAtSkin.setSound(ClickSoundType.LIGHT);
		purchase_buttons_at_skin_table.add(purchaseButtonAtSkin)
				.spaceLeft(SKIN_CELL_WIDTH - purchaseButtonAtSkin.getWidth());

		// buyButtons collects btns for hover effect
		purchase_buttons.add(purchaseButtonAtSkin);
		purchase_buttons.add(purchaseButtonAtDesc);

		// show button
		CustomTextbutton showButton = new CustomTextbutton();

		// equip button
		CustomTextbutton equipButton = new CustomTextbutton(TextData.getWord("equip"),
				StyleContainer.button_bold_20_style, StaticGraphic.gui_bg_equip, Align.center);
		equip_buttons.add(equipButton);
		equipButton.setSound(ClickSoundType.HEAVY);
		equipButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				// set all skins to not equipped
				for (GameclassSkin skinLoop : skins)
					skinLoop.setEquipped(false);
				// select the equipped one
				skin.setEquipped(true);
				equipButton.setVisible(false);
				SettingsLoader.save();

				GUIUtil.clickActor(showButton);

			}

			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				if (toActor != showButton && toActor != showButton.getLabel() && toActor != equipButton
						&& toActor != equipButton.getLabel() && pointer == -1) {
					equipButton.setVisible(false);
					/*
					 * Note: invisible widget won't be handled by Hoverhandler anymore, so we have
					 * to set the state manually
					 */
					equipButton.setState(State.NOT_SELECTED);
				}
			}

		});

		equipButton.setVisible(false);

		if (skin.isEquipped() == true)
			// initial click
			GUIUtil.clickActor(equipButton);

		equip_buttons_table.add(equipButton).spaceLeft(SKIN_CELL_WIDTH - equipButton.getWidth());

		showButton.setSize(SKIN_CELL_WIDTH, SKIN_CELL_HEIGHT);
		showButton.onewayClicklistener(true);

		// pedestal image
		CustomSpineWidget spinePlatform = new CustomSpineWidget(
				AnimationContainer.makeHomeScreenAnimation(
						Gdx.files.internal("graphic/animation_home_screen_skin_platform.json"), 0.4f),
				"skin_platform_idle", true);
		spinePlatform.setSize(SKIN_CELL_WIDTH, SKIN_CELL_HEIGHT);
		spinePlatform.setShiftY(yShift);

		showButton.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {

				if (character_model != null) {

					/*
					 * first click has also to be considered, so the following lines has to be
					 * outside the if-statement
					 */
					description_label.setText(skin.getDescription());

					if (has_purchase_section == true && skin.getAvailability() == SkinAvailability.NOT_PAID)
						purchase_button_cell_at_desc.setActor(purchaseButtonAtDesc);
					else
						purchase_button_cell_at_desc.setActor(null);

					boolean differentGameclass = character_model.getGameclass() != gameclass;
					/*
					 * change gameclass only if a new gameclass is chosen, or when the same
					 * gameclass remains but the skin index changed
					 */
					if (differentGameclass == true || (differentGameclass == false
							&& character_model.getMetaClient().skin_index != skin.getIndex())) {

						character_model.getMetaClient().setGameclass(gameclass.getNameAsEnum());
						character_model.getMetaClient().skin_index = skin.getIndex();
						character_model.setSkin(skin);

						character_model.updateByMetaData(character_model.isFlip());
					}

				}
			}

			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				if (skin.getAvailability() == SkinAvailability.AVAILABLE && skin.isEquipped() == false)
					equipButton.setVisible(true);
			}

			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				if (toActor != equipButton && toActor != equipButton.getLabel() && pointer == -1) {

					equipButton.setVisible(false);
					/*
					 * Note: invisible widget won't be handled by Hoverhandler anymore, so we have
					 * to set the state manually
					 */
					equipButton.setState(State.NOT_SELECTED);
				}
			}
		});

		show_buttons.add(showButton);

		// flip = true
		DrawableClient tinyModel = new DrawableClient(new MetaClient(gameclass.getNameAsEnum(), skin.getIndex()),
				AnimationContainer.makeCharacterAnimation(0.2f), false);

		CustomSpineWidget tinySpineActor = new CustomSpineWidget(tinyModel) {

			@Override
			public void draw(Batch batch, float parentAlpha) {
				ShaderPolyBatch shaderBatch = (ShaderPolyBatch) batch;

				/*
				 * the method 'determineHoverEffect' handles both cases SELECTED and HOVERED, in
				 * this case we need a more specific behaviour
				 */
				determineDimming(showButton, equipButton, skin, shaderBatch);

				super.draw(batch, parentAlpha);

				shaderBatch.setShader(shaderBatch.sprite_shader);
				shaderBatch.reset();
			}
		};

		tinySpineActor.setSize(SKIN_CELL_WIDTH, SKIN_CELL_HEIGHT);
		// init widgets finished

		Stack stack = new Stack() {
			@Override
			public void draw(Batch batch, float parentAlpha) {
				setSkinVisibility(skin, shinePedestalEffect, shinePurchaseEffect, equippedImage, purchaseButtonAtSkin,
						purchaseButtonAtDesc);
				super.draw(batch, parentAlpha);

			}
		};
		// add widgets to stack
		stack.addActor(spinePlatform);
		stack.addActor(equippedImage);
		stack.addActor(shinePurchaseEffect);
		stack.addActor(shinePedestalEffect);
		stack.addActor(tinySpineActor);
		stack.addActor(showButton);

		VerticalGroup stackWrapper = new VerticalGroup();
		stackWrapper.addActor(stack);

		// add visuals
		skin_group.addActor(stackWrapper);

	}

	@Override
	protected void drawAdditionalSprites(ShaderPolyBatch batch) {

		/**
		 * setPosition(...,...) has to be called continuously because icon could be
		 * changed
		 */
		if (state == State.DISABLED)
			current_icon.setAlpha(Shared.WIDGET_DISABLED_ALPHA);
		current_icon.setPosition(getX() + getWidth() * 0.5f - current_icon.getWidth() * 0.5f + shift_x,
				getY() + getHeight() * 0.5f - current_icon.getHeight() * 0.5f + shift_y);
		current_icon.draw(batch);
		current_icon.setAlpha(1f);

		if (crossedout == true) {
			StaticGraphic.gui_icon_crossout.setPosition(
					getX() + getWidth() * 0.5f - StaticGraphic.gui_icon_crossout.getWidth() * 0.5f + shift_x,
					getY() + getHeight() * 0.5f - StaticGraphic.gui_icon_crossout.getHeight() * 0.5f + shift_y);
			StaticGraphic.gui_icon_crossout.draw(batch);
		}
		batch.flush();
		batch.reset();

	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		ShaderPolyBatch shaderBatch = (ShaderPolyBatch) batch;

		// state = State.DISABLED;

		GUIUtil.determineHoverEffect(shaderBatch, state);

		if (background != null) {
			if (state == State.DISABLED)
				background.setAlpha(Shared.WIDGET_DISABLED_ALPHA);

			background.setPosition(getX(), getY());
			background.draw(shaderBatch);
			background.setAlpha(1f);
		}
		drawAdditionalSprites(shaderBatch);

		// // DETERMINE FONT
		// if (state == State.HOVERED || state == State.SELECTED)
		// getStyle().fontColor = color_pure;
		// else if (state == State.DISABLED)
		// getStyle().fontColor = color_disabled;
		// else
		// getStyle().fontColor = color_dimmed;

		super.draw(shaderBatch, parentAlpha);

		shaderBatch.flush();
		shaderBatch.reset();

	}

	private void determineDimming(CustomTextbutton showButton, CustomTextbutton equipButton, GameclassSkin skin,
			ShaderPolyBatch shaderBatch) {
		if (showButton.state == State.HOVERED || equipButton.state == State.HOVERED)
			shaderBatch.reset();
		else if (showButton.state == State.NOT_SELECTED) {
			shaderBatch.setBrightness(DIMMING_BRIGHTNESS);
			// shaderBatch.setContrast(DIMMING_CONTRAST);
		}
	}

	private void setSkinVisibility(GameclassSkin skin, CustomEffectWidget shinePedestalEffect,
			CustomEffectWidget shineBuyEffect, CustomIconbutton equippedImage, CustomTextbutton purchaseButtonAtSkin,
			CustomTextbutton purchaseButtonAtDesc) {

		equippedImage.setVisible(skin.isEquipped());
		if (skin.getAvailability() == SkinAvailability.NOT_PAID) {
			shineBuyEffect.setVisible(true);
			purchaseButtonAtSkin.setVisible(true);
			purchaseButtonAtDesc.setVisible(true);
			shinePedestalEffect.setVisible(false);
		} else {
			// skin is available
			shinePedestalEffect.setVisible(true);
			shineBuyEffect.setVisible(false);
			purchaseButtonAtSkin.setVisible(false);
			purchaseButtonAtDesc.setVisible(false);
		}
	}

	public void addToTables(ArrayList<CustomTable> tables) {

		for (Actor actor : skill_group.getChildren()) {
			if (actor instanceof SkillButton) {
				SkillButton skillButton = (SkillButton) actor;
				skillButton.addTooltip(tables, desc_table);
			}
		}

		tables.add(model_table);

		tables.add(desc_table);

		tables.add(skins_table);

		tables.add(purchase_buttons_at_skin_table);

		tables.add(equip_buttons_table);

		// converting purchasebuttons for hoverhandler
		ArrayList<StatePossessable> convPurchaseButtons = new ArrayList<>(purchase_buttons);
		new HoverHandler(convPurchaseButtons);

		// converting showbuttons for hoverhandler
		ArrayList<StatePossessable> convShowButtons = new ArrayList<>(show_buttons);
		new HoverHandler(convShowButtons);

		// converting equipbuttons for hoverhandler
		ArrayList<StatePossessable> convEquipButtons = new ArrayList<>(equip_buttons);
		new HoverHandler(convEquipButtons);

	}

	public Gameclass getGameClass() {
		return gameclass;
	}

	@Override
	public void setVisibilityWidgets(boolean visible) {

		for (Actor a : model_table.getChildren())
			a.setVisible(visible);

		for (Actor a : desc_table.getChildren())
			a.setVisible(visible);

		for (Actor a : purchase_buttons_at_skin_table.getChildren())
			a.setVisible(visible);

		for (Actor a : skill_group.getChildren())
			a.setVisible(visible);

		for (Actor a : skin_group.getChildren())
			a.setVisible(visible);

	}

	public void clickShowButton(int index) {

		if (show_buttons.size() > index)
			GUIUtil.clickActor(show_buttons.get(index));
	}

	public void setCrossedOut(boolean crossedout) {
		this.crossedout = crossedout;
	}

	/**
	 * Sets the index of this button in context of all filtered gameclass buttons.
	 * 
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}

	public ArrayList<GameclassSkin> getSkins() {
		return skins;
	}

}
