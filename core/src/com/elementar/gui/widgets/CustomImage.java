package com.elementar.gui.widgets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

/**
 * 
 * @author lukassongajlo
 *
 */
public class CustomImage extends WidgetGroup {

	private Sprite sprite;

	private boolean center_aligned = false, bottom_aligned = false;

	public CustomImage(Sprite sprite) {
		this.sprite = sprite;
	}

	@Override
	public void act(float delta) {
		if (isVisible() == true) {
			float offsetX = 0, offsetY = 0;
			if (center_aligned == true) {
				offsetX = getWidth() * 0.5f - sprite.getBoundingRectangle().width * 0.5f;
				// offsetY = getHeight() * 0.5f
				// - sprite.getBoundingRectangle().height * 0.5f;
			}
			if (bottom_aligned == true)
				offsetY = -sprite.getBoundingRectangle().height * 0.5f;

			sprite.setPosition(
					getX() + offsetX
							+ 0.5f * (sprite.getBoundingRectangle().width - sprite.getWidth()),
					getY() + offsetY
							+ 0.5f * (sprite.getBoundingRectangle().height - sprite.getHeight()));
			// sprite.setPosition(getX() + offsetX, getY() + offsetY);
		}
		super.act(delta);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {

		if (isVisible() == true) {
			sprite.getColor().a = parentAlpha;
			sprite.draw(batch);
			batch.flush();
		}

	}

	public Sprite getSprite() {
		return sprite;
	}

	/**
	 * 
	 * @param centerAligned
	 */
	public void setCenter(boolean centerAligned) {
		center_aligned = centerAligned;
	}

	/**
	 * 
	 * @param bottomAligned
	 */
	public void setBottom(boolean bottomAligned) {
		bottom_aligned = bottomAligned;
	}

	@Override
	public float getPrefHeight() {
		return getHeight();
	}

	@Override
	public float getPrefWidth() {
		return getWidth();
	}
}
