package com.elementar.gui.widgets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.elementar.data.container.AudioData;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.widgets.interfaces.StatePossessable;

public class CustomCheckbox extends CheckBox implements StatePossessable {

	private State state;

	private boolean hovered;

	public CustomCheckbox(CheckBoxStyle checkboxStyle) {
		super("", checkboxStyle);
		state = State.NOT_SELECTED;
		addListener(getHoverListener());
		addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				AudioData.playEffect(AudioData.gui_btn_click_light1);
			}
		});
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		ShaderPolyBatch shaderBatch = (ShaderPolyBatch) batch;
		GUIUtil.determineHoverEffect(shaderBatch, state);
		super.draw(shaderBatch, parentAlpha);
		shaderBatch.flush();
	}

	@Override
	public State getState() {
		return state;
	}

	@Override
	public void setState(State state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "checkbox, checked: " + isChecked();
	}

	@Override
	public void setHovered(boolean hovered) {
		this.hovered = hovered;
	}

	@Override
	public boolean isHovered() {
		return hovered;
	}
}
