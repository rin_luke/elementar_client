package com.elementar.gui.widgets;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.GameclassContainer;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.Team;

/**
 * Act as bounding button around the whole slot
 * 
 * @author lukassongajlo
 * 
 */
public class LobbySlotContainer extends VerticalGroup {

	private int index;
	private Team team;
	private CustomIconbutton thumbnail_button;
	private CustomLabel playername_label;

	private boolean preview = false;

	public LobbySlotContainer(int index, Team team, final LobbySlotContextMenu contextMenu) {
		super();

		setSize(Shared.WIDTH5, Shared.HEIGHT2);

		this.team = team;
		this.index = index;

		thumbnail_button = new CustomIconbutton(null, StaticGraphic.gui_bg_circle_lobby);
		thumbnail_button.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				if (button == Buttons.RIGHT) {
					/**
					 * TODO first check availabilities of the context menu. Menu
					 * points couldnt be available for some reason ( i.e no
					 * player to report, no player to send whisper message)
					 */
					synchronized (contextMenu) {

						contextMenu.setVisible(true);
						// contextMenu.setPosition();
						// contextMenu.setTeamSlotPair(new
						// TeamSlotPair(LobbySlotContainer.this.team,
						// LobbySlotContainer.this.index));
					}
				}
				return false;
			}
		});

		playername_label = new CustomLabel("", StyleContainer.label_medium_whitepure_20_style,
				StaticGraphic.gui_bg_circle_lobby.getWidth(), Shared.HEIGHT1, Align.center);

		addActor(thumbnail_button);
		addActor(playername_label);

	}

	// der hat auch ein pre und ein normales, bzw. zwei modi, und beide können
	// gesetzt werden wobei der selected dann stärker gewichtet wird, s/w shader
	// finden
	@Override
	public void draw(Batch batch, float parentAlpha) {

		if (thumbnail_button.getIcon() != null) {
			if (preview == true)
				thumbnail_button.getIcon().setColor(0.5f, 0.5f, 0.5f, 1);
			else
				thumbnail_button.getIcon().setColor(1, 1, 1, 1);
		}
		super.draw(batch, parentAlpha);
	}

	public int getIndex() {
		return index;
	}

	/**
	 * If <code>true</code> the thumbnail is drawn in a dimmed way
	 * 
	 * @param preview
	 */
	public void setPreview(boolean preview) {
		this.preview = preview;
	}

	public void setThumbnail(GameclassName gameclassName) {
		/*
		 * etwas unschön frameweise immer 'new' aufzurufen, aber sei's drum
		 */
		if (gameclassName != null) {
			Sprite symbolSprite = new Sprite(
					GameclassContainer.filterByName(gameclassName).getSymbol());
			symbolSprite.setScale(0.85f);
			thumbnail_button.setIcon(symbolSprite);
		}
	}

	/**
	 * Use this to set the playername
	 */
	@Override
	public void setName(String name) {
		playername_label.setText(name);
	}

	public void reset() {
		playername_label.setText("");
		thumbnail_button.setIcon(null);
	}


}
