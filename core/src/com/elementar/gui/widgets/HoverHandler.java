package com.elementar.gui.widgets;

import java.util.ArrayList;

import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;
import com.elementar.logic.util.lists.NotNullArrayList;

/**
 * 
 * This class handles the {@link State#HOVERED}, {@link State#SELECTED} and
 * {@link State#NOT_SELECTED} states of widgets. Before, the widgets must be
 * added to a handler object. There are some cases that you should keep in mind
 * when using this class
 * <p>
 * In code there might be case where 2 widgets are set to {@link State#SELECTED}
 * . Consider, depending on the order the widget were added, the last widget get
 * the highest priority.
 * <p>
 * The order defines also the hovered widget. Globally (over all
 * {@link HoverHandler} objects) just one hovered widget is allowed. That's the
 * reason why {@link #last_hovered} is declared as a static field. So besides
 * the adding order of the widgets, the adding order of the {@link HoverHandler}
 * objects matters.
 * <p>
 * Imagine a normal scene. Now the player opens a context menu by right-clicking
 * a button. This context menu overlaps the button a little bit, so it might be
 * the case that both the button and one point of the context menu got hovered.
 * Now just one widget is allowed to be hovered. In this case the
 * {@link HoverHandler} object of the context menu should be the last added
 * {@link HoverHandler} object of all objects in {@link #HOVER_HANDLERS}
 * 
 * @author lukassongajlo
 * 
 */
public class HoverHandler {

	private static NotNullArrayList<HoverHandler> HOVER_HANDLERS
			= new NotNullArrayList<HoverHandler>();
	public static HoverHandler NOT_NULL_HOVERHANDLER = new HoverHandler(null);

	private ArrayList<StatePossessable> state_possessables;

	private boolean active = true;

	/**
	 * this field is necessary to keep in storage the old selected widget to set
	 * its state to NOT_SELECTED
	 */
	private StatePossessable last_selected;
	private static StatePossessable last_hovered;

	/**
	 * Creates a new hover-handler and adds it to {@link #HOVER_HANDLERS}
	 * 
	 * @param widgets
	 */
	public HoverHandler(ArrayList<StatePossessable> widgets) {
		this(widgets, null);
	}

	/**
	 * 
	 * Creates a new hover-handler and adds it to {@link #HOVER_HANDLERS}
	 * 
	 * @param widgets
	 * @param currentSelected
	 */
	public HoverHandler(ArrayList<StatePossessable> widgets, StatePossessable currentSelected) {
		this.state_possessables = widgets;
		this.last_selected = currentSelected;
		if (currentSelected != null)
			currentSelected.setState(State.SELECTED);
		add(this);
	}

	public static void update() {
		for (HoverHandler handler : HOVER_HANDLERS)
			handler.updateHover();
	}

	/**
	 * This method is called for each hover_handler added in
	 * {@link #HOVER_HANDLERS}.
	 * <p>
	 * We also count the number of <not-hovered> widgets to recognize when a
	 * widget is not hovered anymore.
	 * <p>
	 */
	private void updateHover() {
		if (active == true)
			for (StatePossessable widget : state_possessables) {

				if (widget.getState() != State.DISABLED)
					if (widget.isVisible() == true) {
						/*
						 * it's important to set the priority like this. First
						 * checking whether selected or not, then hover, then
						 * not-selected. Imagine I would start with
						 * hover-checking: if(inBounds() == true) and what
						 * happened in the else case? A widget would never get
						 * selected.
						 */
						if (widget.getState() == State.SELECTED) {
							if (last_selected != null && last_selected.getState() != State.DISABLED)
								last_selected.setState(State.NOT_SELECTED);
							last_selected = widget;
							last_selected.setState(State.SELECTED);
							// transition if the selected was the last hovered:
							if (last_selected == last_hovered)
								last_hovered = null;
						} else if (widget.isHovered() == true) {

							/*
							 * second part of the if statement is needed for
							 * buttons, when they become DISABLED after click
							 * (for example select button gameclass selection)
							 */
							if (last_hovered != null && last_hovered.getState() != State.DISABLED) {
								last_hovered.setState(State.NOT_SELECTED);
							}
							last_hovered = widget;
							last_hovered.setState(State.HOVERED);
						} else {
							// not selected-case
							widget.setState(State.NOT_SELECTED);
						}
					}
			}

	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public StatePossessable getCurrentSelected() {
		return last_selected;
	}

	public void remove(StatePossessable widget) {
		state_possessables.remove(widget);
	}

	public void add(StatePossessable widget) {
		state_possessables.add(widget);
	}

	public static void remove(HoverHandler handler) {
		HOVER_HANDLERS.remove(handler);
	}

	public static void add(HoverHandler handler) {
		HOVER_HANDLERS.add(handler);
	}

	public static void clear() {
		HOVER_HANDLERS.clear();
	}

	/**
	 * global activate, at the moment just needed for cover-modules
	 */
	public static void setActiveGlobally(boolean activeGlobal) {
		for (HoverHandler handler : HOVER_HANDLERS)
			handler.setActive(activeGlobal);
	}
}