package com.elementar.gui.widgets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.util.CustomSkeleton;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.logic.characters.DrawableClient;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.Animation.MixBlend;
import com.esotericsoftware.spine.Animation.MixDirection;

/**
 * Centric spine
 * 
 * @author lukassongajlo
 *
 */
public class CustomSpineWidget extends Actor {

	protected CustomSkeleton skeleton;
	protected Animation animation;
	protected boolean loop;
	protected float animation_time;

	protected DrawableClient client_model;

	protected float shift_x, shift_y;

	/**
	 * {@link MixBlend#first} by default, but can be set in
	 * {@link #setMixBehaviour(MixBlend, MixDirection)}
	 */
	protected MixBlend blend;

	/**
	 * {@link MixDirection#in} by default, but can be set in
	 * {@link #setMixBehaviour(MixBlend, MixDirection)}
	 */
	protected MixDirection direction;

	public CustomSpineWidget(CustomSkeleton skeleton, String animationName, boolean loop) {
		this.skeleton = skeleton;
		this.animation = skeleton.getData().findAnimation(animationName);
		this.loop = loop;

		blend = MixBlend.setup;
		direction = MixDirection.in;

	}

	public CustomSpineWidget(DrawableClient clientModel) {
		client_model = clientModel;
	}

	@Override
	public void act(float delta) {

		if (isVisible() == true)
			if (client_model != null) {
				client_model.getSkeleton().setPosition(getX() + getWidth() * 0.5f, getY() + getHeight() * 0.5f);
				client_model.animationRoutine(delta, true);
			} else {

				animation_time += delta;
				skeleton.setX(getX() + getWidth() * 0.5f + shift_x);
				skeleton.setY(getY() + getHeight() * 0.5f + shift_y);

				animation.apply(skeleton, animation_time, animation_time, loop, null, 1, blend, direction);

				skeleton.updateWorldTransform();
			}
		super.act(delta);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {

		if (isVisible() == true) {
			if (client_model != null) {
				BasicScreen.SKELETON_RENDERER.draw((ShaderPolyBatch) batch, client_model);
			} else {
				skeleton.getColor().a = parentAlpha;
				BasicScreen.SKELETON_RENDERER.draw(batch, skeleton);
			}
			super.draw(batch, parentAlpha);

			batch.flush();
		}
	}

	public DrawableClient getClientModel() {
		return client_model;
	}

	/**
	 * 
	 * @param animationTime
	 */
	public void setAnimationTime(float animationTime) {
		animation_time = animationTime;
	}

	public CustomSkeleton getSkeleton() {
		return skeleton;
	}

	/**
	 * 
	 * @param blend
	 * @param direction
	 */
	public void setMixBehaviour(MixBlend blend, MixDirection direction) {
		this.blend = blend;
		this.direction = direction;
	}

	public void setAnimation(String animationString) {
		animation = skeleton.getData().findAnimation(animationString);
	}

	/**
	 * horizontal shift where the origin is in the center
	 * 
	 * @param shift
	 */
	public void setShiftX(float shift) {
		shift_x = shift;
	}

	/**
	 * vertical shift where the origin is in the center
	 * 
	 * @param shift
	 */
	public void setShiftY(float shift) {
		shift_y = shift;
	}

	public String getCurrentAnimation() {
		if (animation != null)
			return animation.getName();
		return "";
	}

}
