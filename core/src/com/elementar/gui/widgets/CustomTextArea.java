package com.elementar.gui.widgets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StyleContainer;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.util.SharedColors;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.WidgetGroupable;

/**
 * This widget is used to display information for the user and does not receive
 * input.
 * 
 * @author lukassongajlo
 * 
 */
public class CustomTextArea extends TextArea implements StatePossessable, WidgetGroupable {

	protected static final float PADDING_HORIZONTAL = 33;

	protected Sprite background;

	protected CustomTextbutton bounding_button;

	/**
	 * Creates a new customized text area. The dimensions depend on the text length
	 * where the width is constant and height flexible.
	 * 
	 * @param text
	 * @param style
	 */
	public CustomTextArea(String text, TextFieldStyle style) {
		super(text, style);
	}

	/**
	 * Creates a new customize text area with a background sprite. The width and
	 * height of the given sprite defines the dimensions of the area. For tooltips
	 * where the dimensions are depending on text use
	 * {@link #CustomTextArea(String)}
	 * 
	 * @param text
	 * @param background
	 */
	public CustomTextArea(String text, Sprite background) {
		super(text, StyleContainer.textfield_bold_18_style);
		this.background = background;
		setDisabled(true);
		bounding_button = new CustomTextbutton("", StyleContainer.button_bold_30_style, background.getWidth(),
				background.getHeight(), Align.center);
	}

	public WidgetGroup getGroup() {

		WidgetGroup group = new WidgetGroup();

		group.addActor(bounding_button);

		Table table = new Table();
		table.setPosition(group.getX() + bounding_button.getWidth() * 0.5f,
				group.getY() + bounding_button.getHeight() * 0.5f);
		table.add(this).width(bounding_button.getWidth() - 2 * PADDING_HORIZONTAL).height(bounding_button.getHeight());
		// 2
		// *
		// PADDING);

		group.addActor(table);

		return group;
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {

		ShaderPolyBatch shaderBatch = (ShaderPolyBatch) batch;
		shaderBatch.reset();
		if (background != null) {
			background.setPosition(bounding_button.getX(), bounding_button.getY());
			background.draw(shaderBatch);
		}
		getStyle().fontColor = SharedColors.IVORY_2;
		super.draw(shaderBatch, parentAlpha);
	}

	public Actor getBoundingButton() {
		return bounding_button;
	}

	@Override
	public State getState() {
		return bounding_button.getState();
	}

	@Override
	public void setState(State state) {
		bounding_button.setState(state);
	}

	@Override
	public void setHovered(boolean hovered) {
	}

	@Override
	public boolean isHovered() {
		return false;
	}

	@Override
	public void setVisibilityWidgets(boolean visible) {
		bounding_button.setVisible(visible);
		setVisible(visible);
	}

}
