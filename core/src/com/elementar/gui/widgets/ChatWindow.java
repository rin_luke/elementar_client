package com.elementar.gui.widgets;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.gui.util.CursorUICheckListener;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.Unfocusable;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.AlphaTransition;
import com.elementar.logic.characters.AlphaTransitionAccessor;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.Team;
import com.elementar.logic.util.lists.NotNullArrayList;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

public class ChatWindow implements Unfocusable {

	private final int MAX_ENTRIES = 20;
	private final int MAX_TEXTLENGTH = 160;
	private final int NUMBER_ROWS = 5;

	private CustomTextfield text_input_field;
	private CustomIconbutton bounding_button_history, bounding_button_input;
	private CustomLabel pre_text_label;

	private ScrollPane scrollpane;
	private VerticalGroup chat_entries;

	private ArrayList<ChatTextLabel> text_labels = new ArrayList<>();

	private boolean active;
	private boolean in_world;

	private String scope_prefix = "";

	private TweenManager tween_manager = new TweenManager();

	private CursorUICheckListener scrollpane_listener = new CursorUICheckListener();

	/**
	 * The chat window is separated by 2 section, a display section where you can
	 * see all inputs done by other players, yourself or/and the KI, and the input
	 * section where you have the opportunity to type something in By default
	 * {@link #active} is <code>true</code> and {@link #to_allies} is
	 * <code>false</code>
	 * 
	 * @param bgInput, background for input field section
	 * @param inWorld, if <code>false</code> this chat window is used inside the
	 *                 lobby
	 */
	public ChatWindow(Sprite bgInput, boolean inWorld) {

		in_world = inWorld;
		chat_entries = new VerticalGroup();
		scrollpane = new ScrollPane(chat_entries);
		scrollpane.setFadeScrollBars(true);
		scrollpane.setOverscroll(false, true);
		scrollpane.setFillParent(true);

		scrollpane.addListener(new InputListener() {
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				if (scrollpane.getStage() == null)
					return;

				scrollpane.getStage().setScrollFocus(scrollpane);
			}

			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				if (scrollpane.getStage() == null)
					return;

				scrollpane.getStage().setScrollFocus(null);
			}
		});

		pre_text_label = new CustomLabel("", StyleContainer.label_bold_whitepure_18_style, 120, Shared.HEIGHT1,
				Align.center);

		float textInputWidth = in_world == false ? bgInput.getWidth() : bgInput.getWidth() - pre_text_label.getWidth();
		text_input_field = new CustomTextfield(MAX_TEXTLENGTH, "", textInputWidth, Shared.HEIGHT1);
		text_input_field.setFocusTraversal(false);

		/*
		 * cursor ui check listener has to be added to both, input field and its
		 * bounding bounding button
		 */
		text_input_field.addListener(new CursorUICheckListener());
		text_input_field.getBoundingButton().addListener(new CursorUICheckListener());

		bounding_button_input = new CustomIconbutton(null, bgInput);

		bounding_button_history = new CustomIconbutton(null, Shared.WIDTH7, Shared.HEIGHT1 * NUMBER_ROWS);
		bounding_button_history.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				bounding_button_history.getStage().setKeyboardFocus(text_input_field);
				return false;
			}

			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				if (scrollpane.getStage() == null)
					return;
				scrollpane.getStage().setScrollFocus(scrollpane);
			}

			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				if (scrollpane.getStage() == null)
					return;
				scrollpane.getStage().setScrollFocus(null);
			}
		});

		// add empty lines, so the first line starts at the bottom
		for (int i = 0; i < NUMBER_ROWS; i++)
			addEmptyLine();

		setActiveAndScope(true, false);

		NotNullArrayList<StatePossessable> widgets = new NotNullArrayList<StatePossessable>();
		widgets.add(text_input_field);

		new HoverHandler(widgets);

	}

	public void updateAlpha(float delta) {

		if (active == false) {

			boolean allLinesDisappeared = true;
			for (ChatTextLabel textLabel : text_labels) {

				textLabel.increaseTime(delta);

				if (textLabel.hasExpired() == true)
					if (textLabel.isTweening() == false) {

						Tween.to(textLabel.getAlphaTransition(), AlphaTransitionAccessor.FADE_OUT, 2f).target(0f)
								.ease(TweenEquations.easeInOutSine).start(tween_manager);

						textLabel.setTweening();
					}

				textLabel.setAlpha(textLabel.getAlphaTransition().getValue());

				if (MathUtils.isZero(textLabel.getAlphaTransition().getValue()) == false)
					allLinesDisappeared = false;

			}

			if (allLinesDisappeared == true) {
				if (scrollpane.getListeners().contains(scrollpane_listener, true) == true) {
					scrollpane_listener.exit(null, 0, 0, -1, null);
					scrollpane.removeListener(scrollpane_listener);
				}
			}

		}

		tween_manager.update(delta);
	}

	public void updateMessage(OmniClient client) {

		addLine(client.getMetaClient().name, client.getMetaClient().chat_message, client.getMetaClient().team);

		client.getMetaClient().chat_message = "";
	}

	/**
	 * 
	 * @param active   if <code>true</code> the keyboard focus is on the input
	 *                 field, the background image of the scroll pane is drawn. In
	 *                 this mode the text is always shown with full transparency. If
	 *                 <code>false</code> new text will disappear after a while, the
	 *                 input field and background image of the scroll pane aren't
	 *                 visible.
	 * @param toAllies if <code>true</code> the chat window will add an "ally"
	 *                 prefix before the name of the chat member is written.
	 * 
	 */
	public void setActiveAndScope(boolean active, boolean toAllies) {
		this.active = active;

		// set visibility
		scrollpane.setVisible(true);
		scrollpane.scrollTo(0, 0, 0, 0);
		if (active == true)
			for (ChatTextLabel textLabel : text_labels)
				textLabel.setAlpha(1f);

		bounding_button_input.setVisible(active);
		bounding_button_history.setVisible(active);
		pre_text_label.setVisible(active);
		text_input_field.setVisibilityWidgets(active);

		// set keyboard focus
		if (text_input_field.getStage() != null)
			if (active == false) {
				text_input_field.getStage().setKeyboardFocus(null);
			} else {

				if (toAllies == true) {
					scope_prefix = "[ally]";
					pre_text_label.setText(TextData.getWord("toAllies"));
				} else {
					scope_prefix = "";
					pre_text_label.setText(TextData.getWord("toAll"));
				}

				text_input_field.getStage().setKeyboardFocus(text_input_field);
				text_input_field.setCursorPosition(text_input_field.getText().length());

				if (scrollpane.getListeners().contains(scrollpane_listener, true) == false)
					scrollpane.addListener(scrollpane_listener);

			}
	}

	public boolean isActive() {
		return active;
	}

	public WidgetGroup getGroup() {
		WidgetGroup chatGroup = new WidgetGroup();
		chatGroup.addActor(bounding_button_history);
		chatGroup.addActor(scrollpane);

		CustomTable table = new CustomTable();
		table.add(chatGroup).width(bounding_button_history.getWidth()).height(bounding_button_history.getHeight())
				.colspan(3);
		table.row();
		table.add(bounding_button_input).width(0);
		if (in_world == true)
			table.add(pre_text_label);// .width(bounding_button_history.getWidth()
										// *
										// 0.2f);
		table.add(text_input_field);// .width(bounding_button_history.getWidth()
									// * 0.8f);

		WidgetGroup group = new WidgetGroup();
		group.addActor(table);
		return group;
	}

	public CustomTextfield getTextField() {
		return text_input_field;
	}

	@Override
	public void unfocusWidget() {

		if (in_world == false) {
			/*
			 * the widget gets unfocused after clicking elsewhere, unless it isn't the
			 * history bounding box.
			 */
			if (bounding_button_history.isHovered() == false)
				text_input_field.unfocusWidget();
		} else {
			setActiveAndScope(false, true);
		}
	}

	private void addEmptyLine() {
		CustomTable container = new CustomTable();

		ChatTextLabel label = new ChatTextLabel();

		container.add(label).width(bounding_button_history.getWidth()).height(Shared.HEIGHT1);

		chat_entries.addActor(container);

		boolean atBottom = scrollpane.isBottomEdge();

		scrollpane.layout();

		if (atBottom == true)
			scrollpane.scrollTo(0, 0, 0, 0);

	}

	/**
	 * 
	 * @param name
	 * @param message
	 * @param teamAuthor
	 */
	private void addLine(String name, String message, Team teamAuthor) {

		if (message.isEmpty() == true)
			return;

		CustomTable container = new CustomTable();

		String namePrefix = "";

		if (message.startsWith("[ally]") == true) {
			if (LogicTier.VIEW_HANDLER.getTeam() != teamAuthor)
				return;
			// in 'message' is still the ally-info before the message. We have to remove it.
			message = message.replace("[ally]", "");
			namePrefix = "[" + TextData.getWord("ally") + "] ";
		}

		ChatTextLabel label = new ChatTextLabel("");
		if (name.equals("") == true)
			label.setText(message);
		else
			label.setText(namePrefix + "[GREEN]" + name + "[]: " + message);

		/*
		 * the calculated height of the whole entry depends on the number of lines of
		 * the text. The number of lines depends on the width we determined inside the
		 * constructor.
		 */
		label.setWrap(true);
		label.pack();

		float padLeft = 20, padTopBot = 16;
		label.background_sprite.setSize(label.background_sprite.getWidth(),
				label.getGlyphLayout().height + padTopBot * 2);

		container.add(label).width(bounding_button_history.getWidth() - padLeft).padLeft(padLeft)
				.height(label.getGlyphLayout().height).padTop(padTopBot).padBottom(padTopBot);
		chat_entries.addActor(container);
		text_labels.add(label);

		if (chat_entries.getChildren().size > MAX_ENTRIES) {
			chat_entries.getChildren().get(0).remove();
			text_labels.remove(0);
		}
		boolean atBottom = scrollpane.isBottomEdge();

		scrollpane.layout();

		if (atBottom == true)
			scrollpane.scrollTo(0, 0, 0, 0);
	}

	public boolean hasKeyboardFocus() {
		if (text_input_field.getStage() != null)
			return text_input_field.getStage().getKeyboardFocus() == text_input_field;
		return false;
	}

	public String getScopePrefix() {
		return scope_prefix;
	}

	private class ChatTextLabel extends CustomLabel {

		private final static float MAX_ACTIVE_TIME = 5f;
		/**
		 * this time variable gets increased while the chat window is inactive. After
		 * reaching a certain threshold the label shouldn't be displayed anymore while
		 * the chat window is inactive
		 */
		private float elapsed_time;

		private boolean tweening = false;
		private AlphaTransition alpha_transition = new AlphaTransition();

		public ChatTextLabel(CharSequence text) {
			super(text, StyleContainer.label_bold_whitepure_18_style, StaticGraphic.gui_bg_chat_history, Align.left);
		}

		/**
		 * Empty text entry
		 */
		public ChatTextLabel() {
			super("", StyleContainer.label_bold_whitepure_18_style, StaticGraphic.gui_bg_chat_history.getWidth(),
					StaticGraphic.gui_bg_chat_history.getHeight(), Align.left);
		}

		public void increaseTime(float delta) {
			elapsed_time += delta;
		}

		public boolean hasExpired() {
			return elapsed_time > MAX_ACTIVE_TIME;
		}

		public AlphaTransition getAlphaTransition() {
			return alpha_transition;
		}

		/**
		 * Once the alpha tweening should start, this method is called to ensure that
		 * the tweening procedure will only be applied once.
		 */
		public void setTweening() {
			tweening = true;
		}

		public boolean isTweening() {
			return tweening;
		}

		@Override
		public void setAlpha(float alpha) {
			super.setAlpha(alpha);
			background_sprite.setAlpha(alpha);

		}

	}

}
