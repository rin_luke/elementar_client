package com.elementar.gui.widgets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.OptionsData;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.GUITier;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.Unfocusable;

/**
 * Holds three {@link CustomDropdown} widgets to select resolution out of three
 * aspect ratios (4:3, 16:9, 16:10). Provides methods to get and set the current
 * visible dropdown menu. If one dropdown menu is visible, the others have to be
 * invisible.
 * 
 * @author lukassongajlo
 *
 */
public class ResolutionDropdown implements Unfocusable {

	// for window mode
	private CustomDropdown resolution_4_to_3_window_dropdown, resolution_16_to_9_window_dropdown,
			resolution_16_to_10_window_dropdown;

	// for fullscreen mode
	private CustomDropdown resolution_4_to_3_fullscreen_dropdown,
			resolution_16_to_9_fullscreen_dropdown, resolution_16_to_10_fullscreen_dropdown;

	private CustomDropdown current_dropdown;

	private String resolution;

	public ResolutionDropdown() {

		// window mode
		ArrayList<String> four_to_3_ratios = new ArrayList<>();
		ArrayList<String> sixteen_to_9_ratios = new ArrayList<>();
		ArrayList<String> sixteen_to_10_ratios = new ArrayList<>();

		four_to_3_ratios.add("1600x1200");
		four_to_3_ratios.add("1400x1050");
		four_to_3_ratios.add("1280x1024");
		four_to_3_ratios.add("1280x960");
		four_to_3_ratios.add("1152x864");
		four_to_3_ratios.add("1024x768");

		sixteen_to_9_ratios.add("2560x1440");
		sixteen_to_9_ratios.add("1920x1080");
		sixteen_to_9_ratios.add("1768x992");
		sixteen_to_9_ratios.add("1600x900");
		sixteen_to_9_ratios.add("1366x768");
		sixteen_to_9_ratios.add("1360x768");
		sixteen_to_9_ratios.add("1280x720");

		sixteen_to_10_ratios.add("1920x1200");
		sixteen_to_10_ratios.add("1680x1050");
		sixteen_to_10_ratios.add("1600x1024");
		sixteen_to_10_ratios.add("1440x960");
		sixteen_to_10_ratios.add("1440x900");
		sixteen_to_10_ratios.add("1280x768");
		sixteen_to_10_ratios.add("1280x800");

		resolution_4_to_3_window_dropdown = createDropdown(filterResolutions(four_to_3_ratios));
		resolution_16_to_9_window_dropdown = createDropdown(filterResolutions(sixteen_to_9_ratios));
		resolution_16_to_10_window_dropdown
				= createDropdown(filterResolutions(sixteen_to_10_ratios));

		// clear for new entries for fullscreen mode
		four_to_3_ratios = new ArrayList<>();
		sixteen_to_9_ratios = new ArrayList<>();
		sixteen_to_10_ratios = new ArrayList<>();

		for (DisplayMode mode : Gdx.graphics.getDisplayModes()) {
			String aspectRatioString = getAspectRatioString(((float) mode.width) / mode.height);
			String resolution = mode.width + "x" + mode.height;
			ArrayList<String> resoContainer = null;
			if (aspectRatioString.equals("4:3") == true)
				resoContainer = four_to_3_ratios;
			else if (aspectRatioString.equals("16:9") == true)
				resoContainer = sixteen_to_9_ratios;
			else if (aspectRatioString.equals("16:10") == true)
				resoContainer = sixteen_to_10_ratios;

			if (resoContainer != null && resoContainer.contains(resolution) == false)
				resoContainer.add(resolution);
		}

		ResolutionComparator comparator = new ResolutionComparator();

		four_to_3_ratios = filterResolutions(four_to_3_ratios);
		if (four_to_3_ratios.isEmpty() == true)
			four_to_3_ratios.add("noSupportedResolutions");
		Collections.sort(four_to_3_ratios, comparator);
		resolution_4_to_3_fullscreen_dropdown = createDropdown(four_to_3_ratios);

		sixteen_to_9_ratios = filterResolutions(sixteen_to_9_ratios);
		if (sixteen_to_9_ratios.isEmpty() == true)
			sixteen_to_9_ratios.add("noSupportedResolutions");
		Collections.sort(sixteen_to_9_ratios, comparator);
		resolution_16_to_9_fullscreen_dropdown = createDropdown(sixteen_to_9_ratios);

		sixteen_to_10_ratios = filterResolutions(sixteen_to_10_ratios);
		if (sixteen_to_10_ratios.isEmpty() == true)
			sixteen_to_10_ratios.add("noSupportedResolutions");
		Collections.sort(sixteen_to_10_ratios, comparator);
		resolution_16_to_10_fullscreen_dropdown = createDropdown(sixteen_to_10_ratios);

		setCurrent(OptionsData.aspect_ratio, OptionsData.fullscreen_on);
	}

	public static String getAspectRatioString(float ratio) {
		if (ratio > 1.3f && ratio < 1.4f)
			return "4:3";
		else if (ratio > 1.7f && ratio < 1.8f)
			return "16:9";
		else if (ratio > 1.57f && ratio < 1.61f)
			return "16:10";
		return "16:9";
	}

	private ArrayList<String> filterResolutions(ArrayList<String> inputResoList) {
		int myScreenWidth = GUITier.SCREEN_WIDTH;
		int myScreenHeight = GUITier.SCREEN_HEIGHT;
		int resoWidth;
		int resoHeight;
		Iterator<String> itItems;
		ArrayList<String> resultList = new ArrayList<>();
		itItems = inputResoList.iterator();
		Integer[] resolution;

		while (itItems.hasNext() == true) {
			String item = itItems.next();
			resolution = parseResolution(item);

			if (resolution != null) {
				resoWidth = resolution[0];
				resoHeight = resolution[1];

				if ((myScreenWidth < resoWidth || myScreenHeight < resoHeight) == false)
					resultList.add(item);
			}
		}

		return resultList;

	}

	private CustomDropdown createDropdown(ArrayList<String> resolutionList) {

		CustomDropdown dropdown
				= new CustomDropdown(resolutionList, StaticGraphic.gui_bg_btn_height1_width5,
						StaticGraphic.gui_bg_dropdown_height1_width3_closed,
						StaticGraphic.gui_bg_dropdown_height1_width3_open, Align.center) {
					@Override
					public void clickedItem() {
						// change resolution
						resolution = getSelectedText();
					}
				};

		dropdown.setSound(ClickSoundType.LIGHT);

		return dropdown;
	}

	public void setCurrent(String aspectRatio, boolean fullScreen) {
		switch (aspectRatio) {
		case "4:3":
			if (fullScreen == true) {
				current_dropdown = resolution_4_to_3_fullscreen_dropdown;
				resolution_4_to_3_fullscreen_dropdown.setVisibilityWidgets(true);
				resolution_4_to_3_window_dropdown.setVisibilityWidgets(false);
			} else {
				current_dropdown = resolution_4_to_3_window_dropdown;
				resolution_4_to_3_fullscreen_dropdown.setVisibilityWidgets(false);
				resolution_4_to_3_window_dropdown.setVisibilityWidgets(true);
			}
			resolution_16_to_9_fullscreen_dropdown.setVisibilityWidgets(false);
			resolution_16_to_10_fullscreen_dropdown.setVisibilityWidgets(false);
			resolution_16_to_9_window_dropdown.setVisibilityWidgets(false);
			resolution_16_to_10_window_dropdown.setVisibilityWidgets(false);

			break;
		case "16:9":
			resolution_4_to_3_fullscreen_dropdown.setVisibilityWidgets(false);
			resolution_4_to_3_window_dropdown.setVisibilityWidgets(false);

			if (fullScreen == true) {
				current_dropdown = resolution_16_to_9_fullscreen_dropdown;
				resolution_16_to_9_fullscreen_dropdown.setVisibilityWidgets(true);
				resolution_16_to_9_window_dropdown.setVisibilityWidgets(false);
			} else {
				current_dropdown = resolution_16_to_9_window_dropdown;
				resolution_16_to_9_fullscreen_dropdown.setVisibilityWidgets(false);
				resolution_16_to_9_window_dropdown.setVisibilityWidgets(true);
			}
			resolution_16_to_10_fullscreen_dropdown.setVisibilityWidgets(false);
			resolution_16_to_10_window_dropdown.setVisibilityWidgets(false);

			break;
		case "16:10":
			resolution_4_to_3_fullscreen_dropdown.setVisibilityWidgets(false);
			resolution_4_to_3_window_dropdown.setVisibilityWidgets(false);
			resolution_16_to_9_fullscreen_dropdown.setVisibilityWidgets(false);
			resolution_16_to_9_window_dropdown.setVisibilityWidgets(false);
			if (fullScreen == true) {
				current_dropdown = resolution_16_to_10_fullscreen_dropdown;
				resolution_16_to_10_fullscreen_dropdown.setVisibilityWidgets(true);
				resolution_16_to_10_window_dropdown.setVisibilityWidgets(false);
			} else {
				current_dropdown = resolution_16_to_10_window_dropdown;
				resolution_16_to_10_fullscreen_dropdown.setVisibilityWidgets(false);
				resolution_16_to_10_window_dropdown.setVisibilityWidgets(true);
			}
			break;
		}

		resolution = current_dropdown.getSelectedText();

	}

	/**
	 * Returns an {@link Integer} array with two entries - width and height.
	 * Might be <code>null</code> if parsing is not possible.
	 * 
	 * @param resolution
	 * @return
	 */
	public static Integer[] parseResolution(String resolution) {
		Integer result[] = null;

		String[] string = resolution.split("x");

		if (string.length == 2) {
			result = new Integer[2];
			result[0] = Integer.valueOf(string[0]);
			result[1] = Integer.valueOf(string[1]);
		}

		return result;
	}

	public CustomDropdown getCurrent() {
		return current_dropdown;
	}

	/**
	 * layout method
	 * 
	 * @param tables
	 * @param table
	 * @param cell
	 * @param offset
	 */
	public void addItems(ArrayList<CustomTable> tables, CustomTable table,
			Cell<CustomDropdown> cell) {
		/*
		 * Usually when a dropdown menu is added to a table, the cell will be
		 * automatically be set by the CustomTable class. In this case not every
		 * dropdown menu was added previously to the table, so the cell field of
		 * them is null - we have to set it manually
		 */
		resolution_4_to_3_window_dropdown.setCell(cell);
		resolution_16_to_9_window_dropdown.setCell(cell);
		resolution_16_to_10_window_dropdown.setCell(cell);

		resolution_4_to_3_fullscreen_dropdown.setCell(cell);
		resolution_16_to_9_fullscreen_dropdown.setCell(cell);
		resolution_16_to_10_fullscreen_dropdown.setCell(cell);

		cell.setActor(resolution_4_to_3_window_dropdown);
		resolution_4_to_3_window_dropdown.setTable(tables, table);
		cell.setActor(resolution_16_to_9_window_dropdown);
		resolution_16_to_9_window_dropdown.setTable(tables, table);
		cell.setActor(resolution_16_to_10_window_dropdown);
		resolution_16_to_10_window_dropdown.setTable(tables, table);

		cell.setActor(resolution_4_to_3_fullscreen_dropdown);
		resolution_4_to_3_fullscreen_dropdown.setTable(tables, table);
		cell.setActor(resolution_16_to_9_fullscreen_dropdown);
		resolution_16_to_9_fullscreen_dropdown.setTable(tables, table);
		cell.setActor(resolution_16_to_10_fullscreen_dropdown);
		resolution_16_to_10_fullscreen_dropdown.setTable(tables, table);

		cell.setActor(getCurrent());

	}

	/**
	 * For {@link HoverHandler}
	 * 
	 * @param widgets
	 */
	public void addToHoverhandler(ArrayList<StatePossessable> widgets) {
		widgets.add(resolution_4_to_3_window_dropdown);
		widgets.addAll(resolution_4_to_3_window_dropdown.getItems());
		widgets.add(resolution_16_to_9_window_dropdown);
		widgets.addAll(resolution_16_to_9_window_dropdown.getItems());
		widgets.add(resolution_16_to_10_window_dropdown);
		widgets.addAll(resolution_16_to_10_window_dropdown.getItems());

		widgets.add(resolution_4_to_3_fullscreen_dropdown);
		widgets.addAll(resolution_4_to_3_fullscreen_dropdown.getItems());
		widgets.add(resolution_16_to_9_fullscreen_dropdown);
		widgets.addAll(resolution_16_to_9_fullscreen_dropdown.getItems());
		widgets.add(resolution_16_to_10_fullscreen_dropdown);
		widgets.addAll(resolution_16_to_10_fullscreen_dropdown.getItems());
	}

	@Override
	public void unfocusWidget() {
		resolution_4_to_3_window_dropdown.unfocusWidget();
		resolution_16_to_9_window_dropdown.unfocusWidget();
		resolution_16_to_10_window_dropdown.unfocusWidget();

		resolution_4_to_3_fullscreen_dropdown.unfocusWidget();
		resolution_16_to_9_fullscreen_dropdown.unfocusWidget();
		resolution_16_to_10_fullscreen_dropdown.unfocusWidget();

	}

	public String getResolution() {
		return resolution;
	}

	private class ResolutionComparator implements java.util.Comparator<String> {

		@Override
		public int compare(String o1, String o2) {

			Integer[] resolution1 = parseResolution(o1);

			if (resolution1 != null) {
				int resoWidth1 = resolution1[0];
				int resoHeight1 = resolution1[1];

				Integer[] resolution2 = parseResolution(o2);

				if (resolution2 != null) {
					int resoWidth2 = resolution2[0];
					int resoHeight2 = resolution2[1];

					if (resoWidth1 < resoWidth2)
						return 1;
					if (resoWidth1 > resoWidth2)
						return -1;
					if (resoHeight1 < resoHeight2)
						return 1;
					if (resoHeight1 > resoHeight2)
						return -1;
					return 0;

				}

			}
			return 0;
		}
	}

}
