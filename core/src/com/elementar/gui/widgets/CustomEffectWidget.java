package com.elementar.gui.widgets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.elementar.data.container.util.CustomParticleEffect;

public class CustomEffectWidget extends Actor {

	private CustomParticleEffect effect;
	protected float shift_y;

	/**
	 * Note if the effect is used multiple times, wrap the effect with "new
	 * CustomParticleEffect(effect)" instead of passing the raw effect parameter
	 * 
	 * @param effect
	 */
	public CustomEffectWidget(CustomParticleEffect effect) {
		this.effect = effect;
	}

	@Override
	public void act(float delta) {
		effect.setPosition(getX() + getWidth() * 0.5f, getY() + shift_y + getHeight() * 0.5f);
		super.act(delta);
	}

	/**
	 * vertical shift where the origin is in the center
	 * 
	 * @param shift
	 */
	public void setShiftY(float shift) {
		shift_y = shift;
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (isVisible() == true) {
			effect.draw(batch, Gdx.graphics.getDeltaTime());
			batch.flush();
		}

	}

	public CustomParticleEffect getParticleEffect() {
		return effect;
	}

}
