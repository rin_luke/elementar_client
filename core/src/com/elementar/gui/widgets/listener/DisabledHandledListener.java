package com.elementar.gui.widgets.listener;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.StatePossessable.State;

/**
 * This is a clicklistener and extends the normal {@link ClickListener} of
 * libgdx. It checks whether the actor is disabled or not. If not you have the
 * chance to define an certain behavior in {@link #afterDisabledCheck()}.
 * 
 * 
 * @author lukassongajlo
 * 
 */
public abstract class DisabledHandledListener extends ClickListener {

	private StatePossessable widget;

	/**
	 * 
	 * @param widget
	 */
	public DisabledHandledListener(StatePossessable widget) {
		this.widget = widget;
	}

	@Override
	public void clicked(InputEvent event, float x, float y) {
		if (widget.getState() == State.DISABLED)
			return;
		afterDisabledCheck();
	}

	/**
	 * define in this method what should be happen after a click and when
	 * actor's state isn't DISABLED
	 */
	public abstract void afterDisabledCheck();
}
