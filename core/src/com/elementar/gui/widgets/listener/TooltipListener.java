package com.elementar.gui.widgets.listener;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.elementar.gui.abstracts.BasicScreen;
import com.elementar.gui.widgets.TooltipTextArea;

/**
 * Add this listener to a widget which should have a tooltip
 * 
 * @author lukassongajlo
 * 
 */
public class TooltipListener extends InputListener {

	private TooltipTextArea tooltip;

	/**
	 * 
	 * @param tooltip
	 */
	public TooltipListener(TooltipTextArea tooltip) {
		this.tooltip = tooltip;
	}

	@Override
	public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {

		if (tooltip.getDisplayDelay() > 0) {

			BasicScreen.TOOLTIP_DELAYED = tooltip;
			tooltip.setElapsedTime(0);
			return;
		}
		tooltip.setVisible(true);

	}

	@Override
	public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {

		if (pointer != -1)
			return;

		tooltip.setVisible(false);
		BasicScreen.TOOLTIP_DELAYED = null;
	}
}