package com.elementar.gui.widgets.listener;

import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * This class was created to distinguish manual clicks and clicks executed by
 * the code
 * 
 * @author lukassongajlo
 *
 */
public class SoundClickListener extends ClickListener {
}
