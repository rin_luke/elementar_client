package com.elementar.gui.widgets;

public interface IDisplayTranslator {

	public void translate(float x, float y);

}
