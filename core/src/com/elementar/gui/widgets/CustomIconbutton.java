package com.elementar.gui.widgets;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.logic.util.Shared;

/**
 * Can be used as icon button with an up- and down-image, or just as an icon
 * image without button functionality.
 * 
 * @author lukassongajlo
 *
 */
public class CustomIconbutton extends CustomTextbutton {

	protected Sprite current_icon, icon_up, icon_down;

	protected float shift_x, shift_y;

	/**
	 * With background and without iconDown-image
	 * 
	 * @param icon
	 * @param background
	 */
	public CustomIconbutton(Sprite icon, Sprite background) {
		this(icon, null, background);

	}

	/**
	 * No background-image
	 * 
	 * @param icon
	 * @param width
	 * @param height
	 */
	public CustomIconbutton(Sprite icon, float width, float height) {
		super("", StyleContainer.button_bold_30_style, width, height, Align.center);
		this.current_icon = icon;

	}

	/**
	 * 
	 * @param text
	 * @param style
	 * @param align
	 * @param icon
	 * @param width
	 * @param height
	 */
	public CustomIconbutton(String text, TextButtonStyle style, int align, Sprite icon, float width,
			float height) {
		super(text, style, width, height, align);
		this.current_icon = icon;

	}

	/**
	 * full icon_button without background (e.g. radio buttons) /**
	 * 
	 * @param iconUp
	 * @param iconDown
	 * @param width
	 * @param height
	 */
	public CustomIconbutton(final Sprite iconUp, final Sprite iconDown, float width, float height) {
		super("", StyleContainer.button_bold_30_style, width, height, Align.center);

		defineFullButton(iconUp, iconDown);

	}

	/**
	 * full icon_button
	 * 
	 * @param iconUp
	 * @param iconDown
	 * @param background
	 */
	public CustomIconbutton(final Sprite iconUp, final Sprite iconDown, Sprite background) {
		super("", StyleContainer.button_bold_30_style, background, Align.center);

		defineFullButton(iconUp, iconDown);

	}

	/**
	 * Help method to cover both ctors for full buttons
	 * 
	 * @param iconUp
	 * @param iconDown
	 */
	protected void defineFullButton(Sprite iconUp, Sprite iconDown) {
		current_icon = iconUp;

		icon_up = iconUp;
		icon_down = iconDown;

		if (iconDown != null)
			addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					toggleIconSprites();
				}
			});
	}

	/**
	 * Toggles icon sprites
	 */
	public void toggleIconSprites() {
		if (current_icon == icon_up)
			current_icon = icon_down;
		else
			current_icon = icon_up;
	}

	public void setIcon(Sprite currentIcon) {
		current_icon = currentIcon;
	}

	/**
	 * horizontal shift where the origin is in the center
	 * 
	 * @param shift
	 */
	public void setIconShiftX(float shift) {
		shift_x = shift;
	}

	/**
	 * vertical shift where the origin is in the center
	 * 
	 * @param shift
	 */
	public void setIconShiftY(float shift) {
		shift_y = shift;
	}

	public void setIconShiftXY(float ShiftX, float shiftY) {
		shift_x = ShiftX;
		shift_y = shiftY;
	}

	public void setTopAlign() {
		shift_y = getHeight() * 0.5f - current_icon.getBoundingRectangle().height * 0.5f;
	}

	public void setBottomAlign() {
		shift_y = -getHeight() * 0.5f + current_icon.getBoundingRectangle().height * 0.5f;
	}

	public void setRightAlign() {
		shift_x = getWidth() * 0.5f - current_icon.getBoundingRectangle().width * 0.5f;
	}

	public void setLeftAlign() {
		shift_x = -getWidth() * 0.5f + current_icon.getBoundingRectangle().width * 0.5f;
	}

	@Override
	protected void drawAdditionalSprites(ShaderPolyBatch batch) {
		if (current_icon != null) {
			/**
			 * setPosition(...,...) has to be called continuously because icon
			 * could be changed
			 */
			if (state == State.DISABLED)
				current_icon.setAlpha(Shared.WIDGET_DISABLED_ALPHA);
			current_icon.setPosition(
					getX() + getWidth() * 0.5f - current_icon.getWidth() * 0.5f + shift_x,
					getY() + getHeight() * 0.5f - current_icon.getHeight() * 0.5f + shift_y);
			current_icon.draw(batch);
			current_icon.setAlpha(1f);
			batch.flush();
			batch.reset();
		}
	}

	public Sprite getIcon() {
		return current_icon;
	}

	public void setIconToButtonSize() {
		if (current_icon != null)
			current_icon.setSize(getWidth(), getHeight());
	}

	/**
	 * NOTE: Only use this method for radio buttons
	 * 
	 * @return
	 */
	public boolean isClicked() {
		return current_icon == StaticGraphic.gui_icon_radiobutton_checked;
	}

	public float getShiftX() {
		return shift_x;
	}

	public float getShiftY() {
		return shift_y;
	}

}
