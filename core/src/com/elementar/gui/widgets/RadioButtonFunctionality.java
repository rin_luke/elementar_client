package com.elementar.gui.widgets;

import java.util.ArrayList;

/**
 * Has no layout functionality, just organize all added buttons to achieve
 * radio-button-functionality.
 * 
 * @author lukassongajlo
 *
 */
public class RadioButtonFunctionality {

	private ArrayList<CustomIconbutton> radio_buttons = new ArrayList<>();

	/**
	 * 
	 */
	public RadioButtonFunctionality() {
	}

	/**
	 * Adds a {@link CustomIconbutton} to the radio button functionality, so
	 * there can be only one checked button.
	 * 
	 * @param radioButton
	 */
	public void add(CustomIconbutton radioButton) {
		radio_buttons.add(radioButton);
	}

	/**
	 * Checks the button with the passed index and uncheck the other ones
	 * 
	 * @param index
	 */
	public void check(int index) {
		CustomIconbutton radioButton = null;
		for (int i = 0; i < radio_buttons.size(); i++) {
			radioButton = radio_buttons.get(i);
			if (radioButton != null) {
				if (index == i && radioButton.isClicked() == false)
					radio_buttons.get(i).toggleIconSprites();
				if (index != i && radioButton.isClicked() == true)
					radio_buttons.get(i).toggleIconSprites();
			}
		}
	}

	public ArrayList<CustomIconbutton> getButtons() {
		return radio_buttons;
	}

}
