package com.elementar.gui.widgets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StyleContainer;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.util.SharedColors;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.Unfocusable;
import com.elementar.gui.widgets.interfaces.WidgetGroupable;
import com.elementar.logic.util.Shared;

public class CustomTextfield extends TextField implements StatePossessable, Unfocusable, WidgetGroupable {

	private static final float BLINKTIME = 0.5f;

	private CustomTextbutton bounding_button;
	private Sprite background;

	// kommt nicht zurecht mit dem bounding button, zellen mäßig

	private String unfocused_text = "";

	/**
	 * no unfocused text
	 * 
	 * @param background
	 * @param maxTextlength
	 */
	public CustomTextfield(Sprite background, int maxTextlength) {
		this(background, maxTextlength, "");
	}

	/**
	 * with unfocused text
	 * 
	 * @param background
	 * @param maxTextlength
	 * @param unfocusedText
	 * @param stage
	 */
	public CustomTextfield(Sprite background, int maxTextlength, String unfocusedText) {
		this(maxTextlength, unfocusedText, background.getWidth(), background.getHeight());
		this.unfocused_text = unfocusedText;
		this.background = background;
	}

	/**
	 * no background
	 * 
	 * @param maxTextLength
	 * @param unfocusedText
	 * @param width
	 * @param height
	 */
	public CustomTextfield(int maxTextLength, String unfocusedText, float width, float height) {
		super(unfocusedText, StyleContainer.textfield_bold_18_style);

		bounding_button = new CustomTextbutton("", StyleContainer.button_bold_20_style, width, height, Align.center);
		setBlinkTime(BLINKTIME);
		setMaxLength(maxTextLength);

		addListener(new FocusListener() {
			@Override
			public void keyboardFocusChanged(FocusEvent event, Actor actor, boolean focused) {
				handleTextfieldFocus(focused);
			}
		});

		/*
		 * we need this additional listener for overlapping widgets. Consider the case
		 * hovering over the outer bounding button and then the textfield: the buttons
		 * isHovered() will return false although the cursor is still inside the buttons
		 * dimensions
		 */
		addListener(new ClickListener() {
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				super.enter(event, x, y, pointer, fromActor);
				bounding_button.setHovered(true);
			}

			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				super.exit(event, x, y, pointer, toActor);
				bounding_button.setHovered(false);
			}
		});

		bounding_button.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				getStage().setKeyboardFocus(CustomTextfield.this);
				return false;
			}
		});

	}

	@Override
	public void draw(Batch batch, float parentAlpha) {

		ShaderPolyBatch shaderBatch = (ShaderPolyBatch) batch;
		GUIUtil.determineHoverEffect(shaderBatch, getState());

		if (background != null) {
			if (getState() == State.DISABLED)
				background.setAlpha(Shared.WIDGET_DISABLED_ALPHA);

			background.setPosition(bounding_button.getX(), bounding_button.getY());

			background.draw(shaderBatch);
			background.setAlpha(1f);
		}
		if (getState() == State.HOVERED || getState() == State.SELECTED)
			getStyle().fontColor = SharedColors.IVORY_2;
		else if (getState() == State.DISABLED)
			getStyle().fontColor = SharedColors.IVORY_2_DISABLED;
		else
			getStyle().fontColor = SharedColors.IVORY_3;
		super.draw(shaderBatch, parentAlpha);
	}

	@Override
	public void unfocusWidget() {
		if (bounding_button.isHovered() == false) {
			handleTextfieldFocus(false);
			getStage().unfocus(this);
		}
	}

	private void handleTextfieldFocus(boolean focused) {
		String result = getText();
		if (focused == true) {
			setState(State.SELECTED);
			if (getText().equals(unfocused_text))
				result = "";
		} else {
			setState(State.NOT_SELECTED);
			if (getText().equals("") == true)
				result = unfocused_text;
		}
		setText(result);
	}

	public Actor getBoundingButton() {
		return bounding_button;
	}

	@Override
	public State getState() {
		return bounding_button.getState();
	}

	@Override
	public void setState(State state) {
		bounding_button.setState(state);
	}

	@Override
	public void setHovered(boolean hovered) {
		bounding_button.setHovered(hovered);
	}

	@Override
	public boolean isHovered() {
		return bounding_button.isHovered();
	}

	public WidgetGroup getGroup() {

		WidgetGroup group = new WidgetGroup();

		group.addActor(bounding_button);

		Table table = new Table();
		table.setPosition(group.getX() + bounding_button.getWidth() * 0.5f,
				group.getY() + bounding_button.getHeight() * 0.5f);
		table.add(this).width(bounding_button.getWidth() - 2 * Shared.PADDING_LEFT_HEIGHT1)
				.height(bounding_button.getHeight());
		group.addActor(table);

		return group;
	}

	@Override
	public void setVisibilityWidgets(boolean visible) {
		bounding_button.setVisible(visible);
		setVisible(visible);
	}

	@Override
	public String getText() {
		return (super.getText().equals(unfocused_text) == true ? "" : super.getText());
	}

	public String getUnfocusedText() {
		return unfocused_text;
	}

}
