package com.elementar.gui.widgets;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class SeedTextfield extends CustomTextfield {

	public static final int SEED_LENGTH = 5;

	public SeedTextfield(Sprite background, String unfocusedText) {
		super(background, SEED_LENGTH, unfocusedText);

		// removeListener(getDefaultInputListener());
		//
		// TextFieldClickListener customListener = new TextFieldClickListener()
		// {
		// public boolean keyTyped(com.badlogic.gdx.scenes.scene2d.InputEvent
		// event,
		// char character) {
		//
		// if (Character.isDigit(character) == true)
		// super.keyTyped(event, character);
		//
		// return true;
		//
		// };
		// };
		//
		// addListener(customListener);

	}

}
