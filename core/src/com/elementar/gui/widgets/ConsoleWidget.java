package com.elementar.gui.widgets;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.elementar.data.container.StyleContainer;
import com.elementar.gui.util.CursorUICheckListener;
import com.elementar.logic.network.gameserver.ConsoleEvent;
import com.elementar.logic.network.gameserver.GameserverLogic;

public class ConsoleWidget {

	private final int MAX_LINES = 300;

	private CustomTextArea text_area;
	private ScrollPane scrollpane;

	/**
	 * The chat window is separated by 2 section, a display section where you
	 * can see all inputs done by other players, yourself or/and the KI, and the
	 * input section where you have the opportunity to type something in By
	 * default {@link #active} is <code>true</code> and {@link #to_allies} is
	 * <code>false</code>
	 * 
	 * @param bgInput,
	 *            background for input field section
	 * @param inWorld,
	 *            if <code>false</code> this chat window is used inside the
	 *            lobby
	 */
	public ConsoleWidget() {

		text_area = new CustomTextArea("", StyleContainer.textfield_bold_18_style) {
			public float getPrefHeight() {
				return getLines() * getStyle().font.getLineHeight();
			}
		};

		text_area.setDisabled(true);

		scrollpane = new ScrollPane(text_area, StyleContainer.scrollpane_server_style);
		scrollpane.setFadeScrollBars(true);
		scrollpane.setOverscroll(false, true);
		scrollpane.setFillParent(false);
		/*
		 * the first listener is responsible for touchDown/drag events, which
		 * should be disabled
		 */
		scrollpane.removeListener(scrollpane.getListeners().get(0));

		scrollpane.addListener(new CursorUICheckListener());

		scrollpane.addListener(new InputListener() {
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				scrollpane.getStage().setScrollFocus(scrollpane);
			}
		});
		scrollpane.layout();

	}

	public void update(GameserverLogic gameserverLogic, String parsedTimeText) {

		for (ConsoleEvent event : gameserverLogic.getConsoleEvents())
			addLine(parsedTimeText, event.getText());

		// clear event list
		gameserverLogic.getConsoleEvents().clear();

	}

	public void addLine(String time, String message) {

		if (text_area.getLines() > MAX_LINES)
			text_area.setText(text_area.getText().substring(text_area.getText().indexOf("\n") + 1));

		text_area.appendText("[" + time + "]  " + message + "\n");

		// scrollpane.scrollTo(0, 0, 0, 0);
		// scrollpane.layout();

	}

	public void clear() {
		text_area.clear();
	}

	public ScrollPane getScrollPane() {
		return scrollpane;
	}

}
