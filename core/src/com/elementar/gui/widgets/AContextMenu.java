package com.elementar.gui.widgets;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.widgets.interfaces.OverlappingWidget;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.interfaces.Unfocusable;
import com.elementar.logic.util.Shared;

/**
 * A Context menu will be displayed when the player right-clicks a certain
 * button. The Context menu itself also consists of several buttons. The draw
 * order depends into which stage this widget will be added. It is a overlapping
 * widget.
 * 
 * @author lukassongajlo
 * 
 */
public abstract class AContextMenu extends VerticalGroup implements Unfocusable, OverlappingWidget {

	private Sprite background;

	protected final TextButtonStyle button_style = StyleContainer.button_bold_18_style;
	protected final int align = Align.left;

	protected AContextMenu() {
		loadButtons();
		setListeners();
		ArrayList<CustomTextbutton> buttons = new ArrayList<CustomTextbutton>();
		addButtons(buttons);
		init(buttons);
	}

	protected abstract void loadButtons();

	protected abstract void setListeners();

	protected abstract void addButtons(ArrayList<CustomTextbutton> buttons);

	private void init(ArrayList<CustomTextbutton> buttons) {
		setVisible(false);
		float width = 0, height = 0;
		for (Actor a : buttons) {
			width = a.getWidth();
			height += a.getHeight();
			addActor(a);
		}
		padTop(Shared.HEIGHT1 * 0.2f);
		padBottom(Shared.HEIGHT1 * 0.2f);
		float deductionPerItem = (Shared.HEIGHT1 * 0.4f) / buttons.size();
		for (Actor a : buttons)
			a.setHeight(a.getHeight() - deductionPerItem);

		background = new Sprite(StaticGraphic.gui_bg_tooltip);
		setSize(width, height);

		ArrayList<StatePossessable> widgets = new ArrayList<StatePossessable>();
		widgets.addAll(buttons);
		new HoverHandler(widgets);

	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		ShaderPolyBatch shaderBatch = (ShaderPolyBatch) batch;
		if (background != null) {
			background.setPosition(getX(), getY());
			background.draw(shaderBatch);
		}
		super.draw(shaderBatch, parentAlpha);
	}

	// /**
	// * For context menus the coords x and y should be the mouse coords
	// */
	// public void setPosition() {
	// Vector2 vec = new Vector2(Gdx.input.getX(), Gdx.input.getY());
	// getParent().screenToLocalCoordinates(vec);
	// float x = vec.x, y = vec.y;
	// if (x + getWidth() > Shared.VIRTUAL_WIDTH)
	// x = x - getWidth();
	// float diff = y - getHeight();
	// y = diff < 0 ? y - getHeight() - diff : diff;
	// super.setPosition(x, y);
	// }

	@Override
	public void unfocusWidget() {
	}

}
