package com.elementar.gui.widgets;

import static com.elementar.logic.util.Shared.HEIGHT1;
import static com.elementar.logic.util.Shared.PADDING_BOTTOM;
import static com.elementar.logic.util.Shared.PADDING_LEFT_HEIGHT1;
import static com.elementar.logic.util.Shared.PADDING_LEFT_HEIGHT2;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.AudioData;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.util.GUIUtil;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.util.SharedColors;
import com.elementar.gui.widgets.interfaces.StatePossessable;
import com.elementar.gui.widgets.listener.DisabledHandledListener;
import com.elementar.gui.widgets.listener.SoundClickListener;
import com.elementar.logic.util.Shared;

/**
 * Extension of {@link TextButton}. Holds a state (see {@link StatePossessable}
 * ). Holds a background sprite which will be drawn in the overridden
 * {@link #draw(Batch, float)}-method.
 * <p>
 * After you initiate an object of this class you can call
 * {@link #onewayClicklistener(boolean)} or
 * {@link #twowayClicklistener(boolean)} to add the functionality of switching
 * the state by clicking the button.
 * 
 * @author lukassongajlo
 * 
 */
public class CustomTextbutton extends TextButton implements StatePossessable {

	protected boolean help_bool = false;
	protected Sprite background;
	protected State state;

	/**
	 * after a click the state stays in SELECTED, a second does not lead to
	 * NON_SELECTED as you'd expect -> one way
	 */
	private DisabledHandledListener oneway_clicklistener;
	protected DisabledHandledListener twoway_clicklistener;

	private boolean hovered;

	/**
	 * by default equals white color
	 */
	private Color color_pure, color_dimmed, color_disabled;

	/**
	 * Empty button, no background, no dimensions
	 */
	public CustomTextbutton() {
		this("", StyleContainer.button_bold_18_style, 0, 0, Align.center);
	}

	public CustomTextbutton(Sprite background) {
		this("", StyleContainer.button_bold_18_style, background.getWidth(), background.getHeight(), Align.center);
	}

	/**
	 * just click buttons. For more powerful buttons use onewaySelection- or
	 * fullSelection- method. Use this constructor when the button has a
	 * background-image.
	 * 
	 * @param text
	 * @param style
	 * @param background
	 * @param align
	 */
	public CustomTextbutton(String text, TextButtonStyle style, Sprite background, int align) {
		this(text, style, background != null ? background.getWidth() : 0,
				background != null ? background.getHeight() : 0, align);
		this.background = background;
	}

	/**
	 * just click buttons. For more powerful buttons use onewaySelection- or
	 * twowaySelection- method. Use this constructor when the button hasn't a
	 * background image.
	 * 
	 * @param text
	 * @param style
	 * @param width
	 * @param height
	 * @param align
	 */
	public CustomTextbutton(String text, TextButtonStyle style, float width, float height, int align) {
		super(text, style);

		this.state = State.NOT_SELECTED;
		setWidth(width);
		setHeight(height);

		setAlignment(align);

		setColor(SharedColors.IVORY_2, SharedColors.IVORY_2, SharedColors.IVORY_2_DISABLED);

		addListener(getHoverListener());
	}

	/**
	 * The help_bool variable is used to set NOT_SELECTED just once to bring this
	 * widget out of DISABLED.
	 */
	@Override
	public void act(float delta) {

		super.act(delta);
		if (disabledCondition() == true) {
			setState(State.DISABLED);
			help_bool = true;
		} else if (help_bool == true) {
			setState(State.NOT_SELECTED);
			// help-bool helps to set button's state just once to NOT_SELECTED
			help_bool = false;
		}
	}

	/**
	 * Override this method if your button has a certain condition to be disabled.
	 * 
	 * @return false by default
	 */
	public boolean disabledCondition() {
		return false;
	}

	/**
	 * Call this method if the button should be selected after a click, but a second
	 * click results in no reaction.
	 */
	public void onewayClicklistener(boolean active) {
		if (active == true) {
			twowayClicklistener(false);
			addListener(oneway_clicklistener = new DisabledHandledListener(this) {
				@Override
				public void afterDisabledCheck() {
					state = State.SELECTED;
				}
			});
		} else if (oneway_clicklistener != null)
			removeListener(oneway_clicklistener);
	}

	/**
	 * first click leads to {@link State#SELECTED}, second to
	 * {@link State#NOT_SELECTED} (two-way listener)
	 * 
	 * @param active
	 */
	public void twowayClicklistener(boolean active) {
		if (active == true) {
			onewayClicklistener(false);
			addListener(twoway_clicklistener = new DisabledHandledListener(this) {
				@Override
				public void afterDisabledCheck() {
					if (state == State.SELECTED)
						state = State.NOT_SELECTED;
					else
						state = State.SELECTED;

				}
			});
		} else if (twoway_clicklistener != null)
			removeListener(twoway_clicklistener);

	}

	public void turnOnOffListener(boolean active) {
		if (active == true) {
			addListener(oneway_clicklistener = new DisabledHandledListener(this) {

				@Override
				public void afterDisabledCheck() {
					state = State.SELECTED;
				}
			});
		} else if (oneway_clicklistener != null)
			removeListener(oneway_clicklistener);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {

		ShaderPolyBatch shaderBatch = (ShaderPolyBatch) batch;

		GUIUtil.determineHoverEffect(shaderBatch, state);

		if (background != null) {
			if (state == State.DISABLED)
				background.setAlpha(Shared.WIDGET_DISABLED_ALPHA);

			background.setPosition(getX(), getY());
			background.draw(shaderBatch);
			background.setAlpha(1f);
		}
		drawAdditionalSprites(shaderBatch);

		// DETERMINE FONT
		if (state == State.HOVERED || state == State.SELECTED)
			getStyle().fontColor = color_pure;
		else if (state == State.DISABLED)
			getStyle().fontColor = color_disabled;
		else
			getStyle().fontColor = color_dimmed;

		super.draw(shaderBatch, parentAlpha);

		shaderBatch.flush();
		shaderBatch.reset();
	}

	/**
	 * Override this method to draw additional sprites besides the background.
	 * 
	 * @param shaderBatch
	 */
	protected void drawAdditionalSprites(ShaderPolyBatch shaderBatch) {

	}

	@Override
	public State getState() {
		return state;
	}

	@Override
	public void setState(State state) {
		this.state = state;
	}

	@Override
	public void setHovered(boolean hovered) {
		this.hovered = hovered;
	}

	@Override
	public boolean isHovered() {
		return hovered;
	}

	public void setColor(Color pure, Color dimmed, Color disabled) {
		color_pure = pure;
		color_dimmed = dimmed;
		color_disabled = disabled;
	}

	public void setAlignment(int alignment) {
		getLabel().setAlignment(alignment);
		float paddingLeft = 0;
		if (MathUtils.isEqual(getHeight(), HEIGHT1))
			paddingLeft = PADDING_LEFT_HEIGHT1;
		else
			paddingLeft = PADDING_LEFT_HEIGHT2;

		if (alignment == Align.center)
			paddingLeft = 0;

		getLabelCell().pad(0, paddingLeft, PADDING_BOTTOM, 0);

	}

	public void setSound(ClickSoundType type) {
		final Sound clickSound;

		if (type == ClickSoundType.HEAVY)
			clickSound = AudioData.gui_btn_click_heavy1;
		else
			clickSound = AudioData.gui_btn_click_light1;

		addListener(new SoundClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (state != State.DISABLED) {
					AudioData.playEffect(clickSound);
				}
			}
		});
	}

	/**
	 * Sets the background sprite
	 * 
	 * @param bg
	 */
	public void setBG(Sprite bg) {
		background = bg;
	}

	public Sprite getBackgroundSprite() {
		return background;
	}

	@Override
	public float getPrefHeight() {
		return getHeight();
	}

	@Override
	public float getPrefWidth() {
		return getWidth();
	}

	public void setTextPaddingBot(float padBottom) {
		getLabelCell().padBottom(padBottom);
	}

	@Override
	public String toString() {
		return getText().toString();
	}
}
