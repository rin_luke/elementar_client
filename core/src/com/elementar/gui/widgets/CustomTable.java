package com.elementar.gui.widgets;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.elementar.gui.widgets.interfaces.OverlappingWidget;

public class CustomTable extends Table {

	private boolean contains_overlapping = false;

	@Override
	public <T extends Actor> Cell<T> add(T actor) {

		if (actor instanceof OverlappingWidget)
			contains_overlapping = true;

		if (actor instanceof CustomTextfield) {
			CustomTextfield textfield = (CustomTextfield) actor;
			return (Cell<T>) super.add(textfield.getGroup()).width(textfield.getBoundingButton().getWidth())
					.height(textfield.getBoundingButton().getHeight());

		}

		if (actor instanceof CustomTextArea) {
			CustomTextArea area = (CustomTextArea) actor;
			return (Cell<T>) super.add(area.getGroup()).width(area.getBoundingButton().getWidth())
					.height(area.getBoundingButton().getHeight());
		}

		if (actor instanceof CustomDropdown) {
			CustomDropdown dd = (CustomDropdown) actor;
			Cell<T> cell = (Cell<T>) super.add(dd);
			dd.setCell((Cell<CustomDropdown>) cell);
			return cell;
		}
		if (actor instanceof CustomSlider)
			return (Cell<T>) super.add((CustomSlider) actor).width(actor.getWidth());

		return super.add(actor);
	}

	/**
	 * While new actors are added to the table, there might be one that implements
	 * the {@link OverlappingWidget} interface. In this case the
	 * {@link #contains_overlapping} flag is set to <code>true</code>
	 * 
	 * @return whether the table contains an actor that implements
	 *         {@link OverlappingWidget} (tooltips or context menues for example)
	 */
	public boolean containsOverlapping() {
		return contains_overlapping;
	}

	public void setContainsOverlapping() {
		contains_overlapping = true;
	}

}
