package com.elementar.gui.widgets;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;
import com.elementar.data.container.AudioData.ClickSoundType;
import com.elementar.gui.abstracts.AModule;
import com.elementar.gui.widgets.interfaces.Unfocusable;
import com.elementar.gui.widgets.interfaces.WidgetGroupable;

public class CustomDropdown extends CustomTextbutton implements WidgetGroupable, Unfocusable {

	/**
	 * Each drop-down has items which are put into a separate {@link VerticalGroup}
	 * to ensure that they aren't drawn above other widgets when the menu is open.
	 */
	private VerticalGroup items_group;

	private ArrayList<CustomTextbutton> items_ordered;

	private CustomTextbutton selected_button;

	private boolean is_open = false;
	private Sprite dropdown_background_closed, dropdown_background_open;

	private boolean allowing_visible_change = false;

	/**
	 * In one case I need the key of the selected string. See
	 * {@link #getSelectedAsKey()}
	 */
	private ArrayList<KeyStringPair> key_string_pairs;

	/**
	 * Note: Do not you use 'null' parameters!
	 * 
	 * @param itemsAsStrings
	 * @param itemBackground
	 * @param dropdownBackgroundClosed
	 * @param dropdownBackgroundOpen
	 * @param align
	 */
	public CustomDropdown(ArrayList<String> itemsAsStrings, Sprite itemBackground, Sprite dropdownBackgroundClosed,
			Sprite dropdownBackgroundOpen, int align) {
		super("", StyleContainer.button_bold_18_style, dropdownBackgroundClosed, align);

		dropdown_background_closed = dropdownBackgroundClosed;
		dropdown_background_open = dropdownBackgroundOpen;

		addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				if (is_open == true)
					closeDropdown();
				else
					openDropdown();

			}

		});

		items_group = new VerticalGroup();
		items_ordered = new ArrayList<>();

		key_string_pairs = new ArrayList<KeyStringPair>(itemsAsStrings.size());

		KeyStringPair pair;
		for (int i = 0; i < itemsAsStrings.size(); i++) {
			pair = new KeyStringPair(itemsAsStrings.get(i));
			key_string_pairs.add(pair);

			final CustomTextbutton button = new CustomTextbutton(pair.string, StyleContainer.button_bold_18_style,
					itemBackground, Align.center) {

				@Override
				public void setVisible(boolean visible) {
					if (allowing_visible_change == true)
						super.setVisible(visible);
				}
			};
			button.setSound(ClickSoundType.LIGHT);
			button.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					super.clicked(event, x, y);
					if (is_open == true) {
						selected_button = button;
						sortItems();
						closeDropdown();
						clickedItem();
					}

				}

			});

			items_ordered.add(button);
			items_group.addActor(button);
		}

		// select item, sort items and close dropdown menu
		selected_button = (CustomTextbutton) items_group.getChildren().get(0);
		sortItems();
		closeDropdown();
	}

	/**
	 * Override this method to add a functionality after clicking on a item-button
	 */
	public void clickedItem() {
	}

	@Override
	public void setPosition(float x, float y) {
		super.setPosition(x, y);
		sortItems();
	}

	private void closeDropdown() {
		setText(getSelectedText());
		setState(State.NOT_SELECTED);
		background = dropdown_background_closed;
		is_open = false;
		setVisibilityItems(false);
	}

	private void openDropdown() {
		setState(State.SELECTED);
		background = dropdown_background_open;
		is_open = true;
		setVisibilityItems(true);
	}

	/**
	 * Removes the {@link #selected_button} from the {@link VerticalGroup}.
	 * Afterwards the button joins the end of the queue.
	 */
	private void sortItems() {

		items_group.clearChildren();

		for (CustomTextbutton item : items_ordered)
			items_group.addActor(item);

	}

	public String getSelectedText() {
		return selected_button.getText().toString();
	}

	public String getSelectedAsKey() {
		for (KeyStringPair pair : key_string_pairs)
			if (pair.string.equals(getSelectedText()) == true)
				return pair.key;
		return "";
	}

	@Override
	public void unfocusWidget() {
		if (isHovered() == false) {
			boolean mouseclickOutside = true;
			for (CustomTextbutton item : items_ordered) {
				if (item.isHovered() == true) {
					mouseclickOutside = false;
					break;
				}
			}
			if (mouseclickOutside == true)
				closeDropdown();

		}
	}

	/**
	 * Returns items as list
	 * 
	 * @return
	 */
	public ArrayList<CustomTextbutton> getItems() {
		ArrayList<CustomTextbutton> itemsAsList = new ArrayList<>();
		for (Actor item : items_group.getChildren())
			itemsAsList.add((CustomTextbutton) item);
		return itemsAsList;
	}

	@Override
	public void setVisibilityWidgets(boolean visible) {
		// the visibility of items are just handled by this class (no access of
		// other modules allowed)
		setVisible(visible);
		if (visible == false)
			closeDropdown();
	}

	private void setVisibilityItems(boolean visible) {
		allowing_visible_change = true;
		for (Actor item : items_group.getChildren())
			item.setVisible(visible);
		allowing_visible_change = false;
	}

	private class KeyStringPair {

		private String key, string;

		public KeyStringPair(String key) {
			this.key = key;
			this.string = TextData.getWord(key);
		}

	}

	/**
	 * Has to be called in {@link AModule#setLayout()} to layout the dropdown-menu
	 * items.
	 * 
	 * @param tables
	 * @param tableLessPrioritized
	 */
	public void setTable(ArrayList<CustomTable> tables, CustomTable tableLessPrioritized) {
		/*
		 * confirm preceding layout of the less prioritized for positioning the dropdown
		 * items
		 */
		tableLessPrioritized.validate();

		CustomTable tableDropdownItems = new CustomTable();
		tableDropdownItems.setFillParent(true);
		tables.add(tableDropdownItems);

		// always left aligned
		tableDropdownItems.add(items_group);
		tableDropdownItems.layout();
		tableDropdownItems.setPosition(cell.getActorX() + getWidth() * 0.5f,
				cell.getActorY() - (items_group.getChildren().size - 1) * getHeight() * 0.5f);

	}

	private Cell<CustomDropdown> cell;

	public void setCell(Cell<CustomDropdown> cell) {
		this.cell = cell;
	}
}
