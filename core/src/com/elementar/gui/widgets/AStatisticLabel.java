package com.elementar.gui.widgets;

import com.badlogic.gdx.utils.Align;
import com.elementar.data.container.StyleContainer;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.util.Shared;

public abstract class AStatisticLabel extends CustomLabel {

	protected OmniClient client;

	public AStatisticLabel(OmniClient client) {
		super("", StyleContainer.label_bold_whitepure_18_style, Shared.WIDTH2, Shared.HEIGHT2,
				Align.center);
		this.client = client;
	}

	@Override
	public void act(float delta) {
		super.act(delta);

		setText("" + getStatistic());

	}

	protected abstract int getStatistic();

}
