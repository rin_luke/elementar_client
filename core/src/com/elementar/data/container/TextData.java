package com.elementar.data.container;

import static com.elementar.data.container.SettingsLoader.loadCurrentLanguage;

import java.util.Locale;

import com.badlogic.gdx.assets.loaders.I18NBundleLoader.I18NBundleParameter;
import com.badlogic.gdx.utils.I18NBundle;
import com.elementar.data.DataTier;

/**
 * All Strings which are used in the game are stored in .properties files in the
 * languages folder. Each file represents one language. With
 * {@link #getWord(String)} you can find the word that you are looking for in a
 * specific language.
 * <p>
 * The language will be loaded from the {@link SettingsLoader}.
 * 
 * @author lukassongajlo
 * 
 */
public class TextData {

	private static I18NBundle word_bundle;

	public static void load() {
		DataTier.ASSET_MANAGER.load("languages/RiNBundle1", I18NBundle.class,
				new I18NBundleParameter(Locale.forLanguageTag(loadCurrentLanguage())));
	}

	public static void setFields() {
		word_bundle = DataTier.ASSET_MANAGER.get("languages/RiNBundle1", I18NBundle.class);
	}

	/**
	 * Finds a String in the .properties file by the corresponding key.
	 * 
	 * @param key
	 * @return
	 */
	public static String getWord(String key) {
		String word = "";
		try {
			word = word_bundle.get(key);
		} catch (Exception e) {
			/*
			 * if there is no corresponding word for the key, we use the key as
			 * result, especially needed for numbers like resolutions
			 */
			word = key;
		}

		return word;
	}
}
