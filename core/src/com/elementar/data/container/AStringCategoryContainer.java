package com.elementar.data.container;

import java.util.ArrayList;

import com.elementar.logic.util.lists.CategoryArrayList;

/**
 * Template for string-category-containers. A derived container class consists
 * of "pots". Each pot represents a bunch of categories and holds the
 * sprite-names that share these categories. To see a concrete implementations
 * take a look at {@link PlatformContainer} or/and {@link VegetationContainer}.
 * 
 * @author lukassongajlo
 * 
 */
public abstract class AStringCategoryContainer {

	// sorted by Category
	protected ArrayList<CategoryArrayList<String>> all_pots;

	public AStringCategoryContainer() {
		all_pots = new ArrayList<CategoryArrayList<String>>();
		createPots();
		addStrings();
		addPots();
	}

	protected abstract void createPots();

	protected abstract void addStrings();

	protected abstract void addPots();

	public ArrayList<CategoryArrayList<String>> getAllPots() {
		return all_pots;
	}

}
