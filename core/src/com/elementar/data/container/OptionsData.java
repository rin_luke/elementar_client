package com.elementar.data.container;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.elementar.data.DataTier;
import com.elementar.gui.modules.options.GraphicsModule;
import com.elementar.gui.modules.options.OptionsModule;
import com.elementar.gui.util.VegetationSprite;
import com.elementar.gui.widgets.ResolutionDropdown;

/**
 * Loads options-data without audio! Audio is handled in {@link AudioData}. Will
 * be loaded from the {@link DataTier} class at the beginning.
 * <p>
 * Options can be modified by the User in {@link OptionsModule}.
 * 
 * @author lukassongajlo
 * 
 */
public class OptionsData {
	public static boolean fps_on, fullscreen_on, v_sync_on, minimap_left;
	/**
	 * Describes which plants should have a hitbox where the player can going
	 * through and which not. If all possible sizes of plants (small, medium,
	 * large) should have a hitbox the slider in {@link GraphicsModule} have to
	 * set to its maximum value (full right). Maximum value - 1 means all
	 * without small plants, maximum - 2 all without small and medium, and so
	 * on,
	 * {@link VegetationSprite#hasPhysicalBody(int, logic.utility.Shared.CategorySize)}
	 * <p>
	 * Note: if the player is within the world, changes will be occur after
	 * closing the {@link OptionsModule}
	 * 
	 */
	public static int vegetation_intersection_level;
	
	public static float combo_rose_sensitivity;

	public static String aspect_ratio;
	public static String resolution;

	public static void load() {
		String arr[] = SettingsLoader.loadGraphicSettings();
		fps_on = Boolean.valueOf(arr[0]);

		fullscreen_on = Boolean.valueOf(arr[1]);

		vegetation_intersection_level = Integer.valueOf(arr[2]);

		aspect_ratio = arr[3];
		resolution = arr[4];
		v_sync_on = Boolean.valueOf(arr[5]);
		minimap_left = Boolean.valueOf(arr[6]);
		
		combo_rose_sensitivity = Float.valueOf(arr[7]);

		SettingsLoader.loadKeysConfiguration();

		// graphics
		Integer[] resolutionIntegers = ResolutionDropdown.parseResolution(resolution);

		if (resolutionIntegers != null) {

			int resoWidth = resolutionIntegers[0];
			int resoHeight = resolutionIntegers[1];

			if (OptionsData.fullscreen_on == false)
				Gdx.graphics.setWindowedMode(resoWidth, resoHeight);
			else {

				for (DisplayMode mode : Gdx.graphics.getDisplayModes())
					if (mode.width == resoWidth && mode.height == resoHeight)
						Gdx.graphics.setFullscreenMode(mode);

			}

		} else if (OptionsData.fullscreen_on == true)
			Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
		else
			Gdx.graphics.setWindowedMode(Gdx.graphics.getDisplayMode().width,
					Gdx.graphics.getDisplayMode().height);

	}
}
