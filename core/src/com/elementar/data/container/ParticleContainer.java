package com.elementar.data.container;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.elementar.data.DataTier;
import com.elementar.data.container.util.CustomParticleEffect;
import com.elementar.data.container.util.EmissionParticleEffect;
import com.elementar.data.container.util.Gameclass;
import com.elementar.data.container.util.GameclassParticleEffect;
import com.elementar.data.container.util.OverTimeParticleEffect;
import com.elementar.data.container.util.ParticleEmitter;
import com.elementar.data.container.util.TweenableSound;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.data.container.util.ParticleEmitter.SpriteMode;
import com.elementar.gui.modules.roots.IntroRoot;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.creeps.OmniCreep;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.SparseProjectile;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.map.MapManager;
import com.elementar.logic.map.buildings.ABuilding;
import com.elementar.logic.map.buildings.Tower;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.Shared.Team;

/**
 * Loads particle-data. Will be loaded from the {@link DataTier} class at the
 * beginning.
 * 
 * @author lukassongajlo
 * 
 */
public class ParticleContainer {

	/**
	 * Contains all particle sprites.
	 */
	private static TextureAtlas atlas_particles;

	/**
	 * Contains all prototypes for gameclasses (particles on characters except
	 * combohand effects)
	 */
	private static ArrayList<GameclassParticleEffect> gameclass_particle_prototypes;

	private static ArrayList<CustomParticleEffect> creep_particle_prototypes;

	/**
	 * Contains all copies of gamecass particle effect made out of prototypes (lying
	 * in {@link #gameclass_particle_prototypes})
	 */
	private static ArrayList<GameclassParticleEffect> gameclass_particle_copies = new ArrayList<>();

	private static ArrayList<CustomParticleEffect> creep_particle_copies = new ArrayList<>();

	private static ArrayList<CustomParticleEffect> over_time_particle_copies = new ArrayList<>();

	private static HashMap<Integer, EmissionEffectPool> emission_effect_pools;

	public static CustomParticleEffect particle_gui_btn_home, particle_skin_pedestal, particle_stunned, particle_rooted,
			particle_silenced, particle_disarmed, particle_skin_buy, particle_skin_equipped, particle_gui_prelobby,
			particle_gui_kill_left_enemy_team, particle_gui_kill_right_enemy_team, particle_gui_kill_left_my_team,
			particle_gui_kill_right_my_team;

	/**
	 * Has to be called inside {@link IntroRoot} because we need one particle effect
	 * before loading the whole data
	 */
	public static void load() {
		DataTier.ASSET_MANAGER.load("graphic/animation_characters.atlas", TextureAtlas.class);
	}

	public static void setFields() {

		atlas_particles = DataTier.ASSET_MANAGER.get("graphic/animation_characters.atlas", TextureAtlas.class);

		creep_particle_prototypes = new ArrayList<>();
		loadCreep();

		gameclass_particle_prototypes = new ArrayList<>();

		loadIce();
		loadLeaf();
		loadLight();
		loadLightning();
		loadSand();
		loadStone();
		loadToxic();
		loadVoid();
		loadWater();
		loadWind();

		loadEmissionEffects();

		particle_gui_btn_home = loadParticle("graphic/particles/particle_gui/gui_btn_home1.p");

		// kill effect enemy team at info bar
		particle_gui_kill_left_enemy_team = loadParticleWithoutStart(
				"graphic/particles/particle_gui/gui_kill_enemy_team.p");
		particle_gui_kill_right_enemy_team = loadParticleWithoutStart(
				"graphic/particles/particle_gui/gui_kill_enemy_team.p");
		particle_gui_kill_right_enemy_team.setFlipX(true);

		// kill effect my team at info bar
		particle_gui_kill_left_my_team = loadParticleWithoutStart("graphic/particles/particle_gui/gui_kill_my_team.p");
		particle_gui_kill_right_my_team = loadParticleWithoutStart("graphic/particles/particle_gui/gui_kill_my_team.p");
		particle_gui_kill_right_my_team.setFlipX(true);

		// effects when player hovered over an other character or buildings
		// (base, tower)

		particle_gui_prelobby = loadParticle("graphic/particles/particle_gui/gui_prelobby.p");

		particle_skin_pedestal = loadParticle("graphic/particles/particle_gui/gui_skin_pedestal.p");

		particle_skin_buy = loadParticle("graphic/particles/particle_gui/gui_skin_buy.p");

		particle_skin_equipped = loadParticle("graphic/particles/particle_gui/gui_skin_equipped.p");

		float scale = 0.175f;
		particle_stunned = loadParticle("graphic/particles/particle_global/global_cc_stunned.p");
		particle_stunned.scaleEffect(scale);

		particle_rooted = loadParticle("graphic/particles/particle_global/global_cc_rooted.p");
		particle_rooted.scaleEffect(scale);

		particle_disarmed = loadParticle("graphic/particles/particle_global/global_cc_disarmed.p");
		particle_disarmed.scaleEffect(scale);

		particle_silenced = loadParticle("graphic/particles/particle_global/global_cc_silenced.p");
		particle_silenced.scaleEffect(scale);

	}

	public static void loadEmissionEffects() {
		emission_effect_pools = new HashMap<>();

		/*
		 * emission particle effects are stored inside 'particle_global' and
		 * 'particle_skill', the names are stored in "emission_effects_names.txt"
		 */
		FileHandle handle = Gdx.files.internal("graphic/particles/emission_effects_names.txt");
		String fileNameArray[] = handle.readString().split("\\r?\\n");

		String fileName;
		for (short i = 0; i < fileNameArray.length; i++) {
			fileName = fileNameArray[i];
			EmissionParticleEffect particleEffect = new EmissionParticleEffect();

//			System.out.println("ParticleContainer.setFields()			i = " + i + ", fileName = " + fileName);

			particleEffect.load(fileName, atlas_particles, "characters/particle_sprites/");

			// each particle effect gets an unique id
			EmissionEffectPool pool = new EmissionEffectPool(i, particleEffect);

			// add emissionType
			emission_effect_pools.put((int) i, pool);

		}
	}

	/**
	 * 
	 * @param t
	 * @return
	 */
	public static CustomParticleEffect makeTowerParticle(Tower t) {

		CustomParticleEffect effect = loadParticle(
				"graphic/particles/particle_building/building_tower_team" + Util.getTeamNumber(t.getTeam()) + ".p");

		Vector2 shift = new Vector2(0, 0);
		shift.y = 490 * Shared.SCALE_BUILDINGS;

		Vector2 pos = Util.addVectors(t.getPosition(), shift);

		effect.setPosition(pos.x, pos.y);
		effect.scaleEffect(Shared.SCALE_BUILDINGS);

		return effect;
	}

	/**
	 * Returns and starts a particle-effect. Loads png file from
	 * {@link #atlas_particles}. Note: Do not position or scale the effect!
	 * 
	 * @param internalPath
	 * @return
	 */
	public static CustomParticleEffect loadParticle(String internalPath) {
		CustomParticleEffect effect = new CustomParticleEffect();
		effect.load(Gdx.files.internal(internalPath), atlas_particles, "characters/particle_sprites/");
		effect.start();
		return effect;

	}

	/**
	 * Returns a particle effect. Loads png file from {@link #atlas_particles}.
	 * Note: Do not position or scale the effect!
	 * 
	 * @param internalPath
	 * @return
	 */
	public static CustomParticleEffect loadParticleWithoutStart(String internalPath) {
		CustomParticleEffect effect = new CustomParticleEffect();
		effect.load(Gdx.files.internal(internalPath), atlas_particles, "characters/particle_sprites/");
		return effect;

	}

	/**
	 * 
	 * @param internalPath   , e.g.
	 *                       "graphic/particles/particle_skill/lightning_skill1_combohand_skin0.p"
	 * @param skinName       , e.g. "character_light_skin0"
	 * @param slotName       , e.g. "character_hand_back_combo" or
	 *                       "character_hand_front_combo"
	 * @param drawBehindBone
	 * @return
	 */
	public static CustomParticleEffect loadCombohandParticle(String internalPath, GameclassName gameclassName,
			String slotName, boolean drawBehindBone) {
		return loadGameclassParticle(internalPath, gameclassName, "", slotName, 0, 0, drawBehindBone);
	}

	/**
	 * 
	 * @param pFileString
	 * @param flip
	 * @return
	 */
	public static OverTimeParticleEffect loadOverTimeEffect(String pFileString) {
		CustomParticleEffect effect = loadParticleWithoutStart("graphic/particles/particle_skill/" + pFileString);
		OverTimeParticleEffect overtimeParticleEffect = new OverTimeParticleEffect(effect);
		over_time_particle_copies.add(overtimeParticleEffect);
		return overtimeParticleEffect;
	}

	/**
	 * 
	 * @param internalPath   , e.g.
	 *                       "graphic/particles/particle_character/character_light_skin0_head1.p"
	 * @param skinName       , e.g. "character_light_skin0"
	 * @param slotName       , e.g. "character_head"
	 * @param offsetX
	 * @param offsetY
	 * @param drawBehindBone
	 * @return
	 */
	private static GameclassParticleEffect loadGameclassParticle(String internalPath, GameclassName gameclassName,
			String skinName, String slotName, float offsetX, float offsetY, boolean drawBehindBone) {

		GameclassParticleEffect gameclassParticleEffect = new GameclassParticleEffect(gameclassName, skinName, slotName,
				offsetX, offsetY, drawBehindBone);

		gameclassParticleEffect.load(Gdx.files.internal(internalPath), atlas_particles, "characters/particle_sprites/");
		gameclassParticleEffect.start();

		gameclass_particle_prototypes.add(gameclassParticleEffect);

		return gameclassParticleEffect;
	}

	/**
	 * 
	 * @param internalPath   , e.g.
	 *                       "graphic/particles/particle_character/character_light_skin0_head1.p"
	 * @param skinName       , e.g. "character_light_skin0"
	 * @param slotName       , e.g. "character_head"
	 * @param offsetX
	 * @param offsetY
	 * @param drawBehindBone
	 * @return
	 */
	private static void loadCreepParticle(String internalPath, String skinName, String slotName, float offsetX,
			float offsetY, boolean drawBehindBone) {

		CustomParticleEffect customEffect = new CustomParticleEffect(skinName, slotName, offsetX, offsetY,
				drawBehindBone);

		customEffect.load(Gdx.files.internal(internalPath), atlas_particles, "characters/particle_sprites/");
		customEffect.start();

		creep_particle_prototypes.add(customEffect);
	}

	public static void drawWORLDParticles(ShaderPolyBatch batch, float delta, MapManager mapManager) {
		if (mapManager.getBaseLeft().isAlive() == true)
			mapManager.getBaseLeft().drawEffects(batch, delta);

		if (mapManager.getBaseRight().isAlive() == true)
			mapManager.getBaseRight().drawEffects(batch, delta);

		for (Tower t : mapManager.getTowers()) {
			if (t.isAlive() == true)
				if (t.getParticleEffect() != null)
					t.getParticleEffect().draw(batch, delta);
		}
	}

	public static void dispose() {
		if (gameclass_particle_prototypes != null)
			for (GameclassParticleEffect particleEffect : gameclass_particle_prototypes)
				particleEffect.dispose();

		if (creep_particle_prototypes != null)
			for (CustomParticleEffect particleEffect : creep_particle_prototypes)
				particleEffect.dispose();

		if (gameclass_particle_copies != null)
			for (GameclassParticleEffect particleEffect : gameclass_particle_copies)
				particleEffect.dispose();

		if (creep_particle_copies != null)
			for (CustomParticleEffect particleEffect : creep_particle_copies)
				particleEffect.dispose();

		if (over_time_particle_copies != null)
			for (CustomParticleEffect particleEffect : over_time_particle_copies)
				particleEffect.dispose();

		disposeEmissionEffects();
	}

	/**
	 * Dispose all emission effects.
	 * 
	 * @param withReload
	 */
	public static void disposeEmissionEffects() {
		if (emission_effect_pools == null)
			return;
		for (EmissionEffectPool pool : emission_effect_pools.values()) {
			for (EmissionParticleEffect effect : pool.actives.values())
				effect.dispose();
			pool.clear();
		}

		emission_effect_pools.clear();

	}

	public static HashMap<String, GameclassParticleEffect> getParticleEffectsByGameclassAndSkin(Gameclass gameclass,
			String skin) {

		HashMap<String, GameclassParticleEffect> resultingMap = new HashMap<>();

		for (GameclassParticleEffect particleEffect : gameclass_particle_prototypes)
			if (particleEffect.getGameclassName() == gameclass.getNameAsEnum()
					&& particleEffect.getSkin().equals(skin)) {

				// copy effect
				GameclassParticleEffect effect = new GameclassParticleEffect(particleEffect);
				gameclass_particle_copies.add(effect);
				resultingMap.put(effect.getSlotName(), effect);
			}
		return resultingMap;
	}

	public static ArrayList<CustomParticleEffect> getCreepParticleEffects(String skinName) {
		ArrayList<CustomParticleEffect> resultingList = new ArrayList<>();
		for (CustomParticleEffect particleEffect : creep_particle_prototypes)
			if (particleEffect.getSkin().equals(skinName)) {
				// copy effect
				CustomParticleEffect effect = new CustomParticleEffect(particleEffect);
				creep_particle_copies.add(effect);
				resultingList.add(effect);
			}
		return resultingList;
	}

	private static void loadCreep() {

		loadCreepParticle("graphic/particles/particle_creep/creep_small1_torso.p", "creep_small1", "character_torso",
				40, 85, false);
	}

	private static void loadIce() {
		// ICE skin0
		loadGameclassParticle("graphic/particles/particle_character/character_ice_skin0_torso1.p", GameclassName.ICE,
				"character_ice_skin0", "character_torso", -30, 60, false);

		// ICE skin1
		loadGameclassParticle("graphic/particles/particle_character/character_ice_skin1_torso1.p", GameclassName.ICE,
				"character_ice_skin1", "character_torso", 0, 20, false);
		loadGameclassParticle("graphic/particles/particle_character/character_ice_skin1_head1.p", GameclassName.ICE,
				"character_ice_skin1", "character_head", 20, 80, false);
		loadGameclassParticle("graphic/particles/particle_character/character_ice_skin1_hand1.p", GameclassName.ICE,
				"character_ice_skin1", "character_hand_front", 60, -40, false);
		loadGameclassParticle("graphic/particles/particle_character/character_ice_skin1_hand1.p", GameclassName.ICE,
				"character_ice_skin1", "character_hand_back", 60, -40, false);
	}

	private static void loadLeaf() {
		// LEAF skin0
		loadGameclassParticle("graphic/particles/particle_character/character_leaf_skin0_head1.p", GameclassName.LEAF,
				"character_leaf_skin0", "character_head", 170, 0, false);
		
		// LEAF skin1
		loadGameclassParticle("graphic/particles/particle_character/character_leaf_skin1_torso1.p", GameclassName.LEAF,
				"character_leaf_skin1", "character_torso", 10, 100, true);
		loadGameclassParticle("graphic/particles/particle_character/character_leaf_skin1_head1.p", GameclassName.LEAF,
				"character_leaf_skin1", "character_head", -30, 80, true);
		loadGameclassParticle("graphic/particles/particle_character/character_leaf_skin1_hand1.p", GameclassName.LEAF,
				"character_leaf_skin1", "character_hand_front", -50, 35, true);
		loadGameclassParticle("graphic/particles/particle_character/character_leaf_skin1_hand1.p", GameclassName.LEAF,
				"character_leaf_skin1", "character_hand_back", -50, 35, true);
	}

	private static void loadLight() {
		// LIGHT skin0
		// loadGameclassParticle("graphic/particles/particle_character/character_light_skin0_head1.p",
		// GameclassName.LIGHT,
		// "character_light_skin0", "character_head", 42, 40, false);
		loadGameclassParticle("graphic/particles/particle_character/character_light_skin0_torso1.p",
				GameclassName.LIGHT, "character_light_skin0", "character_torso", 0, 0, false);
		loadGameclassParticle("graphic/particles/particle_character/character_light_skin0_hand1.p", GameclassName.LIGHT,
				"character_light_skin0", "character_hand_front", 20, -10, false);
		loadGameclassParticle("graphic/particles/particle_character/character_light_skin0_hand1.p", GameclassName.LIGHT,
				"character_light_skin0", "character_hand_back", 20, -10, false);

		// LIGHT skin1
		loadGameclassParticle("graphic/particles/particle_character/character_light_skin1_torso1.p",
				GameclassName.LIGHT, "character_light_skin1", "character_torso", 0, 0, false);
		loadGameclassParticle("graphic/particles/particle_character/character_light_skin1_hand1.p", GameclassName.LIGHT,
				"character_light_skin1", "character_hand_front", 20, -10, false);
		loadGameclassParticle("graphic/particles/particle_character/character_light_skin1_hand1.p", GameclassName.LIGHT,
				"character_light_skin1", "character_hand_back", 20, -10, false);

		// LIGHT skin2
		loadGameclassParticle("graphic/particles/particle_character/character_light_skin2_torso1.p",
				GameclassName.LIGHT, "character_light_skin2", "character_torso", -10, -40, false);
		loadGameclassParticle("graphic/particles/particle_character/character_light_skin2_hand1.p", GameclassName.LIGHT,
				"character_light_skin2", "character_hand_back", 30, 35, false);
		loadGameclassParticle("graphic/particles/particle_character/character_light_skin2_hand1.p", GameclassName.LIGHT,
				"character_light_skin2", "character_hand_front", 30, 40, false);

	}

	private static void loadLightning() {
		// LIGHTNING skin0
		loadGameclassParticle("graphic/particles/particle_character/character_lightning_skin0_torso1.p",
				GameclassName.LIGHTNING, "character_lightning_skin0", "character_torso", -40, 0, true);
		loadGameclassParticle("graphic/particles/particle_character/character_lightning_skin0_head1.p",
				GameclassName.LIGHTNING, "character_lightning_skin0", "character_head", -15, 60, false);
//		loadGameclassParticle("graphic/particles/particle_character/character_lightning_skin0_hand1.p",
//				GameclassName.LIGHTNING, "character_lightning_skin0", "character_hand_front", -75, 0, false);
//		loadGameclassParticle("graphic/particles/particle_character/character_lightning_skin0_hand1.p",
//				GameclassName.LIGHTNING, "character_lightning_skin0", "character_hand_back", -75, 0, false);

		// LIGHTNING skin1
		loadGameclassParticle("graphic/particles/particle_character/character_lightning_skin1_torso1.p",
				GameclassName.LIGHTNING, "character_lightning_skin1", "character_torso", 10, 50, false);
		loadGameclassParticle("graphic/particles/particle_character/character_lightning_skin1_head1.p",
				GameclassName.LIGHTNING, "character_lightning_skin1", "character_head", -25, 45, false);
		loadGameclassParticle("graphic/particles/particle_character/character_lightning_skin1_hand1.p",
				GameclassName.LIGHTNING, "character_lightning_skin1", "character_hand_front", -75, 0, false);
		loadGameclassParticle("graphic/particles/particle_character/character_lightning_skin1_hand1.p",
				GameclassName.LIGHTNING, "character_lightning_skin1", "character_hand_back", -75, 0, false);

	}

	private static void loadSand() {
		// SAND skin0
		loadGameclassParticle("graphic/particles/particle_character/character_sand_skin0_weapon1.p", GameclassName.SAND,
				"character_sand_skin0", "character_weapon_front3", 0, 0, false);
		loadGameclassParticle("graphic/particles/particle_character/character_sand_skin0_hand1.p", GameclassName.SAND,
				"character_sand_skin0", "character_hand_front", 0, 0, false);
		loadGameclassParticle("graphic/particles/particle_character/character_sand_skin0_head1.p", GameclassName.SAND,
				"character_sand_skin0", "character_head", -40, 20, false);
		loadGameclassParticle("graphic/particles/particle_character/character_sand_skin0_torso1.p", GameclassName.SAND,
				"character_sand_skin0", "character_torso", -15, 70, false);
	}

	private static void loadStone() {
		// STONE skin0
		loadGameclassParticle("graphic/particles/particle_character/character_stone_skin0_torso1.p",
				GameclassName.STONE, "character_stone_skin0", "character_torso", -105, 35, false);
		loadGameclassParticle("graphic/particles/particle_character/character_stone_skin0_head1.p", GameclassName.STONE,
				"character_stone_skin0", "character_head", 20, 60, false);

		// STONE skin1
		loadGameclassParticle("graphic/particles/particle_character/character_stone_skin1_torso1.p",
				GameclassName.STONE, "character_stone_skin1", "character_torso", 80, 25, false);
		loadGameclassParticle("graphic/particles/particle_character/character_stone_skin1_head1.p", GameclassName.STONE,
				"character_stone_skin1", "character_head", 10, 115, false);
		loadGameclassParticle("graphic/particles/particle_character/character_stone_skin1_hand1.p", GameclassName.STONE,
				"character_stone_skin1", "character_hand_front", 50, -30, false);
		loadGameclassParticle("graphic/particles/particle_character/character_stone_skin1_hand1.p", GameclassName.STONE,
				"character_stone_skin1", "character_hand_back", 40, -30, false);
	}

	private static void loadToxic() {
		// TOXIC skin0
		loadGameclassParticle("graphic/particles/particle_character/character_toxic_skin0_weapon1.p", GameclassName.TOXIC,
				"character_toxic_skin0", "character_weapon_front2", 0, 0, false);
		loadGameclassParticle("graphic/particles/particle_character/character_toxic_skin0_torso1.p",
				GameclassName.TOXIC, "character_toxic_skin0", "character_torso", -30, 70, false);
		loadGameclassParticle("graphic/particles/particle_character/character_toxic_skin0_head1.p", GameclassName.TOXIC,
				"character_toxic_skin0", "character_head", 60, 80, false);

		// TOXIC skin1
		loadGameclassParticle("graphic/particles/particle_character/character_toxic_skin1_head1.p", GameclassName.TOXIC,
				"character_toxic_skin1", "character_head", -10, 10, false);
		loadGameclassParticle("graphic/particles/particle_character/character_toxic_skin1_hand1.p", GameclassName.TOXIC,
				"character_toxic_skin1", "character_hand_front", -20, 70, false);
		loadGameclassParticle("graphic/particles/particle_character/character_toxic_skin1_hand1.p", GameclassName.TOXIC,
				"character_toxic_skin1", "character_hand_back", -20, 70, false);

	}

	private static void loadVoid() {
		// VOID skin0
		loadGameclassParticle("graphic/particles/particle_character/character_void_skin0_torso1.p", GameclassName.VOID,
				"character_void_skin0", "character_torso", -10, 50, true);
		loadGameclassParticle("graphic/particles/particle_character/character_void_skin0_head1.p", GameclassName.VOID,
				"character_void_skin0", "character_head", 35, 50, false);
		loadGameclassParticle("graphic/particles/particle_character/character_void_skin0_hand1.p", GameclassName.VOID,
				"character_void_skin0", "character_hand_front", 10, -25, true);
		loadGameclassParticle("graphic/particles/particle_character/character_void_skin0_hand2.p", GameclassName.VOID,
				"character_void_skin0", "character_hand_back", 30, -20, true);

		// VOID skin1
		loadGameclassParticle("graphic/particles/particle_character/character_void_skin1_torso1.p", GameclassName.VOID,
				"character_void_skin1", "character_torso", -60, 55, false);
		loadGameclassParticle("graphic/particles/particle_character/character_void_skin1_head1.p", GameclassName.VOID,
				"character_void_skin1", "character_head", -20, 30, false);
	}

	private static void loadWater() {

		// WATER skin0
		loadGameclassParticle("graphic/particles/particle_character/character_water_skin0_torso1.p",
				GameclassName.WATER, "character_water_skin0", "character_torso", -35, -50, true);
		loadGameclassParticle("graphic/particles/particle_character/character_water_skin0_head1.p", GameclassName.WATER,
				"character_water_skin0", "character_head", -50, 100, true);
		// loadGameclassParticle("graphic/particles/particle_character/character_water_skin0_hand1.p",
		// GameclassName.WATER,
		// "character_water_skin0", "character_hand_front", 0, 0, true);
		// loadGameclassParticle("graphic/particles/particle_character/character_water_skin0_hand1.p",
		// GameclassName.WATER,
		// "character_water_skin0", "character_hand_back", 0, 0, false);

		// WATER skin1
		loadGameclassParticle("graphic/particles/particle_character/character_water_skin1_head1.p", GameclassName.WATER,
				"character_water_skin1", "character_head", 42, 45, false);
		loadGameclassParticle("graphic/particles/particle_character/character_water_skin1_hand1.p", GameclassName.WATER,
				"character_water_skin1", "character_hand_front", 50, -15, false);
		loadGameclassParticle("graphic/particles/particle_character/character_water_skin1_hand1.p", GameclassName.WATER,
				"character_water_skin1", "character_hand_back", 50, -15, false);

		// WATER skin2
		loadGameclassParticle("graphic/particles/particle_character/character_water_skin2_hand1.p", GameclassName.WATER,
				"character_water_skin2", "character_hand_front", 40, -30, false);
		loadGameclassParticle("graphic/particles/particle_character/character_water_skin2_hand1.p", GameclassName.WATER,
				"character_water_skin2", "character_hand_back", 40, -30, false);
		loadGameclassParticle("graphic/particles/particle_character/character_water_skin2_shoulder1.p",
				GameclassName.WATER, "character_water_skin2", "character_shoulder_front", 80, -50, true);
		loadGameclassParticle("graphic/particles/particle_character/character_water_skin2_shoulder1.p",
				GameclassName.WATER, "character_water_skin2", "character_shoulder_back", 80, -50, true);

	}

	private static void loadWind() {
		// WIND skin0
		loadGameclassParticle("graphic/particles/particle_character/character_wind_skin0_torso1.p", GameclassName.WIND,
				"character_wind_skin0", "character_torso", -70, 0, true);
		loadGameclassParticle("graphic/particles/particle_character/character_wind_skin0_hand1.p", GameclassName.WIND,
				"character_wind_skin0", "character_weapon_front3", 0, 0, true);
		// WIND skin1
		loadGameclassParticle("graphic/particles/particle_character/character_wind_skin1_hand1.p", GameclassName.WIND,
				"character_wind_skin1", "character_hand_front", 40, 20, true);
		loadGameclassParticle("graphic/particles/particle_character/character_wind_skin1_hand1.p", GameclassName.WIND,
				"character_wind_skin1", "character_hand_back", 40, 20, true);
	}

	/**
	 * 
	 * @author lukassongajlo
	 * 
	 */
	private static class EmissionEffectPool extends Pool<EmissionParticleEffect> {

		private AEmissionDef emission_def;

		private short pool_id;

		/**
		 * holds all active particle effects in a map with their projectile_ids as keys.
		 */
		private HashMap<Short, EmissionParticleEffect> actives;

		private final EmissionParticleEffect prototype;

		public EmissionEffectPool(short poolID, final EmissionParticleEffect prototype) {
			super(10, 100);
			this.pool_id = poolID;
			this.actives = new HashMap<>();
			this.prototype = prototype;
		}

		@Override
		protected EmissionParticleEffect newObject() {
			return new EmissionParticleEffect(prototype);
		}

		/**
		 * 
		 * @param active
		 */
		private void setActives(boolean active) {
			Iterator<EmissionParticleEffect> itActives = actives.values().iterator();
			while (itActives.hasNext() == true)
				itActives.next().setActive(active);
		}

		private void removeInactives(float delta) {
			Iterator<EmissionParticleEffect> itActives = actives.values().iterator();
			EmissionParticleEffect particleEffect;
			while (itActives.hasNext() == true) {
				particleEffect = itActives.next();
				if (particleEffect.isActive() == false) {

					/*
					 * emission_def might be a combined one (e.g. pool_id = 65+93 = 6593)
					 */
					if (emission_def != null) {
						// fade out
						switch (emission_def.getParticleEffectCategory()) {
						case MOVEMENT:
							particleEffect.stopMovementEffect(delta);
							break;
						case PROJECTILE:
							particleEffect.fadeOut();
							break;
						}
					} else
						particleEffect.fadeOut();

					// remove
					if (particleEffect.isFadeOutDone() == true) {
						free(particleEffect);
						itActives.remove();
					}
				}
			}
		}

	}

	/**
	 * Returns the unique id of the passed particle effect. Sets multiplier and
	 * whether it should be draw behind or in the foreground
	 * 
	 * @param def          , don't pass <code>null</code>
	 * @param internalPath
	 * @param multiplier   , character ingame scale * multiplier. So the passed
	 *                     multiplier starts its scaling from world size.
	 * @param drawBehind
	 * @return returns a generated, unique emission pool id
	 */
	public static short setEmissionPool(AEmissionDef def, String internalPath, float multiplier, boolean drawBehind) {

		EmissionEffectPool resultPool = null;

		for (EmissionEffectPool emissionPool : emission_effect_pools.values()) {
			if (emissionPool.prototype.getPath().equals(internalPath) == true) {
				resultPool = emissionPool;
				break;
			}
		}

		// set emission def for later look up
		resultPool.emission_def = def;

		EmissionParticleEffect prototype = resultPool.prototype;
		prototype.scaleEffect(Shared.SCALE_CHARACTER_INGAME * multiplier);
		prototype.setDrawBehind(drawBehind);

		return resultPool.pool_id;
	}

	/**
	 * 
	 * @param poolID
	 * @return
	 */
	public static AEmissionDef getDef(int poolID) {
		return emission_effect_pools.get(poolID).emission_def;
	}

	/**
	 * Heavy-weight method. Iterates through all player of
	 * {@link LogicTier#WORLD_PLAYER_MAP} (and their sparse projectiles) and updates
	 * all particle effects of {@link LogicTier#CURRENT_EMISSION_EFFECTS} by those
	 * projectiles. It looks for new projectiles, if there are new ones the
	 * corresponding particle effect will be created. Also all existing particle
	 * effects will be updated (position, angle).
	 * 
	 * @param delta
	 * 
	 * @param myTeam , team of the own player
	 */
	public static void updateEmissionEffects(float delta) {

		/*
		 * set all active effects to <active = false> to sort them out if they are not
		 * in use
		 */
		for (EmissionEffectPool pool : emission_effect_pools.values())
			pool.setActives(false);

		// iterate through all clients...
		for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values())
			updateEffects(client.getPhysicalClient().isVisible(), client.getDrawableClient().sparse_projectiles,
					client.getMetaClient().team, client.getMetaClient().gameclass);

		// ... and buildings
		for (ABuilding building : LogicTier.BUILDINGS_MAP.values())
			updateEffects(false, building.getDrawableSparseProjectiles(), building.getTeam(), null);

		// ... and creeps
		for (OmniCreep creep : LogicTier.CREEP_LIVING_LIST)
			updateEffects(false, creep.getDrawableCreep().sparse_projectiles, null, null);

		// remove all inactives
		for (EmissionEffectPool pool : emission_effect_pools.values())
			pool.removeInactives(delta);

		// update tween-manager for fading out
		CustomParticleEffect.tween_manager.update(delta);

	}

	/**
	 * 
	 * @param isClientVisible,  only matters insofar as client projectiles are
	 *                          concerned.
	 * @param sparseProjectiles
	 * @param team
	 */
	private static void updateEffects(boolean isClientVisible, LinkedList<SparseProjectile> sparseProjectiles,
			Team team, Gameclass primaryGameclass) {
		if (sparseProjectiles == null)
			return;
		Iterator<SparseProjectile> itProjectiles = sparseProjectiles.iterator();
		// iterate through all inc projectiles
		while (itProjectiles.hasNext() == true) {
			SparseProjectile incProjectile = itProjectiles.next();

			EmissionEffectPool relatedPool = null;

			if (incProjectile.pool_ids[1] != -1 || incProjectile.pool_ids[2] != -1) {
				int combinedPoolID = getCombinedPoolID(incProjectile);
				relatedPool = emission_effect_pools.get(combinedPoolID);

				if (relatedPool == null) {

//					System.out.println("ParticleContainer.updateEffects()			POOL_ID[1] = "
//							+ incProjectile.pool_ids[1] + ", POOL_ID[2] = " + incProjectile.pool_ids[2]
//							+ ", combinedPoolID = " + combinedPoolID);

					Array<CombinedParticleEmitter> newEmitters = new Array<>();
					EmissionParticleEffect effectCopyPrimary = new EmissionParticleEffect(
							emission_effect_pools.get((int) incProjectile.pool_ids[0]).prototype);
					EmissionParticleEffect effectCopySecondary = null;
					EmissionParticleEffect effectCopyTertiar = null;

					if (incProjectile.pool_ids[1] != -1) {
						effectCopySecondary = new EmissionParticleEffect(
								emission_effect_pools.get((int) incProjectile.pool_ids[1]).prototype);

						float[] colorSecondary = emission_effect_pools.get((int) incProjectile.pool_ids[1]).emission_def
								.getColorGameclass();

						combineParticleEffects(effectCopyPrimary, effectCopySecondary, newEmitters, colorSecondary);
					}

					if (incProjectile.pool_ids[2] != -1) {
						effectCopyTertiar = new EmissionParticleEffect(
								emission_effect_pools.get((int) incProjectile.pool_ids[2]).prototype);

						float[] colorTertiar = emission_effect_pools.get((int) incProjectile.pool_ids[2]).emission_def
								.getColorGameclass();

						combineParticleEffects(effectCopyPrimary, effectCopyTertiar, newEmitters, colorTertiar);
					}
					for (CombinedParticleEmitter newEmitter : newEmitters) {

						Iterator<ParticleEmitter> it = effectCopyPrimary.getEmitters().iterator();
						int i = 0;
//						boolean starEmitter = false;
						while (it.hasNext() == true) {
							ParticleEmitter origEmitter = it.next();
							if (newEmitter.original_reference == origEmitter) {

								/*
								 * insert the combined emitter directly after the original emitter
								 */
								effectCopyPrimary.getEmitters().insert(i + 1, newEmitter);

								break;
							}
							i++;
						}

//						if (starEmitter == true)
						if (newEmitter.getName().charAt(0) == '*')
							effectCopyPrimary.getEmitters().removeValue(newEmitter.original_reference, true);
					}

					// make room from new hashemitter
					newEmitters.clear();

					if (effectCopySecondary != null)
						addHashEmitter(effectCopySecondary, newEmitters);
					if (effectCopyTertiar != null)
						addHashEmitter(effectCopyTertiar, newEmitters);

					System.out.println(
							"ParticleContainer.updateEffects()			size of #-Emitters = " + newEmitters.size);

					for (CombinedParticleEmitter newHashEmitter : newEmitters) {
						System.out.println("ParticleContainer.updateEffects()				newHashEmitter = "
								+ newHashEmitter.getName());

						/*
						 * hash emitters of secondary or tertiar emitters get painted with gameclass
						 * color of the primary
						 */
						if (primaryGameclass != null)
							if (primaryGameclass.getColor() != null)
								newHashEmitter.getTint().setColors(primaryGameclass.getColor());

						// add hash emitter
						effectCopyPrimary.getEmitters().add(newHashEmitter);
					}
					/*
					 * we modify the path a little bit - why? If we wouldn't modify the path, we
					 * would have been adding a new pool, with a new pool ID but same path which
					 * leads to a wrong behaviour when new emission definitions are created.
					 */
					effectCopyPrimary.setPath(effectCopyPrimary.getPath() + combinedPoolID);
					relatedPool = new EmissionEffectPool((short) combinedPoolID, effectCopyPrimary);

					emission_effect_pools.put(combinedPoolID, relatedPool);
				}
			} else {
				relatedPool = emission_effect_pools.get((int) incProjectile.pool_ids[0]);
			}

			// System.out.println(
			// "ParticleContainer.updateEffects() incProjectile, pool_ids[0]
			// = "
			// + incProjectile.pool_ids[0] + ", pool_ids[1] = "
			// + incProjectile.pool_ids[1]);

			EmissionParticleEffect correspondingEffect = relatedPool.actives.get(incProjectile.projectile_id);

			// obtain new particle effect from pool if necessary
			if (correspondingEffect == null) {

				// System.out.println("ParticleContainer.update()
				// OBTAIN! Effect = "
				// + correspondingEffect + ", projectile id = "
				// + incProjectile.projectile_id + ", pool id"
				// + incProjectile.pool_ids);

				correspondingEffect = relatedPool.obtain();

				// reset
				correspondingEffect.reset();
				// add particle effect
				relatedPool.actives.put(incProjectile.projectile_id, correspondingEffect);

			}
			AEmissionDef defEmission = getDef(incProjectile.pool_ids[0]);

			// update
			correspondingEffect.setActive(true);
			correspondingEffect.setPosition(incProjectile.pos.x, incProjectile.pos.y);

			if (defEmission.isRotatable() == true)
				correspondingEffect.setRotation(incProjectile.direction);

			// -----handle visibility
			DefProjectile defProjectile = defEmission.getDefProjectile();

			boolean isAttached = defProjectile.getAttachmentOption() != null;
			boolean sameTeam = LogicTier.VIEW_HANDLER.getTeam() == team;

			boolean isInsideViewCircles = false;

			if (defProjectile.getRectangleShape() != null) {
				float largestSide = Math.max(defProjectile.getRectangleShape().getWidth(),
						defProjectile.getRectangleShape().getHeight());
				isInsideViewCircles = LogicTier.VIEW_HANDLER.isInside(incProjectile.pos, largestSide * 0.5f) == true;
			} else {
				isInsideViewCircles = LogicTier.VIEW_HANDLER.isInside(incProjectile.pos,
						defProjectile.scale_end * Projectile.SCALE_PROJECTILE_SHAPE) == true;
			}
			/*
			 * initially false, and stays false if it's the same team & it is attached while
			 * being outside any view circles. It also stays false if it's not the same team
			 * and outside any view circles.
			 */
			boolean visible = false;

			if (isInsideViewCircles == true) {
				visible = true;
			} else
			// outside circles
			if (sameTeam == true)
				if (isAttached == false)
					visible = true;

			correspondingEffect.setVisible(visible);

			// -----handle sound
			TweenableSound sound = defEmission.getSound();
			if (sound == null)
				continue;

			if (incProjectile.sound_id == -1) {
				// start playing whether it's visible or not
				incProjectile.sound_id = sound.playWithoutTween();
			}
			if (visible == true)
				sound.updateVolume(incProjectile.sound_id, incProjectile.pos.x, incProjectile.pos.y);
			else {
				sound.updateVolume(incProjectile.sound_id, 0, 100000);
			}

		}

	}

	private static int getCombinedPoolID(SparseProjectile incProjectile) {
		String combinedID = incProjectile.pool_ids[0] + "";
		if (incProjectile.pool_ids[1] != -1 && incProjectile.pool_ids[2] != -1)
			combinedID += Math.min(incProjectile.pool_ids[1], incProjectile.pool_ids[2]) + ""
					+ Math.max(incProjectile.pool_ids[1], incProjectile.pool_ids[2]);
		else if (incProjectile.pool_ids[1] != -1)
			combinedID += incProjectile.pool_ids[1];
		else if (incProjectile.pool_ids[2] != -1)
			combinedID += incProjectile.pool_ids[2];

		return Integer.valueOf(combinedID);
	}

	/**
	 * Fills newEmitters with combined emitters.
	 * 
	 * Basically we take all information from the added effect, create an emitter
	 * copy of the primary, manipulate this emitter with these information of the
	 * added effect and write the result into newEmitters. After this method has
	 * proceeded we add the result (newEmitters) to the primary effect.
	 * 
	 * This method does not touch the two effect copies.
	 * 
	 * @param effectPrimary , copy of the primary effect
	 * @param effectAdded   , copy of the added effect
	 * @param newEmitters
	 * @param colorAdded
	 */
	private static void combineParticleEffects(EmissionParticleEffect effectPrimary, EmissionParticleEffect effectAdded,
			Array<CombinedParticleEmitter> newEmitters, float[] colorAdded) {

		// + for trails
		matchEmitters('+', effectPrimary, effectAdded, newEmitters, colorAdded);

		// # bigger ones (ice block, slime globe, big stone)
		// matchEmitters('#', copy1, copy2, newEmitters, originalEmitters);

		// * means color
		matchEmitters('*', effectPrimary, effectAdded, newEmitters, colorAdded);

	}

	/**
	 * 
	 * @param effectAdded
	 * @param newEmitters
	 */
	private static void addHashEmitter(EmissionParticleEffect effectAdded, Array<CombinedParticleEmitter> newEmitters) {

		for (int i = 0; i < effectAdded.getEmitters().size; i++) {
			ParticleEmitter emitterAdded = effectAdded.getEmitters().get(i);

			if (emitterAdded.getName().charAt(0) != '#')
				// emitterAdded has not the proper sign
				continue;

			// emitterAdded has the proper sign and copied emitter gets added
			newEmitters.add(new CombinedParticleEmitter(emitterAdded));

		}

	}

	/**
	 * 
	 * @param sign
	 * @param effectPrimary
	 * @param effectAdded
	 * @param newEmitters
	 */
	private static void matchEmitters(char sign, EmissionParticleEffect effectPrimary,
			EmissionParticleEffect effectAdded, Array<CombinedParticleEmitter> newEmitters, float[] colorAdded) {

		ParticleEmitter emitterAdded = null;
		int lastIndexOfAdded = 0;

		for (int i = 0; i < effectPrimary.getEmitters().size; i++) {
			ParticleEmitter emitterPrimary = effectPrimary.getEmitters().get(i);

//			System.out.println("ParticleContainer.matchEmitters() NAME PRIMÄR = " + emitterPrimary.getName());

			if (emitterPrimary.getName().charAt(0) != sign)
				// emitterPrimary has not the proper sign
				continue;

			// emitterPrimary has the proper sign
			for (int j = lastIndexOfAdded; j < effectAdded.getEmitters().size; j++) {
				ParticleEmitter potentialEmitter = effectAdded.getEmitters().get(j);

				if (potentialEmitter.getName().charAt(0) == sign) {
					/*
					 * set emitterAdded and in the next iteration we try to get another emitterAdded
					 * by increasing lastIndexOfAdded
					 */
					emitterAdded = potentialEmitter;
					lastIndexOfAdded = j;
					lastIndexOfAdded++;
					break;
				}

			}

			// create copy to modify it
			CombinedParticleEmitter emitterCopyPrimary = new CombinedParticleEmitter(emitterPrimary);

			if (sign == '*') {
				// set tint
				if (colorAdded != null)
					emitterCopyPrimary.getTint().setColors(colorAdded);

				newEmitters.add(emitterCopyPrimary);
				continue;
			}

			if (emitterAdded == null)
				return;

			/*
			 * sign != '*'
			 */

			// set sprite
			Array<Sprite> spriteArray = new Array<>();
			for (Sprite sprite : emitterAdded.getSprites())
				spriteArray.add(new Sprite(sprite));

			emitterCopyPrimary.setSprites(spriteArray);

			// set spritemode
			if (emitterAdded.getSpriteMode() == SpriteMode.random)
				emitterCopyPrimary.setSpriteMode(SpriteMode.random);

			// set additive
			emitterCopyPrimary.setAdditive(emitterAdded.isAdditive());

			/*
			 * TODO, könnte Probleme geben. Muss noch getestet werden. set continuous
			 */
			// emitterCopy1.setContinuous(emitterCopy2.isContinuous());

			// set tint
			emitterCopyPrimary.getTint().setColors(emitterAdded.getTint().getColors());

			// set transparency

			boolean allowTransparency = false;

			for (int j = 0; j < emitterAdded.getTransparency().getScaling().length; j++) {
				if (MathUtils.isZero(emitterAdded.getTransparency().getScaling()[j]) == false) {
					allowTransparency = true;
					break;
				}
			}

			if (allowTransparency == true) {
				emitterCopyPrimary.getTransparency().setScaling(emitterAdded.getTransparency().getScaling());
				emitterCopyPrimary.getTransparency().setTimeline(emitterAdded.getTransparency().getTimeline());
			}

//				emitterCopyPrimary.getTransparency().setLow(emitterAdded.getTransparency().getLowMin(),
//						emitterAdded.getTransparency().getLowMax());
//				emitterCopyPrimary.getTransparency().setHigh(emitterAdded.getTransparency().getHighMin(),
//						emitterAdded.getTransparency().getHighMax());

			// set rotation
			emitterCopyPrimary.getRotation().setLow(emitterAdded.getRotation().getLowMin(),
					emitterAdded.getRotation().getLowMax());
			emitterCopyPrimary.getRotation().setHigh(emitterAdded.getRotation().getHighMin(),
					emitterAdded.getRotation().getHighMax());

			/*
			 * set size, nicht gut. Wenn primär ganz klein ist und sekundär groß dann ragen
			 * die effekte raus
			 */
			// TODO durchschnitt

			// float lowMinAvg = 1.5f * 0.5f *
			// (emitterCopy1.getXScale().getLowMin()
			// + emitterCopy2.getXScale().getLowMin());
			// float lowMaxAvg = 1.5f * 0.5f *
			// (emitterCopy1.getXScale().getLowMax()
			// + emitterCopy2.getXScale().getLowMax());
			// float highMinAvg = 1.5f * 0.5f *
			// (emitterCopy1.getXScale().getHighMin()
			// + emitterCopy2.getXScale().getHighMin());
			// float highMaxAvg = 1.5f * 0.5f *
			// (emitterCopy1.getXScale().getHighMax()
			// + emitterCopy2.getXScale().getHighMax());
			// emitterCopy1.getXScale().setLow(lowMinAvg,
			// lowMaxAvg);
			// emitterCopy1.getXScale().setHigh(highMinAvg,
			// highMaxAvg);

			/*
			 * take secondary effect
			 */
			// emitterCopy1.getXScale().setLow(emitterCopy2.getXScale().getLowMin(),
			// emitterCopy2.getXScale().getLowMax());
			// emitterCopy1.getXScale().setHigh(emitterCopy2.getXScale().getHighMin(),
			// emitterCopy2.getXScale().getHighMax());
			/*
			 * take primary effect
			 */
			emitterCopyPrimary.getXScale().setLow(1.5f * emitterCopyPrimary.getXScale().getLowMin(),
					1.5f * emitterCopyPrimary.getXScale().getLowMax());
			emitterCopyPrimary.getXScale().setHigh(1.5f * emitterCopyPrimary.getXScale().getHighMin(),
					1.5f * emitterCopyPrimary.getXScale().getHighMax());

			// set emission rate
			// emitterCopy1.getEmission().setLow(
			// emitterCopy2.getEmission().getLowMin(),
			// emitterCopy2.getEmission().getLowMax());
			// emitterCopy1.getEmission().setHigh(
			// emitterCopy2.getEmission().getHighMin(),
			// emitterCopy2.getEmission().getHighMax());

			newEmitters.add(emitterCopyPrimary);

		}

	}

	/**
	 * Draw all active particle effects
	 * 
	 * @param batch
	 * @param delta
	 * @param drawBehind , if <code>true</code> all particle effects that were
	 *                   declared as to be drawn behind will be drawn. Too draw all
	 *                   foreground particle effects pass <code>false</code>
	 */
	public static void drawEmissionEffects(ShaderPolyBatch batch, float delta, boolean drawBehind) {
		for (EmissionEffectPool pool : emission_effect_pools.values()) {
			for (EmissionParticleEffect particleEffect : pool.actives.values()) {

				if (particleEffect.isDrawBehind() != drawBehind)
					continue;
				// only update if effect is not visible
				if (particleEffect.isVisible() == false)
					particleEffect.update(delta);
				else if (particleEffect.isActive() == true) {
					particleEffect.draw(batch, delta);
				} else {
					/*
					 * emission_def is null when it is a combined one (e.g. pool_id = 65+93 = 6593)
					 */
					if (pool.emission_def != null) {
						switch (pool.emission_def.getParticleEffectCategory()) {
						case MOVEMENT:
							particleEffect.draw(batch, delta);
							break;
						case PROJECTILE:
							particleEffect.draw(batch);
						}
					} else
						particleEffect.draw(batch);
				}
			}
		}

	}

	private static class CombinedParticleEmitter extends ParticleEmitter {
		private ParticleEmitter original_reference;

		/**
		 * 
		 * @param copy
		 * @param indexInStructure
		 */
		public CombinedParticleEmitter(ParticleEmitter copy) {
			super(copy);
			original_reference = copy;
		}
	}

}