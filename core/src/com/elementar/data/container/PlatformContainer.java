package com.elementar.data.container;

import com.elementar.logic.util.Shared.CategoryFunctionality;
import com.elementar.logic.util.Shared.CategorySize;
import com.elementar.logic.util.lists.CategoryArrayList;

/**
 * See {@link AStringCategoryContainer} for further information. We have
 * {@link CategorySize#SMALL} and {@link CategorySize#LARGE} sprites
 * 
 * @author lukassongajlo
 * 
 */
public class PlatformContainer extends AStringCategoryContainer {

	protected CategoryArrayList<String>
	// nonblur
			small_slide,
			small_walk, small_nonwalk, large_slide,
			large_walk,
			large_nonwalk;

	@Override
	protected void createPots() {
		small_slide = new CategoryArrayList<String>(
				CategoryFunctionality.SLIDE, CategorySize.SMALL);
		small_walk = new CategoryArrayList<String>(CategoryFunctionality.WALK,
				CategorySize.SMALL);
		small_nonwalk = new CategoryArrayList<String>(
				CategoryFunctionality.NON_WALK, CategorySize.SMALL);

		large_slide = new CategoryArrayList<String>(
				CategoryFunctionality.SLIDE, CategorySize.LARGE);
		large_walk = new CategoryArrayList<String>(CategoryFunctionality.WALK,
				CategorySize.LARGE);
		large_nonwalk = new CategoryArrayList<String>(
				CategoryFunctionality.NON_WALK, CategorySize.LARGE);

	}

	@Override
	protected void addStrings() {
		large_nonwalk.add("world_platform_large_nonwalkable1");
		large_nonwalk.add("world_platform_large_nonwalkable2");
		large_nonwalk.add("world_platform_large_nonwalkable3");
		large_nonwalk.add("world_platform_large_nonwalkable4");
//
		large_slide.add("world_platform_large_slidable1");
		large_slide.add("world_platform_large_slidable2");
		large_slide.add("world_platform_large_slidable3");
		large_slide.add("world_platform_large_slidable4");
//		large_slide.add("world_platform_large_slidable5");
//
		large_walk.add("world_platform_large_walkable1");
		large_walk.add("world_platform_large_walkable2");
		large_walk.add("world_platform_large_walkable3");
		large_walk.add("world_platform_large_walkable4");
//		large_walk.add("world_platform_large_walkable5");

		small_nonwalk.add("world_platform_small_nonwalkable1");
		small_nonwalk.add("world_platform_small_nonwalkable2");
		small_nonwalk.add("world_platform_small_nonwalkable3");
		small_nonwalk.add("world_platform_small_nonwalkable4");
//		small_nonwalk.add("world_platform_small_nonwalkable5");
//		small_nonwalk.add("world_platform_small_nonwalkable6");
//		small_nonwalk.add("world_platform_small_nonwalkable7");
//		small_nonwalk.add("world_platform_small_nonwalkable8");

		small_slide.add("world_platform_small_slidable1");
		small_slide.add("world_platform_small_slidable2");
		small_slide.add("world_platform_small_slidable3");
		small_slide.add("world_platform_small_slidable4");
//		small_slide.add("world_platform_small_slidable5");
//		small_slide.add("world_platform_small_slidable6");
//		small_slide.add("world_platform_small_slidable7");
//		small_slide.add("world_platform_small_slidable8");

		small_walk.add("world_platform_small_walkable1");
		small_walk.add("world_platform_small_walkable2");
		small_walk.add("world_platform_small_walkable3");
		small_walk.add("world_platform_small_walkable4");
//		small_walk.add("world_platform_small_walkable5");
//		small_walk.add("world_platform_small_walkable6");
//		small_walk.add("world_platform_small_walkable7");
//		small_walk.add("world_platform_small_walkable8");
		
	}

	@Override
	protected void addPots() {
		all_pots.add(small_slide);
		all_pots.add(small_walk);
		all_pots.add(small_nonwalk);

		all_pots.add(large_slide);
		all_pots.add(large_walk);
		all_pots.add(large_nonwalk);
	}

}
