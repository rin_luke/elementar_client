package com.elementar.data.container;

import static com.elementar.data.container.SettingsLoader.loadAudioVolume;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.elementar.data.DataTier;
import com.elementar.data.container.util.TweenableMusic;
import com.elementar.data.container.util.TweenableMusicAccessor;
import com.elementar.data.container.util.TweenableSound;
import com.elementar.data.container.util.TweenableSoundAccessor;
import com.elementar.gui.modules.options.AudioModule;
import com.elementar.gui.modules.options.OptionsModule;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

/**
 * Loads and updates audio-data. Will be loaded from the {@link DataTier} class
 * at the beginning and updated within the {@link AudioModule} class in the
 * {@link OptionsModule}. Usage: For sounds you call just the 'play()' method of
 * the specific sound object.
 * 
 * 
 * @author lukassongajlo
 *
 */
public class AudioData {

	// music
	public static Music music_gui2, music_world_calm1;

	// ambient
	public static TweenableMusic global_ambient_tweenable;

	// effects
	public static Sound dirt_explosion1, player_interrupt1, gui_btn_click_light1, gui_btn_click_heavy1,
			gui_combo_feedback, player_teleport1, player_spawn, global_climbing, global_jump, global_doublejump,
			global_landing, global_receive_damage, global_sliding, global_walk, global_crystal_collected, global_death,
			global_combo_feedback;

	// ice
	public static Sound ice_skill2_e1, ice_skill2_e2, ice_skill2_e3, ice_skill1_e2, ice_skill1_e3, ice_skill1_e4,
			ice_skill0_e1, ice_skill0_e2;
	// leaf
	public static Sound leaf_skill2_e1, leaf_skill2_e2, leaf_skill2_e3, leaf_skill2_e5, leaf_skill2_e6, leaf_skill1_e1,
			leaf_skill1_e2, leaf_skill1_e3, leaf_skill1_e4, leaf_skill0_e1, leaf_skill0_e2;
	// light
	public static Sound light_skill2_e1, light_skill2_e2, light_skill2_e3, light_skill2_e4, light_skill2_e5,
			light_skill2_e6, light_skill1_e1, light_skill1_e2, light_skill0_e1, light_skill0_e2, light_skill0_e3;
	// lightning
	public static Sound lightning_skill2_e1, lightning_skill2_e2, lightning_skill1_e1, lightning_skill1_e2,
			lightning_skill0_e1, lightning_skill0_e2;
	// sand
	public static Sound sand_skill2_e1, sand_skill2_e2, sand_skill1_e0, sand_skill1_e2, sand_skill1_e3, sand_skill1_e4,
			sand_skill0_e1, sand_skill0_e2, sand_skill0_e3;
	// stone
	public static Sound stone_skill2_e0, stone_skill2_e1, stone_skill2_e3, stone_skill2_e4, stone_skill2_e8,
			stone_skill1_e0, stone_skill1_e1, stone_skill0_e1, stone_skill0_e2;
	// toxic
	public static Sound toxic_skill2_e1, toxic_skill2_e2, toxic_skill2_e4, toxic_skill1_e2, toxic_skill1_e4,
			toxic_skill0_e1, toxic_skill0_e2, toxic_skill0_e3;
	// void
	public static Sound void_skill2_e1, void_skill2_e2, void_skill2_e3, void_skill2_e4, void_skill2_e5, void_skill2_e6,
			void_skill2_e7, void_skill1_e1, void_skill1_e2, void_skill1_e3, void_skill0_e1, void_skill0_e2,
			void_skill0_e5, void_skill0_e6;
	// water
	public static Sound water_skill2_e1, water_skill2_e2, water_skill2_e3, global_water_resurrection;
	public static Sound water_skill1_e1, water_skill1_e2, water_skill1_e3, water_skill1_e4, water_skill1_e5,
			water_skill1_e6;
	public static Sound water_skill0_e1, water_skill0_e2, water_skill0_e3;
	// wind
	public static Sound wind_skill2_e1, wind_skill2_e2, wind_skill1_e1, wind_skill0_e1, wind_skill0_e2;

	private static Sound global_silent_sound;

	/*
	 * for continuous playing music (music and ambient) there is one "current" field
	 * to pause it when the player makes changes in audio options
	 */
	public static Music current_music;
	public static TweenableMusic current_ambient;

	public static float general_volume, music_volume, effects_volume, ambient_volume;

	private static TweenManager tween_manager = new TweenManager();

	public enum ClickSoundType {
		HEAVY, LIGHT
	}

	/**
	 * Returns <code>true</code> if the passed volume value is greater than 0
	 * 
	 * @param volume
	 * @return
	 */
	public static boolean isOn(float volume) {
		return volume > 0;
	}

	public static void loadServer() {

	}

	public static void load() {
		AssetManager manager = DataTier.ASSET_MANAGER;
		manager.load("audio/music/menu/music_gui2.mp3", Music.class);
		manager.load("audio/music/world/music_world_calm1.mp3", Music.class);
		manager.load("audio/ambient/global_ambient.mp3", Music.class);

		// silent sound as placeholder
		manager.load("audio/effects/global_silent_sound.mp3", Sound.class);

		manager.load("audio/effects/skills/global_dirt_explosion1.mp3", Sound.class);
		// ice
		manager.load("audio/effects/skills/ice_skill2_e1.wav", Sound.class);
		manager.load("audio/effects/skills/ice_skill2_e2.wav", Sound.class);
		manager.load("audio/effects/skills/ice_skill2_e3.wav", Sound.class);
		manager.load("audio/effects/skills/ice_skill1_e2.wav", Sound.class);
		manager.load("audio/effects/skills/ice_skill1_e3.wav", Sound.class);
		manager.load("audio/effects/skills/ice_skill1_e4.wav", Sound.class);
		manager.load("audio/effects/skills/ice_skill0_e1.wav", Sound.class);
		manager.load("audio/effects/skills/ice_skill0_e2.wav", Sound.class);
		// leaf
		manager.load("audio/effects/skills/leaf_skill2_e1.wav", Sound.class);
		manager.load("audio/effects/skills/leaf_skill2_e2.wav", Sound.class);
		manager.load("audio/effects/skills/leaf_skill2_e3.wav", Sound.class);
		manager.load("audio/effects/skills/leaf_skill2_e5.wav", Sound.class);
		manager.load("audio/effects/skills/leaf_skill2_e6.wav", Sound.class);
		manager.load("audio/effects/skills/leaf_skill1_e1.wav", Sound.class);
		manager.load("audio/effects/skills/leaf_skill1_e2.wav", Sound.class);
		manager.load("audio/effects/skills/leaf_skill1_e3.wav", Sound.class);
		manager.load("audio/effects/skills/leaf_skill1_e4.wav", Sound.class);
		manager.load("audio/effects/skills/leaf_skill0_e1.wav", Sound.class);
		manager.load("audio/effects/skills/leaf_skill0_e2.wav", Sound.class);
		// light
		manager.load("audio/effects/skills/light_skill2_e1.wav", Sound.class);
		manager.load("audio/effects/skills/light_skill2_e2.wav", Sound.class);
		manager.load("audio/effects/skills/light_skill2_e3.wav", Sound.class);
		manager.load("audio/effects/skills/light_skill2_e4.wav", Sound.class);
		manager.load("audio/effects/skills/light_skill2_e5.wav", Sound.class);
		manager.load("audio/effects/skills/light_skill2_e6.wav", Sound.class);
		manager.load("audio/effects/skills/light_skill1_e1.wav", Sound.class);
		manager.load("audio/effects/skills/light_skill1_e2.wav", Sound.class);
		manager.load("audio/effects/skills/light_skill0_e1.wav", Sound.class);
		manager.load("audio/effects/skills/light_skill0_e2.wav", Sound.class);
		manager.load("audio/effects/skills/light_skill0_e3.wav", Sound.class);
		// lightning
		manager.load("audio/effects/skills/lightning_skill2_e1.wav", Sound.class);
		manager.load("audio/effects/skills/lightning_skill2_e2.wav", Sound.class);
		manager.load("audio/effects/skills/lightning_skill1_e1.wav", Sound.class);
		manager.load("audio/effects/skills/lightning_skill1_e2.wav", Sound.class);
		manager.load("audio/effects/skills/lightning_skill0_e1.wav", Sound.class);
		manager.load("audio/effects/skills/lightning_skill0_e2.wav", Sound.class);
		// sand
		manager.load("audio/effects/skills/sand_skill2_e1.wav", Sound.class);
		manager.load("audio/effects/skills/sand_skill2_e2.wav", Sound.class);
		manager.load("audio/effects/skills/sand_skill1_e0.wav", Sound.class);
		manager.load("audio/effects/skills/sand_skill1_e2.wav", Sound.class);
		manager.load("audio/effects/skills/sand_skill1_e3.wav", Sound.class);
		manager.load("audio/effects/skills/sand_skill1_e4.wav", Sound.class);
		manager.load("audio/effects/skills/sand_skill0_e1.wav", Sound.class);
		manager.load("audio/effects/skills/sand_skill0_e2.wav", Sound.class);
		manager.load("audio/effects/skills/sand_skill0_e3.wav", Sound.class);
		// stone
		manager.load("audio/effects/skills/stone_skill2_e0.wav", Sound.class);
		manager.load("audio/effects/skills/stone_skill2_e1.wav", Sound.class);
		manager.load("audio/effects/skills/stone_skill2_e3.wav", Sound.class);
		manager.load("audio/effects/skills/stone_skill2_e4.wav", Sound.class);
		manager.load("audio/effects/skills/stone_skill2_e8.wav", Sound.class);
		manager.load("audio/effects/skills/stone_skill1_e0.wav", Sound.class);
		manager.load("audio/effects/skills/stone_skill1_e1.wav", Sound.class);
		manager.load("audio/effects/skills/stone_skill0_charge.wav", Sound.class);
		manager.load("audio/effects/skills/stone_skill0_e2.wav", Sound.class);
		// toxic
		manager.load("audio/effects/skills/toxic_skill2_e1.wav", Sound.class);
		manager.load("audio/effects/skills/toxic_skill2_e2_e4.wav", Sound.class);
		manager.load("audio/effects/skills/toxic_skill1_e2.wav", Sound.class);
		manager.load("audio/effects/skills/toxic_skill1_e4.wav", Sound.class);
		manager.load("audio/effects/skills/toxic_skill0_e1.wav", Sound.class);
		manager.load("audio/effects/skills/toxic_skill0_e2.wav", Sound.class);
		manager.load("audio/effects/skills/toxic_skill0_e3.wav", Sound.class);
		// void
		manager.load("audio/effects/skills/void_skill2_e1.wav", Sound.class);
		manager.load("audio/effects/skills/void_skill2_e2.wav", Sound.class);
		manager.load("audio/effects/skills/void_skill2_e3.wav", Sound.class);
		manager.load("audio/effects/skills/void_skill2_e4_e6.wav", Sound.class);
		manager.load("audio/effects/skills/void_skill2_e5_e7.wav", Sound.class);
		manager.load("audio/effects/skills/void_skill1_e1.wav", Sound.class);
		manager.load("audio/effects/skills/void_skill1_e2.wav", Sound.class);
		manager.load("audio/effects/skills/void_skill1_e3.wav", Sound.class);
		manager.load("audio/effects/skills/void_skill0_e1.wav", Sound.class);
		manager.load("audio/effects/skills/void_skill0_e2.wav", Sound.class);
		manager.load("audio/effects/skills/void_skill0_e5.wav", Sound.class);
		manager.load("audio/effects/skills/void_skill0_e6.wav", Sound.class);
		// water
		manager.load("audio/effects/skills/water_skill2_e1.wav", Sound.class);
		manager.load("audio/effects/skills/water_skill2_e2.wav", Sound.class);
		manager.load("audio/effects/skills/water_skill2_e3.wav", Sound.class);
		manager.load("audio/effects/skills/water_skill1_e1.wav", Sound.class);
		manager.load("audio/effects/skills/water_skill1_e2.wav", Sound.class);
		manager.load("audio/effects/skills/water_skill1_e3.wav", Sound.class);
		manager.load("audio/effects/skills/water_skill1_e4.wav", Sound.class);
		manager.load("audio/effects/skills/water_skill1_e5.wav", Sound.class);
		manager.load("audio/effects/skills/water_skill1_e6.wav", Sound.class);
		manager.load("audio/effects/skills/water_skill0_e1.wav", Sound.class);
		manager.load("audio/effects/skills/water_skill0_e2.wav", Sound.class);
		manager.load("audio/effects/skills/water_skill0_e3.wav", Sound.class);
		// wind
		manager.load("audio/effects/skills/wind_skill2_e1.wav", Sound.class);
		manager.load("audio/effects/skills/wind_skill2_e2.wav", Sound.class);
		manager.load("audio/effects/skills/wind_skill1_e1.wav", Sound.class);
		manager.load("audio/effects/skills/wind_skill0_e1.wav", Sound.class);
		manager.load("audio/effects/skills/wind_skill0_e2.wav", Sound.class);

		manager.load("audio/effects/gui/gui_btn_click_heavy1.mp3", Sound.class);
		manager.load("audio/effects/gui/gui_btn_click_light1.mp3", Sound.class);
		manager.load("audio/effects/gui/gui_combo_feedback.wav", Sound.class);

		manager.load("audio/effects/world/global_crystal_collected.mp3", Sound.class);
		manager.load("audio/effects/world/global_jump.mp3", Sound.class);
		manager.load("audio/effects/world/global_doublejump.mp3", Sound.class);
		manager.load("audio/effects/world/global_landing.mp3", Sound.class);
		manager.load("audio/effects/world/global_receive_damage.mp3", Sound.class);
		manager.load("audio/effects/world/global_sliding.mp3", Sound.class);
		manager.load("audio/effects/world/global_climbing.mp3", Sound.class);
		manager.load("audio/effects/world/global_sliding.mp3", Sound.class);
		manager.load("audio/effects/world/global_walk.mp3", Sound.class);
		manager.load("audio/effects/world/global_death.wav", Sound.class);
		manager.load("audio/effects/world/player_interrupt1.mp3", Sound.class);
		manager.load("audio/effects/world/player_teleport1.mp3", Sound.class);
		manager.load("audio/effects/world/player_spawn.mp3", Sound.class);

	}

	public static void setFields() {
		setVolumes();
		setAssets();
	}

	private static void setVolumes() {
		float arr[] = loadAudioVolume();
		general_volume = arr[0];
		music_volume = arr[1];
		effects_volume = arr[2];
		ambient_volume = arr[3];
	}

	private static void setAssets() {
		AssetManager manager = DataTier.ASSET_MANAGER;
		music_gui2 = manager.get("audio/music/menu/music_gui2.mp3", Music.class);
		music_gui2.setLooping(true);

		music_world_calm1 = manager.get("audio/music/world/music_world_calm1.mp3", Music.class);
		music_world_calm1.setLooping(true);

		Music globalAmbient = manager.get("audio/ambient/global_ambient.mp3", Music.class);
		globalAmbient.setLooping(true);
		globalAmbient.setVolume(0);
		global_ambient_tweenable = new TweenableMusic(globalAmbient);

		dirt_explosion1 = manager.get("audio/effects/skills/global_dirt_explosion1.mp3", Sound.class);

		// ice
		ice_skill2_e1 = manager.get("audio/effects/skills/ice_skill2_e1.wav", Sound.class);
		ice_skill2_e2 = manager.get("audio/effects/skills/ice_skill2_e2.wav", Sound.class);
		ice_skill2_e3 = manager.get("audio/effects/skills/ice_skill2_e3.wav", Sound.class);
		ice_skill1_e2 = manager.get("audio/effects/skills/ice_skill1_e2.wav", Sound.class);
		ice_skill1_e3 = manager.get("audio/effects/skills/ice_skill1_e3.wav", Sound.class);
		ice_skill1_e4 = manager.get("audio/effects/skills/ice_skill1_e4.wav", Sound.class);
		ice_skill0_e1 = manager.get("audio/effects/skills/ice_skill0_e1.wav", Sound.class);
		ice_skill0_e2 = manager.get("audio/effects/skills/ice_skill0_e2.wav", Sound.class);
		// leaf
		leaf_skill2_e1 = manager.get("audio/effects/skills/leaf_skill2_e1.wav", Sound.class);
		leaf_skill2_e2 = manager.get("audio/effects/skills/leaf_skill2_e2.wav", Sound.class);
		leaf_skill2_e3 = manager.get("audio/effects/skills/leaf_skill2_e3.wav", Sound.class);
		leaf_skill2_e5 = manager.get("audio/effects/skills/leaf_skill2_e5.wav", Sound.class);
		leaf_skill2_e6 = manager.get("audio/effects/skills/leaf_skill2_e6.wav", Sound.class);
		leaf_skill1_e1 = manager.get("audio/effects/skills/leaf_skill1_e1.wav", Sound.class);
		leaf_skill1_e2 = manager.get("audio/effects/skills/leaf_skill1_e2.wav", Sound.class);
		leaf_skill1_e3 = manager.get("audio/effects/skills/leaf_skill1_e3.wav", Sound.class);
		leaf_skill1_e4 = manager.get("audio/effects/skills/leaf_skill1_e4.wav", Sound.class);
		leaf_skill0_e1 = manager.get("audio/effects/skills/leaf_skill0_e1.wav", Sound.class);
		leaf_skill0_e2 = manager.get("audio/effects/skills/leaf_skill0_e2.wav", Sound.class);
		// light
		light_skill2_e1 = manager.get("audio/effects/skills/light_skill2_e1.wav", Sound.class);
		light_skill2_e2 = manager.get("audio/effects/skills/light_skill2_e2.wav", Sound.class);
		light_skill2_e3 = manager.get("audio/effects/skills/light_skill2_e3.wav", Sound.class);
		light_skill2_e4 = manager.get("audio/effects/skills/light_skill2_e4.wav", Sound.class);
		light_skill2_e5 = manager.get("audio/effects/skills/light_skill2_e5.wav", Sound.class);
		light_skill2_e6 = manager.get("audio/effects/skills/light_skill2_e6.wav", Sound.class);
		light_skill1_e1 = manager.get("audio/effects/skills/light_skill1_e1.wav", Sound.class);
		light_skill1_e2 = manager.get("audio/effects/skills/light_skill1_e2.wav", Sound.class);
		light_skill0_e1 = manager.get("audio/effects/skills/light_skill0_e1.wav", Sound.class);
		light_skill0_e2 = manager.get("audio/effects/skills/light_skill0_e2.wav", Sound.class);
		light_skill0_e3 = manager.get("audio/effects/skills/light_skill0_e3.wav", Sound.class);
		// lightning
		lightning_skill2_e1 = manager.get("audio/effects/skills/lightning_skill2_e1.wav", Sound.class);
		lightning_skill2_e2 = manager.get("audio/effects/skills/lightning_skill2_e2.wav", Sound.class);
		lightning_skill1_e1 = manager.get("audio/effects/skills/lightning_skill1_e1.wav", Sound.class);
		lightning_skill1_e2 = manager.get("audio/effects/skills/lightning_skill1_e2.wav", Sound.class);
		lightning_skill0_e1 = manager.get("audio/effects/skills/lightning_skill0_e1.wav", Sound.class);
		lightning_skill0_e2 = manager.get("audio/effects/skills/lightning_skill0_e2.wav", Sound.class);
		// sand
		sand_skill2_e1 = manager.get("audio/effects/skills/sand_skill2_e1.wav", Sound.class);
		sand_skill2_e2 = manager.get("audio/effects/skills/sand_skill2_e2.wav", Sound.class);
		sand_skill1_e0 = manager.get("audio/effects/skills/sand_skill1_e0.wav", Sound.class);
		sand_skill1_e2 = manager.get("audio/effects/skills/sand_skill1_e2.wav", Sound.class);
		sand_skill1_e3 = manager.get("audio/effects/skills/sand_skill1_e3.wav", Sound.class);
		sand_skill1_e4 = manager.get("audio/effects/skills/sand_skill1_e4.wav", Sound.class);
		sand_skill0_e1 = manager.get("audio/effects/skills/sand_skill0_e1.wav", Sound.class);
		sand_skill0_e2 = manager.get("audio/effects/skills/sand_skill0_e2.wav", Sound.class);
		sand_skill0_e3 = manager.get("audio/effects/skills/sand_skill0_e3.wav", Sound.class);
		// stone
		stone_skill2_e0 = manager.get("audio/effects/skills/stone_skill2_e0.wav", Sound.class);
		stone_skill2_e1 = manager.get("audio/effects/skills/stone_skill2_e1.wav", Sound.class);
		stone_skill2_e3 = manager.get("audio/effects/skills/stone_skill2_e3.wav", Sound.class);
		stone_skill2_e4 = manager.get("audio/effects/skills/stone_skill2_e4.wav", Sound.class);
		stone_skill2_e8 = manager.get("audio/effects/skills/stone_skill2_e8.wav", Sound.class);
		stone_skill1_e0 = manager.get("audio/effects/skills/stone_skill1_e0.wav", Sound.class);
		stone_skill1_e1 = manager.get("audio/effects/skills/stone_skill1_e1.wav", Sound.class);
		stone_skill0_e1 = manager.get("audio/effects/skills/stone_skill0_charge.wav", Sound.class);
		stone_skill0_e2 = manager.get("audio/effects/skills/stone_skill0_e2.wav", Sound.class);
		// toxic
		toxic_skill2_e1 = manager.get("audio/effects/skills/toxic_skill2_e1.wav", Sound.class);
		toxic_skill2_e2 = manager.get("audio/effects/skills/toxic_skill2_e2_e4.wav", Sound.class);
		toxic_skill2_e4 = manager.get("audio/effects/skills/toxic_skill2_e2_e4.wav", Sound.class);
		toxic_skill1_e2 = manager.get("audio/effects/skills/toxic_skill1_e2.wav", Sound.class);
		toxic_skill1_e4 = manager.get("audio/effects/skills/toxic_skill1_e4.wav", Sound.class);
		toxic_skill0_e1 = manager.get("audio/effects/skills/toxic_skill0_e1.wav", Sound.class);
		toxic_skill0_e2 = manager.get("audio/effects/skills/toxic_skill0_e2.wav", Sound.class);
		toxic_skill0_e3 = manager.get("audio/effects/skills/toxic_skill0_e3.wav", Sound.class);
		// void
		void_skill2_e1 = manager.get("audio/effects/skills/void_skill2_e1.wav", Sound.class);
		void_skill2_e2 = manager.get("audio/effects/skills/void_skill2_e2.wav", Sound.class);
		void_skill2_e3 = manager.get("audio/effects/skills/void_skill2_e3.wav", Sound.class);
		void_skill2_e4 = manager.get("audio/effects/skills/void_skill2_e4_e6.wav", Sound.class);
		void_skill2_e5 = manager.get("audio/effects/skills/void_skill2_e5_e7.wav", Sound.class);
		void_skill2_e6 = manager.get("audio/effects/skills/void_skill2_e4_e6.wav", Sound.class);
		void_skill2_e7 = manager.get("audio/effects/skills/void_skill2_e5_e7.wav", Sound.class);
		void_skill1_e1 = manager.get("audio/effects/skills/void_skill1_e1.wav", Sound.class);
		void_skill1_e2 = manager.get("audio/effects/skills/void_skill1_e2.wav", Sound.class);
		void_skill1_e3 = manager.get("audio/effects/skills/void_skill1_e3.wav", Sound.class);
		void_skill0_e1 = manager.get("audio/effects/skills/void_skill0_e1.wav", Sound.class);
		void_skill0_e2 = manager.get("audio/effects/skills/void_skill0_e2.wav", Sound.class);
		void_skill0_e5 = manager.get("audio/effects/skills/void_skill0_e5.wav", Sound.class);
		void_skill0_e6 = manager.get("audio/effects/skills/void_skill0_e6.wav", Sound.class);
		// water
		water_skill2_e1 = manager.get("audio/effects/skills/water_skill2_e1.wav", Sound.class);
		water_skill2_e2 = manager.get("audio/effects/skills/water_skill2_e2.wav", Sound.class);
		water_skill2_e3 = manager.get("audio/effects/skills/water_skill2_e3.wav", Sound.class);
		water_skill1_e1 = manager.get("audio/effects/skills/water_skill1_e1.wav", Sound.class);
		water_skill1_e2 = manager.get("audio/effects/skills/water_skill1_e2.wav", Sound.class);
		water_skill1_e3 = manager.get("audio/effects/skills/water_skill1_e3.wav", Sound.class);
		water_skill1_e4 = manager.get("audio/effects/skills/water_skill1_e4.wav", Sound.class);
		water_skill1_e5 = manager.get("audio/effects/skills/water_skill1_e5.wav", Sound.class);
		water_skill1_e6 = manager.get("audio/effects/skills/water_skill1_e6.wav", Sound.class);
		water_skill0_e1 = manager.get("audio/effects/skills/water_skill0_e1.wav", Sound.class);
		water_skill0_e2 = manager.get("audio/effects/skills/water_skill0_e2.wav", Sound.class);
		water_skill0_e3 = manager.get("audio/effects/skills/water_skill0_e3.wav", Sound.class);
		// wind
		wind_skill2_e1 = manager.get("audio/effects/skills/wind_skill2_e1.wav", Sound.class);
		wind_skill2_e2 = manager.get("audio/effects/skills/wind_skill2_e2.wav", Sound.class);
		wind_skill1_e1 = manager.get("audio/effects/skills/wind_skill1_e1.wav", Sound.class);
		wind_skill0_e1 = manager.get("audio/effects/skills/wind_skill0_e1.wav", Sound.class);
		wind_skill0_e2 = manager.get("audio/effects/skills/wind_skill0_e2.wav", Sound.class);

		// gui
		gui_btn_click_light1 = manager.get("audio/effects/gui/gui_btn_click_light1.mp3", Sound.class);
		gui_btn_click_heavy1 = manager.get("audio/effects/gui/gui_btn_click_heavy1.mp3", Sound.class);
		gui_combo_feedback = manager.get("audio/effects/gui/gui_combo_feedback.wav", Sound.class);

		// global
		global_crystal_collected = manager.get("audio/effects/world/global_crystal_collected.mp3", Sound.class);
		global_jump = manager.get("audio/effects/world/global_jump.mp3", Sound.class);
		global_doublejump = manager.get("audio/effects/world/global_doublejump.mp3", Sound.class);
		global_landing = manager.get("audio/effects/world/global_landing.mp3", Sound.class);
		global_receive_damage = manager.get("audio/effects/world/global_receive_damage.mp3", Sound.class);
		global_sliding = manager.get("audio/effects/world/global_sliding.mp3", Sound.class);
		global_climbing = manager.get("audio/effects/world/global_climbing.mp3", Sound.class);
		global_walk = manager.get("audio/effects/world/global_walk.mp3", Sound.class);
		global_death = manager.get("audio/effects/world/global_death.wav", Sound.class);

		player_interrupt1 = manager.get("audio/effects/world/player_interrupt1.mp3", Sound.class);
		player_teleport1 = manager.get("audio/effects/world/player_teleport1.mp3", Sound.class);
		player_spawn = manager.get("audio/effects/world/player_spawn.mp3", Sound.class);

		global_silent_sound = manager.get("audio/effects/global_silent_sound.mp3", Sound.class);
		// override chosen sound fields with silent
		overrideSoundsFields();

	}

	private static void overrideSoundsFields() {
		dirt_explosion1 = global_silent_sound;

		player_interrupt1 = global_silent_sound;
		player_teleport1 = global_silent_sound;
		player_spawn = global_silent_sound;
	}

	/**
	 * Sets {@link #current_music} and {@link #current_ambient} fields and updates
	 * them depending on audio settings
	 * 
	 * @param music   , pass <code>null</code> if there should be no music
	 * @param ambient , pass <code>null</code> if there should be no ambient sound
	 */
	public static void set(Music music, TweenableMusic ambient) {
		// 1. pause
		pauseMusic(current_music);

		if (current_ambient != null) {
			pauseMusic(current_ambient.getMusicObj());
		}

		// 2. set
		current_music = music;
		current_ambient = ambient;
		// 3. update
		update();
	}

	public static void updateTweenManager(float delta) {
		/*
		 * we take a determined delta to avoid jumps that happened by high deltas (for
		 * example while a root module is changing)
		 */
		tween_manager.update(60f / 1000f);
	}

	/**
	 * Will be called after changes of audio options within the {@link AudioModule}
	 * class.
	 */
	public static void update() {
		// update volume
		music_gui2.setVolume(general_volume * music_volume);
		music_world_calm1.setVolume(general_volume * music_volume);
		global_ambient_tweenable.setVolumeBegin(general_volume * ambient_volume);
		global_ambient_tweenable.setTweenedVolume(general_volume * ambient_volume);

		// update continuous played music
		if (isOn(general_volume) == true && isOn(music_volume) == true)
			playMusic(current_music);
		else
			pauseMusic(current_music);

		if (current_ambient != null)
			// ambient
			if (isOn(general_volume) == true && isOn(ambient_volume) == true) {
				playMusicWithFadeIn(current_ambient, 6f);
			} else {
				pauseMusic(current_ambient.getMusicObj());
			}

		// first update to avoid noise at the beginning
		tween_manager.update(0.01f);
	}

	/**
	 * Returns the id of a new created sound instance. Note the passed parameter is
	 * like a blue print and each call of the 'play' method will create a new sound
	 * instance of this blue print. If you call this method twice a new instance is
	 * created and will be played concurrently to already existing instances.
	 * 
	 * @param effect
	 * @return id of the created instance
	 */
	public static long playEffect(Sound effect) {
		return playEffect(effect, general_volume * effects_volume);
	}

	/**
	 * 
	 * @param effect
	 * @param volume
	 * @return id of the created instance
	 */
	public static long playEffect(Sound effect, float volume) {
		return effect.play(volume);
	}

	/**
	 * Stops the passed effect specified by an id
	 * 
	 * @param effect
	 * @param id
	 */
	public static void stopEffect(Sound effect, long id) {
		if (effect != null)
			effect.stop(id);
	}

	private static void pauseMusic(Music music) {
		if (music != null && music.isPlaying() == true)
			music.pause();
	}

	private static void playMusic(Music music) {
		if (music != null && music.isPlaying() == false)
			music.play();
	}

	private static void playMusicWithFadeIn(TweenableMusic tweenMusic, float tweenDuration) {
		if (tweenMusic != null)
			if (tweenMusic.getMusicObj().isPlaying() == false) {
				// start tweening volume
				Tween.to(tweenMusic, TweenableMusicAccessor.FADE_IN, tweenDuration).target(1)
						.ease(TweenEquations.easeOutQuad).setCallback(new TweenCallback() {

							@Override
							public void onEvent(int arg0, BaseTween<?> arg1) {
								tweenMusic.setTweening(false);
							}
						}).start(tween_manager);
				tweenMusic.getMusicObj().setVolume(1f);
				tweenMusic.getMusicObj().play();
			}
	}

	/**
	 * 
	 * @param tweenMusic
	 * @param tweenDuration
	 */
	public static void pauseMusicWithFadeOut(TweenableMusic tweenMusic, float tweenDuration) {
		if (tweenMusic != null)
			if (tweenMusic.getMusicObj().isPlaying() == true) {
				// start tweening volume
				if (tweenMusic.isTweening() == false)

					Tween.to(tweenMusic, TweenableMusicAccessor.FADE_OUT, tweenDuration).target(0)
							.ease(TweenEquations.easeOutQuad).setCallback(new TweenCallback() {

								@Override
								public void onEvent(int arg0, BaseTween<?> arg1) {
									tweenMusic.setTweening(false);
									tweenMusic.getMusicObj().pause();
								}
							}).start(tween_manager);
			}
	}

	public static void loopSoundWithFadeIn(TweenableSound tweenSound, float tweenDuration) {
		if (tweenSound != null)
			if (tweenSound.isPlaying() == false) {
				// start tweening volume
				if (tweenSound.isTweening() == false) {

					long id = tweenSound.getSoundObj().loop(general_volume * effects_volume);

					tweenSound.setVolumeBegin(general_volume * effects_volume);
					tweenSound.setPlaying(true);
					tweenSound.setSoundID(id);

					Tween.to(tweenSound, TweenableSoundAccessor.FADE_IN, tweenDuration).target(1)
							.ease(TweenEquations.easeOutQuad).setCallback(new TweenCallback() {

								@Override
								public void onEvent(int arg0, BaseTween<?> arg1) {
									tweenSound.setTweening(false);
								}
							}).start(tween_manager);
				}
			}
	}

	public static void pauseSoundWithFadeOut(TweenableSound tweenSound, float tweenDuration) {
		if (tweenSound != null)
			if (tweenSound.isPlaying() == true) {
				// start tweening volume
				if (tweenSound.isTweening() == false)
					Tween.to(tweenSound, TweenableSoundAccessor.FADE_OUT, tweenDuration).target(0)
							.ease(TweenEquations.easeOutQuad).setCallback(new TweenCallback() {

								@Override
								public void onEvent(int arg0, BaseTween<?> arg1) {
									tweenSound.setTweening(false);
									tweenSound.setPlaying(false);

									stopEffect(tweenSound.getSoundObj(), tweenSound.getSoundID());
								}
							}).start(tween_manager);
			}

	}

}
