package com.elementar.data.container;

import com.elementar.logic.util.Shared.CategoryFunctionality;
import com.elementar.logic.util.Shared.CategorySize;
import com.elementar.logic.util.lists.CategoryArrayList;

/**
 * See {@link AStringCategoryContainer} for further information.
 * 
 * @author lukassongajlo
 * 
 */
public class VegetationContainer extends AStringCategoryContainer {

	protected CategoryArrayList<String> small_slide, small_walk, small_nonwalk,
			medium_walk, medium_nonwalk;

	@Override
	protected void createPots() {
		small_slide = new CategoryArrayList<String>(
				CategoryFunctionality.SLIDE, CategorySize.SMALL);
		small_walk = new CategoryArrayList<String>(CategoryFunctionality.WALK,
				CategorySize.SMALL);
		small_nonwalk = new CategoryArrayList<String>(
				CategoryFunctionality.NON_WALK, CategorySize.SMALL);

		medium_walk = new CategoryArrayList<String>(CategoryFunctionality.WALK,
				CategorySize.MEDIUM);
		medium_nonwalk = new CategoryArrayList<String>(
				CategoryFunctionality.NON_WALK, CategorySize.MEDIUM);

	}

	@Override
	protected void addStrings() {
		small_walk.add("world_vegetation_small_walkable1");
		small_walk.add("world_vegetation_small_walkable2");
		small_walk.add("world_vegetation_small_walkable3");
		small_walk.add("world_vegetation_small_walkable4");
//		small_walk.add("world_vegetation_small_walkable5");
//		small_walk.add("world_vegetation_small_walkable6");

		small_slide.add("world_vegetation_small_slidable1");
		small_slide.add("world_vegetation_small_slidable2");
		small_slide.add("world_vegetation_small_slidable3");
		small_slide.add("world_vegetation_small_slidable4");
//		small_slide.add("world_vegetation_small_slidable5");

		small_nonwalk.add("world_vegetation_small_nonwalkable1");
		small_nonwalk.add("world_vegetation_small_nonwalkable2");
//		small_nonwalk.add("world_vegetation_small_nonwalkable3");
//		small_nonwalk.add("world_vegetation_small_nonwalkable4");
//		small_nonwalk.add("world_vegetation_small_nonwalkable5");

		medium_walk.add("world_vegetation_medium_walkable1");
		medium_walk.add("world_vegetation_medium_walkable2");
//		medium_walk.add("world_vegetation_medium_walkable3");
//		medium_walk.add("world_vegetation_medium_walkable4");
//		medium_walk.add("world_vegetation_medium_walkable5");
//		medium_walk.add("world_vegetation_medium_walkable6");
//		medium_walk.add("world_vegetation_medium_walkable7");

		medium_nonwalk.add("world_vegetation_medium_nonwalkable1");
		medium_nonwalk.add("world_vegetation_medium_nonwalkable2");
//		medium_nonwalk.add("world_vegetation_medium_nonwalkable3");
//		medium_nonwalk.add("world_vegetation_medium_nonwalkable4");
//		medium_nonwalk.add("world_vegetation_medium_nonwalkable5");
	}

	@Override
	protected void addPots() {
		all_pots.add(small_slide);
		all_pots.add(small_walk);
		all_pots.add(small_nonwalk);

		all_pots.add(medium_walk);
		all_pots.add(medium_nonwalk);

	}

}
