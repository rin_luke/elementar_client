package com.elementar.data.container;

import com.badlogic.gdx.scenes.scene2d.ui.CheckBox.CheckBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar.ProgressBarStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox.SelectBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Slider.SliderStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.elementar.gui.util.SharedColors;

/**
 * Load this class after FontData.load(); and after StaticGraphic.load(); Each
 * style variable has the same name-structure:
 * "type"_"schriftschnitt"_"color"_"scale"_style
 * 
 * @author lukassongajlo
 * 
 */
public class StyleContainer {

	public static TextButtonStyle button_bold_18_style, button_bold_20_style, button_bold_30_style,
			button_bold_ivory1_30_bordered_style;

	/**
	 * fontColor of LabelStyle objects can be set directly because they never
	 * changed by user interaction
	 */
	public static LabelStyle label_keymodule_titles_style, label_medium_whitepure_20_style,
			label_medium_whitepure_20_time_style, label_bold_ivory2_50_bordered_blue7_style,
			label_bold_ivory2_40_bordered_blue7_style, label_bold_whitepure_40_style, label_bold_white_30_style,
			label_bold_ivory2_25_bordered_blue7_style, label_bold_whitepure_18_style, label_semibold_ivory2_14_style,
			label_bold_ivory2_18_bordered_style, label_bold_ivory2_18_bordered_healthmana_style,
			label_medium_ivory3_18_style, label_error_message_style;

	public static LabelStyle label_bold_blue1_20_style, label_bold_blue3_25_style, label_bold_ivory1_20_style, label_bold_ivory2_20_style,
			label_bold_ivory3_20_style;

	public static TextFieldStyle textfield_bold_18_style, tooltip_style, textfield_medium_ivory2_20_style,
			textfield_bold_ivory1_22_style;

	public static ScrollPaneStyle scrollpane_server_style, scrollpane_client_style;

	public static ProgressBarStyle progress_bar_style;

	public static CheckBoxStyle checkbox_style;

	public static SliderStyle slider_enabled_style, slider_disabled_style;

	public static SelectBoxStyle selectbox_style_width2;

	public static void setFields() {

		button_bold_18_style = new TextButtonStyle();
		button_bold_18_style.font = FontData.font_bold_18;

		button_bold_20_style = new TextButtonStyle();
		button_bold_20_style.font = FontData.font_bold_20;

		button_bold_30_style = new TextButtonStyle();
		button_bold_30_style.font = FontData.font_bold_30;

		button_bold_ivory1_30_bordered_style = new TextButtonStyle();
		button_bold_ivory1_30_bordered_style.font = FontData.font_bold_30_ivory1_bordered;

		label_bold_ivory2_18_bordered_style = new LabelStyle();
		label_bold_ivory2_18_bordered_style.font = FontData.font_bold_18_bordered;
		label_bold_ivory2_18_bordered_style.fontColor = SharedColors.IVORY_2;

		label_bold_blue1_20_style = new LabelStyle();
		label_bold_blue1_20_style.font = FontData.font_bold_20;
		label_bold_blue1_20_style.fontColor = SharedColors.BLUE_1;
		
		label_bold_blue3_25_style = new LabelStyle();
		label_bold_blue3_25_style.font = FontData.font_bold_25;
		label_bold_blue3_25_style.fontColor = SharedColors.BLUE_3;

		label_bold_ivory1_20_style = new LabelStyle();
		label_bold_ivory1_20_style.font = FontData.font_bold_20;
		label_bold_ivory1_20_style.fontColor = SharedColors.IVORY_1;

		label_bold_ivory2_20_style = new LabelStyle();
		label_bold_ivory2_20_style.font = FontData.font_bold_20;
		label_bold_ivory2_20_style.fontColor = SharedColors.IVORY_2;

		label_bold_ivory3_20_style = new LabelStyle();
		label_bold_ivory3_20_style.font = FontData.font_bold_20;
		label_bold_ivory3_20_style.fontColor = SharedColors.IVORY_3;

		textfield_medium_ivory2_20_style = new TextFieldStyle();
		textfield_medium_ivory2_20_style.font = FontData.font_medium_20;
		textfield_medium_ivory2_20_style.fontColor = SharedColors.IVORY_2;

		textfield_bold_ivory1_22_style = new TextFieldStyle();
		textfield_bold_ivory1_22_style.font = FontData.font_bold_22;
		textfield_bold_ivory1_22_style.fontColor = SharedColors.IVORY_1;

		label_bold_ivory2_18_bordered_healthmana_style = new LabelStyle();
		label_bold_ivory2_18_bordered_healthmana_style.font = FontData.font_bold_18_bordered_healthmana;
		label_bold_ivory2_18_bordered_healthmana_style.fontColor = SharedColors.IVORY_2;

		label_semibold_ivory2_14_style = new LabelStyle();
		label_semibold_ivory2_14_style.font = FontData.font_semibold_14;
		label_semibold_ivory2_14_style.fontColor = SharedColors.IVORY_2;

		label_medium_ivory3_18_style = new LabelStyle();
		label_medium_ivory3_18_style.font = FontData.font_medium_18;
		label_medium_ivory3_18_style.fontColor = SharedColors.IVORY_3;

		label_bold_white_30_style = new LabelStyle();
		label_bold_white_30_style.font = FontData.font_bold_30;
		label_bold_white_30_style.fontColor = SharedColors.IVORY_2;

		label_bold_ivory2_50_bordered_blue7_style = new LabelStyle();
		label_bold_ivory2_50_bordered_blue7_style.font = FontData.font_bold_50_bordered;
		label_bold_ivory2_50_bordered_blue7_style.fontColor = SharedColors.IVORY_2;

		label_bold_ivory2_40_bordered_blue7_style = new LabelStyle();
		label_bold_ivory2_40_bordered_blue7_style.font = FontData.font_bold_40_bordered;
		label_bold_ivory2_40_bordered_blue7_style.fontColor = SharedColors.IVORY_2;

		label_bold_ivory2_25_bordered_blue7_style = new LabelStyle();
		label_bold_ivory2_25_bordered_blue7_style.font = FontData.font_bold_25_bordered;
		label_bold_ivory2_25_bordered_blue7_style.fontColor = SharedColors.IVORY_2;

		label_bold_whitepure_40_style = new LabelStyle();
		label_bold_whitepure_40_style.font = FontData.font_bold_40;
		label_bold_whitepure_40_style.fontColor = SharedColors.IVORY_2;

		label_error_message_style = new LabelStyle();
		label_error_message_style.font = FontData.font_semibold_26_bordered_blue8;
		label_error_message_style.fontColor = SharedColors.RED_1;

		label_keymodule_titles_style = new LabelStyle();
		label_keymodule_titles_style.font = FontData.font_bold_20;
		label_keymodule_titles_style.fontColor = SharedColors.BLUE_4;

		label_bold_whitepure_18_style = new LabelStyle();
		label_bold_whitepure_18_style.font = FontData.font_bold_18;
		label_bold_whitepure_18_style.fontColor = SharedColors.IVORY_2;

		label_medium_whitepure_20_style = new LabelStyle();
		label_medium_whitepure_20_style.font = FontData.font_medium_20;
		label_medium_whitepure_20_style.fontColor = SharedColors.IVORY_2;

		label_medium_whitepure_20_time_style = new LabelStyle();
		label_medium_whitepure_20_time_style.font = FontData.font_medium_20_time;
		label_medium_whitepure_20_time_style.fontColor = SharedColors.IVORY_2;

		textfield_bold_18_style = new TextFieldStyle();
		textfield_bold_18_style.font = FontData.font_bold_18;
		textfield_bold_18_style.cursor = StaticGraphic.getDrawable("gui_textfield_cursor");
		textfield_bold_18_style.selection = StaticGraphic.getDrawable("gui_textfield_selection");

		tooltip_style = new TextFieldStyle();
		tooltip_style.font = FontData.font_bold_18_tooltip;
		tooltip_style.cursor = StaticGraphic.getDrawable("gui_textfield_cursor");
		tooltip_style.selection = StaticGraphic.getDrawable("gui_textfield_selection");

		scrollpane_server_style = new ScrollPaneStyle();
		scrollpane_server_style.vScroll = StaticGraphic.getDrawable("gui_server_vscroll_bg");
		scrollpane_server_style.vScrollKnob = StaticGraphic.getDrawable("gui_server_vscroll_knob");

		scrollpane_client_style = new ScrollPaneStyle();
		scrollpane_client_style.vScroll = StaticGraphic.getDrawable("gui_vscroll_bg");
		scrollpane_client_style.vScrollKnob = StaticGraphic.getDrawable("gui_vscroll_knob");

		progress_bar_style = new ProgressBarStyle(StaticGraphic.getDrawable("slider"),
				StaticGraphic.getDrawable("knob"));

		checkbox_style = new CheckBoxStyle();
		checkbox_style.checkboxOff = StaticGraphic.getDrawable("gui_icon_checkbox_unchecked");
		checkbox_style.checkboxOn = StaticGraphic.getDrawable("gui_icon_checkbox_checked");
		// font mustn't be null
		checkbox_style.font = FontData.font_bold_30;

		slider_enabled_style = new SliderStyle();
		slider_enabled_style.background = StaticGraphic.getDrawable("slider");
		slider_enabled_style.knob = StaticGraphic.getDrawable("knob");

		slider_disabled_style = new SliderStyle();
		slider_disabled_style.background = StaticGraphic.getDrawable("slider_hover");
		slider_disabled_style.knob = StaticGraphic.getDrawable("knob_hover");

	}

}
