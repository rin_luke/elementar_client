package com.elementar.data.container;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.codedisaster.steamworks.SteamInventory;
import com.elementar.data.container.util.GameclassSkin;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.data.container.util.GameclassSkin.SkinAvailability;

public class GameclassSkinContainer {

	public static HashMap<Integer, GameclassSkin> SKINS = new HashMap<>();

	public static void loadSkins(List<Integer> itemDefs, SteamInventory inventory) {

		SKINS.clear();

		for (Integer itemDefID : itemDefs) {

			GameclassSkin skin = new GameclassSkin();

			// set item def id
			skin.setItemDefID(itemDefID);

			// set description
			skin.setDescription(getStringByID(itemDefID, "description", inventory));

			/*
			 * the technical description follows a certain convention rule, e.g.
			 * "ice_skin1". If the tech. description violates this rule we are not using
			 * this item as a skin.
			 */
			String techDesc = getStringByID(itemDefID, "tech_description", inventory);

			skin.setTechnicalDescription(techDesc);

			String[] techDescArr = techDesc.split("_");
			if (techDescArr.length > 0) {
				// set gameclass name

				// find skin
				for (GameclassName gameclassName : GameclassName.values()) {

					if (gameclassName.name().toLowerCase().equals(techDescArr[0]))
						skin.setGameclassName(gameclassName);

				}

				if (skin.getGameclassName() == null) {
					skin = null;
					continue;
				}
			} else {
				skin = null;
				continue;

			}

			skin.setIndex(Integer.parseInt(String.valueOf(techDescArr[1].charAt(techDescArr[1].length() - 1))));

			if (skin.getIndex() == 0)
				skin.setAvailability(SkinAvailability.AVAILABLE);
			else
				skin.setAvailability(SkinAvailability.NOT_PAID);

			SKINS.put(itemDefID, skin);

		}

	}

	public static void loadSkinsWithoutSteam() {

		SKINS.clear();

		int i = 0;
		for (GameclassName name : GameclassName.values()) {
			
			GameclassSkin skin = new GameclassSkin();
			skin.setAvailability(SkinAvailability.AVAILABLE);
			skin.setEquipped(true);
			skin.setGameclassName(name);
			skin.setTechnicalDescription(name.toString().toLowerCase()+"_skin0");
			SKINS.put(i, skin);
			i++;
		}
		
	}

	private static String getStringByID(Integer itemDefID, String property, SteamInventory inventory) {

		final List<String> values = new ArrayList<>();
		inventory.getItemDefinitionProperty(itemDefID, property, values);

		String result = "";

		for (String s : values)
			result += s;

		return result;
	}

}
