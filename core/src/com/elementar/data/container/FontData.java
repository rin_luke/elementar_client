
package com.elementar.data.container;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader.FreeTypeFontLoaderParameter;
import com.elementar.data.DataTier;
import com.elementar.gui.modules.ingame.APlayerUIModule;
import com.elementar.gui.util.SharedColors;

/**
 * Loads font-data. Will be loaded from the {@link DataTier} class at the
 * beginning. Provides also methods to create new font objects.
 * 
 * @author lukassongajlo
 * 
 */
public class FontData {

	public static BitmapFont font_bold_50_bordered, font_bold_40_bordered, font_bold_40, font_bold_30,
			font_bold_30_ivory1_bordered, font_bold_22, font_bold_25_bordered, font_bold_25, font_bold_20, font_bold_18,
			font_bold_18_tooltip, font_bold_18_bordered, font_bold_18_bordered_healthmana;

	public static BitmapFont font_medium_18, font_medium_20, font_medium_20_time;

	public static BitmapFont font_semibold_italic_17_bordered, font_semibold_14, font_semibold_26_bordered_blue8;

	/**
	 * for damage and healing number inside the world
	 */
	public static BitmapFont font_bold_22_ivory3_bordered, font_bold_22_red2_bordered, font_bold_22_green3_bordered,
			font_bold_22_blue1_bordered;

	public static void load() {
		AssetManager manager = DataTier.ASSET_MANAGER;

		FileHandleResolver resolver = new InternalFileHandleResolver();
		manager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
		manager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));

		FreeTypeFontLoaderParameter params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_50_bordered.ttf";
		params.fontParameters.size = 50;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		params.fontParameters.spaceX = -1;
		params.fontParameters.borderColor = SharedColors.BLUE_7;
		params.fontParameters.borderWidth = 4.5f;
		manager.load("font/Rawline-Bold_50_bordered.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_40_bordered.ttf";
		params.fontParameters.size = 40;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		params.fontParameters.spaceX = -1;
		params.fontParameters.borderColor = SharedColors.BLUE_7;
		params.fontParameters.borderWidth = 4.5f;
		manager.load("font/Rawline-Bold_40_bordered.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_25_bordered.ttf";
		params.fontParameters.size = (int) (25 * APlayerUIModule.SCALE_UI);
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		params.fontParameters.spaceX = -1;
		params.fontParameters.borderColor = SharedColors.BLUE_7;
		params.fontParameters.borderWidth = 4.5f;
		manager.load("font/Rawline-Bold_25_bordered.ttf", BitmapFont.class, params);

		float borderWidthNumbers = 3f;
		// for ivory numbers (dealt damage)
		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_22_ivory_bordered.ttf";
		params.fontParameters.size = 22;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		params.fontParameters.spaceX = -1;
		params.fontParameters.borderColor = SharedColors.BLUE_7;
		params.fontParameters.borderWidth = borderWidthNumbers;
		manager.load("font/Rawline-Bold_22_ivory_bordered.ttf", BitmapFont.class, params);

		// for red numbers (received damage)
		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_22_red_bordered.ttf";
		params.fontParameters.size = 22;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		params.fontParameters.spaceX = -1;
		params.fontParameters.borderColor = SharedColors.BLUE_7;
		params.fontParameters.borderWidth = borderWidthNumbers;
		manager.load("font/Rawline-Bold_22_red_bordered.ttf", BitmapFont.class, params);

		// for green numbers (healing)
		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_22_green_bordered.ttf";
		params.fontParameters.size = 22;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		params.fontParameters.spaceX = -1;
		params.fontParameters.borderColor = SharedColors.BLUE_7;
		params.fontParameters.borderWidth = borderWidthNumbers;
		manager.load("font/Rawline-Bold_22_green_bordered.ttf", BitmapFont.class, params);

		// for blue numbers (mana)
		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_22_blue_bordered.ttf";
		params.fontParameters.size = 22;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		params.fontParameters.spaceX = -1;
		params.fontParameters.borderColor = SharedColors.BLUE_7;
		params.fontParameters.borderWidth = borderWidthNumbers;
		manager.load("font/Rawline-Bold_22_blue_bordered.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_40.ttf";
		params.fontParameters.size = 40;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-Bold_40.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_30.ttf";
		params.fontParameters.size = 30;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-Bold_30.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_25.ttf";
		params.fontParameters.size = 25;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-Bold_25.ttf", BitmapFont.class, params);
		
		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_22.ttf";
		params.fontParameters.size = 22;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-Bold_22.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_20.ttf";
		params.fontParameters.size = 20;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-Bold_20.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_18.ttf";
		params.fontParameters.size = 18;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-Bold_18.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Medium_20.ttf";
		params.fontParameters.size = 20;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-Medium_20.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Medium_20_time.ttf";
		params.fontParameters.size = (int) (20 * APlayerUIModule.SCALE_UI);
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-Medium_20_time.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Medium_18.ttf";
		params.fontParameters.size = 18;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-Medium_18.ttf", BitmapFont.class, params);

		// rolefilter font
		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-SemiBold_14.ttf";
		params.fontParameters.size = 14;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-SemiBold_14.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-SemiBoldItalic_17_bordered.ttf";
		params.fontParameters.size = 17;
		params.fontParameters.spaceX = -1;
		params.fontParameters.borderColor = SharedColors.BLACK;
		params.fontParameters.borderWidth = 1.5f;
		params.fontParameters.genMipMaps = true;
		params.fontParameters.minFilter = TextureFilter.MipMapLinearLinear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-SemiBoldItalic_17_bordered.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_18_bordered.ttf";
		params.fontParameters.size = 18;
		params.fontParameters.spaceX = -1;
		params.fontParameters.borderColor = SharedColors.BLUE_8;
		params.fontParameters.borderWidth = 1.5f;
		// params.fontParameters.shadowColor = SharedColors.IVORY_2;
		// params.fontParameters.shadowOffsetX = 2;
		// params.fontParameters.shadowOffsetY = 2;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-Bold_18_bordered.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_30_ivory_bordered.ttf";
		params.fontParameters.size = 30;
		params.fontParameters.spaceX = -1;
		params.fontParameters.borderColor = SharedColors.BLUE_8;
		params.fontParameters.borderWidth = 3f;
		// params.fontParameters.shadowColor = SharedColors.IVORY_2;
		// params.fontParameters.shadowOffsetX = 2;
		// params.fontParameters.shadowOffsetY = 2;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-Bold_30_ivory_bordered.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-Bold_18_bordered_healthmana.ttf";
		params.fontParameters.size = (int) (18 * APlayerUIModule.SCALE_UI);
		params.fontParameters.spaceX = -1;
		params.fontParameters.borderColor = SharedColors.BLUE_8;
		params.fontParameters.borderWidth = 1.5f;
		// params.fontParameters.shadowColor = SharedColors.IVORY_2;
		// params.fontParameters.shadowOffsetX = 2;
		// params.fontParameters.shadowOffsetY = 2;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-Bold_18_bordered_healthmana.ttf", BitmapFont.class, params);

		params = new FreeTypeFontLoaderParameter();
		params.fontFileName = "font/Rawline-SemiBold_26_bordered.ttf";
		params.fontParameters.size = 26;
		params.fontParameters.spaceX = -1;
		params.fontParameters.borderColor = SharedColors.BLUE_8;
		params.fontParameters.borderWidth = 1.5f;
		params.fontParameters.minFilter = TextureFilter.Linear;
		params.fontParameters.magFilter = TextureFilter.Linear;
		manager.load("font/Rawline-SemiBold_26_bordered.ttf", BitmapFont.class, params);

	}

	public static void setFields() {

		AssetManager manager = DataTier.ASSET_MANAGER;

		font_bold_50_bordered = manager.get("font/Rawline-Bold_50_bordered.ttf", BitmapFont.class);
		font_bold_40_bordered = manager.get("font/Rawline-Bold_40_bordered.ttf", BitmapFont.class);
		font_bold_40 = manager.get("font/Rawline-Bold_40.ttf", BitmapFont.class);
		font_bold_30 = manager.get("font/Rawline-Bold_30.ttf", BitmapFont.class);
		font_bold_30_ivory1_bordered = manager.get("font/Rawline-Bold_30_ivory_bordered.ttf", BitmapFont.class);
		font_bold_30_ivory1_bordered.setColor(SharedColors.IVORY_1);
		font_bold_25_bordered = manager.get("font/Rawline-Bold_25_bordered.ttf", BitmapFont.class);
		font_bold_25 = manager.get("font/Rawline-Bold_25.ttf", BitmapFont.class);
		font_bold_22 = manager.get("font/Rawline-Bold_22.ttf", BitmapFont.class);
		font_bold_22_ivory3_bordered = manager.get("font/Rawline-Bold_22_ivory_bordered.ttf", BitmapFont.class);
		font_bold_22_ivory3_bordered.setColor(SharedColors.IVORY_1);
		font_bold_22_red2_bordered = manager.get("font/Rawline-Bold_22_red_bordered.ttf", BitmapFont.class);
		font_bold_22_red2_bordered.setColor(SharedColors.RED_2);
		font_bold_22_blue1_bordered = manager.get("font/Rawline-Bold_22_blue_bordered.ttf", BitmapFont.class);
		font_bold_22_blue1_bordered.setColor(SharedColors.BLUE_1);
		font_bold_22_green3_bordered = manager.get("font/Rawline-Bold_22_green_bordered.ttf", BitmapFont.class);
		font_bold_22_green3_bordered.setColor(SharedColors.GREEN_1);
		font_bold_20 = manager.get("font/Rawline-Bold_20.ttf", BitmapFont.class);
		font_bold_18_tooltip = manager.get("font/Rawline-Bold_18.ttf", BitmapFont.class);
		font_bold_18 = manager.get("font/Rawline-Bold_18.ttf", BitmapFont.class);
		// enable colors
		font_bold_18.getData().markupEnabled = true;

		font_medium_18 = manager.get("font/Rawline-Medium_18.ttf", BitmapFont.class);
		font_medium_20 = manager.get("font/Rawline-Medium_20.ttf", BitmapFont.class);
		font_medium_20_time = manager.get("font/Rawline-Medium_20_time.ttf", BitmapFont.class);

		font_semibold_14 = manager.get("font/Rawline-SemiBold_14.ttf", BitmapFont.class);
		font_semibold_14.getData().markupEnabled = true;

		font_semibold_italic_17_bordered = manager.get("font/Rawline-SemiBoldItalic_17_bordered.ttf", BitmapFont.class);

		font_bold_18_bordered = manager.get("font/Rawline-Bold_18_bordered.ttf", BitmapFont.class);
		font_bold_18_bordered_healthmana = manager.get("font/Rawline-Bold_18_bordered_healthmana.ttf",
				BitmapFont.class);

		font_semibold_26_bordered_blue8 = manager.get("font/Rawline-SemiBold_26_bordered.ttf", BitmapFont.class);
		font_semibold_26_bordered_blue8.getData().markupEnabled = true;

	}

}
