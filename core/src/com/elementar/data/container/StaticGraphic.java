package com.elementar.data.container;

import static com.elementar.logic.util.Shared.ALPHA_HOVER;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.elementar.data.DataTier;
import com.elementar.gui.modules.ingame.APlayerUIModule;
import com.elementar.gui.util.PlatformManager;
import com.elementar.gui.util.SharedColors;
import com.elementar.gui.util.VegetationManager;
import com.elementar.gui.util.VegetationSprite;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.Shared.CategoryFunctionality;
import com.elementar.logic.util.Shared.CategorySize;
import com.elementar.logic.util.lists.CategoryArrayList;

/**
 * Holds all static sprites like GUI-sprites, or sprites within in the world
 * (platforms, vegetation). Or in other words: this class holds sprites that
 * aren't animated with Spine and aren't particle-effects.
 * 
 * @author lukassongajlo
 * 
 */
public class StaticGraphic {

	/**
	 * für SMALL und MEDIUM (evtl. später eigene variable für MEDIUM,
	 * julian-abhängig)
	 */
	private static final int OFFSET_PLATFORM_HEIGHT_IN_PIXEL = 80;
	/**
	 * hier 300 da steine in den LARGE platformen.
	 */
	private static final int OFFSET_PLATFORM_HEIGHT_IN_PIXEL_LARGE = 280;

	private static final int OFFSET_VEGETATION_HEIGHT_IN_PIXEL = 320;// MathUtils.random(292,
																		// 305);

	/**
	 * Holds GUI sprites
	 */
	private static TextureAtlas atlas_gui;

	/**
	 * Holds Game Server GUI sprites
	 */
	private static TextureAtlas atlas_server_gui;

	/**
	 * Vegetation and foreground platform sprites
	 */
	private static TextureAtlas atlas_world_foreground;
	private static Skin skin = new Skin();

	public static Texture island_filling_texture;

	public static Sprite gui_bg_join1, gui_bg_host1, gui_bg_options1, gui_bg_esc1,
			gui_character_selection_shadow_rightside, gui_character_selection_shadow_leftside, gui_bg_nav1,
			gui_nav_decline_shadow, gui_icon_close_game, gui_icon_close_server, gui_icon_minimize_game,
			gui_icon_refresh, gui_icon_refreshstop, gui_icon_options, gui_icon_smallarrow_up, gui_icon_smallarrow_down,
			gui_btn_home, gui_icon_fighter, gui_icon_tank, gui_icon_support, gui_bg_demospirit, gui_bg_rolefilter,
			gui_bg_demospirit_inputfield, gui_progressbar, gui_frame_buff, gui_frame_debuff, gui_bg_equip,
			gui_equipped_indicator, gui_icon_help, gui_host_portframe, gui_icon_locked, gui_icon_server_connecting,
			gui_icon_server_inactive, gui_icon_offline_mode, gui_icon_server_connected, gui_minimap_creep,
			gui_bg_world_chat_input, gui_icon_character_switch, gui_bg_skillbar, gui_bg_minimap,
			gui_bg_minimap_ornament, gui_bg_infobar, gui_bg_gameserver_row, gui_bg_circle, gui_bg_skillbar_health,
			gui_bg_skillbar_mana, gui_icon_radiobutton_unchecked, gui_icon_radiobutton_checked, gui_transparent_pixel,
			gui_bg_circle_lobby, gui_bg_btn_custom1, gui_bg_btn_custom2, gui_bg_btn_custom3, gui_skillbar_health,
			gui_skillbar_mana, gui_bg_btn_keyconfig, gui_bg_progressbar, gui_bg_buy_big, gui_bg_buy,
			gui_bg_chat_history, gui_bg_confirmation, gui_bg_player_statistics, gui_bg_stats_team1, gui_bg_stats_team2,
			gui_emblem_team1, gui_emblem_team2, gui_bg_tooltip, gui_icon_back, gui_icon_stonearch, gui_logo_stonearch,
			gui_icon_crossout, gui_bg_death_timer, gui_bg_tickets_left_my_team, gui_bg_tickets_right_my_team,
			gui_bg_tickets_left_enemy_team, gui_bg_tickets_right_enemy_team, gui_ticket_enemy_team, gui_ticket_my_team,
			gui_icon_ticket_enemy_team, gui_icon_ticket_my_team, gui_bg_post_lobby, gui_icon_cooldown, gui_icon_mana,
			// DROPDOWN
			gui_bg_dropdown_height1_width3_closed, gui_bg_dropdown_height1_width3_open,
			// BTN BG
			// HEIGHT1
			gui_bg_btn_height1_width2, gui_bg_btn_height1_width3, gui_bg_btn_height1_width4, gui_bg_btn_height1_width5,
			gui_bg_btn_height1_width7,
			// HEIGHT2
			gui_bg_btn_height2_width3, gui_bg_btn_height2_width4, gui_bg_btn_height2_width5;

	public static Sprite
	// INPUTFIELD BG
	// Height1
	gui_bg_inputfield_height1_width2, gui_bg_inputfield_height1_width3, gui_bg_inputfield_height1_width4,
			gui_bg_inputfield_height1_width5, gui_bg_inputfield_height1_width6, gui_bg_inputfield_height1_width7;

	public static Pixmap gui_cursor1, gui_cursor_world_regular1, gui_cursor_transparent;

	private static PlatformContainer platform_container;

	private static VegetationContainer vegetation_container;

	public static float platform_small_height, platform_large_height;
	public static float platform_small_width, platform_large_width;

	public static float vegetation_height, vegetation_width;

	/**
	 * For pre loading phase where we need the cursor images before the regular
	 * loading starts
	 */
	public static void loadCursorImages() {
		AssetManager manager = DataTier.ASSET_MANAGER;
		manager.load("graphic/gui_cursor1.png", Pixmap.class);
		manager.load("graphic/gui_cursor_world_regular1.png", Pixmap.class);
		manager.load("graphic/gui_cursor_transparent.png", Pixmap.class);
		manager.finishLoading();
		gui_cursor1 = manager.get("graphic/gui_cursor1.png", Pixmap.class);
		gui_cursor_world_regular1 = manager.get("graphic/gui_cursor_world_regular1.png", Pixmap.class);
		gui_cursor_transparent = manager.get("graphic/gui_cursor_transparent.png", Pixmap.class);
	}

	public static Texture loadLoadingIndicator() {
		TextureParameter param = new TextureParameter() {
			{
				minFilter = TextureFilter.Linear;
				magFilter = TextureFilter.Linear;
			}
		};
		AssetManager manager = DataTier.ASSET_MANAGER;
		manager.load("graphic/icon128.png", Texture.class, param);
		manager.finishLoading();
		return manager.get("graphic/icon128.png", Texture.class);
	}

	public static Sprite gui_server_bg, gui_server_icon_help, gui_server_inputfield, gui_server_inputfield_ports,
			gui_server_start, gui_server_icon_leaf, gui_server_line, gui_server_leave;

	public static void loadAndSetServerSprites() {
		AssetManager manager = DataTier.ASSET_MANAGER;
		manager.load("graphic/server_gui_sprites.atlas", TextureAtlas.class);
		manager.finishLoading();

		atlas_server_gui = manager.get("graphic/server_gui_sprites.atlas", TextureAtlas.class);

		gui_server_bg = atlas_server_gui.createSprite("gui_server_bg");
		gui_server_icon_help = atlas_server_gui.createSprite("gui_server_icon_help");
		gui_server_icon_leaf = atlas_server_gui.createSprite("gui_server_icon_leaf");
		gui_server_inputfield = atlas_server_gui.createSprite("gui_server_inputfield");
		gui_server_inputfield_ports = atlas_server_gui.createSprite("gui_server_inputfield_ports");
		gui_server_start = atlas_server_gui.createSprite("gui_server_start");
		gui_server_line = atlas_server_gui.createSprite("gui_server_line");
		gui_server_leave = atlas_server_gui.createSprite("gui_server_leave");
		gui_bg_tooltip = atlas_server_gui.createSprite("gui_server_bg_tooltip");
		gui_bg_confirmation = atlas_server_gui.createSprite("gui_server_bg_confirmation");
		gui_bg_btn_height1_width4 = atlas_server_gui.createSprite("gui_server_bg_btn_height1_width4");
		gui_host_portframe = atlas_server_gui.createSprite("gui_server_host_portframe");

		Sprite knobHover, sliderBackgroundHover;

		knobHover = atlas_server_gui.createSprite("gui_server_icon_regulator");
		knobHover.setColor(1, 1, 1, ALPHA_HOVER);

		sliderBackgroundHover = atlas_server_gui.createSprite("gui_server_icon_regulatorbar");
		sliderBackgroundHover.setColor(1, 1, 1, ALPHA_HOVER);

		skin.add("gui_textfield_cursor", atlas_server_gui.createSprite("gui_server_textfield_cursor"));
		skin.add("gui_textfield_selection", atlas_server_gui.createSprite("gui_server_textfield_selection"));
		skin.add("slider", atlas_server_gui.createSprite("gui_server_icon_regulatorbar"));
		skin.add("slider_hover", sliderBackgroundHover);
		skin.add("knob", atlas_server_gui.createSprite("gui_server_icon_regulator"));
		skin.add("knob_hover", knobHover);
		skin.add("gui_icon_checkbox_checked", atlas_server_gui.createSprite("gui_server_icon_checked"));
		skin.add("gui_icon_checkbox_unchecked", atlas_server_gui.createSprite("gui_server_icon_unchecked"));
		skin.add("gui_server_vscroll_bg", atlas_server_gui.createSprite("gui_server_vscroll_bg"));
		skin.add("gui_server_vscroll_knob", atlas_server_gui.createSprite("gui_server_vscroll_knob"));
	}

	public static void load() {
		AssetManager manager = DataTier.ASSET_MANAGER;
		manager.load("graphic/gui_sprites.atlas", TextureAtlas.class);
		manager.load("graphic/world_foreground_sprites.atlas", TextureAtlas.class);
		TextureParameter param = new TextureParameter();
		param.wrapU = TextureWrap.Repeat;
		param.wrapV = TextureWrap.Repeat;
		manager.load("graphic/world_island_filling.jpg", Texture.class, param);
	}

	public static void setFields() {

		AssetManager manager = DataTier.ASSET_MANAGER;

		atlas_gui = manager.get("graphic/gui_sprites.atlas", TextureAtlas.class);
		atlas_world_foreground = manager.get("graphic/world_foreground_sprites.atlas", TextureAtlas.class);

		island_filling_texture = manager.get("graphic/world_island_filling.jpg", Texture.class);

		// TODO was needed for stencilling tests
		// circle = createGUISprite("circle");
		// circle.setOrigin(0, 0);
		// circle.setScale(Shared.SCALE_CHARACTER_INGAME*2);
		// circle.setPosition(Base.LEFT_SPAWN_POINT.x, Base.LEFT_SPAWN_POINT.y);

		gui_bg_join1 = createGUISprite("gui_bg_join1");
		gui_bg_host1 = createGUISprite("gui_bg_host1");
		gui_bg_options1 = createGUISprite("gui_bg_options1");
		gui_bg_player_statistics = createGUISprite("gui_bg_playerstatistics");

		gui_bg_stats_team1 = createGUISprite("gui_bg_stats_team1");
		gui_bg_stats_team2 = createGUISprite("gui_bg_stats_team2");

		gui_btn_home = createGUISprite("gui_btn_home1");

		gui_icon_fighter = createGUISprite("gui_icon_fighter");
		gui_icon_tank = createGUISprite("gui_icon_protector");
		gui_icon_support = createGUISprite("gui_icon_tactician");
		
		gui_bg_nav1 = createGUISprite("gui_bg_nav1");

		gui_bg_esc1 = createGUISprite("gui_bg_esc1");
		gui_bg_esc1.setPosition(0, 308);

		gui_bg_death_timer = createGUISprite("gui_bg_death_timer");
		gui_bg_death_timer.setSize(gui_bg_death_timer.getWidth() * APlayerUIModule.SCALE_UI,
				gui_bg_death_timer.getHeight() * APlayerUIModule.SCALE_UI);

		gui_bg_tickets_left_my_team = createGUISprite("gui_bg_tickets_my_team");
		gui_bg_tickets_left_my_team.setSize(gui_bg_tickets_left_my_team.getWidth() * APlayerUIModule.SCALE_UI,
				gui_bg_tickets_left_my_team.getHeight() * APlayerUIModule.SCALE_UI);

		gui_bg_tickets_right_my_team = createGUISprite("gui_bg_tickets_my_team");
		gui_bg_tickets_right_my_team.setSize(gui_bg_tickets_right_my_team.getWidth() * APlayerUIModule.SCALE_UI,
				gui_bg_tickets_right_my_team.getHeight() * APlayerUIModule.SCALE_UI);
		gui_bg_tickets_right_my_team.setFlip(true, false);

		gui_bg_tickets_left_enemy_team = createGUISprite("gui_bg_tickets_enemy_team");
		gui_bg_tickets_left_enemy_team.setSize(gui_bg_tickets_left_enemy_team.getWidth() * APlayerUIModule.SCALE_UI,
				gui_bg_tickets_left_enemy_team.getHeight() * APlayerUIModule.SCALE_UI);

		gui_bg_tickets_right_enemy_team = createGUISprite("gui_bg_tickets_enemy_team");
		gui_bg_tickets_right_enemy_team.setSize(gui_bg_tickets_right_enemy_team.getWidth() * APlayerUIModule.SCALE_UI,
				gui_bg_tickets_right_enemy_team.getHeight() * APlayerUIModule.SCALE_UI);
		gui_bg_tickets_right_enemy_team.setFlip(true, false);

		int ticketWidth = (int) (232 * APlayerUIModule.SCALE_UI);
		int ticketHeight = (int) (45 * APlayerUIModule.SCALE_UI);
		gui_ticket_my_team = createGUISprite("gui_whitepixel");
		gui_ticket_my_team.setSize(ticketWidth, ticketHeight);
		gui_ticket_my_team.setColor(124f / 255f, 153f / 255f, 78f / 255f, 1f);
		gui_ticket_enemy_team = createGUISprite("gui_whitepixel");
		gui_ticket_enemy_team.setSize(ticketWidth, ticketHeight);
		gui_ticket_enemy_team.setColor(153f / 255f, 30f / 255f, 59f / 255f, 1f);

		gui_icon_ticket_my_team = createGUISprite("gui_icon_ticket_my_team");
		int ticketIconWidth = (int) (gui_icon_ticket_my_team.getWidth() * APlayerUIModule.SCALE_UI);
		int ticketIconHeight = (int) (gui_icon_ticket_my_team.getHeight() * APlayerUIModule.SCALE_UI);
		gui_icon_ticket_my_team.setSize(ticketIconWidth, ticketIconHeight);

		gui_icon_ticket_enemy_team = createGUISprite("gui_icon_ticket_enemy_team");
		gui_icon_ticket_enemy_team.setSize(ticketIconWidth, ticketIconHeight);

		gui_icon_stonearch = createGUISprite("gui_icon_stonearch");
		gui_logo_stonearch = createGUISprite("gui_logo_stonearch");

		gui_frame_buff = createGUISprite("gui_frame_buff");
		gui_frame_debuff = createGUISprite("gui_frame_debuff");

		gui_equipped_indicator = createGUISprite("gui_equipped_indicator");

		gui_icon_cooldown = createGUISprite("gui_icon_cooldown");
		gui_icon_mana = createGUISprite("gui_icon_mana");

		gui_minimap_creep = createGUISprite("gui_minimap_creep");
		gui_minimap_creep.setScale(Shared.SCALE_MINIMAP_SYMBOLS);

		gui_nav_decline_shadow = createGUISprite("gui_shadow_fadeleft");
		gui_nav_decline_shadow.setBounds(1807, 990, gui_nav_decline_shadow.getWidth(),
				gui_nav_decline_shadow.getHeight());

		gui_character_selection_shadow_rightside = createGUISprite("gui_shadow_fadeleft");
		gui_character_selection_shadow_rightside.setSize(gui_character_selection_shadow_rightside.getWidth(), 110);

		gui_character_selection_shadow_leftside = createGUISprite("gui_shadow_fadeleft");
		gui_character_selection_shadow_leftside.setSize(gui_character_selection_shadow_leftside.getWidth(), 110);
		gui_character_selection_shadow_leftside.flip(true, false);

		gui_bg_rolefilter = createGUISprite("gui_bg_rolefilter");

		gui_bg_demospirit_inputfield = createGUISprite("gui_bg_demohero_inputfield");

		gui_icon_character_switch = createGUISprite("gui_icon_character_switch");

		gui_bg_skillbar_health = createGUISprite("gui_bg_skillbar_healthmana");
		gui_bg_skillbar_health.setScale(APlayerUIModule.SCALE_UI);

		gui_bg_skillbar_mana = createGUISprite("gui_bg_skillbar_healthmana");
		gui_bg_skillbar_mana.setScale(APlayerUIModule.SCALE_UI);

		gui_skillbar_health = createGUISprite("gui_health");
		gui_skillbar_health.setScale(APlayerUIModule.SCALE_UI);

		gui_skillbar_mana = createGUISprite("gui_mana");
		gui_skillbar_mana.setScale(APlayerUIModule.SCALE_UI);

		gui_bg_equip = createGUISprite("gui_bg_equip");

		gui_icon_close_game = createGUISprite("gui_icon_close_game");
		gui_icon_close_server = createGUISprite("gui_icon_close_server");
		gui_icon_minimize_game = createGUISprite("gui_icon_minimize_game");

		gui_icon_refresh = createGUISprite("gui_icon_refresh");
		gui_icon_refreshstop = createGUISprite("gui_icon_refreshstop");
		gui_icon_options = createGUISprite("gui_icon_options");
		gui_icon_smallarrow_up = createGUISprite("gui_icon_smallarrow_up");
		gui_icon_smallarrow_down = createGUISprite("gui_icon_smallarrow_up");
		gui_icon_smallarrow_down.flip(false, true);

		gui_icon_locked = createGUISprite("gui_icon_locked");

		gui_icon_radiobutton_checked = createGUISprite("gui_icon_radiobutton_checked");
		gui_icon_radiobutton_unchecked = createGUISprite("gui_icon_radiobutton_unchecked");

		gui_bg_demospirit = createGUISprite("gui_bg_demohero");

		gui_emblem_team1 = createGUISprite("gui_emblem_team1");
		gui_emblem_team2 = createGUISprite("gui_emblem_team2");

		gui_bg_circle = createGUISprite("gui_bg_circle");
		gui_bg_circle.setSize(100, 100);
		gui_bg_circle.setOriginCenter();

		gui_icon_crossout = createGUISprite("gui_icon_crossedout");
		gui_icon_crossout.setScale(0.625f);
		gui_icon_crossout.setRotation(-45);

		Sprite circleLobby = createGUISprite("gui_bg_circle");
		circleLobby.setSize(120, 120);
		gui_bg_circle_lobby = circleLobby;

		gui_bg_skillbar = createGUISprite("gui_bg_skillbar");
		gui_bg_skillbar.setScale(APlayerUIModule.SCALE_UI);

		gui_bg_minimap = createGUISprite("gui_bg_minimap");

		gui_bg_minimap_ornament = createGUISprite("gui_bg_minimap_ornament");

		gui_bg_infobar = createGUISprite("gui_bg_infobar");
		gui_bg_infobar.setSize(gui_bg_infobar.getWidth() * APlayerUIModule.SCALE_UI,
				gui_bg_infobar.getHeight() * APlayerUIModule.SCALE_UI);

		gui_icon_help = createGUISprite("gui_icon_help");

		gui_host_portframe = createGUISprite("gui_host_portframe");

		gui_bg_dropdown_height1_width3_closed = createGUISprite("gui_bg_dropdown_height1_width3_closed");
		gui_bg_dropdown_height1_width3_open = createGUISprite("gui_bg_dropdown_height1_width3_open");

		gui_bg_confirmation = createGUISprite("gui_bg_confirmation1");

		gui_bg_post_lobby = createGUISprite("gui_bg_confirmation1");
		gui_bg_post_lobby.setScale(1.5f);

		gui_bg_chat_history = createGUISprite("gui_bg_chat_history");
		gui_bg_world_chat_input = createGUISprite("gui_bg_world_chat_input");

		gui_bg_btn_custom1 = createGUISprite("gui_bg_btn_custom1");
		gui_bg_btn_custom2 = createGUISprite("gui_bg_btn_custom2");
		gui_bg_btn_custom3 = createGUISprite("gui_bg_btn_custom3");

		gui_server_line = createGUISprite("gui_server_line");

		gui_bg_buy = createGUISprite("gui_bg_buy");
		gui_bg_buy_big = createGUISprite("gui_bg_buy_big");

		gui_bg_gameserver_row = createGUISprite("gui_whitepixel");
		gui_bg_gameserver_row.setSize(960, Shared.HEIGHT1);

		gui_transparent_pixel = createGUISprite("gui_whitepixel");
		gui_transparent_pixel.setColor(0f, 0f, 0f, 0f);

		gui_icon_server_connected = createGUISprite("gui_icon_server_connected");
		gui_icon_server_connecting = createGUISprite("gui_icon_server_connecting");
		gui_icon_server_inactive = createGUISprite("gui_icon_server_inactive");
		gui_icon_offline_mode = createGUISprite("gui_icon_offline_mode");

		gui_bg_progressbar = createGUISprite("gui_bg_progressbar");

		gui_progressbar = createGUISprite("gui_whitepixel");
		// in pixel
		float leftMarging = 5;
		gui_progressbar.setSize(gui_bg_progressbar.getWidth() - leftMarging, 14);
		gui_progressbar.setOrigin(0, 0);
		gui_progressbar.setColor(85f / 255f, 222f / 255f, 255f / 255f, 1f);

		gui_bg_tooltip = createGUISprite("gui_bg_tooltip");

		gui_icon_back = createGUISprite("gui_icon_back");

		// BTN BG
		// Height1
		gui_bg_btn_height1_width2 = createGUISprite("gui_bg_btn_height1_width2");
		gui_bg_btn_keyconfig = createGUISprite("gui_bg_btn_keyconfig");
		gui_bg_btn_height1_width3 = createGUISprite("gui_bg_btn_height1_width3");
		gui_bg_btn_height1_width4 = createGUISprite("gui_bg_btn_height1_width4");
		gui_bg_btn_height1_width5 = createGUISprite("gui_bg_btn_height1_width5");
		gui_bg_btn_height1_width7 = createGUISprite("gui_bg_btn_height1_width7");
		// Height2
		gui_bg_btn_height2_width3 = createGUISprite("gui_bg_btn_height2_width3");
		gui_bg_btn_height2_width4 = createGUISprite("gui_bg_btn_height2_width4");
		gui_bg_btn_height2_width5 = createGUISprite("gui_bg_btn_height2_width5");

		// INPUTFIELD BG
		// Height1
		gui_bg_inputfield_height1_width2 = createGUISprite("gui_bg_inputfield_height1_width2");
		gui_bg_inputfield_height1_width3 = createGUISprite("gui_bg_inputfield_height1_width3");
		gui_bg_inputfield_height1_width4 = createGUISprite("gui_bg_inputfield_height1_width4");
		gui_bg_inputfield_height1_width5 = createGUISprite("gui_bg_inputfield_height1_width5");
		gui_bg_inputfield_height1_width6 = createGUISprite("gui_bg_inputfield_height1_width6");
		gui_bg_inputfield_height1_width7 = createGUISprite("gui_bg_inputfield_height1_width7");

		Sprite knobHover, sliderBackgroundHover;

		knobHover = createGUISprite("gui_icon_regulator");
		knobHover.setColor(1, 1, 1, ALPHA_HOVER);

		sliderBackgroundHover = createGUISprite("gui_icon_regulatorbar");
		sliderBackgroundHover.setColor(1, 1, 1, ALPHA_HOVER);

		Sprite screenCover;

		screenCover = createGUISprite("gui_whitepixel");
		
		Color colCover = new Color(SharedColors.BLUE_8);
		colCover.a = 0.7f;
		
		screenCover.setColor(colCover);
		skin.add("gui_bg_screencover", screenCover);

		screenCover = createGUISprite("gui_whitepixel");
		screenCover.setColor(colCover);
		skin.add("gui_bg_selection_world", screenCover);

		skin.add("gui_textfield_cursor", createGUISprite("gui_textfield_cursor"));
		skin.add("gui_textfield_selection", createGUISprite("gui_textfield_selection"));
		skin.add("slider", createGUISprite("gui_icon_regulatorbar"));
		skin.add("slider_hover", sliderBackgroundHover);
		skin.add("knob", createGUISprite("gui_icon_regulator"));
		skin.add("knob_hover", knobHover);
		skin.add("gui_icon_checkbox_checked", createGUISprite("gui_icon_checkbox_checked"));
		skin.add("gui_icon_checkbox_unchecked", createGUISprite("gui_icon_checkbox_unchecked"));
		skin.add("gui_vscroll_bg", createGUISprite("gui_vscroll_bg"));
		skin.add("gui_vscroll_knob", createGUISprite("gui_vscroll_knob"));

		platform_container = new PlatformContainer();
		vegetation_container = new VegetationContainer();

		setPlatformsDimensions();
		setVegetationDimensions();

	}

	public static void dispose() {
		skin.dispose();
	}

	public static Drawable getDrawable(String name) {
		if (skin.has(name, Sprite.class) == true)
			return skin.getDrawable(name);
		return null;
	}

	/**
	 * Creates an dummy vegetation sprite to determine the vegetation dimensions
	 * ahead of {@link VegetationManager#place(Vector2[])}
	 */
	private static void setVegetationDimensions() {
		/*
		 * it's sufficient to take a look on a single small plant, because all
		 * categorized sizes have the same sprite size.
		 */
		Sprite testSpriteSmall = makeVegetationSprite(CategorySize.SMALL, CategoryFunctionality.WALK, new Vector2(0, 0),
				0);
		vegetation_height = testSpriteSmall.getBoundingRectangle().height;
		vegetation_width = testSpriteSmall.getBoundingRectangle().width;
	}

	/**
	 * Creates platform-sprite dummies to determine the platform dimensions ahead of
	 * {@link PlatformManager#place(Vector2[])}
	 */
	private static void setPlatformsDimensions() {
		Sprite testSpriteSmall = makePlatformSprite(CategorySize.SMALL, CategoryFunctionality.WALK, new Vector2(0, 0),
				0);
		platform_small_width = testSpriteSmall.getBoundingRectangle().width;
		platform_small_height = testSpriteSmall.getBoundingRectangle().height;

		Sprite testSpriteLarge = makePlatformSprite(CategorySize.LARGE, CategoryFunctionality.WALK, new Vector2(0, 0),
				0);
		platform_large_width = testSpriteLarge.getBoundingRectangle().width;
		platform_large_height = testSpriteLarge.getBoundingRectangle().height;
	}

	/**
	 * This method is NOT for little platforms on curved corners! Just for longer
	 * platforms
	 * 
	 * @param size
	 * @param functionality
	 * @param position
	 * @param angle
	 * @return
	 */
	public static VegetationSprite makeVegetationSprite(CategorySize size, CategoryFunctionality functionality,
			Vector2 position, float angle) {

		float scale = Shared.SCALE_PLATFORM_AND_VEGETATION_SPRITES;

		Sprite vegetationSprite = createWorldForegroundSprite(
				getSpriteNameByCategories(size, functionality, vegetation_container));
		if (vegetationSprite == null)
			return null;
		float correctionLength = getVegetationCorrectionLength(scale);
		return new VegetationSprite(
				manipulateSprite(vegetationSprite, position, angle, correctionLength, scale, 1f, 1f), functionality,
				size);

	}

	/**
	 * This method creates a sprite, where the platformLength is very short and
	 * suited for a curved corner of an island. It takes always a
	 * {@link CategorySize#SMALL} sprite and scales it down to fit the short
	 * platform.
	 * 
	 * @param functionality
	 * @param position
	 * @param angle
	 * @param width
	 * @return
	 */
	public static VegetationSprite makeVegetationSprite(CategoryFunctionality functionality, Vector2 position,
			float angle, float width) {

		float scale = Shared.SCALE_PLATFORM_AND_VEGETATION_SPRITES;

		Sprite vegetationSprite = createWorldForegroundSprite(
				getSpriteNameByCategories(CategorySize.SMALL, functionality, vegetation_container));
		float correctionLength = getVegetationCorrectionLength(scale);

		return new VegetationSprite(manipulateSprite(vegetationSprite, position, angle, correctionLength, scale,
				width / vegetation_width, 1f), functionality, CategorySize.SMALL);

	}

	/**
	 * This method creates and places a platformSprite. Assumption: the given size
	 * fits onto platform. For curved sections use
	 * {@link #makePlatformSprite(CategoryFunctionality, Vector2, float, float, float)}
	 * 
	 * @param size
	 * @param functionality
	 * @param position
	 * @param angle
	 * @param scale
	 * @return
	 */
	public static Sprite makePlatformSprite(CategorySize size, CategoryFunctionality functionality, Vector2 position,
			float angle) {
		return makePlatformSprite(size, functionality, position, angle, 1f, 1f);
	}

	/**
	 * This method creates a sprite, where the platformLength is very short and
	 * suited for a curved corner of an island. It takes always a
	 * {@link CategorySize#SMALL} sprite and scales it down to fit the short
	 * platform.
	 * 
	 * @param size
	 * @param functionality
	 * @param position
	 * @param angle
	 * @param scaleX
	 * @return
	 */
	public static Sprite makePlatformSprite(CategorySize size, CategoryFunctionality functionality, Vector2 position,
			float angle, float scaleX, float scaleY) {

		float scale = Shared.SCALE_PLATFORM_AND_VEGETATION_SPRITES;

		float correctionLength = getPlatformCorrectionLengthBySize(size, scale, scaleY);
		return manipulateSprite(createPlatformSprite(size, functionality), position, angle, correctionLength, scale,
				scaleX, scaleY);
	}

	private static Sprite createPlatformSprite(CategorySize size, CategoryFunctionality functionality) {
		String platformName = getSpriteNameByCategories(size, functionality, platform_container);
		return createWorldForegroundSprite(platformName);
	}

	/**
	 * Creates a sprite from {@link #atlas_gui} with a specific index
	 * 
	 * @param name
	 * @return
	 */
	public static Sprite createGUISprite(String name, int index) {
		return atlas_gui.createSprite(name, index);
	}

	/**
	 * Creates a sprite from {@link #atlas_gui}
	 * 
	 * @param name
	 * @return
	 */
	public static Sprite createGUISprite(String name) {
		return atlas_gui.createSprite(name);
	}

	/**
	 * Creates a sprites from {@link #atlas_world_foreground}
	 * 
	 * @param name
	 * @return
	 */
	public static Sprite createWorldForegroundSprite(String name) {
		return atlas_world_foreground.createSprite(name);
	}

	/**
	 * Holds the last-random string that was generated by randomness to avoid
	 * repetitions.
	 */
	private static String last_random_string = "";

	private static String getSpriteNameByCategories(CategorySize size, CategoryFunctionality functionality,
			AStringCategoryContainer container) {
		CategoryArrayList<String> pot = null;
		for (CategoryArrayList<String> list : container.getAllPots()) {
			if (list.getCategorySize().equals(size) && list.getCategoryFunctionality().equals(functionality)) {
				pot = list;
			}
		}
		String randomString;
		do {
			if (pot == null)
				return "";
			randomString = pot.getRandomString();
		} while (randomString.equals(last_random_string));
		last_random_string = randomString;
		return last_random_string;
	}

	private static float getPlatformCorrectionLengthBySize(CategorySize size, float scale, float scaleY) {
		float correctionLength = 0;
		switch (size) {
		case SMALL:
			correctionLength = ((platform_small_height / scale) - OFFSET_PLATFORM_HEIGHT_IN_PIXEL) * scale;
			break;
		case LARGE:
			correctionLength = ((platform_large_height / scale) - OFFSET_PLATFORM_HEIGHT_IN_PIXEL_LARGE) * scale;
			break;
		}
		return correctionLength * scaleY;
	}

	private static float getVegetationCorrectionLength(float scale) {
		return ((vegetation_height / scale) - OFFSET_VEGETATION_HEIGHT_IN_PIXEL) * scale;
	}

	/**
	 * @param sprite
	 * @param position
	 * @param angle
	 * @param correctionLength
	 * @param scale
	 * @param scaleX
	 * @param scaleY
	 * @return
	 */
	private static Sprite manipulateSprite(Sprite sprite, Vector2 position, float angle, float correctionLength,
			float scale, float scaleX, float scaleY) {

		float correctionAngle = angle - 90f;

		// create correction vector with polar coords
		Vector2 correctionVector = Util.getPolarCoords(correctionLength, correctionAngle);

		sprite.setOrigin(0, 0);
		sprite.setRotation(angle);
		sprite.setScale(scale * scaleX, scale * scaleY);
		sprite.setPosition(position.x + correctionVector.x, position.y + correctionVector.y);
		return sprite;
	}

}
