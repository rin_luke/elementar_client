package com.elementar.data.container;

import java.util.ArrayList;

import com.elementar.data.container.util.Gameclass;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.data.container.util.Gameclass.Role;

/**
 * Loads and holds gameclass-data. Gameclasses are e.g. the Stone or Wind
 * character. Provides also methods to get all skin-sprites of a character (
 * {@link #getSkinThumbnails(GameclassName)}), or to filter characters by their
 * role.
 * 
 * @author lukassongajlo
 * 
 */
public class GameclassContainer {

	public static ArrayList<Gameclass> ALL_GAMECLASSES;

	/**
	 * Note: Must be called after loading sprites from static spritesheet!
	 */
	public static void setFields() {

		Gameclass.setMetaSkillFields();

		ALL_GAMECLASSES = new ArrayList<Gameclass>();

		ALL_GAMECLASSES.add(new Gameclass(GameclassName.ICE, Role.FIGHTER));
		ALL_GAMECLASSES.add(new Gameclass(GameclassName.LEAF, Role.TANK));
		ALL_GAMECLASSES.add(new Gameclass(GameclassName.LIGHT, Role.SUPPORT));
		ALL_GAMECLASSES.add(new Gameclass(GameclassName.LIGHTNING, Role.FIGHTER));
		ALL_GAMECLASSES.add(new Gameclass(GameclassName.SAND, Role.SUPPORT));
		ALL_GAMECLASSES.add(new Gameclass(GameclassName.STONE, Role.TANK));
		ALL_GAMECLASSES.add(new Gameclass(GameclassName.TOXIC, Role.FIGHTER));
		ALL_GAMECLASSES.add(new Gameclass(GameclassName.VOID, Role.SUPPORT));
		ALL_GAMECLASSES.add(new Gameclass(GameclassName.WATER, Role.SUPPORT));
		ALL_GAMECLASSES.add(new Gameclass(GameclassName.WIND, Role.SUPPORT));

	}

	public static ArrayList<Gameclass> filterByRole(Role role) {
		if (role == Role.ALL)
			return ALL_GAMECLASSES;

		ArrayList<Gameclass> filteredList = new ArrayList<Gameclass>();
		for (Gameclass gameclass : ALL_GAMECLASSES)
			if (gameclass.getRole() == role)
				filteredList.add(gameclass);

		return filteredList;
	}

	/**
	 * Name parameter has to be an enum-value
	 * 
	 * @param name
	 * @return
	 */
	public static Gameclass filterByName(GameclassName name) {
		for (Gameclass gameclass : ALL_GAMECLASSES)
			if (gameclass.getNameAsEnum() == name)
				return gameclass;
		return null;
	}

}
