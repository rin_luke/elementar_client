package com.elementar.data.container.util;

import com.elementar.data.container.util.Gameclass.GameclassName;

/**
 * Extension of {@link CustomParticleEffect}.
 * 
 * 
 * @author lukassongajlo
 * 
 */
public class GameclassParticleEffect extends CustomParticleEffect {

	private GameclassName gameclass_name;

	/**
	 * 
	 * @param gameclassName
	 * @param skin
	 * @param slotName
	 * @param offsetX
	 * @param offsetY
	 * @param drawBehindBone
	 */
	public GameclassParticleEffect(GameclassName gameclassName, String skin, String slotName,
			float offsetX, float offsetY, boolean drawBehindBone) {
		super(skin, slotName, offsetX, offsetY, drawBehindBone);
		gameclass_name = gameclassName;
	}

	public GameclassParticleEffect(GameclassParticleEffect particleEffect) {
		super(particleEffect);
		gameclass_name = particleEffect.gameclass_name;
	}

	/**
	 * 
	 * @return
	 */
	public GameclassName getGameclassName() {
		return gameclass_name;
	}

}
