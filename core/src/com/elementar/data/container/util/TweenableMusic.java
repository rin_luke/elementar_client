package com.elementar.data.container.util;

import com.badlogic.gdx.audio.Music;

public class TweenableMusic {

	private Music music;

	private float volume_begin;

	private boolean tweening = false;

	public TweenableMusic(Music music) {
		this.music = music;
	}

	public void setVolumeBegin(float volumeBegin) {
		this.volume_begin = volumeBegin;
	}

	public float getVolumeBegin() {
		return volume_begin;
	}

	public Music getMusicObj() {
		return music;
	}

	public void setTweenedVolume(float tweenedAlpha) {
		music.setVolume(volume_begin * tweenedAlpha);
	}

	public void setTweening(boolean tweening) {
		this.tweening = tweening;
	}

	public boolean isTweening() {
		return tweening;
	}

}
