package com.elementar.data.container.util;

import java.util.ArrayList;
import java.util.Collections;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.elementar.data.container.AnimationContainer;
import com.elementar.gui.GUITier;
import com.elementar.gui.modules.ingame.APlayerUIModule;
import com.elementar.gui.util.CustomStage;
import com.elementar.gui.widgets.CustomTable;
import com.elementar.gui.widgets.SkillButton;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.ASkill;
import com.elementar.logic.characters.skills.DyingSkill;
import com.elementar.logic.characters.skills.MetaFeedback;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.characters.skills.SpawnSkillAfterDeath;
import com.elementar.logic.characters.skills.TransferSkill;
import com.elementar.logic.characters.skills.TransferSkillUpdated;
import com.elementar.logic.characters.skills.ice.IceSkill0;
import com.elementar.logic.characters.skills.ice.IceSkill1;
import com.elementar.logic.characters.skills.ice.IceSkill2;
import com.elementar.logic.characters.skills.leaf.LeafSkill0;
import com.elementar.logic.characters.skills.leaf.LeafSkill1;
import com.elementar.logic.characters.skills.leaf.LeafSkill2;
import com.elementar.logic.characters.skills.light.LightSkill0;
import com.elementar.logic.characters.skills.light.LightSkill1;
import com.elementar.logic.characters.skills.light.LightSkill2;
import com.elementar.logic.characters.skills.lightning.LightningSkill0;
import com.elementar.logic.characters.skills.lightning.LightningSkill1;
import com.elementar.logic.characters.skills.lightning.LightningSkill2;
import com.elementar.logic.characters.skills.sand.SandSkill0;
import com.elementar.logic.characters.skills.sand.SandSkill1;
import com.elementar.logic.characters.skills.sand.SandSkill2;
import com.elementar.logic.characters.skills.stone.StoneSkill0;
import com.elementar.logic.characters.skills.stone.StoneSkill1;
import com.elementar.logic.characters.skills.stone.StoneSkill2;
import com.elementar.logic.characters.skills.toxic.ToxicSkill0;
import com.elementar.logic.characters.skills.toxic.ToxicSkill1;
import com.elementar.logic.characters.skills.toxic.ToxicSkill2;
import com.elementar.logic.characters.skills.void_.VoidSkill0;
import com.elementar.logic.characters.skills.void_.VoidSkill1;
import com.elementar.logic.characters.skills.void_.VoidSkill2;
import com.elementar.logic.characters.skills.water.WaterSkill0;
import com.elementar.logic.characters.skills.water.WaterSkill1;
import com.elementar.logic.characters.skills.water.WaterSkill2;
import com.elementar.logic.characters.skills.wind.WindSkill0;
import com.elementar.logic.characters.skills.wind.WindSkill1;
import com.elementar.logic.characters.skills.wind.WindSkill2;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.ExecutionMode;

/**
 * A gameclass is described by a name, role, whether it is available (during the
 * pick mode) and sprites (symbols and skins). The sprites are thumbnails, have
 * nothing to do with Spine animations and get used in
 * {@link GameclassScrollerNonWorldModule}.
 * 
 * @author lukassongajlo
 * 
 */
public class Gameclass {

	private GameclassName name;
	private Role role;

	private float[] color;
	protected Sprite symbol_gui, symbol_statistics, symbol_minimap_team1, symbol_minimap_team2;
	protected HorizontalGroup skill_group_in_selection;
	protected CustomTable skill_main_table_world, skill_combo_feeding_table_world;
	protected CustomTable skill_combo1_table_world, skill_combo2_table_world;

	/**
	 * contains only skills for this specific game class. Does not contain
	 * {@link PortSkill}, {@link SpawnSkillAfterDeath}, {@link DyingSkill} or
	 * {@link TransferSkill}
	 */
	private ArrayList<MetaSkill> meta_skills;

	private SkillButton skill1_button, skill2_button;

	public static ArrayList<MetaSkill> ALL_META_SKILLS;

	private static String SKILLS_JSON = "{\n" + "	\"skills\":[\n" + "		{ \n"
			+ "			\"nameKey\": \"iceSkill0Name\",\n" + "			\"gameclass\": \"ICE\",\n"
			+ "			\"P1Version\" : 5,\n" + "			\"P1TimeScale\" : 6.666, \n"
			+ "			\"cooldown\": 0.4,\n" + "			\"manacost\": 0,\n" + "			\"feedbacks\":[\n"
			+ "				{\n" + "					\"damage\": -12	\n" + "				}\n" + "			]\n"
			+ "		},\n" + "		{\n" + "			\"nameKey\": \"iceSkill1Name\",\n"
			+ "			\"gameclass\": \"ICE\",\n" + "			\"descriptionKey\": \"iceSkill1Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_ice_skill1\",\n"
			+ "			\"comboHandFile\": \"ice_skill1_combohand.p\",\n" + "			\"P1Version\" : 5,\n"
			+ "			\"P1TimeScale\" : 6.666, \n" + "			\"cooldown\": 12.0,\n"
			+ "			\"manacost\": 30,\n" + "			\"comboFactorPrim\": 3.0,\n"
			+ "			\"comboFactorAdded\": 3.0,\n" + "			\"feedbacks\":[\n" + "				{\n"
			+ "					\"slowAmount\": -0.7,\n" + "					\"slowDuration\": 1500\n"
			+ "				}\n" + "			]\n" + "		},\n" + "		{\n"
			+ "			\"nameKey\": \"iceSkill2Name\",\n" + "			\"gameclass\": \"ICE\",\n"
			+ "			\"descriptionKey\": \"iceSkill2Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_ice_skill2\",\n"
			+ "			\"comboHandFile\": \"ice_skill2_combohand.p\",\n" + "			\"P1Version\" : 5,\n"
			+ "			\"P1TimeScale\" : 6.666, \n" + "			\"cooldown\": 10.0,\n"
			+ "			\"manacost\": 30,\n" + "			\"comboFactorPrim\": 1.0,\n"
			+ "			\"comboFactorAdded\": 1.0,\n" + "			\"feedbacks\":[\n" + "				{\n"
			+ "					\"damage\": -80	\n" + "				}\n" + "			]\n" + "		},\n"
			+ "		{\n" + "			\"nameKey\": \"leafSkill0Name\",\n" + "			\"gameclass\": \"LEAF\",\n"
			+ "			\"P1Version\" : 5,\n" + "			\"P1TimeScale\" : 6.333, \n"
			+ "			\"cooldown\": 0.5,\n" + "			\"manacost\": 0,\n" + "			\"feedbacks\":[\n"
			+ "				{\n" + "					\"damage\": -10	\n" + "				}\n" + "			]\n"
			+ "		},\n" + "		{\n" + "			\"nameKey\": \"leafSkill1Name\",\n"
			+ "			\"gameclass\": \"LEAF\",\n" + "			\"descriptionKey\": \"leafSkill1Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_leaf_skill1\",\n"
			+ "			\"comboHandFile\": \"leaf_skill1_combohand.p\",\n" + "			\"P1Version\" : 0,\n"
			+ "			\"P1TimeScale\" : 6.666, \n" + "			\"cooldown\": 8.0,\n"
			+ "			\"manacost\": 30,\n" + "			\"comboFactorPrim\": 1.0,\n"
			+ "			\"comboFactorAdded\": 1.0,\n" + "			\"feedbacks\":[\n" + "				{\n"
			+ "					\"damage\": -30	\n" + "				}\n" + "			]\n" + "		},\n"
			+ "		{\n" + "			\"nameKey\": \"leafSkill2Name\",\n" + "			\"gameclass\": \"LEAF\",\n"
			+ "			\"descriptionKey\": \"leafSkill2Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_leaf_skill2\",\n"
			+ "			\"comboHandFile\": \"leaf_skill2_combohand.p\",\n" + "			\"P1Version\" : 0,\n"
			+ "			\"P1TimeScale\" : 6.666, \n" + "			\"cooldown\": 12.0,\n"
			+ "			\"manacost\": 30,\n" + "			\"comboFactorPrim\": 5.0,\n"
			+ "			\"comboFactorAdded\": 1.0,\n" + "			\"feedbacks\":[\n" + "				{\n"
			+ "					\"index\": 0, \n" + "					\"dotAmountPerSec\": -10,\n"
			+ "					\"duration\": 2000\n" + "				},\n" + "				{\n"
			+ "					\"index\": 1,\n" + "					\"dotAmountPerSec\": -10,\n"
			+ "					\"duration\": 3000		\n" + "				}\n" + "			]\n" + "		},\n"
			+ "		{\n" + "			\"nameKey\": \"lightSkill0Name\",\n" + "			\"gameclass\": \"LIGHT\",\n"
			+ "			\"P1Version\" : 0,\n" + "			\"P1TimeScale\" : 6.666, \n"
			+ "			\"cooldown\": 0.5,\n" + "			\"manacost\": 0,\n" + "			\"feedbacks\":[\n"
			+ "				{\n" + "					\"index\": 0,\n" + "					\"heal\": 12\n"
			+ "				},\n" + "				{\n" + "					\"index\": 1, \n"
			+ "					\"damage\": -8\n" + "				}\n" + "			]\n" + "		},\n"
			+ "		{\n" + "			\"nameKey\": \"lightSkill1Name\",\n" + "			\"gameclass\": \"LIGHT\",\n"
			+ "			\"descriptionKey\": \"lightSkill1Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_light_skill1\",\n"
			+ "			\"comboHandFile\": \"light_skill1_combohand.p\",\n" + "			\"P1Version\" : 2,\n"
			+ "			\"P1TimeScale\" : 2.0, \n" + "			\"cooldown\": 12.0,\n" + "			\"manacost\": 30,\n"
			+ "			\"comboFactorPrim\": 1.0,\n" + "			\"comboFactorAdded\": 1.0,\n"
			+ "			\"feedbacks\":[\n" + "				{\n" + "					\"index\": 0,\n"
			+ "					\"heal\": 40\n" + "				},\n" + "				{\n"
			+ "					\"index\": 1, \n" + "					\"damage\": -30\n" + "				}\n"
			+ "			]\n" + "		},\n" + "		{\n" + "			\"nameKey\": \"lightSkill2Name\",\n"
			+ "			\"gameclass\": \"LIGHT\",\n" + "			\"descriptionKey\": \"lightSkill2Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_light_skill2\",\n"
			+ "			\"comboHandFile\": \"light_skill2_combohand.p\",\n" + "			\"P1Version\" : 4,\n"
			+ "			\"P1TimeScale\" : 10.0, \n" + "			\"cooldown\": 12.0,\n" + "			\"manacost\": 30,\n"
			+ "			\"comboFactorPrim\": 1.0,\n" + "			\"comboFactorAdded\": 1.0,\n"
			+ "			\"feedbacks\":[\n" + "				{\n" + "					\"index\": 0, \n"
			+ "					\"heal\": 80\n" + "				},\n" + "				{\n"
			+ "					\"index\": 1, \n" + "					\"silenceDuration\": 2000\n"
			+ "				}\n" + "			]\n" + "		},\n" + "		{\n"
			+ "			\"nameKey\": \"lightningSkill0Name\",\n" + "			\"gameclass\": \"LIGHTNING\",\n"
			+ "			\"P1Version\" : 0,\n" + "			\"P1TimeScale\" : 7.333, \n"
			+ "			\"cooldown\": 0.35,\n" + "			\"manacost\": 0,\n" + "			\"feedbacks\":[\n"
			+ "				{\n" + "					\"damage\": -10	\n" + "				}\n" + "			]\n"
			+ "		},\n" + "		{\n" + "			\"nameKey\": \"lightningSkill1Name\",\n"
			+ "			\"gameclass\": \"LIGHTNING\",\n" + "			\"descriptionKey\": \"lightningSkill1Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_lightning_skill1\",\n"
			+ "			\"comboHandFile\": \"lightning_skill1_combohand.p\",\n" + "			\"P1Version\" : 5,\n"
			+ "			\"P1TimeScale\" : 1.0, \n" + "			\"cooldown\": 14.0,\n" + "			\"manacost\": 40,\n"
			+ "			\"comboFactorPrim\": 1.0,\n" + "			\"comboFactorAdded\": 1.0,\n"
			+ "			\"feedbacks\":[\n" + "				{\n" + "					\"damage\": -50	\n"
			+ "				}\n" + "			]\n" + "		},\n" + "		{\n"
			+ "			\"nameKey\": \"lightningSkill2Name\",\n" + "			\"gameclass\": \"LIGHTNING\",\n"
			+ "			\"descriptionKey\": \"lightningSkill2Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_lightning_skill2\",\n"
			+ "			\"comboHandFile\": \"lightning_skill2_combohand.p\",\n" + "			\"P1Version\" : 2,\n"
			+ "			\"P1TimeScale\" : 0.8, \n" + "			\"cooldown\": 0.0,\n" + "			\"manacost\": 40,\n"
			+ "			\"comboFactorPrim\": 1.0,\n" + "			\"comboFactorAdded\": 1.0,\n"
			+ "			\"feedbacks\":[\n" + "				{\n" + "					\"damage\": -20,\n"
			+ "					\"stunDuration\": 800	\n" + "				}\n" + "			]\n" + "		},\n"
			+ "		{\n" + "			\"nameKey\": \"sandSkill0Name\",\n" + "			\"gameclass\": \"SAND\",\n"
			+ "			\"P1Version\" : 5,\n" + "			\"P1TimeScale\" : 6.666, \n"
			+ "			\"cooldown\": 0.5,\n" + "			\"manacost\": 0,\n" + "			\"feedbacks\":[\n"
			+ "				{\n" + "					\"damage\": -10\n" + "				}\n" + "			]\n"
			+ "		},\n" + "		{\n" + "			\"nameKey\": \"sandSkill1Name\",\n"
			+ "			\"gameclass\": \"SAND\",\n" + "			\"descriptionKey\": \"sandSkill1Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_sand_skill1\",\n"
			+ "			\"comboHandFile\": \"sand_skill1_combohand.p\",\n" + "			\"P1Version\" : 4,\n"
			+ "			\"P1TimeScale\" : 10, \n" + "			\"cooldown\": 12.0,\n" + "			\"manacost\": 40,\n"
			+ "			\"comboFactorPrim\": 10.0,\n" + "			\"comboFactorAdded\": 10.0,\n"
			+ "			\"feedbacks\":[\n" + "				{\n" + "					\"damage\": -10\n"
			+ "				}\n" + "			]\n" + "		},\n" + "		{\n"
			+ "			\"nameKey\": \"sandSkill2Name\",\n" + "			\"gameclass\": \"SAND\",\n"
			+ "			\"descriptionKey\": \"sandSkill2Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_sand_skill2\",\n"
			+ "			\"comboHandFile\": \"sand_skill2_combohand.p\",\n" + "			\"P1Version\" : 4,\n"
			+ "			\"P1TimeScale\" : 10.0, \n" + "			\"cooldown\": 8.0,\n" + "			\"manacost\": 40,\n"
			+ "			\"comboFactorPrim\": 1.0,\n" + "			\"comboFactorAdded\": 1.0,\n"
			+ "			\"feedbacks\":[\n" + "				{\n" + "					\"disarmedDuration\": 2500\n"
			+ "				}\n" + "			]\n" + "		},\n" + "		{\n"
			+ "			\"nameKey\": \"stoneSkill0Name\",\n" + "			\"gameclass\": \"STONE\",\n"
			+ "			\"P1Version\" : 5,\n" + "			\"P1TimeScale\" : 5.0, \n"
			+ "			\"cooldown\": 1.0,\n" + "			\"manacost\": 0,\n" + "			\"feedbacks\":[\n"
			+ "				{\n" + "					\"damage\": -20\n" + "				}\n" + "			]\n"
			+ "		},\n" + "		{\n" + "			\"nameKey\": \"stoneSkill1Name\",\n"
			+ "			\"gameclass\": \"STONE\",\n" + "			\"descriptionKey\": \"stoneSkill1Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_stone_skill1\",\n"
			+ "			\"comboHandFile\": \"stone_skill1_combohand.p\",\n" + "			\"P1Version\" : 0,\n"
			+ "			\"P1TimeScale\" : 6.666, \n" + "			\"cooldown\": 8.0,\n"
			+ "			\"manacost\": 40,\n" + "			\"comboFactorPrim\": 1.0,\n"
			+ "			\"comboFactorAdded\": 1.0,\n" + "			\"feedbacks\":[\n" + "				{\n"
	//		+ "					\"stunDuration\": 2500,\n" + "					\"only_with_combo\": true\n"
			+ "				}\n" + "			]\n" + "		},\n" + "		{\n"
			+ "			\"nameKey\": \"stoneSkill2Name\",\n" + "			\"gameclass\": \"STONE\",\n"
			+ "			\"descriptionKey\": \"stoneSkill2Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_stone_skill2\",\n"
			+ "			\"comboHandFile\": \"stone_skill2_combohand.p\",\n" + "			\"P1Version\" : 0,\n"
			+ "			\"P1TimeScale\" : 6.666, \n" + "			\"cooldown\": 14.0,\n"
			+ "			\"manacost\": 40,\n" + "			\"comboFactorPrim\": 1.0,\n"
			+ "			\"comboFactorAdded\": 1.0,\n" + "			\"feedbacks\":[\n" + "				{\n"
			+ "					\"damage\": -30,\n" + "					\"stunDuration\": 2500\n" + "				}\n"
			+ "			]\n" + "		},\n" + "		{\n" + "			\"nameKey\": \"toxicSkill0Name\",\n"
			+ "			\"gameclass\": \"TOXIC\",\n" + "			\"P1Version\" : 5,\n"
			+ "			\"P1TimeScale\" : 6.666, \n" + "			\"cooldown\": 0.5,\n"
			+ "			\"manacost\": 0,\n" + "			\"feedbacks\":[\n" + "				{\n"
			+ "					\"comment\": \"takes data of toxic skill2\"\n" + "				}\n" + "			]\n"
			+ "		},\n" + "		{\n" + "			\"nameKey\": \"toxicSkill1Name\",\n"
			+ "			\"gameclass\": \"TOXIC\",\n" + "			\"descriptionKey\": \"toxicSkill1Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_toxic_skill1\",\n"
			+ "			\"comboHandFile\": \"toxic_skill1_combohand.p\",\n" + "			\"P1Version\" : 0,\n"
			+ "			\"P1TimeScale\" : 6.666, \n" + "			\"cooldown\": 12.0,\n"
			+ "			\"manacost\": 30,\n" + "			\"comboFactorPrim\": 5.0,\n"
			+ "			\"comboFactorAdded\": 3.0,\n" + "			\"feedbacks\":[\n" + "				{\n"
			+ "					\"comment\": \"takes data of toxic skill2\",\n"
			+ "					\"dotDuration\": 3000\n" + "				}\n" + "			]\n" + "		},\n"
			+ "		{\n" + "			\"nameKey\": \"toxicSkill2Name\",\n" + "			\"gameclass\": \"TOXIC\",\n"
			+ "			\"descriptionKey\": \"toxicSkill2Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_toxic_skill2\",\n"
			+ "			\"comboHandFile\": \"toxic_skill2_combohand.p\",\n" + "			\"P1Version\" : 0,\n"
			+ "			\"P1TimeScale\" : 6.666, \n" + "			\"cooldown\": 8.0,\n"
			+ "			\"manacost\": 30,\n" + "			\"comboFactorPrim\": 1.0,\n"
			+ "			\"comboFactorAdded\": 1.0,\n" + "			\"feedbacks\":[\n" + "				{\n"
			+ "					\"damage\": -30,\n" + "					\"dotAmountPerSec\": -3,\n"
			+ "					\"dotDuration\": 3000\n" + "				}\n" + "			]\n" + "		},\n"
			+ "		{\n" + "			\"nameKey\": \"voidSkill0Name\",\n" + "			\"gameclass\": \"VOID\",\n"
			+ "			\"P1Version\" : 0,\n" + "			\"P1TimeScale\" : 6.666, \n"
			+ "			\"cooldown\": 0.3,\n" + "			\"manacost\": 0,\n" + "			\"feedbacks\":[\n"
			+ "				{\n" + "					\"index\": 0,\n" + "					\"damage\": -14\n"
			+ "				},\n" + "				{\n" + "					\"index\": 1,\n"
			+ "					\"damage\": -14\n" + "				}\n" + "			]\n" + "		},\n"
			+ "		{\n" + "			\"nameKey\": \"voidSkill1Name\",\n" + "			\"gameclass\": \"VOID\",\n"
			+ "			\"descriptionKey\": \"voidSkill1Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_void_skill1\",\n"
			+ "			\"comboHandFile\": \"void_skill1_combohand.p\",\n" + "			\"P1Version\" : 4,\n"
			+ "			\"P1TimeScale\" : 5.0, \n" + "			\"cooldown\": 14.0,\n" + "			\"manacost\": 20,\n"
			+ "			\"comboFactorPrim\": 1.0,\n" + "			\"comboFactorAdded\": 1.0,\n"
			+ "			\"feedbacks\":[\n" + "				{\n" + "					\"duration\": 3000\n"
			+ "				}\n" + "			]\n" + "		},\n" + "		{\n"
			+ "			\"nameKey\": \"voidSkill2Name\",\n" + "			\"gameclass\": \"VOID\",\n"
			+ "			\"descriptionKey\": \"voidSkill2Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_void_skill2\",\n"
			+ "			\"comboHandFile\": \"void_skill2_combohand.p\",\n" + "			\"P1Version\" : 4,\n"
			+ "			\"P1TimeScale\" : 4.0, \n" + "			\"cooldown\": 20.0,\n" + "			\"manacost\": 50,\n"
			+ "			\"comboFactorPrim\": 1.0,\n" + "			\"comboFactorAdded\": 1.0,\n"
			+ "			\"feedbacks\":[\n" + "				{\n" + "					\"silenceDuration\": 4000\n"
			+ "				}\n" + "			]\n" + "		},\n" + "		{\n"
			+ "			\"nameKey\": \"waterSkill0Name\",\n" + "			\"gameclass\": \"WATER\",\n"
			+ "			\"P1Version\" : 4,\n" + "			\"P1TimeScale\" : 10.0,\n"
			+ "			\"cooldown\": 0.8,\n" + "			\"manacost\": 0,\n" + "			\"feedbacks\":[\n"
			+ "				{\n" + "					\"index\": 0,\n" + "					\"damage\": -10\n"
			+ "				},\n" + "				{\n" + "					\"index\": 1,\n"
			+ "					\"heal\": 12\n" + "				}\n" + "			]\n" + "		},\n" + "		{\n"
			+ "			\"nameKey\": \"waterSkill1Name\",\n" + "			\"gameclass\": \"WATER\",\n"
			+ "			\"descriptionKey\": \"waterSkill1Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_water_skill1\",\n"
			+ "			\"comboHandFile\": \"water_skill1_combohand.p\",\n" + "			\"P1Version\" : 4,\n"
			+ "			\"P1TimeScale\" : 8.0, \n" + "			\"cooldown\": 14.0,\n" + "			\"manacost\": 40,\n"
			+ "			\"comboFactorPrim\": 10.0,\n" + "			\"comboFactorAdded\": 10.0,\n"
			+ "			\"feedbacks\":[\n" + "				{\n" + "					\"index\": 0,\n"
			+ "					\"damage\": -10\n" + "				},\n" + "				{\n"
			+ "					\"index\": 1,\n" + "					\"heal\": 10\n" + "				}\n"
			+ "			]\n" + "		},\n" + "		{\n" + "			\"nameKey\": \"waterSkill2Name\",\n"
			+ "			\"gameclass\": \"WATER\",\n" + "			\"descriptionKey\": \"waterSkill2Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_water_skill2\",\n"
			+ "			\"comboHandFile\": \"water_skill2_combohand.p\",\n" + "			\"P1Version\" : 4,\n"
			+ "			\"P1TimeScale\" : 8.0, \n" + "			\"cooldown\": 20.0,\n" + "			\"manacost\": 50,\n"
			+ "			\"comboFactorPrim\": 6.0,\n" + "			\"comboFactorAdded\": 6.0,\n"
			+ "			\"feedbacks\":[\n" + "				{\n" + "					\"heal\": 50,\n"
			+ "					\"hotAmountPerSec\": 10,\n" + "					\"hotDuration\": 5000 \n"
			+ "				}\n" + "			]\n" + "		},\n" + "		{\n"
			+ "			\"nameKey\": \"windSkill0Name\",\n" + "			\"gameclass\": \"WIND\",\n"
			+ "			\"P1Version\" : 5,\n" + "			\"P1TimeScale\" : 5.0, \n"
			+ "			\"cooldown\": 0.5,\n" + "			\"manacost\": 0,\n" + "			\"feedbacks\":[\n"
			+ "				{\n" + "					\"index\": 0,\n" + "					\"damage\": -10\n"
			+ "				}\n" + "			]\n" + "		},\n" + "		{\n"
			+ "			\"nameKey\": \"windSkill1Name\",\n" + "			\"gameclass\": \"WIND\",\n"
			+ "			\"descriptionKey\": \"windSkill1Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_wind_skill1\",\n"
			+ "			\"comboHandFile\": \"wind_skill1_combohand.p\",\n" + "			\"P1Version\" : 5,\n"
			+ "			\"P1TimeScale\" : 5.0, \n" + "			\"cooldown\": 5.0,\n" + "			\"manacost\": 20,\n"
			+ "			\"comboFactorPrim\": 3.0,\n" + "			\"comboFactorAdded\": 1.0,\n"
			+ "			\"feedbacks\":[\n" + "				{\n" + "					\"duration\": 1000\n"
			+ "				}\n" + "			]\n" + "		},\n" + "		{\n"
			+ "			\"nameKey\": \"windSkill2Name\",\n" + "			\"gameclass\": \"WIND\",\n"
			+ "			\"descriptionKey\": \"windSkill2Desc\",\n"
			+ "			\"thumbnail\": \"gui_thumbnail_char_wind_skill2\",\n"
			+ "			\"comboHandFile\": \"wind_skill2_combohand.p\",\n" + "			\"P1Version\" : 7,\n"
			+ "			\"P1TimeScale\" : 1.515, \n" + "			\"cooldown\": 14.0,\n"
			+ "			\"manacost\": 30,\n" + "			\"comboFactorPrim\": 2.0,\n"
			+ "			\"comboFactorAdded\": 1.0,\n" + "			\"feedbacks\":[\n" + "				{\n"
			+ "					\"disarmedDuration\": 2000\n" + "				}\n" + "			]\n" + "		}\n"
			+ "	] \n" + "}";

	public static void setMetaSkillFields() {

		JsonReader json = new JsonReader();
		JsonValue skills = json.parse(SKILLS_JSON);

		ALL_META_SKILLS = new ArrayList<>();

		for (JsonValue componentSkill : skills.get("skills")) {

			MetaSkill metaSkill = new MetaSkill();

			// find name
			if (componentSkill.has("nameKey") == true)
				metaSkill.setNameKey(componentSkill.getString("nameKey"));

			// find gameclass name
			if (componentSkill.has("gameclass") == true)
				metaSkill.setGameclassName(GameclassName.valueOf(componentSkill.getString("gameclass")));

			// find description
			if (componentSkill.has("descriptionKey") == true)
				metaSkill.setDescriptionKey(componentSkill.getString("descriptionKey"));

			// find P1 version
			if (componentSkill.has("P1Version") == true)
				metaSkill.setP1Version(componentSkill.getInt("P1Version"));

			// find P1 time scale
			if (componentSkill.has("P1TimeScale") == true)
				metaSkill.setP1TimeScale(componentSkill.getFloat("P1TimeScale"));

			// find thumbnail sprite
			if (componentSkill.has("thumbnail") == true)
				metaSkill.loadThumbnail(componentSkill.getString("thumbnail"));

			// find combo-hand effect
			if (componentSkill.has("comboHandFile") == true)
				metaSkill.loadComboHandEffects(componentSkill.getString("comboHandFile"));

			// find manacost
			if (componentSkill.has("manacost") == true)
				metaSkill.setManaCost(componentSkill.getInt("manacost"));

			// find cooldown
			if (componentSkill.has("cooldown") == true)
				metaSkill.setCooldown(componentSkill.getFloat("cooldown"));

			// find combo factor prim
			if (componentSkill.has("comboFactorPrim") == true)
				metaSkill.setComboFactorPrim(componentSkill.getFloat("comboFactorPrim"));

			// find combo factor added
			if (componentSkill.has("comboFactorAdded") == true)
				metaSkill.setComboFactorAdded(componentSkill.getFloat("comboFactorAdded"));

			// find order
			if (componentSkill.has("order") == true)
				metaSkill.setOrder(componentSkill.getString("order"));

			if (componentSkill.has("feedbacks") == true) {
				for (JsonValue componentFeedback : componentSkill.get("feedbacks")) {

					MetaFeedback metaFeedback = new MetaFeedback();

					// find index
					if (componentFeedback.has("index") == true)
						metaFeedback.setIndex(componentFeedback.getInt("index"));

					// find slow amount
					if (componentFeedback.has("slowAmount") == true)
						metaFeedback.setSlowAmount("slowAmount", componentFeedback.getFloat("slowAmount"));

					// find slow duration
					if (componentFeedback.has("slowDuration") == true)
						metaFeedback.setSlowDuration("slowDuration", componentFeedback.getInt("slowDuration"));

					// find disarmed duration
					if (componentFeedback.has("disarmedDuration") == true)
						metaFeedback.setDisarmedDuration("disarmedDuration",
								componentFeedback.getInt("disarmedDuration"));

					// find root duration
					if (componentFeedback.has("rootDuration") == true)
						metaFeedback.setRootDuration("rootDuration", componentFeedback.getInt("rootDuration"));

					// find stun duration
					if (componentFeedback.has("stunDuration") == true)
						metaFeedback.setStunDuration("stunDuration", componentFeedback.getInt("stunDuration"));

					// find silence duration
					if (componentFeedback.has("silenceDuration") == true)
						metaFeedback.setSilenceDuration("silenceDuration", componentFeedback.getInt("silenceDuration"));

					// find damage
					if (componentFeedback.has("damage") == true)
						metaFeedback.setDamage("damage", componentFeedback.getInt("damage"));

					// find heal
					if (componentFeedback.has("heal") == true)
						metaFeedback.setHeal("heal", componentFeedback.getInt("heal"));

					// find dot amount
					if (componentFeedback.has("dotAmountPerSec") == true)
						metaFeedback.setDotAmountPerSec("dotAmountPerSec", componentFeedback.getInt("dotAmountPerSec"));

					// find dot duration
					if (componentFeedback.has("dotDuration") == true)
						metaFeedback.setDotDuration("dotDuration", componentFeedback.getInt("dotDuration"));

					// find hot amount
					if (componentFeedback.has("hotAmountPerSec") == true)
						metaFeedback.setHotAmountPerSec("hotAmountPerSec", componentFeedback.getInt("hotAmountPerSec"));

					// find dot duration
					if (componentFeedback.has("hotDuration") == true)
						metaFeedback.setHotDuration("hotDuration", componentFeedback.getInt("hotDuration"));

					// find duration (general)
					if (componentFeedback.has("duration") == true)
						metaFeedback.setDuration("duration", componentFeedback.getInt("duration"));

					metaSkill.getFeedbacks().add(metaFeedback);

				}

				Collections.sort(metaSkill.getFeedbacks());

			}

			ALL_META_SKILLS.add(metaSkill);

		}

		// add two transfer skills
		for (GameclassName gameclassName : GameclassName.values()) {

			ALL_META_SKILLS.add(getMetaTransferSkill(gameclassName, "skill1"));
			ALL_META_SKILLS.add(getMetaTransferSkill(gameclassName, "skill2"));

		}

	}

	/**
	 * 
	 * @param gameclassName
	 * @param order
	 * @return
	 */
	private static MetaSkill getMetaTransferSkill(GameclassName gameclassName, String order) {
		MetaSkill metaSkill = new MetaSkill(gameclassName, order);

		metaSkill.setP1Version(0);
		metaSkill.setP1TimeScale(5f);
		metaSkill.setManaCost(0);
		metaSkill.setCooldown(0f);

		return metaSkill;
	}

	/**
	 * Returns an {@link ArrayList} of {@link ASkill} objects
	 * 
	 * @param drawableClient
	 * @param physicalClient
	 * @param skin0
	 * @return
	 */
	public ArrayList<ASkill> getSkills(DrawableClient drawableClient, PhysicalClient physicalClient, String skinName) {

		/*
		 * some skills don't has extra effects depending on the skin. For those just use
		 * following string instead of the passed skinName parameter.
		 */
		String skin0 = "skin0";

		ArrayList<ASkill> skills = new ArrayList<>();

		skills.add(new SpawnSkillAfterDeath(physicalClient, skin0));
		skills.add(new DyingSkill(drawableClient, physicalClient, skin0));
		switch (name) {
		case ICE:
			skills.add(new IceSkill0(meta_skills.get(0), physicalClient, drawableClient, skin0));
			skills.add(new IceSkill1(meta_skills.get(1), physicalClient, drawableClient, skin0));
			skills.add(new IceSkill2(meta_skills.get(2), physicalClient, drawableClient, skin0));
			break;
		case LEAF:
			skills.add(new LeafSkill0(meta_skills.get(0), physicalClient, drawableClient, skin0));
			skills.add(new LeafSkill1(meta_skills.get(1), physicalClient, drawableClient, skin0));
			skills.add(new LeafSkill2(meta_skills.get(2), physicalClient, drawableClient, skin0));
			break;
		case LIGHT:
			skills.add(new LightSkill0(meta_skills.get(0), physicalClient, drawableClient, skin0));
			skills.add(new LightSkill1(meta_skills.get(1), physicalClient, drawableClient, skin0));
			skills.add(new LightSkill2(meta_skills.get(2), physicalClient, drawableClient, skin0));
			break;
		case LIGHTNING:
			skills.add(new LightningSkill0(meta_skills.get(0), physicalClient, drawableClient, skin0));
			skills.add(new LightningSkill1(meta_skills.get(1), physicalClient, drawableClient, skin0));
			skills.add(new LightningSkill2(meta_skills.get(2), physicalClient, drawableClient, skin0));
			break;
		case SAND:
			skills.add(new SandSkill0(meta_skills.get(0), physicalClient, drawableClient, skin0));
			skills.add(new SandSkill1(meta_skills.get(1), physicalClient, drawableClient, skin0));
			skills.add(new SandSkill2(meta_skills.get(2), physicalClient, drawableClient, skin0));
			break;
		case VOID:
			skills.add(new VoidSkill0(meta_skills.get(0), physicalClient, drawableClient, skin0));
			skills.add(new VoidSkill1(meta_skills.get(1), physicalClient, drawableClient, skin0));
			skills.add(new VoidSkill2(meta_skills.get(2), physicalClient, drawableClient, skin0));
			break;
		case STONE:
			skills.add(new StoneSkill0(meta_skills.get(0), physicalClient, drawableClient, skin0));
			skills.add(new StoneSkill1(meta_skills.get(1), physicalClient, drawableClient, skin0));
			skills.add(new StoneSkill2(meta_skills.get(2), physicalClient, drawableClient, skin0));
			break;
		case TOXIC:
			ToxicSkill2 toxicSkill2 = new ToxicSkill2(meta_skills.get(2), physicalClient, drawableClient, skin0);
			skills.add(new ToxicSkill0(toxicSkill2, meta_skills.get(0), physicalClient, drawableClient, skin0));
			skills.add(new ToxicSkill1(toxicSkill2, meta_skills.get(1), physicalClient, drawableClient, skin0));
			skills.add(toxicSkill2);

			break;
		case WATER:
			skills.add(new WaterSkill0(meta_skills.get(0), physicalClient, drawableClient, skin0));
			skills.add(new WaterSkill1(meta_skills.get(1), physicalClient, drawableClient, skin0));
			skills.add(new WaterSkill2(meta_skills.get(2), physicalClient, drawableClient, skin0));
			break;
		case WIND:
			skills.add(new WindSkill0(meta_skills.get(0), physicalClient, drawableClient, skin0));
			skills.add(new WindSkill1(meta_skills.get(1), physicalClient, drawableClient, skin0));
			skills.add(new WindSkill2(meta_skills.get(2), physicalClient, drawableClient, skin0));
			break;
		}
		skills.add(new TransferSkillUpdated(meta_skills.get(3), physicalClient, drawableClient));
		skills.add(new TransferSkillUpdated(meta_skills.get(4), physicalClient, drawableClient));

		// indexing skills
		int tableSkillIndex = 0;
		for (ASkill skill : skills) {
			skill.setTableSkillIndex(tableSkillIndex);
			tableSkillIndex++;
		}

		return skills;
	}

	public Gameclass(GameclassName name, Role role) {
		this.name = name;
		this.role = role;

		meta_skills = getMetaSkills(name);

		setColor();

		if (Shared.EXECUTION_MODE == ExecutionMode.CLIENT) {
			symbol_gui = AnimationContainer.createCharacterSprite(
					"characters/particle_sprites/gui_thumbnail_char_" + name.toString().toLowerCase() + "_symbol");
			symbol_gui.setScale(0.8f);

			symbol_minimap_team1 = AnimationContainer.createCharacterSprite(
					"characters/minimap_symbol/gui_minimap_" + name.toString().toLowerCase() + "_team1");
			symbol_minimap_team1.setScale(Shared.SCALE_MINIMAP_SYMBOLS);
			symbol_minimap_team2 = AnimationContainer.createCharacterSprite(
					"characters/minimap_symbol/gui_minimap_" + name.toString().toLowerCase() + "_team2");
			symbol_minimap_team2.setScale(Shared.SCALE_MINIMAP_SYMBOLS);

			symbol_statistics = new Sprite(symbol_gui);
			symbol_statistics.setScale(0.5f);

			skill_main_table_world = createSkillMainTableWorld();
			skill_combo_feeding_table_world = createSkillComboFeedbackTableWorld(
					GUITier.getInstance().getStageByDrawOrder(0));
			skill_combo1_table_world = createSkillCombo1TableWorld();
			skill_combo2_table_world = createSkillCombo2TableWorld();
			skill_group_in_selection = createSkillGroupInSelection();
		}
	}

	public float initVelXFactor() {

		float velXFactor = 1;

		switch (name) {
		case STONE:
			velXFactor = 0.8f;
			break;
		case WIND:
		case LIGHT:
		case LIGHTNING:
			velXFactor = 1.05f;
			break;
		case LEAF:
		case SAND:
		case ICE:
		case VOID:
		case TOXIC:
		case WATER:
			velXFactor = 0.9f;
			break;
		}
		return velXFactor;
	}

	private void setColor() {

		color = null;
		switch (name) {
		case SAND:
			color = new float[] { 239 / 255f, 136 / 255f, 86 / 255f };
			break;
		case STONE:
			color = new float[] { 255 / 255f, 212 / 255f, 35 / 255f };
			break;
		case WIND:
			color = new float[] { 114 / 255f, 239 / 255f, 228 / 255f };
			break;
		case ICE:
			color = new float[] { 52 / 255f, 215 / 255f, 255 / 255f };
			break;
		case LEAF:
			color = new float[] { 189 / 255f, 255 / 255f, 45 / 255f };
			break;
		case LIGHT:
			color = new float[] { 255 / 255f, 234 / 255f, 151 / 255f };
			break;
		case LIGHTNING:
			color = new float[] { 88 / 255f, 143 / 255f, 255 / 255f };
			break;
		case VOID:
			color = new float[] { 146 / 255f, 32 / 255f, 255 / 255f };
			break;
		case TOXIC:
			color = new float[] { 112 / 255f, 255 / 255f, 102 / 255f };
			break;
		case WATER:
			color = new float[] { 12 / 255f, 192 / 255f, 255 / 255f };
			break;
		}
	}

	private ArrayList<MetaSkill> getMetaSkills(GameclassName gameclassName) {

		ArrayList<MetaSkill> metaSkills = new ArrayList<>();

		for (MetaSkill skill : ALL_META_SKILLS)

			if (gameclassName.equals(skill.getGameclassName()) == true) {
				metaSkills.add(skill);
			}

		return metaSkills;
	}

	private float pad_horizontal_skills = 125f;

	private CustomTable createSkillMainTableWorld() {
		CustomTable table = new CustomTable();
		table.padBottom(79 * APlayerUIModule.SCALE_UI).padRight(123 * APlayerUIModule.SCALE_UI);

		// create button
		/*
		 * the first skills are already assigned with standard skills (e.g. death...).
		 * The skill index is necessary to get the current cooldown
		 */

		Sprite thumbnail1 = meta_skills.get(1).getThumbnailWorldBig();
		Sprite thumbnail2 = meta_skills.get(2).getThumbnailWorldBig();

		float displayDelayTooltip = 0.3f;

		skill1_button = new SkillButton(meta_skills.get(1), PhysicalClient.SKILL_1_INDEX, thumbnail1,
				displayDelayTooltip);
		skill2_button = new SkillButton(meta_skills.get(2), PhysicalClient.SKILL_2_INDEX, thumbnail2,
				displayDelayTooltip);

		skill1_button.setWidth(thumbnail1.getWidth());
		skill1_button.setHeight(thumbnail1.getHeight());
		skill2_button.setWidth(thumbnail1.getWidth());
		skill2_button.setHeight(thumbnail1.getHeight());

		table.add(skill1_button).padRight(17 * APlayerUIModule.SCALE_UI);
		table.add(skill2_button);

		return table;

	}

	private CustomTable createSkillComboFeedbackTableWorld(CustomStage stage) {
		CustomTable table = new CustomTable();
		table.bottom().setFillParent(true);
		table.padBottom(132 * APlayerUIModule.SCALE_UI).padRight(125 * APlayerUIModule.SCALE_UI);

		table.add(skill1_button.getSpineComboWidget()).padRight(pad_horizontal_skills * APlayerUIModule.SCALE_UI);
		table.add(skill2_button.getSpineComboWidget());

		return table;
	}

	private CustomTable createSkillCombo1TableWorld() {
		CustomTable table = new CustomTable();
		table.padBottom(170 * APlayerUIModule.SCALE_UI).padLeft(214 * APlayerUIModule.SCALE_UI);

		// Combo slot1 button
		SkillButton comboSlot1Btn = new SkillButton(null, -1, null, 0);
		table.add(comboSlot1Btn);

		return table;
	}

	private CustomTable createSkillCombo2TableWorld() {
		CustomTable table = new CustomTable();
		table.padBottom(106 * APlayerUIModule.SCALE_UI).padLeft(293 * APlayerUIModule.SCALE_UI);

		// Combo slot2 button
		SkillButton comboSlot2Btn = new SkillButton(null, -1, null, 0);
		table.add(comboSlot2Btn);

		return table;
	}

	/**
	 * 
	 * @return
	 */
	private HorizontalGroup createSkillGroupInSelection() {
		HorizontalGroup skillGroup = new HorizontalGroup();

		Sprite thumbnail;

		for (MetaSkill metaSkill : meta_skills) {

			thumbnail = metaSkill.getThumbnailSelection();

			// create button
			if (thumbnail != null) {

				SkillButton skillThumbnail = new SkillButton(metaSkill, -1, thumbnail, 0);
				skillThumbnail.setSize(thumbnail.getWidth() + 6, thumbnail.getHeight());

				skillGroup.addActor(skillThumbnail);

			}
		}

		return skillGroup;
	}

	public float[] getColor() {
		return color;
	}

	/**
	 * Returns a vec2 where x is health and y is mana (start values)
	 * 
	 * @return
	 */
	public Vector2 getStartHealthMana() {
		Vector2 healthMana = new Vector2();
		switch (name) {
		/*
		 * Fighter
		 */
		case ICE:
		case LIGHTNING:
		case TOXIC:
			healthMana.set(180, 100);
			break;
		/*
		 * Tank
		 */
		case LEAF:
			healthMana.set(270, 100);
			break;
		case STONE:
			healthMana.set(300, 100);
			break;
		/*
		 * Support
		 */
		case LIGHT:
		case WATER:
		case SAND:
		case VOID:
		case WIND:
			healthMana.set(250, 100);
			break;
		}
		return healthMana;
	}

	/**
	 * Returns a vec2 where x is health regeneration in percent per second and y is
	 * mana regeneration (start values)
	 * 
	 * @return
	 */
	public Vector2 getStartHealthManaRegen() {
		Vector2 healthManaRegen = new Vector2();
		switch (name) {
		case ICE:
		case LEAF:
		case LIGHT:
		case LIGHTNING:
		case SAND:
		case VOID:
		case STONE:
		case TOXIC:
		case WATER:
		case WIND:
			healthManaRegen.set(0.005f, 0.04f); // manaregen before: 0.1f
		}
		return healthManaRegen;
	}

	/**
	 * Returns skill thumbnails as {@link HorizontalGroup} for gameclass selection
	 * 
	 * @return
	 */
	public HorizontalGroup getSkillGroupSelection() {
		return skill_group_in_selection;
	}

	/**
	 * Returns skill thumbnails as {@link CustomTable} for world UI
	 * 
	 * @param physicalClient
	 * @param tables
	 * 
	 * @return
	 */
	public CustomTable getSkillMainTableWorld(PhysicalClient physicalClient) {

		skill1_button.setClient(physicalClient);

		skill2_button.setClient(physicalClient);

		return skill_main_table_world;
	}

	/**
	 * Returns spine feedback widgets as {@link CustomTable} for world UI
	 * 
	 * @return
	 */
	public CustomTable getSkillComboFeedbackTableWorld() {
		return skill_combo_feeding_table_world;
	}

	public CustomTable getSkillCombo1TableWorld(PhysicalClient physicalClient) {

		((SkillButton) skill_combo1_table_world.getChild(0)).setClient(physicalClient);

		return skill_combo1_table_world;
	}

	public CustomTable getSkillCombo2TableWorld(PhysicalClient physicalClient) {

		((SkillButton) skill_combo2_table_world.getChild(0)).setClient(physicalClient);

		return skill_combo2_table_world;
	}

	public Sprite getSymbolMinimapGreen() {
		return symbol_minimap_team1;
	}

	public Sprite getSymbolMinimapRed() {
		return symbol_minimap_team2;
	}

	public Sprite getSymbolStatistics() {
		return symbol_statistics;
	}

	public Sprite getSymbol() {
		return symbol_gui;
	}

	public GameclassName getNameAsEnum() {
		return name;
	}

	/**
	 * name in lowercase
	 * 
	 * @return
	 */
	public String getName() {
		return getName(name);
	}

	public static String getName(GameclassName gameclassEnum) {
		return gameclassEnum != null ? gameclassEnum.name().toLowerCase() : "";
	}

	/**
	 * The field 'ALL' is just to filter
	 */
	public static enum Role {
		ALL, FIGHTER, TANK, SUPPORT
	}

	public static enum GameclassName {
		LEAF, WATER, LIGHT, WIND, STONE, LIGHTNING, VOID, TOXIC, SAND, ICE;
	}

	public Role getRole() {
		return role;
	}

	@Override
	public String toString() {
		return name.name();
	}

	public SkillButton getSkill1Button() {
		return skill1_button;
	}

	public SkillButton getSkill2Button() {
		return skill2_button;
	}

}
