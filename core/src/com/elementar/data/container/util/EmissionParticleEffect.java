package com.elementar.data.container.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Pool.Poolable;

public class EmissionParticleEffect extends CustomParticleEffect implements Poolable {

	private String path;

	public EmissionParticleEffect() {
		super();
	}

	/**
	 * Copy constructor for newObject method of pool;
	 * 
	 * @param effect
	 */
	public EmissionParticleEffect(EmissionParticleEffect effect) {
		super(effect);
		this.path = effect.path;
	}

	public void load(String internalPath, TextureAtlas atlas, String atlasPrefix) {
		path = internalPath;
		if (atlas != null)
			super.load(Gdx.files.internal(internalPath), atlas, atlasPrefix);
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}

}
