package com.elementar.data.container.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.elementar.data.DataTier;
import com.elementar.data.container.AudioData;
import com.elementar.gui.util.ParallaxCamera;
import com.elementar.logic.characters.PhysicalClient;

public class TweenableSound {

	private Sound sound;

	private long sound_id = -1;

	private float volume_begin, volume_tweened;

	private boolean tweening = false, playing = false, locational = false, loop = false, interruptible = false;

	/**
	 * 
	 * @param sound
	 * @param locational, if <code>true</code> the volume depends on distance
	 *                    between player and camera.
	 */
	public TweenableSound(Sound sound, boolean locational) {
		this.sound = sound;
		this.locational = locational;
	}

	public void setVolumeBegin(float volumeBegin) {
		this.volume_begin = volumeBegin;
	}

	public Sound getSoundObj() {
		return sound;
	}

	public void updateVolume(float x, float y) {
		updateVolume(sound_id, x, y);
	}

	/**
	 * if <code>true</code> the sound ends after the death of a projectile is
	 * recognized
	 * 
	 * @param interruptible
	 */
	public void setInterruptible(boolean interruptible) {
		this.interruptible = interruptible;
	}

	public boolean isInterruptible() {
		return interruptible;
	}

	public void updateVolume(long soundID, float x, float y) {

		if (soundID != -1) {

			if (loop == true)
				volume_tweened = AudioData.general_volume * AudioData.effects_volume;

			if (locational == true) {

				float xDiff = x - ParallaxCamera.getInstance().position.x;
				float distanceDiff = ParallaxCamera.getInstance().position.dst(x, y, 0);

				float plateau = 1.5f;

				float panning = panningFunc(xDiff, PhysicalClient.PLAYER_VIEW_RADIUS * 1.7f, plateau);
				float volume = volumeFunc(distanceDiff, PhysicalClient.PLAYER_VIEW_RADIUS * 1.9f, plateau);

				// set pan and volume by distance
				sound.setPan(soundID, panning, volume_tweened * volume);
			} else
				sound.setVolume(soundID, volume_tweened);
		}
	}

	/**
	 * 
	 * @param x
	 * @param maxRadius
	 * @param plateau   describes the area in meters where the panning stays 0, so
	 *                  the sound will be heard on both ears.
	 * @return
	 */
	private float panningFunc(float x, float maxRadius, float plateau) {
		if (Math.abs(x) < plateau)
			return 0;
		else if (x < -maxRadius)
			return -1;
		else if (x > maxRadius)
			return 1;
		else {
			// linear func
			if (x > 0) {
				float m = 1 / (maxRadius - plateau);
				float b = -m * plateau;
				return m * x + b;
			} else {
				float m = -1 / (-maxRadius - (-plateau));
				float b = -m * (-plateau);
				return m * x + b;
			}
		}

	}

	/**
	 * 
	 * @param dist
	 * @param maxRadius
	 * @param plateau   is a radius which describes a circle in meters around the
	 *                  subject. Within that circle the volume stays maximal (= 1f).
	 * @return
	 */
	private float volumeFunc(float dist, float maxRadius, float plateau) {
		if (Math.abs(dist) < plateau)
			return 1;
		else if (dist > maxRadius)
			return 0;
		else {
			// linear func
			float m = (-1) / (maxRadius - plateau);
			float b = -m * plateau + 1;
			return m * dist + b;
		}

	}

	public long playWithoutTween() {

		volume_tweened = AudioData.general_volume * AudioData.effects_volume;

		/*
		 * start with volume = 0, the volume will be updated with updateVolume()
		 */
		sound_id = AudioData.playEffect(sound, 0);

		return sound_id;
	}

	public void stop() {
		sound.stop(sound_id);
	}

	public void setTweenedVolume(float tweenedAlpha) {
		volume_tweened = volume_begin * tweenedAlpha;
	}

	public void setSoundID(long id) {
		this.sound_id = id;
	}

	public void setPlaying(boolean playing) {
		this.playing = playing;
	}

	public void setTweening(boolean tweening) {
		this.tweening = tweening;
	}

	public long getSoundID() {
		return sound_id;
	}

	public boolean isPlaying() {
		return playing;
	}

	public boolean isTweening() {
		return tweening;
	}

	public boolean isLocational() {
		return locational;
	}

}
