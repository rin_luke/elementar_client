package com.elementar.data.container.util;

public class AdvancedKey {

	private int key;

	/**
	 * if <code>true</code> this key is from the keyboard otherwise from the
	 * mouse.
	 */
	private boolean from_keyboard;

	/**
	 * 
	 * @param key
	 * @param fromKeyboard
	 */
	public AdvancedKey(int key, boolean fromKeyboard) {
		this.key = key;
		this.from_keyboard = fromKeyboard;
	}

	public int getKey() {
		return key;
	}

	public boolean isFromKeyboard() {
		return from_keyboard;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public boolean equals(AdvancedKey advancedKey) {
		return advancedKey.from_keyboard == from_keyboard && advancedKey.key == key;
	}

}
