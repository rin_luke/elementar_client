package com.elementar.data.container.util;

import aurelienribon.tweenengine.TweenAccessor;

public class TweenableSoundAccessor implements TweenAccessor<TweenableSound> {

	public static final int FADE_IN = 1;
	public static final int FADE_OUT = 2;

	@Override
	public int getValues(TweenableSound target, int tweenType, float[] returnValues) {

		switch (tweenType) {
		case FADE_IN:
			target.setTweening(true);
			returnValues[0] = 0f;
			return 1;
		case FADE_OUT:
			target.setTweening(true);
			returnValues[0] = 1f;
			return 1;
		}

		return 0;
	}

	@Override
	public void setValues(TweenableSound target, int tweenType, float[] newValues) {

		switch (tweenType) {
		case FADE_IN:
		case FADE_OUT:

			target.setTweenedVolume(newValues[0]);
		}

	}

}
