package com.elementar.data.container.util;

import com.elementar.data.container.util.ParticleEmitter.Particle;

import aurelienribon.tweenengine.TweenAccessor;

public class ParticleAccessor implements TweenAccessor<Particle> {

	public static final int FADE_OUT = 1;

	@Override
	public int getValues(Particle target, int tweenType, float[] returnValues) {

		switch (tweenType) {
		case FADE_OUT:
			returnValues[0] = target.getColor().a;
			return 1;
		}

		return 0;
	}

	@Override
	public void setValues(Particle target, int tweenType, float[] newValues) {

		switch (tweenType) {
		case FADE_OUT:
			target.setAlpha(newValues[0]);
		}

	}

}
