package com.elementar.data.container.util;

import com.elementar.data.container.util.ParticleEmitter.Particle;
import com.elementar.logic.util.Util;

public class OverTimeParticleEffect extends CustomParticleEffect {

	private boolean is_color = false;

	public OverTimeParticleEffect(CustomParticleEffect effect) {
		super(effect);
	}

	/**
	 * Use this method directly after instantiate an {@link OverTimeParticleEffect}
	 * object if it is accompanied by a change in character's color.
	 * 
	 * @param isColor
	 */
	public void setColor(boolean isColor) {
		is_color = isColor;
	}

	public boolean isColor() {
		return is_color;
	}

	/**
	 * flipX for over time effects.
	 * 
	 * @param flipX
	 */
	public void setFlipXOT(boolean flipX) {
	}

	/**
	 * The difference between this method and its parent implementation is that we
	 * only confirm a flip (flip_x = flipX) if the {@link #flipX()} was successful.
	 * E.g. the water ghost particle effect had a specific issue, each time a flip
	 * was required in the same moment of existence the particle effect wasn't
	 * active yet. Now we perform a flip after it is active (worked == true)
	 */
	@Override
	public void setFlipX(boolean flipX) {
		if (flip_x != flipX)
			if (flipX() == true)
				flip_x = flipX;
	}

	@Override
	protected boolean flipX() {
		offset_x *= (-1);

		ParticleEmitter emitter;

		for (int i = 0; i < getEmitters().size; i++) {

			emitter = getEmitters().get(i);

			// flip existing particles
			boolean worked = false;
			Particle[] particles = emitter.getParticles();
			boolean[] active = emitter.getActive();
			for (int j = 0, n = active.length; j < n; j++) {
				if (active[j] == true) {
					Particle particle = particles[j];

					int sign = particle.isFlipX() == false ? 1 : -1;
					particle.translateX(-2 * particle.totalVelX - sign * 2 * emitter.getXOffsetValue().getLowMax());
					particle.totalVelX *= -1;
					particle.angle = Util.switchAngleUnitCircle(particle.angle);
					particle.setFlip(!particle.isFlipX(), false);

					worked = true;

				}
			}
			if (worked == false)
				return false;

			// flip future particles
			commonEmitterFlip(emitter);
		}
		return true;

	}
}
