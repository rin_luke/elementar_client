package com.elementar.data.container.util;

import com.badlogic.gdx.math.MathUtils;
import com.elementar.data.container.ParticleContainer;
import com.elementar.data.container.util.ParticleEmitter.Particle;
import com.elementar.logic.util.Util;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

/**
 * Extension of {@link ParticleEffect}.
 * <p>
 * Unlike the superior class this method has a {@link #flipX()}-method ( because
 * the superior class has only {@link #flipY()} ).
 * <p>
 * Members:
 * <p>
 * 1. Has a corresponding skin
 * <p>
 * 2. Has a corresponding bone
 * <p>
 * 3. Has offset variables to shift the particle effect depending on skin.
 * <p>
 * 4. Has a boolean member to define whether the particle effect has to be drawn
 * before bone or after
 * <p>
 * Note: I decided to set the fields by a single method to avoid a new
 * constructor in the {@link ParticleEffect} hierarchy. With the same
 * constructor I leave the loading process of a particle effect object as it is.
 * Otherwise I would have to write a other load method in
 * {@link ParticleContainer}.
 * 
 * @author lukassongajlo
 * 
 */
public class CustomParticleEffect extends ParticleEffect {

	private static float FADE_OUT_TIME_IN_SEC = 0.3f;

	private String skin;
	private String slot_name;
	protected float offset_x, offset_y;
	
	/**
	 * for character effects this value is relative to bones, for
	 * {@link EmissionParticleEffect} this is relative to all characters, so
	 * <code>true</code> means this effect will be drawn behind all characters and
	 * vice versa.
	 */
	protected boolean draw_behind = true;

	protected boolean flip_x = false;
	private float rotation_angle;

	private float scale = 1;

	protected boolean active = true;
	private boolean fade_out_phase = false;
	private boolean fade_out_done = false;

	private boolean visible = false;

	private float max_life_time;

	public static TweenManager tween_manager = new TweenManager();

	/**
	 * scale is 1 by default
	 */
	public CustomParticleEffect() {
		super();
	}

	/**
	 * scale is 1 by default
	 * 
	 * @param skin
	 * @param slotName
	 * @param offsetX        , x-offset of particle effect when bones rotation angle
	 *                       is 0
	 * @param offsetY        , y-offset of particle effect when bones rotation angle
	 *                       is 0
	 * @param drawBehindBone
	 */
	public CustomParticleEffect(String skin, String slotName, float offsetX, float offsetY, boolean drawBehindBone) {
		super();
		this.skin = skin;
		this.slot_name = slotName;
		this.offset_x = offsetX;
		this.offset_y = offsetY;
		this.draw_behind = drawBehindBone;
	}

	public CustomParticleEffect(CustomParticleEffect effect) {
		super(effect);
		this.skin = effect.skin;
		this.slot_name = effect.slot_name;
		this.offset_x = effect.offset_x;
		this.offset_y = effect.offset_y;
		this.draw_behind = effect.draw_behind;
		this.visible = effect.visible;
	}

	/**
	 * This method is not deprecated and fine to use. Don't use
	 * {@link #setFlip(boolean, boolean)} by {@link ParticleEffect} class
	 * 
	 * @param flipX
	 */
	public void setFlipX(boolean flipX) {

		if (flip_x != flipX)
			flipX();
		flip_x = flipX;
	}

	protected boolean flipX() {
		offset_x *= (-1);

		ParticleEmitter emitter;

		for (int i = 0; i < getEmitters().size; i++) {

			emitter = getEmitters().get(i);

			// flip existing particles
			Particle[] particles = emitter.getParticles();
			boolean[] active = emitter.getActive();
			for (int j = 0, n = active.length; j < n; j++) {
				if (active[j] == true) {
					Particle particle = particles[j];

					if (particle.isFlipX() == !flip_x)
						continue;

					int sign = particle.isFlipX() == false ? 1 : -1;
					particle.translateX(-2 * particle.totalVelX - sign * 2 * emitter.getXOffsetValue().getLowMax());
					particle.totalVelX *= -1;
					particle.angle = Util.switchAngleUnitCircle(particle.angle);
					particle.setFlip(!particle.isFlipX(), false);

				}
			}

			// flip future particles
			commonEmitterFlip(emitter);

		}
		
		return true;

	}

	protected void commonEmitterFlip(ParticleEmitter emitter) {
		if (MathUtils.isZero(emitter.getAngle().getHighMax()) == false
				|| MathUtils.isZero(emitter.getAngle().getLowMax()) == false) {
			float diffMinMax, diffHighLowMin, switchValueHighMin;

			// pre calc values
			diffHighLowMin = emitter.getAngle().getHighMin() - emitter.getAngle().getLowMin();

			diffMinMax = emitter.getAngle().getHighMin() - emitter.getAngle().getHighMax();

			switchValueHighMin = Util.switchAngleUnitCircle(emitter.getAngle().getHighMin());
			// angle
			emitter.getAngle().setHigh(switchValueHighMin, switchValueHighMin + diffMinMax);

			diffMinMax = emitter.getAngle().getLowMax() - emitter.getAngle().getLowMin();

			emitter.getAngle().setLow(switchValueHighMin + diffHighLowMin,
					switchValueHighMin + diffHighLowMin + diffMinMax);

			// pre calc values
			diffHighLowMin = emitter.getRotation().getHighMin() - emitter.getRotation().getLowMin();

			diffMinMax = emitter.getRotation().getHighMin() - emitter.getRotation().getHighMax();

			switchValueHighMin = Util.switchAngleUnitCircle(emitter.getRotation().getHighMin());

			// rotation
			emitter.getRotation().setHigh(switchValueHighMin, switchValueHighMin + diffMinMax);

			diffMinMax = emitter.getRotation().getLowMax() - emitter.getRotation().getLowMin();

			emitter.getRotation().setLow(switchValueHighMin + diffHighLowMin,
					switchValueHighMin + diffHighLowMin + diffMinMax);

			// wind
			emitter.getWind().setHigh(-emitter.getWind().getHighMin(), -emitter.getWind().getHighMax());

			emitter.getWind().setLow(-emitter.getWind().getLowMin(), -emitter.getWind().getLowMax());

			// x offset
			emitter.getXOffsetValue().setLow(-emitter.getXOffsetValue().getLowMin(),
					-emitter.getXOffsetValue().getLowMax());

			// spawn width
			emitter.getSpawnWidth().setLow(-emitter.getSpawnWidth().getLowMin(), -emitter.getSpawnWidth().getLowMax());
			emitter.getSpawnWidth().setHigh(-emitter.getSpawnWidth().getHighMin(),
					-emitter.getSpawnWidth().getHighMax());

		}

	}

	// help variables for setRotation(float)
	float cos, sin, angle_diff, temp_offset_x;

	/**
	 * @param angle
	 */
	public void setRotation(float angle) {
		// only set when different

		if (MathUtils.isEqual(angle, rotation_angle) == false) {

			angle_diff = angle - rotation_angle;
			angle_diff *= flip_x == true ? -1 : 1;
			rotation_angle = angle;

			cos = (float) Math.cos(angle_diff * MathUtils.degreesToRadians);
			sin = (float) Math.sin(angle_diff * MathUtils.degreesToRadians);

			temp_offset_x = offset_x;

			offset_x = temp_offset_x * cos - offset_y * sin;
			offset_y = temp_offset_x * sin + offset_y * cos;

			// change particle effect rotation
			ParticleEmitter emitter;
			for (int i = 0; i < getEmitters().size; i++) {
				emitter = getEmitters().get(i);

				/*
				 * following lines sets the angle and rotation for new particles not for the
				 * current ones. Julian considers that the particles don't live too long, so the
				 * rotation effect is seen quickly. There are also 1 sprite emitters that stays
				 * longer, I have to apply the new rotation immediately to the current active
				 * particle.
				 */
				if (emitter.getActive().length == 1)
					if (emitter.getActive()[0] == true)
						emitter.getParticles()[0].setRotation(angle);

				// angle (direction of effect)
				emitter.getAngle().setHigh(emitter.getAngle().getHighMin() + angle_diff,
						emitter.getAngle().getHighMax() + angle_diff);

				emitter.getAngle().setLow(emitter.getAngle().getLowMin() + angle_diff,
						emitter.getAngle().getLowMax() + angle_diff);

				// rotation (direction of particle sprite!)
				emitter.getRotation().setHigh(emitter.getRotation().getHighMin() + angle_diff,
						emitter.getRotation().getHighMax() + angle_diff);

				emitter.getRotation().setLow(emitter.getRotation().getLowMin() + angle_diff,
						emitter.getRotation().getLowMax() + angle_diff);

				temp_offset_x = emitter.getXOffsetValue().getLowMin();

				// x/y offset (assume that min = max)
				emitter.getXOffsetValue().setLowMin(temp_offset_x * cos - emitter.getYOffsetValue().getLowMin() * sin);
				emitter.getYOffsetValue().setLowMin(temp_offset_x * sin + emitter.getYOffsetValue().getLowMin() * cos);
				// min = max:
				emitter.getXOffsetValue().setLowMax(emitter.getXOffsetValue().getLowMin());
				emitter.getYOffsetValue().setLowMax(emitter.getYOffsetValue().getLowMin());

			}
		}
	}

	/**
	 * 
	 * @param delta, in seconds
	 */
	public void stopMovementEffect(float delta) {

		if (fade_out_phase == false) {
			fade_out_phase = true;

			max_life_time = 5000;

			ParticleEmitter emitter;
			for (int i = 0; i < getEmitters().size; i++) {

				emitter = getEmitters().get(i);

				if (max_life_time > emitter.getLife().getHighMax())
					max_life_time = emitter.getLife().getHighMax();

				emitter.getEmission().setHigh(0f);
			}

			// convert ms in seconds
			max_life_time /= 1000f;

		}
		if (fade_out_phase == true) {
			max_life_time -= delta;

			if (max_life_time < 0)
				fade_out_done = true;

		}

	}

	@Override
	public void reset() {
		fade_out_phase = false;
		fade_out_done = false;
		flip_x = false;
		super.reset();
	}

	@Override
	public void reset(boolean resetScaling) {
		fade_out_phase = false;
		fade_out_done = false;
		flip_x = false;
		super.reset(resetScaling);
	}

	/**
	 * Returns <code>true</code> if one particle did the tween-tour or if no
	 * particle lives after projectile has died.
	 * 
	 * @return
	 */
	public boolean isFadeOutDone() {
		return fade_out_done;
	}

	/**
	 * initiate the fade out phase
	 */
	public void fadeOut() {
		if (fade_out_phase == false) {
			fade_out_phase = true;

			boolean noActiveParticles = true;

			for (ParticleEmitter emitter : getEmitters()) {
				// current particles
				for (int i = 0; i < emitter.getActive().length; i++)
					if (emitter.getActive()[i] == true) {

						noActiveParticles = false;

						// init tweening process
						Tween.to(emitter.getParticles()[i], ParticleAccessor.FADE_OUT, FADE_OUT_TIME_IN_SEC).target(0)
								.ease(TweenEquations.easeOutCubic).start(tween_manager)
								.setCallback(new TweenCallback() {

									@Override
									public void onEvent(int arg0, BaseTween<?> arg1) {
										fade_out_done = true;
									}
								});

					}

				if (noActiveParticles == true)
					fade_out_done = true;

			}
		}
	}

	@Override
	public void scaleEffect(float scaleFactor) {
		// this.reset();
		super.scaleEffect(scaleFactor / scale);
		offset_x = offset_x * (scaleFactor / scale);
		offset_y = offset_y * (scaleFactor / scale);
		this.scale = scaleFactor;
	}

	public float getScale() {
		return scale;
	}

	public String getSkin() {
		return skin;
	}

	public String getSlotName() {
		return slot_name;
	}

	/**
	 * If <code>true</code> this particle effect should be drawn behind bone sprite.
	 * For {@link EmissionParticleEffect} this is relative to all characters, so
	 * <code>true</code> means this effect will be drawn behind all characters and
	 * vice versa.
	 * 
	 * @return
	 */
	public boolean isDrawBehind() {
		return draw_behind;
	}

	public void setDrawBehind(boolean drawBehind) {
		draw_behind = drawBehind;
	}

	public float getOffsetX() {
		return offset_x;
	}

	public float getOffsetY() {
		return offset_y;
	}

	public void setSlotName(String slotName) {
		slot_name = slotName;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * By default equals <code>true</code>
	 * 
	 * @return
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isVisible() {
		return visible;
	}

	public boolean isFlipX() {
		return flip_x;
	}

	/**
	 * 
	 * @param visible
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

}
