package com.elementar.data.container.util;

import com.elementar.data.container.ParticleContainer;
import com.elementar.data.container.util.Gameclass.GameclassName;

/**
 * Note that particle effects are separately handled by the
 * {@link ParticleContainer} class
 * 
 * @author lukassongajlo
 *
 */
public class GameclassSkin {

	private int item_def_id;

	private int index;

	private String description;

	/**
	 * e.g. ice_skin1
	 */
	private String technical_description;

	private GameclassName gameclass_name;

	private SkinAvailability availability;

	private String currency;

	private int price_in_cents;

	/**
	 * each character has an active/equipped skin that will be used for matches. If
	 * a skin is locked because of a quest or is not purchased, the skin can't be
	 * equipped.
	 */
	private boolean equipped;

	/**
	 * 
	 * @param rareness
	 * @param presentationName
	 */
	public GameclassSkin() {
	}

	public void setItemDefID(int itemDefID) {
		item_def_id = itemDefID;
	}

	public int getItemDefID() {
		return item_def_id;
	}

	public void setDescription(String presentationName) {
		description = presentationName;
	}

	/**
	 * 
	 * @param currency
	 * @param price    in cents
	 */
	public void setPrice(String currency, int price) {
		this.price_in_cents = price;
		this.currency = currency;
	}

	public String getCurrency() {
		return currency;
	}

	/**
	 * 
	 * @return
	 */
	public String getPrice() {
		return ("" + (price_in_cents / 100)).toString() + "." + ("" + (price_in_cents % 100)).toString();
	}

	/**
	 * Range within a gameclass.
	 * 
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	public void setGameclassName(GameclassName gameclassName) {
		this.gameclass_name = gameclassName;
	}

	public GameclassName getGameclassName() {
		return gameclass_name;
	}

	public void setAvailability(SkinAvailability availability) {
		this.availability = availability;
	}

	public void setEquipped(boolean equipped) {
		this.equipped = equipped;
	}

	/**
	 * starting with 0
	 * 
	 * @return
	 */
	public int getIndex() {
		return index;
	}

	public SkinAvailability getAvailability() {
		return availability;
	}

	public boolean isEquipped() {
		return equipped;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return super.toString() + ", skin = " + gameclass_name + "_" + index;
	}

	public static enum SkinAvailability {
		AVAILABLE, NOT_PAID
	}

	public void setTechnicalDescription(String techDesc) {
		technical_description = techDesc;
	}

	public String getTechnicalDescription() {
		return technical_description;
	}

}
