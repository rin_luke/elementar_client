package com.elementar.data.container;

import java.util.HashMap;
import java.util.Map.Entry;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.elementar.data.container.util.AdvancedKey;
import com.elementar.logic.util.Util;

/**
 * The {@link WorldAction} enum describes player actions inside the world.
 * <p>
 * A configuration is map consists of Integer keys (of {@link Keys}) and a
 * corresponding action. So the player decides which key leads to a specific
 * action. For example the key 'A' is the default configuration for walking to
 * the left. Players can set this to 'S' if they want.
 * 
 * @author lukassongajlo
 *
 */
public class KeysConfiguration {

	public static int KEY_TRANSFER_SKILL1 = -1;
	public static int KEY_TRANSFER_SKILL2 = -2;

	public enum WorldAction {
		/**
		 * movement
		 */
		MOVE_LEFT(0, 1), MOVE_RIGHT(1, 2), JUMP(2, 4), HOLD(3, 8),
		/**
		 * chat
		 */
		OPEN_CHAT(4, 16),
		/**
		 * skills
		 */
		SWITCH_COMBO_MODE(5, 32), EXECUTE_SKILL0(6, 64), EXECUTE_SKILL1(7, 128), EXECUTE_SKILL2(8, 256),
		EXECUTE_TRANSFER_SKILL1(9, 512), EXECUTE_TRANSFER_SKILL2(10, 1024), SKILL0_SUCCESSFUL_CLIENTSIDE(11, 2048),
		SKILL1_SUCCESSFUL_CLIENTSIDE(12, 4096), SKILL2_SUCCESSFUL_CLIENTSIDE(13, 8192),
		OPEN_PLAYER_STATISTICS(14, 16384);

		private short value;
		private int index;

		WorldAction(int index, int value) {
			this.index = index;
			this.value = (short) value;
		}

		public int getIndex() {
			return index;
		}

		public short getValue() {
			return value;
		}

	}

	/**
	 * Maps an Integer (which is key of {@link Keys}) to a corresponding
	 * {@link WorldAction}
	 */
	private static HashMap<Integer, WorldAction> keyboard_configuration = new HashMap<>();

	private static HashMap<Integer, WorldAction> mouse_configuration = new HashMap<>();

	/**
	 * Default mapping for all {@link WorldAction} fields with a int key of
	 * {@link Keys}
	 */
	private static HashMap<Integer, WorldAction> built_in_keyboard_configuration = new HashMap<>();

	private static HashMap<Integer, WorldAction> built_in_mouse_configuration = new HashMap<>();

	/**
	 * Loads the build-in configuration and actions map
	 */
	public static void loadDefault() {

		built_in_keyboard_configuration.put(Keys.A, WorldAction.MOVE_LEFT);
		built_in_keyboard_configuration.put(Keys.D, WorldAction.MOVE_RIGHT);
		built_in_keyboard_configuration.put(Keys.Q, WorldAction.EXECUTE_SKILL1);
		built_in_keyboard_configuration.put(Keys.E, WorldAction.EXECUTE_SKILL2);
		built_in_keyboard_configuration.put(Keys.SPACE, WorldAction.JUMP);
		built_in_keyboard_configuration.put(Keys.SHIFT_LEFT, WorldAction.HOLD);
		built_in_keyboard_configuration.put(Keys.ENTER, WorldAction.OPEN_CHAT);
		built_in_keyboard_configuration.put(Keys.TAB, WorldAction.OPEN_PLAYER_STATISTICS);
		built_in_keyboard_configuration.put(KEY_TRANSFER_SKILL1, WorldAction.EXECUTE_TRANSFER_SKILL1);
		built_in_keyboard_configuration.put(KEY_TRANSFER_SKILL2, WorldAction.EXECUTE_TRANSFER_SKILL2);

		built_in_mouse_configuration.put(Buttons.LEFT, WorldAction.EXECUTE_SKILL0);
		built_in_mouse_configuration.put(Buttons.RIGHT, WorldAction.SWITCH_COMBO_MODE);
	}

	/**
	 * O(1)-operation. Returns action by passing the key which is in an integer of
	 * {@link Keys}
	 * 
	 * @param advancedKey
	 * @return
	 */
	public static WorldAction getAction(AdvancedKey advancedKey) {
		if (advancedKey != null) {
			if (advancedKey.isFromKeyboard() == true)
				return keyboard_configuration.get(advancedKey.getKey());
			else
				return mouse_configuration.get(advancedKey.getKey());

		}
		return null;
	}

	/**
	 * Sets the {@link #keyboard_configuration} or {@link #mouse_configuration} for
	 * a given {@link WorldAction}.
	 * 
	 * @param worldAction
	 * @param keycode
	 */
	public static void setNewConfig(AdvancedKey advancedKey, WorldAction worldAction) {
		// remove previous assignment
		removeByAction(worldAction);

		// new assignment
		if (advancedKey.isFromKeyboard() == true)
			keyboard_configuration.put(advancedKey.getKey(), worldAction);
		else
			mouse_configuration.put(advancedKey.getKey(), worldAction);
	}

	private static void removeByAction(WorldAction action) {

		Util.removeByValue(keyboard_configuration, action);
		Util.removeByValue(mouse_configuration, action);

	}

	/**
	 * O(n)-operation, because iterating over actions
	 * 
	 * @param action
	 * @return the key if the action is mapped, otherwise the returned
	 *         {@link Integer} is <code>null</code>
	 */
	public static AdvancedKey getKeycode(WorldAction action) {
		for (Entry<Integer, WorldAction> entry : keyboard_configuration.entrySet())
			if (entry.getValue() == action)
				return new AdvancedKey(entry.getKey(), true);
		for (Entry<Integer, WorldAction> entry : mouse_configuration.entrySet())
			if (entry.getValue() == action)
				return new AdvancedKey(entry.getKey(), false);
		return null;
	}

	/**
	 * O(n)-operation, because iterating over actions
	 * 
	 * @param action
	 * @return the key if the action is mapped, otherwise the returned
	 *         {@link Integer} is <code>null</code>
	 */
	public static AdvancedKey getKeycodeBuiltIn(WorldAction action) {
		for (Entry<Integer, WorldAction> entry : built_in_keyboard_configuration.entrySet())
			if (entry.getValue() == action)
				return new AdvancedKey(entry.getKey(), true);
		for (Entry<Integer, WorldAction> entry : built_in_mouse_configuration.entrySet())
			if (entry.getValue() == action)
				return new AdvancedKey(entry.getKey(), false);
		return null;
	}

	public static HashMap<Integer, WorldAction> getKeyboardConfiguration() {
		return keyboard_configuration;
	}

	public static HashMap<Integer, WorldAction> getMouseConfiguration() {
		return mouse_configuration;
	}

}
