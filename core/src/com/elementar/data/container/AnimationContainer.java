package com.elementar.data.container;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Pool;
import com.elementar.data.DataTier;
import com.elementar.gui.util.BGSkeleton;
import com.elementar.gui.util.CustomSkeleton;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.ExecutionMode;
import com.elementar.logic.util.Shared.Team;
import com.esotericsoftware.spine.SkeletonJson;
import com.esotericsoftware.spine.Skin;
import com.esotericsoftware.spine.attachments.AttachmentLoader;
import com.esotericsoftware.spine.attachments.BoundingBoxAttachment;
import com.esotericsoftware.spine.attachments.ClippingAttachment;
import com.esotericsoftware.spine.attachments.MeshAttachment;
import com.esotericsoftware.spine.attachments.PathAttachment;
import com.esotericsoftware.spine.attachments.PointAttachment;
import com.esotericsoftware.spine.attachments.RegionAttachment;

/**
 * Contains all animations that are needed inside the world as well as in the
 * main-menu. Provides also methods to create new animations like
 * {@link #makeCharacterAnimation(float)}
 * 
 * @author lukassongajlo
 * 
 */
public class AnimationContainer {

	public static float SCALE_HOME_SCREEN = 0.7f;

	public static TextureAtlas atlas_characters, atlas_bg1_menu, atlas_buildings, atlas_world_bg, atlas_home_screen;

	/**
	 * for each animation one skeleton
	 */
	public static CustomSkeleton skeleton_bg1_menu, skeleton_base_left, skeleton_base_right, skeleton_victory,
			skeleton_defeat, skeleton_comborose_head_left, skeleton_comborose_head_right,
			skeleton_combo_rose_skill1_left, skeleton_combo_rose_skill2_left, skeleton_combo_rose_skill1_right,
			skeleton_combo_rose_skill2_right;

	public static CustomSkeleton skeleton_home_screen_host, skeleton_home_screen_play, skeleton_home_screen_discord,
			skeleton_home_screen_platform, skeleton_home_screen_title;

	public static ArrayList<BGSkeleton> bg1_skeletons = new ArrayList<>();
	public static ArrayList<BGSkeleton> bg2_skeletons = new ArrayList<>();
	public static ArrayList<BGSkeleton> bg3_skeletons = new ArrayList<>();
	public static ArrayList<BGSkeleton> bg4_skeletons = new ArrayList<>();

	private static CreepSkeletonPool creep_skeleton_pool;

	public static void loadServerBuildings() {
		skeleton_base_left = makeBaseAnimation("base_team1", Team.TEAM_1);
		skeleton_base_right = makeBaseAnimation("base_team2", Team.TEAM_2);
	}

	public static void load() {
		AssetManager manager = DataTier.ASSET_MANAGER;
		manager.load("graphic/gui_bg1_menu.atlas", TextureAtlas.class);
		manager.load("graphic/animation_characters.atlas", TextureAtlas.class);
		manager.load("graphic/animation_world_building.atlas", TextureAtlas.class);
		manager.load("graphic/animation_world_bg.atlas", TextureAtlas.class);
		manager.load("graphic/animation_home_screen.atlas", TextureAtlas.class);
	}

	public static void setFields() {
		AssetManager manager = DataTier.ASSET_MANAGER;

		// atlas
		atlas_bg1_menu = manager.get("graphic/gui_bg1_menu.atlas", TextureAtlas.class);
		atlas_characters = manager.get("graphic/animation_characters.atlas", TextureAtlas.class);
		atlas_buildings = manager.get("graphic/animation_world_building.atlas", TextureAtlas.class);
		atlas_world_bg = manager.get("graphic/animation_world_bg.atlas", TextureAtlas.class);
		atlas_home_screen = manager.get("graphic/animation_home_screen.atlas", TextureAtlas.class);

		skeleton_bg1_menu = makeAnimation(atlas_bg1_menu, Gdx.files.internal("graphic/gui_bg1_menu.json"), 0.8f);

		skeleton_base_left = makeBaseAnimation("base_team1", Team.TEAM_1);
		skeleton_base_right = makeBaseAnimation("base_team2", Team.TEAM_2);

		skeleton_victory = makeAnimation(atlas_buildings, Gdx.files.internal("graphic/victory-defeat.json"));
		skeleton_victory.setSkin("victory");

		skeleton_defeat = makeAnimation(atlas_buildings, Gdx.files.internal("graphic/victory-defeat.json"));
		skeleton_defeat.setSkin("defeat");

		// begin home screen
		skeleton_home_screen_host = makeHomeScreenAnimation(
				Gdx.files.internal("graphic/animation_home_screen_button_big.json"));
		skeleton_home_screen_host.setSkin("host");

		skeleton_home_screen_play = makeHomeScreenAnimation(
				Gdx.files.internal("graphic/animation_home_screen_button_big.json"));
		skeleton_home_screen_play.setSkin("play");

		skeleton_home_screen_discord = makeHomeScreenAnimation(
				Gdx.files.internal("graphic/animation_home_screen_button_big.json"));
		skeleton_home_screen_discord.setSkin("discord");

		skeleton_home_screen_platform = makeHomeScreenAnimation(
				Gdx.files.internal("graphic/animation_home_screen_skin_platform.json"));

		skeleton_home_screen_title = makeHomeScreenAnimation(
				Gdx.files.internal("graphic/animation_home_screen_skin_title.json"));
		skeleton_home_screen_title.setSkin("ice_skin1");
		// end home screen

		// begin combo rose

		// head slots
//		skeleton_comborose_head_left = makeAnimation(atlas_characters,
//				Gdx.files.internal("graphic/animation_comborose_head.json"));
//		skeleton_comborose_head_left.setSkin("comborose_head_basic");
//
//		skeleton_comborose_head_right = makeAnimation(atlas_characters,
//				Gdx.files.internal("graphic/animation_comborose_head.json"));
//		skeleton_comborose_head_right.setSkin("comborose_head_basic");

		// skill slots
//		skeleton_combo_rose_skill1_left = makeAnimation(atlas_characters,
//				Gdx.files.internal("graphic/animation_comborose_skill.json"));
//		skeleton_combo_rose_skill1_left.setSkin("comborose_skill_basic");
//
//		skeleton_combo_rose_skill2_left = makeAnimation(atlas_characters,
//				Gdx.files.internal("graphic/animation_comborose_skill.json"));
//		skeleton_combo_rose_skill2_left.setSkin("comborose_skill_basic");
//
//		skeleton_combo_rose_skill1_right = makeAnimation(atlas_characters,
//				Gdx.files.internal("graphic/animation_comborose_skill.json"));
//		skeleton_combo_rose_skill1_right.setSkin("comborose_skill_basic");
//
//		skeleton_combo_rose_skill2_right = makeAnimation(atlas_characters,
//				Gdx.files.internal("graphic/animation_comborose_skill.json"));
//		skeleton_combo_rose_skill2_right.setSkin("comborose_skill_basic");
		// end combo rose

		creep_skeleton_pool = new CreepSkeletonPool();

		// world backgrounds
		addBGSekelton(bg1_skeletons, Gdx.files.internal("graphic/world_bg1.json"), "");
		for (int i = 0; i < 6; i++)
			addBGSekelton(bg2_skeletons, Gdx.files.internal("graphic/world_bg2.json"), "world_bg2");
		for (int i = 0; i < 6; i++)
			addBGSekelton(bg3_skeletons, Gdx.files.internal("graphic/world_bg3.json"), "world_bg3");
		for (int i = 0; i < 6; i++)
			addBGSekelton(bg4_skeletons, Gdx.files.internal("graphic/world_bg4.json"), "world_bg4");
	}

	private static CustomSkeleton makeHomeScreenAnimation(FileHandle jsonFileHandle) {
		CustomSkeleton skeleton = makeAnimation(atlas_home_screen, jsonFileHandle);
		skeleton.setScale(SCALE_HOME_SCREEN, SCALE_HOME_SCREEN);
		return skeleton;
	}

	public static CustomSkeleton makeHomeScreenAnimation(FileHandle jsonFileHandle, float scale) {
		CustomSkeleton skeleton = makeAnimation(atlas_home_screen, jsonFileHandle);
		skeleton.setScale(scale, scale);
		return skeleton;
	}

	private static void addBGSekelton(ArrayList<BGSkeleton> skeletons, FileHandle json, String animationName) {
		BGSkeleton skeleton = makeBGAnimation(atlas_world_bg, json, Shared.SCALE_BG, animationName);
		skeletons.add(skeleton);
	}

	/**
	 * Creates a sprite from {@link #atlas_characters}
	 * 
	 * @param name
	 * @return
	 */
	public static Sprite createCharacterSprite(String name) {
		return atlas_characters.createSprite(name);
	}

	public static CustomSkeleton obtainCreepSkeleton() {
		return creep_skeleton_pool.obtain();
	}

	public static void freeCreepSkeleton(CustomSkeleton creepSkeleton) {
		creep_skeleton_pool.free(creepSkeleton);
	}

	/**
	 * Returns a skeleton object.
	 * 
	 * @param atlas
	 * @param jsonFile
	 * @return
	 */
	public static CustomSkeleton makeAnimation(TextureAtlas atlas, FileHandle jsonFile) {
		return makeAnimation(atlas, jsonFile, 1);
	}

	/**
	 * Returns a skeleton object with a specific scale.
	 * 
	 * @param atlas
	 * @param jsonFile
	 * @param scale
	 * @return
	 */
	private static BGSkeleton makeBGAnimation(TextureAtlas atlas, FileHandle jsonFile, float scale,
			String animationName) {

		SkeletonJson json = null;
		if (Shared.EXECUTION_MODE == ExecutionMode.GAMESERVER)
			json = new SkeletonJson(new GameserverAttachmentLoader());
		else
			json = new SkeletonJson(atlas);
		json.setScale(scale);
		return new BGSkeleton(json.readSkeletonData(jsonFile), scale, animationName);

	}

	/**
	 * Returns a skeleton object with a specific scale.
	 * 
	 * @param atlas
	 * @param jsonFile
	 * @param scale
	 * @return
	 */
	public static CustomSkeleton makeAnimation(TextureAtlas atlas, FileHandle jsonFile, float scale) {

		SkeletonJson json = null;
		if (Shared.EXECUTION_MODE == ExecutionMode.GAMESERVER)
			json = new SkeletonJson(new GameserverAttachmentLoader());
		else
			json = new SkeletonJson(atlas);
		json.setScale(scale);
		return new CustomSkeleton(json.readSkeletonData(jsonFile), scale);

	}

	private static CustomSkeleton makeCreepAnimationForWorld() {
		return makeAnimation(atlas_characters, Gdx.files.internal("graphic/animation_characters.json"),
				Shared.SCALE_CREEP_INGAME);
	}

	/**
	 * Returns a skeleton object for new a character animation with a specific scale
	 * value.
	 * 
	 * @param scale
	 * @return
	 */
	public static CustomSkeleton makeCharacterAnimation(float scale) {
		return makeAnimation(atlas_characters, Gdx.files.internal("graphic/animation_characters.json"), scale);

	}

	/**
	 * Like {@link #makeCharacterAnimation(float)} but with a fixed scale value
	 * ({@link Shared#SCALE_CHARACTER_INGAME}) suited for characters inside the
	 * world.
	 * 
	 * @return
	 */
	public static CustomSkeleton makeCharacterAnimationForWorld() {
		return makeCharacterAnimation(Shared.SCALE_CHARACTER_INGAME);

	}

	/**
	 * Makes a shrine animation with a fixed scale value (
	 * {@link Shared#SCALE_BUILDINGS}). A skin will be set.
	 * 
	 * @return
	 */
	public static CustomSkeleton makeShrineAnimation() {
		CustomSkeleton shrineSkeleton = makeAnimation(atlas_buildings,
				Gdx.files.internal("graphic/world_building_shrine.json"), Shared.SCALE_BUILDINGS);
		// shrineSkeleton.setSkin("shrine1");
		return shrineSkeleton;
	}

	/**
	 * Makes a tower animation with a fixed scale value (
	 * {@link Shared#SCALE_BUILDINGS}). A skin will be set.
	 * 
	 * @param baseTower , if <code>true</code> it creates a base tower otherwise a
	 *                  normal tower
	 * @param alive     , if <code>false</code> the "world_tower_destroyed" skin is
	 *                  chosen, otherwise the "world_tower_team1"
	 * @return
	 * 
	 */
	public static CustomSkeleton makeTowerAnimation() {
		CustomSkeleton towerSkeleton = makeAnimation(atlas_buildings,
				Gdx.files.internal("graphic/world_building_tower.json"), Shared.SCALE_BUILDINGS);

		towerSkeleton.setSkin("world_tower_team1");

		return towerSkeleton;
	}

	/**
	 * 
	 * @param skinName
	 * @param team
	 * @return
	 */
	public static CustomSkeleton makeBaseAnimation(String skinName, Team team) {
		CustomSkeleton baseSkeleton = makeAnimation(atlas_buildings,
				Gdx.files.internal("graphic/world_building_base.json"), Shared.SCALE_BUILDINGS);
		boolean flip = team == Team.TEAM_2;
		baseSkeleton.setSkin(skinName);
		baseSkeleton.setFlipX(flip);

		return baseSkeleton;
	}

	private static class CreepSkeletonPool extends Pool<CustomSkeleton> {

		@Override
		protected CustomSkeleton newObject() {
			return makeCreepAnimationForWorld();
		}

	}

	/**
	 * Ignores visuals, just loading bounding boxes
	 * 
	 * @author lukassongajlo
	 *
	 */
	private static class GameserverAttachmentLoader implements AttachmentLoader {

		@Override
		public RegionAttachment newRegionAttachment(Skin skin, String name, String path) {
			return null;
		}

		@Override
		public MeshAttachment newMeshAttachment(Skin skin, String name, String path) {
			return null;
		}

		@Override
		public BoundingBoxAttachment newBoundingBoxAttachment(Skin skin, String name) {
			return new BoundingBoxAttachment(name);
		}

		@Override
		public ClippingAttachment newClippingAttachment(Skin skin, String name) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public PathAttachment newPathAttachment(Skin skin, String name) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public PointAttachment newPointAttachment(Skin skin, String name) {
			// TODO Auto-generated method stub
			return null;
		}

	}

}
