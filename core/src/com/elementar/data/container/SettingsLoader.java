package com.elementar.data.container;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.elementar.data.container.KeysConfiguration.WorldAction;
import com.elementar.data.container.util.AdvancedKey;
import com.elementar.data.container.util.GameclassSkin;
import com.elementar.gui.modules.options.OptionsModule;
import com.elementar.gui.modules.selection.GameclassCollectionModule;
import com.elementar.gui.widgets.GameclassIconbutton;
import com.elementar.gui.widgets.ResolutionDropdown;
import com.elementar.logic.util.Shared.CategorySize;

/**
 * Contains a {@link Preferences} object which is saved as file in player's file
 * system. Within that file there are options which the player had set in
 * {@link OptionsModule}. So after a restart of the game the player hasn't to
 * re-adjust his options.
 * <p>
 * This class provides load-methods which load the informations from the file in
 * a suited array. (e.g. {@link #loadAudioVolume()} )
 * <p>
 * To save informations to the file, call {@link #save()}.
 * 
 * @author lukassongajlo
 * 
 */
public class SettingsLoader {

	private final static Preferences settings = Gdx.app.getPreferences("MYSETTINGSx7");
	private final static Preferences settings_audio = Gdx.app.getPreferences("MYSETTINGSAUDIO2");

	public static float[] loadAudioVolume() {
		float defaultVolume = 0.5f;
		float arr[] = new float[4];
		arr[0] = settings_audio.getFloat("generalVolume", defaultVolume);
		arr[1] = settings_audio.getFloat("musicVolume", defaultVolume);
		arr[2] = settings_audio.getFloat("effectsVolume", defaultVolume);
		arr[3] = settings_audio.getFloat("ambientVolume", defaultVolume);
		return arr;
	}

	public static String[] loadGraphicSettings() {
		String arr[] = new String[8];
		arr[0] = String.valueOf(settings.getBoolean("fpsOn", false));
		arr[1] = String.valueOf(settings.getBoolean("fullscreenOn", true));
		arr[2] = String.valueOf(settings.getInteger("vegetationInteractionLevel", CategorySize.values().length));

		String defaultAspectRatio = ResolutionDropdown.getAspectRatioString(
				((float) (Gdx.graphics.getDisplayMode().width)) / Gdx.graphics.getDisplayMode().height);
		arr[3] = settings.getString("aspectRatio", defaultAspectRatio);

		String defaultResolution = Gdx.graphics.getDisplayMode().width + "x" + Gdx.graphics.getDisplayMode().height;
		arr[4] = settings.getString("resolution", defaultResolution);
		arr[5] = String.valueOf(settings.getBoolean("vSyncOn", false));
		arr[6] = String.valueOf(settings.getBoolean("minimapLeft", true));
		arr[7] = String.valueOf(settings.getFloat("comboRoseSensitivity", 0.5f));
		return arr;
	}

	/**
	 * 
	 * @param skinTechnicalDescription e.g. ice_skin1
	 * @return
	 */
	public static boolean loadSkinEquipped(String skinTechnicalDescription) {
		return settings.getBoolean("equipped_" + skinTechnicalDescription, false);
	}

	public static String loadCurrentLanguage() {
		return settings.getString("language", "EN");
	}

	public static void loadKeysConfiguration() {
		// load standard configuration
		KeysConfiguration.loadDefault();

		// load custom configuration
		for (WorldAction action : WorldAction.values()) {

			// advString holds "<key>.<bool>", for example "3.true".
			String string[] = settings.getString(action.name(), "0.true").split("\\.");
			AdvancedKey advancedKey = new AdvancedKey(Integer.valueOf(string[0]), Boolean.valueOf(string[1]));
			if (advancedKey.getKey() == 0) {
				advancedKey = KeysConfiguration.getKeycodeBuiltIn(action);
				if (advancedKey == null)
					continue;
			}

			KeysConfiguration.setNewConfig(advancedKey, action);
		}
	}

	/**
	 * data of the last connection have to be saved separately
	 * 
	 * @param ipGameserver
	 * @param matchID
	 * @param connectionID
	 * @param isHost
	 */
	public static void saveAudio() {
		settings_audio.clear();

		settings_audio.putFloat("generalVolume", AudioData.general_volume);
		settings_audio.putFloat("musicVolume", AudioData.music_volume);
		settings_audio.putFloat("effectsVolume", AudioData.effects_volume);
		settings_audio.putFloat("ambientVolume", AudioData.ambient_volume);

		settings_audio.flush();
	}

	public static void save() {
		settings.clear();

		settings.putBoolean("fpsOn", OptionsData.fps_on);
		settings.putBoolean("fullscreenOn", OptionsData.fullscreen_on);
		settings.putInteger("vegetationInteractionLevel", OptionsData.vegetation_intersection_level);

		settings.putString("aspectRatio", OptionsData.aspect_ratio);
		settings.putString("resolution", OptionsData.resolution);
		settings.putBoolean("minimapLeft", OptionsData.minimap_left);
		settings.putFloat("comboRoseSensitivity", OptionsData.combo_rose_sensitivity);

		// keyboad keys
		for (Integer key : KeysConfiguration.getKeyboardConfiguration().keySet()) {
			WorldAction action = KeysConfiguration.getAction(new AdvancedKey(key, true));
			if (action != null)
				settings.putString(action.name(), key + ".true");
		}
		// mouse keys
		for (Integer key : KeysConfiguration.getMouseConfiguration().keySet()) {
			WorldAction action = KeysConfiguration.getAction(new AdvancedKey(key, false));
			if (action != null)
				settings.putString(action.name(), key + ".false");
		}
		settings.putBoolean("vSyncOn", OptionsData.v_sync_on);

		for (GameclassIconbutton gameclassIconButton : GameclassCollectionModule.GAMECLASS_BUTTONS)
			for (GameclassSkin skin : gameclassIconButton.getSkins())
				settings.putBoolean("equipped_" + skin.getTechnicalDescription(), skin.isEquipped());

		settings.flush();
	}

}
