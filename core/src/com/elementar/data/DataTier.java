package com.elementar.data;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Pixmap;
import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.AudioData;
import com.elementar.data.container.FontData;
import com.elementar.data.container.GameclassContainer;
import com.elementar.data.container.OptionsData;
import com.elementar.data.container.ParticleContainer;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.StyleContainer;
import com.elementar.data.container.TextData;

/**
 * Loads and disposes all essential data for the game.
 * 
 * @author lukassongajlo
 * 
 */
public class DataTier {

	public static AssetManager ASSET_MANAGER = new AssetManager();

	public static void preload() {
		FontData.load();
		StaticGraphic.load();

		while (ASSET_MANAGER.update() != true) {

		}

		FontData.setFields();
		StaticGraphic.setFields();

		StyleContainer.setFields();
	}

	public static void loadServer() {
		// cursor load and set
		AssetManager manager = DataTier.ASSET_MANAGER;
		manager.load("graphic/gui_cursor1.png", Pixmap.class);
		manager.finishLoading();
		StaticGraphic.gui_cursor1 = manager.get("graphic/gui_cursor1.png", Pixmap.class);

		StaticGraphic.loadAndSetServerSprites();

		FontData.load();
		TextData.load();
		AudioData.load();
		ASSET_MANAGER.finishLoading();

		FontData.setFields();
		// requires static sprites of skin object
		StyleContainer.setFields();
		TextData.setFields();
		AudioData.setFields();

		// load hitbox from the spine project of the base
		AnimationContainer.loadServerBuildings();
		GameclassContainer.setFields();
		ParticleContainer.loadEmissionEffects();

	}

	public static void load() {
		OptionsData.load();
		TextData.load();

		AnimationContainer.load();

		AudioData.load();
		ParticleContainer.load();
	}

	public static void setFields() {
		AnimationContainer.setFields();
		TextData.setFields();
		ParticleContainer.setFields();
		GameclassContainer.setFields();
		AudioData.setFields();

	}

	public static void dispose() {

		ASSET_MANAGER.dispose();

		ParticleContainer.dispose();
		StaticGraphic.dispose();
	}

}
