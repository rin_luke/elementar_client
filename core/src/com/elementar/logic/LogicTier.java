package com.elementar.logic;

import static com.elementar.logic.network.protocols.ClientGameserverProtocol.ARRIVAL_TIME_META_LAST;
import static com.elementar.logic.network.protocols.ClientGameserverProtocol.GAMESERVER_CAPACITY;
import static com.elementar.logic.network.protocols.ClientGameserverProtocol.GAMESERVER_TIME_INTERPOLATED;
import static com.elementar.logic.network.protocols.ClientGameserverProtocol.GAMESERVER_TIME_LAST;
import static com.elementar.logic.network.protocols.ClientGameserverProtocol.TARGET_TIME_RATE;
import static com.elementar.logic.util.Shared.GRAVITY;
import static com.elementar.logic.util.Shared.POSITION_ITERATIONS;
import static com.elementar.logic.util.Shared.VELOCITY_ITERATIONS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.elementar.data.container.ParticleContainer;
import com.elementar.data.container.util.ParticleAccessor;
import com.elementar.data.container.util.TweenableMusic;
import com.elementar.data.container.util.TweenableMusicAccessor;
import com.elementar.data.container.util.TweenableSound;
import com.elementar.data.container.util.TweenableSoundAccessor;
import com.elementar.data.container.util.ParticleEmitter.Particle;
import com.elementar.gui.util.CurvedTrajectory;
import com.elementar.gui.util.CurvedTrajectoryAccessor;
import com.elementar.gui.util.DamageDiffbar;
import com.elementar.gui.util.DamageDiffbarAccessor;
import com.elementar.gui.util.EffectNumberAccessor;
import com.elementar.gui.util.ParallaxCamera;
import com.elementar.gui.util.SkillThumbnailAccessor;
import com.elementar.gui.util.SkillThumbnailMovement;
import com.elementar.gui.util.VegetationSpriteAccessor;
import com.elementar.gui.util.WorldCamAccessor;
import com.elementar.logic.characters.AlphaTransition;
import com.elementar.logic.characters.AlphaTransitionAccessor;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.SparseClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.DrawableCreep;
import com.elementar.logic.creeps.OmniCreep;
import com.elementar.logic.creeps.SparseCreep;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.SparseProjectile;
import com.elementar.logic.emission.Trigger;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.effect.AEffectWithNumber;
import com.elementar.logic.map.buildings.ABuilding;
import com.elementar.logic.network.connection.ClientGameserverConnection;
import com.elementar.logic.network.gameserver.GameserverLogic;
import com.elementar.logic.network.gameserver.MetaDataGameserver;
import com.elementar.logic.network.gameserver.ServerContactListener;
import com.elementar.logic.network.gameserver.GameserverLogic.ServerState;
import com.elementar.logic.network.protocols.ClientGameserverProtocol;
import com.elementar.logic.network.protocols.ClientMainserverProtocol;
import com.elementar.logic.network.response.AInfo;
import com.elementar.logic.network.response.InfoAnimations;
import com.elementar.logic.network.response.InfoDeathTime;
import com.elementar.logic.network.response.InfoEmissionInterruption;
import com.elementar.logic.network.response.InfoMessage;
import com.elementar.logic.network.response.InfoTriggers;
import com.elementar.logic.network.response.InfoWinnerTeam;
import com.elementar.logic.network.response.MetaDataResponse;
import com.elementar.logic.network.response.WorldDataResponse;
import com.elementar.logic.network.util.SnapshotInterpolation;
import com.elementar.logic.util.ErrorMessage;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.ViewHandler;
import com.elementar.logic.util.Shared.ClientState;
import com.elementar.logic.util.Shared.Team;
import com.elementar.logic.util.maps.LimitedHashMap;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

public class LogicTier {

	public static int TEAM1_REMAINING_LIVES, TEAM2_REMAINING_LIVES;

	public static Team WINNER_TEAM;

	public static ServerState SERVER_STATE;

	public static boolean ENTERED_WORLD = false;
	/**
	 * Holds all client-side characters inside a map structure to get access in
	 * O(1). It's the meta realm of all clients. For handling clients on world
	 * 
	 * level the clients get added to the {@link LogicTier#WORLD_PLAYER_MAP}
	 */
	public static LimitedHashMap<Short, OmniClient> META_PLAYER_MAP = new LimitedHashMap<Short, OmniClient>(15);

	/**
	 * Holds all client-side characters inside a map structure to get access in
	 * O(1). It's the world realm of all clients. For handling clients on meta level
	 * the clients get added to the {@link LogicTier#META_PLAYER_MAP}
	 */
	public static LimitedHashMap<Short, OmniClient> WORLD_PLAYER_MAP = new LimitedHashMap<Short, OmniClient>(15);

	public static HashMap<Short, OmniCreep> CREEP_MAP = new HashMap<>();

	/**
	 * Creeps will be removed when the last projectile of the last emission
	 * disappears.
	 */
	public static ArrayList<OmniCreep> CREEP_LIVING_LIST = new ArrayList<>();

	/**
	 * Holds all client-side buildings inside a map structure to get access in O(1)
	 */
	public static LimitedHashMap<Integer, ABuilding> BUILDINGS_MAP;

	/**
	 * last processed id of server
	 */
	public static int MOST_RECENT_PROCESSED_ID;

	public static ViewHandler VIEW_HANDLER;

	public static ErrorMessage ERROR_MESSAGE;

	private SnapshotInterpolation snapshot_interpolation = new SnapshotInterpolation();

	private ArrayList<SparseClient> interpolated_player_list;

	private ClientHistory client_history;

	private World client_main_world, client_para_world;

	private GameserverLogic gameserver_logic;

	private static OmniClient MY_CLIENT;

	private TweenManager tween_manager_vegetation = new TweenManager();

	public LogicTier() {
		client_history = new ClientHistory();
		createNewClientWorlds();

		Tween.registerAccessor(ParallaxCamera.class, new WorldCamAccessor());
		Tween.registerAccessor(AlphaTransition.class, new AlphaTransitionAccessor());

		// see AudioData
		Tween.registerAccessor(TweenableMusic.class, new TweenableMusicAccessor());
		Tween.registerAccessor(TweenableSound.class, new TweenableSoundAccessor());

		// see CustomParticleEffect
		Tween.registerAccessor(Particle.class, new ParticleAccessor());

		// see BarDisplayManager
		Tween.registerAccessor(DamageDiffbar.class, new DamageDiffbarAccessor());
		Tween.registerAccessor(SkillThumbnailMovement.class, new SkillThumbnailAccessor());
		Tween.registerAccessor(AEffectWithNumber.class, new EffectNumberAccessor());

		// see VegetationManager
		Tween.registerAccessor(Sprite.class, new VegetationSpriteAccessor());

		// see TransferSkill
		Tween.registerAccessor(CurvedTrajectory.class, new CurvedTrajectoryAccessor());

	}

	public void updateBySnapshots() {

		// update meta data
		synchronized (ClientGameserverProtocol.LIST_META_DATA) {

			MetaDataResponse snapshot;

			Iterator<MetaDataResponse> itMetaSnapshot = ClientGameserverProtocol.LIST_META_DATA.iterator();
			while (itMetaSnapshot.hasNext() == true) {
				snapshot = itMetaSnapshot.next();
				if (snapshot != null) {

					// update
					updateMetaPlayerlist(snapshot);

					// delete disconnected players
					OmniClient client;
					Iterator<Entry<Short, OmniClient>> it = META_PLAYER_MAP.entrySet().iterator();
					while (it.hasNext() == true) {

						client = it.next().getValue();
						boolean isInside = false;

						for (int i = 0; i < snapshot.meta_player_list.size(); i++)
							if (snapshot.meta_player_list.get(i).connection_id == client.getMetaClient().connection_id)
								isInside = true;

						if (isInside == false) {
							it.remove();

							deleteCharacter(client.getPhysicalClient());
							deleteProjectiles(client.getPhysicalClient());

							if (client.getDrawableClient() != null) {

								/*
								 * find old projectiles that are not used anymore (=dead) and stop their sound
								 */
								deleteSounds(client.getDrawableClient().sparse_projectiles);

							}

							Util.removeByValue(WORLD_PLAYER_MAP, client);
						}
					}
				}

				itMetaSnapshot.remove();

			}

		}

		// interpolate game server time
		GAMESERVER_TIME_INTERPOLATED = MathUtils.lerp(GAMESERVER_TIME_LAST, GAMESERVER_TIME_LAST + TARGET_TIME_RATE,
				(System.currentTimeMillis() - ARRIVAL_TIME_META_LAST) * 0.001f);

		synchronized (ClientGameserverProtocol.LIST_SNAPSHOTS) {
			// update world data

			/*
			 * apply one time event by extracting so-called "infos" out of world snapshots.
			 * After applying they get deleted.
			 */

			WorldDataResponse snapshot;

			Iterator<WorldDataResponse> itInfoSnapshot = ClientGameserverProtocol.LIST_SNAPSHOTS.getInfoList()
					.iterator();
			while (itInfoSnapshot.hasNext() == true) {
				snapshot = itInfoSnapshot.next();
				if (snapshot != null)
					updateInfo(snapshot);
				itInfoSnapshot.remove();
			}

			// update world with most recent data
			snapshot = snapshot_interpolation.getInterpolation(ClientGameserverProtocol.LIST_SNAPSHOTS);
			if (snapshot != null)
				updateWorldPlayerlist(snapshot);

		}

	}

	public static void deleteProjectiles(PhysicalClient client) {
		// destroy all projectiles
		if (client == null)
			return;

		for (Emission e : client.getEmissionList())
			for (Projectile p : e.getEmittedProjectiles())
				p.destroy();
	}

	public static void deleteCharacter(PhysicalClient client) {
		if (client == null)
			return;

		if (client.getHitbox() != null)
			client.getHitbox().destroy();

	}

	/**
	 * if snapshot is a {@link MetaDataResponse} snapshot this method updates the
	 * meta player list and
	 * {@link ClientGameserverProtocol#GAMESERVER_TIME_INTERPOLATED} +
	 * {@link ClientGameserverProtocol#GAMESERVER_CAPACITY}
	 */
	private void updateMetaPlayerlist(MetaDataResponse snapshot) {
		// Add all new Players or update
		if (snapshot != null) {

			SERVER_STATE = snapshot.server_state;

			TEAM1_REMAINING_LIVES = snapshot.team1_remaining_lives;
			TEAM2_REMAINING_LIVES = snapshot.team2_remaining_lives;

			/*
			 * time is usually incrementing but after a match or before arena opens we use a
			 * count-down.
			 */
			TARGET_TIME_RATE = Shared.SEND_RATE_META;
			if (snapshot.gameserver_time < GAMESERVER_TIME_LAST)
				TARGET_TIME_RATE = -Shared.SEND_RATE_META;

			GAMESERVER_TIME_LAST = snapshot.gameserver_time;
			ARRIVAL_TIME_META_LAST = snapshot.arrival_time;

			GAMESERVER_CAPACITY = snapshot.gameserver_capacity;

			/*
			 * Bei meta daten ist es nicht notwendig die interpolierte playerlist version zu
			 * benutzen
			 */
			for (MetaClient metaClient : snapshot.meta_player_list) {
				OmniClient updatedClient = META_PLAYER_MAP.get(metaClient.connection_id);

				/*
				 * create new client if meta-player map doesn't hold this specific client yet.
				 */
				if (updatedClient == null) {
					updatedClient = new OmniClient(metaClient, false, Location.CLIENTSIDE_MULTIPLAYER);
					META_PLAYER_MAP.put(metaClient.connection_id, updatedClient);
				}

				updateMetaDataGameclients(metaClient, updatedClient.getMetaClient());

				/*
				 * put client in world-player map if team and gameclass have been chosen.
				 */
				if (metaClient.team != null && metaClient.gameclass_name != null) {
					updatedClient = WORLD_PLAYER_MAP.get(metaClient.connection_id);
					if (updatedClient == null) {
						updatedClient = META_PLAYER_MAP.get(metaClient.connection_id);
						updatedClient.init(client_main_world, client_para_world);
						WORLD_PLAYER_MAP.put(metaClient.connection_id, updatedClient);
					}
				}

			}

		}

	}

	/**
	 * Updates two {@link MetaClient} clients.
	 * 
	 * @param sourceClient  , the source
	 * @param updatedClient , the client which is getting updated.
	 */
	private void updateMetaDataGameclients(MetaClient sourceClient, MetaClient updatedClient) {
		updatedClient.connection_id = sourceClient.connection_id;
		updatedClient.setGameclass(sourceClient.gameclass_name);
		updatedClient.name = sourceClient.name;
		updatedClient.client_state = sourceClient.client_state;
		updatedClient.skin_index = sourceClient.skin_index;
		updatedClient.slot_index = sourceClient.slot_index;
		updatedClient.team = sourceClient.team;
		updatedClient.pre_selected_gameclass = sourceClient.pre_selected_gameclass;
		updatedClient.health_absolute = sourceClient.health_absolute;
		updatedClient.mana_absolute = sourceClient.mana_absolute;
		updatedClient.ping = sourceClient.ping;
	}

	/**
	 * Updates {@link #WORLD_PLAYER_MAP} which holds all {@link DrawableClient} that
	 * are within the world. Takes data from snapshot ( {@link WorldDataResponse}).
	 */
	private void updateWorldPlayerlist(WorldDataResponse snapshot) {

		interpolated_player_list = snapshot.player_list;

		/*
		 * Next we update all clients with interpolated data, except my_client.
		 */
		if (MY_CLIENT == null)
			return;

		if (MY_CLIENT.getMetaClient().client_state != ClientState.MATCH)
			return;

		/*
		 * we override the interpolated time directly by the game server time. We does
		 * not need any interpolation since the delivered packet comes high-frequently.
		 */
		GAMESERVER_TIME_INTERPOLATED = snapshot.gameserver_time;

		for (SparseClient recentGameclient : interpolated_player_list) {

			OmniClient updatedClient = WORLD_PLAYER_MAP.get(recentGameclient.connection_id);

			if (updatedClient != null) {

				DrawableClient updatedDrawableClient = updatedClient.getDrawableClient();
				PhysicalClient updatedPhysicalClient = updatedClient.getPhysicalClient();

				/*
				 * updating health/mana, projectiles, cast time for both kinds of player (no
				 * matter whether others or myself get updated)
				 */

				updateNonMovementData(recentGameclient, updatedDrawableClient);

				if (updatedClient != MY_CLIENT) {

					/*
					 * Here just other clients will be updated because the new data is interpolated
					 * and not the most recent one
					 */

					updatePosition(recentGameclient, updatedDrawableClient);

					// physical client has to update as well
					if (updatedPhysicalClient.getHitbox() != null)
						updatedPhysicalClient.setPosition(recentGameclient.pos.x, recentGameclient.pos.y);

					// cursor pos will locally updated
					updatedDrawableClient.cursor_pos = recentGameclient.cursor_pos;
					updatedDrawableClient.flipByCursor();

					/*
					 * adds new re-directed input data which's already processed by the server. Now
					 * the inputs will be simulated commands by user like minimap pings, commands
					 * from rose, audio effects and animations
					 */
					updatedPhysicalClient.addToInputQueue(recentGameclient.redirected_inputs);
					recentGameclient.redirected_inputs.clear();

				}

			}
		}

		// my_client gets the most recent data of the snapshot list.

		WorldDataResponse worldData = ClientGameserverProtocol.LIST_SNAPSHOTS.get(0);
		if (worldData != null) {

			for (SparseClient mostRecentClient : worldData.player_list)
				if (mostRecentClient.connection_id == MY_CLIENT.getMetaClient().connection_id) {
					MOST_RECENT_PROCESSED_ID = worldData.message_id;

					updatePosition(mostRecentClient, MY_CLIENT.getDrawableClient());

					break;
				}
		}
	}

	private void updateInfo(WorldDataResponse snapshot) {

		OmniClient client;
		for (SparseClient sparseClient : snapshot.player_list) {

			if (ENTERED_WORLD == false)
				continue;

			Iterator<AInfo> itInfos = sparseClient.player_infos.iterator();

			while (itInfos.hasNext() == true) {
				AInfo info = itInfos.next();

				itInfos.remove();

				client = META_PLAYER_MAP.get(sparseClient.connection_id);

				if (client == null)
					continue;

				// set death time
				if (info instanceof InfoDeathTime) {

					client.getPhysicalClient().death_time_current = ((InfoDeathTime) info).death_time;

				} else if (info instanceof InfoTriggers) {
					client = WORLD_PLAYER_MAP.get(sparseClient.connection_id);
					if (client != null)
						// set triggers
						for (Trigger trigger : ((InfoTriggers) info).emission_triggers) {
//							System.out.println("LogicTier.updateInfo()			ARRIVED CHAR TRIGGER, pool id = "
//									+ trigger.pool_id + ", target id = " + trigger.target_id + ", trigger id = "
//									+ trigger.trigger_id + ", center pos = " + trigger.center_pos);

							Emission e = new Emission(trigger, client.getPhysicalClient(), client.getDrawableClient());

							client.getPhysicalClient().getEmissionList().add(e);
						}

				} else if (info instanceof InfoAnimations) {

					if (client != MY_CLIENT)
						updateAnimations((InfoAnimations) info, client.getDrawableClient(), true, true);
					else
						updateAnimations((InfoAnimations) info, client.getDrawableClient(), false, false);

				} else if (info instanceof InfoEmissionInterruption) {
					Emission interruptedEmission = client.getPhysicalClient().getEmissionList().getMapForInterruptions()
							.get(((InfoEmissionInterruption) info).trigger_id);
					if (interruptedEmission != null)
						interruptedEmission.setInterrupted(true);

				} else if (info instanceof InfoMessage) {
					client.getMetaClient().chat_message = ((InfoMessage) info).message;
				} else if (info instanceof InfoWinnerTeam)
					WINNER_TEAM = ((InfoWinnerTeam) info).winner_team;

			}
		}

	}

	/**
	 * Updates position and velocity
	 * 
	 * @param sourceClient  , the source
	 * @param updatedClient , the client which is getting updated.
	 */
	public static void updatePosition(SparseClient sourceClient, SparseClient updatedClient) {
		updatedClient.pos.set(sourceClient.pos);
		updatedClient.vel.set(sourceClient.vel);

	}

	public static void updatePosition(SparseCreep sourceCreep, SparseCreep updatedCreep) {
		updatedCreep.pos.set(sourceCreep.pos);
		updatedCreep.vel.set(sourceCreep.vel);
	}

	/**
	 * Updates non-movement data: projectiles, cast time
	 * 
	 * @param sourceClient
	 * @param updatedClient
	 */
	public static void updateNonMovementData(SparseClient sourceClient, SparseClient updatedClient) {

		/*
		 * TODO anhand der team zugehörigkeit von my_client könnte ich hier den
		 * cooldown, das mana, die cast time entsprechend ausblenden lassen
		 */

		// updatedClient.cursor_pos.set(sourceClient.cursor_pos);

		// update projectiles
		updateProjectiles(sourceClient.sparse_projectiles, updatedClient.sparse_projectiles);

		// update combo storage
		updatedClient.sparse_combo_storage1.meta_skill_id = sourceClient.sparse_combo_storage1.meta_skill_id;

		if (updatedClient.sparse_combo_storage1.isStoring() != sourceClient.sparse_combo_storage1.isStoring()
				&& sourceClient.sparse_combo_storage1.isStoring() == true)
			if (updatedClient instanceof DrawableClient) {
				// recognized a positive change of the absorption state
				((DrawableClient) updatedClient).setAbsorptionChange1(true);
			}
		updatedClient.sparse_combo_storage1.duration_ratio = sourceClient.sparse_combo_storage1.duration_ratio;

		updatedClient.sparse_combo_storage2.meta_skill_id = sourceClient.sparse_combo_storage2.meta_skill_id;

		if (updatedClient.sparse_combo_storage2.isStoring() != sourceClient.sparse_combo_storage2.isStoring()
				&& sourceClient.sparse_combo_storage2.isStoring() == true)
			if (updatedClient instanceof DrawableClient) {
				// recognized a positive change of the absorption state
				((DrawableClient) updatedClient).setAbsorptionChange2(true);
			}
		updatedClient.sparse_combo_storage2.duration_ratio = sourceClient.sparse_combo_storage2.duration_ratio;

		updatedClient.health_current = sourceClient.health_current;
		updatedClient.mana_current = sourceClient.mana_current;
		updatedClient.cooldown_skill0 = sourceClient.cooldown_skill0;
		updatedClient.cooldown_skill1 = sourceClient.cooldown_skill1;
		updatedClient.cooldown_skill2 = sourceClient.cooldown_skill2;
	}

	public static void updateProjectiles(LinkedList<SparseProjectile> sourceProjectiles,
			LinkedList<SparseProjectile> updatedProjectiles) {

		LinkedList<SparseProjectile> oldProjectiles = new LinkedList<>();
		for (SparseProjectile oldProjectile : updatedProjectiles)
			oldProjectiles.add(oldProjectile);

		updatedProjectiles.clear();

		/*
		 * update to the most recent projectiles, consider the old ones in terms of
		 * sound and view circles.
		 */
		for (int i = 0; i < sourceProjectiles.size(); i++) {
			SparseProjectile newProjectile = new SparseProjectile(sourceProjectiles.get(i));

			/*
			 * find potential view circles of DrawableClient and transfer it to the new
			 * projectile.
			 */

			for (SparseProjectile oldProjectile : oldProjectiles) {

				if (newProjectile.projectile_id == oldProjectile.projectile_id) {
					if (oldProjectile.sound_id != -1)
						newProjectile.sound_id = oldProjectile.sound_id;
					break;
				}
			}

			updatedProjectiles.add(newProjectile);

		}

		/*
		 * find old projectiles that are not used anymore (=dead) and stop their sound
		 */
		for (Iterator<SparseProjectile> itOld = oldProjectiles.iterator(); itOld.hasNext();) {
			SparseProjectile pOld = itOld.next();

			boolean dead = true;

			for (Iterator<SparseProjectile> itNew = sourceProjectiles.iterator(); itNew.hasNext();) {
				SparseProjectile pNew = itNew.next();

				if (pOld.projectile_id == pNew.projectile_id) {
					dead = false;
					break;
				}
			}

			if (dead == true) {

				if (pOld.sound_id == -1)
					continue;
				AEmissionDef def = ParticleContainer.getDef(pOld.pool_ids[0]);
				if (def == null)
					continue;

				TweenableSound sound = def.getSound();
				if (sound == null)
					continue;

				if (sound.isInterruptible() == true)
					sound.updateVolume(pOld.sound_id, 0, 100000);

			}
		}

		// only one attached projectile of a kind allowed
		Iterator<SparseProjectile> itNew = updatedProjectiles.iterator();
		while (itNew.hasNext() == true) {
			SparseProjectile newProjectile1 = itNew.next();

			for (SparseProjectile newProjectile2 : updatedProjectiles) {
				if (newProjectile1 == newProjectile2)
					continue;
				if (newProjectile1.pool_ids[0] != newProjectile2.pool_ids[0])
					continue;

				// same pool and different projectiles
				if (newProjectile1.connection_id_attached != -1
						&& newProjectile1.connection_id_attached == newProjectile2.connection_id_attached) {
					// we kill the older one
					if (newProjectile1.projectile_id < newProjectile2.projectile_id) {
						itNew.remove();
						break;
					}
				}

			}

		}

	}

	/**
	 * 
	 * @param infoAnimations
	 * @param updatedClient
	 * @param withTrack0     , my own client updates its track0 by client side
	 *                       physics. Pass <code>true</code> for updating other
	 *                       clients like mates and enemies, pass <code>false</code>
	 *                       if your own client gets updated
	 */
	public static void updateAnimations(InfoAnimations infoAnimations, DrawableClient updatedClient, boolean withTrack0,
			boolean withTrack1) {

		updatedClient.info_animations = infoAnimations;
		if (withTrack0 == true)
			updatedClient.setCurrentAnimation(infoAnimations.track_0);
		if (withTrack1 == true)
			updatedClient.setCurrentAnimation(infoAnimations.track_1);
		updatedClient.setCurrentAnimation(infoAnimations.track_2);
		updatedClient.setCurrentAnimation(infoAnimations.track_3);
		updatedClient.setCurrentAnimation(infoAnimations.track_4);

	}

	public static void updateAnimations(InfoAnimations infoAnimations, DrawableCreep updatedCreep) {

		updatedCreep.info_animations = infoAnimations;
		updatedCreep.setCurrentAnimation(infoAnimations.track_0);
		updatedCreep.setCurrentAnimation(infoAnimations.track_1);
		updatedCreep.setCurrentAnimation(infoAnimations.track_2);
		updatedCreep.setCurrentAnimation(infoAnimations.track_3);
		updatedCreep.setCurrentAnimation(infoAnimations.track_4);

	}

	public static void confirmAnimations(DrawableClient drawableClient) {
		drawableClient.update();
		drawableClient.getSkeleton().setX(drawableClient.pos.x);
		drawableClient.getSkeleton().setY(drawableClient.pos.y - 3f / Shared.PPM);
		drawableClient.getSkeleton().updateWorldTransform();
	}

	public static void confirmAnimations(DrawableCreep drawableCreep) {
		drawableCreep.update();
		drawableCreep.getSkeleton().setX(drawableCreep.pos.x);
		drawableCreep.getSkeleton().setY(drawableCreep.pos.y - 3f / Shared.PPM);
		drawableCreep.getSkeleton().updateWorldTransform();
	}

	/**
	 * Sets the own client manually for {@link Location#CLIENTSIDE_TRAINING}-case
	 * 
	 * @param omniClient
	 */
	public void setMyClient(OmniClient omniClient) {
		MY_CLIENT = omniClient;
	}

	/**
	 * Returns the own client which was added in {@link #META_PLAYER_MAP} (while
	 * playing via network)
	 * 
	 * @return
	 */
	public static OmniClient getMyClient() {

		if (MY_CLIENT == null)
			MY_CLIENT = META_PLAYER_MAP.get((short) ClientGameserverConnection.getInstance().getClient().getID());

		return MY_CLIENT;
	}

	public GameserverLogic getGameserverLogic() {
		if (gameserver_logic == null) {

			MetaDataGameserver mdgs = ClientMainserverProtocol.MY_HOSTED_GAMESERVER_METADATA;

			// if mtgs != null we have a response from mainserver!
			if (mdgs == null)
				return null;

			gameserver_logic = new GameserverLogic(mdgs);
		}
		return gameserver_logic;
	}

	public void disposeGameserverLogic() {
		if (gameserver_logic != null) {
			gameserver_logic.getWorld().dispose();
			gameserver_logic.getGameserverMainserverConnection().getClient().close();
			gameserver_logic.getGameserverClientConnection().getServer().stop();
			gameserver_logic.getGameserverClientConnection().getServer().close();

			ClientMainserverProtocol.MY_HOSTED_GAMESERVER_METADATA = null;
			gameserver_logic = null;

		}
	}

	/**
	 * world->step with a rate which is collectively used by server and client.
	 * 
	 * @param world
	 */
	public static void updateWorld(World world) {
		world.step(Shared.SEND_AND_UPDATE_RATE_WORLD, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
	}

	/**
	 * Creates a new client world (saved in {@link #client_main_world}) where all
	 * client predictions will happen (predicted pos, predicted velocity,...) and
	 * where all local physics (vegetation) will be simulated. So within this world
	 * the client lives in the future due to its predicted steps.
	 * <p>
	 * To ensure that the client had predicted the right position I need a second
	 * world, so this methods creates also an additional world (named
	 * {@link #client_para_world}) which takes the last valid snapshot from the
	 * server and replays all steps of the predicted world_steps. After that I get
	 * the right predicted data based on the last valid snapshot. If there are some
	 * differences between the re-calculated data in {@link #client_para_world} and
	 * the current data in {@link #client_main_world}, I have to adjust the data in
	 * {@link #client_main_world}. In addition this the para-world simulates all
	 * lights
	 */
	public void createNewClientWorlds() {
		if (client_main_world != null)
			client_main_world.dispose();
		if (client_para_world != null)
			client_para_world.dispose();

		client_main_world = new World(new Vector2(0, GRAVITY), true);
		client_main_world.setContactListener(new ClientContactListener(tween_manager_vegetation));

		client_para_world = new World(new Vector2(0, GRAVITY), true);
		client_para_world.setContactListener(new ServerContactListener());
	}

	public World getClientMainWorld() {
		return client_main_world;
	}

	/**
	 * This world is used to update lights and for calculate whether the client
	 * prediction was right (synchronization with server)
	 * 
	 * @return
	 */
	public World getClientParaWorld() {
		return client_para_world;
	}

	public TweenManager getTweenManagerVegetation() {
		return tween_manager_vegetation;
	}

	public ClientHistory getClientHistory() {
		return client_history;
	}

	public void reset() {

		/*
		 * when you reset all sounds of your and other sparse projectiles should stop
		 * and all hitboxes in client world should be destroyed.
		 */
		for (OmniClient client : WORLD_PLAYER_MAP.values()) {

			deleteCharacter(client.getPhysicalClient());
			deleteProjectiles(client.getPhysicalClient());

			if (client.getDrawableClient() != null) {

				/*
				 * find old projectiles that are not used anymore (=dead) and stop their sound
				 */
				deleteSounds(client.getDrawableClient().sparse_projectiles);

			}
		}

		ClientGameserverConnection.getInstance().disconnect();
		ClientGameserverProtocol.resetVariables();
		softReset();
	}

	private void deleteSounds(LinkedList<SparseProjectile> projectiles) {

		for (Iterator<SparseProjectile> itProjectiles = projectiles.iterator(); itProjectiles.hasNext();) {

			SparseProjectile projectile = itProjectiles.next();

			if (projectile.sound_id == -1)
				continue;
			AEmissionDef def = ParticleContainer.getDef(projectile.pool_ids[0]);
			if (def == null)
				continue;

			TweenableSound sound = def.getSound();
			if (sound == null)
				continue;

			sound.updateVolume(projectile.sound_id, 0, 100000);

		}
	}

	/**
	 * no network reset
	 */
	public void softReset() {
		MY_CLIENT = null;
		META_PLAYER_MAP.clear();
		WORLD_PLAYER_MAP.clear();
		CREEP_MAP.clear();
		CREEP_LIVING_LIST.clear();
		ENTERED_WORLD = false;
		WINNER_TEAM = null;
		SERVER_STATE = null;
		TEAM1_REMAINING_LIVES = Shared.INITIAL_REMAINING_LIVES;
		TEAM2_REMAINING_LIVES = Shared.INITIAL_REMAINING_LIVES;
	}

}
