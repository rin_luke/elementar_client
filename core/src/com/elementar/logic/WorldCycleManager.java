package com.elementar.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.badlogic.gdx.physics.box2d.World;
import com.elementar.logic.creeps.CreepGenerator;
import com.elementar.logic.creeps.OmniCreep;
import com.elementar.logic.map.MapManager;

public class WorldCycleManager {

	private CreepGenerator creep_generator_team1_top, creep_generator_team1_bot,
			creep_generator_team2_top, creep_generator_team2_bot;

	private static final float RATE_SHRINE_APPEARANCE = 10f;

	private float accu_shrine_appearance;

	private HashMap<Short, OmniCreep> creep_map;
	private MapManager map_manager;

	public WorldCycleManager(MapManager mapManager, HashMap<Short, OmniCreep> creepMap,
			ArrayList<OmniCreep> creepLivingList) {
		this.creep_map = creepMap;
		this.map_manager = mapManager;

//		creep_generator_team1_top = new CreepGenerator(pathTopFromLeft, Team.TEAM_1, creepMap,
//				creepLivingList);
//		creep_generator_team1_bot = new CreepGenerator(pathBotFromLeft, Team.TEAM_1, creepMap,
//				creepLivingList);
//		creep_generator_team2_top = new CreepGenerator(pathTopFromRight, Team.TEAM_2, creepMap,
//				creepLivingList);
//		creep_generator_team2_bot = new CreepGenerator(pathBotFromRight, Team.TEAM_2, creepMap,
//				creepLivingList);

	}

	public void generate(World world) {

		// boolean hasTopPathChanged = map_manager.updatePaths(Lane.LANE_TOP);
		// boolean hasBotPathChanged = map_manager.updatePaths(Lane.LANE_BOT);
		//
		// if (hasTopPathChanged == true || hasBotPathChanged == true) {
		// LinkedPaths pathTopFromLeft = map_manager.getPathTopFromLeft();
		// LinkedPaths pathBotFromLeft = map_manager.getPathBotFromLeft();
		//
		// LinkedPaths pathTopFromRight = LinkedPaths.makeCopy(pathTopFromLeft);
		// Collections.reverse(pathTopFromRight);
		// for (ArrayList<Node> subPath : pathTopFromRight)
		// Collections.reverse(subPath);
		// pathTopFromRight.setPathDirection(PathDirection.TOP_FROM_RIGHT);
		//
		// LinkedPaths pathBotFromRight = LinkedPaths.makeCopy(pathBotFromLeft);
		// Collections.reverse(pathBotFromRight);
		// for (ArrayList<Node> subPath : pathBotFromRight)
		// Collections.reverse(subPath);
		// pathBotFromRight.setPathDirection(PathDirection.BOT_FROM_RIGHT);
		//
		// creep_generator_team1_bot.setPaths(pathBotFromLeft);
		// creep_generator_team1_top.setPaths(pathTopFromLeft);
		// creep_generator_team2_bot.setPaths(pathBotFromRight);
		// creep_generator_team2_top.setPaths(pathTopFromRight);
		//
		// for (OmniCreep creep : creep_map.values()) {
		// PhysicalCreep physicalCreep = creep.getPhysicalCreep();
		// switch (physicalCreep.getPathDirection()) {
		// case BOT_FROM_LEFT:
		// physicalCreep.setNewPaths(pathBotFromLeft);
		// break;
		// case BOT_FROM_RIGHT:
		// physicalCreep.setNewPaths(pathBotFromRight);
		// break;
		// case TOP_FROM_LEFT:
		// physicalCreep.setNewPaths(pathTopFromLeft);
		// break;
		// case TOP_FROM_RIGHT:
		// physicalCreep.setNewPaths(pathTopFromRight);
		// break;
		//
		// }
		// }
		// }
		//
		// creep_generator_team1_top.generate(world);
		// creep_generator_team1_bot.generate(world);
		// creep_generator_team2_top.generate(world);
		// creep_generator_team2_bot.generate(world);

		// accu_shrine_appearance += Math.min(Shared.SEND_AND_UPDATE_RATE,
		// MAX_DELTA_TIME);
		//
		// while (accu_shrine_appearance >= RATE_SHRINE_APPEARANCE) {
		// accu_shrine_appearance -= RATE_SHRINE_APPEARANCE;
		// }

	}

	public Collection<OmniCreep> getCreeps() {
		return creep_map.values();
	}

}
