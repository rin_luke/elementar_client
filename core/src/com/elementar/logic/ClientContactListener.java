package com.elementar.logic;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.elementar.gui.util.VegetationManager;
import com.elementar.gui.util.VegetationSprite;
import com.elementar.gui.util.VegetationSpriteAccessor;
import com.elementar.logic.network.gameserver.ServerContactListener;
import com.elementar.logic.util.userdata.PlantContacts;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

public class ClientContactListener extends ServerContactListener {

	private final static float VEGETATION_UNILATERAL_RUN_TROUGH_DURATION = 0.2f;
	private final static float VEGETATION_CORRECTION_DURATION = 0.5f;

	private TweenManager tween_manager;

	public ClientContactListener(TweenManager tweenManager) {
		tween_manager = tweenManager;
	}

	@Override
	protected boolean beginBoth(Contact contact, Fixture fixtureA, Fixture fixtureB) {
		boolean superRes = super.beginBoth(contact, fixtureA, fixtureB);
		if (handleRunTroughAnimation(fixtureA, fixtureB) == true)
			return true;
		if (handleRunTroughAnimation(fixtureB, fixtureA) == true)
			return true;
		return superRes;
	}

	/**
	 * If method could be executed (after not-null checks at the beginning) it
	 * return true, otherwise false. Handles contact between player and plant.
	 * 
	 * @param fixtureA
	 * @param fixtureB
	 * @return
	 */
	private boolean handleRunTroughAnimation(Fixture fixtureA, Fixture fixtureB) {
		if (fixtureA.getUserData() != null) {
			if (fixtureA.getUserData() instanceof PlantContacts) {
				PlantContacts plantContact = (PlantContacts) fixtureA.getUserData();
				if (plantContact.isInAnimation() == false) {
					plantContact.getSprite().setClientVelocityVector(
							new Vector2(fixtureB.getBody().getLinearVelocity()));
					tween_manager.killTarget(plantContact.getSprite());

					/*
					 * if a player runs through a plant there are two steps of
					 * plants movement. first is the amplitude in direction of
					 * players velocity (unilateral amplitude). The duration of
					 * this animation-part is short because it should look like
					 * the player crashes into the plant. After the crash the
					 * plant gets to its normal state so there is a correction
					 * phase.
					 */
					// full time = amplitude + correction
					plantContact.setAnimationTime(VEGETATION_CORRECTION_DURATION
							+ VEGETATION_UNILATERAL_RUN_TROUGH_DURATION);
					// first step
					Tween.to(plantContact.getSprite(), VegetationSpriteAccessor.CONTACT,
							VEGETATION_UNILATERAL_RUN_TROUGH_DURATION).target(0f, 0f)
							.ease(TweenEquations.easeInOutSine)
							.setUserData(plantContact.getSprite()).setCallback(correction_callback)
							.start(tween_manager);
				}
				return true;
			}
			return false;
		}
		return false;

	}

	private final TweenCallback correction_callback = new TweenCallback() {
		@Override
		public void onEvent(int type, BaseTween<?> source) {

			VegetationSprite sprite = (VegetationSprite) source.getUserData();
			// second step
			Tween.to(sprite, VegetationSpriteAccessor.CORRECTION_AFTER_CONTACT,
					VEGETATION_CORRECTION_DURATION).target(0, 0).ease(TweenEquations.easeInOutSine)
					.setUserData(sprite).setCallback(wind_callback).start(tween_manager);

		}
	};

	private final TweenCallback wind_callback = new TweenCallback() {
		@Override
		public void onEvent(int type, BaseTween<?> source) {
			// Takes the sprite from the user data
			VegetationManager.applyStartAnimation(tween_manager,
					(VegetationSprite) source.getUserData());
		}
	};

}
