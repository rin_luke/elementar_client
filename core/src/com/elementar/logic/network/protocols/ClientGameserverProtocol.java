package com.elementar.logic.network.protocols;

import com.elementar.logic.LogicTier;
import com.elementar.logic.network.requests.PingRequest;
import com.elementar.logic.network.response.AReliablePacket;
import com.elementar.logic.network.response.FullServerResponse;
import com.elementar.logic.network.response.IncorrectPasswordResponse;
import com.elementar.logic.network.response.JoinSuccessfulToGameserverResponse;
import com.elementar.logic.network.response.MetaDataResponse;
import com.elementar.logic.network.response.PingResponse;
import com.elementar.logic.network.response.WorldDataResponse;
import com.elementar.logic.network.util.ReliabilitySystem;
import com.elementar.logic.network.util.SnapshotList;
import com.elementar.logic.util.ErrorMessage;
import com.elementar.logic.util.lists.MaintainingLimitedArrayList;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

/**
 * Holds a bunch of static variables that are necessary for networking between
 * player and gameserver.
 * 
 * @author lukassongajlo
 * 
 */
public class ClientGameserverProtocol extends Listener {

	public static ReliabilitySystem RELIABILITY_SYSTEM;

	public static boolean JOIN_SUCCESSFUL_GAMESERVER = false;

	public static long MAP_SEED = -1;

	/**
	 * Holds the recent snapshots for position, health, mana... updates. Some
	 * snapshots will be skipped because they might not the most recent ones
	 * anymore.
	 */
	public static SnapshotList LIST_SNAPSHOTS;
	public static MaintainingLimitedArrayList<MetaDataResponse> LIST_META_DATA;

	public static float GAMESERVER_TIME_LAST;
	public static long ARRIVAL_TIME_META_LAST;

	// depends on whether time is incrementing or decrementing.
	public static float TARGET_TIME_RATE;

	public static float GAMESERVER_TIME_INTERPOLATED;
	public static int GAMESERVER_CAPACITY;
	public static String GAMESERVER_NAME = "";

	public ClientGameserverProtocol() {
		resetVariables();
	}

	@Override
	public void received(Connection connection, java.lang.Object object) {

		if (object instanceof AReliablePacket) {
			AReliablePacket recvdPacket = (AReliablePacket) object;
			RELIABILITY_SYSTEM.recvPacket(recvdPacket);
		}

		if (object instanceof WorldDataResponse) {
			synchronized (LIST_SNAPSHOTS) {
				WorldDataResponse res = (WorldDataResponse) object;
				LIST_SNAPSHOTS.add(res);
			}
			return;
		}

		if (object instanceof MetaDataResponse) {
			synchronized (LIST_META_DATA) {
				// add() method is overridden and will set the arrival time of the packet.
				LIST_META_DATA.add((MetaDataResponse) object);
			}
			return;
		}

		if (object instanceof PingRequest) {
			/*
			 * game server has sent a ping request, we reply with a ping response.
			 */
			connection.sendUDP(new PingResponse());
			return;
		}

		if (object instanceof FullServerResponse) 
			LogicTier.ERROR_MESSAGE = ErrorMessage.serverFull;

		if (object instanceof IncorrectPasswordResponse) 
			LogicTier.ERROR_MESSAGE = ErrorMessage.incorrectPassword;

		if (object instanceof JoinSuccessfulToGameserverResponse) {
			JoinSuccessfulToGameserverResponse res = (JoinSuccessfulToGameserverResponse) object;
			MAP_SEED = res.map_seed;
			GAMESERVER_NAME = res.gameserver_name;
			JOIN_SUCCESSFUL_GAMESERVER = true;
			return;
		}

	}

	/**
	 * After a successful join to the game server or when the application starts all
	 * static variables have to be reseted to prepare them for the next join.
	 */
	public static void resetVariables() {
		JOIN_SUCCESSFUL_GAMESERVER = false;
		GAMESERVER_CAPACITY = -1;
		LIST_SNAPSHOTS = new SnapshotList(10);
		LIST_META_DATA = new MaintainingLimitedArrayList<MetaDataResponse>(10) {
			@Override
			public boolean add(MetaDataResponse mdr) {

				mdr.arrival_time = System.currentTimeMillis();

				return super.add(mdr);
			}
		};
		RELIABILITY_SYSTEM = new ReliabilitySystem();

	}

	/**
	 * Checks whether join was successful, if <code>true</code> the
	 * {@link #resetVariables()} method is called.
	 * 
	 * @return
	 */
	public static boolean isJoinSuccessful() {
		if (JOIN_SUCCESSFUL_GAMESERVER == true) {
			resetVariables();
			return true;
		}
		return false;
	}

}
