package com.elementar.logic.network.protocols;

import com.badlogic.gdx.math.MathUtils;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.ServerPhysicalClient;
import com.elementar.logic.network.gameserver.ConsoleEvent;
import com.elementar.logic.network.gameserver.GameserverLogic;
import com.elementar.logic.network.gameserver.ConsoleEvent.Type;
import com.elementar.logic.network.requests.InputRequest;
import com.elementar.logic.network.requests.PlayerJoinRequest;
import com.elementar.logic.network.requests.PreMatchRequest;
import com.elementar.logic.network.response.AReliablePacket;
import com.elementar.logic.network.response.FullServerResponse;
import com.elementar.logic.network.response.IncorrectPasswordResponse;
import com.elementar.logic.network.response.JoinSuccessfulToGameserverResponse;
import com.elementar.logic.network.response.PingResponse;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.Shared.ClientState;
import com.elementar.logic.util.Shared.Team;
import com.elementar.logic.util.maps.LimitedHashMap;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

public class GameserverClientProtocol extends Listener {

	private int match_id;

	private LimitedHashMap<Short, ServerPhysicalClient> player_map;

	private long map_seed;

	private String ip;

	private String server_name;

	private String password;

	private GameserverLogic gameserver_logic;

	public GameserverClientProtocol(GameserverLogic gameserverLogic) {
		gameserver_logic = gameserverLogic;
		ip = gameserverLogic.getMetaDataGameserver().ip;
		server_name = gameserverLogic.getMetaDataGameserver().server_name;
		password = gameserverLogic.getMetaDataGameserver().password;
		map_seed = gameserverLogic.getMetaDataGameserver().seed;
		player_map = new LimitedHashMap<Short, ServerPhysicalClient>(gameserverLogic.getMetaDataGameserver().capacity);
		// match id is a random number between 1 and 1 billion
		match_id = MathUtils.random(1, 1000000000);
	}

	@Override
	public void connected(Connection connection) {
	}

	@Override
	public void received(Connection connection, Object object) {

		short connectionID = (short) connection.getID();

		if (object instanceof AReliablePacket) {
			ServerPhysicalClient client = player_map.get(connectionID);
			if (client != null) {
				AReliablePacket recvdPacket = (AReliablePacket) object;
				client.getReliabilitySystem().recvPacket(recvdPacket);
			}
		}

		if (object instanceof InputRequest) {
			synchronized (player_map) {
				player_map.get(connectionID).addInput((InputRequest) object);
			}
			return;
		}

		if (object instanceof PreMatchRequest) {
			synchronized (player_map) {
				PreMatchRequest req = (PreMatchRequest) object;

				MetaClient metaClient = player_map.get(connectionID).getMetaClient();

				// handle team
				if (metaClient.slot_index == -1)
					metaClient.slot_index = getNextSlotByTeam(req.team);

				if (metaClient.client_state != ClientState.MATCH) {
					if (metaClient.slot_index == -1)
						metaClient.client_state = ClientState.PICKING_TEAM;
					else
						metaClient.client_state = ClientState.PICKING_GAMECLASS;
				}

				if (metaClient.team == null && req.team != null)
					gameserver_logic.addConsoleEvent(new ConsoleEvent(Type.TEAM_SELECTED, metaClient));

				// only set the team if it was not set before
				if (metaClient.team == null)
					metaClient.team = req.team;

				// handle skin
				metaClient.skin_index = req.skin_index;

				// handle pre-selected gameclass
				metaClient.pre_selected_gameclass = req.pre_selected_gameclass;
				// handle selection
				if (metaClient.gameclass_name == null && req.selected_gameclass != null)
					if (isGameclassTaken(player_map, req.selected_gameclass) == false) {
						metaClient.setGameclass(req.selected_gameclass);
						metaClient.client_state = ClientState.MATCH;
						gameserver_logic.addConsoleEvent(new ConsoleEvent(Type.GAMECLASS_SELECTED, metaClient));
						// = ClientState.PICKING_END;
					}
			}

			return;
		}

		if (object instanceof PingResponse) {
			ServerPhysicalClient client = player_map.get(connectionID);
			client.setPingEnd();
		}

		if (object instanceof PlayerJoinRequest) {
			handleJoinRequest(connection, object);
			return;
		}

	}

	private void handleJoinRequest(Connection connection, Object object) {

		if (player_map.isFull())
			connection.sendUDP(new FullServerResponse());
		else {

			PlayerJoinRequest req = (PlayerJoinRequest) object;

			if (req.password.equals(password) == false) {
				connection.sendUDP(new IncorrectPasswordResponse());
			} else {
				synchronized (player_map) {

					if (player_map.containsKey((short) connection.getID()) == false) {

						// create a player
						// slot and team haven't be assigned yet.
						MetaClient newMetaClient = new MetaClient(connection.getID(), req.player_name);
						player_map.put((short) connection.getID(), new ServerPhysicalClient(newMetaClient));
						gameserver_logic.addConsoleEvent(new ConsoleEvent(Type.PLAYER_CONNECTED, newMetaClient));
					}
				}

				connection.sendUDP(new JoinSuccessfulToGameserverResponse(server_name, map_seed, ip, match_id,
						connection.getID(), req.is_host));

			}
		}
	}

	public static boolean isGameclassTaken(LimitedHashMap<Short, ServerPhysicalClient> playerMap,
			GameclassName gameclass) {
		for (PhysicalClient gameClient : playerMap.values())
			if (gameClient.getMetaClient().gameclass_name == gameclass)
				return true;
		return false;
	}

	/**
	 * Returns a TeamSlotPair object, which holds the lowest slot index of the team
	 * with the lowest number of player.
	 * 
	 * @return
	 */
	private int getNextSlotByTeam(Team team) {

		if (team == null)
			return -1;

		int[] availableIndicesTeam1 = new int[Shared.MAX_TEAM_CAPACITY],
				availableIndicesTeam2 = new int[Shared.MAX_TEAM_CAPACITY];

		// fill with available indices (for now all are available)
		for (int i = 0; i < availableIndicesTeam1.length; i++) {
			availableIndicesTeam1[i] = i;
			availableIndicesTeam2[i] = i;
		}

		/*
		 * the list contains player who might have not a valid slot_index
		 */
		for (PhysicalClient client : player_map.values()) {
			int slotIndex = client.getMetaClient().slot_index;
			if (slotIndex >= 0)
				if (client.getMetaClient().team == Team.TEAM_1)
					availableIndicesTeam1[slotIndex] = Shared.MAX_TEAM_CAPACITY;
				else if (client.getMetaClient().team == Team.TEAM_2)
					availableIndicesTeam2[slotIndex] = Shared.MAX_TEAM_CAPACITY;
		}

		int availableSlotIndex;
		if (team == Team.TEAM_1) {
			availableSlotIndex = Util.getMinValue(availableIndicesTeam1);
			if (availableSlotIndex == Shared.MAX_TEAM_CAPACITY)
				return -1;
			else
				return availableSlotIndex;
		} else if (team == Team.TEAM_2) {
			availableSlotIndex = Util.getMinValue(availableIndicesTeam2);
			if (availableSlotIndex == Shared.MAX_TEAM_CAPACITY)
				return -1;
			else
				return availableSlotIndex;
		}
		return -1;
	}

	public LimitedHashMap<Short, ServerPhysicalClient> getPlayerMap() {
		return player_map;
	}

}
