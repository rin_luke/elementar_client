package com.elementar.logic.network.protocols;

import com.elementar.data.container.PlayerData;
import com.elementar.logic.network.gameserver.MetaDataGameserver;
import com.elementar.logic.network.response.CreateGameserverResponse;
import com.elementar.logic.network.response.JoinSuccessfulToMainserverResponse;
import com.elementar.logic.network.response.ServerlistResponse;
import com.elementar.logic.util.lists.GameserverArrayList;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

public class ClientMainserverProtocol extends Listener {

	public static GameserverArrayList<MetaDataGameserver> GAMESERVER_LIST = new GameserverArrayList<MetaDataGameserver>();

	// at the moment not in use
	public static boolean CONNECTION_SUCCESSFUL_MAINSERVER = false;

	/**
	 * While hosting a gameserver the mainserver send back the meta data from
	 * the gameserver.
	 */
	public static MetaDataGameserver MY_HOSTED_GAMESERVER_METADATA;

	@Override
	public void received(Connection connection, java.lang.Object object) {
		if (object instanceof JoinSuccessfulToMainserverResponse) {
			CONNECTION_SUCCESSFUL_MAINSERVER = true;
			PlayerData.connection_id_to_mainserver = ((JoinSuccessfulToMainserverResponse) object).connection_id;
		}

		if (object instanceof CreateGameserverResponse)
			MY_HOSTED_GAMESERVER_METADATA = ((CreateGameserverResponse) object).metadata_gameserver;

		if (object instanceof ServerlistResponse)
			synchronized (GAMESERVER_LIST) {
				GAMESERVER_LIST = ((ServerlistResponse) object).gameserver_list;
				GAMESERVER_LIST.refreshed = true;
			}
	}

}
