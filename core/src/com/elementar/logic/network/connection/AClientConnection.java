package com.elementar.logic.network.connection;

import static com.elementar.logic.util.Shared.MAX_CONNECTING_TIME;

import java.io.IOException;

import com.elementar.logic.network.util.ConnectionEstablisher;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Listener;

/**
 * Abstract class for "Client to Server"-Connections. Note the order
 * (ClientServer...)
 * 
 * @author lukassongajlo
 * 
 */
public abstract class AClientConnection implements Sender {

	public enum ConnectionStatus {
		TRYING, CONNECTED, FAILED, ALREADY_CONNECTED, RESTING
	}

	protected ConnectionStatus connection_status = ConnectionStatus.RESTING;

	protected Listener protocol;

	protected Client client = new Client(10000, 10000) {
		public void close() {

			super.close();
			connection_status = ConnectionStatus.RESTING;
		};

	};
	protected String ip;

	@Override
	public void sendUDP(int connectionID, Object packet) {
		client.sendUDP(packet);
	}

	public void connect(String desiredIP, int tcp, int udp, Listener protocol) {
		this.ip = desiredIP;
		if (connection_status == ConnectionStatus.TRYING)
			return;
		Thread t = new Thread() {
			@Override
			public void run() {

				if (client.isConnected() == false) {
					try {

						connection_status = ConnectionStatus.TRYING;

						ConnectionEstablisher.clientInitialize(client, MAX_CONNECTING_TIME, desiredIP, tcp, udp,
								protocol);

						connection_status = ConnectionStatus.CONNECTED;
					} catch (IOException e) {
						connection_status = ConnectionStatus.FAILED;
					}
				} else
					connection_status = ConnectionStatus.ALREADY_CONNECTED;
			}
		};
		t.start();
	}

	public void disconnect() {
		client.close();
	}

	/**
	 * 
	 * @param status
	 */
	public void setConnectionStatus(ConnectionStatus status) {
		this.connection_status = status;
	}

	/**
	 * 
	 * @return
	 */
	public ConnectionStatus getConnectionStatus() {
		return connection_status;
	}

	public Client getClient() {
		return client;
	}

	public boolean isConnected() {
		return client.isConnected();
	}

	public void setIP(String ip) {
		if (!(ip == null || ip.equals("")))
			this.ip = ip;

	}

	public String getIP() {
		return ip;
	}
}
