package com.elementar.logic.network.connection;

import static com.elementar.logic.util.Shared.IP_MAINSERVER;
import static com.elementar.logic.util.Shared.MAX_CONNECTING_TIME;

import java.io.IOException;

import com.elementar.logic.network.gameserver.GameserverMainserverProtocol;
import com.elementar.logic.network.util.ConnectionEstablisher;
import com.elementar.logic.util.Shared;
import com.esotericsoftware.kryonet.Client;

public class GameserverMainserverConnection {

	private Client client = new Client();

	private GameserverMainserverProtocol protocol;

	public GameserverMainserverConnection() {
		connect();
	}

	public void connect() {
		try {
			protocol = new GameserverMainserverProtocol();
			ConnectionEstablisher.clientInitialize(client, MAX_CONNECTING_TIME, IP_MAINSERVER,
					Shared.GAMESERVER_TCP_TO_MAIN, Shared.GAMESERVER_UDP_TO_MAIN, protocol);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void disconnect() {
		client.close();
	}

	public Client getClient() {
		return client;
	}

	public GameserverMainserverProtocol getProtocol() {
		return protocol;
	}

}
