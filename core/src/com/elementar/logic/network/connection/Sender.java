package com.elementar.logic.network.connection;

/**
 * Common Interface for server and client connections
 * 
 * @author lukassongajlo
 * 
 */
public interface Sender {

	/**
	 * connectionID needed if sender is a server. Ignore if sender is a client.
	 * 
	 * @param connectionID
	 * @param packet
	 */
	public void sendUDP(int connectionID, Object packet);

}
