package com.elementar.logic.network.connection;

import static com.elementar.logic.util.Shared.CLIENT_TCP_TO_MAIN;
import static com.elementar.logic.util.Shared.CLIENT_UDP_TO_MAIN;
import static com.elementar.logic.util.Shared.IP_MAINSERVER;

import com.elementar.logic.network.protocols.ClientMainserverProtocol;

/**
 * Defined as singleton
 * 
 * @author lukassongajlo
 * 
 */
public class ClientMainserverConnection extends AClientConnection {

	private static ClientMainserverConnection instance = new ClientMainserverConnection();

	private ClientMainserverConnection() {
		protocol = new ClientMainserverProtocol();
	}

	public static synchronized ClientMainserverConnection getInstance() {
		return ClientMainserverConnection.instance;
	}

	public void connect() {
		super.connect(IP_MAINSERVER, CLIENT_TCP_TO_MAIN, CLIENT_UDP_TO_MAIN, protocol);
	}

}
