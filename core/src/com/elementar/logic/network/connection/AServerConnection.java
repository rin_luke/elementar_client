package com.elementar.logic.network.connection;

import com.esotericsoftware.kryonet.Server;


/**
 * Abstract class, "Server for Clients". Note the order (ServerClient...) Is
 * also use for mainserver classes
 * 
 * @author lukassongajlo
 * 
 */
public abstract class AServerConnection implements Sender {

	protected Server server = new Server(20000, 20000);

	@Override
	public void sendUDP(int connectionID, Object packet) {
		server.sendToUDP(connectionID, packet);
	}

	public void disconnect() {
		server.close();
	}

	public Server getServer() {
		return server;
	}

}
