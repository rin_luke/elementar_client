package com.elementar.logic.network.connection;

import com.elementar.logic.network.gameserver.GameserverLogic;
import com.elementar.logic.network.protocols.GameserverClientProtocol;
import com.elementar.logic.network.util.ConnectionEstablisher;

public class GameserverClientConnection extends AServerConnection {

	private GameserverClientProtocol protocol;

	public GameserverClientConnection(GameserverLogic gameserverLogic) {
		protocol = new GameserverClientProtocol(gameserverLogic);
		ConnectionEstablisher.serverInitialize(server,
				gameserverLogic.getMetaDataGameserver().port_tcp_game,
				gameserverLogic.getMetaDataGameserver().port_udp_game, protocol);
	}

	public GameserverClientProtocol getProtocol() {
		return protocol;
	}
}
