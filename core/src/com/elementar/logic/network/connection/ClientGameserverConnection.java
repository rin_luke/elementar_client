package com.elementar.logic.network.connection;

import com.elementar.logic.network.protocols.ClientGameserverProtocol;

/**
 * Defined as Singleton
 * 
 * @author lukassongajlo
 * 
 */
public class ClientGameserverConnection extends AClientConnection {

	private static ClientGameserverConnection instance = new ClientGameserverConnection();

	private ClientGameserverConnection() {
		protocol = new ClientGameserverProtocol();
	}

	public static ClientGameserverConnection getInstance() {
		return ClientGameserverConnection.instance;
	}

	public void connect(String desiredIP, int portTCP, int portUDP) {
		super.connect(desiredIP, portTCP, portUDP, protocol);
	}

}