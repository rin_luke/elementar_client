package com.elementar.logic.network.requests;

import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.logic.network.response.AReliablePacket;
import com.elementar.logic.util.Shared.Team;

public class PreMatchRequest extends AReliablePacket {

	public Team team;
	public GameclassName pre_selected_gameclass;
	public GameclassName selected_gameclass;
	public int skin_index;

	public PreMatchRequest() {
	}

}
