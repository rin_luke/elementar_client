package com.elementar.logic.network.requests;

import com.elementar.logic.network.gameserver.MetaDataGameserver;

/**
 * The request from gameserver to mainserver
 * 
 * @author lukassongajlo
 * 
 */
public class GameserverJoinRequest {

	public MetaDataGameserver metadata_gameserver;

	public GameserverJoinRequest() {

	}

	public GameserverJoinRequest(MetaDataGameserver metaDataGameServer) {
		this.metadata_gameserver = metaDataGameServer;
	}

}
