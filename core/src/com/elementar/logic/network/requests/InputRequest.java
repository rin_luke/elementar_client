package com.elementar.logic.network.requests;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.network.response.AReliablePacket;

/**
 * Contains a list of input-entities (active_keys) that are "non processed". So
 * the server should simulate that input. The input requests are identified by
 * their message id.
 * <p>
 * Usage: the request is sent with an ID (message id), the server processes it
 * and send it back with that ID, so you know which input the server processed
 * 
 * @author lukassongajlo
 * 
 */
public class InputRequest extends AReliablePacket {

	public short id_combo_ally;

	public Vector2 cursor_pos;

	/**
	 * holds the current keys as bit-field.
	 */
	public short non_processed_input;

	public String chat_message;

	public transient long arrival_time;

	public InputRequest() {

	}

	/**
	 * 
	 * @param id
	 * @param input
	 * @param cursorPos
	 * @param idComboAlly
	 * @param message
	 */
	public InputRequest(int id, short input, Vector2 cursorPos, short idComboAlly, String message) {
		super(id);
		non_processed_input = input;
		cursor_pos = cursorPos;
		id_combo_ally = idComboAlly;
		chat_message = message;
	}

	/**
	 * Copy constructor
	 * 
	 * @param req
	 */
	public InputRequest(InputRequest req) {
		super(req.message_id);
		non_processed_input = req.non_processed_input;
		cursor_pos = new Vector2(req.cursor_pos);
		id_combo_ally = req.id_combo_ally;
		chat_message = req.chat_message;
	}

	@Override
	public String toString() {

		return " [ id = " + message_id + " ] " + Integer.toBinaryString(non_processed_input);
	}
}
