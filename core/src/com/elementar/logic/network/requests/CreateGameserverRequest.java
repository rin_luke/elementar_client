package com.elementar.logic.network.requests;

import com.elementar.gui.modules.roots.HostRoot;
import com.elementar.logic.network.gameserver.MetaDataGameserver;

/**
 * In {@link HostRoot} the player can sends a {@link CreateGameserverRequest}
 * object to the mainserver with {@link #metadata_gameserver}.
 * 
 * @author lukassongajlo
 * 
 */
public class CreateGameserverRequest {
	public MetaDataGameserver metadata_gameserver;

	public CreateGameserverRequest() {
	}

	public CreateGameserverRequest(MetaDataGameserver metaDataGameServer) {
		this.metadata_gameserver = metaDataGameServer;
	}

}