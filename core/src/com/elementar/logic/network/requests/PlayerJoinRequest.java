package com.elementar.logic.network.requests;

public class PlayerJoinRequest {

	public String player_name;
	public String password;
	public boolean is_host;

	public PlayerJoinRequest() {

	}

	public PlayerJoinRequest(String name, String password, boolean isHost) {
		this.player_name = name;
		this.password = password;
		this.is_host = isHost;
	}

}
