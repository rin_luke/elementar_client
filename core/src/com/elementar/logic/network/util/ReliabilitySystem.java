package com.elementar.logic.network.util;

import java.util.Iterator;
import java.util.LinkedList;

import com.elementar.logic.network.connection.Sender;
import com.elementar.logic.network.response.AReliablePacket;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.lists.MaintainingLimitedArrayList;

/**
 * Based on:
 * https://gafferongames.com/post/reliability_ordering_and_congestion_avoidance_over_udp/
 * 
 * @author lukassongajlo
 */
public class ReliabilitySystem {

	private int local_sequence_id = 0;
	private int remote_sequence_id = 0;

	/**
	 * sent packets which have not been acked yet (kept until rtt_maximum)
	 */
	private LinkedList<AReliablePacket> pending_acks_packets = new LinkedList<AReliablePacket>();
	/**
	 * received packets for determining the ack-bitfield. The ack-bitfield will be
	 * set in the state of composing a new packet. If the a packet is inside of
	 * {@link #received_packets} the n-th bit is set to 1. Designed as
	 * {@link MaintainingLimitedArrayList} to guarantee a limit.
	 */
	private MaintainingLimitedArrayList<AReliablePacket> received_packets = new MaintainingLimitedArrayList<AReliablePacket>(
			32);

	/**
	 * sent packets that haven't been 'acked'. Those have to be re-sent, see
	 * {@link #update(Sender, int)}
	 */
	private LinkedList<AReliablePacket> lost_packets = new LinkedList<AReliablePacket>();

	// TODO later useful to implement a congestion avoidence
	private final float MAX_RTT = 1f;
	private MovingAverage rtt_moving_average = new MovingAverage(0.1f);
	private float rtt;

	public final float HIGH_SEND_RATE = 1 / 25f, LOW_SEND_RATE = 1 / 10f;

	/**
	 * Before sending packet, it has to be prepared with meta data (sequence id,
	 * ack, ackBits)
	 * 
	 * @param packet
	 */
	public void preparePacket(AReliablePacket packet) {

		packet.sequence_id = local_sequence_id;
		packet.ack = remote_sequence_id;
		packet.ack_bits = generateAckBits();

		packet.time = 0;
		synchronized (pending_acks_packets) {
			pending_acks_packets.add(packet);
		}

		local_sequence_id++;

	}

	private int generateAckBits() {

		int ackBits = 0;
		int bitIndex = 0;
		synchronized (received_packets) {
			for (AReliablePacket recvdPacket : received_packets) {

				// bitIndex = remote_sequence_id - 1 -recvdPacket.sequence_id;
				bitIndex = getBitIndex(remote_sequence_id, recvdPacket.sequence_id);

				if (bitIndex < 32 && bitIndex > -1)
					ackBits |= 1 << bitIndex;
			}
		}

		return ackBits;
	}

	/**
	 * 
	 * @param packet
	 */
	public void recvPacket(AReliablePacket packet) {

		synchronized (received_packets) {
			received_packets.add(packet);
		}

		// update remote id
		if (packet.sequence_id > remote_sequence_id)
			remote_sequence_id = packet.sequence_id;

		// process ack
		synchronized (pending_acks_packets) {

			Iterator<AReliablePacket> it = pending_acks_packets.iterator();
			while (it.hasNext() == true) {
				AReliablePacket unAckedPacket = it.next();

				boolean acked = false;
				// determine 'acked'

				// is the un-acked packet inside the bitfield?
				int bitIndex = getBitIndex(packet.ack, unAckedPacket.sequence_id);

				if (bitIndex < 32 && bitIndex > -1)
					acked = ((packet.ack_bits >> bitIndex) & 1) == 1;

				if (acked == true) {
					// remove from pending acks list
					it.remove();

					rtt = rtt_moving_average.average(unAckedPacket.time);

				}
				// those which got not acked, will resend after max_rtt

			}
		}
	}

	/**
	 * get the bit position between the passed sequences to set the ack bitfield
	 * 
	 * @param ack
	 * @param sequence
	 * @return
	 */
	private int getBitIndex(int ack, int sequence) {
		return ack - 1 - sequence;
	}

	/**
	 * 
	 * @param sender       , for re-send lost packets
	 * @param connectionID , for re-send lost packets
	 */
	public void update(Sender sender, int connectionID) {

		// update pending acks packets
		synchronized (pending_acks_packets) {

			// increase time of pending acks packets
			Iterator<AReliablePacket> it = pending_acks_packets.iterator();
			while (it.hasNext() == true) {
				AReliablePacket unAckedPacket = it.next();

				unAckedPacket.time += Shared.SEND_AND_UPDATE_RATE_WORLD;

				/*
				 * kick packets out which exceeded 1 second to re-send them
				 */
				if (unAckedPacket.time > MAX_RTT) {
					it.remove();
					lost_packets.add(unAckedPacket);

					rtt = rtt_moving_average.average(MAX_RTT);
				}

			}
		}

		// re-send lost packets
		Iterator<AReliablePacket> itLostPackets = lost_packets.iterator();
		AReliablePacket lostPacket;
		while (itLostPackets.hasNext() == true) {
			lostPacket = itLostPackets.next();
			preparePacket(lostPacket);

			/*
			 * re-send lost packets (lost packets are packets which haven't been acked
			 * within 1 second)
			 */
			sender.sendUDP(connectionID, lostPacket);

			itLostPackets.remove();
		}

	}

	private class MovingAverage {
		private float alpha;
		private Float oldValue;

		public MovingAverage(float alpha) {
			this.alpha = alpha;
		}

		public float average(float value) {
			if (oldValue == null) {
				oldValue = value;
				return value;
			}
			float newValue = oldValue + alpha * (value - oldValue);
			oldValue = newValue;
			return newValue;
		}
	}

	public float getRTT() {
		return rtt;
	}

}
