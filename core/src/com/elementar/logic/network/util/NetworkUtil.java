package com.elementar.logic.network.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import com.badlogic.gdx.math.Vector2;
import com.elementar.data.container.KeysConfiguration.WorldAction;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.characters.SparseClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.skills.SparseComboStorage;
import com.elementar.logic.emission.SparseProjectile;
import com.elementar.logic.emission.Trigger;
import com.elementar.logic.emission.EmissionRuntimeData.ComboType;
import com.elementar.logic.emission.effect.AEffect;
import com.elementar.logic.emission.effect.AEffectCC;
import com.elementar.logic.emission.effect.AEffectImpulse;
import com.elementar.logic.emission.effect.AEffectTimed;
import com.elementar.logic.emission.effect.AEffectWithNumber;
import com.elementar.logic.emission.effect.EffectBlackhole;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.emission.effect.EffectDeathTime;
import com.elementar.logic.emission.effect.EffectDisarmed;
import com.elementar.logic.emission.effect.EffectEmpty;
import com.elementar.logic.emission.effect.EffectHeal;
import com.elementar.logic.emission.effect.EffectHealthChangeOverTime;
import com.elementar.logic.emission.effect.EffectImpulseCursorFollowing;
import com.elementar.logic.emission.effect.EffectImpulseCursorOnetime;
import com.elementar.logic.emission.effect.EffectImpulseEmitterFollowing;
import com.elementar.logic.emission.effect.EffectImpulsePrevProj;
import com.elementar.logic.emission.effect.EffectImpulseToEmission;
import com.elementar.logic.emission.effect.EffectImpulseY;
import com.elementar.logic.emission.effect.EffectLightPort;
import com.elementar.logic.emission.effect.EffectMana;
import com.elementar.logic.emission.effect.EffectResurrection;
import com.elementar.logic.emission.effect.EffectRoot;
import com.elementar.logic.emission.effect.EffectSilence;
import com.elementar.logic.emission.effect.EffectStun;
import com.elementar.logic.emission.effect.EffectVelChange;
import com.elementar.logic.map.buildings.SparseBase;
import com.elementar.logic.map.buildings.SparseTower;
import com.elementar.logic.network.connection.AClientConnection.ConnectionStatus;
import com.elementar.logic.network.gameserver.MetaDataGameserver;
import com.elementar.logic.network.gameserver.GameserverLogic.ServerState;
import com.elementar.logic.network.requests.CreateGameserverRequest;
import com.elementar.logic.network.requests.GameserverJoinRequest;
import com.elementar.logic.network.requests.InputRequest;
import com.elementar.logic.network.requests.PingRequest;
import com.elementar.logic.network.requests.PlayerJoinRequest;
import com.elementar.logic.network.requests.PreMatchRequest;
import com.elementar.logic.network.requests.ServerListRequest;
import com.elementar.logic.network.response.AInfo;
import com.elementar.logic.network.response.CreateGameserverResponse;
import com.elementar.logic.network.response.FullServerResponse;
import com.elementar.logic.network.response.IncorrectPasswordResponse;
import com.elementar.logic.network.response.InfoAnimations;
import com.elementar.logic.network.response.InfoDeathTime;
import com.elementar.logic.network.response.InfoEmissionInterruption;
import com.elementar.logic.network.response.InfoMessage;
import com.elementar.logic.network.response.InfoTriggers;
import com.elementar.logic.network.response.InfoWinnerTeam;
import com.elementar.logic.network.response.JoinSuccessfulToGameserverResponse;
import com.elementar.logic.network.response.JoinSuccessfulToMainserverResponse;
import com.elementar.logic.network.response.MetaDataResponse;
import com.elementar.logic.network.response.PingResponse;
import com.elementar.logic.network.response.ServerlistResponse;
import com.elementar.logic.network.response.WorldDataResponse;
import com.elementar.logic.util.ErrorMessage;
import com.elementar.logic.util.SparseObject;
import com.elementar.logic.util.Shared.ClientState;
import com.elementar.logic.util.Shared.PickMode;
import com.elementar.logic.util.Shared.Team;
import com.elementar.logic.util.lists.GameserverArrayList;
import com.elementar.logic.util.lists.NotNullArrayList;
import com.esotericsoftware.kryo.Kryo;

public class NetworkUtil {

	public static ConnectionStatus INTERNET_CONNECTION_STATUS = ConnectionStatus.RESTING;

	/**
	 * if <code>null</code> is returned, there's no internet connection
	 * 
	 * @return
	 */
	public static String getExternalIP() {
		URL connection = null;
		URLConnection con = null;
		String externalIP = null;
		BufferedReader reader = null;
		try {
			connection = new URL("http://checkip.amazonaws.com/");
			con = connection.openConnection();
			con.setConnectTimeout(2000);
			reader = new BufferedReader(new InputStreamReader(con.getInputStream()));

			externalIP = reader.readLine();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return externalIP;
	}

	/**
	 * 
	 * @param connectTimeout, in seconds
	 * @return
	 */
	public static void isInternetReachable(float connectTimeout) {

		// TODO runs out of memory after a while.
		Thread t = new Thread() {
			@Override
			public void run() {

				try {
					// make a URL to a known source
					URL url = new URL("http://www.google.com");

					// open a connection to that source
					HttpURLConnection urlConnect = (HttpURLConnection) url.openConnection();
					urlConnect.setConnectTimeout((int) (connectTimeout * 1000));
					/*
					 * trying to retrieve data from the source. If there is no connection, this line
					 * will fail
					 */
					urlConnect.getContent();

					INTERNET_CONNECTION_STATUS = ConnectionStatus.CONNECTED;

					urlConnect.disconnect();

				} catch (Exception e) {
					INTERNET_CONNECTION_STATUS = ConnectionStatus.FAILED;
				}

			}
		};
		t.start();

	}

	/**
	 * 
	 * @param kryo
	 */
	public static void register(Kryo kryo) {
		kryo.register(CreateGameserverRequest.class);
		kryo.register(CreateGameserverResponse.class);
		kryo.register(JoinSuccessfulToMainserverResponse.class);
		kryo.register(ArrayList.class);

		kryo.register(PickMode.class);
		kryo.register(PlayerJoinRequest.class);
		kryo.register(JoinSuccessfulToGameserverResponse.class);

		kryo.register(ServerListRequest.class);
		kryo.register(ServerlistResponse.class);
		kryo.register(GameserverArrayList.class);
		kryo.register(MetaDataGameserver.class);
		kryo.register(SparseClient.class);
		kryo.register(WorldDataResponse.class);
		kryo.register(GameserverJoinRequest.class);
		kryo.register(GameclassName.class);
		kryo.register(HashMap.class);
		// input
		kryo.register(WorldAction.class);
		// lobby
		kryo.register(MetaDataResponse.class);
		kryo.register(MetaClient.class);
		kryo.register(ClientState.class);
		kryo.register(ServerState.class);
		kryo.register(Team.class);
		kryo.register(PreMatchRequest.class);

		// map-vertices TODO check whether the vector[] is necessary
		kryo.register(Vector2[].class);
		kryo.register(Vector2.class);
		kryo.register(Class.class);

		kryo.register(ErrorMessage.class);

		kryo.register(PingRequest.class);
		kryo.register(PingResponse.class);

		kryo.register(InputRequest.class);
		kryo.register(NotNullArrayList.class);
		kryo.register(LinkedList.class);
		kryo.register(SparseProjectile.class);
		kryo.register(AnimationName.class);
		kryo.register(SparseObject.class);
		kryo.register(SparseBase.class);
		kryo.register(SparseTower.class);
		kryo.register(Trigger.class);
		kryo.register(SparseComboStorage.class);

		kryo.register(ComboType.class);
		kryo.register(AEffect.class);
		kryo.register(AEffectTimed.class);
		kryo.register(AEffectCC.class);
		kryo.register(EffectDisarmed.class);
		kryo.register(EffectRoot.class);
		kryo.register(EffectSilence.class);
		kryo.register(EffectStun.class);
		kryo.register(EffectBlackhole.class);
		kryo.register(EffectLightPort.class);
		kryo.register(AEffectImpulse.class);
		kryo.register(EffectImpulseCursorOnetime.class);
		kryo.register(EffectImpulseCursorFollowing.class);
		kryo.register(EffectImpulseEmitterFollowing.class);
		kryo.register(EffectImpulsePrevProj.class);
		kryo.register(EffectImpulseToEmission.class);
		kryo.register(EffectImpulseY.class);
		kryo.register(EffectDeathTime.class);
		kryo.register(EffectHealthChangeOverTime.class);
		kryo.register(EffectResurrection.class);
		kryo.register(EffectVelChange.class);
		kryo.register(AEffectWithNumber.class);
		kryo.register(EffectDamage.class);
		kryo.register(EffectHeal.class);
		kryo.register(EffectMana.class);
		kryo.register(EffectEmpty.class);

		// event based packets
		kryo.register(AInfo.class);
		kryo.register(InfoDeathTime.class);
		kryo.register(InfoTriggers.class);
		kryo.register(InfoAnimations.class);
		kryo.register(InfoEmissionInterruption.class);
		kryo.register(InfoMessage.class);
		kryo.register(InfoWinnerTeam.class);
		kryo.register(short[].class);

		// errors
		kryo.register(FullServerResponse.class);
		kryo.register(IncorrectPasswordResponse.class);

	}

}
