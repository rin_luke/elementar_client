package com.elementar.logic.network.util;

import com.elementar.gui.widgets.CustomTextbutton;
import com.elementar.logic.network.connection.AClientConnection;
import com.elementar.logic.network.requests.PingRequest;
import com.elementar.logic.network.response.PingResponse;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

public class PingStarter extends Thread {

	private String ip;
	private int capacity;
	private int port_udp, port_tcp;
	private CustomTextbutton ping_button, capacity_button;

	// data we want to receive
	private Long ping;
	private byte server_size;

	/**
	 * @param pingButton
	 * @param capacityButton
	 * @param capacity
	 * @param ip
	 * @param portTCP
	 * @param portUDP
	 */
	public PingStarter(CustomTextbutton pingButton, CustomTextbutton capacityButton, int capacity, String ip,
			int portTCP, int portUDP) {
		this.ping_button = pingButton;
		this.capacity_button = capacityButton;
		this.ip = ip;
		this.capacity = capacity;
		this.port_tcp = portTCP;
		this.port_udp = portUDP;
		start();
	}

	@Override
	public void run() {

		// try each Xms until we get a result
		PingConnection connection = new PingConnection();
		PingClientProtocol protocol = new PingClientProtocol();
		connection.connect(ip, port_tcp, port_udp, protocol);

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// send ping request
		if (connection.isConnected() == true) {
			connection.sendUDP(connection.getClient().getID(), new PingRequest());
			protocol.start_time = System.currentTimeMillis();
		}

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		connection.disconnect();

		if (ping != null) {
			// set widgets text
			ping_button.setText("" + ping);
			capacity_button.setText(server_size + " / " + capacity);
		}
	}

	private class PingConnection extends AClientConnection {

	}

	private class PingClientProtocol extends Listener {

		private long start_time;

		@Override
		public void received(Connection connection, Object object) {

			if (object instanceof PingResponse) {
				PingResponse res = (PingResponse) object;
				ping = new Long(System.currentTimeMillis() - start_time);
				server_size = res.server_size;
			}

		}

	}

}