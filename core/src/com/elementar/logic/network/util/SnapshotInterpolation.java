package com.elementar.logic.network.util;

import java.util.Iterator;
import java.util.LinkedList;

import com.badlogic.gdx.math.MathUtils;
import com.elementar.data.container.ParticleContainer;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.characters.SparseClient;
import com.elementar.logic.creeps.SparseCreep;
import com.elementar.logic.emission.SparseProjectile;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.network.response.WorldDataResponse;
import com.elementar.logic.util.Shared;

/**
 * Useful information which describes snapshot interpolation perfectly.
 * https://developer.valvesoftware.com/wiki/Source_Multiplayer_Networking
 * 
 * @author lukassongajlo
 *
 */
public class SnapshotInterpolation {

	private final long INTERPOLATION_BUFFER = Shared.SEND_AND_UPDATE_RATE_IN_MS * 3;

	private WorldDataResponse prev_snapshot, next_snapshot;

	public SnapshotInterpolation() {
	}

	/**
	 * Returns the interpolation of two snapshots that are already arrived.
	 * 
	 * @param list
	 * @return
	 */
	public WorldDataResponse getInterpolation(SnapshotList snapshots) {

		if (snapshots.size() < 2)
			return null;

		long bufferedTime = System.currentTimeMillis() - INTERPOLATION_BUFFER;

		prev_snapshot = null;
		next_snapshot = null;

		// find the both snapshots that frame the bufferedTime
		for (int i = 0; i < snapshots.size(); i++) {
			WorldDataResponse prevSnapshot = snapshots.get(i);
			if (prevSnapshot.arrival_time < bufferedTime) {
				if (prev_snapshot == null)
					prev_snapshot = prevSnapshot;
				else if (prevSnapshot.arrival_time > prev_snapshot.arrival_time)
					prev_snapshot = prevSnapshot;
			}
		}

		for (int i = 0; i < snapshots.size(); i++) {
			WorldDataResponse nextSnapshot = snapshots.get(i);
			if (nextSnapshot.arrival_time > bufferedTime) {
				if (next_snapshot == null)
					next_snapshot = nextSnapshot;
				else if (nextSnapshot.arrival_time < next_snapshot.arrival_time)
					next_snapshot = nextSnapshot;
			}
		}

		// set fields

		if (prev_snapshot == null)
			return null;

		if (next_snapshot != null) {

			float fraction = ((bufferedTime - prev_snapshot.arrival_time) * 1f)
					/ ((next_snapshot.arrival_time - prev_snapshot.arrival_time) * 1f);

			prev_snapshot.arrival_time = bufferedTime;

			// interpolate world clients
			SparseClient prevClient;
			for (int i = 0; i < prev_snapshot.player_list.size(); i++) {

				prevClient = prev_snapshot.player_list.get(i);

				// search the corresponding client
				for (SparseClient correspondingClient : next_snapshot.player_list)
					if (prevClient.connection_id == correspondingClient.connection_id) {

						interpolateClients(prevClient, correspondingClient, fraction);

						break;
					}
			}

		} else {
			// extrapolate

			// float time = bufferedTime - prev_snapshot.arrival_time;

		}

		return prev_snapshot;
	}

	/**
	 * Returns the interpolated {@link SparseClient}
	 * 
	 * @param prevClient
	 * @param recentClient
	 * @param fraction
	 * @return
	 */
	public static SparseClient interpolateClients(SparseClient prevClient, SparseClient recentClient, float fraction) {

		/*
		 * if distance between two points is too far we take the new position
		 * immediately
		 */
		if (prevClient.pos.dst(recentClient.pos) < 4)
			prevClient.pos.lerp(recentClient.pos, fraction);
		else
			prevClient.pos.set(recentClient.pos);

		prevClient.cursor_pos.lerp(recentClient.cursor_pos, fraction);
		prevClient.health_current = (short) MathUtils.lerp(prevClient.health_current, recentClient.health_current,
				fraction);
		prevClient.mana_current = (short) MathUtils.lerp(prevClient.mana_current, recentClient.mana_current, fraction);

		prevClient.cooldown_skill0 = recentClient.cooldown_skill0;
		prevClient.cooldown_skill1 = recentClient.cooldown_skill1;
		prevClient.cooldown_skill2 = recentClient.cooldown_skill2;

		prevClient = interpolateProjectiles(prevClient, recentClient, fraction);

		return prevClient;
	}

	public static SparseClient interpolateProjectiles(SparseClient pastClient, SparseClient recentClient, float alpha) {

		interpolateProjectiles(pastClient.sparse_projectiles, recentClient.sparse_projectiles, alpha);

		return pastClient;

	}

	public static void interpolateProjectiles(LinkedList<SparseProjectile> pastProjectiles,
			LinkedList<SparseProjectile> recentProjectiles, float alpha) {
		// declare past and recent projectiles
		SparseProjectile pastProjectile, recentProjectile;

		// declare iterators
		Iterator<SparseProjectile> itPast, itRecent;

		itPast = pastProjectiles.iterator();
		while (itPast.hasNext() == true) {
			pastProjectile = itPast.next();
			recentProjectile = null;

			// find corresponding projectile if existing
			itRecent = recentProjectiles.iterator();
			while (itRecent.hasNext() == true) {
				recentProjectile = itRecent.next();
				if (recentProjectile.projectile_id == pastProjectile.projectile_id)
					// the first entry is the own pool id
					if (recentProjectile.pool_ids[0] == pastProjectile.pool_ids[0])
						break;

				recentProjectile = null;
			}

			if (recentProjectile != null) {

				pastProjectile.pos.lerp(recentProjectile.pos, alpha);

				pastProjectile.direction = (short) MathUtils.lerpAngleDeg(pastProjectile.direction,
						recentProjectile.direction, alpha);

			}

			// override position if it is attached.
			OmniClient attachedClient = LogicTier.WORLD_PLAYER_MAP.get(pastProjectile.connection_id_attached);
			if (attachedClient != null) {
				AEmissionDef def = ParticleContainer.getDef(pastProjectile.pool_ids[0]);
				float offsetAttached = def.getDefProjectile().getOffsetAttached();
				pastProjectile.pos.set(
						attachedClient.getDrawableClient().pos.x
								+ offsetAttached * MathUtils.cosDeg(pastProjectile.direction),
						attachedClient.getDrawableClient().pos.y
								+ offsetAttached * MathUtils.sinDeg(pastProjectile.direction));
			}
		}
	}

	public static SparseCreep interpolateCreeps(SparseCreep pastCreep, SparseCreep recentCreep, float alpha) {
		// pastClient.track_0 = recentClient.track_0;
		// pastClient.track_1 = recentClient.track_1;
		// pastClient.track_2 = recentClient.track_2;
		// pastClient.track_3 = recentClient.track_3;

		pastCreep.pos.lerp(recentCreep.pos, alpha);
		interpolateProjectiles(pastCreep.sparse_projectiles, recentCreep.sparse_projectiles, alpha);

		return pastCreep;
	}

}