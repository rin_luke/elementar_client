package com.elementar.logic.network.util;

import com.elementar.logic.util.Shared.Team;

public class TeamSlotPair {
	public Team team;
	public int slot_index;

	public TeamSlotPair(Team team, int slotIndex) {
		this.team = team;
		this.slot_index = slotIndex;
	}
}
