package com.elementar.logic.network.util;

import java.util.ArrayList;

import com.elementar.logic.network.response.WorldDataResponse;
import com.elementar.logic.util.lists.MaintainingLimitedArrayList;

public class SnapshotList extends MaintainingLimitedArrayList<WorldDataResponse> {

	private int most_recent_id;

	private ArrayList<WorldDataResponse> info_list = new ArrayList<>();

	public SnapshotList(int limit) {
		super(limit);
		most_recent_id = Integer.MIN_VALUE;
	}

	@Override
	public boolean add(WorldDataResponse snapshot) {

		/*
		 * infos have to be reliable, so we take them anyway
		 */
		info_list.add(snapshot);

		/*
		 * snapshots have not to be reliable, we take only the most recent ones
		 */
		if (snapshot.message_id > most_recent_id) {

			// System.out.println("SnapshotList.add() incoming message id = " +
			// snapshot.message_id
			// + ", server_steps = " + snapshot.server_steps + ",
			// diffServerSteps = "
			// + ((snapshot.message_id - most_recent_id) -
			// snapshot.server_steps));

			most_recent_id = snapshot.message_id;

			/*
			 * the snapshot gets a timestamp.
			 */
			snapshot.arrival_time = System.currentTimeMillis();

			return super.add(snapshot);
		}

		// HashMap<Short, InfoAnimations> infoList = new HashMap<>();
		//
		// for (SparseClient c : snapshot.player_list) {
		// for (AInfo i : c.player_infos) {
		// if (i instanceof InfoAnimations)
		// infoList.put(c.connection_id, (InfoAnimations) i);
		// }
		// }
		//
		// if (infoList.values().size() > 0) {
		// System.out.println("SnapshotList.add() TOO OLD, message id = "
		// + snapshot.message_id + ", infos = ");
		// for (Entry<Short, InfoAnimations> entry : infoList.entrySet()) {
		// System.out.println("SnapshotList.add() connection_id = " +
		// entry.getKey()
		// + ", infoAnim = " + entry.getValue());
		// }
		// }
		return true;
	}

	public ArrayList<WorldDataResponse> getInfoList() {
		return info_list;
	}

}
