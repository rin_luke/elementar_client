package com.elementar.logic.network.util;

import java.io.IOException;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

public class ConnectionEstablisher {

	/**
	 * 
	 * @param server
	 * @param tcp
	 * @param udp
	 * @param protocol
	 * @return
	 */
	public static void serverInitialize(Server server, int tcp, int udp, Listener protocol) {
		if (server != null) {
			NetworkUtil.register(server.getKryo());
			server.start();
			try {
				server.bind(tcp, udp);
			} catch (IOException e) {
				e.getStackTrace();
			}
			server.addListener(protocol);
		}
	}

	/**
	 * 
	 * @param client
	 * @param connectingTime
	 * @param ip
	 * @param tcp
	 * @param udp
	 * @param protocol
	 * @throws IOException
	 */
	public static void clientInitialize(Client client, int connectingTime, String ip, int tcp,
			int udp, Listener protocol) throws IOException {
		if (client != null) {
			NetworkUtil.register(client.getKryo());
			client.start();
			client.connect(connectingTime, ip, tcp, udp);
			client.addListener(protocol);
		}
	}
}
