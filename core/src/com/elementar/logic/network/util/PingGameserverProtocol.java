package com.elementar.logic.network.util;

import java.util.HashMap;

import com.elementar.logic.characters.ServerPhysicalClient;
import com.elementar.logic.network.requests.PingRequest;
import com.elementar.logic.network.response.PingResponse;
import com.elementar.logic.util.maps.LimitedHashMap;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

public class PingGameserverProtocol extends Listener {

	private HashMap<Short, ServerPhysicalClient> player_map;

	public PingGameserverProtocol(LimitedHashMap<Short, ServerPhysicalClient> playerMap) {
		this.player_map = playerMap;
	}

	@Override
	public void received(Connection connection, Object object) {
		if (object instanceof PingRequest)
			connection.sendUDP(new PingResponse((byte) player_map.size()));

	}
}
