package com.elementar.logic.network.response;

import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;

public class InfoAnimations extends AInfo {

	public AnimationName track_0, track_1, track_2, track_3, track_4;
	/**
	 * -1 if no skill is active, otherwise 0, 1, ...
	 * <p>
	 * Will be set in physics and used in {@link DrawableClient} to get the
	 * right skill animation and timeScale/duration
	 */
	public int current_table_skill_index;

	public InfoAnimations() {
	}

	public InfoAnimations(int skillIndex, AnimationName track0, AnimationName track1,
			AnimationName track2, AnimationName track3, AnimationName track4) {
		this.current_table_skill_index = skillIndex;
		this.track_0 = track0;
		this.track_1 = track1;
		this.track_2 = track2;
		this.track_3 = track3;
		this.track_4 = track4;
	}

	public void clear() {
		track_0 = null;
		track_1 = null;
		track_2 = null;
		track_3 = null;
		track_4 = null;
		current_table_skill_index = -1;
	}

	@Override
	public String toString() {
		return "[track0 = " + track_0 + ", track1 = " + track_1 + ", track2 = " + track_2
				+ ", track3 = " + track_3 + ", track4 = " + track_4 + "]";
	}

}
