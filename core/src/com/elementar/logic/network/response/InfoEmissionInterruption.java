package com.elementar.logic.network.response;

import com.elementar.logic.emission.EmissionList;

public class InfoEmissionInterruption extends AInfo {

	/**
	 * ID of sent trigger to find emission in {@link EmissionList}
	 */
	public short trigger_id;

	public InfoEmissionInterruption() {
	}

	public InfoEmissionInterruption(short triggerID) {
		this.trigger_id = triggerID;
	}
}
