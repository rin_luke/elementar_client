package com.elementar.logic.network.response;

import java.util.ArrayList;

import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.network.gameserver.GameserverLogic.ServerState;

/**
 * MetaData about players + server are needed especially in lobby phases.
 * 
 * @author lukassongajlo
 * 
 */
public class MetaDataResponse {

	public ArrayList<MetaClient> meta_player_list;

	public float gameserver_time;

	public byte gameserver_capacity;

	public byte team1_remaining_lives, team2_remaining_lives;

	public ServerState server_state;
	
	public transient long arrival_time;

	public MetaDataResponse() {
	}

	/**
	 * 
	 * @param metaPlayerList
	 * @param capacity
	 * @param gameserverTime
	 * @param team1RemainingLives
	 * @param team2RemainingLives
	 * @param serverState,
	 *            pre_, post_ or in_match
	 */
	public MetaDataResponse(ArrayList<MetaClient> metaPlayerList, int capacity,
			float gameserverTime, int team1RemainingLives, int team2RemainingLives,
			ServerState serverState) {
		this.gameserver_time = gameserverTime;
		this.meta_player_list = metaPlayerList;
		this.gameserver_capacity = (byte) capacity;
		this.team1_remaining_lives = (byte) team1RemainingLives;
		this.team2_remaining_lives = (byte) team2RemainingLives;
		this.server_state = serverState;
	}
}
