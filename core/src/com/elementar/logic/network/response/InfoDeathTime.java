package com.elementar.logic.network.response;

public class InfoDeathTime extends AInfo {

	public float death_time;

	public InfoDeathTime() {

	}

	public InfoDeathTime(float deathTime) {
		this.death_time = deathTime;
	}

}
