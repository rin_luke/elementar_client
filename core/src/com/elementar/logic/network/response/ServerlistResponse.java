package com.elementar.logic.network.response;

import com.elementar.logic.network.gameserver.MetaDataGameserver;
import com.elementar.logic.util.lists.GameserverArrayList;

public class ServerlistResponse {

	public GameserverArrayList<MetaDataGameserver> gameserver_list;

	public ServerlistResponse() {
	}

	public ServerlistResponse(
			GameserverArrayList<MetaDataGameserver> gameserverList) {
		this.gameserver_list = gameserverList;
	}
}
