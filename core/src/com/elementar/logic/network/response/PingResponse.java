package com.elementar.logic.network.response;

public class PingResponse {

	public byte server_size;

	public PingResponse() {

	}

	public PingResponse(byte size) {
		server_size = size;
	}

}
