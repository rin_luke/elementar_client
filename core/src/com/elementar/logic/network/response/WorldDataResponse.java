package com.elementar.logic.network.response;

import java.util.ArrayList;

import com.elementar.logic.characters.SparseClient;

/**
 * Holds a playerlist of {@link SparseClient}s. These clients are designed for
 * continuous transfer from server to players.
 * 
 * @author lukassongajlo
 * 
 */
public class WorldDataResponse extends AReliablePacket {

	public float gameserver_time;

	// shared data; data of other players you need
	public ArrayList<SparseClient> player_list;

	public transient long arrival_time;

	public WorldDataResponse() {
	}

	public WorldDataResponse(ArrayList<SparseClient> playerList, float gameserverTime) {
		this.player_list = playerList;
		this.gameserver_time = gameserverTime;
	}

}
