package com.elementar.logic.network.response;

public class CountDownResponse {

	public float elapsed_time;

	public CountDownResponse() {

	}

	public CountDownResponse(float elapsedTime) {
		this.elapsed_time = elapsedTime;
	}
}
