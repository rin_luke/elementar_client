package com.elementar.logic.network.response;

public class InfoMessage extends AInfo {

	public String message;

	public InfoMessage() {
	}

	public InfoMessage(String message) {
		this.message = message;
	}
}
