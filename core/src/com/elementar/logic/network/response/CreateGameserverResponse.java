package com.elementar.logic.network.response;

import com.elementar.logic.network.gameserver.MetaDataGameserver;

public class CreateGameserverResponse {
	public MetaDataGameserver metadata_gameserver;

	public CreateGameserverResponse() {
	}

	public CreateGameserverResponse(MetaDataGameserver metaDataGameServer) {
		this.metadata_gameserver = metaDataGameServer;
	}

}
