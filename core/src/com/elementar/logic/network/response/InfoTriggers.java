package com.elementar.logic.network.response;

import java.util.LinkedList;

import com.elementar.logic.emission.Trigger;

public class InfoTriggers extends AInfo {

	/**
	 * Holds trigger, created on server side, for decorative emissions that will
	 * be triggered on client side
	 */
	public LinkedList<Trigger> emission_triggers = new LinkedList<Trigger>();

	public InfoTriggers() {

	}

	public InfoTriggers(LinkedList<Trigger> triggers) {
		this.emission_triggers = triggers;
	}
}
