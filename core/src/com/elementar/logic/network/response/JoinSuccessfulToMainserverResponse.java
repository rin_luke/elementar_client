package com.elementar.logic.network.response;

public class JoinSuccessfulToMainserverResponse {

	public int connection_id;

	public JoinSuccessfulToMainserverResponse() {
	}

	/**
	 * @param connectionID
	 */
	public JoinSuccessfulToMainserverResponse(int connectionID) {
		this.connection_id = connectionID;
	}
}
