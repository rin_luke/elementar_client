package com.elementar.logic.network.response;

/**
 * @author lukassongajlo
 * 
 */
public abstract class AReliablePacket {

	public int sequence_id, ack, ack_bits, message_id;

	/**
	 * time offset since packet was sent. 
	 */
	public transient float time;

	public AReliablePacket() {

	}

	/**
	 * Creates a reliable packet with a specific message id. Unlike the other
	 * ids, the message id is bounded to data. The other ids build the signature
	 * of a packet. So if data has to be resent, the message id stays the same.
	 * 
	 * @param messageID
	 */
	public AReliablePacket(int messageID) {
		message_id = messageID;
	}

	@Override
	public String toString() {
		return "[" + sequence_id + "]";
	}
	
	public String superToString(){
		return super.toString();
	}

}
