package com.elementar.logic.network.response;

public class JoinSuccessfulToGameserverResponse {

	public int connection_id;
	public long map_seed;

	public String ip_address;
	public int match_id;

	public boolean is_host;
	public String gameserver_name;

	public JoinSuccessfulToGameserverResponse() {
	}

	/**
	 * Called after a join request from client to gameserver. gameserver answers
	 * with the {@link #map_seed}, so client starts loading the map, and some
	 * data for a potential reconnection like match id, ip address, connection
	 * id.
	 * 
	 * @param mapSeed
	 * @param ipAddress
	 * @param matchID
	 * @param connectionID
	 * @param isHost
	 */
	public JoinSuccessfulToGameserverResponse(String gameserverName, long mapSeed, String ipAddress, int matchID,
			int connectionID, boolean isHost) {
		this.gameserver_name = gameserverName;
		this.map_seed = mapSeed;
		this.ip_address = ipAddress;
		this.match_id = matchID;
		this.connection_id = connectionID;
		this.is_host = isHost;
	}

	/**
	 * called after a join request from client to mainserver. mainserver answers
	 * with the connectionID of the client, so the client knows its own ID.
	 * 
	 * @param connectionID
	 */
	public JoinSuccessfulToGameserverResponse(int connectionID) {
		this.connection_id = connectionID;
	}

}
