package com.elementar.logic.network.response;

public class InfoBuildingHealth extends AInfo {

	public short health_current;

	public InfoBuildingHealth() {
	}

	public InfoBuildingHealth(short healthCurrent) {
		this.health_current = healthCurrent;
	}

}
