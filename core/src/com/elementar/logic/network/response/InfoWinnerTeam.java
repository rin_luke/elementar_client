package com.elementar.logic.network.response;

import com.elementar.logic.util.Shared.Team;

public class InfoWinnerTeam extends AInfo {

	public Team winner_team;

	public InfoWinnerTeam() {
	}

	public InfoWinnerTeam(Team team) {
		this.winner_team = team;
	}

}
