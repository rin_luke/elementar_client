package com.elementar.logic.network.gameserver;

import com.elementar.logic.network.response.JoinSuccessfulToGameserverResponse;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

public class GameserverMainserverProtocol extends Listener {
	private boolean join_successful_mainserver = false;

	@Override
	public void received(Connection connection, Object object) {
		if (object instanceof JoinSuccessfulToGameserverResponse)
			join_successful_mainserver = true;
	}

	public boolean isJoinSuccessfulMainserver() {
		return join_successful_mainserver;
	}

}
