package com.elementar.logic.network.gameserver.picking;

import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.ServerPhysicalClient;
import com.elementar.logic.network.protocols.GameserverClientProtocol;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.Shared.ClientState;
import com.elementar.logic.util.maps.LimitedHashMap;

public abstract class APickingSystem {

	protected float gameserver_time;

	protected LimitedHashMap<Short, ServerPhysicalClient> player_map;

	public APickingSystem(LimitedHashMap<Short, ServerPhysicalClient> playerMap) {
		this.player_map = playerMap;
	}

	/**
	 * Determine who starts picking
	 * 
	 * @return {@link #getDuration()}
	 */
	public abstract float begin();

	/**
	 * Define what should happened when the time lapsed
	 * 
	 * @return <code>true</code> if the server should change its mode to
	 *         {@link GameserverState#PICKING_END}, otherwise <code>false</code>
	 */
	public abstract boolean ends();

	/**
	 * 
	 * @return picking duration in seconds
	 */
	protected abstract float getDuration();

	public float getGameserverTime() {
		return gameserver_time;
	}

	/**
	 * Assigns {@link GameclassName} if no character was chosen and resets
	 * {@link MetaClient#client_state} field to
	 * {@link ClientState#PICKING_GAMECLASS}
	 */
	protected void resetPlayers() {
		for (PhysicalClient client : player_map.values())
			if (client.getMetaClient().client_state == ClientState.PICKING_TEAM) {
				// assign random value
				if (client.getMetaClient().gameclass_name == null) {
					GameclassName gameclassName;
					do {
						gameclassName = Util.randomEnum(GameclassName.class);
					} while (GameserverClientProtocol.isGameclassTaken(player_map,
							gameclassName) == true);

					client.getMetaClient().setGameclass(gameclassName);
				}
				client.getMetaClient().client_state = ClientState.PICKING_GAMECLASS;
			}

	}

	public boolean haveAllPlayersPicked() {
		for (PhysicalClient client : player_map.values())
			if (client.getMetaClient().gameclass_name == null)
				return false;

		return true;

	}

	protected void startPicking(PhysicalClient gameClient) {
		gameserver_time = getDuration();
		gameClient.getMetaClient().client_state = ClientState.PICKING_TEAM;
	}

}
