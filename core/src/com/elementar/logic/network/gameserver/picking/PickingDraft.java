package com.elementar.logic.network.gameserver.picking;

import java.util.ArrayList;

import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.ServerPhysicalClient;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.Shared.Team;
import com.elementar.logic.util.maps.LimitedHashMap;

/**
 * The draft picking starts with one player of a randomly chosen team, who picks
 * the first game class. Afterwards two player of the next opposing team choose
 * a game class successively. Then the next two player... and so on.
 * 
 * @author lukassongajlo
 * 
 */
public class PickingDraft extends APickingSystem {

	private DraftPickingArrayList draft_picking_list;

	public PickingDraft(LimitedHashMap<Short, ServerPhysicalClient> playerMap) {
		super(playerMap);
	}

	@Override
	public float begin() {
		// which team starts?
		Team team = Util.randomEnum(Team.class);

		// note that when you debug alone the chosen team might be empty, just
		// restart
		draft_picking_list = new DraftPickingArrayList(team);
		PhysicalClient firstPickingClient = draft_picking_list
				.getNextPickingCandidate(team);
		startPicking(firstPickingClient);
		return getDuration();

	}

	@Override
	public boolean ends() {
		resetPlayers();

		// next or current pair?
		DraftPickingPair pair;
		if (draft_picking_list.current_picking_pair == null)
			// might be null at the beginning
			pair = draft_picking_list.getNextPair();
		else if (draft_picking_list.current_picking_pair.both_picked == true)
			pair = draft_picking_list.getNextPair();
		else if (draft_picking_list.current_picking_pair.first_picked == true
				&& draft_picking_list.current_picking_pair.player2 == null)
			pair = draft_picking_list.getNextPair();
		else
			pair = draft_picking_list.current_picking_pair;

		// all picked
		if (pair.player1 == null && pair.player2 == null) {

			return true;
		}

		if (pair.player1 != null && pair.first_picked == false) {
			startPicking(pair.player1);
			pair.first_picked = true;
		} else if (pair.player2 != null) {
			startPicking(pair.player2);
			pair.both_picked = true;
		}
		return false;
	}

	@Override
	protected float getDuration() {
		return 10;
	}

	private class DraftPickingArrayList extends ArrayList<PhysicalClient> {

		private DraftPickingPair current_picking_pair;
		private Team picking_team;

		/**
		 * 
		 * @param firstPickingTeam
		 */
		public DraftPickingArrayList(Team firstPickingTeam) {
			super();
			this.addAll(player_map.values());
			this.picking_team = firstPickingTeam;
		}

		/**
		 * Returns the next picking pair
		 * 
		 * @return
		 */
		public DraftPickingPair getNextPair() {
			current_picking_pair = new DraftPickingPair();
			// Define team of next pair
			picking_team = picking_team == Team.TEAM_1 ? Team.TEAM_2
					: Team.TEAM_1;

			current_picking_pair.player1 = getNextPickingCandidate(picking_team);
			current_picking_pair.player2 = getNextPickingCandidate(picking_team);
			return current_picking_pair;
		}

		protected PhysicalClient getNextPickingCandidate(Team team) {
			int minIndex = Shared.MAX_TEAM_CAPACITY;
			PhysicalClient pickingCandidate = null;
			MetaClient metaClient = null;
			for (PhysicalClient client : this) {
				metaClient = client.getMetaClient();
				if (metaClient.team == team)
					if (metaClient.slot_index < minIndex) {
						minIndex = metaClient.slot_index;
						pickingCandidate = client;
					}
			}
			if (pickingCandidate != null)
				remove(pickingCandidate);
			return pickingCandidate;

		}
	}

	private class DraftPickingPair {

		/**
		 * if this field is <code>true</code> the first player has picked and
		 * now the second one can continue
		 */
		private boolean first_picked = false;
		/**
		 * if this field is <code>true</code> both players has picked
		 */
		private boolean both_picked = false;
		private PhysicalClient player1, player2;

	}

}
