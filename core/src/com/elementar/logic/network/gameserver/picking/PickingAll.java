package com.elementar.logic.network.gameserver.picking;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.ServerPhysicalClient;
import com.elementar.logic.util.maps.LimitedHashMap;

public class PickingAll extends APickingSystem {

	public PickingAll(LimitedHashMap<Short, ServerPhysicalClient> playerMap) {
		super(playerMap);
	}

	@Override
	public float begin() {
		for (PhysicalClient client : player_map.values())
			startPicking(client);
		return getDuration();
	}

	@Override
	public boolean ends() {
		resetPlayers();

		return true;
	}

	@Override
	protected float getDuration() {
		return 120;
	}

}
