package com.elementar.logic.network.gameserver;

import static com.elementar.logic.util.Shared.GRAVITY;
import static com.elementar.logic.util.Shared.POSITION_ITERATIONS;
import static com.elementar.logic.util.Shared.SEND_AND_UPDATE_RATE_WORLD;
import static com.elementar.logic.util.Shared.SEND_RATE_META;
import static com.elementar.logic.util.Shared.VELOCITY_ITERATIONS;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.characters.ServerPhysicalClient;
import com.elementar.logic.characters.SparseClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.OmniCreep;
import com.elementar.logic.map.MapLoader;
import com.elementar.logic.map.buildings.ABuilding;
import com.elementar.logic.map.buildings.Tower;
import com.elementar.logic.network.connection.GameserverClientConnection;
import com.elementar.logic.network.connection.GameserverMainserverConnection;
import com.elementar.logic.network.gameserver.ConsoleEvent.Type;
import com.elementar.logic.network.protocols.GameserverClientProtocol;
import com.elementar.logic.network.requests.GameserverJoinRequest;
import com.elementar.logic.network.requests.PingRequest;
import com.elementar.logic.network.response.InfoDeathTime;
import com.elementar.logic.network.response.InfoEmissionInterruption;
import com.elementar.logic.network.response.InfoMessage;
import com.elementar.logic.network.response.InfoTriggers;
import com.elementar.logic.network.response.InfoWinnerTeam;
import com.elementar.logic.network.response.MetaDataResponse;
import com.elementar.logic.network.response.WorldDataResponse;
import com.elementar.logic.network.util.PingGameserverProtocol;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.Shared.ClientState;
import com.elementar.logic.util.Shared.Team;
import com.elementar.logic.util.maps.LimitedHashMap;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author lukassongajlo
 * 
 */
public class GameserverLogic {

	private final float PRE_MATCH_DURATION = 10f, POST_MATCH_DURATION = 9f;// 14f;

	private MetaDataGameserver metadata;

	private GameserverMainserverConnection server_mainserver_connection;

	private GameserverClientConnection server_client_connection; // server
	private GameserverClientProtocol server_client_protocol;

	private World world;

	public static LimitedHashMap<Short, ServerPhysicalClient> PLAYER_MAP;

	public static LimitedHashMap<Short, OmniCreep> CREEP_MAP;

	public static AtomicInteger TEAM1_REMAINING_LIVES, TEAM2_REMAINING_LIVES;

	public static Team WINNER_TEAM;

	private float time_meta_accumulator;
	private float time_world_accumulator;

	private float gameserver_time;

	/**
	 * To send world data over network
	 */
	private WorldDataResponse next_world_snapshot = new WorldDataResponse();
	/**
	 * To send meta data over network of players
	 */
	private MetaDataResponse next_meta_snapshot = new MetaDataResponse();

	// we need this field to set the skill and location of the towers
	private MapLoader map_loader;

	private ArrayList<ConsoleEvent> console_events = new ArrayList<>();

	private long new_time, current_time = 0;

	private ServerState server_state;

	public enum ServerState {
		PRE_MATCH, IN_MATCH, POST_MATCH
	}

	/**
	 * Creates a new {@link GameserverLogic} object. Within the constructor a new
	 * world with a given seed will be created.
	 * 
	 * @param metaDataGameServer
	 */
	public GameserverLogic(MetaDataGameserver metaDataGameServer) {

		addConsoleEvent(new ConsoleEvent(Type.SERVER_ONLINE));

		metadata = metaDataGameServer;

		// to mainserver
		server_mainserver_connection = new GameserverMainserverConnection();

		// server for clients
		server_client_connection = new GameserverClientConnection(this) {
			@Override
			public String toString() {
				return "SERVER";
			}
		};

		server_client_protocol = server_client_connection.getProtocol();

		PLAYER_MAP = server_client_protocol.getPlayerMap();
		// add a second listener to the server to handle ping requests
		server_client_connection.getServer().addListener(new PingGameserverProtocol(PLAYER_MAP));

		world = new World(new Vector2(0, GRAVITY), true);
		world.setContactListener(new ServerContactListener());

		map_loader = new MapLoader(Location.SERVERSIDE);
		map_loader.startLoadingOnce(world, metadata.seed);

		// send JoinRequest with meta data to the main server
		server_mainserver_connection.getClient().sendTCP(new GameserverJoinRequest(metadata));

		addConsoleEvent(new ConsoleEvent(Type.CREATE_MAP));

		while (map_loader.isCompleted() == false)
			Util.sleep(300);
		// ____________
		// Map loading finished

		addConsoleEvent(new ConsoleEvent(Type.MAP_LOADED));

		// set tickets
		TEAM1_REMAINING_LIVES = new AtomicInteger(Shared.INITIAL_REMAINING_LIVES);
		TEAM2_REMAINING_LIVES = new AtomicInteger(Shared.INITIAL_REMAINING_LIVES);

		changeServerState(ServerState.PRE_MATCH, PRE_MATCH_DURATION);

		current_time = System.currentTimeMillis();
	}

	public void update() {
		new_time = System.currentTimeMillis();
		float delta = (new_time - current_time) * .001f;
		current_time = new_time;

		// update world data
		time_world_accumulator += delta;
		while (time_world_accumulator >= SEND_AND_UPDATE_RATE_WORLD) {
			time_world_accumulator -= SEND_AND_UPDATE_RATE_WORLD;

			if (server_state == ServerState.IN_MATCH)
				gameserver_time += SEND_AND_UPDATE_RATE_WORLD;
			else
				/*
				 * in pre_ and post_match the time counter decreases
				 */
				gameserver_time -= SEND_AND_UPDATE_RATE_WORLD;

			// update
			synchronized (PLAYER_MAP) {

				switch (server_state) {
				case PRE_MATCH:
					if (gameserver_time < 0)
						changeServerState(ServerState.IN_MATCH, 0);
					break;
				case IN_MATCH:
					handleMatch();
					break;
				case POST_MATCH:
					if (gameserver_time < 0) {
						changeServerState(ServerState.PRE_MATCH, PRE_MATCH_DURATION);
						WINNER_TEAM = null;
					}
					break;

				}

				ArrayList<SparseClient> playerList = new ArrayList<SparseClient>();
				Iterator<ServerPhysicalClient> itPlayerMap = PLAYER_MAP.values().iterator();
				ServerPhysicalClient client;

				synchronized (server_client_connection.getServer().getConnections()) {
					while (itPlayerMap.hasNext() == true) {

						client = itPlayerMap.next();

						handleDisconnections(client, itPlayerMap);

						// fill player list for transmitting
						playerList.add(client.getSparseClient());
						fillInfoResponse(client);

					}
				}

				sendWorldDataToAll(playerList);
			}

		}

		// update meta data
		time_meta_accumulator += delta;
		while (time_meta_accumulator >= SEND_RATE_META) {

			// update
			synchronized (PLAYER_MAP) {
				sendMetaDataToAll();
			}

			time_meta_accumulator -= SEND_RATE_META;

		}

	}

	private ServerPhysicalClient handleDisconnections(ServerPhysicalClient client,
			Iterator<ServerPhysicalClient> itPlayerMap) {
		boolean disconnected = true;

		// TODO event based
		for (Connection connection : server_client_connection.getServer().getConnections())
			if (client.getMetaClient().connection_id == connection.getID())
				disconnected = false;

		/*
		 * if player has disconnected he'll get removed from the list
		 */
		if (disconnected == true) {
			
			LogicTier.deleteCharacter(client);
			LogicTier.deleteProjectiles(client);

			addConsoleEvent(new ConsoleEvent(Type.PLAYER_DISCONNECTED, client.getMetaClient()));
			itPlayerMap.remove();
		}
		return client;
	}

	private void sendWorldDataToAll(ArrayList<SparseClient> playerList) {

		// send to all via UDP
		for (ServerPhysicalClient client : PLAYER_MAP.values()) {

			short connectionID = client.getMetaClient().connection_id;

			// update reliability system
			client.getReliabilitySystem().update(server_client_connection, connectionID);

			// update player specific data
			next_world_snapshot = new WorldDataResponse();
			next_world_snapshot.player_list = playerList;
			next_world_snapshot.message_id = client.getLastProcessedInputID();
			next_world_snapshot.gameserver_time = gameserver_time;

			// send recent packet (world update)
			client.getReliabilitySystem().preparePacket(next_world_snapshot);

			// send world update via UDP
			server_client_connection.getServer().sendToUDP(connectionID, next_world_snapshot);

			if (Shared.DEBUG_MODE_SYSOS_NETCODE_SERVER == true)
				System.out.println("GameServerLogic.sendToAll()				SEND TO CLIENT (client id = "
						+ client.connection_id + ") BACK, with id = " + client.getLastProcessedInputID());
		}
		// reset after send
		Iterator<ServerPhysicalClient> it = PLAYER_MAP.values().iterator();
		ServerPhysicalClient client;
		while (it.hasNext() == true) {

			client = it.next();

			// emission interruptions
			client.redirected_inputs.clear();
			client.getEmissionInterruptions().clear();
			client.resetOutsidePhysics();
			client.getMetaClient().chat_message = "";

		}

		for (ABuilding building2 : map_loader.getMapManager().getBuildings().values()) {
			building2.getTriggers().clear();
			building2.updateLastHealth();
		}
	}

	private void sendMetaDataToAll() {

		// create meta snapshot

		ArrayList<MetaClient> list = makeClientsMeta();

		// send to all via UDP
		for (ServerPhysicalClient client : PLAYER_MAP.values()) {

			next_meta_snapshot = new MetaDataResponse(list, metadata.capacity, gameserver_time,
					TEAM1_REMAINING_LIVES.get(), TEAM2_REMAINING_LIVES.get(), server_state);

			short connectionID = client.getMetaClient().connection_id;

			/*
			 * packet aren't going to be tagged, because it's not necessary to send it again
			 * when lost
			 */
			server_client_connection.getServer().sendToUDP(connectionID, next_meta_snapshot);

			/*
			 * send packet to ascertain the ping
			 */
			server_client_connection.getServer().sendToUDP(connectionID, new PingRequest());
			client.setPingStart();
		}
	}

	/**
	 * a server state change comes along with a new time for the time counter.
	 * Instead of setting the {@link #server_state} variable directly use this
	 * method.
	 * 
	 * @param state
	 * @param newTime
	 */
	private void changeServerState(ServerState state, float newTime) {
		server_state = state;
		gameserver_time = newTime;
	}

	public World getWorld() {
		return world;
	}

	private ArrayList<MetaClient> makeClientsMeta() {

		ArrayList<MetaClient> resultList = new ArrayList<MetaClient>();

		for (ServerPhysicalClient client : PLAYER_MAP.values()) {
			client.getMetaClient().ping = client.getPing();
			resultList.add(client.getMetaClient());
		}
		return resultList;
	}

	/**
	 * For Lobby and Ingame environment
	 * 
	 * @param client
	 * @param infoList
	 */
	private void fillInfoResponse(ServerPhysicalClient client) {
		/*
		 * collect infos
		 */
		if (client.death_time_current > -3)
			// a bit under 0
			client.player_infos.add(new InfoDeathTime(client.death_time_current));

		if (client.getTriggers().size() > 0)
			client.player_infos.add(new InfoTriggers(client.getTriggers()));

		if (client.getMetaClient().chat_message.isEmpty() == false)
			client.player_infos.add(new InfoMessage(client.getMetaClient().chat_message));

		// animations
		client.player_infos.add(client.info_animations);

		// emission interruptions
		for (Iterator<InfoEmissionInterruption> it = client.getEmissionInterruptions().iterator(); it.hasNext();) {

			client.player_infos.add(it.next());

		}

		if (WINNER_TEAM != null)
			client.player_infos.add(new InfoWinnerTeam(WINNER_TEAM));
	}

	protected void handleMatch() {

		for (ServerPhysicalClient client : PLAYER_MAP.values()) {

			if (client.getMetaClient().client_state == ClientState.MATCH) {

				if (client.getHitbox() == null) {
					// meta data is set
					client.setHitbox(world);
					client.updateByMetaData(client.getHitbox(), null);

				}

				client.setTime(gameserver_time);
				client.update();

				if (Shared.DEBUG_MODE_SYSOS_NETCODE_SERVER == true)
					System.out.println("GameserverLogic.ingameLoop()				server-step, velocity before = "
							+ client.getHitbox().getBody().getLinearVelocity() + ", active keys = "
							+ client.getCurrentActions());
			}
		}

		for (Tower tower : map_loader.getMapManager().getTowers())
			tower.update();
		map_loader.getMapManager().getBaseLeft().update();
		map_loader.getMapManager().getBaseRight().update();

		world.step(SEND_AND_UPDATE_RATE_WORLD, VELOCITY_ITERATIONS, POSITION_ITERATIONS);

		for (ServerPhysicalClient client : PLAYER_MAP.values()) {
			if (client.getMetaClient().client_state == ClientState.MATCH) {

				if (client.getHitbox() != null)
					client.updateAfterStep();

				if (Shared.DEBUG_MODE_SYSOS_NETCODE_SERVER == true)
					System.out.println("GameServerLogic.ingameLoop()				server-step, resulting pos = "
							+ client.getHitbox().getPosition() + "   client-id = "
							+ client.getMetaClient().connection_id + ", resulting vel = "
							+ client.getHitbox().getBody().getLinearVelocity() + ", active keys = "
							+ client.getCurrentActions() + ", xy vel from physicalGameclient = " + client.vel);

			}
		}

		if (TEAM1_REMAINING_LIVES.get() <= 0) {
			WINNER_TEAM = Team.TEAM_1;
			changeServerState(ServerState.POST_MATCH, POST_MATCH_DURATION);

			resetMatch();

		} else if (TEAM2_REMAINING_LIVES.get() <= 0) {
			WINNER_TEAM = Team.TEAM_2;
			changeServerState(ServerState.POST_MATCH, POST_MATCH_DURATION);

			resetMatch();

		}

	}

	private void resetMatch() {

		TEAM1_REMAINING_LIVES = new AtomicInteger(Shared.INITIAL_REMAINING_LIVES);
		TEAM2_REMAINING_LIVES = new AtomicInteger(Shared.INITIAL_REMAINING_LIVES);

		for (Entry<Short, ServerPhysicalClient> entry : PLAYER_MAP.entrySet()) {
			ServerPhysicalClient oldServerPhysClient = entry.getValue();
			// remove old hitboxes
			if (oldServerPhysClient.getHitbox() != null)
				oldServerPhysClient.getHitbox().destroy();

			ServerPhysicalClient newServerPhysClient = new ServerPhysicalClient(
					new MetaClient(oldServerPhysClient.getMetaClient()));

			entry.setValue(newServerPhysClient);

		}
	}

	public float getTime() {
		return gameserver_time;
	}

	public GameserverClientConnection getGameserverClientConnection() {
		return server_client_connection;
	}

	public GameserverMainserverConnection getGameserverMainserverConnection() {
		return server_mainserver_connection;
	}

	public MetaDataGameserver getMetaDataGameserver() {
		return metadata;
	}

	public void addConsoleEvent(ConsoleEvent consoleEvent) {
		console_events.add(consoleEvent);
	}

	public ArrayList<ConsoleEvent> getConsoleEvents() {
		return console_events;
	}

}