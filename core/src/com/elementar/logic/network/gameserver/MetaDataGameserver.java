package com.elementar.logic.network.gameserver;

/**
 * This class holds the meta data of a gameserver. own connection id will just
 * be set on mainserver to check whether the gameserver is still in the
 * connections-pool, if the server isnt there it get kicked out from the pool.
 * hoster connection id: imagine the server sends commands regarding his
 * gameserver. With the hoster connection id the mainserver can make a
 * connection between hosted gameserver and the hoster.
 * 
 * 
 * NOTE: for future I think we need an additional field called boolen official
 * to fill the observer list with official servers
 * 
 * @author lukassongajlo
 * 
 */
public class MetaDataGameserver {
	public String ip, server_name, password;

	/**
	 * connection id will be set when server joins the mainserver
	 */
	public int connection_id;
	public int capacity;
	public boolean has_password;
	public long seed;

	/**
	 * will be calculated clientside
	 */
	public int ping;

	public int port_tcp_game, port_udp_game;

	public MetaDataGameserver() {

	}

	/**
	 * 
	 * @param ip
	 * @param portTCPGame
	 * @param portUDPGame
	 * @param serverName
	 * @param password
	 * @param size
	 * @param seed
	 */
	public MetaDataGameserver(String ip, int portTCPGame, int portUDPGame, String serverName,
			String password, int size, long seed) {
		this.ip = ip;
		this.port_tcp_game = portTCPGame;
		this.port_udp_game = portUDPGame;
		this.server_name = serverName;
		this.password = password;
		this.capacity = size;
		this.seed = seed;
	}

}
