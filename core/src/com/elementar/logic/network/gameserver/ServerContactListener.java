package com.elementar.logic.network.gameserver;

import static com.elementar.logic.util.Shared.CHARACTER_MAX_VELOCITY_X;

import java.util.Iterator;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.Shape;
import com.elementar.data.container.KeysConfiguration.WorldAction;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.creeps.PhysicalCreep;
import com.elementar.logic.emission.IEmitter;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.CollectableType;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.map.buildings.ABuilding;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.Shared.Team;
import com.elementar.logic.util.userdata.AContacts;
import com.elementar.logic.util.userdata.ASlideContacts;
import com.elementar.logic.util.userdata.CharacterLandingContacts;
import com.elementar.logic.util.userdata.CharacterMidTopContacts;
import com.elementar.logic.util.userdata.CharacterUndersideContacts;
import com.elementar.logic.util.userdata.SeparatorContacts;

public class ServerContactListener implements ContactListener {

	/**
	 * Called for scenarios where both involved fixtures interact with one another.
	 * 
	 * @param contact
	 * @param fixtureA
	 * @param fixtureB
	 * @return <code>false</code> if there is no combined szenario for both
	 *         fixtures, otherwise <code>true</code>
	 */
	protected boolean beginBoth(Contact contact, Fixture fixtureA, Fixture fixtureB) {
		if (fixtureA.getUserData() != null || fixtureB.getUserData() != null) {
			if (fixtureA.getUserData() instanceof Projectile) {
				if (fixtureB.getUserData() instanceof PhysicalCreep)
					handleProjectileCreepContact(contact, fixtureA, fixtureB);
				else if (fixtureB.getUserData() instanceof PhysicalClient)
					handleProjectilePlayerContact(contact, fixtureA, fixtureB);
				else if (fixtureB.getUserData() instanceof ABuilding)
					handleProjectileBuildingContact(contact, fixtureA, fixtureB);
				else if (fixtureA.getUserData() instanceof Projectile)
					handleProjectileProjectileContact(contact, fixtureA, fixtureB);
				else
					handleProjectileGroundContact(contact, fixtureA);

				return true;
			} else if (fixtureB.getUserData() instanceof Projectile) {
				if (fixtureA.getUserData() instanceof PhysicalCreep)
					handleProjectileCreepContact(contact, fixtureB, fixtureA);
				else if (fixtureA.getUserData() instanceof PhysicalClient)
					handleProjectilePlayerContact(contact, fixtureB, fixtureA);
				else if (fixtureA.getUserData() instanceof ABuilding)
					handleProjectileBuildingContact(contact, fixtureB, fixtureA);
				else if (fixtureA.getUserData() instanceof Projectile)
					handleProjectileProjectileContact(contact, fixtureA, fixtureB);
				else
					handleProjectileGroundContact(contact, fixtureB);

				return true;
			}

			if (fixtureA.getUserData() instanceof SeparatorContacts
					&& fixtureB.getUserData() instanceof SeparatorContacts) {

				handleSeparationContacts(fixtureA, fixtureB, true);
				return true;
			}

		}
		return false;

	}

	/**
	 * 
	 * 
	 * @param fixtureA
	 * @param fixtureB
	 * @param beginContacts, <code>true</code> if the contact begins,
	 *                       <code>false</code> if it ends
	 */
	private void handleSeparationContacts(Fixture fixtureA, Fixture fixtureB, boolean beginContacts) {

		SeparatorContacts c1 = (SeparatorContacts) fixtureA.getUserData(),
				c2 = (SeparatorContacts) fixtureB.getUserData();
		if (beginContacts == true) {
			c1.add(c2);
			c2.add(c1);
		} else {
			c1.remove(c2);
			c2.remove(c1);
		}

	}

	private void handleProjectileCreepContact(Contact contact, Fixture fixtureProjectile, Fixture fixtureCreep) {
		Projectile projectile = (Projectile) fixtureProjectile.getUserData();
		PhysicalCreep targetCreep = (PhysicalCreep) fixtureCreep.getUserData();

		if (projectile.isAlive() == true) {

			if (projectile.getEmission().getDef().oneTouchCheck(targetCreep) == true) {
				contact.setEnabled(false);
				return;
			}

			IEmitter emitter = projectile.getEmission().getEmitter();

			if (emitter instanceof PhysicalCreep)
				return;

			// set slayer
			targetCreep.setSlayer(projectile.getEmission().getEmitter());

			Iterator<AEmissionDef> it = projectile.getDef().getSuccessors().iterator();
			while (it.hasNext() == true) {
				AEmissionDef succDef = it.next();

				if (succDef.getStartConditions().isAfterPlayer() == true) {

					AEmissionDef activeSucc = succDef.makeCopy();

					// add new def to projectile
					projectile.getActiveSuccessors().add(activeSucc);

					// set data of new def
					activeSucc.setTarget(targetCreep);

					switch (succDef.getAlignment().getBehaviour()) {
					case OBSTACLE:
						activeSucc.getAlignment().setAngle((short) contact.getWorldManifold().getNormal().angle());
						break;
					case REFLECTION:
						activeSucc.getAlignment().setAngle((short) contact.getWorldManifold().getNormal().angle());
						// contact.getWorldManifold().getNormal().angle();
						break;
					default:
						break;
					}

					if (projectile.getDef().isFlyThroughTargets() == false) {
						/*
						 * if at least one successor has triggered, the projectile should disappear
						 */
						fixtureProjectile.getBody().setLinearVelocity(0, 0);

						projectile.setLifeTime(0);

					}

					/*
					 * the following if is interesting for a bridge between collision and an
					 * attached projectile
					 */
					if (succDef.hasPersecuteeAfterCollison() == true) {
						activeSucc.setPersecutee(targetCreep);
						activeSucc.setAbsorber(targetCreep, activeSucc.getDefProjectile().getLifeTime() / 1000f);
					}
				}

			}
		} else
			contact.setEnabled(false);

	}

	private void handleProjectilePlayerContact(Contact contact, Fixture fixtureProjectile, Fixture fixturePlayer) {

		Projectile projectile = (Projectile) fixtureProjectile.getUserData();

		PhysicalClient targetPlayer = (PhysicalClient) fixturePlayer.getUserData();

		if (projectile.getEmission().getEmitter() == targetPlayer
				&& projectile.getLifeTime() > projectile.getDef().getLifeTime()
						- projectile.getDef().getTimeUntilEmitterCollision()) {
			return;
		}
		/*
		 * projectile might be already dead, so check whether it is alive
		 */

		if (projectile.isAlive() == true) {

			if (targetPlayer == projectile.getAbsorber()) {

				if (projectile.getDef().getCollectableType() != null) {
					// collectable could be crystal or for combo
					if (projectile.getDef().getCollectableType() == CollectableType.CRYSTAL)
						targetPlayer.increaseCrystals();

					/*
					 * if at least one successor has triggered, the projectile should disappear
					 */
					projectile.getBody().setLinearVelocity(0, 0);
					projectile.setLifeTime(0);

					Iterator<AEmissionDef> it = projectile.getDef().getSuccessors().iterator();
					while (it.hasNext() == true) {
						AEmissionDef succDef = it.next();

						boolean isAfterAbsorption = succDef.getStartConditions().isAfterAbsorption();

						if (isAfterAbsorption == true) {

							AEmissionDef activeSucc = succDef.makeCopy();

							// add new def to projectile
							projectile.getActiveSuccessors().add(activeSucc);

							// set data of new def
							activeSucc.setTarget(targetPlayer);
							/*
							 * break because just one successor fits the start conditions
							 */
							break;
						}

					}
				}
			} else {

				if (projectile.getEmission().getDef().oneTouchCheck(targetPlayer) == true) {
					contact.setEnabled(false);
					return;
				}

				Team emitterTeam = projectile.getEmission().getEmitter().getTeam();

				// set slayer
				if (targetPlayer.getTeam() != emitterTeam)
					targetPlayer.setSlayer(projectile.getEmission().getEmitter());

				Iterator<AEmissionDef> it = projectile.getDef().getSuccessors().iterator();

				while (it.hasNext() == true) {

					AEmissionDef succDef = it.next();

					StartConditions startConditions = succDef.getStartConditions();

					boolean onEnemy = startConditions.isAfterCastOnEnemy() && targetPlayer.getTeam() != emitterTeam;
					boolean onAlly = targetPlayer.getTeam() == emitterTeam
							&& ((startConditions.isAfterCastOnAllyExcluded()
									&& targetPlayer != projectile.getEmission().getEmitter())
									|| startConditions.isAfterCastOnAllyIncluded());

					boolean onEmitter = startConditions.isAfterCastOnEmitter()
							&& targetPlayer == projectile.getEmission().getEmitter();

					if (succDef.getStartConditions().isAfterPlayer() == true && (onEnemy || onAlly || onEmitter)) {

						// made for void black hole
						if (succDef.isDestroyPrevProjectileAfterPlayer() == true)
							projectile.setLifeTime(0);

						AEmissionDef activeSucc = succDef.makeCopy();

						// add new def to projectile
						projectile.getActiveSuccessors().add(activeSucc);

						// set data of new def
						activeSucc.setTarget(targetPlayer);

						// set velocity of previous emissions when they're not zero.
						Vector2 prevVel = null;

						if (MathUtils.isZero(projectile.getVelocity().len()) == false)
							prevVel = projectile.getVelocity();
						else
							prevVel = projectile.getEmission().getDef().getPrevProjVel();

						activeSucc.setPrevProjVel(prevVel);

						switch (succDef.getAlignment().getBehaviour()) {
						case OBSTACLE:
							activeSucc.getAlignment().setAngle((short) contact.getWorldManifold().getNormal().angle());
							break;
						case REFLECTION:
							activeSucc.getAlignment().setAngle((short) contact.getWorldManifold().getNormal().angle());
							break;
						case STEERING:
							activeSucc.getAlignment().setAngle((short) (projectile.getAngleBeginning() + 180));
						default:
							break;
						}

						if (projectile.getDef().isFlyThroughTargets() == false) {
							/*
							 * if at least one successor has triggered, the projectile should disappear
							 */
							fixtureProjectile.getBody().setLinearVelocity(0, 0);

							projectile.setLifeTime(0);

						}

						/*
						 * the following if is interesting for a bridge between collision and an
						 * attached projectile
						 */
						if (succDef.hasPersecuteeAfterCollison() == true) {
							activeSucc.setPersecutee(targetPlayer);
							activeSucc.setAbsorber(targetPlayer, activeSucc.getDefProjectile().getLifeTime() / 1000f);
						}
					}

				}
			}
		} else
			contact.setEnabled(false);

	}

	private void handleProjectileBuildingContact(Contact contact, Fixture fixtureProjectile, Fixture fixtureBuilding) {
		// Projectile projectile = (Projectile) fixtureProjectile.getUserData();
		// // projectile might be already dead, so check whether it is alive
		//
		// if (projectile.isAlive() == true) {
		//
		// ABuilding targetBuilding = (ABuilding) fixtureBuilding.getUserData();
		//
		// if (projectile.getEmission().getDef().oneTouchCheck(targetBuilding)
		// == true) {
		// contact.setEnabled(false);
		// return;
		// }
		//
		// Team emitterTeam = projectile.getEmission().getEmitter().getTeam();
		// if (emitterTeam == targetBuilding.getTeam()) {
		// contact.setEnabled(false);
		// return;
		// }
		//
		// Iterator<AEmissionDef> it =
		// projectile.getDef().getSuccessors().iterator();
		//
		// boolean buildingAttackable, againstEnemy, againstMate;
		//
		// while (it.hasNext() == true) {
		// AEmissionDef succDef = it.next();
		// /*
		// * 1. wurde für dieses building schon eine emissionskopie für
		// * diese succ emission erstellt?
		// */
		// if (succDef.getStartConditions().isAfterBuilding() == true) {
		// buildingAttackable = targetBuilding.isAttackable();
		// againstEnemy = succDef.getStartConditions().isAfterCastOnEnemy()
		// && targetBuilding.getTeam() != emitterTeam;
		// againstMate = succDef.getStartConditions().isAfterCastOnMate()
		// && targetBuilding.getTeam() == emitterTeam;
		// if (((buildingAttackable == true
		// && succDef.getStartConditions().isAfterAttackable() == true)
		// || (buildingAttackable == false
		// && succDef.getStartConditions().isAfterNotAttackable() == true))
		// && (againstEnemy)) {
		//
		// // make copy
		// AEmissionDef activeSucc = succDef.makeCopy();
		//
		// // add new def to projectile
		// projectile.getActiveSuccessors().add(activeSucc);
		//
		// // set data of new def
		// activeSucc.setTarget(targetBuilding);
		//
		// switch (succDef.getAlignment().getBehaviour()) {
		// case OBSTACLE:
		// activeSucc.getAlignment().setAngle(
		// (short) contact.getWorldManifold().getNormal().angle());
		// break;
		// case REFLECTION:
		// activeSucc.getAlignment().setAngle(
		// (short) contact.getWorldManifold().getNormal().angle());
		// break;
		// default:
		// break;
		// }
		// fixtureProjectile.getBody().setLinearVelocity(0, 0);
		// projectile.setLifeTime(0);
		// }
		//
		// }
		// }
		//
		// } else
		// contact.setEnabled(false);
	}

	private void handleProjectileProjectileContact(Contact contact, Fixture fixtureA, Fixture fixtureB) {
		Projectile projGround = null;
		Projectile projectile = null;

		if ((fixtureA.getFilterData().categoryBits & CollisionData.BIT_PROJ_GROUND) != 0) {
			// fixtureA is projGround
			projGround = (Projectile) fixtureA.getUserData();
			projectile = (Projectile) fixtureB.getUserData();
		} else if ((fixtureB.getFilterData().categoryBits & CollisionData.BIT_PROJ_GROUND) != 0) {
			// fixtureB is projGround
			projGround = (Projectile) fixtureB.getUserData();
			projectile = (Projectile) fixtureA.getUserData();
		}

		if (projectile == null) {
			/*
			 * no projGround collision, just an ordinary collision between two projectile
			 * (e.g. water drops)
			 */
			projectile = (Projectile) fixtureA.getUserData();
			contact.setFriction(projectile.getDef().getFriction());

			if (projectile.getDef().isOneSidedCancellation() == true) {
				Projectile projectile2 = (Projectile) fixtureB.getUserData();

				contact.setEnabled(false);

				/*
				 * destroy the one with the greater life time
				 */
				if (projectile.getLifeTime() > 0 && projectile2.getLifeTime() > 0) {

					if (projectile.getLifeTime() > projectile2.getLifeTime())
						projectile.setLifeTime(0);
					else if (projectile.getLifeTime() < projectile2.getLifeTime())
						projectile2.setLifeTime(0);
				}
			}

		}

		if (projGround == null || projectile == null)
			return;

		/*
		 * if the projectile has the same team membership as the ground we make the
		 * projectile pervious
		 */
		Team teamGround = projGround.getEmission().getEmitter().getTeam();
		Team teamProjectile = projectile.getEmission().getEmitter().getTeam();
		if (teamGround == teamProjectile) {
			contact.setEnabled(false);
			return;
		}

		if (projectile.isAlive() == true) {

			if (projectile.getDef().isStopAfterGroundCollision()) {
				projectile.setGravityScale(0);
				projectile.getBody().setLinearVelocity(0, 0);
			}

			Iterator<AEmissionDef> it = projectile.getDef().getSuccessors().iterator();

			while (it.hasNext() == true) {
				AEmissionDef succDef = it.next();

				/*
				 * true is the default case. If you want to extend the life time after a
				 * projectile has collided with the ground use the set method of DefProjectile
				 */
				if (succDef.isDestroyPrevProjectileAfterGround() == true)
					projectile.setLifeTime(0);

				/*
				 * is successor allowed to start after projectile died via ground?
				 */
				if (succDef.getStartConditions().isAfterGround() == true) {

					AEmissionDef activeSucc = succDef.makeCopy();

					projectile.getActiveSuccessors().add(activeSucc);

					if (succDef.isRemoveFromSucclistAfterGroundCollision() == true)
						it.remove();

					switch (succDef.getAlignment().getBehaviour()) {
					case OBSTACLE:
						activeSucc.getAlignment().setAngle((short) contact.getWorldManifold().getNormal().angle());
						break;
					case REFLECTION:
						short incoming = (short) projectile.getAngleBeginning();
						incoming = (short) (((short) incoming < 0 ? 360 + incoming : incoming));
						short normal = (short) contact.getWorldManifold().getNormal().angle();
						short outgoing = (short) (2 * normal - (incoming + 180));
						activeSucc.getAlignment().setAngle(outgoing);
						break;
					case EMITTER:
						short angle = (short) (Util.getAngleBetweenTwoPoints(
								projectile.getEmission().getEmitter().getPos(), projectile.getPosition()) + 180);

						activeSucc.getAlignment().setAngle(angle);
						break;
					case STEERING:
						activeSucc.getAlignment().setAngle((short) (projectile.getAngleBeginning() + 180));
						break;
					default:
						break;
					}

					/*
					 * center of succDef will be set in Emission#update() (lower part)
					 */

				}
			}
		}
	}

	private void handleProjectileGroundContact(Contact contact, Fixture fixtureProjectile) {

		Projectile projectile = (Projectile) fixtureProjectile.getUserData();

		// set friction
		contact.setFriction(projectile.getDef().getFriction());

		if (projectile.isAlive() == true) {

			if (projectile.getDef().isStopAfterGroundCollision()) {
				projectile.setGravityScale(0);
				projectile.getBody().setLinearVelocity(0, 0);
			}

			Iterator<AEmissionDef> it = projectile.getDef().getSuccessors().iterator();

			while (it.hasNext() == true) {
				AEmissionDef succDef = it.next();

				/*
				 * true is the default case. If you want to extend the life time after a
				 * projectile has collided with the ground use the set method of DefProjectile
				 */
				if (succDef.isDestroyPrevProjectileAfterGround() == true)
					projectile.setLifeTime(0);

				/*
				 * is successor allowed to start after projectile died via ground?
				 */
				if (succDef.getStartConditions().isAfterGround() == true) {

					AEmissionDef activeSucc = succDef.makeCopy();

					projectile.getActiveSuccessors().add(activeSucc);

					if (succDef.isRemoveFromSucclistAfterGroundCollision() == true)
						it.remove();

					switch (succDef.getAlignment().getBehaviour()) {
					case OBSTACLE:
						activeSucc.getAlignment().setAngle((short) contact.getWorldManifold().getNormal().angle());
						break;
					case REFLECTION:
						short incoming = (short) projectile.getAngleBeginning();
						incoming = (short) (((short) incoming < 0 ? 360 + incoming : incoming));
						short normal = (short) contact.getWorldManifold().getNormal().angle();
						short outgoing = (short) (2 * normal - (incoming + 180));
						activeSucc.getAlignment().setAngle(outgoing);
						break;
					case EMITTER:
						short angle = (short) (Util.getAngleBetweenTwoPoints(
								projectile.getEmission().getEmitter().getPos(), projectile.getPosition()) + 180);

						activeSucc.getAlignment().setAngle(angle);
						break;
					case STEERING:
						activeSucc.getAlignment().setAngle((short) (projectile.getAngleBeginning() + 180));
						break;
					default:
						break;
					}

					/*
					 * center of succDef will be set in Emission#update() (lower part)
					 */

				}
			}
		}
	}

	/**
	 * Called for scenarios where just one fixture is sufficient to handle the
	 * contact. For example if a character-sensor touches the ground, we need only
	 * the sensor to modify some values, the ground stays the same.
	 * 
	 * @param contact
	 * @param fixture
	 * @param anglePlatform
	 */
	protected void beginContact(Contact contact, Fixture fixture, int anglePlatform) {

		if (fixture.getUserData() != null) {

			// setPlatformAngle has to be called before increment
			if (fixture.getUserData() instanceof ASlideContacts)
				((ASlideContacts) fixture.getUserData()).setPlatformAngle(anglePlatform);

			if (fixture.getUserData() instanceof AContacts) {
				((AContacts) fixture.getUserData()).incrementNumContacts();

				if (fixture.getUserData() instanceof CharacterMidTopContacts)
					((CharacterMidTopContacts) fixture.getUserData()).getJumping().jump_active = false;
				else if (fixture.getUserData() instanceof CharacterLandingContacts)
					((CharacterLandingContacts) fixture.getUserData()).setVelY(fixture.getBody().getLinearVelocity().y);

			}

			if (fixture.getUserData() instanceof CharacterUndersideContacts) {
				CharacterUndersideContacts underside;
				underside = (CharacterUndersideContacts) fixture.getUserData();
				PhysicalClient physClient = underside.getPhysicalClient();
				/*
				 * that velocity amount will be split into a x- and y component. Might be
				 * negative.
				 */
				Vector2 vel = fixture.getBody().getLinearVelocity();

				boolean moveLeft = physClient.isCurrentActionActive(WorldAction.MOVE_LEFT),
						moveRight = physClient.isCurrentActionActive(WorldAction.MOVE_RIGHT);
				boolean left = false;

				Body body = fixture.getBody();
				if (underside.isJumping() == false) {
					if (PhysicalClient.isSlidableByAngle(anglePlatform) == false) {
						// deactivate gravity when walking on walkable platforms
						body.setGravityScale(0);
						if (moveLeft == false && moveRight == true)
							left = false;
						else if (moveRight == false && moveLeft == true)
							left = true;
						else if (moveRight == true && moveLeft == true)
							if (physClient.isLeftDominant() == true)
								left = true;
							else
								left = false;
						else {
							physClient.setBrakeEnabled(true);
							physClient.applyLinearImpulse(new Vector2(0, -vel.y));

							return;
						}

						float velY = 0;
						float velX = 0;
						if (left == true)
							velX = Math.max(vel.x, -CHARACTER_MAX_VELOCITY_X * underside.getVelocityMovementFactor());
						else
							velX = Math.min(vel.x, CHARACTER_MAX_VELOCITY_X * underside.getVelocityMovementFactor());

						velY = PhysicalClient.getCorrespondingVelY(velX, anglePlatform);
						physClient.applyLinearImpulse(new Vector2(velX - vel.x, velY - vel.y));
					}
				}
			}

		}
	}

	protected void preSolve(Fixture fixture, int anglePlatform) {

		if (fixture.getUserData() != null) {

			if (fixture.getUserData() instanceof CharacterUndersideContacts) {
				CharacterUndersideContacts underside;
				underside = (CharacterUndersideContacts) fixture.getUserData();

				// It's possible that underside dedects more than
				// just one angle!
				if (underside.isAlternating(anglePlatform)) {
					if (Math.abs(anglePlatform - 90) >= Math.abs(underside.getPlatformAngle() - 90))
						underside.setAnglePlatform(anglePlatform);
				} else {
					underside.setAnglePlatform(anglePlatform);
				}
				underside.setPrevAnalyticalAnglePlatform(underside.getPrevAnglePlatform());
				underside.setCurrentAnalyticalAnglePlatform(anglePlatform);

				if (PhysicalClient.isSlidableByAngle(anglePlatform) == false) {
					// deactivate gravity when walking on walkable platforms
					fixture.getBody().setGravityScale(0);

					boolean moveLeft = underside.getPhysicalClient().isCurrentActionActive(WorldAction.MOVE_LEFT),
							moveRight = underside.getPhysicalClient().isCurrentActionActive(WorldAction.MOVE_RIGHT);
					if (moveLeft == false && moveRight == false && underside.isJumping() == false) {
						Vector2 vel = fixture.getBody().getLinearVelocity();
						// x is handled in PhysicalGameclient
						underside.getPhysicalClient().applyLinearImpulse(new Vector2(0, -vel.y));
					}
				}
			}

		}
	}

	// private void postSolve(Fixture fixture) {
	// if (fixture.getUserData() instanceof HeadContacts) {
	// HeadContacts headContacts = (HeadContacts) fixture.getUserData();
	// fixture.getBody().setLinearVelocity(headContacts.getVelocityX(),
	// fixture.getBody().getLinearVelocity().y);
	//
	// }
	//
	// }

	protected void endContact(Fixture fixture, int anglePlatform) {

		if (fixture.getUserData() != null) {

			// setPlatformAngle has to be called before decrement
			if (fixture.getUserData() instanceof ASlideContacts)
				((ASlideContacts) fixture.getUserData()).setPlatformAngle(anglePlatform);

			if (fixture.getUserData() instanceof AContacts)
				((AContacts) fixture.getUserData()).decrementNumContacts();

			if (fixture.getUserData() instanceof CharacterUndersideContacts) {
				CharacterUndersideContacts underside;
				underside = (CharacterUndersideContacts) fixture.getUserData();
				fixture.getBody().setGravityScale(1f);
				/*
				 * avoid lift-off: if player hasn't contact anymore the y velocity will be set
				 * to 0, so the player doesn't lift off
				 */
				if (underside.getNumContacts() == 0 && fixture.getBody().getLinearVelocity().y >= 0
						&& underside.isJumping() == false)
					// set vel y = 0
					fixture.getBody().setLinearVelocity(fixture.getBody().getLinearVelocity().x, 0);
			}
		}
	}

	@Override
	public void beginContact(Contact contact) {
		Fixture fa = contact.getFixtureA();
		Fixture fb = contact.getFixtureB();

		if (fa == null || fb == null)
			return;

		int anglePlatform = 0;
		if (fa.getShape() instanceof ChainShape)
			anglePlatform = getPlatformAngle(fa.getShape(), contact);
		else if (fb.getShape() instanceof ChainShape)
			anglePlatform = getPlatformAngle(fb.getShape(), contact);

		/*
		 * platform manager iterates clockwise, but calculated angle is
		 * counter-clockwise, correction:
		 */
		anglePlatform += 180;

		anglePlatform = Util.minimizePlatformAngle(anglePlatform);

		if (beginBoth(contact, fa, fb) == false) {
			beginContact(contact, fa, anglePlatform);
			beginContact(contact, fb, anglePlatform);
		}

	}

	/**
	 * Calculates the platform angle by shape and contact (also between sensors!).
	 * 
	 * @param shape
	 * @param contact
	 * @return
	 */
	private int getPlatformAngle(Shape shape, Contact contact) {
		ChainShape chainShape = (ChainShape) shape;
		Vector2 platformBegin = new Vector2(), platformEnd = new Vector2();
		chainShape.getVertex(contact.getChildIndexA(), platformBegin);
		chainShape.getVertex(
				contact.getChildIndexA() + 1 == chainShape.getChildCount() ? 0 : contact.getChildIndexA() + 1,
				platformEnd);

		return Math.round(platformBegin.sub(platformEnd).angleDeg());
	}

	/**
	 * BOX2D DOESN'T CALL THIS FOR SENSORS
	 */
	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		Fixture fa = contact.getFixtureA();
		Fixture fb = contact.getFixtureB();

		if (fa == null || fb == null)
			return;
		// Anders als bei beginContact() kann ich hier diese Methode der
		// Winkelberechnung anwenden, da in preSolve nur feste Körper und keine
		// Sensoren mehr gefragt sind und die Winkelberechnung somit korrekt
		// abläuft!
		int anglePlatform = Math.round(contact.getWorldManifold().getNormal().angleDeg()) + 90;

		/*
		 * platform manager iterates clockwise, but calculated angle is
		 * counter-clockwise, correction:
		 */
		anglePlatform += 180;

		// anglePlatform might be over 360, so it has to be minimized
		anglePlatform = Util.minimizePlatformAngle(anglePlatform);

		preSolve(fa, anglePlatform);
		preSolve(fb, anglePlatform);

		disableContactBuildingProjectile(contact, fa, fb);
		disableContactBuildingProjectile(contact, fb, fa);

		disableContactProjGroundProjectile(contact, fa, fb);
	}

	private void disableContactProjGroundProjectile(Contact contact, Fixture fixtureA, Fixture fixtureB) {
		if (fixtureA.getUserData() instanceof Projectile && fixtureB.getUserData() instanceof Projectile) {

			Projectile projGround = null;
			Projectile projectile = null;

			if ((fixtureA.getFilterData().categoryBits & CollisionData.BIT_PROJ_GROUND) != 0) {
				// fixtureA is projGround
				projGround = (Projectile) fixtureA.getUserData();
				projectile = (Projectile) fixtureB.getUserData();
			} else if ((fixtureB.getFilterData().categoryBits & CollisionData.BIT_PROJ_GROUND) != 0) {
				// fixtureB is projGround
				projGround = (Projectile) fixtureB.getUserData();
				projectile = (Projectile) fixtureA.getUserData();
			}

			if (projGround == null || projectile == null)
				return;

			/*
			 * if the projectile has the same team membership as the ground we make the
			 * projectile pervious
			 */
			Team teamGround = projGround.getEmission().getEmitter().getTeam();
			Team teamProjectile = projectile.getEmission().getEmitter().getTeam();
			if (teamGround == teamProjectile) {
				contact.setEnabled(false);
				return;
			}

		}
	}

	private void disableContactBuildingProjectile(Contact contact, Fixture fa, Fixture fb) {
		if (fa.getUserData() instanceof Projectile && fb.getUserData() instanceof ABuilding) {
			Projectile projectile = (Projectile) fa.getUserData();

			// projectile might be already dead, so check whether it is alive
			if (projectile.isAlive() == true) {

				ABuilding targetBuilding = (ABuilding) fb.getUserData();
				Team emitterTeam = projectile.getEmission().getEmitter().getTeam();
				if (emitterTeam == targetBuilding.getTeam()) {
					contact.setEnabled(false);
					return;
				}

			}
		}
	}

	@Override
	public void endContact(Contact contact) {
		Fixture fa = contact.getFixtureA();
		Fixture fb = contact.getFixtureB();

		if (fa == null || fb == null)
			return;

		int anglePlatform = 0;
		if (fa.getShape() instanceof ChainShape)
			anglePlatform = getPlatformAngle(fa.getShape(), contact);
		else if (fb.getShape() instanceof ChainShape)
			anglePlatform = getPlatformAngle(fb.getShape(), contact);

		/*
		 * platform manager iterates clockwise, but calculated angle is
		 * counter-clockwise, correction:
		 */
		anglePlatform += 180;

		anglePlatform = Util.minimizePlatformAngle(anglePlatform);

		if (endBoth(contact, fa, fb) == false) {
			endContact(fa, anglePlatform);
			endContact(fb, anglePlatform);
		}
	}

	private boolean endBoth(Contact contact, Fixture fixtureA, Fixture fixtureB) {

//		if (fixtureA.getUserData() != null || fixtureB.getUserData() != null) {
//			if (fixtureA.getUserData() instanceof Projectile) {
//				if (fixtureB.getUserData() instanceof PhysicalCreep) {
//
//					// end projectile creep contact
//					System.out.println("ServerContactListener.endBoth()			test creep end	A");
////					handleProjectileCreepContact(contact, fixtureA, fixtureB);
//
//				} else if (fixtureB.getUserData() instanceof PhysicalClient) {
//
//					// end projectile player contact
//					System.out.println("ServerContactListener.endBoth()			test player end  A");
//					handleProjectilePlayerContactEnd(contact, fixtureA, fixtureB);
////					handleProjectilePlayerContact(contact, fixtureA, fixtureB);
//
//				}
//
//				return true;
//
//			} else if (fixtureB.getUserData() instanceof Projectile) {
//				if (fixtureA.getUserData() instanceof PhysicalCreep) {
//					// end projectile creep contact
//					System.out.println("ServerContactListener.endBoth()			test creep end  B");
//
////					handleProjectileCreepContact(contact, fixtureB, fixtureA);
//
//				} else if (fixtureA.getUserData() instanceof PhysicalClient) {
//
//					// end projectile player contact
//					System.out.println("ServerContactListener.endBoth()			test player end  B");
//					handleProjectilePlayerContactEnd(contact, fixtureB, fixtureA);
////					handleProjectilePlayerContact(contact, fixtureB, fixtureA);
//				}
//
//				return true;
//			}

		if (fixtureA.getUserData() instanceof SeparatorContacts
				&& fixtureB.getUserData() instanceof SeparatorContacts) {
			handleSeparationContacts(fixtureA, fixtureB, false);
			return true;
		}
//		}
		return false;

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// Fixture fa = contact.getFixtureA();
		// Fixture fb = contact.getFixtureB();
		//
		// if (fa == null || fb == null)
		// return;
		//
		// postSolve(fa);
		// postSolve(fb);

	}
}