package com.elementar.logic.network.gameserver;

import com.elementar.data.container.TextData;
import com.elementar.logic.characters.MetaClient;

public class ConsoleEvent {

	private Type type;
	private MetaClient client;

	/**
	 * client-related events
	 * 
	 * @param type
	 * @param client
	 */
	public ConsoleEvent(Type type, MetaClient client) {
		this(type);
		this.client = client;
	}

	/**
	 * More general or System-related events
	 * 
	 * @param type
	 */
	public ConsoleEvent(Type type) {
		this.type = type;
	}

	public String getText() {

		String text = "";

		switch (type) {
		case PLAYER_CONNECTED:
			text = client.name + " " + TextData.getWord("connected");
			break;
		case PLAYER_DISCONNECTED:
			text = client.name + " " + TextData.getWord("disconnected");
			break;
		case TEAM_SELECTED:
			text = client.name + " " + TextData.getWord("selected") + " " + client.team;
			break;
		case GAMECLASS_SELECTED:
			text = client.name + " " + TextData.getWord("selected") + " " + client.gameclass_name;
			break;
		case CREATE_MAP:
			return TextData.getWord("createMap") + "...";
		case MAP_LOADED:
			return "..." + TextData.getWord("mapLoaded");
		case SERVER_ONLINE:
			text = TextData.getWord("serverOnline");
		}

		// add a dot
		text += ".";
		return text;
	}

	public static enum Type {
		GAMECLASS_SELECTED, TEAM_SELECTED, PLAYER_CONNECTED, PLAYER_DISCONNECTED, SERVER_ONLINE, MAP_LOADED, CREATE_MAP
	}

}
