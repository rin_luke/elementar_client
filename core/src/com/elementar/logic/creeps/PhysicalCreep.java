package com.elementar.logic.creeps;

import static com.elementar.logic.util.Shared.SEND_AND_UPDATE_RATE_WORLD;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.elementar.gui.modules.roots.WorldOnlineRoot;
import com.elementar.gui.modules.roots.WorldTrainingRoot;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.ServerPhysicalClient;
import com.elementar.logic.characters.SparseClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.characters.skills.ComboStorage;
import com.elementar.logic.creeps.skills.ACreepSkill;
import com.elementar.logic.creeps.skills.CreepDyingSkill;
import com.elementar.logic.creeps.skills.CreepSkill0;
import com.elementar.logic.emission.EmissionList;
import com.elementar.logic.emission.IEmitter;
import com.elementar.logic.emission.SparseProjectile;
import com.elementar.logic.emission.Trigger;
import com.elementar.logic.emission.effect.AEffectTimed;
import com.elementar.logic.network.gameserver.GameserverLogic;
import com.elementar.logic.util.CreepAndTowerCastCallback;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.SparseObject;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.ViewCircle;
import com.elementar.logic.util.Shared.Team;
import com.elementar.logic.util.userdata.SeparatorContacts;

//Note: Es wird dafür gesorgt, dass das Feld 'character' nicht null ist. Daher entfallen entsprechende null checks
public class PhysicalCreep extends ACreep implements IEmitter {

	private static final float HEALTH_REGEN_BATTLE = 0f;

	private static final float HEALTH_REGEN_OUTSIDE_BATTLE = 0.7f;

	private static final float MAX_WALKED_DISTANCE = 2.5f;

	private static final float TIME_AFTER_DEATH_TIL_REMOVE = 5f;

	private static final float CREEP_AGGRO_RADIUS = 0.3f;
	private static final float CREEP_RANGE_RADIUS = 2f;

	private static final short HEALTH = 100;

	private static final float AIR_DRAG_DEFAULT = 0.87f;

	private static final float INERTIA = Shared.CREEP_MAX_VELOCITY / 10f;

	private ComboStorage combo_container = new ComboStorage(0);

	private Location location;

	private PhysicalHitboxCreep physical_hitbox;

	private float health_regen, health_accu;

	private ViewCircle view_circle, aggro_circle, range_circle;

	private float cast_time;
	private float elapsed_time = 0;

	/**
	 * local time or gameserver time, depends on {@link #location}
	 */
	private float time;

	private EmissionList emission_list;

	private boolean prev_alive = true;

	private boolean stunned = false, rooted = false, disarmed = false;

	private SparseCreep sparse_creep;

	/**
	 * Holds triggers created on server side to create emissions at client side
	 */
	private LinkedList<Trigger> triggers = new LinkedList<Trigger>();

	/**
	 * <code>false</code> if the player isn't in one of the enemies view circles,
	 * otherwise <code>true</code>
	 */
	private boolean visible = true;

	/**
	 * holds all active effects that aren't one time effects. Timed effects have an
	 * apply-tickrate.
	 */
	private ArrayList<AEffectTimed> effects_timed;

	private IEmitter current_target;

	/**
	 * this target is relevant when {@link #current_target} is <code>null</code>.
	 * Determines whether the fight stance is active or not
	 */
	private IEmitter current_potential_target;

	private ACreepSkill current_skill;

	private Comparator comparator;

	private IEmitter slayer;

	private Vector2 origin = new Vector2();

	/**
	 * 
	 * @param metaCreep
	 * @param paths
	 * @param loc
	 */
	public PhysicalCreep(MetaCreep metaCreep, CreepCategory creepCategory, Location loc) {
		super(metaCreep, creepCategory);

		emission_list = new EmissionList(this);
		location = loc;
		sparse_creep = new SparseCreep();

		comparator = new Comparator();

		/*
		 * radius = 2, note that the fog dispel has an additional radius offset
		 */
		view_circle = new ViewCircle(pos, 2);
		aggro_circle = new ViewCircle(pos, CREEP_AGGRO_RADIUS);
		range_circle = new ViewCircle(pos, CREEP_RANGE_RADIUS);
		effects_timed = new ArrayList<>();
	}

	@Override
	public void update() {

		elapsed_time += Shared.SEND_AND_UPDATE_RATE_WORLD;

		// sort and update effects caused by emissions
		Collections.sort(effects_timed);
		Iterator<AEffectTimed> it = effects_timed.iterator();
		while (it.hasNext() == true)
			it.next().update(it);

		if (isAlive() == true) {

			float diffDistance = pos.dst(origin);

			// collect all players that are inside RADIUS_ATTACK?

			/*
			 * skill ging in hasFinished rein, dadurch sollte er fertig sein und möglichst
			 * bald den nächsten skill casten
			 */
			if (current_skill == null && skill_0.getCooldownCurrent() < 0) {
				// find new targets
				ArrayList<IEmitter> targets = new ArrayList<>();
				ArrayList<IEmitter> potentialTargets = new ArrayList<>();

				if (location == Location.SERVERSIDE) {
					for (ServerPhysicalClient client : GameserverLogic.PLAYER_MAP.values()) {
						if (Util.circleContains(aggro_circle, client.getHitbox().getPosition()) == true)
							targets.add(client);
					}

				} else if (location == Location.CLIENTSIDE_TRAINING) {

					for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {
						if (client.getPhysicalClient().isAlive() == true) {
							Vector2 pos = client.getPhysicalClient().getHitbox().getPosition();

							if (Util.circleContains(aggro_circle, pos) == true)
								targets.add(client.getPhysicalClient());
							if (Util.circleContains(range_circle, pos) == true)
								potentialTargets.add(client.getPhysicalClient());

						}
					}

				}

				Collections.sort(targets, comparator);

				// first, test whether current target is still in range
				if (current_target != null) {

					health_regen = calcHealthRegen(HEALTH_REGEN_BATTLE);

					/*
					 * did the creep try to catch the current target enough?
					 */
					if (diffDistance >= MAX_WALKED_DISTANCE) {

						if (Util.circleContains(range_circle, current_target.getPos()) == true) {
							/*
							 * target is still in range, now do an attack and if something is between creep
							 * and target?
							 */
							if (castRay(current_target, getPos()) == false)
								current_target = null;

						} else
							/*
							 * creep tried enough and target is out of range
							 */
							current_target = null;
					} else if (Util.circleContains(range_circle, current_target.getPos()) == true)
						castRay(current_target, getPos());
				}

				if (current_target == null) {
					for (IEmitter target : targets)
						// do an attack and set current_target
						castRay(target, getPos());

					if (potentialTargets.size() > 0) {

						// there are potential targets
						Collections.sort(potentialTargets, comparator);

						// check gradient change
						boolean steppedIntoFightZone = current_potential_target == null && potentialTargets.size() > 0;

						current_potential_target = potentialTargets.get(0);

						if (steppedIntoFightZone == true && current_potential_target.isAlive() == true)
							info_animations.track_1 = AnimationName.CREEP_FIGHT_STANCE_ON;
					} else {
						/*
						 * there are no potential targets and no current target
						 */

						health_regen = calcHealthRegen(HEALTH_REGEN_OUTSIDE_BATTLE);

						current_potential_target = null;
						if (isAtOrigin() == true)
							info_animations.track_1 = AnimationName.CREEP_FIGHT_STANCE_OFF;
					}

				}
			}

			if (hasFinishedCurrentSkill() == true) {
				/*
				 * assign P3 when queued skill is null and the current skill has finished its
				 * animation
				 */
				info_animations.track_1 = AnimationName.P3;
				current_skill = null;
			}

			// update regen of health
			health_accu += health_regen;
			while (health_accu >= 1) {
				health_current += 1;
				if (health_current >= health_absolute)
					health_current = health_absolute;
				health_accu -= 1;
			}

			physical_hitbox.getBody().setLinearDamping(AIR_DRAG_DEFAULT);

			/*
			 * walk is omni-present and overwrites the stunned animation after stunned has
			 * finished
			 */
			info_animations.track_0 = AnimationName.WALK;

			walkingRoutine(diffDistance);

			if (physical_hitbox.getBody().getLinearVelocity().isZero(0.2f) == true)
				info_animations.track_0 = AnimationName.IDLE;

			// override if player is stunned
			if (stunned == true)
				info_animations.track_0 = AnimationName.STUNNED;

			// stands the creep in one another?
			if (MathUtils.isZero(vel.len2()) == true)
				for (SparseObject otherClient : physical_hitbox.separator_contacts.getListClients())
					if (MathUtils.isZero(otherClient.vel.len2()) == true) {
						// add random number to avoid the "temp = 0" -case
						float temp = pos.x - otherClient.pos.x + MathUtils.random(-0.001f, 0.001f);
						temp = 2 * SeparatorContacts.SEPARATOR_RADIUS * (temp < 0 ? -1 : 1) - temp;
						applyLinearImpulse(new Vector2(temp * 1.5f, 0f));
					}
			for (SparseObject otherCreep : physical_hitbox.separator_contacts.getListCreeps()) {
				// add random number to avoid the "temp = 0" -case
				float tempY = pos.y - otherCreep.pos.y + MathUtils.random(-0.001f, 0.001f);
				tempY = 2 * SeparatorContacts.SEPARATOR_RADIUS * (tempY < 0 ? -1 : 1) - tempY;

				float tempX = pos.x - otherCreep.pos.x + MathUtils.random(-0.001f, 0.001f);
				tempX = 2 * SeparatorContacts.SEPARATOR_RADIUS * (tempX < 0 ? -1 : 1) - tempX;

				applyLinearImpulse(new Vector2(tempX * 1.5f, tempY * 1.5f));
			}

		}

		skill_0.update();

		dying_skill.update();

	}

	private void walkingRoutine(float diffDistance) {
		if (current_target != null) {
			if (diffDistance < MAX_WALKED_DISTANCE
					&& Util.circleContains(range_circle, current_target.getPos()) == false)
				// walk to target
				applyWalkImpulse(current_target.getPos());
			else
				// velocity = 0
				applyLinearImpulse(new Vector2(-physical_hitbox.getBody().getLinearVelocity().x,
						-physical_hitbox.getBody().getLinearVelocity().y));

		} else {

			// creep has no current target
			if (MathUtils.isZero(diffDistance, 0.2f) == false)
				// walk back to origin
				applyWalkImpulse(origin);
			else
				// velocity = 0
				applyLinearImpulse(new Vector2(-physical_hitbox.getBody().getLinearVelocity().x,
						-physical_hitbox.getBody().getLinearVelocity().y));

		}

		if (rooted == true || stunned == true)
			// velocity = 0
			applyLinearImpulse(new Vector2(-physical_hitbox.getBody().getLinearVelocity().x,
					-physical_hitbox.getBody().getLinearVelocity().y));

	}

	private void applyWalkImpulse(Vector2 targetPos) {
		/*
		 * desiredVelX will be split into a x- and y component, when walking on an
		 * inclined. Might be negative. Usage: first we determine the desiredVelX,
		 * including inertia. Then we look how much we are away from the current
		 * velocity x, the difference will be applied with an impulse. When you apply
		 * the whole negative velocity (new Vector2(-vel.x, -vel.y)) you remove the
		 * velocity completely. When you apply 0 you say, that the current velocity
		 * should be maintained.
		 */
		float desiredVelX = 0;
		/*
		 * y component of desired velocity
		 */
		float desiredVelY = 0;

		// last veloctiy data
		float currentVelX = physical_hitbox.getBody().getLinearVelocity().x,
				currentVelY = physical_hitbox.getBody().getLinearVelocity().y;

		float velMovementFactor = 1f;// getGameclass().getVelXFactor();

		boolean left = targetPos.x - getPos().x < 0;
		boolean bottom = targetPos.y - getPos().y < 0;
		// step by step increasing for inertia/acceleration
		if (left == true)
			desiredVelX = Math.max(currentVelX - INERTIA * velMovementFactor,
					-Shared.CREEP_MAX_VELOCITY * velMovementFactor);
		else
			desiredVelX = Math.min(currentVelX + INERTIA * velMovementFactor,
					Shared.CREEP_MAX_VELOCITY * velMovementFactor);

		if (bottom == true)
			desiredVelY = Math.max(currentVelY - INERTIA * velMovementFactor,
					-Shared.CREEP_MAX_VELOCITY * velMovementFactor);
		else
			desiredVelY = Math.min(currentVelY + INERTIA * velMovementFactor,
					Shared.CREEP_MAX_VELOCITY * velMovementFactor);

		applyLinearImpulse(new Vector2(desiredVelX - currentVelX, desiredVelY - currentVelY));
	}

	/**
	 * Returns <code>false</code> if the casted ray don't find a player- or
	 * creep-fixture
	 * 
	 * @param targetEmitter
	 * @param originPos     , this creep's position
	 * @return
	 */
	private boolean castRay(IEmitter targetEmitter, Vector2 originPos) {

		if (targetEmitter.isAlive() == true) {
			if (Util.isNan(targetEmitter.getPos()) == false)
				if (originPos.dst2(targetEmitter.getPos()) < 0.01f) {
					// no ray-cast necessary
					current_target = targetEmitter;
					skill_0.setPersecutee(targetEmitter);
					skill_0.init();
				} else {

					CreepAndTowerCastCallback callback = new CreepAndTowerCastCallback(null);

					physical_hitbox.getWorld().rayCast(callback, originPos, targetEmitter.getPos());

					if (callback.getFixtureTarget() != null
							&& targetEmitter == callback.getFixtureTarget().getUserData()) {
						current_target = targetEmitter;
						skill_0.setPersecutee(targetEmitter);
						skill_0.init();
						return true;
					}
				}
		}
		return false;
	}

	private float calcHealthRegen(float healthRegenPercent) {
		return healthRegenPercent * health_absolute * SEND_AND_UPDATE_RATE_WORLD;
	}

	public void applyLinearImpulse(Vector2 impulse) {
		physical_hitbox.getBody().applyLinearImpulse(impulse, physical_hitbox.getBody().getWorldCenter(), false);
	}

	/**
	 * This method needs to know the team membership. Look at the {@link MetaCreep}
	 * object of this class and set its field before you call this method
	 * 
	 * @param world
	 */
	public void setHitbox(World world) {
		physical_hitbox = new PhysicalHitboxCreep(world, this);
	}

	/**
	 * Note that the {@link #physical_hitbox} must not be <code>null</code>
	 * 
	 * @param drawableCreep
	 * 
	 */
	public void updateByMetaData(DrawableCreep drawableCreep) {
		if (physical_hitbox != null) {

			dying_skill = new CreepDyingSkill(this, drawableCreep, "skin0");
			skill_0 = new CreepSkill0(creep_category, this, drawableCreep, "skin0");

			health_absolute = HEALTH;
			health_current = health_absolute;
			physical_hitbox.getBody().setActive(true);

		}
	}

	/**
	 * Forced termination of the {@link #current_skill}
	 */
	private void cancelCurrentSkill() {
		if (current_skill != null)
			info_animations.track_1 = AnimationName.P3;
		current_skill = null;
	}

	public void setCurrentTarget(IEmitter target) {
		current_target = target;
	}

	public float getCastTime() {
		return cast_time;
	}

	/**
	 * Might be <code>null</code>
	 * 
	 * @return
	 */
	public ACreepSkill getCurrentSkill() {
		return current_skill;
	}

	@Override
	public void setSlayer(IEmitter slayer) {
		this.slayer = slayer;
	}

	@Override
	public IEmitter getSlayer() {
		return slayer;
	}

	/**
	 * Returns <code>true</code> if the player has finished the current skill
	 * animation. Otherwise <code>false</code> (and when current skill is equal to
	 * <code>null</code>)
	 * 
	 * @return
	 */
	private boolean hasFinishedCurrentSkill() {
		// no current skill => return false.
		if (current_skill == null)
			return false;
		return current_skill.isAnimationComplete() == true;
	}

	public PhysicalHitboxCreep getHitbox() {
		return physical_hitbox;
	}

	/**
	 * After a world step the physical_character (with body member) holds new values
	 * about position and velocity. We need to update the position and velocity of
	 * this class ( {@link PhysicalClient} ).
	 */
	public void updateAfterStep() {
		pos.set(physical_hitbox.getPosition());
		vel.set(physical_hitbox.getVelocity());

		// the player might got a deadly shot in step()
		if (isAlive() == false && prev_alive == true) {

			elapsed_time = 0;

			prev_alive = false;
			cancelCurrentSkill();

			if (location != Location.CLIENTSIDE_MULTIPLAYER) {
				dying_skill.init();
			}
		}

		emission_list.update();

		// current_skill_animation = null;
	}

	/**
	 * Cannot be called in {@link #update()} because multiple calls would override
	 * those fields
	 */
	public void resetOutsidePhysics() {
		// reset all animation fields, so no animation is permament active.
		info_animations.clear();

		triggers.clear();
	}

	@Override
	public void increaseCrystals() {
		// no chrystals for creeps
	}

	@Override
	public void setPosition(float x, float y) {
		origin.set(x, y);
		pos.set(x, y);
		physical_hitbox.setPosition(x, y);
	}

	/**
	 * Returns a sparse representation of {@link PhysicalClient} to transmit its
	 * information from server to clients frequently
	 * 
	 * @return {@link SparseClient}
	 */
	public SparseCreep getSparseCreep() {
		sparse_creep.set(meta_creep.id, pos, vel, sparse_projectiles);
		return sparse_creep;
	}

	public ViewCircle getViewCircle() {
		return view_circle;
	}

	public ViewCircle getAttackEntryCircle() {
		return aggro_circle;
	}

	public ViewCircle getFightStanceEntryCircle() {
		return range_circle;
	}

	/**
	 * 
	 * @param visible
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * Returns whether this player is inside of enemies view circles
	 * 
	 * @return
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * Returns all current active timed effects
	 * 
	 * @return
	 */
	public ArrayList<AEffectTimed> getEffectsTimed() {
		return effects_timed;
	}

	@Override
	public void addEffect(AEffectTimed effect) {
		effects_timed.add(effect);
		Collections.sort(effects_timed);
	}

	@Override
	public LinkedList<Trigger> getTriggers() {
		return triggers;
	}

	@Override
	public World getWorld() {
		return physical_hitbox.getWorld();
	}

	@Override
	public Location getLocation() {
		return location;
	}

	@Override
	public Vector2 getPos() {
		return pos;
	}

	/**
	 * Returns the current interpolated time depending on {@link #location}. In
	 * {@link WorldOnlineRoot} it's the gameserver time, in
	 * {@link WorldTrainingRoot} it's the local time. Useful to update cooldowns
	 * without sending the cooldown of each skill.
	 * 
	 * @return
	 */
	public float getTime() {
		return time;
	}

	public CreepCategory getCreepCategory() {
		return creep_category;
	}

	/**
	 * Sets the current world time. Useful to update cooldowns without sending the
	 * cooldown of each skill.
	 * 
	 * @param time
	 */
	public void setTime(float time) {
		this.time = time;
	}

	@Override
	public EmissionList getEmissionList() {
		return emission_list;
	}

	@Override
	public Team getTeam() {
		return null;
	}

	@Override
	public Body getBody() {
		return physical_hitbox.getBody();
	}

	public void setStunned(boolean stunned) {
		this.stunned = stunned;
	}

	public void setRooted(boolean rooted) {
		this.rooted = rooted;
	}

	public void setDisarmed(boolean disarmed) {
		this.disarmed = disarmed;
	}

	public boolean isStunned() {
		return stunned;
	}

	public boolean isDisarmed() {
		return disarmed;
	}

	@Deprecated
	@Override
	public Vector2 getCursorPos() {
		return new Vector2();
	}

	@Override
	public LinkedList<SparseProjectile> getSparseProjectiles() {
		return sparse_projectiles;
	}

	@Override
	public ComboStorage getComboStorage1() {
		return combo_container;
	}

	@Override
	public ComboStorage getComboStorage2() {
		return null;
	}

	@Override
	public ComboStorage getComboStorageTransfer() {
		return null;
	}

	private class Comparator implements java.util.Comparator<IEmitter> {

		private Vector2 pos;

		public Comparator() {
			pos = sparse_creep.pos;
		}

		@Override
		public int compare(IEmitter o1, IEmitter o2) {
			if (o1.getPos().dst2(pos) < o2.getPos().dst2(pos))
				return -1;
			return 1;
		}
	}

	public IEmitter getCurrentTarget() {
		return current_target;
	}

	public IEmitter getCurrentPotentialTarget() {
		return current_potential_target;
	}

	public void setCurrentSkill(ACreepSkill skill) {
		current_skill = skill;
	}

	public void setCastTime(float castTime) {
		cast_time = castTime;
	}

	/**
	 * Remove creep from list when it's dead and a certain time has elapsed
	 * 
	 * @return
	 */
	public boolean hasToBeRemoved() {
		return elapsed_time > TIME_AFTER_DEATH_TIL_REMOVE && isAlive() == false;
	}

	public Vector2 getOrigin() {
		return origin;
	}

	public boolean isAtOrigin() {
		return MathUtils.isZero(getPos().dst(origin), 0.2f);
	}

}
