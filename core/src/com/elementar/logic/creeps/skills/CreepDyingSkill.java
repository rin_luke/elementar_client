package com.elementar.logic.creeps.skills;

import com.badlogic.gdx.physics.box2d.Filter;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.ASkill;
import com.elementar.logic.creeps.DrawableCreep;
import com.elementar.logic.creeps.PhysicalCreep;
import com.elementar.logic.creeps.ACreep.CreepCategory;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.DefProjectile.CollectableType;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefChrystals;
import com.elementar.logic.emission.def.EmissionDefDeathExplosionCharacter;
import com.elementar.logic.emission.effect.EffectDying;
import com.elementar.logic.emission.order.RandomOrder;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

/**
 * 
 * @author lukassongajlo
 * 
 */
public class CreepDyingSkill extends ACreepSkill {

	private AEmissionDef def_death1, def_death2, def_death3, def_death4;

	/**
	 * 
	 * @param physicalClient
	 * @param enabledHitboxes , should be <code>true</code> on client-side
	 */
	public CreepDyingSkill(PhysicalCreep physicalCreep, DrawableCreep drawableCreep, String unparsedSkinName) {
		super(CreepCategory.SMALL, physicalCreep, drawableCreep, null, 0, 0, 0);
	}

	@Override
	protected void updateCastTime() {

	}

	@Override
	protected void defineEmissions() {

		float multiplier = 1f;

		DefProjectile defProjectile4 = new DefProjectile(2000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_EMITTER);

		def_death4 = new EmissionDefAngleDirected("graphic/particles/particle_creep/creep_death4.p", 1, 1f, false,
				defProjectile4, 1, 0, new FixedAlignment(90))
						.setStartConditions(new StartConditions(0, CollisionKinds.AFTER_ABSORPTION.getValue()));

		DefProjectile defProjectile3 = new DefProjectile(4000,
				getCollisionFilter(CollisionData.BIT_CHARACTER_PROJECTILE)).setVelocity(2f).setVelocityVariation(0.5f)
						.setGravityScale(0f).setCollectable(CollectableType.CRYSTAL).addSuccessor(def_death4)
						.setAngleVariation(0.5f);

		def_death3 = new EmissionDefChrystals("graphic/particles/particle_creep/creep_death3.p", defProjectile3);

		Filter collisionFilter = getCollisionFilter(0);

		DefProjectile defProjectile2 = new DefProjectile(500, collisionFilter).setVelocity(1.4f).setGravityScale(0.4f)
				.setPolygonShape(3).setFriction(1f);

		def_death2 = new EmissionDefDeathExplosionCharacter("graphic/particles/particle_creep/creep_death2.p",
				multiplier, true, defProjectile2, 4, 0, 220, 20, new RandomOrder(), new FixedAlignment(0))
						.addEffect(new EffectDying()).setTriggerEmissionAllowed();

		DefProjectile defProjectile1 = new DefProjectile(3000, ASkill.getNeutralFilter());

		def_death1 = new EmissionDefAngleDirected("graphic/particles/particle_creep/creep_death1.p", multiplier,
				multiplier, false, defProjectile1, 1, 0, new FixedAlignment(0)).setSound(AudioData.global_death)
						.addSuccessorTimewise(0, def_death2);

	}

	@Override
	public void init() {
		physical_creep.setCurrentSkill(this);
		startEmission();
	}

	@Override
	protected Emission create() {

		/*
		 * the following works just because this skill is used only once.
		 */
		if (physical_creep.getSlayer() instanceof PhysicalClient)
			def_death1.addSuccessorTimewise(0, def_death3);

		def_death4.setCenter(physical_creep.getSlayer().getPos());

		def_death3.setCenter(physical_creep.getPos());

		def_death2.setCenter(physical_creep.getPos());
		def_death2.setBonesModel(drawable_creep);
		def_death2.setTarget(physical_creep);
		/*
		 * Spieler muss hier gesetzt werden, da keine kollision vorher stattfand (und
		 * somit kein Setzen des effect targets)
		 */
		def_death1.setCenter(physical_creep.getPos());
		return new Emission(def_death1, physical_creep);
	}

}
