package com.elementar.logic.creeps.skills;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.World;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.DrawableCreep;
import com.elementar.logic.creeps.PhysicalCreep;
import com.elementar.logic.creeps.ACreep.CreepCategory;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.Team;

public abstract class ACreepSkill {

	protected PhysicalCreep physical_creep;

	protected DrawableCreep drawable_creep;

	/**
	 * unparsed would be 'character_wind_skin0' and parsed just 'skin0'
	 */
	protected String skin_name_parsed;

	// next members are meta data of a skill

	protected float cooldown_current;

	/**
	 * in seconds
	 */
	protected float animation_duration;

	/**
	 * in seconds
	 */
	protected float elapsed_time;

	/**
	 * Each skill has an origin value (at the beginning, unchangeable) and an
	 * absolute, changeable value (might changes when the player gets a buff
	 * etc.)
	 * <p>
	 * in seconds.
	 */
	protected float cooldown_modifiable_absolute, cooldown_absolute;

	/**
	 * Holds the team membership info of the target
	 */
	protected Team team_target;

	protected World world;

	protected Body body;

	/**
	 * Has an absolute value (at the beginning) and a changeable value (might
	 * changes when the player gets a buff etc.)
	 * <p>
	 * in seconds.
	 * <p>
	 * + = channel, - = charge, instant skills will get a little charge time,
	 * too
	 */
	protected float cast_time_current, cast_time_absolute;

	/**
	 * p1 time scale depends on fire rate (basic attacks), so I need
	 * additionally the absolute value to calculate the current value
	 */
	protected float time_scale_p1_current, time_scale_p1_absolute;

	protected CreepCategory creep_category;

	/**
	 * 
	 * @param creepCategory
	 * @param physicalCreep
	 * @param drawableCreep
	 * @param unparsedSkinName
	 * @param teamTarget
	 * @param castTime
	 * @param cooldown
	 * @param p1Duration
	 */
	public ACreepSkill(CreepCategory creepCategory, PhysicalCreep physicalCreep,
			DrawableCreep drawableCreep, String unparsedSkinName, float castTime, float cooldown,
			float p1Duration) {

		creep_category = creepCategory;

		cooldown_modifiable_absolute = cooldown_absolute = cooldown;
		this.physical_creep = physicalCreep;
		this.drawable_creep = drawableCreep;

		cast_time_current = cast_time_absolute = castTime;
		time_scale_p1_current = time_scale_p1_absolute = 1f / p1Duration;

		if (physicalCreep != null) {
			world = physical_creep.getWorld();
			body = physical_creep.getBody();
		}
		// parse the unparsed skin name
		// skin_name_parsed = unparsedSkinName != null ? unparsedSkinName
		// .substring(unparsedSkinName.length() - 5,
		// unparsedSkinName.length()) : null;

		defineEmissions();
	}

	/**
	 * Create for each emission def a field.
	 */
	protected abstract void defineEmissions();

	/**
	 * Returns the first emission. If available, set the right center positions
	 * for the defined def-fields
	 * 
	 * 
	 * @return first emission
	 */
	protected abstract Emission create();

	protected abstract void updateCastTime();

	public void init() {
		// cooldown-check in Creep class
		physical_creep.setCurrentSkill(this);
		physical_creep.setCastTime(cast_time_current);
		elapsed_time = 0;
	}

	/**
	 * Wrapper method for {@link #create()}. This method creates the first
	 * emission. It is called where the emission should start. For channel
	 * skills the emission has to begin instantly, for charge skills after the
	 * cast time has elapsed
	 * 
	 * @return
	 */
	protected Emission startEmission() {
		cooldown_current = cooldown_modifiable_absolute;
		Emission firstEmission = null;
		if (physical_creep.getLocation() != Location.CLIENTSIDE_MULTIPLAYER) {
			firstEmission = create();
			physical_creep.getEmissionList().add(firstEmission);
		}
		return firstEmission;
	}

	/**
	 * Delta time is not the frame time. As the world, skills got updated not
	 * framewise.
	 */
	public void update() {

		if (physical_creep.getCurrentSkill() == this) {
			if (isAnimationComplete() == false) {
				// casting phase (animation)

				updateCastTime();
			}

			elapsed_time += Shared.SEND_AND_UPDATE_RATE_WORLD;

		} else
			// decrease cooldown
			cooldown_current -= Shared.SEND_AND_UPDATE_RATE_WORLD;

	}

	public boolean isAnimationComplete() {
		return elapsed_time > animation_duration;
	}

	/**
	 * 
	 * @param collisionObjects
	 *            , sum of all valid collision_bits of objects
	 * @return
	 */
	protected Filter getCollisionFilter(int collisionObjects) {
		Filter collisionFilter = new Filter();
		// setting category and mask bits
		collisionFilter.categoryBits = CollisionData.BIT_PROJECTILE;
		collisionFilter.maskBits = (short) collisionObjects;
		return collisionFilter;
	}

	/**
	 * bodies with that filter just collide with ground
	 * 
	 * @return
	 */
	protected static Filter getNeutralFilter() {
		Filter collisionFilter = new Filter();
		collisionFilter.categoryBits = CollisionData.BIT_CHARACTER_MOVEMENT;
		collisionFilter.maskBits = CollisionData.BIT_GROUND;
		return collisionFilter;
	}

	public float getCooldownCurrent() {
		return cooldown_current;
	}

	/**
	 * Just needed for charge skills
	 * 
	 * @return
	 */
	public float getTimeScaleP1() {
		return time_scale_p1_current;
	}

	/**
	 * Returns the current absolute cooldown value of this skill, not the
	 * remaining cooldown.
	 * 
	 * @return
	 */
	public float getCooldownModifiableAbsolute() {
		return cooldown_modifiable_absolute;
	}

}
