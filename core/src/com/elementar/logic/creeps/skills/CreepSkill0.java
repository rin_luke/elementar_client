package com.elementar.logic.creeps.skills;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.DrawableCreep;
import com.elementar.logic.creeps.PhysicalCreep;
import com.elementar.logic.creeps.ACreep.CreepCategory;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.IEmitter;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class CreepSkill0 extends ACreepSkill {

	private AEmissionDef e1;

	protected EmissionDefBoneFollowing charge_def;

	private static final float DEFAULT_SIEGE_PROJECTILE_VEL = 3f;

	public CreepSkill0(CreepCategory creepCategory, PhysicalCreep physicalCreep, DrawableCreep drawableCreep,
			String unparsedSkinName) {
		super(creepCategory, physicalCreep, drawableCreep, unparsedSkinName, getCastTimeByCategory(creepCategory),
				getCooldownByCategory(creepCategory) // cooldown
				, getP1DurationByCategory(creepCategory));

		String p1AnimationString = creep_category == CreepCategory.SMALL ? "creep_small1_charge_p1"
				: "creep_big1_charge_p1";

		float durationP1 = drawableCreep.getSkeleton().getData().findAnimation(p1AnimationString).getDuration();
		animation_duration = durationP1 / time_scale_p1_current;

		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter())
				.setFlyThroughTargets();

		String path;
		if (creep_category == CreepCategory.BIG)
			path = "graphic/particles/particle_creep/creep_big1_skill0_charge.p";
		else
			path = "graphic/particles/particle_creep/creep_small1_skill0_charge.p";

		charge_def = new EmissionDefBoneFollowing(path, 1, 1, false, prototypeCharge, "character_hand_front");

	}

	private static float getP1DurationByCategory(CreepCategory creepCategory) {
		if (creepCategory == CreepCategory.SMALL)
			return 0.15f;
		return 0.3f;
	}

	private static float getCooldownByCategory(CreepCategory creepCategory) {
		if (creepCategory == CreepCategory.SMALL)
			return 1.2f;
		return 2f;
	}

	private static float getCastTimeByCategory(CreepCategory creepCategory) {
		if (creepCategory == CreepCategory.SMALL)
			return -0.125f;
		return -0.2f;
	}

	@Override
	public void init() {
		if (physical_creep.isDisarmed() == true || physical_creep.isStunned() == true)
			return;

		super.init();
		if (charge_def != null)
			if (physical_creep.getLocation() != Location.CLIENTSIDE_MULTIPLAYER) {

				// for charge, the center pos is necessary for playing sound
				// correctly
				charge_def.setCenter(physical_creep.pos);

				charge_def.setBonesModel(drawable_creep);
				physical_creep.getEmissionList().add(new Emission(charge_def, physical_creep));
			}
	}

	@Override
	protected void updateCastTime() {
		physical_creep.info_animations.track_1 = AnimationName.P1_CHARGE;
		// inc cast time
		physical_creep.setCastTime(physical_creep.getCastTime() + Shared.SEND_AND_UPDATE_RATE_WORLD);
		// change animation when cast time is finished.
		if (physical_creep.getCastTime() >= 0) {
			// one time event
			if (physical_creep.getCastTime() - Shared.SEND_AND_UPDATE_RATE_WORLD < 0) {
				/*
				 * startEmission() will add the first emission to client's emission list to
				 * update that emission loop-wise
				 */
				startEmission();
			}
		}
	}

	@Override
	protected void defineEmissions() {

		String e1Path, e2Path;

		if (creep_category == CreepCategory.SMALL) {
			e1Path = "graphic/particles/particle_creep/creep_small1_skill0_e1.p";
			e2Path = "graphic/particles/particle_creep/creep_small1_skill0_e2.p";
		} else {
			e1Path = "graphic/particles/particle_creep/creep_big1_skill0_e1.p";
			e2Path = "graphic/particles/particle_creep/creep_big1_skill0_e2.p";
		}

		// eMagical
		DefProjectile defProjectile = new DefProjectile(600, getNeutralFilter());

		AEmissionDef e2 = new EmissionDefAngleDirected(e2Path, 2f, 2f, false, defProjectile, 1, 0,
				new ObstacleAlignment()).setStartConditions(
						new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_BUILDING,
								CollisionKinds.CAST_ON_ENEMY.getValue() + CollisionKinds.AFTER_LIFETIME.getValue()))
						.addEffect(new EffectDamage(-10));

		// e1, main projectile

		if (creep_category == CreepCategory.BIG) {
			DefProjectile prototype = new DefProjectile(2500,
					getCollisionFilter(CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_BUILDING))
							.setGravityScale(1f).addSuccessor(e2);

			e1 = new EmissionDefAngleDirected(e1Path, 1f, 1f, true, prototype, 1, 0, new FixedAlignment(90));

		} else {
			DefProjectile prototype = new DefProjectile(2500,
					getCollisionFilter(CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_BUILDING))
							.setVelocity(3.5f).addSuccessor(e2);

			e1 = new EmissionDefAngleDirected(e1Path, 0.7f, 0.7f, true, prototype, 1, 0, new FixedAlignment(90));
		}

	}

	public void setPersecutee(IEmitter persecutee) {
		e1.getDefProjectile().setPersecutee(persecutee, e1, physical_creep.getSparseCreep().pos);
	}

	@Override
	protected Emission create() {
		e1.setCenter(physical_creep.getSparseCreep().pos);

		if (creep_category == CreepCategory.BIG) {
			float v0 = DEFAULT_SIEGE_PROJECTILE_VEL;
			float g = -1 * Shared.GRAVITY * e1.getDefProjectile().getGravityScale();
			Vector2 posTarget = e1.getDefProjectile().getPersecutee().getPos();
			Vector2 posCreep = physical_creep.getSparseCreep().pos;
			float x = posTarget.x - posCreep.x, y = posTarget.y - posCreep.y;

			float angle = 0;

			/*
			 * if the innerSqrt is negative, there isn't any solution with the current v0,
			 * so we have to find a higher velocity to fit the trajectory
			 */
			float innerSqrt = -1;
			float offset = 0;
			while (innerSqrt < 0) {
				innerSqrt = (float) (Math.pow(v0, 4) - g * (g * x * x + 2 * y * v0 * v0));
				if (innerSqrt < 0) {
					v0 = (float) Math.sqrt((y * g) + Math.sqrt(y * y + x * x) * g) + offset;
					offset = offset + 0.1f;
				}
			}
			/*
			 * there a two possible solutions and we take always the negative sqrt to get
			 * the direct trajectory
			 */
			float numerator = (v0 * v0) - (float) Math.sqrt(Math.pow(v0, 4) - g * (g * x * x + 2 * y * v0 * v0));
			angle = MathUtils.radiansToDegrees * (float) Math.atan(numerator / (g * x));

			// flip the shot direction
			if (posTarget.x - posCreep.x < 0)
				angle = (-1) * Util.switchAngleUnitCircle(angle);

			e1.getDefProjectile().setVelocity(v0);
			e1.getAlignment().setAngle((short) angle);

			e1.getDefProjectile().setPersecutee(null, null, null);
		}

		return new Emission(e1, physical_creep);
	}
}
