package com.elementar.logic.creeps;

import static com.elementar.logic.util.Shared.PPM;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.elementar.logic.characters.abstracts.APhysicalElement;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.userdata.CharacterPlantContacts;
import com.elementar.logic.util.userdata.SeparatorContacts;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class PhysicalHitboxCreep extends APhysicalElement {

	protected CharacterPlantContacts plant_contacts;

	protected SeparatorContacts separator_contacts;

	/**
	 * surrounding character. category = foot, mask = plant
	 */
	protected Fixture plant_sensor;

	protected Fixture torso_fixture;

	protected Fixture light_fixture;

	/**
	 * surrounding character, collides with other characters to avoid that
	 * characters stand in one another.
	 */
	protected Fixture separator_sensor;

	/**
	 * Surrounds character like {@link #plant_sensor}. category =
	 * {@link CollisionData#BIT_CHARACTER_PROJECTILE}, mask =
	 * {@link CollisionData#BIT_PROJECTILE}
	 */
	protected Fixture projectile_sensor;

	/**
	 * Userdata for {@link #damage_sensor} and {@link #heal_sensor}
	 */
	protected PhysicalCreep physical_creep;

	private static Filter filter_torso, filter_projectile, filter_plant, filter_light,
			filter_separator;

	{
		// define category & mask bits for movement fixtures
		filter_torso = new Filter();
		filter_torso.maskBits = CollisionData.BIT_GROUND;
		filter_torso.categoryBits = CollisionData.BIT_CHARACTER_MOVEMENT;

		// define category & mask bits for hit detection
		filter_projectile = new Filter();
		filter_projectile.maskBits = CollisionData.BIT_PROJECTILE;
		filter_projectile.categoryBits = CollisionData.BIT_CHARACTER_PROJECTILE;

		// define category & mask bits for collisions with plants
		filter_plant = new Filter();
		filter_plant.maskBits = CollisionData.BIT_PLANT;
		filter_plant.categoryBits = CollisionData.BIT_CHARACTER_PLANT;

		// define category & mask bits for collisions with light (para-world
		// character)
		filter_light = new Filter();
		filter_light.maskBits = CollisionData.BIT_LIGHT;
		filter_light.categoryBits = CollisionData.BIT_CHARACTER_LIGHT;

		filter_separator = new Filter();
		filter_separator.maskBits = CollisionData.BIT_CHARACTER_SEPARATOR;
		filter_separator.categoryBits = CollisionData.BIT_CHARACTER_SEPARATOR;

	}

	/**
	 * 
	 * @param world
	 * @param physicalCreep
	 */
	public PhysicalHitboxCreep(World world, PhysicalCreep physicalCreep) {
		super(world);
		physical_creep = physicalCreep;
		plant_contacts = new CharacterPlantContacts();
		separator_contacts = new SeparatorContacts(physicalCreep);

		initPhysics(0, 0);
		setCollisionBits();

	}

	private void setCollisionBits() {
		// set collisions bits

		torso_fixture.setFilterData(filter_torso);
		projectile_sensor.setFilterData(filter_projectile);
		plant_sensor.setFilterData(filter_plant);
		light_fixture.setFilterData(filter_light);
		separator_sensor.setFilterData(filter_separator);

	}

	@Override
	protected void initPhysics(float x, float y) {
		body_def.type = BodyType.DynamicBody;
		body_def.allowSleep = false;
		body_def.position.set(x, y);
		body_def.fixedRotation = true;
		body_def.bullet = true;
		body_def.gravityScale = 0;

		// create bodies
		body = world.createBody(body_def);

		// create shapes
		CircleShape circleShape = new CircleShape();
		PolygonShape polygonShape = new PolygonShape();
		float scale = 1.5f;

		// create torso fixture
		circleShape.setRadius(10f / PPM);
		circleShape.setPosition(new Vector2(0, 6 * scale / PPM));
		fixture_def.shape = circleShape;
		fixture_def.friction = 0f;
		fixture_def.restitution = 0f;
		fixture_def.density = 0f; // also by default = 0...
		torso_fixture = body.createFixture(fixture_def);

		float yShift = 10f / PPM;
		// create light fixture (para world character)
		polygonShape.setAsBox(scale * 6.5f / PPM, scale * 14f / PPM,
				new Vector2(0, -yShift + scale * 14f / PPM), 0);
		fixture_def.shape = polygonShape;
		light_fixture = body.createFixture(fixture_def);

		// SENSORS:
		fixture_def.isSensor = true;
		fixture_def.shape = polygonShape;

		// plant sensor
		polygonShape.setAsBox(scale * 5f / PPM, scale * 15f / PPM,
				new Vector2(0, -yShift + scale * 18f / PPM), 0);
		plant_sensor = body.createFixture(fixture_def);
		plant_sensor.setUserData(plant_contacts);

		// projectile sensor, same dimensions as plant sensor
		projectile_sensor = body.createFixture(fixture_def);
		projectile_sensor.setUserData(physical_creep);

		circleShape.setRadius(SeparatorContacts.SEPARATOR_RADIUS);
		circleShape.setPosition(new Vector2(0, 6 * scale / PPM));
		separator_sensor = body.createFixture(fixture_def);
		separator_sensor.setUserData(separator_contacts);

		polygonShape.dispose();
		circleShape.dispose();

	}

	public void setVelocity(Vector2 vel) {
		body.setLinearVelocity(vel);
	}

	/**
	 * Don't use this method. The physical body of a creep needs more than 1
	 * collision filter.
	 */
	@Deprecated
	@Override
	protected Filter getCollisionFilter() {
		return null;
	}

	public Fixture getProjectileSensor() {
		return projectile_sensor;
	}

}
