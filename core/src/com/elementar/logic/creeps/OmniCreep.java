package com.elementar.logic.creeps;

import com.badlogic.gdx.physics.box2d.World;
import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.ACreep.CreepCategory;
import com.elementar.logic.emission.IEmitter;

public class OmniCreep {
	private static short ID_TRAINING = 0;

	private PhysicalCreep physical_creep;
	private SparseCreep prev_creep;
	private DrawableCreep drawable_creep;

	/**
	 * Constructor for world characters
	 * 
	 * @param metaCreep
	 * @param isDummy
	 * @param serverSide
	 */
	public OmniCreep(MetaCreep metaCreep, CreepCategory creepCategory,
			Location location) {
		prev_creep = new SparseCreep();
		physical_creep = new PhysicalCreep(metaCreep, creepCategory, location);
		drawable_creep = new DrawableCreep(metaCreep, creepCategory, false);
		if (location == Location.CLIENTSIDE_TRAINING) {
			physical_creep.connection_id = drawable_creep.connection_id
					= metaCreep.id = physical_creep.getSparseCreep().connection_id = ID_TRAINING;
			ID_TRAINING++;
		}
	}

	/**
	 * Note that {@link #physical_creep}'s metaClient is equal to
	 * {@link #drawable_creep}'s metaClient
	 * 
	 * @return
	 */
	public MetaCreep getMetaCreep() {
		return physical_creep.getMetaCreep();
	}

	public PhysicalCreep getPhysicalCreep() {
		return physical_creep;
	}

	public DrawableCreep getDrawableCreep() {
		return drawable_creep;
	}

	/**
	 * Creates a hitbox and calls afterwards {@link #updateByMetaData()}
	 * 
	 * @param world
	 *            , for movement, vegetation, skills, client-prediction.
	 */
	public void init(World world) {
		physical_creep.setHitbox(world);
		updateByMetaData();
	}

	/**
	 * if the gameclass, skin index or team of the {@link MetaClient} object has
	 * changed, then {@link #physical_creep} and {@link #drawable_creep} must
	 * update
	 */
	public void updateByMetaData() {
		drawable_creep.updateByMetaData(false);
		physical_creep.updateByMetaData(drawable_creep);

		// set 'pos' fields to avoid interpolating at the beginning
		drawable_creep.pos.set(physical_creep.getHitbox().getPosition());
		physical_creep.pos.set(physical_creep.getHitbox().getPosition());
		prev_creep.pos.set(physical_creep.getHitbox().getPosition());
	}

	public void setPosition(float x, float y) {
		drawable_creep.setPosition(x, y);
		physical_creep.setPosition(x, y);
	}

	public SparseCreep getPrevCreep() {
		return prev_creep;
	}

	public IEmitter getCurrentTarget() {
		return null;
	}

}
