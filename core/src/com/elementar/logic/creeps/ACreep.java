package com.elementar.logic.creeps;

import com.elementar.logic.creeps.skills.CreepDyingSkill;
import com.elementar.logic.creeps.skills.CreepSkill0;
import com.elementar.logic.network.response.InfoAnimations;

public abstract class ACreep extends SparseCreep {

	protected final MetaCreep meta_creep;

	/**
	 * this field will be set to <code>true</code> when the match is over
	 */
	protected boolean match_finished = false;

	protected CreepDyingSkill dying_skill;
	protected CreepSkill0 skill_0;

	public InfoAnimations info_animations = new InfoAnimations();

	public short health_current, health_absolute;
	
	protected CreepCategory creep_category;
	
	public static enum CreepCategory {
		SMALL, BIG
	}

	
	/**
	 * 
	 * @param metaClient
	 */
	public ACreep(MetaCreep metaCreep, CreepCategory creepCategory) {
		super(metaCreep.id);

		creep_category = creepCategory;
		health_current = health_absolute = 1;
		meta_creep = metaCreep;
	}

	public void update() {
	}

	public void setMatchFinished() {
		match_finished = true;
	}

	protected abstract void setPosition(float x, float y);

	public MetaCreep getMetaCreep() {
		return meta_creep;
	}

	public boolean isAlive() {
		return health_current > 0;
	}

}
