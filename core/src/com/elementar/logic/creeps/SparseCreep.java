package com.elementar.logic.creeps;

import java.util.LinkedList;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.emission.SparseProjectile;
import com.elementar.logic.util.SparseObject;

public class SparseCreep extends SparseObject {

	/**
	 * Holds all active projectiles
	 */
	public LinkedList<SparseProjectile> sparse_projectiles = new LinkedList<SparseProjectile>();

	public SparseCreep() {
	}

	public SparseCreep(short id) {
		super(id, new Vector2(), new Vector2());
	}

	/**
	 * To send via network from gameserver to clients
	 * 
	 * @param id
	 * @param creepPos
	 * @param creepVel
	 */
	public void set(int id, Vector2 creepPos, Vector2 creepVel,
			LinkedList<SparseProjectile> projectilesPerSkill) {
		this.connection_id = (short) id;
		this.pos.set(creepPos);
		this.vel.set(creepVel);
		this.sparse_projectiles = projectilesPerSkill;
	}

}
