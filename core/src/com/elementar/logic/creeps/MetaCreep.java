package com.elementar.logic.creeps;

public class MetaCreep {
	// data which will not be send frequently
	public short id;

	public int skin_index;
	
	public MetaCreep() {

	}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param slotIndex
	 * @param team
	 */
	public MetaCreep(int id, int skinIndex) {
		this.id = (short) id;
		skin_index = skinIndex;
	}

}
