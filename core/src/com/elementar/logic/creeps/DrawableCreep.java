package com.elementar.logic.creeps;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.ParticleContainer;
import com.elementar.data.container.util.CustomParticleEffect;
import com.elementar.data.container.util.GameclassParticleEffect;
import com.elementar.data.container.util.GameclassSkin;
import com.elementar.gui.abstracts.AGameclassSelectionModule;
import com.elementar.gui.util.CustomSkeleton;
import com.elementar.gui.util.DamageDiffbar;
import com.elementar.gui.util.HasDamageDifference;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.creeps.skills.CreepSkill0;
import com.elementar.logic.emission.def.IBonesModel;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationState.TrackEntry;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.Bone;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonRenderer;

public class DrawableCreep extends ACreep implements IBonesModel, HasDamageDifference {

	private static final float MIX_TIME_SKILLS = 0.3f;
	private static final int TRACK_0 = 0, TRACK_1 = 1, TRACK_2 = 2, TRACK_3 = 3, TRACK_4 = 4;

	private static final float MIX_TIME = 0.5f;

	private MyAnimation IDLE_ANIMATION, WALK_ANIMATION, RECEIVE_DAMAGE_ANIMATION, DEATH_EXPLOSION_ANIMATION,
			ALPHA_FADE_IN_ANIMATION, ALPHA_FADE_OUT_ANIMATION, SKILL_P1_ANIMATION, STUNNED_ANIMATION,
			FIGHT_STANCE_ON_ANIMATION, FIGHT_STANCE_OFF_ANIMATION, COLOR_ICE_ANIMATION, COLOR_TOXIC_ANIMATION,
			COLOR_DEFAULT_ANIMATION;

	/**
	 * P3 shows the character in a neutral pose. It kicks in after P2 (charge) or P1
	 * (channel) are done. The interpolation between those P's and P3 will show how
	 * the player pull its front arm back (that's the typical case if no further
	 * skill is casted). Otherwise, when a skill is queued and waits to its release,
	 * the interpolation is between a P (as specified above) and the next P1 (of the
	 * queued skill)
	 */
	private MyAnimation P3_ANIMATION;

	private GameclassSkin gameclass_skin;

	private CustomSkeleton skeleton;

	protected ArrayList<CustomParticleEffect> particle_effects_filtered = new ArrayList<>();

	private CustomParticleEffect hand_front_particle_effect, hand_back_particle_effect, torso_particle_effect;

	private Bone head_bone_flip, head_bone_rotation, hand_front_bone_flip, hand_front_bone_rotation,
			hand_back_bone_flip, hand_back_bone_rotation, torso_bone_flip, torso_bone_rotation, shoulder_front_flip,
			shoulder_back_flip;

	private MyAnimationState animation_state;

	private MyAnimation current_track_0_animation, current_track_1_animation, current_track_2_animation,
			current_track_3_animation, current_track_4_animation;

	/**
	 * Animations hold a number_frames_till_change field. When an animation reaches
	 * that number, a change occurs. The frame_counter (which is necessary for
	 * comparison between frame_counter and number_frames_till_change) has to be
	 * reseted if a continuous chain of {@link #setCurrentAnimation(AnimationName)}
	 * is ripped.
	 * <p>
	 * Example: I walk and sometimes I get a signal for sliding, so the sliding
	 * animation plays. It could be that this signal shouldnt be recognize, so I set
	 * the number of frames until a change to 2. When a chain of
	 * {@link #setCurrentAnimation(AnimationName)} is n (here 2)-times called with
	 * the same animation as parameter, the animation is allowed to change. If the
	 * chain got ripped by an other animation ALL frame_counter should be reset to
	 * 0. That's the reason why I need this animation_list.
	 */
	private ArrayList<MyAnimation> animation_list = new ArrayList<MyAnimation>();

	private float alpha_rotation_interpolation;
	/**
	 * used to interpolate the rotation angle
	 */
	private short rotation_angle_prev, rotation_angle_locked;
	private short rotation_angle;

	/**
	 * Holds all rotation bones to simulate death explosion animation
	 */
	private ArrayList<Bone> bones_for_death;

	private short last_health;

	private DamageDiffbar damage_diff_bar = new DamageDiffbar();

	/**
	 * Constructor for {@link AGameclassSelectionModule}
	 * 
	 */
	public DrawableCreep(MetaCreep metaCreep, CreepCategory creepCategory, boolean flip) {
		super(metaCreep, creepCategory);

		this.skeleton = AnimationContainer.obtainCreepSkeleton();
		skeleton.setToSetupPose();
		skeleton.setFlipX(false);
		skeleton.updateWorldTransform();

		AnimationStateData data = new AnimationStateData(skeleton.getData());
		// global mixtimes
		data.setDefaultMix(MIX_TIME / 2f);
		// data.setMix(LAND_ANIMATION.animation, WALK_ANIMATION.animation,
		// 0.5f);
		// data.setMix(LAND_ANIMATION.animation, IDLE_ANIMATION.animation,
		// 0.5f);

		initSkillAnimations(data);
		initAnimations();

		animation_state = new MyAnimationState(data);

		head_bone_flip = skeleton.findBone("character_head_flip");
		head_bone_rotation = skeleton.findBone("character_head_rotation");

		hand_front_bone_flip = skeleton.findBone("character_hand_front_flip");
		hand_front_bone_rotation = skeleton.findBone("character_hand_front_rotation");

		hand_back_bone_flip = skeleton.findBone("character_hand_back_flip");
		hand_back_bone_rotation = skeleton.findBone("character_hand_back_rotation");

		torso_bone_flip = skeleton.findBone("character_torso_flip");
		torso_bone_rotation = skeleton.findBone("character_torso_rotation");

		shoulder_front_flip = skeleton.findBone("character_shoulder_front_flip");

		shoulder_back_flip = skeleton.findBone("character_shoulder_back_flip");

		// prepare for death case
		fillDeathBonesArray();

		// init currentStanceAnimation
		current_track_0_animation = WALK_ANIMATION;
		animation_state.setAnimation(current_track_0_animation);
		current_track_0_animation.begin();

		// has an effect if getGameclass() isn't null
		updateByMetaData(flip);

	}

	@Override
	public void update() {

	}

	/**
	 * Loads skill animation by skills of the specific character
	 */
	private void initSkillAnimations(AnimationStateData data) {

		P3_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_p3"), TRACK_1, false, 0, 0);

		// init charge and channel animations
		String p1String = "creep_small1_charge_p1";
		if (creep_category == CreepCategory.BIG)
			p1String = "creep_big1_charge_p1";

		SKILL_P1_ANIMATION = new MyAnimation(skeleton.getData().findAnimation(p1String), TRACK_1, false, 3, 0);

		// define mix times between animations
		data.setMix(SKILL_P1_ANIMATION.animation, P3_ANIMATION.animation, P3_ANIMATION.animation.getDuration());
		data.setMix(P3_ANIMATION.animation, SKILL_P1_ANIMATION.animation, MIX_TIME_SKILLS);
	}

	private void initAnimations() {

		String creepCat = creep_category == CreepCategory.BIG ? "_big1_" : "_small1_";

		IDLE_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("creep" + creepCat + "idle"), TRACK_0, true,
				0, 0);

		WALK_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("creep" + creepCat + "walk"), TRACK_0, true,
				3, 0);

		STUNNED_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_stunned"), TRACK_0, true, 0, 0);

		// parameter noch unsicher!
		RECEIVE_DAMAGE_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_receive_damage"),
				TRACK_2, false, 0, 0) {
			@Override
			public boolean runThrough(MyAnimation nextAnimation) {
				return false;
			}
		};

		FIGHT_STANCE_ON_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("creep_fightstance_on"), TRACK_1,
				false, 0, 0);

		FIGHT_STANCE_OFF_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("creep_fightstance_off"), TRACK_1,
				false, 0, 0);

		// slow effect
		COLOR_ICE_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("color_ice"), TRACK_3, true, 0, 0);

		// dot effect
		COLOR_TOXIC_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("color_toxic"), TRACK_3, true, 0, 0);

		COLOR_DEFAULT_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("color_default"), TRACK_3, false, 0,
				0);

		// has transparency changes => Track 3
		DEATH_EXPLOSION_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("creep_death_explosion"), TRACK_4,
				false, 0, 0);

		ALPHA_FADE_IN_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("alpha_fade_in"), TRACK_4, false, 0,
				0);

		ALPHA_FADE_OUT_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("alpha_fade_out"), TRACK_4, false,
				0, 0);

	}

	/**
	 * 
	 * @param delta
	 */
	public void animationRoutine(float delta) {

		animation_state.update(delta);

		animation_state.apply(skeleton);

		TrackEntry e = animation_state.getCurrent(TRACK_1);
		if (e != null) {
			if (e.isComplete() == true && e.getAnimation() != P3_ANIMATION.animation) {
				// stretch endtime as long there is no other signal
				current_track_1_animation.duration_extension += e.getTimeScale() * delta;
				e.setAnimationEnd(e.getAnimationEnd() + current_track_1_animation.duration_extension);
			}
			if (e.isComplete() == true && e.getAnimation() == P3_ANIMATION.animation) {
				current_track_1_animation = null;
				if (current_track_0_animation != null)
					// skill inactive
					// apply flip information in begin() (animation specific)
					current_track_0_animation.begin();
			}
		}

		e = animation_state.getCurrent(TRACK_2);
		if (e != null) {
			if (e.isComplete() == true) {
				current_track_2_animation = null;
				animation_state.clearTrack(TRACK_2);
			}
		}

		/*
		 * Now handle Rotation
		 */

		if (isAlive() == true) {
			// rotate bones
			hand_front_bone_rotation.setRotation(rotation_angle_prev);
			if (current_track_0_animation != STUNNED_ANIMATION)
				head_bone_rotation.setRotation(rotation_angle_prev);
			// front hand same angle as head (see hit animation)
			torso_bone_rotation.setRotation(rotation_angle_prev * 0.15f);

			if (current_track_0_animation.animation.getName().equals("character_slide_hold_ground") == false
					&& current_track_0_animation.animation.getName().equals("character_slide_hold_ceiling") == false) {
				hand_back_bone_rotation.setRotation(rotation_angle_prev * 0.4f);
			}
		}

		// rotate particle effects depending on bone rotation

		if (hand_front_particle_effect != null)
			hand_front_particle_effect.setRotation(hand_front_bone_rotation.getRotation());

		if (hand_back_particle_effect != null)
			hand_back_particle_effect.setRotation(hand_back_bone_rotation.getRotation());

		if (torso_particle_effect != null)
			torso_particle_effect.setRotation(torso_bone_rotation.getRotation());

		skeleton.updateWorldTransform();

	}

	@Override
	public boolean isAlive() {
		return health_current > 0;
	}

	/**
	 * Used to flip the creep by target position
	 * <p>
	 * First step: determine angle that might have intrinsic information about how
	 * to flip the creep (that's the case when rotationAngle is outside the range of
	 * 90 to -90).
	 * <p>
	 * Note: The {@link #rotation_angle} is still saved, it can be send via network,
	 * so other clients have also the flipping information.
	 * 
	 * 
	 * @param targetPos, might be a building, character, another creep or the
	 *                   position of the next path vertex.
	 */
	public void flipByTarget(Vector2 targetPos) {

		rotation_angle = Util.getAngleBetweenTwoPoints(pos, targetPos);

		flipByAngle();

	}

	private void flipByAngle() {

		if (rotation_angle <= 90 && rotation_angle > 60)
			rotation_angle = 60;
		else if (rotation_angle < 120 && rotation_angle > 90)
			rotation_angle = 120;
		else if (rotation_angle < -30 && rotation_angle >= -90)
			rotation_angle = -30;
		else if (rotation_angle < -90 && rotation_angle > -150)
			rotation_angle = -150;

		if (rotation_angle < 90 && rotation_angle > -90) {
			if (current_track_1_animation != null)
				current_track_1_animation.flip(false);
			current_track_0_animation.flip(false);
		} else {
			if (current_track_1_animation != null)
				current_track_1_animation.flip(true);
			current_track_0_animation.flip(true);

			rotation_angle = Util.switchAngleUnitCircle(rotation_angle);

		}

	}

	/**
	 * Returns an appropriate {@link GameclassParticleEffect} of
	 * {@link #particle_effects_filtered}. The list
	 * {@link #particle_effects_filtered} is prepared and holds all particle effects
	 * of the chosen gameclass and appropriate skin.
	 * 
	 * This method is used to find the particle effect in
	 * {@link SkeletonRenderer#draw(PolygonSpriteBatch, Skeleton)}
	 * 
	 * @param slot
	 * @return
	 */
	@Override
	public CustomParticleEffect getParticleEffectBySlot(String slot) {
		if (isAlive() == true)
			for (CustomParticleEffect effect : particle_effects_filtered)
				if (effect.getSlotName().equals(slot))
					return effect;
		return null;
	}

	/**
	 * Updates skeleton's skin, particle effects and skill information by meta data
	 * (gameclass and skinIndex)
	 * 
	 */
	public void updateByMetaData(boolean flip) {

		String skinName = "creep_small1";

		if (creep_category == CreepCategory.BIG)
			skinName = "creep_big1";

		skeleton.setSkin(skinName);
		skeleton.setSlotsToSetupPose();
		particle_effects_filtered = ParticleContainer.getCreepParticleEffects(skinName);

		for (CustomParticleEffect effect : particle_effects_filtered)
			effect.setFlipX(flip);

		/*
		 * for flipping more particular I need for each bone a particle effect
		 */
		hand_back_particle_effect = getParticleEffectBySlot("character_hand_back");
		hand_front_particle_effect = getParticleEffectBySlot("character_hand_front");
		torso_particle_effect = getParticleEffectBySlot("character_torso");

		skill_0 = new CreepSkill0(creep_category, null, this, "skin0");

	}

	/**
	 * Note for me: This method doesn't take care whether the member animation
	 * fields are null or not.
	 * 
	 * @param animationName
	 */
	public void setCurrentAnimation(AnimationName animationName) {
		MyAnimation newAnimation = null;

		if (animationName == null)
			return;
		switch (animationName) {
		case IDLE:
			newAnimation = IDLE_ANIMATION;
			break;
		case WALK:
			newAnimation = WALK_ANIMATION;
			if (animation_state.getCurrent(newAnimation.track_index) != null && animation_state
					.getCurrent(newAnimation.track_index).getAnimation() == WALK_ANIMATION.animation) {
				float timeScale = Math.abs(vel.x / Shared.CREEP_MAX_VELOCITY);

				if (timeScale >= 0.2f)
					animation_state.getCurrent(newAnimation.track_index).setTimeScale(timeScale);
				else
					animation_state.getCurrent(newAnimation.track_index).setTimeScale(0.2f);
			}
			break;
		case CREEP_FIGHT_STANCE_ON:
			newAnimation = FIGHT_STANCE_ON_ANIMATION;
			break;
		case CREEP_FIGHT_STANCE_OFF:
			newAnimation = FIGHT_STANCE_OFF_ANIMATION;
			break;
		case P1_CHARGE:
			newAnimation = SKILL_P1_ANIMATION;
			float timeScale = skill_0.getTimeScaleP1();
			// if (newAnimation.animation.getDuration() / timeScale >
			// skill_basic
			// .getCooldownModifiableAbsolute())
			// timeScale = newAnimation.animation.getDuration()
			// / skill_basic.getCooldownModifiableAbsolute();
			newAnimation.time_scale = timeScale;
			break;
		case P3:
			newAnimation = P3_ANIMATION;
			break;
		case STUNNED:
			newAnimation = STUNNED_ANIMATION;
			break;
		case RECEIVE_DAMAGE:
			newAnimation = RECEIVE_DAMAGE_ANIMATION;
			break;
		case COLOR_TOXIC:
			newAnimation = COLOR_TOXIC_ANIMATION;
			break;
		case COLOR_ICE:
			newAnimation = COLOR_ICE_ANIMATION;
			break;
		case COLOR_DEFAULT:
			newAnimation = COLOR_DEFAULT_ANIMATION;
			break;
		case DEATH_EXPLOSION:

			// clear tracks
			animation_state.clearTrack(TRACK_0);
			animation_state.clearTrack(TRACK_1);
			animation_state.clearTrack(TRACK_2);
			newAnimation = DEATH_EXPLOSION_ANIMATION;
			break;
		case ALPHA_FADE_IN:
			newAnimation = ALPHA_FADE_IN_ANIMATION;
			break;
		case ALPHA_FADE_OUT:
			// for example invisibility
			newAnimation = ALPHA_FADE_OUT_ANIMATION;
			break;
		}

		if (newAnimation != null) {

			if (newAnimation.track_index == TRACK_0 && newAnimation != current_track_0_animation) {
				current_track_0_animation = changeAnimationTo(current_track_0_animation, newAnimation);
			} else if (newAnimation.track_index == TRACK_1 && newAnimation != current_track_1_animation) {
				current_track_1_animation = changeAnimationTo(current_track_1_animation, newAnimation);
			} else if (newAnimation.track_index == TRACK_2) {
				current_track_2_animation = changeAnimationTo(current_track_2_animation, newAnimation);
			} else if (newAnimation.track_index == TRACK_3) {
				// track 3, color change for over time effects
				current_track_3_animation = changeAnimationTo(current_track_3_animation, newAnimation);
			} else if (newAnimation.track_index == TRACK_4) {
				// track 3, visibility
				current_track_4_animation = changeAnimationTo(current_track_4_animation, newAnimation);
			}
		}

	}

	/**
	 * Returns the new animation or the old one when some conditions aren't
	 * fulfilled. Override {@link #current_track_0_animation},
	 * {@link #current_track_1_animation}, {@link #current_track_2_animation},
	 * {@link #current_track_3_animation} or {@link #current_track_4_animation} with
	 * the return of this method.
	 * 
	 * @param oldAnimation
	 * @param newAnimation
	 * @return
	 */
	private MyAnimation changeAnimationTo(MyAnimation oldAnimation, MyAnimation newAnimation) {
		int trackIndex = newAnimation.track_index;

		if (oldAnimation != null)
			if (oldAnimation.runThrough(newAnimation) == true)
				if (animation_state.getCurrent(trackIndex) != null)
					if (animation_state.getCurrent(trackIndex).isComplete() == false) {
						return oldAnimation;
					}
		// De morgan (andere schreibweise als oben): überschreibe nur wenn es
		// komplett ist oder wenn es nicht pass through ist.

		// isComplete kann man genau zwischen beendigung und
		// neuanfang fragen, das heißt
		// man hat eine möglichkeit dafür true zu bekommen,
		// danach wird die animation null
		if (newAnimation.isAnimationChangeValid() == true) {

			// reset framecounters
			for (MyAnimation animation : animation_list)
				if (animation != newAnimation)
					animation.frame_counter = 0;

			/*
			 * the timescale of track before has to be reseted to the same timescale as
			 * newAnimation, so the interpolation time between two animations have always
			 * the same length.
			 */
			if (animation_state.getCurrent(newAnimation.track_index) != null)
				animation_state.getCurrent(newAnimation.track_index).setTimeScale(newAnimation.time_scale);
			TrackEntry e = animation_state.setAnimation(newAnimation);

			e.setTimeScale(newAnimation.time_scale);
			newAnimation.begin();
			return newAnimation;
		}

		return oldAnimation;
	}

	@Override
	public CustomSkeleton getSkeleton() {
		return skeleton;
	}

	@Override
	public void fillDeathBonesArray() {
		bones_for_death = new ArrayList<Bone>();
		bones_for_death.add(head_bone_rotation);
		bones_for_death.add(torso_bone_rotation);
		bones_for_death.add(hand_back_bone_rotation);
		bones_for_death.add(hand_front_bone_rotation);
	}

	@Override
	public ArrayList<Bone> getDeathBones() {
		return bones_for_death;
	}

	@Override
	public boolean isFlip() {
		return getHeadFlip();
	}

	@Override
	public boolean isCombo1Stored() {
		return false;
	}

	@Override
	public boolean isCombo2Stored() {
		return false;
	}

	@Override
	public CustomParticleEffect getComboHandFrontParticleEffect() {
		return null;
	}

	@Override
	public CustomParticleEffect getComboHandBackParticleEffect() {
		return null;
	}

	/**
	 * Returns the direction of player's head.
	 * 
	 * @return <code>true</code> = looking to the left, otherwise <code>false</code>
	 */
	public boolean getHeadFlip() {
		return CustomSkeleton.getBoneFlipX(head_bone_flip);
	}

	private void flipBackHandBone(boolean flipX) {
		CustomSkeleton.setBoneFlipX(flipX, hand_back_bone_flip);
		if (hand_back_particle_effect != null)
			hand_back_particle_effect.setFlipX(flipX);
	}

	private void flipFrontHandBone(boolean flipX) {
		CustomSkeleton.setBoneFlipX(flipX, hand_front_bone_flip);
		if (hand_front_particle_effect != null)
			hand_front_particle_effect.setFlipX(flipX);
	}

	private void flipTorsoBone(boolean flipX) {
		CustomSkeleton.setBoneFlipX(flipX, torso_bone_flip);
		if (torso_particle_effect != null)
			torso_particle_effect.setFlipX(flipX);
	}

	private void flipHeadBone(boolean flipX) {
		CustomSkeleton.setBoneFlipX(flipX, head_bone_flip);
	}

	@Override
	public short getLastHealth() {
		return last_health;
	}

	@Override
	public void setLastHealth(short lastHealth) {
		this.last_health = lastHealth;
	}

	@Override
	public DamageDiffbar getDamageDiffBar() {
		return damage_diff_bar;
	}

	private class MyAnimation {
		private Animation animation;
		private boolean loop;

		private int flip_skeleton_when_change = 0, track_index, frame_counter, number_frames_till_change;

		private float time_scale = 1f;

		private float duration_extension;

		/**
		 * Universal animation constructor
		 * <p>
		 * Useful when the skeleton should be flipped in one specific direction at an
		 * animation change.
		 * <p>
		 * It's possible, that the animation changes while A or D is already pressed. So
		 * there will be no flipping until a new pressing. For this case the skeleton
		 * can be flipped with {@link #flip_skeleton_when_change}.
		 * <p>
		 * 
		 * @param animation
		 * @param trackIndex             , determine which track this animation uses
		 * @param loop
		 * @param skeletonFlipWhenChange 0-maintaining regardless of active keys, 1-true
		 *                               (eyes look to the left), 2-false (eyes look the
		 *                               right), 3-skeletonFlipWhenChange is depending
		 *                               on active-keys, if no key is pressed the flip
		 *                               is orientated towards head-flipping
		 * @param numberFramesTillChange , describes how many frames this animation has
		 *                               to wait till it be taken over (bis sie
		 *                               übernommen wird).
		 */
		public MyAnimation(Animation animation, int trackIndex, boolean loop, int skeletonFlipWhenChange,
				int numberFramesTillChange) {

			this.animation = animation;
			this.loop = loop;
			this.track_index = trackIndex;

			this.flip_skeleton_when_change = skeletonFlipWhenChange;
			this.number_frames_till_change = numberFramesTillChange;
			animation_list.add(this);
		}

		/**
		 * Call this method when this animation starts (after a change)
		 */
		public void begin() {
			duration_extension = 0;
			frame_counter = 0;
			switch (flip_skeleton_when_change) {
			case 0:
				// do nothing (no flip)
				break;
			case 1:
				flip(true);
				break;
			case 2:
				flip(false);
				break;
			case 3:
				flip(CustomSkeleton.getBoneFlipX(head_bone_flip));
				break;

			}
		}

		/**
		 * True if the animation has to pass-through the whole time / run until the
		 * animation ends. Default return is false, so the physic dictates which
		 * animation is active. You might override this method if you want a other
		 * behaviour.
		 * 
		 * @param nextAnimation
		 * @return default false, this means that if the physic says "switch animation"
		 *         the animation will switch.
		 */
		public boolean runThrough(MyAnimation nextAnimation) {
			return false;
		}

		public boolean isAnimationChangeValid() {
			if (frame_counter >= number_frames_till_change)
				return true;
			frame_counter++;
			return false;
		}

		/**
		 * Wrapper method. All other flip method will pass this method (bottommost
		 * method of all flip-method). To specify the flip override
		 * {@link #flipSpecific(boolean)}
		 * 
		 * @param flip
		 */
		public void flip(boolean flip) {
			if (isAlive() == true)
				flipSpecific(flip);
		}

		/**
		 * Override this method for more specific flipping!
		 * <p>
		 * Default: flip whole skeleton
		 * 
		 * @param flip
		 */
		public void flipSpecific(boolean flip) {
			flipBackHandBone(flip);
			flipFrontHandBone(flip);
			flipTorsoBone(flip);
			flipHeadBone(flip);

			CustomSkeleton.setBoneFlipX(flip, shoulder_front_flip);
			CustomSkeleton.setBoneFlipX(flip, shoulder_back_flip);

		}

		@Override
		public String toString() {
			if (animation != null)
				return animation.toString();
			return super.toString();
		}
	}

	private class MyAnimationState extends AnimationState {

		public MyAnimationState(AnimationStateData data) {
			super(data);

		}

		public TrackEntry setAnimation(MyAnimation animation) {
			return super.setAnimation(animation.track_index, animation.animation, animation.loop);
		}
	}

	@Override
	public void setPosition(float x, float y) {
		pos.set(x, y);
		skeleton.setPosition(x, y);
	}

	/**
	 * Necessary to get the information whether the quest is done or not. In
	 * {@link AGameclassSelectionModule}
	 * 
	 * @param skin
	 */
	public void setSkin(GameclassSkin skin) {
		gameclass_skin = skin;
	}

	public GameclassSkin getSkin() {
		return gameclass_skin;
	}

	static final float ROTATION_INTERPOLATION_TIME_RANGE = 0.37f;

	public void interpolateRotation(float alpha) {
		// rotation_angle_prev = rotation_angle;
		alpha_rotation_interpolation += alpha;
		if (alpha_rotation_interpolation < ROTATION_INTERPOLATION_TIME_RANGE) {
			rotation_angle_prev = (short) MathUtils.lerp(rotation_angle_prev, rotation_angle_locked,
					alpha_rotation_interpolation / ROTATION_INTERPOLATION_TIME_RANGE);

		} else {
			rotation_angle_locked = rotation_angle;
			alpha_rotation_interpolation = 0;
		}

	}
}
