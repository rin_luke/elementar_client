package com.elementar.logic.creeps;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.physics.box2d.World;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.ACreep.CreepCategory;
import com.elementar.logic.map.pathfinding.LinkedPaths;
import com.elementar.logic.util.Shared;

public class CreepGenerator {

	/**
	 * every x seconds a new creep wave will be created
	 */
	private static final float RATE_WAVE = 15f;

	private static final float RATE_CREEP_PER_WAVE = 0.7f;

	private int current_number_creeps_per_wave = 0;
	private int absolute_number_creeps_per_wave = 0;

	private boolean generating;
	private float accu_wave, accu_creep_generating;
	private LinkedPaths paths;
	private HashMap<Short, OmniCreep> creep_map;
	private ArrayList<OmniCreep> creep_living_list;

	public CreepGenerator(LinkedPaths paths, HashMap<Short, OmniCreep> creepMap,
			ArrayList<OmniCreep> creepLivingList) {
		this.paths = paths;
		creep_map = creepMap;
		creep_living_list = creepLivingList;
	}

	public void generate(World world) {
		accu_wave += Shared.SEND_AND_UPDATE_RATE_WORLD;

		if (generating == false)
			while (accu_wave >= RATE_WAVE) {
				accu_wave -= RATE_WAVE;
				generating = true;
			}
		else {

			accu_creep_generating += Shared.SEND_AND_UPDATE_RATE_WORLD;

			if (accu_creep_generating >= RATE_CREEP_PER_WAVE) {
				accu_creep_generating -= RATE_CREEP_PER_WAVE;
				current_number_creeps_per_wave++;
				if (current_number_creeps_per_wave <= absolute_number_creeps_per_wave) {
					// id will be set automatically for Training case
					MetaCreep metaCreep = new MetaCreep(0, 0);

					CreepCategory creepCategory = CreepCategory.SMALL;

					// is this the last creep?
					if (current_number_creeps_per_wave == absolute_number_creeps_per_wave)
						creepCategory = CreepCategory.BIG;
					// removed paths as parameter
					OmniCreep omniCreep = new OmniCreep(metaCreep, creepCategory,
							Location.CLIENTSIDE_TRAINING);

					omniCreep.init(world);
					creep_living_list.add(omniCreep);
					creep_map.put((short) creep_map.values().size(), omniCreep);
				} else {
					generating = false;
					current_number_creeps_per_wave = 0;
				}

			}

		}

	}

	public void setPaths(LinkedPaths paths) {
		this.paths = paths;
	}

	/**
	 * 
	 * @param number
	 */
	public void setNumberCreepsPerWave(int number) {
		absolute_number_creeps_per_wave = number;
	}

}
