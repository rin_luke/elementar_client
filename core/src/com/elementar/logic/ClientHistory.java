package com.elementar.logic;

import java.util.ArrayList;

import com.elementar.logic.util.lists.ALimitedArrayList;
import com.elementar.logic.util.lists.LIFOLimitedList;

/**
 * Holds all past inputs.
 * 
 * @author lukassongajlo
 * 
 */
public class ClientHistory {

	/**
	 * holds the last id which was send to the server
	 */
	public static int MOST_RECENT_ID = 0;

	private LIFOLimitedList<ClientHistoryEntry> client_history;

	public ClientHistory() {
		client_history = new LIFOLimitedList<ClientHistoryEntry>(10);
	}

	public void add(ClientHistoryEntry entry) {
		MOST_RECENT_ID++;
		entry.setID(MOST_RECENT_ID);

		client_history.add(entry);
	}

	public ClientHistoryEntry getHistoryEntityByID(int id) {
		for (ClientHistoryEntry entry : client_history) {
			if (entry.getID() == id) {
				return entry;
			}
		}
		return null;
	}

	/**
	 * 
	 * @return a list of {@link ClientHistoryEntry}
	 */
	public ALimitedArrayList<ClientHistoryEntry> getHistory() {
		return client_history;
	}

	public void clear() {
		client_history.clear();
	}

	/**
	 * @param ID
	 * @param currentEntry
	 * @return a list with {@link ClientHistoryEntry}s where the entries are
	 *         newer than ID parameter.
	 */
	public ArrayList<ClientHistoryEntry> getHistoryNewerThanID(int ID) {

		LIFOLimitedList<ClientHistoryEntry> result
				= new LIFOLimitedList<ClientHistoryEntry>(client_history.size());

		for (ClientHistoryEntry entry : client_history)
			if (entry.getID() > ID)
				result.add(entry);

		return result;
	}

	@Override
	public String toString() {
		return " size = " + client_history.size() + ", clienthistory as list = "
				+ client_history;
	}

}
