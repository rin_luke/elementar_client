package com.elementar.logic;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.util.PhysicalClientSnapshot;

/**
 * Each history entry consists a non processed input and a position, so the
 * input information before a world-simulation step and the resulting position
 * after that. This class also holds an id to get its order inside the
 * clienthistory.
 * 
 * <p>
 * 
 * Each time I add a ClientHistoryEntry-object to the Clienthistory, I use the
 * copy constructor
 * 
 * @author lukassongajlo
 * 
 */
public class ClientHistoryEntry {

	private int id;

	/**
	 * a copy of the client before a physical simulation step is done.
	 */
	private PhysicalClientSnapshot non_processed_client_snapshot;

	private Vector2 resulting_pos;

	public ClientHistoryEntry() {
	}

	/**
	 * Copy constructor
	 * 
	 * @param entry
	 */
	public ClientHistoryEntry(ClientHistoryEntry entry) {
		id = entry.id;
		non_processed_client_snapshot = new PhysicalClientSnapshot(entry.non_processed_client_snapshot);
		resulting_pos = new Vector2(entry.resulting_pos);
	}

	public void setID(int id) {
		this.id = id;
	}

	public int getID() {
		return id;
	}

	public void setSnapshot(PhysicalClient client) {
		non_processed_client_snapshot = new PhysicalClientSnapshot(client);
	}

	public PhysicalClientSnapshot getSnapshot() {
		return non_processed_client_snapshot;
	}

	public void setResultingPosition(Vector2 position) {
		this.resulting_pos = position;
	}

	public Vector2 getResultingPosition() {
		return resulting_pos;
	}

	@Override
	public String toString() {
		return " [ " + id + ", res = " + resulting_pos + " ] \n\t\t\t\t";
	}

}
