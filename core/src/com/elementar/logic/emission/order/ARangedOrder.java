package com.elementar.logic.emission.order;


public abstract class ARangedOrder {

	protected int last_index;
	
	/**
	 * This method is called after a new projectile is emitted in
	 * {@link AEmissionRanged}. Updates and returns the next section index.
	 * 
	 * @param lastIndex
	 * @param numberSections
	 * @return
	 */
	public abstract int update(int numberSections);

	public abstract ARangedOrder makeCopy();
}
