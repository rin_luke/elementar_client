package com.elementar.logic.emission.order;

public class ReverseOrder extends ARangedOrder {

	private boolean backwards = false;

	@Override
	public int update(int numberSections) {
		if (backwards == false) {
			last_index++;
			if (last_index == numberSections - 1)
				backwards = true;

		} else {
			last_index--;
			if (last_index == 0)
				backwards = false;
		}
		return last_index;
	}

	@Override
	public ARangedOrder makeCopy() {
		return new ReverseOrder();
	}

}
