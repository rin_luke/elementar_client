package com.elementar.logic.emission.order;

import java.util.LinkedList;

import com.elementar.logic.util.MyRandom;

/**
 * The next emitted projectile appears randomly at one of the separated
 * sections.
 * 
 * @author lukassongajlo
 * 
 */
public class RandomOrder extends ARangedOrder {

	protected MyRandom random = new MyRandom();

	private LinkedList<Integer> number_entries;

	@Override
	public int update(int numberSections) {

		// one time event, fill list if null or empty
		if (number_entries == null || number_entries.size() == 0) {
			number_entries = new LinkedList<>();
			for (int i = 0; i < numberSections; i++)
				number_entries.add(i);
		}

		return number_entries.remove(random.random(0, number_entries.size() - 1));

	}

	@Override
	public ARangedOrder makeCopy() {
		return new RandomOrder();
	}

}
