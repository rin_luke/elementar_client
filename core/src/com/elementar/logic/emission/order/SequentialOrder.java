package com.elementar.logic.emission.order;

public class SequentialOrder extends ARangedOrder {

	@Override
	public int update(int numberSections) {
		last_index++;
		if (last_index == numberSections)
			last_index = 0;

		return last_index;
	}

	@Override
	public ARangedOrder makeCopy() {
		return this;
	}

}
