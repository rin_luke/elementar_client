package com.elementar.logic.emission;

import java.util.ArrayList;

import com.elementar.logic.emission.effect.AEffect;

public class EmissionRuntimeData {

	public enum ComboType {
		TRANSFERRING, REGULAR
	}

	private ArrayList<Short> merged_pools;
	private ArrayList<AEffect> merged_effects;

	private ComboType combo_type;

	public EmissionRuntimeData() {
		merged_pools = new ArrayList<>();
		merged_effects = new ArrayList<>();
	}

	private EmissionRuntimeData(EmissionRuntimeData data) {

		merged_pools = new ArrayList<>();
		for (Short s : data.merged_pools)
			merged_pools.add(s);

		merged_effects = new ArrayList<>();
		for (AEffect effect : data.merged_effects)
			merged_effects.add(effect.makeCopy());

		combo_type = data.combo_type;
	}

	/**
	 * Resets {@link #merged_effects}, {@link #merged_pools}
	 */
	public void reset() {
		merged_pools = new ArrayList<>();
		merged_effects = new ArrayList<>();
	}

	public void setMergedEffects(ArrayList<AEffect> mergedEffects) {
		this.merged_effects = mergedEffects;
	}

	public void setComboType(ComboType type) {
		combo_type = type;
	}

	public ComboType getComboType() {
		return combo_type;
	}

	public ArrayList<Short> getMergedPools() {
		return merged_pools;
	}

	public ArrayList<AEffect> getMergedEffects() {
		return merged_effects;
	}

	public EmissionRuntimeData makeCopy() {
		return new EmissionRuntimeData(this);
	}

}
