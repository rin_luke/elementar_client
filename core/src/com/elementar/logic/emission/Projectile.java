package com.elementar.logic.emission;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.joints.DistanceJoint;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.abstracts.APhysicalElement;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.AlignmentBehaviour;
import com.elementar.logic.emission.alignment.CursorFollowingAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;

/**
 * A projectile has always a circle shape as physical representation. To learn
 * more, see also "skill_ingredient_list.pdf" in dropbox
 * 
 * @author lukassongajlo
 * 
 */
public class Projectile extends APhysicalElement {

	public static final float PERSECUTEE_OUT_OF_RANGE = 6f;

	/**
	 * this is the world scale value for the physical shape of DefProjectiles. It
	 * will be multiplied by a multiplier (same value as the for corresponding
	 * particle effects).
	 */
	public static final float SCALE_PROJECTILE_SHAPE = 0.05f;

	private DefProjectile def;

	private SparseProjectile sparse_projectile;

	/**
	 * emission where this projectile is from to get world and the ability to add a
	 * hit player
	 */
	private Emission emission;

	private Fixture fixture;

	private Body body_anchor;
	private DistanceJoint joint_anchor_projectile;

	/**
	 * in milliseconds, depends on collision and/or duration
	 */
	private int life_time;

	private float velocity_with_variation;

	private int angle_beginning;// , vel_sign_by_pursuit = 0;

	private ArrayList<AEmissionDef> active_successors = new ArrayList<AEmissionDef>();

	private IEmitter persecutee;

	/**
	 * if not equals <code>null</code> this projectile touched the detection sensor
	 * of {@link ComboModeSwitchSkill} successfully and is ready to get absorbed by
	 * the characters absorption sensor.
	 */
	private IEmitter absorber;

	/**
	 * in seconds. Will be set when the combiner is known. Enables a constant time
	 * until the projectile arrives to the combiner
	 */
	private float transition_time_to_absorber;

	private Vector2 start_point;

	private Vector2 cursor_pos_at_beginning;

	/**
	 * Creates a prototype. To add properties of that prototype use the fluent
	 * method: new AProjectile().setRootDuration(x).setLifePointsOneTime(y)....
	 * <p>
	 * That constructor won't create any sensor, so the superior constructor is
	 * called with a null world parameter.
	 * 
	 * @param def
	 * @param emission
	 */
	public Projectile(DefProjectile def, Emission emission) {
		super(emission.world);
		this.def = def;
		this.emission = emission;
		this.life_time = getDiminishedLifeTime(def.life_time);
		persecutee = emission.getDef().getPersecutee();
		setAbsorber(emission.getDef().getAbsorber(), emission.getDef().getAbsorbingTransitionTime());
		sparse_projectile = new SparseProjectile(emission.getDef().getPoolID());
		cursor_pos_at_beginning = new Vector2(emission.getEmitter().getCursorPos());

	}

	@Override
	public void initPhysics(float x, float y) {

		boolean hasOrbit = def.orbit != null;

		velocity_with_variation = def.velocity
				+ MathUtils.random(-def.velocity * def.velocity_variation, def.velocity * def.velocity_variation);

		angle_beginning = angle_beginning + (int) (MathUtils.random(-angle_beginning * def.angle_variation,
				angle_beginning * def.angle_variation));

		// start definition of projectile

		body_def.type = def.body_type;
		body_def.allowSleep = false;
		if (hasOrbit) {
			Vector2 posOffset = Util.getPolarCoords(def.orbit.radius_begin, angle_beginning - 90);
			body_def.position.set(x + posOffset.x, y + posOffset.y);
		} else
			body_def.position.set(x, y);

		start_point = new Vector2(body_def.position);

		// body_def.fixedRotation = true;
		body_def.bullet = true;
		body_def.gravityScale = def.gravity_scale;
		// create body
		body = world.createBody(body_def);

		/*
		 * if there's no orbit the directed velocity has to be applied to the
		 * projectile, otherwise it is applied to the anchor body
		 */
		Body bodyVelLinearApplied = null;

		if (hasOrbit) {

			// set bodyDef for anchor body
			body_def.type = BodyType.KinematicBody;
			body_def.position.set(x, y);

			body_anchor = world.createBody(body_def);

			DistanceJointDef jointDef = new DistanceJointDef();
			jointDef.bodyA = body_anchor;
			jointDef.bodyB = body;
			jointDef.collideConnected = false;

			jointDef.length = def.orbit.radius_begin;
			jointDef.frequencyHz = 100f;

			joint_anchor_projectile = (DistanceJoint) world.createJoint(jointDef);

			bodyVelLinearApplied = body_anchor;
		} else {
			bodyVelLinearApplied = body;
		}

		/*
		 * velocity might be diminished
		 */
		setVelocity(bodyVelLinearApplied, getDiminishedVelocity(velocity_with_variation));

		// create sensor
		fixture_def.friction = def.friction; // by default 0
		fixture_def.isSensor = false;
		fixture_def.restitution = def.restitution;

		body.setFixedRotation(true);
		fixture_def.density = 1f;

		Shape shape = createShape();

		fixture_def.shape = shape;

		fixture = body.createFixture(fixture_def);

		if (body_def.type != BodyType.StaticBody)
			fixture.setUserData(this);
		fixture.setFilterData(def.collision_filter);

		// dispose
		shape.dispose();

	}

	private Shape createShape() {

		Shape shape;

		if (def.number_vertices_shape == 0) {
			shape = new CircleShape();
			shape.setRadius(SCALE_PROJECTILE_SHAPE * def.scale_begin);

			Vector2 offset = Util.getPolarCoords(def.offset_shape, angle_beginning);

			((CircleShape) shape).setPosition(offset);
		} else {

			if (def.rectangle_shape != null) {

				float halfWidth = def.rectangle_shape.getWidth() * 0.5f;
				float halfHeight = def.rectangle_shape.getHeight() * 0.5f;
				float[] floatVertices = null;

				if (def.rounded_rect == true) {
					// stone wall (skill1)
					Vector2 leftTop1 = new Vector2(-halfWidth + halfHeight * 0.5f, halfHeight);
					Vector2 leftTop2 = new Vector2(-halfWidth, halfHeight - halfHeight * 0.5f);

					Vector2 leftBot1 = new Vector2(-halfWidth, -halfHeight + halfHeight * 0.5f);
					Vector2 leftBot2 = new Vector2(-halfWidth + halfHeight * 0.5f, -halfHeight);

					Vector2 rightBot1 = new Vector2(halfWidth - halfHeight * 0.5f, -halfHeight);
					Vector2 rightBot2 = new Vector2(halfWidth, -halfHeight + halfHeight * 0.5f);

					Vector2 rightTop1 = new Vector2(halfWidth, halfHeight - halfHeight * 0.5f);
					Vector2 rightTop2 = new Vector2(halfWidth - halfHeight * 0.5f, halfHeight);

					floatVertices = new float[] { leftTop1.x, leftTop1.y, leftTop2.x, leftTop2.y, leftBot1.x,
							leftBot1.y, leftBot2.x, leftBot2.y, rightBot1.x, rightBot1.y, rightBot2.x, rightBot2.y,
							rightTop1.x, rightTop1.y, rightTop2.x, rightTop2.y };

				} else {
					// normal corners
					Vector2 leftTop = new Vector2(-halfWidth, halfHeight);
					Vector2 leftBot = new Vector2(-halfWidth, -halfHeight);
					Vector2 rightBot = new Vector2(halfWidth, -halfHeight);
					Vector2 rightTop = new Vector2(halfWidth, halfHeight);

					floatVertices = new float[] { leftTop.x, leftTop.y, leftBot.x, leftBot.y, rightBot.x, rightBot.y,
							rightTop.x, rightTop.y };
				}

				Polygon polygon = new Polygon(floatVertices);

				polygon.rotate(angle_beginning);
				Vector2 offset = Util.getPolarCoords(def.offset_shape, angle_beginning);
				polygon.setPosition(offset.x, offset.y);

				if (body_def.type == BodyType.StaticBody) {
					shape = new ChainShape();
					((ChainShape) shape).createLoop(polygon.getTransformedVertices());

				} else {

					shape = new PolygonShape();
					((PolygonShape) shape).set(polygon.getTransformedVertices());
				}
			} else {
				float length = SCALE_PROJECTILE_SHAPE * def.scale_begin;
				Vector2[] polygon = createPolygonShape(def.number_vertices_shape, length);

				if (body_def.type == BodyType.StaticBody) {
					shape = new ChainShape();
					((ChainShape) shape).createLoop(polygon);

				} else {

					shape = new PolygonShape();
					((PolygonShape) shape).set(polygon);
				}
			}
		}
		return shape;
	}

	private float getDiminishedVelocity(float origVelocity) {
		float diminishment = def.velocity_diminishing * emission.getEmittedProjectiles().size();
		return origVelocity - diminishment;
	}

	private int getDiminishedLifeTime(int origLifeTime) {
		int diminishment = def.life_time_diminishing * emission.getEmittedProjectiles().size();
		return origLifeTime - diminishment;
	}

	/**
	 * 
	 * @param n,      between 3 and 8
	 * @param length, between midpoint and vertex
	 * @return
	 */
	private Vector2[] createPolygonShape(int numberVertices, float length) {
		Vector2[] vertices = new Vector2[numberVertices];
		float angle = 360f / numberVertices;
		for (int i = 0; i < numberVertices; i++)
			vertices[i] = Util.getPolarCoords(length, i * angle);

		return vertices;
	}

	public void update() {

		if (isAlive() == false)
			return;

		life_time -= Shared.SEND_AND_UPDATE_RATE_IN_MS;

		if (body == null)
			/*
			 * e.g. charge defs with dec = false on server side, see LightningSkill0
			 */
			return;

		// continuous scaling
		if (MathUtils.isEqual(def.scale_begin, def.scale_end) == false && def.scaling_time_ms_current > 0) {

			Shape shape = fixture.getShape();
			if (shape instanceof CircleShape) {
				// linear interpolation for circle shapes
				float scale = MathUtils.lerp(def.scale_begin, def.scale_end,
						1 - ((float) def.scaling_time_ms_current / def.scaling_time_ms_absolute));
				def.scaling_time_ms_current -= Shared.SEND_AND_UPDATE_RATE_IN_MS;

				shape.setRadius(SCALE_PROJECTILE_SHAPE * scale);
			}
		}

		if (def.orbit != null) {

			if (MathUtils.isEqual(def.orbit.radius_begin, def.orbit.radius_end) == false
					&& def.orbit.scaling_time_ms_current > 0) {

				// linear interpolation for radius
				float interpolatedRadius = MathUtils.lerp(def.orbit.radius_begin, def.orbit.radius_end,
						1 - ((float) def.orbit.scaling_time_ms_current / def.orbit.scaling_time_ms_absolute));
				def.orbit.scaling_time_ms_current -= Shared.SEND_AND_UPDATE_RATE_IN_MS;

				joint_anchor_projectile.setLength(interpolatedRadius);

			}

			/*
			 * the following line ensures that the orbit-projectile has a proper velocity
			 * around a flying anchor projectile. It considers the pre-defined angular
			 * velocity of the orbit object
			 */
			body.setLinearVelocity(
					Util.addVectors(body_anchor.getLinearVelocity(), Util.getPolarCoords(def.orbit.angular_vel,
							Util.getAngleBetweenTwoPoints(body_anchor.getPosition(), body.getPosition()) + 90)));
		}

		// local movement of the projectile
		Vector2 velLocal = new Vector2();

		Vector2 velGlobal = new Vector2(body.getLinearVelocity());

		// note: time_until_stop is infinite by default
		if (life_time < def.life_time - def.time_until_stop) {
			velLocal.set(0, 0);
			if (isAttached() == false)
				velGlobal.set(0, 0);
		}

		if (start_point.dst(getPosition()) > def.range_until_death) {
			life_time = 0;
		}

		// else {
		// adapt velocity
		IEmitter persecutee = null;
		if (this.persecutee != null)
			persecutee = this.persecutee;
		else if (def.persecutee != null)
			persecutee = def.persecutee;

		if (persecutee != null) {

			if (persecutee.getBody() != null && persecutee.isAlive() == true) {

				velLocal.set(0, 0);

				velGlobal.set(Util.getPolarCoords(velocity_with_variation,
						Util.getAngleBetweenTwoPoints(getPosition(), persecutee.getPos())));

				float distance = getPosition().dst(persecutee.getPos());

				if (absorber != null) {
					/*
					 * absorber funktionalität beibehalten, aber ohne feste transitiontime, sondern
					 * konstante geschwindigkeit. Unten im 'else' Zweig fliegt das projektil nun an
					 * die letzte Position des Persecutee.
					 */
					/*
					 * if absorber isn't null the projectile should arrive the absorber in a certain
					 * time
					 */
					if (def.getCurvedTrajectory() != null) {
						velGlobal.set(def.getCurvedTrajectory().calcVel(getPosition()));
					}

					if (transition_time_to_absorber > 1000000) {
						// velocity is not affected
					} else {

						transition_time_to_absorber -= Shared.SEND_AND_UPDATE_RATE_WORLD;
						if (transition_time_to_absorber < 0)
							transition_time_to_absorber = 0.1f;

						velGlobal.setLength(distance / transition_time_to_absorber);
					}

				} else {
					if (distance > PERSECUTEE_OUT_OF_RANGE) {
						velGlobal.setLength(0);
						setLifeTime(0);
					}
				}

			} else {
				/*
				 * if the persecutee is no longer alive, kill this projectile
				 */
				setLifeTime(0);
			}

		}

		body.setLinearVelocity(Util.addVectors(velLocal, velGlobal));

		if (emission.getDef().getAlignment().getBehaviour() == AlignmentBehaviour.CURSOR_FOLLOWING) {

			body.setLinearVelocity(0, 0);

			applyLinearImpulseToIdle();

			float distanceCursor = emission.getEmitter().getCursorPos().dst(getPosition());

			float lerpFactor = 1f;
			if (distanceCursor < 0.5f)
				lerpFactor = distanceCursor;

			float impulseValue = ((CursorFollowingAlignment) emission.getDef().getAlignment()).getImpulseValue();
			applyLinearImpulse(Util.getPolarCoords(impulseValue * lerpFactor,
					Util.getAngleBetweenTwoPoints(getPosition(), emission.getEmitter().getCursorPos())));

		}

		if (start_point.dst(getPosition()) > def.range_until_stop)
			body.setLinearVelocity(0, 0);

		if (start_point.dst(cursor_pos_at_beginning) < start_point.dst(getPosition()) && def.stop_until_reached_cursor)
			body.setLinearVelocity(0, 0);

		if (def.death_at_stop == true && MathUtils.isZero(body.getLinearVelocity().len()) == true)
			setLifeTime(0);

		// check start conditions of succ emissions
		if (isAlive() == false) {
			Iterator<AEmissionDef> it = def.getSuccessors().iterator();
			while (it.hasNext() == true) {
				AEmissionDef succDef = it.next();
				/*
				 * add if the emission should start after projectile died
				 */
				if (succDef.getStartConditions().isAfterLifetime() == true) {

					AEmissionDef activeSucc = succDef.makeCopy();
					active_successors.add(activeSucc);

					if (succDef.isAttached() == true)
						activeSucc.setTarget(emission.getDef().getTarget());

				}
			}
		}

		sparse_projectile.pos.set(getPosition());

		if (isAttached() == true) {

			body.setLinearVelocity(0, 0);

			IEmitter attachedCharacter = null;

			// set attached character
			if (def.attachment_option == AttachmentOptionProjectile.AT_TARGET) {
				if (emission.getDef().getTarget() != null && emission.getDef().getTarget().getBody() != null)
					attachedCharacter = emission.getDef().getTarget();
			} else if (def.attachment_option == AttachmentOptionProjectile.AT_EMITTER)
				attachedCharacter = emission.getEmitter();

			// set position and direction based on character
			if (attachedCharacter != null) {
				if (attachedCharacter instanceof PhysicalClient)
					sparse_projectile.connection_id_attached = ((PhysicalClient) attachedCharacter).connection_id;

				if (emission.getDef().getAlignment().getBehaviour() == AlignmentBehaviour.FIXED)
					sparse_projectile.direction = emission.getDef().getAlignment().getAngle();
				else if (emission.getDef().getAlignment().getBehaviour() == AlignmentBehaviour.CURSOR_FIXED)
					sparse_projectile.direction = emission.getDef().getAlignment().getAngle();
				else
					sparse_projectile.direction = (short) attachedCharacter.getBody().getLinearVelocity().angleDeg();

				setPosition(
						attachedCharacter.getPos().x
								+ def.offset_attached * MathUtils.cosDeg(sparse_projectile.direction),
						attachedCharacter.getPos().y
								+ def.offset_attached * MathUtils.sinDeg(sparse_projectile.direction));
			}

			/*
			 * the projectile might be attached to an emission. In this case we have to
			 * consider that the emission might be sticked to a target (player, creep,
			 * building), so if the target dies the projectile has to disappear as well.
			 */
			if (emission.getDef().getTarget() != null && emission.getDef().getTarget().isAlive() == false)
				life_time = 0;

		} else {
			if (MathUtils.isZero(def.velocity) == true)
				sparse_projectile.direction = (short) angle_beginning;
			else if (body.getLinearVelocity().len2() > 0.01)
				/*
				 * set only if the velocity vector is long enough, otherwise we take the
				 * previous direction. If a vector is too short there's no direction anymore.
				 */
				sparse_projectile.direction = (short) (body.getLinearVelocity().angleDeg()
						+ body.getAngle() * MathUtils.radiansToDegrees);
		}
	}

	private boolean isAttached() {
		return def.attachment_option != null;
	}

	/**
	 * Returns a list of successors that is the result of a collision, or if the
	 * life time ended
	 * 
	 * @return
	 */
	public ArrayList<AEmissionDef> getActiveSuccessors() {
		return active_successors;
	}

	public SparseProjectile getSparseRepresentation() {
		return sparse_projectile;
	}

	/**
	 * Sets the persecuted fixture for this specific projectile. If all projectiles
	 * of an emission should follow a fixture, use
	 * {@link DefProjectile#setPersecutee(Fixture)}.
	 * 
	 * @param persecutee
	 */
	public void setPersecutee(IEmitter persecutee) {
		this.persecutee = persecutee;
	}

	public boolean isAlive() {
		return life_time > 0;
	}

	public Emission getEmission() {
		return emission;
	}

	public DefProjectile getDef() {
		return def;
	}

	@Override
	protected Filter getCollisionFilter() {
		return def.getCollisionFilter();
	}

	public void setLifeTime(int lifeTime) {
		life_time = lifeTime;
	}

	/**
	 * @param angle
	 */
	public void setAngleBeginning(int angle) {
		this.angle_beginning = angle;
	}

	public float getAngleBeginning() {
		return angle_beginning;
	}

	/**
	 * Returns the touched character whose combining stance is active or
	 * <code>null</code> when no character was touched. If a character is touched
	 * successfully, this projectile will fly to the character's center and
	 * disappear after touching the absorption sensor.
	 * 
	 * @return
	 */
	public IEmitter getAbsorber() {
		return absorber;
	}

	/**
	 * Will be called after touching the detection sensor of
	 * {@link ComboModeSwitchSkill}. Necessary to ensure that the projectile only
	 * collides with the passed absorber.
	 * 
	 * @param absorber
	 * @param transitionTimeToAbsorber
	 */
	public void setAbsorber(IEmitter absorber, float transitionTimeToAbsorber) {
		this.absorber = absorber;
		transition_time_to_absorber = transitionTimeToAbsorber;
	}

	/**
	 * with {@link #angle_beginning}
	 * 
	 * @param body
	 * @param velocity
	 */
	public void setVelocity(Body body, float velocity) {
		body.setLinearVelocity(Util.getPolarCoords(velocity, angle_beginning));
	}

	public void setGravityScale(float gravity) {
		body.setGravityScale(gravity);
	}

	public int getLifeTime() {
		return life_time;
	}

	@Override
	public void destroy() {
		if (joint_anchor_projectile != null)
			world.destroyJoint(joint_anchor_projectile);

		super.destroy();

		if (body_anchor != null)
			world.destroyBody(body_anchor);

	}

	private void applyLinearImpulseToIdle() {
		applyLinearImpulse(new Vector2(-body.getLinearVelocity().x, -body.getLinearVelocity().y));
	}

	private void applyLinearImpulse(Vector2 impulse) {
		body.applyLinearImpulse(impulse, body.getWorldCenter(), false);
	}

}
