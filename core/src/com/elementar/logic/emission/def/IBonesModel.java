package com.elementar.logic.emission.def;

import java.util.ArrayList;

import com.elementar.data.container.util.CustomParticleEffect;
import com.elementar.gui.util.CustomSkeleton;
import com.esotericsoftware.spine.Bone;

/**
 * Objects of classes that implement this interface have bones resp. an
 * underlying skeleton which is drawn to the screen. We use this interface to
 * combine emissions with bones, so for example the death explosion needs bones
 * to let them fly in all directions. Another example is the charge animation of
 * a character. An emission-particle effect can be sticked to a bone.
 * 
 * @author lukassongajlo
 * 
 */
public interface IBonesModel {

	/**
	 * Use this method to fill the bones array field
	 */
	public void fillDeathBonesArray();

	public ArrayList<Bone> getDeathBones();

	public CustomSkeleton getSkeleton();

	public boolean isFlip();

	public boolean isCombo1Stored();

	public boolean isCombo2Stored();

	public CustomParticleEffect getParticleEffectBySlot(String slot);

	public CustomParticleEffect getComboHandFrontParticleEffect();

	public CustomParticleEffect getComboHandBackParticleEffect();

}
