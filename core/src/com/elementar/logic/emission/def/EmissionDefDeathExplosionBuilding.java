package com.elementar.logic.emission.def;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.util.Util;
import com.esotericsoftware.spine.Bone;
import com.esotericsoftware.spine.Skeleton;

public class EmissionDefDeathExplosionBuilding extends AEmissionDef {

	private int bone_index = 0;

	/**
	 * 
	 * @param internalPath
	 * @param scaleRadiusHitbox
	 * @param scaleParticleEffect
	 * @param drawBehind
	 * @param prototype
	 * @param projectileAmount
	 * @param duration
	 */
	public EmissionDefDeathExplosionBuilding(String internalPath, float scaleRadiusHitbox,
			float scaleParticleEffect, boolean drawBehind, DefProjectile prototype,
			int projectileAmount, int duration) {
		super(internalPath, scaleRadiusHitbox, scaleParticleEffect, drawBehind, prototype,
				projectileAmount, duration, new FixedAlignment(0));
	}

	public EmissionDefDeathExplosionBuilding(AEmissionDef def) {
		super(def);
	}

	@Override
	public <T extends AEmissionDef> T makeCopy() {
		return (T) new EmissionDefDeathExplosionBuilding(this);
	}

	@Override
	public void positionProjectile(Projectile emittedProjectile, Emission emission) {
		Bone correspondingBone = bones_model.getDeathBones().get(bone_index);
		Skeleton skeleton = correspondingBone.getSkeleton();

		Vector2 rotatedPos = new Vector2(correspondingBone.getX(), correspondingBone.getY());

		rotatedPos.rotate(skeleton.getRootBone().getRotation());

		Vector2 worldBonePos = new Vector2(skeleton.getX() + rotatedPos.x,
				skeleton.getY() + rotatedPos.y);

		emittedProjectile.setAngleBeginning(
				Util.getAngleBetweenTwoPoints(emission.getCenterPos(), worldBonePos));
		emittedProjectile.initPhysics(worldBonePos.x, worldBonePos.y);

		bone_index++;
	}

	@Override
	public void update(Emission emission) {
		if (bones_model != null) {
			Projectile projectile;
			Bone bone;
			for (int i = 0; i < emission.getEmittedProjectiles().size(); i++) {
				projectile = emission.getEmittedProjectiles().get(i);
				bone = bones_model.getDeathBones().get(i);

				// set bone position to projectile position
				Vector2 pos = new Vector2((projectile.getPosition().x - bone.getSkeleton().getX()),
						projectile.getPosition().y - bone.getSkeleton().getY())
								.rotate(-bone.getSkeleton().getRootBone().getRotation());
				bone.setPosition(pos.x, pos.y);
				// rotation
				bone.setRotation(projectile.getBody().getAngle() * MathUtils.radiansToDegrees);
			}
		}
	}
}
