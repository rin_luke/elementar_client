package com.elementar.logic.emission.def;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.alignment.AAlignment;
import com.elementar.logic.emission.order.ARangedOrder;

public class EmissionDefLineRanged extends AEmissionDefRanged {
	private Vector2 line = new Vector2(1, 0);
	private Vector2 current_line_point = new Vector2(1, 0);

	public EmissionDefLineRanged(String internalPath, float scaleRadiusHitbox,
			float scaleParticleEffect, boolean drawBehind, DefProjectile prototype,
			int projectileAmount, int duration, float range, int numberSections,
			ARangedOrder order, AAlignment alignment) {
		super(internalPath, scaleRadiusHitbox, scaleParticleEffect, drawBehind, prototype,
				projectileAmount, duration, range, numberSections, order, alignment);
	}

	public EmissionDefLineRanged(EmissionDefLineRanged def) {
		super(def);
	}

	@Override
	public void positionProjectile(Projectile emittedProjectile, Emission emission) {

		line.set(current_section * MathUtils.cosDeg(alignment.getAngle() + 90),
				current_section * MathUtils.sinDeg(alignment.getAngle() + 90));

		current_line_point.set(x_emission, y_emission).add(line);

		emittedProjectile.initPhysics(current_line_point.x, current_line_point.y);
	}

	@Override
	public <T extends AEmissionDef> T makeCopy() {
		return (T) new EmissionDefLineRanged(this);
	}
}
