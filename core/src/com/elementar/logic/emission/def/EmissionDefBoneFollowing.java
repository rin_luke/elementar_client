package com.elementar.logic.emission.def;

import com.badlogic.gdx.math.MathUtils;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.esotericsoftware.spine.Bone;
import com.esotericsoftware.spine.Skeleton;

public class EmissionDefBoneFollowing extends EmissionDefAngleDirected {

	private String bone_name;

	private Bone corresponding_bone;
	private Bone corresponding_bone_rotation;

	public EmissionDefBoneFollowing(String internalPath, float scaleRadiusHitbox, float scaleParticleEffect,
			boolean drawBehind, DefProjectile prototype, String boneName) {
		// amount projectiles = 1, duration = 0 ms, drawBehind = false
		super(internalPath, scaleRadiusHitbox, scaleParticleEffect, drawBehind, prototype, 1, 0, new FixedAlignment(0));
		bone_name = boneName;

	}

	public EmissionDefBoneFollowing(EmissionDefBoneFollowing def) {
		super(def);
		bone_name = def.bone_name;
	}

	@Override
	public <T extends AEmissionDef> T makeCopy() {
		return (T) new EmissionDefBoneFollowing(this);
	}

	@Override
	public void positionProjectile(Projectile emittedProjectile, Emission emission) {
		if (bones_model != null) {

			Skeleton skeleton = bones_model.getSkeleton();
			corresponding_bone = skeleton.findBone(bone_name);
			corresponding_bone_rotation = skeleton.findBone(bone_name + "_rotation");

			// hack
			if (corresponding_bone_rotation == null) {
				corresponding_bone_rotation = skeleton.findBone("character_hand_front_rotation");
			}

			if (corresponding_bone != null)
				emittedProjectile.initPhysics(corresponding_bone.getWorldX(), corresponding_bone.getWorldY());
		}
	}

	@Override
	public void update(Emission emission) {

		if (corresponding_bone == null)
			return;

		/*
		 * status bericht: wenn der skill beim serverdude nachvollzogen wird, sieht es
		 * toll aus, aber das passiert irgendwie nicht immer, nochmal genau untersuchen
		 * was passiert.
		 */

		if (emission.getEmittedProjectiles().size() > 0) {
			Projectile projectile = emission.getEmittedProjectiles().get(0);

			/*
			 * set projectile (aka particle effect) position to bone position
			 */
			projectile.setPosition(corresponding_bone.getWorldX(), corresponding_bone.getWorldY());

			/*
			 * rotation, note: because velocity is 0, we can use the angle_beginning here
			 */
			projectile
					.setAngleBeginning((int) (corresponding_bone_rotation.getRotation() * MathUtils.radiansToDegrees));

		}
	}

}
