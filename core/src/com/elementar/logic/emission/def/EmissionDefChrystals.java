package com.elementar.logic.emission.def;

import com.badlogic.gdx.math.MathUtils;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.order.SequentialOrder;

public class EmissionDefChrystals extends EmissionDefAngleRanged {

	private static final float TIME_TIL_STASIS_MS = 300f;
	private static final float TIME_TIL_PURSUIT_MS = 300f;

	private static final int BEGIN_COLLECTION_TIME_VARIATION = 100,
			END_COLLECTION_TIME_VARIATION = 200;

	private static final float TRANSITION_TIME = 0.7f;

	private static final int CHRYSTAL_AMOUNT = 3;

	private float collection_time_offset;
	private boolean flag_after_explosion = false;

	public EmissionDefChrystals(String internalPath, DefProjectile prototype) {
		super(internalPath, 1, 0.5f, false, prototype, CHRYSTAL_AMOUNT, 0, 360, CHRYSTAL_AMOUNT,
				new SequentialOrder(), new FixedAlignment(0));
	}

	public EmissionDefChrystals(EmissionDefChrystals def) {
		super(def);
	}

	@Override
	public void update(Emission emission) {

		if (emission.getElapsedTimeMS() > TIME_TIL_STASIS_MS && flag_after_explosion == false) {
			flag_after_explosion = true;
			for (Projectile projectile : emission.getEmittedProjectiles()) {
				projectile.getBody().setLinearVelocity(0, 0);
			}
		}
		if (emission.getElapsedTimeMS() > TIME_TIL_PURSUIT_MS + collection_time_offset) {
			collection_time_offset += MathUtils.random(BEGIN_COLLECTION_TIME_VARIATION,
					END_COLLECTION_TIME_VARIATION);
			for (Projectile projectile : emission.getEmittedProjectiles()) {
				if (projectile.getAbsorber() == null) {
					projectile.setAbsorber(emission.getEmitter().getSlayer(), TRANSITION_TIME);
					projectile.setPersecutee(emission.getEmitter().getSlayer());
					break;
				}
			}
		}
	}

	@Override
	public <T extends AEmissionDef> T makeCopy() {
		return (T) new EmissionDefChrystals(this);
	}

}
