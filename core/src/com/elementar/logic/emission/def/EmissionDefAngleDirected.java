package com.elementar.logic.emission.def;

import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.alignment.AAlignment;

public class EmissionDefAngleDirected extends AEmissionDef {

	/**
	 * 
	 * @param internalPath
	 * @param scaleRadiusHitbox
	 * @param scaleParticleEffect
	 * @param drawBehind
	 * @param prototype
	 * @param projectileAmount
	 * @param durationMS
	 * @param alignment
	 */
	public EmissionDefAngleDirected(String internalPath, float scaleRadiusHitbox,
			float scaleParticleEffect, boolean drawBehind, DefProjectile prototype,
			int projectileAmount, int durationMS, AAlignment alignment) {
		super(internalPath, scaleRadiusHitbox, scaleParticleEffect, drawBehind, prototype,
				projectileAmount, durationMS, alignment);
	}

	public EmissionDefAngleDirected(EmissionDefAngleDirected def) {
		super(def);
	}

	@Override
	public void positionProjectile(Projectile emittedProjectile, Emission emission) {
		emittedProjectile.initPhysics(x_emission, y_emission);
	}

	@Override
	public <T extends AEmissionDef> T makeCopy() {
		return (T) new EmissionDefAngleDirected(this);
	}

}
