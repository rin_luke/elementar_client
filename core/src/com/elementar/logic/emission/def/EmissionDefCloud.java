package com.elementar.logic.emission.def;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.order.SequentialOrder;

public class EmissionDefCloud extends AEmissionDefRanged {

	private ArrayList<Vector2> next_positions;

	/**
	 * 
	 * @param internalPath
	 * @param scaleRadiusHitbox
	 * @param scaleParticleEffect
	 * @param drawBehind
	 * @param prototype
	 * @param midlineSize,
	 *            valid values = 3,5,7,...
	 * @param triangleEdgeLength
	 */
	public EmissionDefCloud(String internalPath, float scaleRadiusHitbox, float scaleParticleEffect, boolean drawBehind,
			DefProjectile prototype, int midlineAmount, float triangleEdgeLength) {
		/*
		 * projectileAmount = n(n+1)-n-2, where n = number of midline
		 * projectiles
		 */
		super(internalPath, scaleRadiusHitbox, scaleParticleEffect, drawBehind, prototype,
				calcProjAmount(midlineAmount), 0, 0, 0, new SequentialOrder(), new ObstacleAlignment());

		next_positions = new ArrayList<>(getProjectileAmount());

		// height equilateral triangle = sqrt (3/4 * x*x)
		float triangleHeight = (float) Math.sqrt(0.75f * (triangleEdgeLength * triangleEdgeLength));
		float layerHeight = triangleHeight;
		int amountOneSide = (-midlineAmount + getProjectileAmount()) / 2;
		int amountLayer = midlineAmount - 1;

		while (amountOneSide > 1) {

			float halfWidth = triangleEdgeLength * (amountLayer - 1) * 0.5f;

			for (int i = 0; i < amountLayer; i++)
				next_positions.add(new Vector2(-halfWidth + i * triangleEdgeLength, layerHeight));

			amountOneSide -= amountLayer;
			amountLayer -= 1;
			layerHeight += triangleHeight;

		}

		int amountBothSides = (-midlineAmount + getProjectileAmount());
		Vector2 mirroredPos;
		for (int i = next_positions.size(), j = 0; i < amountBothSides; i++, j++) {
			mirroredPos = new Vector2(next_positions.get(j));
			mirroredPos.y *= -1;
			next_positions.add(mirroredPos);
		}

		// midline positions
		float halfWidth = triangleEdgeLength * (midlineAmount - 1) * 0.5f;
		for (int i = 0; i < midlineAmount; i++)
			next_positions.add(new Vector2(-halfWidth + i * triangleEdgeLength, 0));

	}

	private static int calcProjAmount(int midlineAmount) {
		int result = 0;

		// calc one side
		int rowsOneSide = midlineAmount / 2;
		for (int i = 1; i < rowsOneSide + 1; i++)
			result += midlineAmount - i;

		// add other side
		result *= 2;

		// add midline
		result += midlineAmount;

		return result;
	}

	public EmissionDefCloud(EmissionDefCloud def) {
		super(def);
		next_positions = new ArrayList<>();
		for (Vector2 position : def.next_positions)
			next_positions.add(new Vector2(position));
	}

	@Override
	public <T extends AEmissionDef> T makeCopy() {
		return (T) new EmissionDefCloud(this);
	}

	@Override
	public void positionProjectile(Projectile emittedProjectile, Emission emission) {

		Vector2 nextPos = next_positions.get(last_index);

		emittedProjectile.setAngleBeginning((int) nextPos.angle());
		// emittedProjectile.getDef().setTimeUntilStop(nextPos.len());

		emittedProjectile.initPhysics(x_emission, y_emission);

	}

}
