package com.elementar.logic.emission.def;

import java.util.ArrayList;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.elementar.data.container.AudioData;
import com.elementar.data.container.GameclassContainer;
import com.elementar.data.container.ParticleContainer;
import com.elementar.data.container.util.Gameclass;
import com.elementar.data.container.util.TweenableSound;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.logic.characters.skills.ASkill;
import com.elementar.logic.characters.skills.ice.IceSkill0;
import com.elementar.logic.characters.skills.lightning.LightningSkill2;
import com.elementar.logic.characters.skills.toxic.ToxicSkill0;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.EmissionRuntimeData;
import com.elementar.logic.emission.IEmitter;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.SuccessorTimewise;
import com.elementar.logic.emission.Trigger;
import com.elementar.logic.emission.alignment.AAlignment;
import com.elementar.logic.emission.alignment.AlignmentBehaviour;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.effect.AEffect;

public abstract class AEmissionDef {

	public enum TargetType {
		ALLY, ENEMY
	}

	private String internal_path;

	private TargetType target_type;

	private short pool_id;

	/**
	 * Holds all successor emissions, which trigger based on a pre-declared time
	 */
	protected ArrayList<SuccessorTimewise> successors_time_wise = new ArrayList<SuccessorTimewise>();

	private DefProjectile projectile_prototype;

	protected float offset = 0f;

	/**
	 * Depends on {@link #offset} (radius) and position ({@link #temp_x_center},
	 * {@link #temp_y_center})
	 */
	protected float x_emission, y_emission;

	/**
	 * If projectile amount is 0 the emission has ended.
	 * <p>
	 * Projectiles are emitted in consistent time intervals depending on duration.
	 * Each projectile emits exactly on the end of the calculated
	 * duration/projectile_amount ratio.
	 */
	private int projectile_amount, remaining_projectile_amount;
	/**
	 * Constant. In ms.
	 * <p>
	 * projectiles are emitted in consistent time intervals depending on duration.
	 * Each projectile emits exactly on the end of the calculated
	 * duration/projectile_amount ratio.
	 */
	protected int duration_ms;

	// needed here for absorbing chrystals
	protected IEmitter absorber, persecutee;

	protected int delay_start_ms = 0;

	/**
	 * {@link #duration_ms}/{@link #projectile_amount}. Just set once at the
	 * beginning (wihtin the constructor)
	 */
	private int time_per_projectiles;

	protected AAlignment alignment;

	protected ArrayList<AEffect> effects = new ArrayList<AEffect>();

	/**
	 * for successor emissions
	 */
	private StartConditions start_conditions = new StartConditions();

	private TweenableSound sound;

	/**
	 * <code>true</code> by default. All Emissions will normally be sent with a
	 * {@link Trigger} object. There are cases, e.g. {@link LightningSkill2}
	 * charge-def where the emission is initiated on both side. If a trigger would
	 * be transmitted the emission would have been activated twice. In this case we
	 * don't want to send any trigger for this particular emission.
	 */
	private boolean transmissible = true;

	/**
	 * flag for in-pipe injections.
	 */
	private boolean set_persecutee_after_collision;

	protected Vector2 center_pos;

	protected float absorbing_transition_time;

	/**
	 * Optional, by default equals <code>null</code>
	 */
	protected IBonesModel bones_model;

	/**
	 * Optional, by default equals <code>false</code>
	 */
	protected boolean attached = false;

	/**
	 * A list of units (players, creeps, buildings) hit by projectiles of this
	 * emission. With this list we can avoid multiple collision. By default
	 * <code>null</code>
	 */
	protected ArrayList<IEmitter> last_hit_list = null;

	/**
	 * An emission might has a target, regardless whether this emission has effects
	 * or not. That's useful in case of effect emissions. Those haven't an own
	 * effect but a target. So we can use the target for position the emission
	 */
	private IEmitter target;

	/**
	 * <code>true</code> by default. If this emission def is a successor and this
	 * flag is set to <code>true</code> the previous projectile will disappear after
	 * ground collision.
	 */
	private boolean destroy_prev_projectile_after_ground_collision = true;

	/**
	 * <code>false</code> by default. If this emission def is a successor and this
	 * flag is set to <code>true</code> the previous projectile will disappear after
	 * player collision.
	 */
	private boolean destroy_prev_projectile_after_player_collision = false;

	/**
	 * <code>true</code> by default. If <code>true</code> the emission gets removed
	 * from the successor list, otherwise it stays in the list.
	 */
	private boolean remove_from_succlist_after_ground_collision = true;

	private ParticleEffectCategory particle_effect_category = ParticleEffectCategory.PROJECTILE;

	private boolean trigger_emission_allowed = false;

	private boolean rotatable = true;

	private boolean is_feedback = false;

	private boolean nirvana_projectile = false;

	/**
	 * velocity of previous projectile before this emission occurs. This field is
	 * run-time data.
	 */
	private Vector2 prev_proj_vel;

	private EmissionRuntimeData runtime_data;

	private float[] color_gameclass;

	public enum ParticleEffectCategory {
		/**
		 * Default. Projectile particle effects disappear by fading them out.
		 */
		PROJECTILE,

		/**
		 * Not the default category. Movement particle effects disappear more naturally
		 * by setting parameter "number particles emitted per second" to 0.
		 */
		MOVEMENT
	}

	/**
	 * 
	 * @param internalPath
	 * @param scaleHitbox
	 * @param scaleParticleEffect
	 * @param drawBehind
	 * @param prototype
	 * @param projectileAmount
	 * @param durationInMS
	 * @param alignment
	 */
	public AEmissionDef(String internalPath, float scaleHitbox, float scaleParticleEffect, boolean drawBehind,
			DefProjectile prototype, int projectileAmount, int durationInMS, AAlignment alignment) {

		// find color_gameclass if possible by parsing the internal path
		if (internalPath != null) {
			String string[] = internalPath.split("/");
			String pFile = string[string.length - 1];
			if (pFile != null) {
				String string2[] = pFile.split("_");
				if (string2.length > 0) {
					String gameclassName = string2[0].toUpperCase();

					for (GameclassName name : GameclassName.values()) {
						if (name.toString().equals(gameclassName) == true) {

							Gameclass gameclass = GameclassContainer.filterByName(name);

							if (gameclass != null)
								color_gameclass = gameclass.getColor();
							break;
						}
					}

				}
			}
		}
		internal_path = internalPath;
		projectile_prototype = prototype;
		projectile_prototype.scale_begin = scaleHitbox;

		/*
		 * scale_end is -1 by default. If it is negative it should be have the same
		 * value as scale_begin, otherwise an interpolation will occur
		 */
		if (projectile_prototype.scale_end < 0)
			projectile_prototype.scale_end = scaleHitbox;

		projectile_amount = remaining_projectile_amount = projectileAmount;
		duration_ms = durationInMS;
		time_per_projectiles = durationInMS / projectileAmount;
		this.alignment = alignment;

		pool_id = ParticleContainer.setEmissionPool(this, internalPath, scaleParticleEffect, drawBehind);

		runtime_data = new EmissionRuntimeData();
	}

	public AEmissionDef(AEmissionDef def) {
		for (SuccessorTimewise succ : def.successors_time_wise)
			successors_time_wise.add(new SuccessorTimewise(succ.getDef().makeCopy(), succ.getElapsedTimeTilTrigger()));

		projectile_prototype = new DefProjectile(def.projectile_prototype);

		center_pos = def.center_pos;

		delay_start_ms = def.delay_start_ms;

		internal_path = def.internal_path;

		pool_id = def.pool_id;

		particle_effect_category = def.particle_effect_category;

		start_conditions = def.start_conditions;
		projectile_amount = def.projectile_amount;
		remaining_projectile_amount = def.remaining_projectile_amount;
		duration_ms = def.duration_ms;
		time_per_projectiles = def.time_per_projectiles;

		/*
		 * some skills use emissions with pre-set null alignments. Because triggers
		 * assume a "complete"-set emissionDef we get a nullpointer exeception here.
		 */
		if (def.alignment != null)
			alignment = def.alignment.makeCopy();
		offset = def.offset;
		sound = def.sound;
		attached = def.attached;
		bones_model = def.bones_model;

		target = def.target;
		persecutee = def.persecutee;
		absorber = def.absorber;
		absorbing_transition_time = def.absorbing_transition_time;
		// copy effects
		for (AEffect defEffect : def.effects)
			effects.add(defEffect.makeCopy());

		if (def.last_hit_list != null)
			last_hit_list = new ArrayList<>();

		set_persecutee_after_collision = def.set_persecutee_after_collision;

		destroy_prev_projectile_after_ground_collision = def.destroy_prev_projectile_after_ground_collision;
		destroy_prev_projectile_after_player_collision = def.destroy_prev_projectile_after_player_collision;

		remove_from_succlist_after_ground_collision = def.remove_from_succlist_after_ground_collision;

		transmissible = def.transmissible;

		trigger_emission_allowed = def.trigger_emission_allowed;

		rotatable = def.rotatable;

		target_type = def.target_type;

		is_feedback = def.is_feedback;

		nirvana_projectile = def.nirvana_projectile;

		if (def.prev_proj_vel != null)
			prev_proj_vel = new Vector2(def.prev_proj_vel);

		runtime_data = def.runtime_data.makeCopy();

		color_gameclass = def.color_gameclass;
	}

	/**
	 * Calls the copy constructor of the specific subclass.
	 * 
	 * @return
	 */
	public abstract <T extends AEmissionDef> T makeCopy();

	/**
	 * With {@link Projectile#initPhysics(float, float)} you can create an physical
	 * body with an initial velocity vector depending on pre-defined velocity amount
	 * and an angle.
	 * <p>
	 * You are responsible to set this angle with
	 * {@link Projectile#setAngleBeginning(int)}
	 * 
	 * @param emittedProjectile
	 * @param emission          , just for
	 *                          {@link EmissionDefDeathExplosionCharacter}
	 */
	public abstract void positionProjectile(Projectile emittedProjectile, Emission emission);

	/**
	 * Emits a new projectile. Depending on emission subclass the start position
	 * might be different. The emission based on {@link #projectile_prototype}.
	 * 
	 * @param emission
	 * 
	 * @return
	 */
	public Projectile emit(short anglePlayerCursor, Emission emission) {
		// create new projectile
		Projectile emittedProjectile = new Projectile(projectile_prototype, emission);

		remaining_projectile_amount--;

		if (alignment.getBehaviour() == AlignmentBehaviour.CURSOR_FOLLOWING_WHILE_EMITTING)
			// change angle dynamically by new cursor pos
			alignment.setAngle(anglePlayerCursor);

		emittedProjectile.setAngleBeginning(alignment.getAngle());

		x_emission = emission.getCenterPos().x + offset * MathUtils.cosDeg(emittedProjectile.getAngleBeginning());
		y_emission = emission.getCenterPos().y + offset * MathUtils.sinDeg(emittedProjectile.getAngleBeginning());

		if (nirvana_projectile == true) {
			x_emission = Float.MAX_VALUE;
			emittedProjectile.setLifeTime(0);
		}

		// position depends on emission type
		positionProjectile(emittedProjectile, emission);

		return emittedProjectile;
	}

	public EmissionRuntimeData getRuntimeData() {
		return runtime_data;
	}

	public short getPoolID() {
		return pool_id;
	}

	/**
	 * 
	 * @param elapsedTimeTilTrigger , in percentage of the ancestor emission
	 *                              duration. If the value is 0.0f the successor
	 *                              will start immediately resp. parallel. 1.0f
	 *                              means the successor's beginning is when the
	 *                              ancestor ends.
	 * @param successorDef
	 * @return
	 */
	public <T extends AEmissionDef> T addSuccessorTimewise(float elapsedTimeTilTrigger, AEmissionDef successorDef) {
		successors_time_wise.add(new SuccessorTimewise(successorDef, elapsedTimeTilTrigger));
		return (T) this;
	}

	public ArrayList<SuccessorTimewise> getSuccessorsTimewise() {
		return successors_time_wise;
	}

	public DefProjectile getDefProjectile() {
		return projectile_prototype;
	}

	public AAlignment getAlignment() {
		return alignment;
	}

	public void setAlignment(AAlignment alignment) {
		this.alignment = alignment;
	}

	public String getInternalPath() {
		return internal_path;
	}

	public String getInternalPathParsed() {
		if (internal_path == null)
			return "";
		String[] arr = internal_path.split("/");
		if (arr.length == 0)
			return "";
		return arr[arr.length - 1];
	}

	public int getDuration() {
		return duration_ms;
	}

	public int getTimePerProjectile() {
		return time_per_projectiles;
	}

	public int getProjectileAmount() {
		return projectile_amount;
	}

	public int getRemainingProjectileAmount() {
		return remaining_projectile_amount;
	}

	public ParticleEffectCategory getParticleEffectCategory() {
		return particle_effect_category;
	}

	public boolean isRotatable() {
		return rotatable;
	}

	public boolean isFeedback() {
		return is_feedback;
	}

	public TargetType getTargetType() {
		return target_type;
	}

	public void setTargetType(TargetType targetType) {
		this.target_type = targetType;
	}

	/**
	 * {@link #prev_proj_vel} run time data.
	 * 
	 * @return
	 */
	public Vector2 getPrevProjVel() {
		return prev_proj_vel;
	}

	public void setPrevProjVel(Vector2 vel) {
		this.prev_proj_vel = vel;
	}

	public <T extends AEmissionDef> T addEffect(AEffect effect) {

		effects.add(effect);

		if (internal_path.contains("feedback") == true && effects.size() > 0) {

			this.is_feedback = true;

			if (internal_path.contains("ally") == true)
				target_type = TargetType.ALLY;
			else if (internal_path.contains("enemy") == true)
				target_type = TargetType.ENEMY;

			this.alignment = new FixedAlignment(0);
			this.projectile_prototype.setCollisionFilter(ASkill.getNeutralFilter());

		}

		return (T) this;
	}

	public <T extends AEmissionDef> T setParticleEffectCategory(ParticleEffectCategory effectCategory) {
		this.particle_effect_category = effectCategory;
		return (T) this;
	}

	/**
	 * Some particle effects have issues with rotation because of an offset set in
	 * the particle editor (e.g. waterghost). This is admittedly a quick fix but we
	 * can prevent visual misbehaviour by setting {@link #rotatable} to
	 * <code>false</code>.
	 * 
	 * @param <T>
	 * @param rotatable
	 * @return
	 */
	public <T extends AEmissionDef> T setRotatable(boolean rotatable) {
		this.rotatable = rotatable;
		return (T) this;
	}

	/**
	 * By default equals <code>null</code>. Optional.
	 * 
	 * @param sound , from {@link AudioData}
	 * @return {@code this} for chaining
	 */
	public <T extends AEmissionDef> T setSound(Sound sound) {
		this.sound = new TweenableSound(sound, true);
		return (T) this;
	}

	/**
	 * 
	 * @param sound         , from {@link AudioData}, <code>null</code> by default.
	 * @param interruptible , <code>false</code> by default.
	 * @return {@code this} for chaining
	 */
	public <T extends AEmissionDef> T setSound(Sound sound, boolean interruptible) {
		this.sound = new TweenableSound(sound, true);
		this.sound.setInterruptible(interruptible);
		return (T) this;
	}

	/**
	 * Call this method to allow the actual emission. The effects of triggers will
	 * be activated either way. This method handles only the projectile output of an
	 * emission.
	 * 
	 * @param <T>
	 * @return
	 */
	public <T extends AEmissionDef> T setTriggerEmissionAllowed() {
		trigger_emission_allowed = true;
		return (T) this;
	}

	public boolean isTriggerEmissionAllowed() {
		return trigger_emission_allowed;
	}

	/**
	 * Calling this method enables that a player can only be touched by one
	 * projectile of an emission.
	 * 
	 * @return
	 */
	public <T extends AEmissionDef> T setOneTouchMechanism() {
		last_hit_list = new ArrayList<>();
		return (T) this;
	}

	/**
	 * for successor emissions. Some emissions shouldn't appear after the projectile
	 * is dead. With begin conditions you can specify in which cases the successor
	 * emission is allowed to start.
	 * 
	 * @param conditions
	 * @return {@code this} for chaining
	 */
	public <T extends AEmissionDef> T setStartConditions(StartConditions conditions) {
		this.start_conditions = conditions;
		return (T) this;
	}

	public StartConditions getStartConditions() {
		return start_conditions;
	}

	/**
	 * Optional, by default equals 0f. Sets the distance (radius) between center
	 * position and the actual emission. Imagine a circle around the center position
	 * where the projectile emits.
	 * 
	 * @param offset
	 * @return {@code this} for chaining
	 */
	public <T extends AEmissionDef> T setOffset(float offset) {
		this.offset = offset;
		return (T) this;
	}

	/**
	 * If the emission should be attached to a player call this method, it turns
	 * {@link #attached} into <code>true</code>. It's <code>false</code> by default.
	 * Can be used to carry a target to the next emission. See {@link ToxicSkill0}
	 * for example.
	 * 
	 * @param <T>
	 * @return
	 */
	public <T extends AEmissionDef> T setAttached() {
		this.attached = true;
		return (T) this;
	}

	/**
	 * 
	 * Usually used for charge emission definitions. We don't want to send them over
	 * the network by using triggers.
	 * 
	 * @param <T>
	 * @param transmissible
	 * @return {@code this} for chaining
	 */
	public <T extends AEmissionDef> T setTransmissible(boolean transmissible) {
		this.transmissible = transmissible;
		return (T) this;
	}

	/**
	 * 
	 * @param <T>
	 * @param delayStartMS
	 * @return
	 */
	public <T extends AEmissionDef> T setDelayStart(int delayStartMS) {
		this.delay_start_ms = delayStartMS;
		return (T) this;
	}

	public int getDelayStart() {
		return delay_start_ms;
	}

	public boolean isTransmissible() {
		return transmissible;
	}

	/**
	 * 
	 * @param target
	 */
	public void setTarget(IEmitter target) {
		this.target = target;
		for (AEffect effect : effects)
			effect.setTarget(target);
	}

	public IEmitter getTarget() {
		return target;
	}

	public void setCenter(Vector2 centerPos) {
		center_pos = centerPos;
	}

	public Vector2 getCenterPos() {
		return center_pos;
	}

	public ArrayList<AEffect> getEffects() {
		return effects;
	}

	/**
	 * Returns whether the emission should be attached. The player on which the
	 * emission is bound is equivalent to target of {@link #effect}. Make sure the
	 * player isn't <code>null</code>
	 * 
	 * @return
	 */
	public boolean isAttached() {
		return attached;
	}

	public TweenableSound getSound() {
		return sound;
	}

	/**
	 * By default <code>null</code>.
	 * 
	 * @param bonesModel
	 * @return
	 */
	public void setBonesModel(IBonesModel bonesModel) {
		this.bones_model = bonesModel;
	}

	public IBonesModel getBonesModel() {
		return bones_model;
	}

	/**
	 * Example: For dying skills were the object's bones fly in several directions,
	 * or updating the {@link EmissionDefChrystals}
	 * 
	 * @param emission
	 */
	public void update(Emission emission) {

	}

	public void decreaseDelayStart(long updateRateMS) {
		delay_start_ms -= updateRateMS;
	}

	public boolean oneTouchCheck(IEmitter target) {
		if (last_hit_list != null)
			// look up whether list contains player
			if (last_hit_list.contains(target) == true)
				return true;
			else
				// add player
				last_hit_list.add(target);

		return false;
	}

	/**
	 * 
	 * @param absorber
	 * @param absorbingTransitionTime
	 */
	public void setAbsorber(IEmitter absorber, float absorbingTransitionTime) {
		this.absorber = absorber;
		this.absorbing_transition_time = absorbingTransitionTime;
	}

	/**
	 * if you choose this setter instead the with the time parameter the velocity of
	 * chasing the absorber will be constant.
	 * 
	 * @param absorber
	 */
	public void setAbsorber(IEmitter absorber) {
		this.absorber = absorber;
		this.absorbing_transition_time = Float.MAX_VALUE;
	}

	/**
	 * Returns the absorbing transition time. You can set it by
	 * {@link #setAbsorber(IEmitter, float)}
	 * 
	 * @return
	 */
	public float getAbsorbingTransitionTime() {
		return absorbing_transition_time;
	}

	public void setPersecutee(IEmitter persecutee) {
		this.persecutee = persecutee;
	}

	public IEmitter getAbsorber() {
		return absorber;
	}

	public IEmitter getPersecutee() {
		return persecutee;
	}

	public <T extends AEmissionDef> T setPersecuteeAfterCollision() {
		set_persecutee_after_collision = true;
		return (T) this;
	}

	/**
	 * If <code>true</code> and the collision is between projectile and player, the
	 * player becomes the persecutee.
	 * 
	 * @return
	 */
	public boolean hasPersecuteeAfterCollison() {
		return set_persecutee_after_collision;
	}

	/**
	 * by default <code>true</code>. Sometimes necessary if the previous emission
	 * has more than 1 projectile, see {@link IceSkill0} for example.
	 * 
	 * @param removeFromSucclistAfterGroundCollision
	 * @return
	 */
	public <T extends AEmissionDef> T setRemoveFromSucclistAfterGroundCollision(
			boolean removeFromSucclistAfterGroundCollision) {
		this.remove_from_succlist_after_ground_collision = removeFromSucclistAfterGroundCollision;
		return (T) this;
	}

	/**
	 * <code>true</code> by default. If this emission def is a successor and this
	 * flag is set to <code>true</code> the previous projectile will disappear after
	 * collision.
	 * 
	 * @param destroyPrevProjectileAfterGroundCollision
	 * @return
	 */
	public <T extends AEmissionDef> T setDestroyPrevProjectileAfterGround(
			boolean destroyPrevProjectileAfterGroundCollision) {
		this.destroy_prev_projectile_after_ground_collision = destroyPrevProjectileAfterGroundCollision;
		return (T) this;
	}

	public boolean isDestroyPrevProjectileAfterGround() {
		return destroy_prev_projectile_after_ground_collision;
	}

	/**
	 * <code>false</code> by default. If this emission def is a successor and this
	 * flag is set to <code>true</code> the previous projectile will disappear after
	 * collision with a player.
	 * 
	 * @param destroyPrevProjectileAfterPlayerCollision
	 * @return
	 */
	public <T extends AEmissionDef> T setDestroyPrevProjectileAfterPlayer(
			boolean destroyPrevProjectileAfterPlayerCollision) {
		this.destroy_prev_projectile_after_player_collision = destroyPrevProjectileAfterPlayerCollision;
		return (T) this;
	}

	public boolean isDestroyPrevProjectileAfterPlayer() {
		return destroy_prev_projectile_after_player_collision;
	}

	/**
	 * by default <code>true</code>
	 * 
	 * @return
	 */
	public boolean isRemoveFromSucclistAfterGroundCollision() {
		return remove_from_succlist_after_ground_collision;
	}

	/**
	 * Pass <code>true</code> if the next emitted projectile should be created in
	 * far distance without any life time.
	 * 
	 * @param nirvanaProjectile
	 */
	public void setNirvanaProjectile(boolean nirvanaProjectile) {
		nirvana_projectile = nirvanaProjectile;
	}

	public float[] getColorGameclass() {
		return color_gameclass;
	}

}
