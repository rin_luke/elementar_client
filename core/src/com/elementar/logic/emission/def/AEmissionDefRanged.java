package com.elementar.logic.emission.def;

import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.alignment.AAlignment;
import com.elementar.logic.emission.order.ARangedOrder;

public abstract class AEmissionDefRanged extends AEmissionDef {
	protected ARangedOrder order;

	/**
	 * Absolute value in degrees or meter
	 */
	protected float range;

	protected int number_sections;

	/**
	 * Starting with 0. Sections are split into indexed parts. The
	 * {@link #last_index} described the last used section.
	 */
	protected int last_index = 0;

	protected float section_width;

	protected float current_section;

	/**
	 * 
	 * @param internalPath
	 * @param scaleRadiusHitbox
	 * @param scaleParticleEffect
	 * @param drawBehind
	 * @param prototype
	 * @param projectileAmount
	 * @param durationInMS
	 * @param range
	 * @param numberSections
	 * @param order
	 * @param alignment
	 */
	public AEmissionDefRanged(String internalPath, float scaleRadiusHitbox, float scaleParticleEffect,
			boolean drawBehind, DefProjectile prototype, int projectileAmount, int durationInMS, float range,
			int numberSections, ARangedOrder order, AAlignment alignment) {
		super(internalPath, scaleRadiusHitbox, scaleParticleEffect, drawBehind, prototype, projectileAmount,
				durationInMS, alignment);
		this.range = range;
		/*
		 * example: if the range is 120 degrees, both borders aren't used, so the cone
		 * will be effective smaller than the 120 degress
		 */
		this.section_width = range / numberSections;
		this.number_sections = numberSections;
		this.order = order;
	}

	public AEmissionDefRanged(AEmissionDefRanged def) {
		super(def);
		this.range = def.range;
		this.number_sections = def.number_sections;
		this.section_width = def.section_width;
		this.order = def.order.makeCopy();
	}

	@Override
	public Projectile emit(short anglePlayerCursor, Emission emission) {

		last_index = order.update(number_sections);

		current_section = last_index * section_width - range * 0.5f + section_width * 0.5f;

		byte signWalkDir = 1;
		if (emission.getEmitter() != null)
			signWalkDir = (byte) (emission.getEmitter().getBody().getLinearVelocity().x < 0 ? -1 : 1);

		current_section *= signWalkDir;

		// super.emit() emits a new projectile
		return super.emit(anglePlayerCursor, emission);

	}

}
