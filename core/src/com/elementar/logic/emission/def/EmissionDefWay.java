package com.elementar.logic.emission.def;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.order.ARangedOrder;
import com.elementar.logic.util.Util;

public class EmissionDefWay extends AEmissionDefRanged {

	private int way_length;

	private Vector2 emission_origin;

	/**
	 * 
	 * @param distance
	 * @param increasing,
	 *            <code>true</code> means start point is emitter,
	 *            <code>false</code> means reverse, so cursor is start point
	 * @param internalPath
	 * @param scaleRadiusHitbox
	 * @param scaleParticleEffect
	 * @param drawBehind
	 * @param prototype
	 * @param projectileAmount
	 * @param durationInMS
	 * @param alignment
	 */
	public EmissionDefWay(String internalPath, float scaleRadiusHitbox, float scaleParticleEffect,
			boolean drawBehind, DefProjectile prototype, int projectileAmount, int durationInMS,
			int wayLength, ARangedOrder order) {
		super(internalPath, scaleRadiusHitbox, scaleParticleEffect, drawBehind, prototype,
				projectileAmount, durationInMS, wayLength, projectileAmount, order,
				new FixedCursorAlignment());
		this.way_length = wayLength;
	}

	public EmissionDefWay(EmissionDefWay def) {
		super(def);
		way_length = def.way_length;
	}

	@Override
	public void positionProjectile(Projectile emittedProjectile, Emission emission) {

		emittedProjectile.setAngleBeginning((alignment.getAngle()));

		Vector2 way = Util.getPolarCoords(
				way_length * ((float) last_index / emission.getDef().getProjectileAmount()),
				alignment.getAngle());

		/*
		 * Problem: x_emission and y_emission is variable/not constant, which
		 * means we can't just take the sum "x_emission+way.x" to get the right
		 * position of the projectile. If the emission center is moving, the
		 * straight way-effect would result in a curve. We need to record the
		 * first emission position (= the origin) and locate the projectile
		 * based on that position
		 */
		if (emission_origin == null)
			emission_origin = new Vector2(x_emission, y_emission);

		emittedProjectile.initPhysics(emission_origin.x + way.x, emission_origin.y + way.y);

		emittedProjectile.setLifeTime(emittedProjectile.getLifeTime()
				- emission.getDef().getTimePerProjectile() * last_index);

	}

	@Override
	public <T extends AEmissionDef> T makeCopy() {
		return (T) new EmissionDefWay(this);
	}

}
