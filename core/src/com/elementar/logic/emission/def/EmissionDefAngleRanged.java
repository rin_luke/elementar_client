package com.elementar.logic.emission.def;

import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.alignment.AAlignment;
import com.elementar.logic.emission.order.ARangedOrder;

public class EmissionDefAngleRanged extends AEmissionDefRanged {

	/**
	 * 
	 * @param internalPath
	 * @param scaleRadiusHitbox
	 * @param scaleParticleEffect
	 * @param drawBehind
	 * @param prototype
	 * @param projectileAmount
	 * @param durationMS
	 * @param range
	 * @param numberSections
	 * @param order
	 * @param alignment
	 */
	public EmissionDefAngleRanged(String internalPath, float scaleRadiusHitbox, float scaleParticleEffect,
			boolean drawBehind, DefProjectile prototype, int projectileAmount, int durationMS, float range,
			int numberSections, ARangedOrder order, AAlignment alignment) {
		super(internalPath, scaleRadiusHitbox, scaleParticleEffect, drawBehind, prototype, projectileAmount, durationMS,
				range, numberSections, order, alignment);
	}

	public EmissionDefAngleRanged(EmissionDefAngleRanged def) {
		super(def);
	}

	@Override
	public void positionProjectile(Projectile emittedProjectile, Emission emission) {
		emittedProjectile.setAngleBeginning((int) (alignment.getAngle() + current_section));
		emittedProjectile.initPhysics(x_emission, y_emission);
	}

	@Override
	public <T extends AEmissionDef> T makeCopy() {
		return (T) new EmissionDefAngleRanged(this);
	}
}
