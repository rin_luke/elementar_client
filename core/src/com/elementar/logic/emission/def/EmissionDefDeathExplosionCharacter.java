package com.elementar.logic.emission.def;

import com.badlogic.gdx.math.MathUtils;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.alignment.AAlignment;
import com.elementar.logic.emission.order.ARangedOrder;
import com.esotericsoftware.spine.Bone;

public class EmissionDefDeathExplosionCharacter extends EmissionDefAngleRanged {

	private int bone_index = 0;

	/**
	 * 
	 * @param internalPath
	 * @param radiusMultiplier
	 * @param drawBehind
	 * @param prototype
	 * @param projectileAmount
	 * @param duration
	 * @param range
	 * @param numberSections
	 * @param order
	 * @param alignment
	 */
	public EmissionDefDeathExplosionCharacter(String internalPath, float radiusMultiplier, boolean drawBehind,
			DefProjectile prototype, int projectileAmount, int duration, float range, int numberSections,
			ARangedOrder order, AAlignment alignment) {
		super(internalPath, radiusMultiplier, radiusMultiplier, drawBehind, prototype, projectileAmount, duration,
				range, numberSections, order, alignment);
	}

	public EmissionDefDeathExplosionCharacter(EmissionDefAngleRanged def) {
		super(def);
	}

	@Override
	public <T extends AEmissionDef> T makeCopy() {
		return (T) new EmissionDefDeathExplosionCharacter(this);
	}

	@Override
	public void positionProjectile(Projectile emittedProjectile, Emission emission) {

		if (bones_model != null) {
			
			Bone correspondingBone = bones_model.getDeathBones().get(bone_index);

			// beim character hängt die Richtung auch vom alignment ab.
			emittedProjectile.setAngleBeginning((int) (alignment.getAngle() + current_section));
			emittedProjectile.initPhysics(correspondingBone.getWorldX(), correspondingBone.getWorldY());
			bone_index++;
		}
	}

	@Override
	public void update(Emission emission) {
		if (bones_model != null) {
			Projectile projectile;
			Bone bone;
			for (int i = 0; i < emission.getEmittedProjectiles().size(); i++) {
				projectile = emission.getEmittedProjectiles().get(i);
				bone = bones_model.getDeathBones().get(i);

				// set bone position to projectile position
				bone.setPosition(
						(projectile.getPosition().x - bone.getSkeleton().getX())
								* (bones_model.isFlip() == true ? -1 : 1),
						projectile.getPosition().y - bone.getSkeleton().getY());
				// rotation
				bone.setRotation(projectile.getBody().getAngle() * MathUtils.radiansToDegrees);
			}
		}
	}
}
