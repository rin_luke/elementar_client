package com.elementar.logic.emission;

import com.elementar.logic.util.ViewHandler;

/**
 * At the beginning of a projectile's life its visibility on client side is
 * UNDEFINED. When the first
 * {@link ViewHandler#isInside(com.badlogic.gdx.math.Vector2)} query is done we
 * set the visibility to <code>true</code> or <code>false</code>.
 * 
 * @author lukassongajlo
 *
 */
public enum ProjectileVisibility {

	UNDEFINED, TRUE

}
