package com.elementar.logic.emission;

import java.util.LinkedList;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.characters.skills.ComboStorage;
import com.elementar.logic.emission.effect.AEffectTimed;
import com.elementar.logic.util.Shared.Team;

public interface IEmitter {

	public Team getTeam();

	public LinkedList<Trigger> getTriggers();

	public World getWorld();

	public Location getLocation();

	public Vector2 getPos();

	public Vector2 getCursorPos();

	public LinkedList<SparseProjectile> getSparseProjectiles();

	public EmissionList getEmissionList();

	public boolean isAlive();

	public Body getBody();

	public ComboStorage getComboStorageTransfer();

	public ComboStorage getComboStorage1();

	public ComboStorage getComboStorage2();

	public void addEffect(AEffectTimed effect);

	public void increaseCrystals();

	public void setSlayer(IEmitter slayer);

	public IEmitter getSlayer();

}
