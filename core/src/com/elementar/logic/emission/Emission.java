package com.elementar.logic.emission;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.elementar.data.container.ParticleContainer;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.characters.ServerPhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.characters.skills.sand.SandSkill1;
import com.elementar.logic.characters.skills.void_.VoidSkill0;
import com.elementar.logic.emission.EmissionRuntimeData.ComboType;
import com.elementar.logic.emission.alignment.AlignmentBehaviour;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.IBonesModel;
import com.elementar.logic.emission.effect.AEffect;
import com.elementar.logic.emission.effect.AEffectImpulse;
import com.elementar.logic.emission.effect.AEffectTimed;
import com.elementar.logic.emission.util.TargetFinding;
import com.elementar.logic.network.gameserver.GameserverLogic;
import com.elementar.logic.network.response.InfoEmissionInterruption;
import com.elementar.logic.util.CreepAndTowerCastCallback;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.ViewCircle;
import com.elementar.logic.util.Shared.Team;
import com.elementar.logic.util.lists.NotNullArrayList;

/**
 * See also "skill_ingredient_list.pdf" in dropbox.
 * 
 * @author lukassongajlo
 * 
 */
public class Emission {

	public static short RECENT_PROJECTILE_ID = 0;

	private AEmissionDef def;

	private ArrayList<Emission> active_successors_time_wise = new ArrayList<Emission>();

	private LinkedList<Projectile> emitted_projectiles = new LinkedList<Projectile>();
	private LinkedList<SparseProjectile> sparse_projectiles = new LinkedList<SparseProjectile>();

	/**
	 * Needed to create projectile with a hitbox
	 */
	public World world;

	private IEmitter emitter;

	/**
	 * position of the emission
	 */
	protected Vector2 center_pos, center_pos_beginning;

	private int accumulator = 0;

	/**
	 * Holds all emission defs, initiated by destroyed projectile of this emission
	 */
	private NotNullArrayList<AEmissionDef> new_emission_defs_by_projectiles = new NotNullArrayList<AEmissionDef>();

	private boolean started = false;

	private boolean interrupted;

	private int elapsed_time_ms;

	private long id_sound;

	/**
	 * 0 means no trigger, otherwise > 0.
	 * 
	 */
	private short id_trigger = 0;

	private boolean is_trigger_emission = false;

	private Location location;

	/**
	 * see {@link SandSkill1} and {@link DefProjectile#setTargetFinding(float)}
	 */
	private IEmitter current_target;

	/**
	 * CenterPos of emission is set by passed definition-object.
	 * 
	 * @param emissionDef
	 * @param emitter
	 */
	public Emission(AEmissionDef emissionDef, IEmitter emitter) {
		this(emissionDef, emitter, emissionDef.getCenterPos());
	}

	/**
	 * Use this constructor if you need a strict separation between same
	 * emissionDefs. This is necessary when several emissions are created at the
	 * same time. In this case they need a own center pos reference (see
	 * {@link VoidSkill0#release()})
	 * 
	 * @param emissionDef
	 * @param emitter
	 * @param centerpos
	 */
	public Emission(AEmissionDef emissionDef, IEmitter emitter, Vector2 centerpos) {

		// note: makeCopy() doesn't change centerpos reference
		def = emissionDef.makeCopy();

		this.emitter = emitter;
		location = emitter.getLocation();

		world = emitter.getWorld();
		center_pos = centerpos;

		if (center_pos == null) {
			center_pos = new Vector2();
			center_pos_beginning = new Vector2();
		} else
			center_pos_beginning = new Vector2(center_pos);

		if (location == Location.SERVERSIDE) {

			if (def.isTransmissible() == false)
				return;

			Trigger trigger = new Trigger(def);
			id_trigger = trigger.trigger_id;
			emitter.getTriggers().add(trigger);
		}

	}

	/**
	 * Constructor for triggered emissions. Centerpos by trigger
	 * 
	 * @param trigger
	 * @param emitter
	 * @param bonesModel
	 */
	public Emission(Trigger trigger, IEmitter emitter, IBonesModel bonesModel) {
		is_trigger_emission = true;
		// get def by trigger's pool-ID, and make a copy
		def = ParticleContainer.getDef(trigger.pool_id).makeCopy();
		def.setBonesModel(bonesModel);
		def.setPrevProjVel(trigger.prev_proj_vel);
		this.emitter = emitter;
		location = emitter.getLocation();
		world = emitter.getWorld();
		id_trigger = trigger.trigger_id;
		center_pos = trigger.center_pos;
		center_pos_beginning = new Vector2(trigger.center_pos);
		if (def.getAlignment() != null)
			def.getAlignment().setAngle(trigger.alignment_angle);
		if (trigger.target_id != -1) {
			OmniClient target = LogicTier.WORLD_PLAYER_MAP.get(trigger.target_id);
			if (target == null)
				return;
			def.setTarget(target.getPhysicalClient());
			// center_pos = target.getPhysicalClient().pos;

			// set run-time data
		}

		def.getRuntimeData().setMergedEffects(trigger.merged_effects);

		for (AEffect effect : trigger.merged_effects) {

			if (effect instanceof AEffectTimed) {

				AEffectTimed effectTimed = ((AEffectTimed) effect);
				short poolID = effectTimed.getPoolID();

				AEmissionDef def2 = ParticleContainer.getDef(poolID);
				for (AEffect effectDef : def2.getEffects()) {
					if (effectDef.getClass().equals(effectTimed.getClass()) == true) {
						effectTimed.setTransientData((AEffectTimed) effectDef);
						break;
					}
				}

			}
		}

		def.getRuntimeData().setComboType(trigger.combo_type);
	}

	/**
	 * initiates the emission
	 */
	public void init() {

		if (def.getRuntimeData().getComboType() != ComboType.REGULAR) {
			// normal or transfer
			for (AEffect effect : def.getEffects()) {
				if (effect.isOnlyWithCombo() == true)
					continue;
				effect.setEmission(this);
				effect.setTarget(def.getTarget());
				effect.apply();
			}

		} else if (def.getRuntimeData().getMergedEffects() != null) {
			// combo case (comboType == REGULAR), we use the merged effects
			ArrayList<AEffect> effects = def.getRuntimeData().getMergedEffects();

			if (effects != null) {

				// each emission might have multiple effects
				for (AEffect effect : effects) {

					AEffect copy = effect.makeCopy();
					copy.setEmission(this);
					copy.setTarget(def.getTarget());
					copy.apply();
				}
			}

			// lightning skill0
			for (AEffect effect : def.getEffects()) {
				if (effect instanceof AEffectImpulse) {
					effect.setEmission(this);
					effect.setTarget(def.getTarget());
					effect.apply();
				}
			}

		}

		/*
		 * generally speaking we don't want trigger emissions to be activated but there
		 * are exceptions
		 */
		if (is_trigger_emission == false || def.isTriggerEmissionAllowed() == true) {

			started = true;

			if (def.getAlignment().getBehaviour() == AlignmentBehaviour.CURSOR_FIXED) {
				short angle = Util.getAngleBetweenTwoPoints(emitter.getPos(), emitter.getCursorPos());
				def.getAlignment().setAngle(angle);
			}
		}

	}

	/**
	 * 
	 * @return a list of new emission-definitions caused by projectile deaths.
	 */
	public NotNullArrayList<AEmissionDef> update() {

		sparse_projectiles.clear();
		if (started == false)
			return new_emission_defs_by_projectiles;

		// begin actual update
		accumulator += Shared.SEND_AND_UPDATE_RATE_IN_MS;
		elapsed_time_ms += Shared.SEND_AND_UPDATE_RATE_IN_MS;

		// should this emission be attached to a player?
		if (def.isAttached() == true)
			if (def.getTarget() != null) {
				center_pos.set(def.getTarget().getPos());
			}
		def.update(this);

		Projectile tempProjectile;

		// emit check
		while (accumulator >= def.getTimePerProjectile() && interrupted == false) {

			if (def.getRemainingProjectileAmount() <= 0)
				break;

			accumulator -= def.getTimePerProjectile();

			// set up for not having nirvana projectiles.
			def.setNirvanaProjectile(false);
			if (def.getDefProjectile().getTargetFinding() != null) {

				TargetFinding targetFinding = def.getDefProjectile().getTargetFinding();

				boolean oppositeTeams = targetFinding.isOppositeTeams();

				ViewCircle detectingCircle = new ViewCircle(center_pos, targetFinding.getRadius());

				if (current_target != null) {
					// is current target still in sight?
					if (Util.circleContains(detectingCircle, current_target.getPos()) == false
							|| castRay(current_target, oppositeTeams) == false)
						current_target = null;
				}

				if (current_target == null) {

					Team teamEmitter = emitter.getTeam();
					// find new targets
					ArrayList<IEmitter> potentialTargets = new ArrayList<>();
					if (location == Location.SERVERSIDE)
						for (ServerPhysicalClient client : GameserverLogic.PLAYER_MAP.values()) {

							if (client == emitter)
								continue;

							if (client.getHitbox() == null)
								continue;

							if (Util.circleContains(detectingCircle, client.getHitbox().getPosition()) == true
									&& ((teamEmitter != client.getMetaClient().team && oppositeTeams == true)
											|| (teamEmitter == client.getMetaClient().team && oppositeTeams == false)))
								potentialTargets.add(client);
						}

					else if (location == Location.CLIENTSIDE_TRAINING) {
						for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {

							if (client.getPhysicalClient() == emitter)
								continue;

							if (client.getPhysicalClient().getHitbox() == null)
								continue;

							if (Util.circleContains(detectingCircle,
									client.getPhysicalClient().getHitbox().getPosition()) == true
									&& ((teamEmitter != client.getMetaClient().team && oppositeTeams == true)
											|| (teamEmitter == client.getMetaClient().team && oppositeTeams == false)))
								potentialTargets.add(client.getPhysicalClient());
						}
//					for (OmniCreep creep : LogicTier.CREEP_LIVING_LIST) {
//						if (Util.circleContains(attackCircle,
//								creep.getPhysicalCreep().getHitbox().getPosition()) == true)
//							potentialTargets.add(creep.getPhysicalCreep());
//
//					}
					}

//				Collections.sort(potentialTargets, comparator);

					for (IEmitter potentialTarget : potentialTargets) {

						if (castRay(potentialTarget, oppositeTeams) == true)
							// found target, ray cast was successful
							break;

					}
				}

				if (current_target != null) {

					def.getAlignment().setAngle(Util.getAngleBetweenTwoPoints(center_pos, current_target.getPos()));

					if (targetFinding.isBecomingPersecutee() == true)
						def.setPersecutee(current_target);

					if (targetFinding.isBecomingAbsorber() == true)
						def.setAbsorber(current_target);

				} else {

					/*
					 * when no target has been found and the target-finding definition allows it a
					 * nirvana projectile is created
					 */
					def.setNirvanaProjectile(targetFinding.isNirvanaProjectile());
				}

			}

			tempProjectile = def.emit(Util.getAngleBetweenTwoPoints(emitter.getPos(), emitter.getCursorPos()), this);

			emitted_projectiles.add(tempProjectile);

			// mark projectile with an unique ID
			tempProjectile.getSparseRepresentation().projectile_id = RECENT_PROJECTILE_ID;
			// inc total projectile count
			RECENT_PROJECTILE_ID++;

			// add secondary pool ids
			ArrayList<Short> secPoolIDs = def.getRuntimeData().getMergedPools();

			if (secPoolIDs == null)
				continue;

			for (int i = 0; i < secPoolIDs.size(); i++) {

				short poolID = secPoolIDs.get(i);

				AEmissionDef emissionDefSec = ParticleContainer.getDef(poolID);

				if (emissionDefSec != null) {
					if (emissionDefSec.isFeedback() == true) {

						int lifeTimeSec = emissionDefSec.getDefProjectile().life_time;

						if (tempProjectile.getLifeTime() < lifeTimeSec) {
							tempProjectile.setLifeTime(lifeTimeSec);

						}
					}
				}

				tempProjectile.getSparseRepresentation().pool_ids[i + 1] = poolID;
			}

		}

		new_emission_defs_by_projectiles.clear();
		Iterator<Projectile> it = emitted_projectiles.iterator();

		while (it.hasNext() == true) {
			tempProjectile = it.next();
			tempProjectile.update();

			// add successor emissions by PROJECTILE
			Iterator<AEmissionDef> itSuccs = tempProjectile.getActiveSuccessors().iterator();

			while (itSuccs.hasNext() == true) {
				AEmissionDef succDef = itSuccs.next();

				if (succDef.getCenterPos() == null) {
					/*
					 * if projectile hasn't collided with a player (but with ground) or died because
					 * of ending life time the center pos must be set
					 */
					succDef.setCenter(new Vector2(tempProjectile.getPosition()));
				}

				itSuccs.remove();
				new_emission_defs_by_projectiles.add(succDef);
			}

			// lifetime check
			if (tempProjectile.isAlive() == false) {
				tempProjectile.destroy();
				it.remove();
				continue;
			}

			/*
			 * if we are operating on the server the transmissible field has to be set to
			 * add the sparse_prj.
			 */
			if (emitter.getLocation() == Location.SERVERSIDE && def.isTransmissible() == true
					|| emitter.getLocation() != Location.SERVERSIDE)
				sparse_projectiles.add(tempProjectile.getSparseRepresentation());
		}

		return new_emission_defs_by_projectiles;
	}

	private boolean castRay(IEmitter potentialTarget, boolean oppositeTeams) {

		if (Util.isNan(potentialTarget.getPos()) == false) {
			if (center_pos.dst2(potentialTarget.getPos()) < 0.01f) {

				current_target = potentialTarget;
				// angle = 0;
				return true;

			} else {

				if (emitter == null)
					return false;

				Team team = oppositeTeams == true ? emitter.getTeam()
						: (emitter.getTeam() == Team.TEAM_1 ? Team.TEAM_2 : Team.TEAM_1);
				CreepAndTowerCastCallback callback = new CreepAndTowerCastCallback(team);

				world.rayCast(callback, center_pos, potentialTarget.getPos());

				if (callback.getFixtureTarget() != null
						&& potentialTarget == callback.getFixtureTarget().getUserData()) {

					current_target = potentialTarget;
					return true;

				}
			}

		}
		return false;
	}

	/**
	 * emission has ended when it emitted all projectiles AND when all emitted
	 * projectiles are dead
	 * 
	 * @return
	 */
	public boolean areAllProjectilesDead() {
		return def.getRemainingProjectileAmount() == 0 && emitted_projectiles.size() == 0;
	}

	public AEmissionDef getDef() {
		return def;
	}

	public boolean hasStarted() {
		return started;
	}

	public LinkedList<SparseProjectile> getSparseProjectiles() {
		return sparse_projectiles;
	}

	public LinkedList<Projectile> getEmittedProjectiles() {
		return emitted_projectiles;
	}

	public void setInterrupted(boolean interrupted) {
		// don't do it twice
		if (this.interrupted == false && interrupted == true)
			recInterruption(this);
		this.interrupted = interrupted;
	}

	/**
	 * Logically succ emissions of projectiles won't be interrupted because they're
	 * already flying and not in control of the emitter (client)
	 * 
	 * @param emission
	 */
	private static void recInterruption(Emission emission) {

		if (emission.emitter instanceof ServerPhysicalClient) {
			// given: trigger_id is always not 0 at server side
			ServerPhysicalClient serverClient = (ServerPhysicalClient) emission.emitter;
			// add interruption info
			serverClient.addEmissionInterruption(new InfoEmissionInterruption(emission.id_trigger));
		}

		// delete existing projectiles
		if (emission.getDef().getDefProjectile().isInterruptByEmission() == true) {
			Iterator<Projectile> it = emission.emitted_projectiles.iterator();
			Projectile tempProjectile;
			while (it.hasNext() == true) {
				tempProjectile = it.next();
				tempProjectile.destroy();
				it.remove();
			}
		}

		// avoid following projectiles
		emission.interrupted = true;

		// cancel all future time wise successors
		emission.getDef().getSuccessorsTimewise().clear();

		// do the same for already existing time wise successors
		for (Emission succ : emission.active_successors_time_wise)
			recInterruption(succ);

		emission.active_successors_time_wise.clear();
	}

	public Vector2 getCenterPos() {
		return center_pos;
	}

	/**
	 * Returns beginning center position without references to player
	 * 
	 * @return
	 */
	public Vector2 getCenterPosUnreferenced() {
		return center_pos_beginning;
	}

	public ArrayList<Emission> getActiveTimeWiseSuccessors() {
		return active_successors_time_wise;
	}

	public short getTriggerID() {
		return id_trigger;
	}

	public IEmitter getEmitter() {
		return emitter;
	}

	public long getSoundID() {
		return id_sound;
	}

	public float getElapsedTimeMS() {
		return elapsed_time_ms;
	}

	public float getProgressInPercentage() {
		return (float) elapsed_time_ms / def.getDuration();
	}

}
