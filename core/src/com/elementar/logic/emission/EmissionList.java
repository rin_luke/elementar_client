package com.elementar.logic.emission;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.maps.FIFOLimitedHashMap;

/**
 * Each {@link PhysicalClient} holds an {@link EmissionList} object to hold and
 * update all emissions. Clients on client-side also updates decorative
 * emissions
 * 
 * @author lukassongajlo
 * 
 */
public class EmissionList extends LinkedList<Emission> {

	private IEmitter emission_user;
	private LinkedList<SparseProjectile> sparse_projectiles;

	private HashMap<Short, Emission> map_for_interruptions = new FIFOLimitedHashMap<Short, Emission>(20);

	private LinkedList<Emission> delayed_emissions = new LinkedList<>();

	/**
	 * 
	 * @param emissionUser
	 */
	public EmissionList(IEmitter emissionUser) {
		emission_user = emissionUser;
		sparse_projectiles = emissionUser.getSparseProjectiles();
	}

	public void update() {

		sparse_projectiles.clear();

		Emission tempEmission;

		// check delayed emissions
		ListIterator<Emission> itDelayed = delayed_emissions.listIterator();
		while (itDelayed.hasNext() == true) {
			tempEmission = itDelayed.next();
			if (tempEmission.getDef().getDelayStart() <= 0) {
				add(tempEmission);
				itDelayed.remove();
			}

			tempEmission.getDef().decreaseDelayStart(Shared.SEND_AND_UPDATE_RATE_IN_MS);

		}

		// check active emissions
		ListIterator<Emission> it = listIterator();

		while (it.hasNext() == true) {

			tempEmission = it.next();
			if (tempEmission != null) {

				if (tempEmission.hasStarted() == false) {

					// not executable
					it.remove();
					/*
					 * Note: Here I don't remove the emission from the map_for_interruptions- map,
					 * because I may need it to interrupt the sound. Non-decorative emissions would
					 * apply the sound and then get killed, so I have no reference for the sound
					 * instance anymore. Those emissions would stay forever in the map, so I decided
					 * to choose a self-created limited fifo hashmap
					 */
					continue;
				}

				if (tempEmission.areAllProjectilesDead() == true) {

					tempEmission.getActiveTimeWiseSuccessors().clear();
					it.remove();

					// remove from map
					if (tempEmission.getTriggerID() != 0)
						map_for_interruptions.remove(tempEmission.getTriggerID());

					continue;
				}

				/*
				 * update emission and add new emissions triggered by projectiles
				 */
				Iterator<AEmissionDef> itNew = tempEmission.update().iterator();

				AEmissionDef succDef;
				while (itNew.hasNext() == true) {
					succDef = itNew.next();

					itNew.remove();

					addSuccEmission(it, succDef);
				}

				sparse_projectiles.addAll(tempEmission.getSparseProjectiles());

				// add TIME-WISE successors
				Iterator<SuccessorTimewise> itSuccs = tempEmission.getDef().getSuccessorsTimewise().iterator();
				SuccessorTimewise succ;

				while (itSuccs.hasNext() == true) {
					succ = itSuccs.next();
					succDef = succ.getDef();

					// stone skill0 for example
					if (succDef.getCenterPos() == null) 
						succDef.setCenter(tempEmission.center_pos);
					
					// has progress reached the "trigger time limit"?
					if (tempEmission.getProgressInPercentage() >= succ.getElapsedTimeTilTrigger()) {
						// remove def from list
						itSuccs.remove();

						tempEmission.getActiveTimeWiseSuccessors().add(addSuccEmission(it, succDef));

					}
				}
			}
		}

	}

	@Override
	public boolean add(Emission emission) {
		if (emission.getDef().getDelayStart() > 0) {
			delayed_emissions.add(emission);
			return false;
		} else {
			commonAdd(emission);
			return super.add(emission);
		}
	}

	private Emission addSuccEmission(ListIterator<Emission> it, AEmissionDef def) {
		if (def != null) {
			Emission e = new Emission(def, emission_user);
			if (e.getDef().getDelayStart() > 0)
				delayed_emissions.add(e);
			else {
				commonAdd(e);
				it.add(e);
				it.previous();
			}
			return e;
		}
		return null;
	}

	/**
	 * Help method to avoid code duplication. Increases object id and initiates
	 * emission
	 * 
	 * @param emission
	 */
	private void commonAdd(Emission emission) {
		if (emission.getTriggerID() != 0)
			map_for_interruptions.put(emission.getTriggerID(), emission);
		emission.init();
	}

	public HashMap<Short, Emission> getMapForInterruptions() {
		return map_for_interruptions;
	}
}
