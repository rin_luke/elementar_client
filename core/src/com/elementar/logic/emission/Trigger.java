package com.elementar.logic.emission;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.emission.EmissionRuntimeData.ComboType;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.effect.AEffect;

/**
 * trigger for emissions. Will be sent over network to initiate new emissions on
 * client side.
 * 
 * @author lukassongajlo
 * 
 */
public class Trigger {

	public static short RECENT_TRIGGER_ID;

	public short trigger_id;

	public short pool_id;

	/**
	 * CenterPos will be given by serverside emissions
	 */
	public Vector2 center_pos;

	public short alignment_angle;

	public short target_id = -1;

	public Vector2 prev_proj_vel;
	
	public ComboType combo_type;
	
	public ArrayList<AEffect> merged_effects;

	public Trigger() {
	}

	/**
	 * 
	 * @param def
	 */
	public Trigger(AEmissionDef def) {
		RECENT_TRIGGER_ID++;
		trigger_id = RECENT_TRIGGER_ID;
		pool_id = def.getPoolID();
		center_pos = def.getCenterPos();
		alignment_angle = def.getAlignment().getAngle();
		
		prev_proj_vel = def.getPrevProjVel();
		combo_type = def.getRuntimeData().getComboType();
		merged_effects = def.getRuntimeData().getMergedEffects();
		
		IEmitter target = def.getTarget();

		/*
		 * target id is an index for a list at client-side where the target lies in
		 */
		if (target != null)
			target_id = ((PhysicalClient) target).connection_id;

	}
}
