package com.elementar.logic.emission;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.util.ViewCircle;

/**
 * A projectile which can be sent over the network. It holds just sparse
 * informations of the projectile to draw at the right position.
 * 
 * @author lukassongajlo
 * 
 */
public class SparseProjectile {

	/**
	 * Each emission has a unique pool id. Inside the array might be additional ids
	 * of absorbed projectiles. The first index is the own pool id, given by the
	 * emission.
	 */
	public short[] pool_ids;
	/**
	 * Will be set directly after emitting a new projectile.
	 */
	public short projectile_id;

	/**
	 * Will be set in each update() cycle
	 */
	public Vector2 pos = new Vector2();

	/**
	 * Will be updated in each update() cycle
	 */
	public short direction;

	/**
	 * contains the connection ID of a player to which this projectile is attached
	 * to.
	 */
	public short connection_id_attached = -1;

	public transient long sound_id = -1;

	public SparseProjectile() {

	}

	/**
	 * 
	 * @param poolIDs
	 */
	public SparseProjectile(short poolID) {
		this.pool_ids = new short[3];
		pool_ids[0] = poolID;
		pool_ids[1] = -1; // will be set by combo
		pool_ids[2] = -1; // will be set by combo
	}

	public SparseProjectile(SparseProjectile projectile) {
		projectile_id = projectile.projectile_id;
		pool_ids = projectile.pool_ids;
		pos.set(projectile.pos);
		direction = projectile.direction;
		connection_id_attached = projectile.connection_id_attached;
	}

}
