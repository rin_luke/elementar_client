package com.elementar.logic.emission.alignment;

public abstract class AAlignment {

	protected short angle;

	protected AlignmentBehaviour behaviour;

	/**
	 * 
	 * @param angle
	 */
	public AAlignment(int angle) {
		this.angle = (short) angle;
		this.behaviour = AlignmentBehaviour.FIXED;
	}

	public AAlignment(AlignmentBehaviour behaviour) {
		this.behaviour = behaviour;
	}

	public AAlignment(AAlignment alignment) {
		angle = alignment.angle;
		behaviour = alignment.behaviour;
	}

	public AlignmentBehaviour getBehaviour() {
		return behaviour;
	}

	public short getAngle() {
		return angle;
	}

	public void setAngle(short angle) {
		this.angle = angle;
	}

	/**
	 * Calls the copy constructor of the specific subclass.
	 * 
	 * @return
	 */
	public abstract <T extends AAlignment> T makeCopy();
}
