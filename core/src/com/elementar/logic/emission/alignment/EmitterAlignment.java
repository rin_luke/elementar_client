package com.elementar.logic.emission.alignment;

public class EmitterAlignment extends AAlignment {

	public EmitterAlignment() {
		super(AlignmentBehaviour.EMITTER);
	}

	public EmitterAlignment(EmitterAlignment alignment) {
		super(alignment);
	}

	@Override
	public <T extends AAlignment> T makeCopy() {
		return (T) new EmitterAlignment(this);
	}

}
