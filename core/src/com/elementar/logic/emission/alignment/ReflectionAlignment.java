package com.elementar.logic.emission.alignment;

public class ReflectionAlignment extends AAlignment {

	public ReflectionAlignment() {
		super(AlignmentBehaviour.REFLECTION);
	}

	public ReflectionAlignment(ReflectionAlignment alignment) {
		super(alignment);
	}

	@Override
	public <T extends AAlignment> T makeCopy() {
		return (T) new ReflectionAlignment(this);
	}

}
