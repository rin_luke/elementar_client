package com.elementar.logic.emission.alignment;

public class SteeringAlignment extends AAlignment {

	public SteeringAlignment() {
		super(AlignmentBehaviour.STEERING);
	}

	public SteeringAlignment(SteeringAlignment alignment) {
		super(alignment);
	}

	@Override
	public <T extends AAlignment> T makeCopy() {
		return (T) new SteeringAlignment(this);
	}

}
