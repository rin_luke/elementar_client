package com.elementar.logic.emission.alignment;

public class FixedAlignment extends AAlignment {

	/**
	 * Creates a fix alignment (with angle in degrees)
	 * 
	 * @param angle
	 */
	public FixedAlignment(int angle) {
		super(angle);
	}

	public FixedAlignment(FixedAlignment alignment) {
		super(alignment);
	}

	@Override
	public <T extends AAlignment> T makeCopy() {
		return (T) new FixedAlignment(this);
	}

}
