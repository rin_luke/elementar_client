package com.elementar.logic.emission.alignment;

public enum AlignmentBehaviour {
	CURSOR_FOLLOWING,
	CURSOR_FIXED,
	CURSOR_FOLLOWING_WHILE_EMITTING,
	FIXED,
	OBSTACLE,
	REFLECTION,
	EMITTER,
	STEERING
}
