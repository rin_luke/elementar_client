package com.elementar.logic.emission.alignment;

/**
 * The angle will be set while emitting a new projectile
 * 
 * @author lukassongajlo
 * 
 */
public class CursorFollowingWhileEmittingAlignment extends AAlignment {

	/**
	 * The angle will be set while emitting a new projectile, following the
	 * cursor.
	 */
	public CursorFollowingWhileEmittingAlignment() {
		super(AlignmentBehaviour.CURSOR_FOLLOWING_WHILE_EMITTING);
	}

	public CursorFollowingWhileEmittingAlignment(CursorFollowingWhileEmittingAlignment alignment) {
		super(alignment);
	}

	@Override
	public <T extends AAlignment> T makeCopy() {
		return (T) new CursorFollowingWhileEmittingAlignment(this);
	}

}
