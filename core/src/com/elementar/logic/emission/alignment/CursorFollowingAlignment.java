package com.elementar.logic.emission.alignment;

public class CursorFollowingAlignment extends AAlignment {

	private float impulse_value;

	/**
	 * 
	 * @param impulseValue
	 */
	public CursorFollowingAlignment(float impulseValue) {
		super(AlignmentBehaviour.CURSOR_FOLLOWING);
		impulse_value = impulseValue;
	}

	public CursorFollowingAlignment(CursorFollowingAlignment alignment) {
		super(alignment);
		this.impulse_value = alignment.impulse_value;
	}

	@Override
	public <T extends AAlignment> T makeCopy() {
		return (T) new CursorFollowingAlignment(this);
	}

	public float getImpulseValue() {
		return impulse_value;
	}

}
