package com.elementar.logic.emission.alignment;

public class ObstacleAlignment extends AAlignment {

	public ObstacleAlignment() {
		super(AlignmentBehaviour.OBSTACLE);
	}

	public ObstacleAlignment(ObstacleAlignment alignment) {
		super(alignment);
	}

	@Override
	public <T extends AAlignment> T makeCopy() {
		return (T) new ObstacleAlignment(this);
	}

}
