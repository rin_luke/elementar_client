package com.elementar.logic.emission.alignment;

/**
 * Alignment for emissions whose projectiles are oriented by the cursor just at
 * their creation. Projectiles shouldn't follow the cursor the whole lifetime.
 * 
 * @author lukassongajlo
 *
 */
public class FixedCursorAlignment extends AAlignment {

	public FixedCursorAlignment() {
		super(AlignmentBehaviour.CURSOR_FIXED);
	}

	public FixedCursorAlignment(FixedCursorAlignment alignment) {
		super(alignment);
	}

	@Override
	public <T extends AAlignment> T makeCopy() {
		return (T) new FixedCursorAlignment(this);
	}
}
