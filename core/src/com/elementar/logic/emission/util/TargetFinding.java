package com.elementar.logic.emission.util;

public class TargetFinding {

	private float radius;

	/**
	 * Set to <code>true</code> by default. If <code>true</code> it tries to find a
	 * target who's team membership is the opposite of the emitter's one. Otherwise
	 * (= <code>false</code>) it tries to find an ally.
	 */
	private boolean opposite_teams = true;

	/**
	 * Set to <code>true</code> by default. Handles the situation of not having
	 * found any target before a new projectile will be created. If
	 * <code>true</code> the new projectile is created in far distance without any
	 * life time.
	 */
	private boolean nirvana_projectile = true;

	/**
	 * Set to <code>false</code> by default. If <code>true</code> the found target
	 * will also be persecuted by the projectile.
	 */
	private boolean becoming_persecutee = false;

	/**
	 * Set to <code>false</code> by default. If <code>true</code> the found target
	 * will also be set as the absorber of the projectile.
	 */
	private boolean becoming_absorber = false;

	/**
	 * {@link #opposite_teams} = <code>true</code> <br>
	 * {@link #nirvana_projectile} = <code>true</code> <br>
	 * {@link #becoming_persecutee} = <code>false</code> <br>
	 * {@link #becoming_absorber} = <code>false</code>
	 * 
	 * @param radius
	 */
	public TargetFinding(float radius) {
		this.radius = radius;
	}

	private TargetFinding(TargetFinding targetFinding) {
		this.opposite_teams = targetFinding.opposite_teams;
		this.becoming_absorber = targetFinding.becoming_absorber;
		this.becoming_persecutee = targetFinding.becoming_persecutee;
		this.nirvana_projectile = targetFinding.nirvana_projectile;
		this.radius = targetFinding.radius;
	}

	public TargetFinding makeCopy() {
		return new TargetFinding(this);
	}

	public TargetFinding setOppositeTeams(boolean oppositeTeams) {
		this.opposite_teams = oppositeTeams;
		return this;
	}

	public TargetFinding setNirvanaProjectile(boolean nirvanaProjectile) {
		this.nirvana_projectile = nirvanaProjectile;
		return this;
	}

	public TargetFinding setBecomingPersecutee(boolean becomesPersecutee) {
		this.becoming_persecutee = becomesPersecutee;
		return this;
	}

	public TargetFinding setBecomingAbsorber(boolean becomesAbsorber) {
		this.becoming_absorber = becomesAbsorber;
		return this;
	}

	public float getRadius() {
		return radius;
	}
	
	public boolean isOppositeTeams() {
		return opposite_teams;
	}

	public boolean isNirvanaProjectile() {
		return nirvana_projectile;
	}

	public boolean isBecomingPersecutee() {
		return becoming_persecutee;
	}

	public boolean isBecomingAbsorber() {
		return becoming_absorber;
	}
}
