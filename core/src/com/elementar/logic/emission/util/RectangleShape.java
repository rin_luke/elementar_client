package com.elementar.logic.emission.util;

public class RectangleShape {

	private float width, height;

	public RectangleShape(float width, float height) {
		this.width = width;
		this.height = height;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

}
