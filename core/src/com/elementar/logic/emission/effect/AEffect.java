package com.elementar.logic.emission.effect;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.characters.skills.stone.StoneSkill1;
import com.elementar.logic.creeps.PhysicalCreep;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.IEmitter;

/**
 * An effect is applied to one {@link PhysicalClient}. For example the health
 * gets reduced.
 * 
 * @author lukassongajlo
 * 
 */
public abstract class AEffect {

	public int duration_current_ms = 0, duration_absolute_ms = 0;

	protected transient Emission emission;

	protected transient IEmitter emitter;

	protected transient IEmitter target;

	/**
	 * target representation as {@link PhysicalClient}, might be <code>null</code>
	 */
	protected transient PhysicalClient target_player;

	/**
	 * target representation as {@link PhysicalCreeps}, might be <code>null</code>
	 */
	protected transient PhysicalCreep target_creep;

	protected transient boolean only_with_combo = false;

	public AEffect() {
	}

	/*
	 * 1. -- Emission am Character (Partikeleffekt, zB Donner von lightning skill1)
	 */
	/*
	 * 2. Bei zeitlichen Effekten (hots / dots): 2.1 --thumbnail oberhalb der großen
	 * Healthbar 2.2 --timebar oberhalb der großen Healthbar
	 */
	/*
	 * 3. Bei CC Effekten (nur bei negativen Effekten): 3.1 --Partikeleffekt
	 * oberhalb des Characters 3.2 --Timebar oberhalb des Characters 3.3 --Font
	 * (STUNNED; ROOTED; ...) 3.4 --prio index
	 */

	public AEffect(int durationMS) {
		duration_current_ms = duration_absolute_ms = durationMS;
	}

	/**
	 * Some effects will only be applied within a combination, see
	 * {@link StoneSkill1} for example.
	 * 
	 * @param <T>
	 * @return
	 */
	public <T extends AEffect> T setOnlyWithCombo() {
		only_with_combo = true;
		return (T) this;
	}

	public AEffect(AEffect effect) {
		duration_current_ms = effect.duration_current_ms;
		duration_absolute_ms = effect.duration_absolute_ms;
		emitter = effect.emitter;
		target = effect.target;
		target_player = effect.target_player;
		target_creep = effect.target_creep;
		only_with_combo = effect.only_with_combo;
		emission = effect.emission;
	}

	public void scaleCombo(float ratio) {
		duration_absolute_ms = (int) (duration_absolute_ms * ratio);
		duration_current_ms = (int) (duration_current_ms * ratio);

	}

	public void apply() {

		if (target_player != null)
			applyPlayer(target_player.getLocation(), target_player);
		else if (target_creep != null)
			applyCreep(target_creep.getLocation(), target_creep);

	}

	/**
	 * Will be called at the beginning of an effect. If it's an effect timed then it
	 * also will be called every tick.
	 * 
	 * @param location
	 * @param targetPlayer
	 */
	protected abstract void applyPlayer(Location location, PhysicalClient targetPlayer);

	protected abstract void applyCreep(Location location, PhysicalCreep targetCreep);

	public abstract <T extends AEffect> T makeCopy();

	public void setTarget(IEmitter target) {
		this.target = target;
		if (target instanceof PhysicalCreep)
			target_creep = (PhysicalCreep) target;
		else if (target instanceof PhysicalClient)
			target_player = (PhysicalClient) target;
	}

	public void reset() {
		duration_current_ms = duration_absolute_ms;
	}

	public float getDurationRatio() {
		return ((float) duration_current_ms) / duration_absolute_ms;
	}

	public int getDurationMS() {
		return duration_current_ms;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

	/**
	 * emitter is equals to the one who executes the emission.
	 * 
	 * @param emitter
	 */
	public void setEmission(Emission emission) {
		this.emission = emission;
		this.emitter = emission.getEmitter();
	}

	public IEmitter getEmitter() {
		return emitter;
	}

	/**
	 * by default <code>false</code>.
	 * 
	 * @return
	 */
	public boolean isOnlyWithCombo() {
		return only_with_combo;
	}

}
