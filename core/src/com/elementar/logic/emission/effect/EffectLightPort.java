package com.elementar.logic.emission.effect;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.PhysicalClient.Location;

public class EffectLightPort extends EffectStun {

	public EffectLightPort() {
	}

	/**
	 * @param poolID
	 * @param delayInMS
	 */
	public EffectLightPort(short poolID, int delayInMS) {
		super(poolID, delayInMS);
		upper_particle_effect = null;
		animation_begin = AnimationName.LIGHTPORT;
	}

	public EffectLightPort(EffectLightPort effect) {
		super(effect);
	}

	@Override
	public void scaleCombo(float ratio) {
		// no scaling
	}

	@Override
	protected boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer) {
		if (target_player != null && emitter != null)
			target_player.setPosition(emitter.getPos().x, emitter.getPos().y);
		super.applyLastTickPlayer(location, targetPlayer);

		return true;

	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {

		if (targetPlayer == emitter) {
			animation_begin = null;
			killEffect();
		} else {
			targetPlayer.applyLinearImpulseToIdle();
			super.applyPlayer(location, targetPlayer);
		}
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectLightPort(this);
	}

}
