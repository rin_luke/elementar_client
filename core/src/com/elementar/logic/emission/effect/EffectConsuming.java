package com.elementar.logic.emission.effect;

import java.util.ArrayList;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectConsuming extends AEffect {

	private transient ArrayList<Class<? extends AEffectTimed>> consuming_effect_types;

	public EffectConsuming(Class<? extends AEffectTimed>... classes) {
		super(0);

		consuming_effect_types = new ArrayList<>();
		for (int i = 0; i < classes.length; i++)
			consuming_effect_types.add(classes[i]);
	}

	public EffectConsuming(EffectConsuming effect) {
		super(effect);
		consuming_effect_types = effect.consuming_effect_types;
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {
		for (AEffectTimed effect : targetPlayer.getEffectsTimed())
			for (Class<? extends AEffectTimed> effectClass : consuming_effect_types)
				if (effect.getClass().equals(effectClass) == true) {
					effect.applyAfterConsumption(targetPlayer);
					effect.killEffect();
					break;
				}

	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
		for (AEffectTimed effect : targetCreep.getEffectsTimed())
			for (Class<? extends AEffectTimed> effectClass : consuming_effect_types)
				if (effect.getClass().equals(effectClass) == true) {
					effect.killEffect();
					break;

				}

	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectConsuming(this);
	}

}
