package com.elementar.logic.emission.effect;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;
import com.elementar.logic.network.gameserver.GameserverLogic;
import com.elementar.logic.util.Shared.Team;

public class EffectDying extends AEffect {

	public EffectDying() {
		super(0);
	}

	public EffectDying(EffectDying effect) {
		super(effect);
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectDying(this);
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {

		/*
		 * following line disables hitboxes and the player doesn't move anymore (gravity
		 * also doesn't effect the velocity)
		 */
		targetPlayer.getHitbox().getBody().setActive(false);

		if (location != Location.CLIENTSIDE_MULTIPLAYER) {

			/*
			 * does the player has an effect which increases death time?
			 */
			short multiplier = 1;
			for (AEffectTimed effect : targetPlayer.getEffectsTimed())
				if (effect instanceof EffectDeathTime) {
					multiplier = ((EffectDeathTime) effect).getMultiplier();

				}
			targetPlayer.death_time_current = targetPlayer.death_time_absolute * multiplier;
			targetPlayer.info_animations.track_4 = AnimationName.DEATH_EXPLOSION;

			if (location == Location.SERVERSIDE)
				if (targetPlayer.getTeam() == Team.TEAM_1)
					GameserverLogic.TEAM1_REMAINING_LIVES.decrementAndGet();
				else
					GameserverLogic.TEAM2_REMAINING_LIVES.decrementAndGet();

		}
	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {

		if (location != Location.CLIENTSIDE_MULTIPLAYER) {
			targetCreep.info_animations.track_4 = AnimationName.DEATH_EXPLOSION;
			targetCreep.getHitbox().getBody().setActive(false);
		}

	}
}