package com.elementar.logic.emission.effect;

import com.elementar.data.container.ParticleContainer;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectStun extends AEffectCC {

	public EffectStun() {
	}

	/**
	 * 
	 * @param durationMS
	 */
	public EffectStun(short poolID, int durationMS) {
		super(poolID, durationMS, ParticleContainer.particle_stunned, "stunned", 0);
	}

	public EffectStun(EffectStun effect) {
		super(effect);
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {
		targetPlayer.cancelCurrentSkill();
		targetPlayer.setStunned(true);
	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
		targetCreep.setStunned(true);
	}

	@Override
	protected boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer) {
		targetPlayer.setStunned(false);
		return true;
	}

	@Override
	protected boolean applyLastTickCreep(Location location, PhysicalCreep targetCreep) {
		targetCreep.setStunned(false);
		return true;
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectStun(this);
	}

}
