package com.elementar.logic.emission.effect;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectDamage extends AEffectWithNumber {

	public EffectDamage() {

	}

	/**
	 * 
	 * @param damageAmount has to be a negative number
	 */
	public EffectDamage(int damageAmount) {
		super(0, damageAmount);
	}

	public EffectDamage(EffectDamage effect) {
		super(effect);
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {

		if (location != Location.CLIENTSIDE_MULTIPLAYER) {
			targetPlayer.health_current += amount;

			if (targetPlayer.isAlive() == true && targetPlayer.isStunned() == false)
				targetPlayer.info_animations.track_2 = AnimationName.RECEIVE_DAMAGE;

			// otherwise player is dead, don't use receive animation

		}

		if (location != Location.SERVERSIDE) {
			targetPlayer.addEffectWithNumber(this);
		}
	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
		if (location != Location.CLIENTSIDE_MULTIPLAYER) {
			targetCreep.health_current += amount;

			if (targetCreep.isAlive() == true) {
				// aggro pull
				if (targetCreep.getSlayer() != null) {

					if (targetCreep.getCurrentTarget() == null)
						targetCreep.info_animations.track_1 = AnimationName.CREEP_FIGHT_STANCE_ON;

					targetCreep.setCurrentTarget(targetCreep.getSlayer());

				}
				if (targetCreep.isStunned() == false)
					targetCreep.info_animations.track_2 = AnimationName.RECEIVE_DAMAGE;
			}
			// otherwise player is dead, don't use receive animation
		}
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectDamage(this);
	}

	@Override
	public String toString() {
		return super.toString() + " [" + amount + " dmg]";
	}

}
