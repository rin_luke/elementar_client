package com.elementar.logic.emission.effect;

import com.elementar.data.container.util.CustomParticleEffect;

public abstract class AEffectCC extends AEffectTimed {

	public AEffectCC() {
	}

	public AEffectCC(short poolID, int durationMS, CustomParticleEffect upperParticleEffect, String textKey,
			int priority) {
		super(poolID, durationMS, upperParticleEffect, textKey);
		this.priority = priority;
	}

	public AEffectCC(AEffectCC effect) {
		super(effect);
	}

}
