package com.elementar.logic.emission.effect;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author lukassongajlo
 * 
 */
public abstract class AEffectWithNumber extends AEffect {

	public short amount;
	public String number_as_string;

	private transient Vector2 tweened_pos;
	private transient BitmapFont font;
	private transient boolean tween_finished = false;
	private transient boolean flip_y;
	private transient Vector2 vertex = new Vector2();
	private transient Vector2 tween_target_pos = new Vector2();

	public AEffectWithNumber() {
	}

	/**
	 * 
	 * @param durationMS
	 * @param amount     , is used to build a string out of it.
	 */
	public AEffectWithNumber(int durationMS, int amount) {
		super(durationMS);

		setAmount((short) amount);
	}

	public AEffectWithNumber(AEffectWithNumber effect) {
		super(effect);
		amount = effect.amount;
		tweened_pos = effect.tweened_pos;
		font = effect.font;
		number_as_string = effect.number_as_string;
		flip_y = effect.flip_y;
		vertex.set(effect.vertex);
		tween_target_pos.set(effect.tween_target_pos);
	}

	@Override
	public void scaleCombo(float ratio) {
		super.scaleCombo(ratio);

		setAmount((short) (amount * ratio));

	}

	private void setAmount(short amount) {
		this.amount = (short) amount;

		number_as_string = String.valueOf(Math.abs(amount));

		if (amount > 0)
			number_as_string = "+" + number_as_string;
	}

	/**
	 * set {@link #tween_finished} to <code>true</code> which leads to removal from
	 * the effects list.
	 */
	public void setTweenFinished() {
		tween_finished = true;
	}

	public boolean isTweenFinished() {
		return tween_finished;
	}

	public void setTweenedPos(Vector2 pos) {
		this.tweened_pos = pos;
	}

	public Vector2 getTweenedPos() {
		return tweened_pos;
	}

	public String getNumberAsString() {
		return number_as_string;
	}

	public void setFont(BitmapFont font) {
		this.font = font;
	}

	/**
	 * returns the font for the numbers you see when e.g. damage is dealt, healing
	 * is received etc.
	 * 
	 * @return
	 */
	public BitmapFont getFont() {
		return font;
	}

	public void setTweenTargetPos(float x, float y) {
		this.tween_target_pos.set(x, y);
	}

	public Vector2 getTweenTargetPos() {
		return tween_target_pos;
	}

	public void setVertex(float x, float y) {
		this.vertex.set(x, y);
	}

	public Vector2 getVertex() {
		return vertex;
	}

	public void setFlipY(boolean flipY) {
		flip_y = flipY;
	}

	/**
	 * depending on this variable the tweening output will be multiplied by -1 or 1.
	 * 
	 * @return
	 */
	public boolean isFlipY() {
		return flip_y;
	}
}
