package com.elementar.logic.emission.effect;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;
import com.elementar.logic.util.Util;

public class EffectImpulseEmitterFollowing extends AEffectImpulse {

	public EffectImpulseEmitterFollowing() {
	}

	public EffectImpulseEmitterFollowing(short poolID, int durationMS, float amplitude) {
		super(poolID, durationMS, amplitude);
	}

	public EffectImpulseEmitterFollowing(EffectImpulseEmitterFollowing effect) {
		super(effect);
	}

	@Override
	protected void updateImpulse() {
		super.updateImpulse();

		if (target_player != null) {
			target_player.getBody().setGravityScale(0);
			target_player.setBrakeEnabled(false);
			/*
			 * exploiting the jumping constraint to avoid animation changes and to avoid
			 * invoking of certain logical code parts that are responsible for movement
			 */
			target_player.getJumping().jump_active = true;
			target_player.setUnderImpulse(true);
		}

	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {

		updateImpulse();

		targetPlayer.applyLinearImpulseToIdle();
		float distanceCursor = targetPlayer.pos.dst(emitter.getPos());
		if (distanceCursor < 0.5f)
			return;

		float lerpFactor = distanceCursor / 1.6f;
		if (lerpFactor > 1f)
			lerpFactor = 1f;

		Vector2 impulse = Util.getPolarCoords(amplitude * lerpFactor,
				Util.getAngleBetweenTwoPoints(targetPlayer.pos, emitter.getPos()));
		targetPlayer.applyLinearImpulse(impulse);

	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
	}

	@Override
	protected boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer) {

		if (super.applyLastTickPlayer(location, targetPlayer) == true) {
			targetPlayer.getBody().setGravityScale(1);
			targetPlayer.setBrakeEnabled(true);
			targetPlayer.setUnderImpulse(false);
			targetPlayer.applyLinearImpulseToIdle();
		}
		return true;

	}

	@Override
	protected boolean applyLastTickCreep(Location location, PhysicalCreep targetCreep) {
		if (super.applyLastTickCreep(location, targetCreep) == true) {

		}

		return true;
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectImpulseEmitterFollowing(this);
	}

}
