package com.elementar.logic.emission.effect;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectEmpty extends AEffect {

	public EffectEmpty() {
		
	}

	public EffectEmpty(EffectEmpty effect) {
		super(effect);
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectEmpty(this);
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {

	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
		
	}

}
