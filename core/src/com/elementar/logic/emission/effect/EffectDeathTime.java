package com.elementar.logic.emission.effect;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectDeathTime extends AEffectTimed {

	public short multiplier;

	public EffectDeathTime() {
	}

	/**
	 * 
	 * @param poolID
	 * @param durationMS
	 * @param multiplier
	 * @param animationBegin
	 * @param animationEnd
	 */
	public EffectDeathTime(short poolID, int durationMS, short multiplier) {
		super(poolID, durationMS, null, "");
		this.multiplier = multiplier;
		kill_after_death = false;
	}

	public EffectDeathTime(EffectDeathTime effect) {
		super(effect);
		multiplier = effect.multiplier;
	}

	public short getMultiplier() {
		return multiplier;
	}

	@Override
	protected void applyAfterConsumption(PhysicalClient targetPlayer) {
		targetPlayer.info_animations.track_3 = animation_end;
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {

	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
	}

	@Override
	protected boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer) {
		/*
		 * we want to end the animation if no other effect of the same kind is carried
		 * by the target. Otherwise if we would override track3 carelessly the animation
		 * begin of the other effects get overridden (-> e.g. the waterghost disappears
		 * while having the resurrection or death time effect are still carried).
		 */
		boolean endValid = true;

		for (AEffectTimed effect : targetPlayer.getEffectsTimed()) {
			if (effect != this && effect.getClass().equals(getClass()) == true) {
				// does the other effect has the same beginning animation?
				if (effect.animation_begin == animation_begin)
					endValid = false;
			}
		}

		if (endValid == true)
			targetPlayer.info_animations.track_3 = animation_end;

		return true;

	}

	@Override
	protected boolean applyLastTickCreep(Location location, PhysicalCreep targetCreep) {
		return true;
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectDeathTime(this);
	}
}
