package com.elementar.logic.emission.effect;

import java.util.Iterator;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.characters.abstracts.AClient;
import com.elementar.logic.characters.skills.ASkill;
import com.elementar.logic.characters.skills.ComboStorage;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectEnableCombining extends AEffectTimed {

	private static final int COMBINING_TIME_MS = 15000;

	private ComboStorage corresponding_combo_storage;

	private String skill_order;

	public EffectEnableCombining(String skillOrder) {
		super((short) -1, COMBINING_TIME_MS, null, "");
		skill_order = skillOrder;
	}

	public EffectEnableCombining(EffectEnableCombining effect) {
		super(effect);
		skill_order = effect.skill_order;
	}

	@Override
	public void update(Iterator<AEffectTimed> it) {
		super.update(it);

		if (corresponding_combo_storage != null)
			corresponding_combo_storage.duration_ratio = getDurationRatio();

	}

	@Override
	protected boolean updateTick() {

		if (corresponding_combo_storage != null)
			if (corresponding_combo_storage.isStoring() == true)
				return true;
		return false;
	}

	@Override
	protected void lastTick() {

		if (corresponding_combo_storage != null) {
			corresponding_combo_storage.clear();
		}
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {

		corresponding_combo_storage = null;
		ComboStorage comboStorage1 = targetPlayer.getComboStorage1();
		ComboStorage comboStorage2 = targetPlayer.getComboStorage2();

		if (emitter instanceof AClient == false)
			return;

		// skill1 and skill2 are the main skill
		ASkill correspondingMainSkill = null;
		if (skill_order.equals("skill1") == true)
			correspondingMainSkill = ((AClient) emitter).getSkill1();
		else if (skill_order.equals("skill2") == true)
			correspondingMainSkill = ((AClient) emitter).getSkill2();

		/*
		 * does one storage already have the skill?
		 */
		if (containsAlready(comboStorage1, correspondingMainSkill, targetPlayer) == true)
			corresponding_combo_storage = comboStorage1;
		else if (containsAlready(comboStorage2, correspondingMainSkill, targetPlayer) == true)
			corresponding_combo_storage = comboStorage2;

		if (corresponding_combo_storage == null)
			// no storage contains the skill
			corresponding_combo_storage = comboStorage1.isStoring() == false ? comboStorage1 : comboStorage2;

		/*
		 * if the projectile of the transfer skill touches the player the combo storage
		 * of the player will be set by the corresponding skill
		 */
		System.out.println("EffectEnableCombining.applyPlayer()			set combo storage#"
				+ corresponding_combo_storage.getIndex() + " with skill: " + correspondingMainSkill);
		corresponding_combo_storage.setSkill(correspondingMainSkill);
	}

	private boolean containsAlready(ComboStorage comboStorage, ASkill correspondingSkill, PhysicalClient targetPlayer) {
		if (comboStorage.isStoring() == true
				&& comboStorage.meta_skill_id == correspondingSkill.getMetaSkill().getMetaSkillID()) {

			// remove old effect
			Iterator<AEffectTimed> it = targetPlayer.getEffectsTimed().iterator();
			while (it.hasNext() == true) {
				AEffectTimed oldEffect = it.next();

				if (oldEffect instanceof EffectEnableCombining)
					if (((EffectEnableCombining) oldEffect).corresponding_combo_storage == comboStorage)
						it.remove();

			}

			return true;

		}
		return false;
	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {

	}

	@Override
	protected boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer) {
		return true;
	}

	@Override
	protected boolean applyLastTickCreep(Location location, PhysicalCreep targetCreep) {
		return true;
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectEnableCombining(this);
	}

}
