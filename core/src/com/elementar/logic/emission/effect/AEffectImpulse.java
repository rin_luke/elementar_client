package com.elementar.logic.emission.effect;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.characters.skills.lightning.LightningSkill2;
import com.elementar.logic.characters.skills.wind.WindSkill1;
import com.elementar.logic.characters.skills.wind.WindSkill2;
import com.elementar.logic.creeps.PhysicalCreep;

public abstract class AEffectImpulse extends AEffectTimed {

	public float amplitude;

	/**
	 * if the impulse emerges from a movement skill like {@link LightningSkill2} or
	 * {@link WindSkill2} we want to stop the impulse after getting rooted or
	 * stunned. Otherwise if the impulse hit the target as in {@link WindSkill1} the
	 * root and stun effects should remain.
	 */
	public boolean movement_impulse = false;

	public AEffectImpulse() {
	}

	public AEffectImpulse(short poolID, int durationMS, float amplitude) {
		super(poolID, durationMS, null, "");
		this.amplitude = amplitude;
	}

	public AEffectImpulse(AEffectImpulse effect) {
		super(effect);
		amplitude = effect.amplitude;
		movement_impulse = effect.movement_impulse;
	}

	@Override
	protected boolean updateTick() {
		super.updateTick();

		updateImpulse();

		return true;
	}

	/**
	 * method to gather common code for impulses
	 */
	protected void updateImpulse() {

		if (target_player != null) {

			/*
			 * if target player is dead, rooted or stunned, the impulse (category =
			 * movement) has to be removed.
			 */
			if (movement_impulse == true && (/*target_player.isRooted() == true ||*/ target_player.isStunned() == true))
				killEffect();

		}

	}

	@Override
	protected boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer) {
		if (target_player != null) {
			for (AEffectTimed effect : target_player.getEffectsTimed()) {
				if (effect == this)
					continue;

				// just for other effects
				if (effect instanceof AEffectImpulse)
					return false;

			}
		}

		return true;

	}

	@Override
	protected boolean applyLastTickCreep(Location location, PhysicalCreep targetCreep) {
		if (target_creep != null) {
			for (AEffectTimed effect : target_creep.getEffectsTimed()) {
				if (effect == this)
					continue;

				// just for other effects
				if (effect instanceof AEffectImpulse)
					return false;

			}
		}

		return true;

	}

	public AEffectImpulse setMovementImpulse() {
		movement_impulse = true;
		return this;
	}
}
