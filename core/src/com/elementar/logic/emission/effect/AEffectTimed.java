package com.elementar.logic.emission.effect;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.elementar.data.container.FontData;
import com.elementar.data.container.StaticGraphic;
import com.elementar.data.container.TextData;
import com.elementar.data.container.util.CustomParticleEffect;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.gui.util.SharedColors;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;
import com.elementar.logic.util.Shared;

public abstract class AEffectTimed extends AEffect implements Comparable<AEffectTimed> {

	public short pool_id = -1;

	/**
	 * the smaller the higher prioritized. Necessary for draw order. For example the
	 * stun effect should have the highest priority. If two effects with the same
	 * priority value have to be compared, the one with the higher duration should
	 * be picked for drawing the effect.
	 */
	protected transient int priority = Integer.MAX_VALUE;

	/**
	 * for example the root effect has a particle effect which is shown above the
	 * character
	 */
	protected transient CustomParticleEffect upper_particle_effect;

	/**
	 * with this key the corresponding string in a language file can be found.
	 */
	protected transient String text_key;

	protected transient AnimationName animation_color_change, animation_begin, animation_end;

	/**
	 * might be <code>null</code>!
	 */
	protected transient Sprite thumbnail_sprite;

	protected transient boolean red_frame;
	protected transient Sprite frame_sprite;

	protected transient boolean stackable = false;

	/**
	 * <code>true</code> by default. In general an effect should be killed after the
	 * death of its target is recognized, though there are some effects which behave
	 * differently like {@link EffectResurrection}.
	 */
	protected transient boolean kill_after_death;

	protected transient GlyphLayout glyph_layout = new GlyphLayout();

	protected transient int delay_ms;

	public AEffectTimed() {
	}
	
	/**
	 * 
	 * @param poolID
	 * @param durationMS
	 * @param upperParticleEffect
	 * @param textKey,            key for dictionary file.
	 */
	public AEffectTimed(short poolID, int durationMS, CustomParticleEffect upperParticleEffect, String textKey) {
		super(durationMS);
		pool_id = poolID;
		upper_particle_effect = upperParticleEffect;
		text_key = textKey;
		kill_after_death = true;
	}

	public AEffectTimed(AEffectTimed effect) {
		super(effect);
		if (effect.upper_particle_effect != null)
			upper_particle_effect = new CustomParticleEffect(effect.upper_particle_effect);
		text_key = effect.text_key;
		priority = effect.priority;
		thumbnail_sprite = effect.thumbnail_sprite;
		red_frame = effect.red_frame;
		frame_sprite = effect.frame_sprite;
		animation_color_change = effect.animation_color_change;
		animation_begin = effect.animation_begin;
		animation_end = effect.animation_end;
		pool_id = effect.pool_id;
		stackable = effect.stackable;
		delay_ms = effect.delay_ms;
	}

	public <T extends AEffectTimed> void setTransientData(T effect) {

		if (effect.upper_particle_effect != null)
			upper_particle_effect = new CustomParticleEffect(effect.upper_particle_effect);
		text_key = effect.text_key;
		priority = effect.priority;
		thumbnail_sprite = effect.thumbnail_sprite;
		red_frame = effect.red_frame;
		frame_sprite = effect.frame_sprite;
		animation_color_change = effect.animation_color_change;
		animation_begin = effect.animation_begin;
		animation_end = effect.animation_end;
		pool_id = effect.pool_id;
		stackable = effect.stackable;
		delay_ms = effect.delay_ms;

	}

	/**
	 * 
	 * @param <T>
	 * @param animationColorChange
	 * @return
	 */
	public <T extends AEffectTimed> T setAnimationColorChange(AnimationName animationColorChange) {
		animation_color_change = animationColorChange;
		return (T) this;
	}

	/**
	 * {@link #stackable} is by default <code>false</code>. Calling this method set
	 * this field to <code>true</code>.
	 * 
	 * @param <T>
	 * @return
	 */
	public <T extends AEffectTimed> T setStackable() {
		stackable = true;
		return (T) this;
	}

	public boolean isStackable() {
		return stackable;
	}

	/**
	 * 
	 * @param <T>
	 * @param delayMS
	 * @return
	 */
	public <T extends AEffectTimed> T setDelayMS(int delayMS) {
		delay_ms = delayMS;
		return (T) this;
	}

	public int getDelayMS() {
		return delay_ms;
	}

	/**
	 * 
	 * @param <T>
	 * @param animationBegin
	 * @return
	 */
	public <T extends AEffectTimed> T setAnimationBegin(AnimationName animationBegin) {
		animation_begin = animationBegin;
		return (T) this;
	}

	/**
	 * 
	 * @param <T>
	 * @param animationEnd
	 * @return
	 */
	public <T extends AEffectTimed> T setAnimationEnd(AnimationName animationEnd) {
		animation_end = animationEnd;
		return (T) this;
	}

	/**
	 * 
	 * @param thumbnail skill thumbnail, might be <code>null</code>. Consider that
	 *                  only one effect of a skill should has a thumbnail. Otherwise
	 *                  the thumbnail will be drawn multiple times
	 * @param redFrame  if <code>true</code> the effect is a debuff showed by a red
	 *                  frame, otherwise the effect is a buff showed by a green
	 *                  frame
	 * @return
	 */
	public <T extends AEffectTimed> T setThumbnail(Sprite thumbnail, boolean redFrame) {
		if (thumbnail != null) {
			thumbnail_sprite = new Sprite(thumbnail);
			thumbnail_sprite.setSize(50, 50);
			red_frame = redFrame;
			if (redFrame == true)
				frame_sprite = new Sprite(StaticGraphic.gui_frame_debuff);
			else
				frame_sprite = new Sprite(StaticGraphic.gui_frame_buff);
		}
		return (T) this;
	}

	@Override
	public void apply() {
		super.apply();

		/*
		 * add effect for calling continuously update()
		 */
		if (target != null) {
			target.addEffect(this);

			if (delay_ms > 0)
				return;

			if (target_player != null)
				applyPlayerBeginning(target_player.getLocation(), target_player);
			else if (target_creep != null)
				applyCreepBeginning(target_creep.getLocation(), target_creep);

			// start upper effect
			if (target.getLocation() != Location.SERVERSIDE && upper_particle_effect != null)
				upper_particle_effect.start();

		}

	}

	/**
	 * Will be called at the beginning of a timed effect.
	 * 
	 * @param location
	 * @param targetPlayer, <code>null</code>-check is already done
	 */
	protected void applyPlayerBeginning(Location location, PhysicalClient targetPlayer) {
		if (targetPlayer.isAlive() == true && animation_begin != null)
			targetPlayer.info_animations.track_3 = animation_begin;

	}

	/**
	 * 
	 * @param location
	 * @param targetCreep, <code>null</code>-check is already done
	 */
	protected void applyCreepBeginning(Location location, PhysicalCreep targetCreep) {
		if (targetCreep.isAlive() == true && animation_begin != null)
			targetCreep.info_animations.track_3 = animation_begin;

	}

	/**
	 * 
	 * @return <code>false</code> if the effect should be removed from the list
	 */
	protected boolean updateTick() {
		if (target_player != null) {
			applyPlayer(target_player.getLocation(), target_player);
			if (target_player.isAlive() == true && animation_color_change != null)
				target_player.info_animations.track_3 = animation_color_change;

		} else if (target_creep != null) {
			applyCreep(target_creep.getLocation(), target_creep);
			if (target_creep.isAlive() == true && animation_color_change != null)
				target_creep.info_animations.track_3 = animation_color_change;

		}
		return true;
	}

	/**
	 * After the effect ends. Use this method to undo effects like stun
	 */
	protected void lastTick() {
		if (target_player != null) {
			applyLastTickPlayer(target_player.getLocation(), target_player);
			if (animation_color_change != null)
				// reset color
				target_player.info_animations.track_3 = AnimationName.COLOR_DEFAULT;
			if (animation_end != null)
				handleEndAnimation(target_player);
		} else if (target_creep != null) {
			applyLastTickCreep(target_creep.getLocation(), target_creep);
			if (animation_color_change != null)
				// reset color
				target_creep.info_animations.track_3 = AnimationName.COLOR_DEFAULT;
			if (animation_end != null)
				target_creep.info_animations.track_3 = animation_end;
		}
	}

	private void handleEndAnimation(PhysicalClient targetPlayer) {
		/*
		 * we want to end the animation if no other effect of the same kind is carried
		 * by the target. Otherwise if we would override track3 carelessly the animation
		 * begin of the other effects get overridden (-> e.g. the waterghost disappears
		 * while having the resurrection or death time effect are still carried). This
		 * is caused by the order. Imagine you have effect1 active and now a second
		 * effect2 comes in, which is of the same, then the animation_begin of the new
		 * effect is called first and afterwards we call animation_end.
		 */
		boolean endValid = true;

		for (AEffectTimed effect : targetPlayer.getEffectsTimed()) {
			if (effect != this && effect.getClass().equals(getClass()) == true) {
				// does the other effect has the same beginning animation?
				if (effect.animation_begin == animation_begin)
					endValid = false;
			}
		}

		if (endValid == true)
			targetPlayer.info_animations.track_3 = animation_end;
	}

	protected void applyAfterConsumption(PhysicalClient targetPlayer) {

	}

	/**
	 * 
	 * @param location
	 * @param targetPlayer
	 * @return needed especially for {@link AEffectImpulse}
	 */
	protected abstract boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer);

	/**
	 * 
	 * @param location
	 * @param targetCreep
	 * @return necessary especially for {@link AEffectImpulse}
	 */
	protected abstract boolean applyLastTickCreep(Location location, PhysicalCreep targetCreep);

	public void update(Iterator<AEffectTimed> it) {

		if (target != null && target.isAlive() == false && kill_after_death == true)
			killEffect();

		boolean delayActive = delay_ms > 0;
		delay_ms -= Shared.SEND_AND_UPDATE_RATE_IN_MS;
		if (delay_ms > 0)
			return;

		if (delayActive == true) {
			// one time recognition
			if (target_player != null)
				applyPlayerBeginning(target_player.getLocation(), target_player);
			else if (target_creep != null)
				applyCreepBeginning(target_creep.getLocation(), target_creep);

			// start upper effect
			if (target.getLocation() != Location.SERVERSIDE && upper_particle_effect != null)
				upper_particle_effect.start();
		}

		duration_current_ms -= Shared.SEND_AND_UPDATE_RATE_IN_MS;
		if (duration_current_ms <= 0) {
			it.remove();
			lastTick();
			return;
		} else {
			if (updateTick() == false)
				// used by combining effect only
				it.remove();

		}

	}

	/**
	 * this method is called when the target has died or the same effect is in the
	 * queue and is replacing this one.
	 */
	public void killEffect() {
		duration_current_ms = -1;
	}

	public void drawUpperParticle(ShaderPolyBatch batch, float x, float y) {
		/*
		 * whether upper effect is null or not is checked before this draw() method is
		 * called.
		 */
		if (/* upper_particle_effect != null && */target != null && target.isAlive() == true) {
			upper_particle_effect.setPosition(x, y);
			upper_particle_effect.draw(batch, Gdx.graphics.getDeltaTime());
		}
	}

	/**
	 * Draws a little frame (red or green depending on whether it's a negative or
	 * positive effect) with a time bar and the corresponding skill thumbnail. Draws
	 * a number for the stacks.
	 * 
	 * @param batch
	 * @param index
	 * @param stageCam
	 * @param shapeRenderer
	 * @param numberStacks
	 */
	public void drawFrame(ShaderPolyBatch batch, int index, Camera stageCam, ShapeRenderer shapeRenderer,
			int numberStacks) {

		float x = stageCam.position.x - 220 + index * (frame_sprite.getWidth() + 5);
		float y = 190;
		thumbnail_sprite.setPosition(x + 2, y + 2);
		frame_sprite.setSize(thumbnail_sprite.getWidth() + 4, thumbnail_sprite.getHeight() + 4);
		frame_sprite.setPosition(x, y);

		thumbnail_sprite.draw(batch);
		frame_sprite.draw(batch);

		if (numberStacks > 0) {
			// draw number stacks
			glyph_layout.setText(FontData.font_bold_18_bordered, Integer.toString(numberStacks));
			FontData.font_bold_18_bordered.draw(batch, glyph_layout, thumbnail_sprite.getX(),
					thumbnail_sprite.getY() + glyph_layout.height);
		}
		// draw time bar
		float barHeight = 7;

		shapeRenderer.setColor(SharedColors.BLACK);
		shapeRenderer.rect(x, y - barHeight, frame_sprite.getWidth(), barHeight);

		if (red_frame == true)
			shapeRenderer.setColor(SharedColors.RED_3);
		else
			shapeRenderer.setColor(SharedColors.GREEN_3);

		shapeRenderer.rect(x, y - barHeight, frame_sprite.getWidth() * getDurationRatio(), barHeight);

	}

	public Sprite getThumbnailSprite() {
		return thumbnail_sprite;
	}

	public String getText() {
		return TextData.getWord(text_key);
	}

	public void setPoolID(short poolID) {
		pool_id = poolID;
	}

	public short getPoolID() {
		return pool_id;
	}

	/**
	 * The higher the priority and the remaining duration the more we put the effect
	 * at the end of list. The reason why we put longer remaining effects at the end
	 * is when an effect is expiring (= applying its last tick) it typically negates
	 * its impact, so for example if a {@link EffectDisarmed} effect expires but we
	 * still have an {@link EffectDisarmed} object in the list, the one with higher
	 * duration will avoid the negation.
	 */
	@Override
	public int compareTo(AEffectTimed other) {
		if (other.priority < priority)
			return -1;
		else if (other.priority > priority)
			return 1;
		else {
			// if priorities are equal, take duration
			if (other.duration_current_ms <= duration_current_ms)
				return 1;
			else
				return -1;
		}
	}

	public int getPriority() {
		return priority;
	}

	/**
	 * if <code>true</code> this effect might be a CC effect
	 * 
	 * @return
	 */
	public boolean hasUpperParticleEffect() {
		return upper_particle_effect != null;
	}

	@Override
	public String toString() {
		return super.toString() + " duration: [" + duration_current_ms + " ms]/[" + duration_absolute_ms + " ms]";
	}
}
