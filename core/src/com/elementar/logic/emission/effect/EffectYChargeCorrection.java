package com.elementar.logic.emission.effect;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

/**
 * 
 * @author lukassongajlo
 *
 */
public class EffectYChargeCorrection extends AEffect {

	public EffectYChargeCorrection() {
		super(0);
	}

	public EffectYChargeCorrection(EffectYChargeCorrection effect) {
		super(effect);
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {
		targetPlayer.applyLinearImpulse(new Vector2(0, -targetPlayer.getHitbox().getVelocity().y * 0.5f));
	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectYChargeCorrection(this);
	}

}
