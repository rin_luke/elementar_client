package com.elementar.logic.emission.effect;

import com.elementar.data.container.ParticleContainer;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectDisarmed extends AEffectCC {

	public EffectDisarmed() {
	}

	/**
	 * 
	 * @param poolID
	 * @param durationMS
	 */
	public EffectDisarmed(short poolID, int durationMS) {
		super(poolID, durationMS, ParticleContainer.particle_disarmed, "disarmed", 1);
	}

	public EffectDisarmed(EffectDisarmed effect) {
		super(effect);
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {
		targetPlayer.setDisarmed(true);
	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
		targetCreep.setDisarmed(true);
	}

	@Override
	protected boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer) {
		targetPlayer.setDisarmed(false);
		return true;
	}

	@Override
	protected boolean applyLastTickCreep(Location location, PhysicalCreep targetCreep) {
		targetCreep.setDisarmed(false);
		return true;
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectDisarmed(this);
	}

}
