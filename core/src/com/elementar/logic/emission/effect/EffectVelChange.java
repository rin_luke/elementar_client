package com.elementar.logic.emission.effect;

import com.badlogic.gdx.math.MathUtils;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectVelChange extends AEffectTimed {

	public float old_vel;
	public float vel_change_in_percent;

	/**
	 * might be negative in case of a slow or positive if it's speed boost buff.
	 */
	public float change_amount;

	public EffectVelChange() {
	}

	/**
	 * 
	 * @param velChangeInPercent
	 * @param durationMS
	 */
	public EffectVelChange(short poolID, float velChangeInPercent, int durationMS) {
		super(poolID, durationMS, null, "");
		vel_change_in_percent = velChangeInPercent;
	}

	public EffectVelChange(EffectVelChange effect) {
		super(effect);
		old_vel = effect.old_vel;
		vel_change_in_percent = effect.vel_change_in_percent;
		change_amount = effect.change_amount;
	}

	@Override
	protected void applyPlayerBeginning(Location location, PhysicalClient targetPlayer) {
		old_vel = targetPlayer.getVelXFactor();
		change_amount = old_vel * vel_change_in_percent;

		targetPlayer.setVelXFactor(old_vel + change_amount);
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {
	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {

	}

	@Override
	protected boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer) {
		targetPlayer.setVelXFactor(targetPlayer.getVelXFactor() - change_amount);

		if (target_player != null) {
			for (AEffectTimed effect : target_player.getEffectsTimed()) {
				if (effect == this)
					continue;

				// just for other effects
				if (effect instanceof EffectVelChange) {
					EffectVelChange effectVel = (EffectVelChange) effect;
					if (MathUtils.isZero(effectVel.change_amount) == false) {
						// undo previous change...
						targetPlayer.setVelXFactor(targetPlayer.getVelXFactor() - effectVel.change_amount);

						// ... and apply vel-change with new basis.
						effect.applyPlayerBeginning(null, target_player);
					}

				}
			}
		}

		return true;
	}

	@Override
	protected boolean applyLastTickCreep(Location location, PhysicalCreep targetCreep) {
		return true;
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectVelChange(this);
	}

	@Override
	public String toString() {
		return super.toString() + ", [" + (vel_change_in_percent * 100) + " %]";
	}

}
