package com.elementar.logic.emission.effect;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.characters.skills.MyRayCastCallback;
import com.elementar.logic.util.Util;

public class EffectBlackhole extends EffectStun {

	private transient boolean reached_time_mid = false;

	public float min_position_shift, max_position_shift;

	public Vector2 random_wormhole_out_pos;

	public EffectBlackhole() {
	}

	/**
	 * @param poolID
	 * @param delayInMS
	 * @param minPositionShift
	 * @param maxPositionShift
	 */
	public EffectBlackhole(short poolID, int delayInMS, float minPositionShift, float maxPositionShift) {
		/*
		 * delay goes to the middle of the duration, then we see the port and after that
		 * we have the same delay-amount to fade out the particle effect.
		 */
		super(poolID, delayInMS);
		upper_particle_effect = null;
		min_position_shift = minPositionShift;
		max_position_shift = maxPositionShift;
		animation_begin = AnimationName.BLACKHOLE;
	}

	public EffectBlackhole(EffectBlackhole effect) {
		super(effect);
		min_position_shift = effect.min_position_shift;
		max_position_shift = effect.max_position_shift;
		random_wormhole_out_pos = effect.random_wormhole_out_pos;
	}

	@Override
	public void scaleCombo(float ratio) {
		// no scaling
	}

	@Override
	protected boolean updateTick() {
		// do the teleport after having reached the time mid...
		if (reached_time_mid == false)
			if (target_player != null)
				if (duration_current_ms < duration_absolute_ms * 0.5f) {
					reached_time_mid = true;

					if (random_wormhole_out_pos == null)
					/*
					 * when the skill got combined the is no pre calculation of the out-position, so
					 * we do the calculation again
					 */ {
						random_wormhole_out_pos = calcWormholeOutPos(target_player);
					}

					target_player.setPosition(random_wormhole_out_pos.x, random_wormhole_out_pos.y);

				}

		return super.updateTick();
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {
		targetPlayer.applyLinearImpulseToIdle();
		super.applyPlayer(location, targetPlayer);
	}

	public Vector2 calcWormholeOutPos(PhysicalClient emitter) {
		// try out as long as necessary
		do {
			Vector2 posShiftDonut = Util.getPolarCoords(MathUtils.random(min_position_shift, max_position_shift),
					MathUtils.random(360));
			random_wormhole_out_pos = MyRayCastCallback.calcClosestOutPos(emitter,
					Util.addVectors(emitter.pos, posShiftDonut));

		} while (random_wormhole_out_pos.dst(emitter.pos) < min_position_shift);

		return random_wormhole_out_pos;

	}

	public void setWormholePos(Vector2 wormholePos) {
		random_wormhole_out_pos = wormholePos;
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectBlackhole(this);
	}

}
