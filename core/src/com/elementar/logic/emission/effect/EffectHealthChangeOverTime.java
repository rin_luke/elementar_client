package com.elementar.logic.emission.effect;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;
import com.elementar.logic.util.Shared;

public class EffectHealthChangeOverTime extends AEffectTimed {

	public int health_change_per_second;
	public float health_change_per_tick;
	private transient float acc = 0;

	public EffectHealthChangeOverTime() {
	}

	/**
	 * 
	 * @param poolID
	 * @param durationMS
	 * @param healthChangePerSecond
	 */
	public EffectHealthChangeOverTime(short poolID, int durationMS, int healthChangePerSecond) {
		super(poolID, durationMS, null, "");
		health_change_per_second = healthChangePerSecond;
		health_change_per_tick = health_change_per_second * Shared.SEND_AND_UPDATE_RATE_WORLD;
	}

	public EffectHealthChangeOverTime(EffectHealthChangeOverTime effect) {
		super(effect);
		health_change_per_second = effect.health_change_per_second;
		health_change_per_tick = effect.health_change_per_tick;
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {
		/*
		 * since this method is called every update tick we want to accumulate the
		 * damage fractions that occur within a second.
		 */
		if (location != Location.CLIENTSIDE_MULTIPLAYER) {

			acc += health_change_per_tick;

			/*
			 * now we truncate (i.e. drop everything after the decimal dot) and subtract
			 * that from the current health.
			 */
			short truncatedAmount = (short) acc;
			targetPlayer.health_current += truncatedAmount;

			// is heal beyond the absolute value?
			if (targetPlayer.health_current > targetPlayer.getMetaClient().health_absolute)
				targetPlayer.health_current = targetPlayer.getMetaClient().health_absolute;

			acc -= truncatedAmount;

		}
	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
		if (location != Location.CLIENTSIDE_MULTIPLAYER) {

			acc += health_change_per_tick;

			/*
			 * now we truncate (i.e. drop everything after the decimal dot) and subtract
			 * that from the current health.
			 */
			short truncatedDamage = (short) acc;
			targetCreep.health_current -= truncatedDamage;
			acc -= truncatedDamage;

		}

	}

	protected boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer) {
		return true;
	}

	protected boolean applyLastTickCreep(Location location, PhysicalCreep targetCreep) {
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", [" + health_change_per_second + " health / sec]";
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectHealthChangeOverTime(this);
	}

}
