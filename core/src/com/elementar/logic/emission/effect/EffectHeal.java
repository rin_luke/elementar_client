package com.elementar.logic.emission.effect;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectHeal extends AEffectWithNumber {

	public EffectHeal() {
	}

	public EffectHeal(int healAmount) {
		super(0, healAmount);
	}

	public EffectHeal(EffectHeal effect) {
		super(effect);
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {
		if (location != Location.CLIENTSIDE_MULTIPLAYER) {
			targetPlayer.health_current += amount;

			// is heal beyond the absolute value?
			if (targetPlayer.health_current > targetPlayer.getMetaClient().health_absolute)
				targetPlayer.health_current = targetPlayer.getMetaClient().health_absolute;
		}

		if (location != Location.SERVERSIDE)
			targetPlayer.addEffectWithNumber(this);
	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
		if (location != Location.CLIENTSIDE_MULTIPLAYER) {
			targetCreep.health_current += amount;

			// is heal beyond the absolute value?
			if (targetCreep.health_current > targetCreep.health_absolute)
				targetCreep.health_current = targetCreep.health_absolute;
		}
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectHeal(this);
	}

}
