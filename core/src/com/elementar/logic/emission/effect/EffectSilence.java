package com.elementar.logic.emission.effect;

import com.elementar.data.container.ParticleContainer;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectSilence extends AEffectCC {

	public EffectSilence() {
	}

	/**
	 * 
	 * @param poolID
	 * @param durationMS
	 */
	public EffectSilence(short poolID, int durationMS) {
		super(poolID, durationMS, ParticleContainer.particle_silenced, "silenced", 1);
	}

	public EffectSilence(EffectSilence effect) {
		super(effect);
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {
		targetPlayer.setSilenced(true);
	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
	}

	@Override
	protected boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer) {
		targetPlayer.setSilenced(false);
		return true;
	}

	@Override
	protected boolean applyLastTickCreep(Location location, PhysicalCreep targetCreep) {
		return true;
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectSilence(this);
	}

}
