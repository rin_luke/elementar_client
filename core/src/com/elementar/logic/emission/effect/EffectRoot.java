package com.elementar.logic.emission.effect;

import com.elementar.data.container.ParticleContainer;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectRoot extends AEffectCC {

	public EffectRoot() {
	}

	public EffectRoot(short poolID, int durationMS) {
		super(poolID, durationMS, ParticleContainer.particle_rooted, "rooted", 1);
	}

	public EffectRoot(AEffectCC effect) {
		super(effect);
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {
		targetPlayer.setRooted(true);
	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
		targetCreep.setRooted(true);
	}

	@Override
	protected boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer) {
		targetPlayer.setRooted(false);
		return true;
	}

	@Override
	protected boolean applyLastTickCreep(Location location, PhysicalCreep targetCreep) {
		targetCreep.setRooted(false);
		return true;
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectRoot(this);
	}

}
