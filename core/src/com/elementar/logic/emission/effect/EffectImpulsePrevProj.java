package com.elementar.logic.emission.effect;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectImpulsePrevProj extends AEffectImpulse {

	public EffectImpulsePrevProj() {
	}

	public EffectImpulsePrevProj(short poolID, int durationMS) {
		super(poolID, durationMS, 0);
	}

	public EffectImpulsePrevProj(EffectImpulsePrevProj effect) {
		super(effect);
	}

	@Override
	protected void updateImpulse() {
		
		if (emission.getDef().getPrevProjVel() == null)
			return;

		if (MathUtils.isZero(emission.getDef().getPrevProjVel().len()) == true) {
			killEffect();
			return;
		}
		
		super.updateImpulse();

		if (target_player != null) {

			target_player.getBody().setGravityScale(0);
			target_player.setBrakeEnabled(false);
			/*
			 * exploiting the jumping constraint to avoid animation changes and to avoid
			 * invoking of certain logical code parts that are responsible for movement
			 */
			target_player.getJumping().jump_active = true;
			target_player.setUnderImpulse(true);
		}

	}

	@Override
	protected void applyPlayerBeginning(Location location, PhysicalClient targetPlayer) {

		if (emission.getDef().getPrevProjVel() == null)
			return;

		if (MathUtils.isZero(emission.getDef().getPrevProjVel().len()) == true) {
			killEffect();
			return;
		}
		updateImpulse();

		targetPlayer.applyLinearImpulseToIdle();

		Vector2 impulse = new Vector2(emission.getDef().getPrevProjVel());// Util.getPolarCoords(amplitude, angle);
		targetPlayer.applyLinearImpulse(impulse);

	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {

	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
	}

	@Override
	protected boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer) {

		if (super.applyLastTickPlayer(location, targetPlayer) == true) {
			targetPlayer.getBody().setGravityScale(1);
			targetPlayer.setBrakeEnabled(true);
			targetPlayer.setUnderImpulse(false);
			targetPlayer.applyLinearImpulseToIdle();
		}
		return true;

	}

	@Override
	protected boolean applyLastTickCreep(Location location, PhysicalCreep targetCreep) {
		if (super.applyLastTickCreep(location, targetCreep) == true) {

		}

		return true;
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectImpulsePrevProj(this);
	}

}
