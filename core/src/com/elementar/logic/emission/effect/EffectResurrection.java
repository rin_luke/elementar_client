package com.elementar.logic.emission.effect;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectResurrection extends AEffectTimed {

	public short resurrection_health;

	public EffectResurrection() {
	}

	/**
	 * 
	 * 
	 * @param poolID
	 * @param durationMS
	 * @param resurrectionHealth
	 * @param animationBegin
	 * @param animationEnd
	 */
	public EffectResurrection(short poolID, int durationMS, int resurrectionHealth) {
		super(poolID, durationMS, null, "");
		resurrection_health = (short) resurrectionHealth;
		kill_after_death = false;
	}

	public EffectResurrection(EffectResurrection effect) {
		super(effect);
		resurrection_health = effect.resurrection_health;
	}

	@Override
	protected void applyAfterConsumption(PhysicalClient targetPlayer) {
		targetPlayer.health_current = resurrection_health;
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {

	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
	}

	@Override
	protected boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer) {
		return true;
	}

	@Override
	protected boolean applyLastTickCreep(Location location, PhysicalCreep targetCreep) {
		return true;
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectResurrection(this);
	}
}
