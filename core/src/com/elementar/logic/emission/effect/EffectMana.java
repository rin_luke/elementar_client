package com.elementar.logic.emission.effect;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectMana extends AEffectWithNumber {

	public EffectMana() {
	}

	public EffectMana(int healAmount) {
		super(0, healAmount);
	}

	public EffectMana(EffectMana effect) {
		super(effect);
	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {
		if (location != Location.CLIENTSIDE_MULTIPLAYER) {
			targetPlayer.mana_current += amount;

			// is heal beyond the absolute value?
			if (targetPlayer.mana_current > targetPlayer.getMetaClient().mana_absolute)
				targetPlayer.mana_current = targetPlayer.getMetaClient().mana_absolute;
		}

		if (location != Location.SERVERSIDE)
			targetPlayer.addEffectWithNumber(this);
	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectMana(this);
	}

}
