package com.elementar.logic.emission.effect;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.PhysicalCreep;

public class EffectSpawnAfterDeath extends AEffectTimed {

	public EffectSpawnAfterDeath() {
	}

	public EffectSpawnAfterDeath(short poolID, int durationMS) {
		super(poolID, durationMS, null, "");

	}

	public EffectSpawnAfterDeath(EffectSpawnAfterDeath effect) {
		super(effect);
	}

	@Override
	public <T extends AEffect> T makeCopy() {
		return (T) new EffectSpawnAfterDeath(this);
	}

	@Override
	protected void applyPlayerBeginning(Location location, PhysicalClient targetPlayer) {

		targetPlayer.setAlive();

		if (location != Location.CLIENTSIDE_MULTIPLAYER) {
			targetPlayer.info_animations.track_4 = AnimationName.ALPHA_FADE_IN;

			Vector2 spawnPos = targetPlayer.getSpawnPos();
			targetPlayer.setPosition(spawnPos.x, spawnPos.y);

		}

		super.applyPlayerBeginning(location, targetPlayer);

	}

	@Override
	protected void applyPlayer(Location location, PhysicalClient targetPlayer) {

		for (AEffectTimed effect : targetPlayer.getEffectsTimed()) {
			if (effect != this)
				effect.killEffect();
		}

		targetPlayer.health_current = targetPlayer.getMetaClient().health_absolute;

	}

	@Override
	protected void applyCreep(Location location, PhysicalCreep targetCreep) {

	}

	@Override
	protected boolean applyLastTickPlayer(Location location, PhysicalClient targetPlayer) {
		return true;
	}

	@Override
	protected boolean applyLastTickCreep(Location location, PhysicalCreep targetCreep) {
		return true;
	}

}
