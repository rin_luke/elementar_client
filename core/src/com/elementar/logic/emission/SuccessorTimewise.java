package com.elementar.logic.emission;

import com.elementar.logic.emission.def.AEmissionDef;

/**
 * Holds the definition of a successor emission. Triggers by time
 * 
 * @author lukassongajlo
 * 
 */
public class SuccessorTimewise {
	private float elapsed_time_til_trigger;
	private AEmissionDef successor_def;

	public SuccessorTimewise(AEmissionDef successor, float elapsedTimeTilTrigger) {
		this.successor_def = successor;
		elapsed_time_til_trigger = elapsedTimeTilTrigger;
	}

	/**
	 * By default equals 0. Value in percentage.
	 * <p>
	 * An example: if the value is 0.5f the successor starts while the ancestor
	 * emission is in the mid of its emitting.
	 * 
	 * @return
	 */
	public float getElapsedTimeTilTrigger() {
		return elapsed_time_til_trigger;
	}

	public AEmissionDef getDef() {
		return successor_def;
	}

}
