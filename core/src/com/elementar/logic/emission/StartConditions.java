package com.elementar.logic.emission;

import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

/**
 * An object of this class holds a state consisting of 3 bits. They describe
 * when a successor emission (of a died projectile) is allowed to start. Either
 * after the projectile died by lifetime, ground collision, player collision
 * and/or building collision
 * 
 * @author lukassongajlo
 * 
 */
public class StartConditions {

	private int collision_objects = 0;
	private int collision_kinds = 0;

	/**
	 * all settings to zero
	 */
	public StartConditions() {
	}

	/**
	 * Each passed parameter is the sum of all conditions the successor emission
	 * should have.
	 * 
	 * @param collisionObjects , see {@link CollisionData} to find the bits you
	 *                         might want to sum up
	 * @param collisionKinds   , in {@link CollisionData} class you find the
	 *                         {@link CollisionKinds} enum
	 */
	public StartConditions(int collisionObjects, int collisionKinds) {
		collision_objects = collisionObjects;
		collision_kinds = collisionKinds;
	}

	/**
	 * Returns <code>true</code> when the successor emission is allowed to start
	 * after the projectile died via lifetime
	 * 
	 * @return
	 */
	public boolean isAfterLifetime() {
		return (collision_kinds & CollisionKinds.AFTER_LIFETIME.getValue()) != 0;
	}

	/**
	 * Returns <code>true</code> when the successor emission is allowed to start
	 * after a collision with ground
	 * 
	 * @return
	 */
	public boolean isAfterGround() {
		return (collision_objects & CollisionData.BIT_GROUND) != 0;
	}

	/**
	 * Returns <code>true</code> when the successor emission is allowed to start
	 * after a collision with a player
	 * 
	 * @return
	 */
	public boolean isAfterPlayer() {
		return (collision_objects & CollisionData.BIT_CHARACTER_PROJECTILE) != 0;
	}

	/**
	 * Returns <code>true</code> when the successor emission is allowed to start
	 * after a collision with an building
	 * 
	 * @return
	 */
	public boolean isAfterBuilding() {
		return (collision_objects & CollisionData.BIT_BUILDING) != 0;
	}

	// help methods

	public boolean isAfterCastOnAllyExcluded() {
		return (collision_kinds & CollisionKinds.CAST_ON_ALLY_EMITTER_EXCLUDED.getValue()) != 0;
	}

	public boolean isAfterCastOnAllyIncluded() {
		return (collision_kinds & CollisionKinds.CAST_ON_ALLY_EMITTER_INCLUDED.getValue()) != 0;
	}

	public boolean isAfterCastOnEmitter() {
		return (collision_kinds & CollisionKinds.CAST_ON_EMITTER.getValue()) != 0;
	}

	public boolean isAfterCastOnEnemy() {
		return (collision_kinds & CollisionKinds.CAST_ON_ENEMY.getValue()) != 0;
	}

	public boolean isAfterAbsorption() {
		return (collision_kinds & CollisionKinds.AFTER_ABSORPTION.getValue()) != 0;
	}

}
