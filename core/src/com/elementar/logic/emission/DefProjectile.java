package com.elementar.logic.emission;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.elementar.gui.util.CurvedTrajectory;
import com.elementar.logic.characters.skills.ice.IceSkill1;
import com.elementar.logic.characters.skills.stone.StoneSkill1;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.util.RectangleShape;
import com.elementar.logic.emission.util.TargetFinding;
import com.elementar.logic.util.Util;
import com.badlogic.gdx.physics.box2d.Filter;

/**
 * Shared information base by all projectiles of one emission
 * 
 * @author lukassongajlo
 * 
 */
public class DefProjectile {

	protected Filter collision_filter;

	/**
	 * Holds all successor emissions, which trigger when this DefProjectile dies
	 */
	protected ArrayList<AEmissionDef> successors = new ArrayList<>();

	/**
	 * length of velocity vector at the beginning. By default equals 0.
	 */
	protected float velocity = 0;

	/**
	 * between 0 and 1. See skill ingredient list. e.g. necessary for explosions
	 */
	protected float velocity_variation = 0f;

	protected float velocity_diminishing = 0f;

	/**
	 * By default equals 0
	 */
	protected float gravity_scale = 0;

	/**
	 * in milliseconds, depends on collision and/or duration
	 */
	protected int life_time;

	protected int life_time_diminishing;

	protected AttachmentOptionProjectile attachment_option;

	protected float offset_attached;

	protected float offset_shape;

	public enum AttachmentOptionProjectile {

		AT_EMITTER,

		AT_TARGET
	}

	/**
	 * 0 by default. Has to be between 3 and 8, otherwise an assertion is triggered
	 * by box2d's backend.
	 */
	protected int number_vertices_shape;

	protected boolean stop_after_ground_collision = false;

	protected float friction = 0f;

	protected float restitution = 0f;

	/**
	 * By default equals <code>false</code>. Set to <code>true</code> if the
	 * projectile should be removed when the emission got interrupted. See for
	 * example {@link PortSkill}.
	 */
	protected boolean interruptible_by_emission;

	/**
	 * Holds the persecuted fixture for all projectiles (->global) that are
	 * described by this definition. The {@link Projectile} class also has a fixture
	 * member, but is only responsible for the individual projectile.
	 */
	protected IEmitter persecutee;

	protected float angle_variation;

	protected CollectableType collectable_type = null;

	/**
	 * <code>true</code> means that projectiles should not die after a collision
	 */
	private boolean fly_trough = false;

	/**
	 * In ms. By default equals to infinity. If this value is <= 0 the projectile
	 * stops moving
	 */
	protected int time_until_stop = Integer.MAX_VALUE;

	/**
	 * In ms. By default equals to zero. If the life time of a projectile has passed
	 * this value the projectile is allowed to collide with the emitter. Useful for
	 * projectiles that can be consumed by the emitter but not at the beginning of a
	 * cast.
	 */
	protected int time_until_emitter_collision = 0;

	public float scale_begin, scale_end = -1;
	public int scaling_time_ms_current, scaling_time_ms_absolute;

	protected RectangleShape rectangle_shape;

	/**
	 * might be <code>null</code>
	 */
	protected Orbit orbit;

	protected float range_until_death = Float.MAX_VALUE;

	protected float range_until_stop = Float.MAX_VALUE;

	protected boolean stop_until_reached_cursor = false;

	protected boolean death_at_stop = false;

	/**
	 * might be <code>null</code>
	 */
	protected TargetFinding target_finding;

	protected BodyType body_type = BodyType.DynamicBody;

	protected boolean one_sided_cancellation = false;

	protected boolean rounded_rect = false;

	protected CurvedTrajectory curved_trajectory;

	public enum CollectableType {
		CRYSTAL, FOR_COMBO
	}

	/**
	 * Creates a prototype. To add properties of that prototype use the fluent
	 * method: new ADefProjectile().setRootDuration(x).setLifePointsOneTime(y)....
	 * <p>
	 * That constructor won't create any sensor, so the superior constructor is
	 * called with a null world parameter.
	 * 
	 * @param lifeTime        , if DefProjectile should ONLY disappear after
	 *                        collision, choose a negative value
	 * @param collisionFilter
	 */
	public DefProjectile(int lifeTime, Filter collisionFilter) {
		life_time = lifeTime;
		collision_filter = collisionFilter;

	}

	/**
	 * Creates a new DefProjectile with sensor using a prototype
	 * 
	 * @param prototype
	 */
	public DefProjectile(DefProjectile prototype) {
		// copy all successor emissions
		for (AEmissionDef succ : prototype.successors) {
			successors.add(succ.makeCopy());
		}

		persecutee = prototype.persecutee;
		collision_filter = prototype.collision_filter;
		interruptible_by_emission = prototype.interruptible_by_emission;
		friction = prototype.friction;
		restitution = prototype.restitution;
		rectangle_shape = prototype.rectangle_shape;
		number_vertices_shape = prototype.number_vertices_shape;
		attachment_option = prototype.attachment_option;
		scale_begin = prototype.scale_begin;
		scale_end = prototype.scale_end;
		scaling_time_ms_absolute = prototype.scaling_time_ms_absolute;
		scaling_time_ms_current = prototype.scaling_time_ms_current;
		life_time = prototype.life_time;
		life_time_diminishing = prototype.life_time_diminishing;
		velocity = prototype.velocity;
		velocity_variation = prototype.velocity_variation;
		velocity_diminishing = prototype.velocity_diminishing;
		gravity_scale = prototype.gravity_scale;
		collectable_type = prototype.collectable_type;
		fly_trough = prototype.fly_trough;
		time_until_stop = prototype.time_until_stop;
		time_until_emitter_collision = prototype.time_until_emitter_collision;
		range_until_death = prototype.range_until_death;
		range_until_stop = prototype.range_until_stop;
		stop_until_reached_cursor = prototype.stop_until_reached_cursor;
		angle_variation = prototype.angle_variation;
		death_at_stop = prototype.death_at_stop;
		body_type = prototype.body_type;
		if (prototype.orbit != null)
			orbit = new Orbit(prototype.orbit);

		if (prototype.target_finding != null)
			target_finding = prototype.target_finding.makeCopy();

		stop_after_ground_collision = prototype.stop_after_ground_collision;
		offset_attached = prototype.offset_attached;
		offset_shape = prototype.offset_shape;

		one_sided_cancellation = prototype.one_sided_cancellation;
		rounded_rect = prototype.rounded_rect;

		curved_trajectory = prototype.curved_trajectory;

	}

	/**
	 * A DefProjectile hasn't every time the same collision filter. I have to create
	 * it externally and pass it to the constructor.
	 */
	public Filter getCollisionFilter() {
		return collision_filter;
	}

	/**
	 * 
	 * @param filter
	 * @return
	 */
	public void setCollisionFilter(Filter filter) {
		this.collision_filter = filter;
	}

	/**
	 * By default equals 0.
	 * 
	 * @param velocity
	 * @return {@code this} for chaining
	 */
	public DefProjectile setVelocity(float velocity) {
		this.velocity = velocity;
		return this;
	}

	/**
	 * By default equals 0.
	 * 
	 * @param offsetAttached
	 * @return {@code this} for chaining
	 */
	public DefProjectile setOffsetAttached(float offsetAttached) {
		this.offset_attached = offsetAttached;
		return this;
	}

	public float getOffsetAttached() {
		return offset_attached;
	}

	/**
	 * By default equals 0. Supported for rectangle and circle shape, not supported
	 * for n-shapes.
	 * 
	 * @param offsetShape
	 * @return {@code this} for chaining
	 */
	public DefProjectile setOffsetShape(float offsetShape) {
		this.offset_shape = offsetShape;
		return this;
	}

	public float getOffsetShape() {
		return offset_shape;
	}

	/**
	 * By default equals infinite. In meter.
	 * 
	 * @param range
	 * @return {@code this} for chaining
	 */
	public DefProjectile setRangeUntilDeath(float range) {
		this.range_until_death = range;
		return this;
	}

	/**
	 * By default equals <code>false</code>
	 * 
	 * @return {@code this} for chaining
	 */
	public DefProjectile setDeathAtStop() {
		this.death_at_stop = true;
		return this;
	}

	/**
	 * By default equals infinite. In meter.
	 * 
	 * @param range
	 * @return {@code this} for chaining
	 */
	public DefProjectile setRangeUntilStop(float range) {
		this.range_until_stop = range;
		return this;
	}

	/**
	 * 
	 * @param width
	 * @param height
	 * @return {@code this} for chaining
	 */
	public DefProjectile setRectangleShape(float width, float height) {
		this.rectangle_shape = new RectangleShape(width, height);
		this.number_vertices_shape = 4;
		return this;
	}

	public RectangleShape getRectangleShape() {
		return rectangle_shape;
	}

	public float getVelocity() {
		return velocity;
	}

	/**
	 * By default just the initial scaling. If this method is called the projectile
	 * will scale continuously. Only applicable on circle shape fixtures.
	 * 
	 * @param scaleEnd,       scaleBegin is defined in
	 *                        {@link AEmissionDef#AEmissionDef(String, float, float, boolean, DefProjectile, int, int, com.rin.logic.emission.alignment.AAlignment)}
	 * @param scalingTimeInMS
	 * @return {@code this} for chaining
	 */
	public DefProjectile setContinuousScaling(float scaleEnd, int scalingTimeInMS) {
		this.scale_end = scaleEnd;
		this.scaling_time_ms_absolute = this.scaling_time_ms_current = scalingTimeInMS;
		return this;
	}

	/**
	 * By default 0f.
	 * 
	 * @param velocityVariation, in percent.
	 * @return {@code this} for chaining
	 */
	public DefProjectile setVelocityVariation(float velocityVariation) {
		this.velocity_variation = velocityVariation;
		return this;
	}

	/**
	 * 
	 * @param velocityDiminishing
	 * @return
	 */
	public DefProjectile setVelocityDiminishing(float velocityDiminishing) {
		this.velocity_diminishing = velocityDiminishing;
		return this;
	}

	/**
	 * 
	 * @param lifeTimeDiminishing
	 * @return
	 */
	public DefProjectile setLifeTimeDiminishing(int lifeTimeDiminishing) {
		this.life_time_diminishing = lifeTimeDiminishing;
		return this;
	}

	/**
	 * 
	 * In ms. By default equals to infinity. If this value is <= 0 the projectile
	 * stops moving.
	 * 
	 * @param timeUntilStop, in ms
	 * @return {@code this} for chaining
	 */
	public DefProjectile setTimeUntilStop(int timeMSUntilStop) {
		this.time_until_stop = timeMSUntilStop;
		return this;
	}

	/**
	 * In ms. By default equals to zero. If the life time of a projectile has passed
	 * this value the projectile is allowed to collide with the emitter. Useful for
	 * projectiles that can be consumed by the emitter but not at the beginning of a
	 * cast.
	 * 
	 * @param timeMSUntilEmitterCollision*@return
	 * 
	 * @return {@code this} for chaining
	 */
	public DefProjectile setTimeUntilEmitterCollision(int timeMSUntilEmitterCollision) {
		this.time_until_emitter_collision = timeMSUntilEmitterCollision;
		return this;
	}

	/**
	 * Sets {@link #fly_trough} to <code>true</code>. <code>true</code> means that
	 * projectiles should not die after a collision. By default <code>false</code>.
	 * 
	 * @return {@code this} for chaining
	 */
	public DefProjectile setFlyThroughTargets() {
		this.fly_trough = true;
		return this;
	}

	/**
	 * By default equals 0.
	 * 
	 * @param gravityScale
	 * @return {@code this} for chaining
	 */
	public DefProjectile setGravityScale(float gravityScale) {
		this.gravity_scale = gravityScale;
		return this;
	}

	/**
	 * By default DefProjectile's shape is always a circle. Call this method to use
	 * a n-gon.
	 * 
	 * @param angleDegrees
	 * @return {@code this} for chaining
	 */
	public DefProjectile setPolygonShape(int numberVertices) {
		number_vertices_shape = numberVertices;
		return this;
	}

	/**
	 * 
	 * @return {@code this} for chaining
	 */
	public DefProjectile setRoundedRect() {
		rounded_rect = true;
		return this;
	}

	public boolean isRoundedRect() {
		return rounded_rect;
	}

	/**
	 * 
	 * @param attachmentOption
	 * @return {@code this} for chaining
	 */
	public DefProjectile setAttachmentOption(AttachmentOptionProjectile attachmentOption) {
		this.attachment_option = attachmentOption;
		return this;
	}

	public AttachmentOptionProjectile getAttachmentOption() {
		return attachment_option;
	}

	public DefProjectile setOrbit(float radiusBegin, float radiusEnd, int scalingTimeMS, float angularVel) {
		this.orbit = new Orbit(radiusBegin, radiusEnd, scalingTimeMS, angularVel);
		return this;
	}

	public void setPersecutee(IEmitter persecutee, AEmissionDef eDef, Vector2 origPos) {
		this.persecutee = persecutee;
		if (persecutee != null)
			eDef.getAlignment().setAngle(Util.getAngleBetweenTwoPoints(origPos, persecutee.getPos()));
	}

	public IEmitter getPersecutee() {
		return persecutee;
	}

	/**
	 * In ms. By default equals to zero. If the life time of a projectile has passed
	 * this value the projectile is allowed to collide with its emitter. Useful for
	 * projectiles that can be consumed by the emitter but not at the beginning of a
	 * cast.
	 * 
	 * @return
	 */
	public int getTimeUntilEmitterCollision() {
		return time_until_emitter_collision;
	}

	/**
	 * By default equals <code>null</code>. Sets successor emission def that occurs
	 * when this DefProjectile dies. A DefProjectile dies when life time is over or
	 * when it collides with ground or player (and there is no fly through condition
	 * active)
	 * 
	 * @param successor
	 * @return {@code this} for chaining
	 */
	public DefProjectile addSuccessor(AEmissionDef successor) {
		this.successors.add(successor);
		return this;
	}

	/**
	 * By default equals 0. Sets the friction between this DefProjectile and ground
	 * 
	 * @param friction
	 * @return {@code this} for chaining
	 */
	public DefProjectile setFriction(float friction) {
		this.friction = friction;
		return this;
	}

	/**
	 * By default equals 0. Sets the restitution (bounciness)
	 * 
	 * @param restitution
	 * @return {@code this} for chaining
	 */
	public DefProjectile setRestitution(float restitution) {
		this.restitution = restitution;
		return this;
	}

	public float getGravityScale() {
		return gravity_scale;
	}

	/**
	 * Returns the friction between projectiles of this def and the ground
	 * 
	 * @return
	 */
	public float getFriction() {
		return friction;
	}

	/**
	 * 
	 * @param lifeTime
	 */
	public void setLifeTime(int lifeTime) {
		this.life_time = lifeTime;
	}

	public ArrayList<AEmissionDef> getSuccessors() {
		return successors;
	}

	/**
	 * 
	 * @param interruptibleByEmission
	 * @return {@code this} for chaining
	 */
	public DefProjectile setInterruptibleByEmission(boolean interruptibleByEmission) {
		this.interruptible_by_emission = interruptibleByEmission;
		return this;
	}

	/**
	 * body type is equals {@link BodyType#DynamicBody} by default. If you choose
	 * {@link BodyType#StaticBody} ensure that you also use
	 * {@link #setPolygonShape(int)} or {@link #setRectangleShape(float, float)}
	 * because circle shapes won't work. Static bodies also need to be established
	 * on client side, therefore {@link AEmissionDef#setTriggerEmissionAllowed()}
	 * might be necessary. See {@link StoneSkill1}.
	 * 
	 * @param bodyType
	 * @return {@code this} for chaining
	 */
	public DefProjectile setBodyType(BodyType bodyType) {
		this.body_type = bodyType;
		return this;
	}

	public boolean isInterruptByEmission() {
		return interruptible_by_emission;
	}

	public int getLifeTime() {
		return life_time;
	}

	// /**
	// * Holds a limited list of emissions that occur when the DefProjectile
	// flies
	// * through a player. Emissions are build up by a prototype.
	// *
	// * @author lukassongajlo
	// *
	// */
	// private class FlyingThroughEmissions {
	//
	// private ADef prototype;
	//
	// private int count, max_number_collisions;
	// private ArrayList<ADef> successors;
	//
	// public FlyingThroughEmissions(int maxNumberCollisions,
	// ADef prototypeEmission) {
	// this.max_number_collisions = maxNumberCollisions;
	// this.prototype = prototypeEmission;
	// this.successors = new ArrayList<ADef>(maxNumberCollisions);
	//
	// }
	//
	// public boolean add(Vector2 center) {
	// if (count < max_number_collisions) {
	// ADef emission = prototype.makeCopy();
	// emission.setCenter(center);
	// successors.add(emission);
	// count++;
	// return true;
	// }
	// return false;
	// }
	//
	// }

	public float getVelocityVariation() {
		return velocity_variation;
	}

	/**
	 * Returns the {@link CollectableType}, so it might be a collectable projectile
	 * or a chrystal of a creep. Might be <code>null</code>
	 * 
	 * @return
	 * 
	 */
	public CollectableType getCollectableType() {
		return collectable_type;
	}

	/**
	 * Sets the {@link #collectable} field to <code>true</code> (by default equals
	 * <code>false</code>).
	 * 
	 * @return
	 */
	public DefProjectile setCollectable(CollectableType type) {
		collectable_type = type;
		return this;
	}

	public boolean isFlyThroughTargets() {
		return fly_trough;
	}

	public DefProjectile setAngleVariation(float angleVariation) {
		angle_variation = angleVariation;
		return this;
	}

	class Orbit {

		float radius_begin, radius_end;
		float angular_vel;
		int scaling_time_ms_current, scaling_time_ms_absolute;

		public Orbit(float radiusBegin, float radiusEnd, int scalingTimeInMS, float angularVel) {
			this.radius_begin = radiusBegin;
			this.radius_end = radiusEnd;
			this.scaling_time_ms_absolute = this.scaling_time_ms_current = scalingTimeInMS;
			this.angular_vel = angularVel;
		}

		public Orbit(Orbit orbit) {
			this.radius_begin = orbit.radius_begin;
			this.radius_end = orbit.radius_end;
			this.scaling_time_ms_absolute = this.scaling_time_ms_current = orbit.scaling_time_ms_absolute;
			this.angular_vel = orbit.angular_vel;
		}

	}

	/**
	 * After collision with the ground the projectile's gravity scale and linear
	 * velocity are set to 0.
	 * 
	 * @return
	 */
	public DefProjectile setStopAfterGroundCollision() {
		stop_after_ground_collision = true;
		return this;
	}

	public boolean isStopAfterGroundCollision() {
		return stop_after_ground_collision;
	}

	/**
	 * By default equals <code>false</code>. Sets the flag to stop the projectile
	 * once it has reached the cursor pos (= the cursor pos the player had right
	 * after initiating the projectile).
	 * 
	 * @return {@code this} for chaining
	 */
	public DefProjectile setStopUntilReachedCursor() {
		stop_until_reached_cursor = true;
		return this;
	}

	public TargetFinding getTargetFinding() {
		return target_finding;
	}

	/**
	 * Activates auto-finding of new targets.
	 * 
	 * @param targetFinding
	 * @return
	 */
	public DefProjectile setTargetFinding(TargetFinding targetFinding) {
		target_finding = targetFinding;
		return this;
	}

	public boolean isOneSidedCancellation() {
		return one_sided_cancellation;
	}

	/**
	 * if two of projectiles of the same kind collides the one with the lower life
	 * time sustains while the other one dies. Useful for {@link IceSkill1} placing
	 * the ice plates.
	 * 
	 * @return
	 */
	public DefProjectile setOneSidedCancellation() {
		one_sided_cancellation = true;
		return this;
	}

	public CurvedTrajectory getCurvedTrajectory() {
		return curved_trajectory;
	}

	public void setTrajectory(CurvedTrajectory trajectory) {
		curved_trajectory = trajectory;
	}

}