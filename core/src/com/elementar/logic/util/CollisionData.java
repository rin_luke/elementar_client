package com.elementar.logic.util;

/**
 * Holds all bits to determine collision scenarios
 * 
 * @author lukassongajlo
 * 
 */
public class CollisionData {

	// category bits
	/*
	 * next possible category bits: 2048, ...., 16384
	 */
	/**
	 * character's hitbox to collide with projectiles
	 */
	public static final short BIT_CHARACTER_PROJECTILE = 1;
	/**
	 * character's hitbox to collide with obstacles/ground
	 */
	public static final short BIT_CHARACTER_MOVEMENT = 2;
	/**
	 * character's hitbox to collide with plants
	 */
	public static final short BIT_CHARACTER_PLANT = 4;

	public static final short BIT_CHARACTER_LIGHT = 8;

	/**
	 * building's hitbox to collide with projectiles
	 */
	public static final short BIT_BUILDING = 16;
	public static final short BIT_GROUND = 32;

	public static final short BIT_PROJECTILE = 64;

	public static final short BIT_LIGHT = 128;
	public static final short BIT_PLANT = 256;
	public static final short BIT_CHARACTER_SEPARATOR = 512;

	public static final short BIT_ICE_SKILL0_PROJECTILE = 1024;

	public static final short BIT_PROJ_GROUND = 2048;

	public static final short BIT_WATER_DROP = 4096;
	
	public static final short BIT_TOXIC_SKILL1_PROJECTILE = 8192;

	public enum CollisionKinds {
		CAST_ON_ALLY_EMITTER_EXCLUDED(1), CAST_ON_ALLY_EMITTER_INCLUDED(2), CAST_ON_ENEMY(4), AFTER_LIFETIME(8),
		AFTER_ABSORPTION(16), CAST_ON_EMITTER(32);

		int bit;

		CollisionKinds(int bit) {
			this.bit = bit;
		}

		public int getValue() {
			return bit;
		}
	}

}
