package com.elementar.logic.util;

import com.badlogic.gdx.math.Vector2;

import box2dLight.Light;

public class ViewCircle {

	private Vector2 pos;
	private float radius;

	/**
	 * is only set when view circles are handled by a {@link ViewHandler} on client
	 * side
	 */
	private Light fog_dispel_world;

	private Light fog_dispel_minimap;

	public ViewCircle() {

	}

	public ViewCircle(Vector2 pos, float radius) {
		this.pos = pos;
		this.radius = radius;
	}

	public void setFogDispelMinimap(Light fogDispelMinimap) {
		fog_dispel_minimap = fogDispelMinimap;
	}

	public Light getFogDispelMinimap() {
		return fog_dispel_minimap;
	}

	public void setFogDispelWorld(Light fogDispel) {
		fog_dispel_world = fogDispel;
	}

	public Light getFogDispelWorld() {
		return fog_dispel_world;
	}

	/**
	 * Activates/Deactives the fog dispel light. Returns viewCircle for chaining
	 * 
	 * @param active
	 * @return
	 */
	public ViewCircle setFogDispelActive(boolean active) {
		if (fog_dispel_world != null)
			fog_dispel_world.setActive(active);
		if (fog_dispel_minimap != null)
			fog_dispel_minimap.setActive(active);
		return this;
	}

	public float getX() {
		return pos.x;
	}

	public float getY() {
		return pos.y;
	}

	public Vector2 getPos() {
		return pos;
	}

	public void setPosition(Vector2 pos) {
		this.pos = pos;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public float getRadius() {
		return radius;
	}

}
