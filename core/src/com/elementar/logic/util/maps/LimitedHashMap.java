package com.elementar.logic.util.maps;

public class LimitedHashMap<K, V> extends ALimitedHashMap<K, V> {

	public LimitedHashMap(int limit) {
		super(limit);
	}

	@Override
	public V put(K key, V value) {
		if (size() + 1 > limit)
			return null;
		return super.put(key, value);

	}
}
