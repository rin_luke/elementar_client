package com.elementar.logic.util.maps;

import java.util.HashMap;

public class ALimitedHashMap<K, V> extends HashMap<K, V> {
	protected int limit;

	public ALimitedHashMap(int limit) {
		super(limit);
		this.limit = limit;
	}

	public int getLimit() {
		return limit;
	}

	public boolean isFull() {
		return limit == size();
	}

}
