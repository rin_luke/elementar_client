package com.elementar.logic.util.maps;

import java.util.LinkedList;

/**
 * Last in first out. The last added will be discarded. For removing elements,
 * don't use an iterator, because the element has not only be removed from map
 * but also from the underlying deque. An Iterator won't remove any elements
 * from the deque.
 * 
 * @author lukassongajlo
 * 
 * @param <K>
 * @param <V>
 */
public class LIFOLimitedHashMap<K, V> extends ALimitedHashMap<K, V> {

	private LinkedList<K> deque;

	public LIFOLimitedHashMap(int limit) {
		super(limit);
		deque = new LinkedList<K>();
	}

	@Override
	public V put(K key, V value) {
		deque.add(key);
		V v = super.put(key, value);
		if (deque.size() > limit) {
			// remove last added element from deque and hashmap
			remove(deque.getLast());
		}
		return v;
	}

	public LinkedList<K> getDeque() {
		return deque;
	}

	@Override
	public V remove(Object key) {
		deque.remove(key);
		return super.remove(key);
	}

}
