package com.elementar.logic.util.maps;

import java.util.LinkedList;

/**
 * first in first out limited map. New elements stay the longest time in the
 * map. For removing elements, don't use an iterator, because the element has
 * not only be removed from map but also from the underlying deque. An Iterator
 * won't remove any elements from the deque.
 * 
 * @author lukassongajlo
 * 
 * @param <K>
 * @param <V>
 */
public class FIFOLimitedHashMap<K, V> extends ALimitedHashMap<K, V> {

	private LinkedList<K> deque;

	public FIFOLimitedHashMap(int limit) {
		super(limit);
		deque = new LinkedList<K>();
	}

	@Override
	public V put(K key, V value) {
		deque.add(key);
		V v = super.put(key, value);
		if (deque.size() > limit)
			// remove last added element from deque and hashmap
			remove(deque.getFirst());
		return v;
	}

	@Override
	public V remove(Object key) {
		deque.remove(key);
		return super.remove(key);
	}

}
