package com.elementar.logic.util;

/**
 * Use the standard constructor for no collision. That leads automatically to
 * xray-behaviour.
 * 
 * @author lukassongajlo
 * 
 */
public class LightCollisionFilter {

	private boolean ground;
	private boolean player;

	/**
	 * No collision, x-ray <code>true</code>
	 */
	public LightCollisionFilter() {
		ground = false;
		player = false;
	}

	/**
	 * 
	 * @param ground
	 *            , if <code>true</code> the light collides with ground
	 * @param player
	 *            , if <code>true</code> the light collides with player's light
	 *            hitbox
	 */
	public LightCollisionFilter(boolean ground, boolean player) {
		this.ground = ground;
		this.player = player;
	}

	public boolean isGround() {
		return ground;
	}

	public boolean isPlayer() {
		return player;
	}

}