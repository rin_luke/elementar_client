package com.elementar.logic.util;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.elementar.logic.emission.IEmitter;
import com.elementar.logic.map.buildings.Base;
import com.elementar.logic.util.Shared.Team;

public class CreepAndTowerCastCallback implements RayCastCallback {

	private Fixture fixture_target;

	private Team team;

	public CreepAndTowerCastCallback(Team team) {
		this.team = team;
	}

	@Override
	public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {

		/*
		 * base is an exception because it is a ground and attackable at the
		 * same time
		 */
		if ((fixture.getFilterData().categoryBits & CollisionData.BIT_GROUND) != 0
				&& fixture.getUserData() instanceof Base == false) {
			// terminate if ground
			fixture_target = null;
			return fraction;
		}

		if ((fixture.getFilterData().categoryBits & CollisionData.BIT_CHARACTER_PROJECTILE) != 0
				|| (fixture.getFilterData().categoryBits & CollisionData.BIT_BUILDING) != 0) {
			// skill init when enemy
			if (fixture.getUserData() instanceof IEmitter) {
				if (((IEmitter) fixture.getUserData()).getTeam() != team) {
					fixture_target = fixture;
					return fraction;
				}
			}
		}
		return -1;
	}

	public Fixture getFixtureTarget() {
		return fixture_target;
	}

}
