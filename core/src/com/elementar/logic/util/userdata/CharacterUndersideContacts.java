package com.elementar.logic.util.userdata;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.network.gameserver.ServerContactListener;

public class CharacterUndersideContacts extends AContacts {

	private int prev_analytical_angle_platform, current_analytical_angle_platform, angle_platform;

	private PhysicalClient client;

	public CharacterUndersideContacts() {
		super();
		this.angle_platform = 180;
		this.current_analytical_angle_platform = 180;
		this.prev_analytical_angle_platform = 180;
	}

	public void setPhysicalGameclient(PhysicalClient client) {
		this.client = client;
	}

	public PhysicalClient getPhysicalClient() {
		return client;
	}

	public int getPlatformAngle() {
		return angle_platform;
	}

	public void setPrevAnalyticalAnglePlatform(int anglePlatform) {
		this.prev_analytical_angle_platform = anglePlatform;
	}

	public void setCurrentAnalyticalAnglePlatform(int anglePlatform) {
		this.current_analytical_angle_platform = anglePlatform;
	}

	public void setAnglePlatform(int anglePlatform) {
		this.angle_platform = anglePlatform;
	}

	public boolean isAlternating(int anglePlatform) {
		return prev_analytical_angle_platform == anglePlatform && current_analytical_angle_platform != anglePlatform;
	}

	public int getPrevAnglePlatform() {
		return current_analytical_angle_platform;
	}

	/**
	 * in {@link ServerContactListener} I need the information whether the player is
	 * jumping or not. If no I can activate the friction part when I just standing
	 * on a platform. Otherwise the physic leads to a little slide effect on
	 * platforms. If yes the friction part should be deactivated.
	 */
	public boolean isJumping() {
		return client.getJumping().jump_active;
	}

	public float getVelocityMovementFactor() {
		return client.getVelXFactor();
	}

}
