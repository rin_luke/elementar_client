package com.elementar.logic.util.userdata;

import com.elementar.logic.characters.util.Jumping;

/**
 * This contact class register contacts at the characters top (above
 * headcontacts) for deactivate jumping
 * 
 * @author lukassongajlo
 * 
 */
public class CharacterMidTopContacts extends AContacts {

	private Jumping jumping;

	public void setJumping(Jumping jumping) {
		this.jumping = jumping;
	}

	public Jumping getJumping() {
		return jumping;
	}
}
