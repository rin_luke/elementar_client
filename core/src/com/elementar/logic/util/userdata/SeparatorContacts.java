package com.elementar.logic.util.userdata;

import static com.elementar.logic.util.Shared.PPM;

import java.util.Collection;
import java.util.HashMap;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.creeps.PhysicalCreep;
import com.elementar.logic.util.SparseObject;
import com.elementar.logic.util.maps.LimitedHashMap;

/**
 * Owner of a separator contacts object might be a {@link PhysicalClient} or
 * {@link PhysicalCreep}
 * 
 * @author lukassongajlo
 *
 */
public class SeparatorContacts extends AContacts {

	public static float SEPARATOR_RADIUS = 23f / PPM;
	
	private LimitedHashMap<Short, PhysicalClient> touching_players = new LimitedHashMap<>(10);
	private HashMap<Short, PhysicalCreep> touching_creeps = new HashMap<>();

	private PhysicalClient client;
	private PhysicalCreep creep;

	public SeparatorContacts(PhysicalClient client) {
		this.client = client;
	}

	public SeparatorContacts(PhysicalCreep creep) {
		this.creep = creep;
	}

	public void add(SeparatorContacts separatorContact) {
		SparseObject object = separatorContact.getObject();
		if (object instanceof PhysicalCreep)
			touching_creeps.put(object.connection_id, (PhysicalCreep) object);
		else
			touching_players.put(object.connection_id, (PhysicalClient) object);
	}

	public void remove(SeparatorContacts separatorContacts) {
		SparseObject object = separatorContacts.getObject();
		if (object instanceof PhysicalCreep)
			touching_creeps.remove(object.connection_id);
		else
			touching_players.remove(object.connection_id);
	}

	public Collection<PhysicalClient> getListClients() {
		return touching_players.values();
	}

	public Collection<PhysicalCreep> getListCreeps() {
		return touching_creeps.values();
	}

	private SparseObject getObject() {
		if (client != null)
			return client;
		return creep;
	}
}
