package com.elementar.logic.util.userdata;

public class CharacterHeadContacts extends AContacts {

	private float velocity_x;

	public void setVelocity(float velocityX) {
		this.velocity_x = velocityX;
	}

	public float getVelocityX() {
		return velocity_x;
	}

}
