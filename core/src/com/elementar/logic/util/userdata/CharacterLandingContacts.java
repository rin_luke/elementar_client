package com.elementar.logic.util.userdata;

public class CharacterLandingContacts extends AContacts {

	private float vel_y;

	public void setVelY(float velY) {
		this.vel_y = velY;
	}

	/**
	 * Returns the velocity y component which where calculated inside the
	 * world's step() call.
	 * 
	 * @return
	 */
	public float getVelY() {
		return vel_y;
	}

}
