package com.elementar.logic.util.userdata;

import com.elementar.gui.util.VegetationSprite;

public class PlantContacts {

	protected VegetationSprite sprite;
	protected float animation_time;

	public PlantContacts(VegetationSprite sprite) {
		this.sprite = sprite;
		animation_time = 0;
	}

	public boolean isInAnimation() {
		if (animation_time > 0)
			return true;
		return false;
	}
	
	public void decrementAnimationTime(float delta) {
		if (animation_time >= 0)
			animation_time -= delta;
	}

	public void setAnimationTime(float t) {
		animation_time = t;
	}

	public VegetationSprite getSprite() {
		return sprite;
	}

}
