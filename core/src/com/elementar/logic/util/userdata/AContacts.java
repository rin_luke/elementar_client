package com.elementar.logic.util.userdata;

public abstract class AContacts {

	protected int num_contacts;

	public AContacts() {
		this.num_contacts = 0;
	}

	/**
	 * Returns <code>true</code> if the {@link #num_contacts} > 0
	 * 
	 * @return
	 */
	public boolean isTouching() {
		return num_contacts > 0;
	}

	public int getNumContacts() {
		return num_contacts;
	}

	public void incrementNumContacts() {
		num_contacts++;
	}

	public void decrementNumContacts() {
		num_contacts--;
	}

}
