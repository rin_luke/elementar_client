package com.elementar.logic.util.userdata;

import com.elementar.logic.util.Util;
import com.elementar.logic.util.Shared.CategoryFunctionality;

public abstract class ASlideContacts extends AContacts {

	protected boolean contact_with_slide;

	/**
	 * Call this before {@link #incrementNumContacts()} and before
	 * {@link #decrementNumContacts()}, because these two method will only
	 * trigger if the angle is suitable for slide-platforms.
	 * 
	 * @param platformAngle
	 */
	public void setPlatformAngle(int platformAngle) {
		contact_with_slide = Util.getPlatformFunctionality(platformAngle) == CategoryFunctionality.SLIDE;
	}

	@Override
	public void incrementNumContacts() {
		if (contact_with_slide == true)
			super.incrementNumContacts();
	}

	@Override
	public void decrementNumContacts() {
		if (contact_with_slide == true)
			super.decrementNumContacts();
	}

}
