package com.elementar.logic.util;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.elementar.data.container.ParticleContainer;
import com.elementar.data.container.util.CustomParticleEffect;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.creeps.OmniCreep;
import com.elementar.logic.creeps.PhysicalCreep;
import com.elementar.logic.map.MapManager;
import com.elementar.logic.map.buildings.Base;
import com.elementar.logic.map.buildings.Tower;
import com.elementar.logic.map.pathfinding.LightCluster;
import com.elementar.logic.util.Shared.Team;

import box2dLight.PointLight;
import box2dLight.RayHandler;

/**
 * Holds a list of all active circles of a team
 * 
 * @author lukassongajlo
 * 
 */
public class ViewHandler {

	public static final int RAY_COUNT_FOG_DISPEL = 250;

	private final int RAY_COUNT_DYNAMIC_LIGHT = 128;

	private final Color COLOR_FOG_DISPEL = new Color(0f, 0f, 0f, 1f),
			COLOR_TEAM1 = new Color(6f / 255f, 78f / 255f, 214f / 255f, 1f),
			COLOR_TEAM2 = new Color(197f / 255f, 54f / 255f, 58f / 255f, 1f), COLOR_WARM = new Color(
					// 1f
					35f / 255f
					// 10f/255f
					, 6f / 255f, 0f, 1f),
			COLOR_COLD = new Color(0f, 3f / 255f,
					// 1f
					40f / 255f
					// 15f/255f
					, 1f);

	private ArrayList<ViewCircle> view_circles = new ArrayList<>();

	/**
	 * map manager provides view circles from buildings (towers and bases)
	 */
	private MapManager map_manager;

	/**
	 * determines which view circles are visible depending on team
	 */
	private Team team;

	private Illuminator illuminator;

	private PhysicalClient temp_client;

	private ArrayList<LightCluster> drawable_lights = new ArrayList<>();

	/**
	 * 
	 * @param mapManager
	 * @param team
	 * @param paraWorld
	 */
	public ViewHandler(MapManager mapManager, Team team, World paraWorld) {
		map_manager = mapManager;
		this.team = team;
		illuminator = new Illuminator(paraWorld);
		illuminator.illuminateWorld(mapManager.getLightCluster());
	}

	/**
	 * clears the {@link #view_circles} list and fills it afterwards with new
	 * circles. One player might be dead, so that circle won't be added. This method
	 * is called with following rate: {@link Shared#SEND_AND_UPDATE_RATE_WORLD} .
	 */
	public void update() {

		calibrate();

		/*
		 * Checks whether enemies and their projectiles are inside one of the view
		 * circles in {@link #view_circles}. If not, the player visibility is set to
		 * false
		 */
		for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {
			temp_client = client.getPhysicalClient();
			if (temp_client.getMetaClient().team != team) {

				boolean visible = isInside(temp_client.pos, 0f);

				temp_client.setVisible(visible);
				temp_client.getHitbox().setPlantCollisionFilter(visible);
			}
		}

		for (OmniCreep creep : LogicTier.CREEP_LIVING_LIST)
			creep.getPhysicalCreep().setVisible(isInside(creep.getPhysicalCreep().pos, 0f));

	}

// alle viewcircles werden immer deaktiviert und gelöscht no matter what.
	// das heißt ich könnte projektil view circles erkennen und erstellen und später
	// wieder kicken.
	/*
	 * das heißt view circles brauchen eine art erkennung wenn sie von einer
	 * bestimmten emissionsdef stammen, denn dann kann ich sagen
	 * "ok du bist von lichtskillbasic, e1. Hier muss ein view circle hin"
	 */
	/**
	 * Calibrates the {@link #view_circles} to the current {@link #team}.
	 */
	private void calibrate() {
		// clear circles and deactivate fog dispel

		for (Iterator<ViewCircle> it = view_circles.iterator(); it.hasNext();) {
			it.next().setFogDispelActive(false);
			it.remove();
		}

		/*
		 * add all known view circles
		 */

		// re-activate base fog dispel
		if (team == Team.TEAM_1)
			updateBase(map_manager.getBaseLeft());
		else
			updateBase(map_manager.getBaseRight());

		// re-activate player fog dispel
		for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {
			temp_client = client.getPhysicalClient();

			if (temp_client.getMetaClient().team == team && temp_client.isAlive() == true
					|| temp_client.isRecentlyDead() == true) {
				view_circles.add(temp_client.getViewCircle().setFogDispelActive(true));
			}
		}

	}

	/**
	 * Re-activates fog of base.
	 * 
	 * @param base
	 */
	private void updateBase(Base base) {
		view_circles.add(base.getViewCircleSpawn().setFogDispelActive(true));
	}

	/**
	 * This method is called frame-wise
	 */
	public void updatePlayerFogDispel() {
		// update player fog dispel positions
		if (illuminator != null)
			illuminator.updateCharacters();
	}

	/**
	 * This method is called frame-wise
	 */
	public void updateCreepFogDispel() {
		// update creep fog dispel positions
		if (illuminator != null)
			illuminator.updateCreeps();
	}

	/**
	 * Minimap
	 * 
	 * @param cam
	 */
	public void renderFogMinimap(OrthographicCamera cam) {
		if (illuminator != null)
			renderRays(illuminator.ray_handler_minimap, cam);
	}

	public void render(OrthographicCamera worldCam) {
		if (illuminator != null) {
			renderRays(illuminator.ray_handler_fog, worldCam);
			renderRays(illuminator.ray_handler_lights, worldCam);
		}
	}

	private void renderRays(RayHandler rayHandler, OrthographicCamera worldCam) {
		rayHandler.setCombinedMatrix(worldCam);
		rayHandler.updateAndRender();
	}

	public void dispose() {
		if (illuminator != null) {
			illuminator.ray_handler_fog.dispose();
			illuminator.ray_handler_lights.dispose();
			illuminator.ray_handler_minimap.dispose();
		}
	}

	public ArrayList<ViewCircle> getViewCircles() {
		return view_circles;
	}

	/**
	 * 
	 * @param pos
	 * @param radius
	 * @return
	 */
	public boolean isInside(Vector2 pos, float radius) {

		boolean isInside = false;

		for (ViewCircle viewCircle : view_circles) {
			if (Util.circleContains(viewCircle, pos) == true) {

				if (viewCircle.getPos().dst2(pos) < 0.1f)
					return true;

				// check if pos is outside of an island.
				Vector2 polarCoords;

				polarCoords = Util.getPolarCoords(radius, Util.getAngleBetweenTwoPoints(viewCircle.getPos(), pos));
				if (isOutsideIsland(viewCircle, pos, polarCoords) == true)
					return true;

				if (radius < 0.01f)
					/*
					 * if after the first check we get a negative result + the radius is 0 we can
					 * continue with the next circle since we are not sampling the circle.
					 */
					continue;

				polarCoords = Util.getPolarCoords(radius, 90 + Util.getAngleBetweenTwoPoints(viewCircle.getPos(), pos));
				if (isOutsideIsland(viewCircle, pos, polarCoords) == true)
					return true;

				polarCoords = Util.getPolarCoords(radius,
						-90 + Util.getAngleBetweenTwoPoints(viewCircle.getPos(), pos));
				if (isOutsideIsland(viewCircle, pos, polarCoords) == true)
					return true;

			}
		}
		return isInside;
	}

	private boolean isOutsideIsland(ViewCircle viewCircle, Vector2 pos, Vector2 polarCoords) {
		ProjectileCastCallback cb = new ProjectileCastCallback();

		// note if radius is equals 0 we compare both center points
		illuminator.world.rayCast(cb, viewCircle.getPos(), Util.subVectors(pos, polarCoords));

		if (MathUtils.isZero(cb.fraction) == true)
			return true;
		return false;
	}

	/**
	 * Help method to illuminate dummy after creation
	 * 
	 * @param dummy
	 */
	public void illuminateDummy(OmniClient dummy) {
		illuminator.addFogDispel(dummy.getPhysicalClient().getViewCircle());
	}

	public ArrayList<LightCluster> getLightClusters() {
		return drawable_lights;
	}

	public Team getTeam() {
		return team;
	}

	private class ProjectileCastCallback implements RayCastCallback {

		private float fraction;

		public ProjectileCastCallback() {

		}

		@Override
		public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {

			if ((fixture.getFilterData().categoryBits & CollisionData.BIT_GROUND) != 0) {
				this.fraction = fraction;
				return fraction;
			}
			return -1;
		}

	}

	private class Illuminator {

		private RayHandler ray_handler_fog, ray_handler_minimap, ray_handler_lights;

		private World world;

		public Illuminator(World paraWorld) {

			world = paraWorld;

			RayHandler.useDiffuseLight(false);
			RayHandler.setGammaCorrection(false);

			ray_handler_fog = new RayHandler(paraWorld);
			ray_handler_fog.setShadows(true);
			ray_handler_fog.setLightShader(getFogLightShader());
			ray_handler_fog.setBlur(true);
			ray_handler_fog.setBlurNum(1);

			ray_handler_lights = new RayHandler(paraWorld);
			ray_handler_lights.setShadows(false);
			ray_handler_lights.setBlur(true);
			ray_handler_lights.setBlurNum(1);

			ray_handler_minimap = new RayHandler(paraWorld);
			ray_handler_minimap.setLightShader(getFogLightShader());
			ray_handler_minimap.setShadows(false);
			ray_handler_minimap.setBlur(false);

			// just one fog dispel will be created
			illuminateBase(map_manager.getBaseLeft());
			illuminateBase(map_manager.getBaseRight());

			// illuminate towers
			CustomParticleEffect effect;
			for (Tower t : map_manager.getTowers()) {
				if (t.getTeam() == team)
					// fog dispel
					addFogDispel(t.getViewCircle());
				// offset zum root, x = 0, y = 485*scale
				effect = ParticleContainer.makeTowerParticle(t);
				t.setParticleEffect(effect);
				Color c = t.getTeam() == Team.TEAM_1 ? COLOR_TEAM1 : COLOR_TEAM2;
				float shiftY = 485 * Shared.SCALE_BUILDINGS;
				t.setLight(addLight(RAY_COUNT_DYNAMIC_LIGHT, c, 1.5f, t.getPosition().x, t.getPosition().y + shiftY, 1f,
						false, new LightCollisionFilter(true, true)));
			}
			/**
			 * ambient light ist nur dann aktiv wenn shadows enabled sind.
			 */
			ray_handler_fog.setAmbientLight(0f, 0f, 0f, 160f / 255f);
		}

		public void illuminateWorld(ArrayList<LightCluster> clusters) {

			float temperatureThreshold = 1.49f;

			ATemperature warmTemperature = new ATemperature() {

				@Override
				boolean isDistanceValid(float distance) {
					return distance > temperatureThreshold;
				}

				@Override
				float getDistanceMultiplier() {
					return 3.5f;
				}

				@Override
				Color getColor() {
					return COLOR_WARM;
				}

			};

			ATemperature coldTemperature = new ATemperature() {

				@Override
				boolean isDistanceValid(float distance) {
					return distance <= temperatureThreshold && distance > 0.49f;
				}

				@Override
				float getDistanceMultiplier() {
					return 5.5f;
				}

				@Override
				Color getColor() {
					return COLOR_COLD;
				}

			};

			// center
			setWorldLights(warmTemperature, clusters, false, true);
			setWorldLights(coldTemperature, clusters, false, true);

			/*
			 * remove all x-positive clusters, because the negative ones get mirrored
			 */
			Iterator<LightCluster> it = clusters.iterator();
			while (it.hasNext()) {
				if (it.next().getPos().x > 0)
					it.remove();
			}

			setWorldLights(warmTemperature, clusters, true, false);
			setWorldLights(coldTemperature, clusters, true, false);

		}

		private void setWorldLights(ATemperature temp, ArrayList<LightCluster> clusters, boolean mirror,
				boolean center) {

			boolean noClustersLeft = true;

			LightCluster cluster = null;

			boolean isCenter, isNotCenter;

			while (true) {

				Iterator<LightCluster> itClusters = clusters.iterator();

				while (itClusters.hasNext() == true) {
					cluster = itClusters.next();

					isCenter = (center == true && MathUtils.isEqual(cluster.getPos().x, 0, 0.5f) == true);
					isNotCenter = (center == false && MathUtils.isEqual(cluster.getPos().x, 0, 0.5f) == false);

					// we are looking for only center or only non-center points
					if (isCenter ^ isNotCenter) {

						if (isCenter)
							cluster.setX(0f);

						if (temp.isDistanceValid(cluster.getDistance())) {

							addLight(16, temp.getColor(), cluster.getDistance() * temp.getDistanceMultiplier(),
									cluster.getPos().x, cluster.getPos().y, 1, true,
									new LightCollisionFilter(true, false));
							if (mirror == true)
								addLight(16, temp.getColor(), cluster.getDistance() * temp.getDistanceMultiplier(),
										cluster.getPos().x * -1, cluster.getPos().y, 1, true,
										new LightCollisionFilter(true, false));
							drawable_lights.add(cluster);
							itClusters.remove();
							break;
						}
					}

					noClustersLeft = itClusters.hasNext();
				}

				if (noClustersLeft == false)
					break;

				// remove cluster that are inside of the new light
				if (cluster != null) {
					itClusters = clusters.iterator();
					LightCluster cluster2;
					while (itClusters.hasNext() == true) {
						cluster2 = itClusters.next();
						// does circles overlap?
						if (cluster2.getPos().dst(cluster.getPos()) <= cluster2.getDistance() + cluster.getDistance())
							itClusters.remove();
					}
				}

				if (clusters.size() == 0)
					break;

			}
		}

		/**
		 * Updates the fog dispel positions of players
		 */
		private void updateCharacters() {

			ViewCircle viewCircle;
			// character fog dispel lights
			for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values())
				if (client.getMetaClient().team == team) {

					viewCircle = client.getPhysicalClient().getViewCircle();

					// create fog dispel if not existing
					if (viewCircle.getFogDispelWorld() == null)
						addFogDispel(viewCircle);

					/*
					 * update fog dispel from characters (drawable pos = interpolated)
					 */
					viewCircle.setPosition(client.getDrawableClient().pos);
					viewCircle.getFogDispelWorld().setPosition(client.getDrawableClient().pos);
					viewCircle.getFogDispelMinimap().setPosition(client.getDrawableClient().pos);

				}
		}

		/**
		 * Updates the fog dispel positions of players
		 */
		private void updateCreeps() {

			PhysicalCreep tempCreep;

			for (OmniCreep creep : LogicTier.CREEP_LIVING_LIST) {

				tempCreep = creep.getPhysicalCreep();

				tempCreep.getAttackEntryCircle().setPosition(creep.getDrawableCreep().pos);
			}
		}

		private void illuminateBase(Base base) {

			base.setBaseParticleEffects();

			// lights and fog dispel
			Color lightTeam = base.getTeam() == Team.TEAM_1 ? COLOR_TEAM1 : COLOR_TEAM2;

			// lights
			base.setLights(addLight(RAY_COUNT_DYNAMIC_LIGHT, lightTeam, 5, base.getViewCircleSpawn().getX(),
					base.getViewCircleSpawn().getY(), 2.5f, false, new LightCollisionFilter(true, true)));

			// fog dispel
			if (team == base.getTeam())
				addFogDispel(base.getViewCircleSpawn());

		}

		/**
		 * 'xRays' always <code>true</code>
		 * 
		 * @param viewCircle
		 * @param staticLight
		 * @return
		 */
		private void addFogDispel(ViewCircle viewCircle) {

			PointLight dispelWorld = new PointLight(ray_handler_fog, RAY_COUNT_FOG_DISPEL, COLOR_FOG_DISPEL,
					viewCircle.getRadius(), viewCircle.getX(), viewCircle.getY());

			dispelWorld.setSoft(true);
			dispelWorld.setSoftnessLength(0.5f);
			dispelWorld.setStaticLight(true);
			dispelWorld.setXray(false);

			short maskBits = CollisionData.BIT_GROUND;
			dispelWorld.setContactFilter(CollisionData.BIT_LIGHT, (short) 0, maskBits);

			viewCircle.setFogDispelWorld(dispelWorld);

			// white light
			PointLight dispelMinimap = new PointLight(ray_handler_minimap, 100,
					new Color(7f / 255f, 10f / 255f, 12f / 255f, 1f), viewCircle.getRadius(), viewCircle.getX(),
					viewCircle.getY());

			dispelMinimap.setSoft(false);
			dispelMinimap.setStaticLight(true);
			dispelMinimap.setXray(false);

			dispelMinimap.setContactFilter(CollisionData.BIT_LIGHT, (short) 0, maskBits);

			viewCircle.setFogDispelMinimap(dispelMinimap);

		}

		/**
		 * 
		 * @param rayCount
		 * @param c
		 * @param distance
		 * @param x
		 * @param y
		 * @param softnessLength
		 * @param staticLight     , <code>true</code> if the light doesn't move, or
		 *                        <code>false</code> if the light should dynamically
		 *                        interact with player
		 * @param collisionFilter , standard constructor means xRays = <code>true</code>
		 * 
		 * @return created {@link PointLight} object
		 */
		private PointLight addLight(int rayCount, Color c, float distance, float x, float y, float softnessLength,
				boolean staticLight, LightCollisionFilter collisionFilter) {

			PointLight l = new PointLight(ray_handler_lights, rayCount, c, distance, x, y);

			// setup collisions by filter
			short maskBits = 0;
			if (collisionFilter.isGround() == true)
				maskBits |= CollisionData.BIT_GROUND;
			if (collisionFilter.isPlayer() == true)
				maskBits |= CollisionData.BIT_CHARACTER_LIGHT;

			if (maskBits == 0)
				l.setXray(true);

			l.setContactFilter(CollisionData.BIT_LIGHT, (short) 0, maskBits);
			l.setSoft(true);
			l.setSoftnessLength(softnessLength);

			return l;
		}

		private ShaderProgram getFogLightShader() {
			String gamma = "";
			if (RayHandler.getGammaCorrection())
				gamma = "sqrt";

			/*
			 * TODO ich hab den Wert bei v_color mal auf 10 gesetzt, Grund für den hohen
			 * Wert ist wahrscheinlich eine Kollision mit dem client, die überwunden werden
			 * muss. Ich glaube, dass das ne Art "Schein"-Wert ist, also inwieweit der
			 * Strahl nach außen scheint.
			 */

			final String vertexShader = "attribute vec4 vertex_positions;\n" //
					+ "attribute vec4 quad_colors;\n" //
					+ "attribute float s;\n" + "uniform mat4 u_projTrans;\n" //
					+ "varying vec4 v_color;\n" //
					+ "void main()\n" //
					+ "{\n" //
					+ "   v_color = 10.0 * s * quad_colors;\n" //
					+ "   gl_Position =  u_projTrans * vertex_positions;\n" //
					+ "}\n";
			final String fragmentShader = "#ifdef GL_ES\n" //
					+ "precision lowp float;\n" //
					+ "#define MED mediump\n" + "#else\n" + "#define MED \n" + "#endif\n" //
					+ "varying vec4 v_color;\n" //
					+ "void main()\n"//
					+ "{\n" //
					+ "  gl_FragColor = " + gamma + "(v_color);\n" //
					+ "}";

			ShaderProgram.pedantic = false;
			ShaderProgram lightShader = new ShaderProgram(vertexShader, fragmentShader);
			if (lightShader.isCompiled() == false) {
				Gdx.app.log("ERROR", lightShader.getLog());
			}
			return lightShader;
		}

		abstract class ATemperature {

			abstract boolean isDistanceValid(float distance);

			abstract float getDistanceMultiplier();

			abstract Color getColor();
		}

	}

}
