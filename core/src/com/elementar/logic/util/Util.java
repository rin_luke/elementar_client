package com.elementar.logic.util;

import static com.elementar.logic.util.Shared.WALKABLE_ANGLE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.util.Shared.CategoryFunctionality;
import com.elementar.logic.util.Shared.Team;

public class Util {

	/**
	 * Out-of-place operation (creates a new Vector2 reference)
	 * 
	 * @param vector1
	 * @param vector2
	 * @param amount
	 * @return
	 */
	public static Vector2 addVectors(Vector2 vector1, Vector2 vector2, float amount) {
		float x = vector1.x + amount * vector2.x;
		float y = vector1.y + amount * vector2.y;
		return new Vector2(x, y);
	}

	/**
	 * Out-of-place operation (creates a new Vector2 reference)
	 * 
	 * @param vector1
	 * @param vector2
	 * @return
	 */
	public static Vector2 addVectors(Vector2 vector1, Vector2 vector2) {
		return addVectors(vector1, vector2, 1);
	}

	/**
	 * Out-of-place operation (creates a new Vector2 reference). Subtracts
	 * vector2 amount-times from vector1.
	 * 
	 * @param vector1
	 * @param vector2
	 * @param amount
	 * @return
	 */
	public static Vector2 subVectors(Vector2 vector1, Vector2 vector2, float amount) {
		float x = vector1.x - amount * vector2.x;
		float y = vector1.y - amount * vector2.y;
		return new Vector2(x, y);
	}

	/**
	 * Out-of-place operation (creates a new Vector2 reference). Subtract two
	 * {@link Vector2} objects.
	 * 
	 * @param vector1
	 * @param vector2
	 * @return
	 */
	public static Vector2 subVectors(Vector2 vector1, Vector2 vector2) {
		return subVectors(vector1, vector2, 1);
	}

	public static float[] convertVector2ArrayIntoFloatArray(Vector2[] vector2Vertices) {

		float floatArray[] = new float[vector2Vertices.length * 2];

		for (int i = 0; i < vector2Vertices.length; i++) {
			floatArray[i * 2] = vector2Vertices[i].x;
			floatArray[i * 2 + 1] = vector2Vertices[i].y;
		}
		return floatArray;
	}

	public static Vector2[] convertFloatArrayIntoVector2Array(float[] floatVertices) {
		Vector2[] vector2Array = new Vector2[floatVertices.length / 2];

		for (int i = 0; i < floatVertices.length; i += 2) {
			vector2Array[Math.round(i / 2)] = new Vector2(floatVertices[i], floatVertices[i + 1]);
		}
		return vector2Array;
	}

	public static ArrayList<Vector2> convertVector2ArrayIntoVector2ArrayList(Vector2[] verticesArray) {
		return new ArrayList<Vector2>(Arrays.asList(verticesArray));
	}

	public static Vector2[] convertVector2ArrayListIntoVector2Array(ArrayList<Vector2> verticesList) {
		return verticesList.toArray(new Vector2[verticesList.size()]);
	}

	/**
	 * Creates a new scaled list (out-of-place algorithm)
	 * 
	 * @param vertices
	 * @param amount
	 * @return
	 */
	public static Vector2[] scaleVertices(Vector2[] vertices, float amount) {
		Vector2[] scaledVertices = new Vector2[vertices.length];

		scaledVertices[0] = vertices[0];
		for (int i = 1; i < vertices.length; i++) {
			scaledVertices[i] = addVectors(scaledVertices[i - 1], subVectors(vertices[i], vertices[i - 1]), amount);
		}

		return scaledVertices;

	}

	/**
	 * Takes an {@link ArrayList} and returns an {@link ArrayList}
	 * 
	 * @param startPoint
	 * @param localVertices
	 *            , won't be touched, out-of-place algorithm
	 * @return
	 */
	public static ArrayList<Vector2> shiftVerticesTo(Vector2 startPoint, ArrayList<Vector2> localVertices) {
		ArrayList<Vector2> globalVertices = new ArrayList<Vector2>(localVertices.size());
		for (int i = 0; i < localVertices.size(); i++)
			globalVertices.add(i, addVectors(localVertices.get(i), startPoint));
		return globalVertices;
	}

	/**
	 * Takes a simple array and returns an array
	 * 
	 * @param startPoint
	 * @param localVertices
	 *            , won't be touched, out-of-place algorithm
	 * @return
	 */
	public static Vector2[] shiftVerticesTo(Vector2 startPoint, Vector2[] localVertices) {
		Vector2[] globalVertices = new Vector2[localVertices.length];
		for (int i = 0; i < localVertices.length; i++)
			globalVertices[i] = addVectors(localVertices[i], startPoint);
		return globalVertices;
	}

	public static Polygon getPolygon(ArrayList<Vector2> vertices) {
		return new Polygon(Util.convertVector2ArrayIntoFloatArray(vertices.toArray(new Vector2[vertices.size()])));
	}

	/**
	 * When I calculate the angle with the help of physics (normals) then I
	 * might be that angle is higher than 360. For sliding platforms we need to
	 * subtract 180 when its higher than 180. (to check both cases (ranges look
	 * like a X))
	 * 
	 * @param anglePlatform
	 * @return
	 */
	public static int minimizePlatformAngle(int anglePlatform) {
		/*
		 * this is needed because at collisions (in ServerContactListener.java)
		 * the calculated angle might be greater than 360
		 */
		if (anglePlatform >= 360)
			anglePlatform -= 360;

		/*
		 * for slidables, will flip the angle from bottom side of unit circle to
		 * top side, not needed anymore
		 */
		// if (anglePlatform >= 180)
		// anglePlatform -= 180;
		return anglePlatform;
	}

	/**
	 * 
	 * @param viewCircle
	 *            (contains position and radius)
	 * @param pos
	 * @return
	 */
	public static boolean circleContains(ViewCircle viewCircle, Vector2 pos) {
		float dx = viewCircle.getPos().x - pos.x;
		float dy = viewCircle.getPos().y - pos.y;
		return dx * dx + dy * dy <= viewCircle.getRadius() * viewCircle.getRadius();
	}

	/**
	 * For further informations see
	 * dropbox/Implementierung/Modelle/getPlatformFunctionality.png
	 * 
	 * @param angle
	 * @return
	 */
	public static CategoryFunctionality getPlatformFunctionality(int angle) {
		CategoryFunctionality functionality;

		// is slidable?
		if (PhysicalClient.isSlidableByAngle(angle) == true)
			functionality = CategoryFunctionality.SLIDE;
		// is nonwalkable?
		else if (Util.isNonWalkablePlatform(angle) == true)
			functionality = CategoryFunctionality.NON_WALK;
		else
			functionality = CategoryFunctionality.WALK;

		return functionality;
	}

	private static boolean isNonWalkablePlatform(int angle) {

		/*
		 * The commented part will increase the amount of nonwalk- platforms
		 */

		if (angle > 180 - WALKABLE_ANGLE// - 10
				&& angle < 180 + WALKABLE_ANGLE)// + 10)
			return true;

		return false;
	}

	public static void sleep(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
		}

	}

	public static int getMinValue(int[] array) {
		int minValue = array[0];
		for (int i = 1; i < array.length; i++)
			if (array[i] < minValue)
				minValue = array[i];
		return minValue;
	}

	/**
	 * Flips the angle over y axis to the other side of the unit circle. <br>
	 * Unit circle for angle between cursor and player:
	 * <p>
	 * 
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 * &nbsp;&nbsp;90
	 * <p>
	 * <p>
	 * 180 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0 <br>
	 * -180
	 * <p>
	 * <p>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 * &nbsp;-90
	 * 
	 * @param rotationAngle
	 * @return
	 */
	public static short switchAngleUnitCircle(short rotationAngle) {
		return (short) (rotationAngle - 2 * (rotationAngle > 0 ? rotationAngle - 90 : rotationAngle + 90));
	}

	/**
	 * Same as {@link #switchAngleUnitCircle(short)} but with float Flips the
	 * angle over y axis to the other side of the unit circle. <br>
	 * Unit circle for angle between cursor and player:
	 * <p>
	 * 
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 * &nbsp;&nbsp;90
	 * <p>
	 * <p>
	 * 180 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0 <br>
	 * -180
	 * <p>
	 * <p>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 * &nbsp;-90
	 * 
	 * @param rotationAngle
	 * @return
	 */
	public static float switchAngleUnitCircle(float rotationAngle) {
		return (short) (rotationAngle - 2 * (rotationAngle > 0 ? rotationAngle - 90 : rotationAngle + 90));
	}

	public static <T extends Enum<?>> T randomEnum(Class<T> clazz) {
		return clazz.getEnumConstants()[MathUtils.random(0, clazz.getEnumConstants().length - 1)];
	}

	private static ArrayList<Float> distances = new ArrayList<Float>();

	/**
	 * This method takes 2 platforms as parameter ( <plat11, plat12> and
	 * <plat21, plat22> ), and calculates the minimal distance between those. If
	 * both intersects the distance will be -1.
	 * 
	 * @param plat11
	 *            , platform 1
	 * @param plat12
	 *            , platform 1
	 * @param plat21
	 *            , platform 2
	 * @param plat22
	 *            , platform 2
	 * @return
	 */
	public static float distanceSegments(Vector2 plat11, Vector2 plat12, Vector2 plat21, Vector2 plat22) {
		if (Intersector.intersectSegments(plat11, plat12, plat21, plat22, null) == true)
			return -1;

		synchronized (distances) {

			distances.clear();
			distances.add(Intersector.distanceSegmentPoint(plat11, plat12, plat21));
			distances.add(Intersector.distanceSegmentPoint(plat11, plat12, plat22));
			distances.add(Intersector.distanceSegmentPoint(plat21, plat22, plat11));
			distances.add(Intersector.distanceSegmentPoint(plat21, plat22, plat12));

			return Collections.min(distances);
		}
	}

	public static short getAngleBetweenTwoPoints(float x1, float y1, float x2, float y2) {
		return (short) (MathUtils.radiansToDegrees * (MathUtils.atan2(y2 - y1, x2 - x1)));
	}

	public static short getAngleBetweenTwoPoints(Vector2 pos1, Vector2 pos2) {
		return getAngleBetweenTwoPoints(pos1.x, pos1.y, pos2.x, pos2.y);
	}

	public static Vector2 getPolarCoords(float length, float angle) {
		// adding 0.0f to avoid -0.0f initiation
		return new Vector2(length * MathUtils.cosDeg(angle) + 0.0f, length * MathUtils.sinDeg(angle) + 0.0f);
	}

	/**
	 * Returns the team number (= 1 or 2)
	 * 
	 * @param team
	 * @return
	 */
	public static String getTeamNumber(Team team) {
		return team.name().substring(team.name().length() - 1);
	}

	public static <K, V> void removeByValue(HashMap<K, V> map, V value) {

		for (Iterator<Entry<K, V>> it = map.entrySet().iterator(); it.hasNext();)
			if (it.next().getValue() == value) {
				it.remove();
				return;
			}

	}

	public static boolean isNan(Vector2 vec) {
		return Float.isNaN(vec.x) || Float.isNaN(vec.y);
	}

}
