package com.elementar.logic.util;

public class Shared {

	public static enum ExecutionMode {
		CLIENT, GAMESERVER, MAINSERVER
	}

	public static final ExecutionMode EXECUTION_MODE = ExecutionMode.CLIENT;
//	public static final ExecutionMode EXECUTION_MODE = ExecutionMode.GAMESERVER;
//	public static final ExecutionMode EXECUTION_MODE = ExecutionMode.MAINSERVER;

	public static final boolean STEAM_ON = false;

	/**
	 * if true ingame foreground has just 1 island
	 */
	public static final boolean DEBUG_MODE_ISLAND_REDUCE = false;
	public static final boolean DEBUG_MODE_MIRROR_MAP = true;

	public static final boolean DEBUG_MODE_SUPERMAN = true;

	/**
	 * Draw red vertices for each island vertex, and edges in cyan
	 */
	public static final boolean DEBUG_MODE_DRAW_CYAN_CONTOURS_AND_RED_VERTICES_OF_ISLANDS = false;
	/**
	 * Draw vegetation viewport and player viewport
	 */
	public static final boolean DEBUG_MODE_DRAW_VIEWPORTS = false;
	public static final boolean DEBUG_MODE_DRAW_CYAN_MAPBORDER = true;
	public static final boolean DEBUG_MODE_RENDER_LIGHT = true;

	public static final boolean DEBUG_MODE_SYSOS_NETCODE_CLIENT = false;
	
	/**
	 * Print sysos on game-server side
	 */
	public static final boolean DEBUG_MODE_SYSOS_NETCODE_SERVER = false;

	/**
	 * if true you are allowed to enable or disable debuglines by pressing the key
	 * "G". Example for debuglines, 1. GUI: buttons, labels, 2. BOX2D ingame.
	 */
	public static final boolean DEBUG_MODE_DEBUGLINES = true;

	/**
	 * Don't set this variable here. Just click G for enabling or disabling. Keep in
	 * mind that {@link #DEBUG_MODE_DEBUGLINES} must be set to true. Otherwise
	 * clicking G has no effect.
	 */
	public static boolean BOX2D_DEBUGLINES = false; // let it to
													// false

	public static final String GAME_VERSION = "Version 0.9.2";

	/**
	 * blue for tweets: r = 21, g = 51, b = 89 OR ALL +100 value range from 0
	 * (black) to 255 (white)
	 */
	public static final float CLEAR_RED = 0f, CLEAR_GREEN = 0f, CLEAR_BLUE = 0f;

	public static final int MAX_PLAYERNAME_LENGTH = 15;
	public static final int MAX_CLANTAG_LENGTH = 5;
	public static final int MAX_GAMESERVERNAME_LENGTH = 60;
	public static final int MAX_GAMESERVERPASSWORD_LENGTH = 60;

	public static final int INITIAL_REMAINING_LIVES = 10;

	public static final int DEFAULT_SERVER_SIZE = 6;
	public static final int MAX_SERVER_SIZE = 6;
	public static final int MIN_SERVER_SIZE = 2;

	public static final int MAX_TEAM_CAPACITY = MAX_SERVER_SIZE / 2;

	public static final float ALPHA_HOVER = 0.7f, ALPHA_DISABLED = 0.3f;

	public static final float WIDGET_HOVER_BRIGHTNESS = 0.15f, WIDGET_HOVER_CONTRAST = 1.3f; // 1.3f
	/**
	 * Defines not the font alpha but the sprite alpha of a widget
	 */
	public static final float WIDGET_DISABLED_ALPHA = 0.3f;
	public static final float PADDING_LEFT_HEIGHT1 = 20, PADDING_LEFT_HEIGHT2 = 40, PADDING_BOTTOM = 1;

	public static final float HEIGHT1 = 45, HEIGHT2 = 90, HEIGHT3 = 135, HEIGHT4 = 180, WIDTH1 = 40, WIDTH2 = 80,
			WIDTH3 = 160, WIDTH4 = 240, WIDTH5 = 320, WIDTH6 = 400, WIDTH7 = 480, WIDTH8 = 560, WIDTH9 = 640;

	/**
	 * pixel per meter, used as multiplier
	 */
	public static final float PPM = 100;

	public static final float WORLD_VIEWPORT_WIDTH = 13f;// 11
	public static final float WORLD_VIEWPORT_HEIGHT = 7.33f;// 6.2f;
// "79.205.255.6"
	public static final String IP_MAINSERVER = "elementar.ddns.net";
	// "ec2-18-232-34-72.compute-1.amazonaws.com";

	public static int CLIENT_UDP_TO_MAIN;
	public static int CLIENT_TCP_TO_MAIN;
	public static int GAMESERVER_UDP_TO_MAIN;
	public static int GAMESERVER_TCP_TO_MAIN;

	public static void setPorts() {

		CLIENT_UDP_TO_MAIN = 13336;
		CLIENT_TCP_TO_MAIN = 13337;
		GAMESERVER_UDP_TO_MAIN = 13338;
		GAMESERVER_TCP_TO_MAIN = 13339;

	}

	public static final int MAX_CONNECTING_TIME = 10000; // Miliseconds

	/**
	 * the {@link #SEND_AND_UPDATE_RATE_WORLD} describes how often a physical
	 * simulation step occurs + send rate. To match physical stuff with rendering
	 * the {@link #SEND_AND_UPDATE_RATE_WORLD} is also the timestep of the
	 * simulation steps, so the nyquist theorem is successfully applied
	 */
	public static final float SEND_AND_UPDATE_RATE_WORLD = 1 / 50f; // 25 hz
	public static final float SEND_RATE_META = 1 / 20f;
	/**
	 * Same as {@link #SEND_AND_UPDATE_RATE_WORLD} but in milliseconds
	 */
	public static final long SEND_AND_UPDATE_RATE_IN_MS = (long) (SEND_AND_UPDATE_RATE_WORLD * 1000);

	// world stuff

	public static final float GRAVITY = -7f;
	public static final int VELOCITY_ITERATIONS = 8;
	public static final int POSITION_ITERATIONS = 3;

	public static final int MAP_WIDTH = DEBUG_MODE_ISLAND_REDUCE ? 40 : 28; // 100
	public static final int MAP_HEIGHT = DEBUG_MODE_ISLAND_REDUCE ? 20 : 12; // 25

	/**
	 * for platform and vegetation sprites
	 */
	public static final float SCALE_PLATFORM_AND_VEGETATION_SPRITES = 0.2f / PPM;

	/**
	 * for world buildings like bases, towers, shrines and base-cannons
	 */
	public static final float SCALE_BUILDINGS = 0.32f / PPM;

	public static final float SCALE_BG = 1f / PPM;

	public static final float SCALE_MINIMAP_SYMBOLS = 6.5f / PPM;

	/**
	 * just for "in-world", not in gameclass scroller (skin shop).
	 */
	public static final float SCALE_CHARACTER_INGAME = 0.1f / PPM;

	/**
	 * just for "in-world", not in gameclass scroller (skin shop).
	 */
	public static final float SCALE_CREEP_INGAME = 0.08f / PPM;

	/**
	 * for skill preview sprites that are used when the player casts a skill
	 */
	public static final float SCALE_SKILL_PREVIEW = 0.5f / PPM;

	public static final float NEARLY_ZERO = .00001f;

	// basic movement

	public static final float WALKABLE_ANGLE = 60f; // lower than WALKABLE ANGLE
													// means walkable

	public static float CHARACTER_MAX_VELOCITY_X = 250 / PPM;

	public static float CREEP_MAX_VELOCITY = 200 / PPM;

	public static enum PickMode {
		/**
		 * players pick simultaneously characters.
		 */
		ALL_PICK,

		/**
		 * players pick characters alternating.
		 */
		DRAFT_PICK
	}

	public static enum ClientState {

		/**
		 * inside picking phase where player is selecting team
		 */
		PICKING_TEAM,

		/**
		 * inside picking phase where player is selecting character
		 */
		PICKING_GAMECLASS,

		MATCH
	}

	/**
	 * Used to categorize platforms and vegetation
	 * 
	 * @author lukassongajlo
	 * 
	 */
	public static enum CategorySize {
		SMALL, MEDIUM, LARGE
	}

	/**
	 * Used to categorize platforms and vegetation
	 * 
	 * @author lukassongajlo
	 * 
	 */
	public static enum CategoryFunctionality {
		SLIDE, WALK, NON_WALK
	}

	public static enum Team {
		TEAM_1, TEAM_2, BOTH
	}

}
