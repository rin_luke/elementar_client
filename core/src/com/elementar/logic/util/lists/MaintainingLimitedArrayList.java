package com.elementar.logic.util.lists;

public class MaintainingLimitedArrayList<E> extends ALimitedArrayList<E> {

	public MaintainingLimitedArrayList(int limit) {
		super(limit);

	}

	@Override
	public boolean add(E e) {
		super.add(0, e);
		if (size() > limit)
			super.remove(size() - 1);
		return true;
	}

}
