package com.elementar.logic.util.lists;

/**
 * If the limit has exceeded the element won't be added
 * 
 * @author lukassongajlo
 * 
 * @param <E>
 */
public class DiscardingLimitedArrayList<E> extends ALimitedArrayList<E> {

	public DiscardingLimitedArrayList(int limit) {
		super(limit);
	}

	/**
	 * Returns <code>false</code> if the limit has been reached
	 */
	@Override
	public boolean add(E e) {
		if (size() + 1 > limit)
			return false;
		return super.add(e);
	}
}
