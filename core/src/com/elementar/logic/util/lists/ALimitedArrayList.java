package com.elementar.logic.util.lists;

import java.util.ArrayList;

/**
 * Holds a field for limitation but does not implement the functionality itself
 * (abstract).
 * 
 * @author lukassongajlo
 *
 * @param <E>
 */
public abstract class ALimitedArrayList<E> extends ArrayList<E> {

	protected int limit;

	public ALimitedArrayList(int limit) {
		this.limit = limit;
	}

	public int getLimit() {
		return limit;
	}

	public boolean isFull() {
		return limit == size();
	}

}
