package com.elementar.logic.util.lists;

import java.util.ArrayList;

import com.badlogic.gdx.math.MathUtils;
import com.elementar.logic.util.Shared.CategoryFunctionality;
import com.elementar.logic.util.Shared.CategorySize;

public class CategoryArrayList<E> extends ArrayList<E> {

	private CategoryFunctionality functionality;
	private CategorySize size;

	public CategoryArrayList(CategoryFunctionality functionality,
			CategorySize size) {
		super();
		this.functionality = functionality;
		this.size = size;
	}

	public E getRandomString() {
		return get(MathUtils.random(size() - 1));
	}

	public CategorySize getCategorySize() {
		return size;
	}

	public CategoryFunctionality getCategoryFunctionality() {
		return functionality;
	}
}
