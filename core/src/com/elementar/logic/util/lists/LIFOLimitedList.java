package com.elementar.logic.util.lists;

public class LIFOLimitedList<E> extends ALimitedArrayList<E> {

	public LIFOLimitedList(int limit) {
		super(limit);
	}

	@Override
	public boolean add(E e) {
		super.add(e);
		while (size() > limit)
			super.remove(0);
		return true;
	}
}
