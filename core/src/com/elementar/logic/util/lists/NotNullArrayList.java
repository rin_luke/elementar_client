package com.elementar.logic.util.lists;

import java.util.ArrayList;

public class NotNullArrayList<E> extends ArrayList<E> {

	public NotNullArrayList(int initialCapacity) {
		super(initialCapacity);
	}

	public NotNullArrayList() {
		super();
	}

	@Override
	public boolean add(E e) {
		if (e != null)
			return super.add(e);
		return false;
	}

	public void addAll(NotNullArrayList<E> list) {
		for (E e : list)
			add(e);
	}

	@Override
	public E get(int index) {
		if (size() > 0)
			return super.get(index);
		else
			return null;
	}
}
