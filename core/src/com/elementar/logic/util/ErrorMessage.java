package com.elementar.logic.util;

/**
 * The notation of the error messages are different to other enum notations.
 * They are also used as keys to find the corresponding string inside of the
 * bundle file.
 * 
 * @author lukassongajlo
 *
 */
public enum ErrorMessage {

	abilityOnCooldown, notEnoughMana, noAllyAround, invalidIPAddress, invalidPorts, serverFull, incorrectPassword, noConnectionMainserver, noServerName;

}
