package com.elementar.logic.util;

import java.util.Random;

public class MyRandom {

	Random random = new Random();

	/**
	 * Really random
	 */
	public MyRandom() {
	}

	/**
	 * Pseudo random with a seed.
	 * 
	 * @param seed
	 */
	public MyRandom(long seed) {
		random.setSeed(seed);
	}

	/**
	 * Returns a random number between 0 (inclusive) and the specified value
	 * (inclusive).
	 */
	public int random(int range) {
		return random.nextInt(range + 1);
	}

	/** Returns a random number between start (inclusive) and end (inclusive). */
	public int random(int start, int end) {
		return start + random.nextInt(end - start + 1);
	}

	/**
	 * Returns a random number between 0 (inclusive) and the specified value
	 * (inclusive).
	 */
	public long random(long range) {
		return (long) (random.nextDouble() * range);
	}

	/** Returns a random number between start (inclusive) and end (inclusive). */
	public long random(long start, long end) {
		return start + (long) (random.nextDouble() * (end - start));
	}

	/** Returns a random boolean value. */
	public boolean randomBoolean() {
		return random.nextBoolean();
	}

	/** Returns random number between 0.0 (inclusive) and 1.0 (exclusive). */
	public float random() {
		return random.nextFloat();
	}

	/**
	 * Returns a random number between 0 (inclusive) and the specified value
	 * (exclusive).
	 */
	public float random(float range) {
		return random.nextFloat() * range;
	}

	/** Returns a random number between start (inclusive) and end (exclusive). */
	public float random(float start, float end) {
		return start + random.nextFloat() * (end - start);
	}

	public Random getRandom() {
		return random;
	}

	public void dispose() {
		random = null;
	}
}
