package com.elementar.logic.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalTime {

	public static String get() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
		return dtf.format(LocalDateTime.now());
	}
}
