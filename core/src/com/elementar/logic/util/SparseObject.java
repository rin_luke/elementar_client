package com.elementar.logic.util;

import com.badlogic.gdx.math.Vector2;

public class SparseObject {

	public short connection_id;

	public Vector2 pos = new Vector2();
	public Vector2 vel = new Vector2();

	public SparseObject() {
	}

	public SparseObject(short id, Vector2 pos, Vector2 vel) {
		this.connection_id = id;
		this.pos.set(pos);
		this.vel.set(vel);
	}

}
