package com.elementar.logic.map.pathfinding;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Node {

	/**
	 * if <code>true</code> this node will be ignored while searching with A*
	 */
	private boolean contains_obstacle;

	/**
	 * pos is the bottom left corner of this cell
	 */
	private Vector2 pos;
	private int index;
	private Array<Connection<Node>> connections;

	public Node(Vector2 pos, boolean containsObstacle, int index) {
		this.pos = pos;
		this.index = index;
		contains_obstacle = containsObstacle;
		connections = new Array<>();
	}

	public boolean containsObstacle() {
		return contains_obstacle;
	}

	public void setContainsObstacle(boolean containsObstacle) {
		contains_obstacle = containsObstacle;
	}

	public Vector2 getPos() {
		return pos;
	}

	public int getIndex() {
		return index;
	}

	public Array<Connection<Node>> getConnections() {
		return connections;
	}

	public void addConnection(Node toNode, float cost) {
		connections.add(new MyConnection(this, toNode, cost));
	}

	private class MyConnection implements Connection<Node> {

		private Node from_node, to_node;

		private float cost;

		public MyConnection(Node fromNode, Node toNode, float cost) {
			from_node = fromNode;
			to_node = toNode;
			this.cost = cost;
		}

		@Override
		public float getCost() {
			return cost;
		}

		@Override
		public Node getFromNode() {
			return from_node;
		}

		@Override
		public Node getToNode() {
			return to_node;
		}

	}

}
