package com.elementar.logic.map.pathfinding;

import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;
import com.badlogic.gdx.ai.pfa.indexed.IndexedGraph;

/**
 * Note that the heuristic function and cost function should have the same
 * "base". Look at:
 * http://www.growingwiththeweb.com/2012/06/a-pathfinding-algorithm.html
 * 
 * @author lukassongajlo
 * 
 */
public class PathFinder extends IndexedAStarPathFinder<Node> {

	private HeuristicUncared heuristic = new HeuristicUncared();

	private GraphPath<Node> result_path;

	public PathFinder(IndexedGraph<Node> graph) {
		super(graph);
	}

	public boolean searchNodePath(Node startNode, Node endNode) {
		result_path = new DefaultGraphPath<Node>();
		return super.searchNodePath(startNode, endNode, heuristic, result_path);
	}

	/**
	 * Might be <code>null</code>
	 * 
	 * @return
	 */
	public GraphPath<Node> getSolution() {
		return result_path;
	}

}
