package com.elementar.logic.map.pathfinding;

import java.util.ArrayList;
import java.util.LinkedList;

public class LinkedPaths extends LinkedList<ArrayList<Node>> {

	private PathDirection path_direction;
	private int version = 0, insertion_index = 0;

	public LinkedPaths() {
		super();
	}

	public static LinkedPaths makeCopy(LinkedPaths paths) {
		LinkedPaths copy = new LinkedPaths();

		for (ArrayList<Node> subPath : paths)
			copy.add(new ArrayList<>(subPath));

		copy.path_direction = paths.path_direction;
		copy.insertion_index = paths.insertion_index;
		copy.version = paths.version;

		return copy;
	}

	public PathDirection getPathDirection() {
		return path_direction;
	}

	public void setPathDirection(PathDirection pathDirection) {
		this.path_direction = pathDirection;
	}

	public void incVersion() {
		this.version++;
	}

	public int getVersion() {
		return version;
	}

	/**
	 * Insetion index is an index in the linked list of sub paths. The creep has
	 * to go on a lower indexed sub path to get the new path. Othwise the creep
	 * won't get the most recent version of the path.
	 * 
	 * @param index
	 */
	public void setInsertionIndex(int index) {
		this.insertion_index = index;
	}

	public int getInsertionIndex() {
		return insertion_index;
	}

	public static enum PathDirection {
		TOP_FROM_LEFT, TOP_FROM_RIGHT, BOT_FROM_LEFT, BOT_FROM_RIGHT
	}
}
