package com.elementar.logic.map.pathfinding;

import java.util.ArrayList;
import java.util.Collections;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.indexed.IndexedGraph;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.elementar.logic.util.Shared;

public class Graph implements IndexedGraph<Node> {

	private Array<Node> nodes;

	/**
	 * cell is quadratic so we only need the side length
	 */
	public static final float CELL_SIDE_LENGTH = 0.5f;// 0.8f

	private static final float OFFSET_BORDER_X = 3;
	private static final float OFFSET_BORDER_Y = 8;

	public static final int NUM_CELLS_PER_ROW = (int) ((Shared.MAP_WIDTH + OFFSET_BORDER_X) / CELL_SIDE_LENGTH);
	public static final int NUM_CELLS_PER_COL = (int) ((Shared.MAP_HEIGHT + OFFSET_BORDER_Y) / CELL_SIDE_LENGTH);

	public static final float HALF_MAP_WIDTH = (Shared.MAP_WIDTH + OFFSET_BORDER_X) * 0.5f,
			HALF_MAP_HEIGHT = (Shared.MAP_HEIGHT + OFFSET_BORDER_Y) * 0.5f;

	public Graph(Array<Node> nodes) {
		this.nodes = nodes;
	}

	@Override
	public Array<Connection<Node>> getConnections(Node fromNode) {
		return fromNode.getConnections();
	}

	@Override
	public int getIndex(Node node) {
		return node.getIndex();
	}

	@Override
	public int getNodeCount() {
		return nodes.size;
	}

	public Array<Node> getNodes() {
		return nodes;
	}

	public static Node getNodeByPos(Vector2 pos, Array<Node> nodes) {
		int cellX = (int) ((pos.x + HALF_MAP_WIDTH) / CELL_SIDE_LENGTH);
		int cellY = (int) ((pos.y + HALF_MAP_HEIGHT) / CELL_SIDE_LENGTH);
		int index = cellY * NUM_CELLS_PER_ROW + cellX;
		if (index >= 0 && index < nodes.size)
			return nodes.get(index);
		return null;
		// return new Node(new Vector2(1000, 1000), true, 1000000);
	}

	public static Node getUpperNeighbor(Node node, Array<Node> nodes) {
		Vector2 pos = new Vector2(node.getPos());
		// get one row higher
		pos.y += CELL_SIDE_LENGTH;
		return getNodeByPos(pos, nodes);
	}

	public static Node getLowerNeighbor(Node node, Array<Node> nodes) {
		Vector2 pos = new Vector2(node.getPos());
		// get one row lower
		pos.y = pos.y - CELL_SIDE_LENGTH;
		return getNodeByPos(pos, nodes);
	}

	public static Node getRightNeighbor(Node node, Array<Node> nodes) {
		Vector2 pos = new Vector2(node.getPos());
		// get one row right
		pos.x += CELL_SIDE_LENGTH;
		return getNodeByPos(pos, nodes);
	}

	public static Node getLeftNeighbor(Node node, Array<Node> nodes) {
		Vector2 pos = new Vector2(node.getPos());
		// get one left higher
		pos.x = pos.x - CELL_SIDE_LENGTH;
		return getNodeByPos(pos, nodes);
	}

	public ArrayList<LightCluster> getLightClusters() {

		ArrayList<LightCluster> lightClusters = new ArrayList<>();

		Node upperNode, upperLeftNode, upperRightNode, lowerNode, lowerLeftNode, lowerRightNode, leftNode, rightNode;
		for (Node centerNode : nodes) {
			// find a cluster

			// check upper
			upperNode = getUpperNeighbor(centerNode, nodes);
			if (upperNode == null || upperNode.containsObstacle() == true)
				continue;
			// upperLeftNode = getLeftNeighbor(upperNode, nodes);
			// if (upperLeftNode == null || upperLeftNode.containsObstacle() ==
			// true)
			// continue;
			// upperRightNode = getRightNeighbor(upperNode, nodes);
			// if (upperRightNode == null || upperRightNode.containsObstacle()
			// == true)
			// continue;

			// check lower
			lowerNode = getLowerNeighbor(centerNode, nodes);
			if (lowerNode == null || lowerNode.containsObstacle() == true)
				continue;
			// lowerLeftNode = getLeftNeighbor(lowerNode, nodes);
			// if (lowerLeftNode == null || lowerLeftNode.containsObstacle() ==
			// true)
			// continue;
			// lowerRightNode = getRightNeighbor(lowerNode, nodes);
			// if (lowerRightNode == null || lowerRightNode.containsObstacle()
			// == true)
			// continue;

			// check left
			leftNode = getLeftNeighbor(centerNode, nodes);
			if (leftNode == null || leftNode.containsObstacle() == true)
				continue;

			// check right
			rightNode = getRightNeighbor(centerNode, nodes);
			if (rightNode == null || rightNode.containsObstacle() == true)
				continue;

			int vertQuality = recVertQualityCheck(upperNode, lowerNode, 0);
			int horiQuality = recHoriQualityCheck(leftNode, rightNode, 0);

			lightClusters.add(new LightCluster(centerNode, vertQuality, horiQuality));

		}

		Collections.sort(lightClusters);

		return lightClusters;
	}

	private int recVertQualityCheck(Node upperNode, Node lowerNode, int quality) {
		Node nextUpperNode = getUpperNeighbor(upperNode, nodes);
		if (nextUpperNode == null || nextUpperNode.containsObstacle() == true)
			return quality;
		Node nextLowerNode = getLowerNeighbor(lowerNode, nodes);
		if (nextLowerNode == null || nextLowerNode.containsObstacle() == true)
			return quality;
		quality++;
		return recVertQualityCheck(nextUpperNode, nextLowerNode, quality);
	}

	private int recHoriQualityCheck(Node leftNode, Node rightNode, int quality) {
		Node nextLeftNode = getLeftNeighbor(leftNode, nodes);
		if (nextLeftNode == null || nextLeftNode.containsObstacle() == true)
			return quality;
		Node nextRightNode = getRightNeighbor(rightNode, nodes);
		if (nextRightNode == null || nextRightNode.containsObstacle() == true)
			return quality;
		quality++;
		return recHoriQualityCheck(nextLeftNode, nextRightNode, quality);
	}

}
