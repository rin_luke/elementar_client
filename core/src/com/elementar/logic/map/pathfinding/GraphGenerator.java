package com.elementar.logic.map.pathfinding;

import java.util.ArrayList;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.elementar.logic.map.MapBorder;
import com.elementar.logic.util.Util;

public class GraphGenerator {

	public static Graph generate(ArrayList<Vector2[]> mapHitboxes, MapBorder mapBorder) {

		Array<Node> nodes = new Array<>();

		Vector2[] localCell = { new Vector2(0, 0), new Vector2(0, Graph.CELL_SIDE_LENGTH),
				new Vector2(Graph.CELL_SIDE_LENGTH, Graph.CELL_SIDE_LENGTH), new Vector2(Graph.CELL_SIDE_LENGTH, 0) };

		Vector2[] globalCell;

		// will be updated by indices
		Vector2 indexedPos;

		int count = 0;

		// create nodes
		for (int i = 0; i < Graph.NUM_CELLS_PER_COL; i++)
			for (int j = 0; j < Graph.NUM_CELLS_PER_ROW; j++) {
				Node node;

				// position cell
				indexedPos = new Vector2(j * Graph.CELL_SIDE_LENGTH - Graph.HALF_MAP_WIDTH,
						i * Graph.CELL_SIDE_LENGTH - Graph.HALF_MAP_HEIGHT);
				globalCell = Util.shiftVerticesTo(indexedPos, localCell);

				boolean containsObstacle = false;

				// is the cell outside of the map?
				Polygon mapHitbox = new Polygon(Util.convertVector2ArrayIntoFloatArray(mapBorder.getLocalVertices()));
				Array<Vector2> mapHitboxArray = new Array<>(mapBorder.getLocalVertices());
				for (int k = 1; k <= globalCell.length; k++) {
					Vector2 cellPoint1 = globalCell[k - 1];
					Vector2 cellPoint2;
					if (k == globalCell.length)
						cellPoint2 = globalCell[0];
					else
						cellPoint2 = globalCell[k];

					if (Intersector.intersectSegmentPolygon(cellPoint1, cellPoint2, mapHitbox) == true
							|| Intersector.isPointInPolygon(mapHitboxArray, cellPoint1) == false) {
						containsObstacle = true;
						break;
					}

				}

				if (containsObstacle == false)
					// is the cell inside of an island?
					outerloop2: for (Vector2[] hitbox : mapHitboxes) {
						Array<Vector2> hitboxAsArray = new Array<Vector2>(hitbox);
						for (Vector2 cellPoint : globalCell)
							if (Intersector.isPointInPolygon(hitboxAsArray, cellPoint) == true) {
								containsObstacle = true;
								break outerloop2;
							}
					}

				// center position
				indexedPos.x += Graph.CELL_SIDE_LENGTH * 0.5f;
				indexedPos.y += Graph.CELL_SIDE_LENGTH * 0.5f;

				node = new Node(indexedPos, containsObstacle, count);
				count++;
				nodes.add(node);
			}

		// increase distance to ceilings
//		for (Node node : nodes)
//			recSetLowerObstacleColumn(node, nodes, 0);

		/*
		 * Note that connections are only necessary when a path is going to be calculated
		 */
//		float costDiagonal = (float) Math.sqrt(2);
//		float costStraight = 1f;
//
//		// make connections between nodes
//		for (int i = 0; i < Graph.NUM_CELLS_PER_COL; i++)
//			for (int j = 0; j < Graph.NUM_CELLS_PER_ROW; j++) {
//				Node node = nodes.get(i * Graph.NUM_CELLS_PER_ROW + j);
//
//				// left
//				if (j - 1 >= 0)
//					makeConnection(node, nodes.get(i * Graph.NUM_CELLS_PER_ROW + j - 1), costStraight);
//
//				// right
//				if (j + 1 < Graph.NUM_CELLS_PER_ROW)
//					makeConnection(node, nodes.get(i * Graph.NUM_CELLS_PER_ROW + j + 1), costStraight);
//
//				// bottom
//				if (i - 1 >= 0)
//					makeConnection(node, nodes.get(Graph.NUM_CELLS_PER_ROW * (i - 1) + j), costStraight);
//
//				// top
//				if (i + 1 < Graph.NUM_CELLS_PER_COL)
//					makeConnection(node, nodes.get(Graph.NUM_CELLS_PER_ROW * (i + 1) + j), costStraight);
//
//				// diagonal
//				// left bottom
//				if (j - 1 >= 0 && i - 1 >= 0)
//					makeConnection(node, nodes.get(Graph.NUM_CELLS_PER_ROW * (i - 1) + j - 1), costDiagonal);
//
//				// left top
//				if (j - 1 >= 0 && i + 1 < Graph.NUM_CELLS_PER_COL)
//					makeConnection(node, nodes.get(Graph.NUM_CELLS_PER_ROW * (i + 1) + j - 1), costDiagonal);
//
//				// right bottom
//				if (j + 1 < Graph.NUM_CELLS_PER_ROW && i - 1 >= 0)
//					makeConnection(node, nodes.get(Graph.NUM_CELLS_PER_ROW * (i - 1) + j + 1), costDiagonal);
//
//				// right top
//				if (j + 1 < Graph.NUM_CELLS_PER_ROW && i + 1 < Graph.NUM_CELLS_PER_COL)
//					makeConnection(node, nodes.get(Graph.NUM_CELLS_PER_ROW * (i + 1) + j + 1), costDiagonal);
//
//			}

		return new Graph(nodes);
	}

//	private static void recSetLowerObstacleColumn(Node node, Array<Node> nodes, int count) {
//		if (count == 2)
//			return;
//		if (node.containsObstacle() == true) {
//			Node lower = Graph.getLowerNeighbor(node, nodes);
//			if (lower.containsObstacle() == false) {
//				Node lowerLower = Graph.getLowerNeighbor(lower, nodes);
//
//				if (lowerLower.containsObstacle() == false)
//
//					// consider right and left as well to ensure path finding
//					// opportunities
//					if (Graph.getRightNeighbor(lower, nodes).containsObstacle() == false
//							&& Graph.getLeftNeighbor(lower, nodes).containsObstacle() == false) {
//						lower.setContainsObstacle(true);
//						count++;
//						recSetLowerObstacleColumn(lower, nodes, count);
//					}
//			}
//		}
//	}

	private static void makeConnection(Node fromNode, Node toNode, float cost) {
		if (toNode.containsObstacle() == false)
			fromNode.addConnection(toNode, cost);
	}
}
