package com.elementar.logic.map.pathfinding;

import com.badlogic.gdx.math.Vector2;

public class LightCluster implements Comparable<LightCluster> {

	private Node center_node;

	/**
	 * describes how good the cluster fits into its environment
	 */
	private int hori_quality, vert_quality, quality;

	/**
	 * describes (minimal extension length in vertical and horizontal
	 * direction+3(cluster-dimensions))*Cell_length.
	 */
	private float distance;

	/**
	 * 
	 * @param centerNode
	 * @param vertQuality
	 * @param horiQuality
	 */
	public LightCluster(Node centerNode, int vertQuality, int horiQuality) {
		this.center_node = centerNode;
		this.vert_quality = vertQuality;
		this.hori_quality = horiQuality;
		this.quality = vertQuality + horiQuality;
		this.distance = (Math.min(vertQuality, horiQuality) + 1) * Graph.CELL_SIDE_LENGTH;
	}

	public Vector2 getPos() {
		return center_node.getPos();
	}

	public float getDistance() {
		return distance;
	}

	@Override
	public int compareTo(LightCluster lightCluster) {
		if (quality < lightCluster.quality)
			return 1;
		if (quality == lightCluster.quality)
			return 0;
		return -1;
	}

	@Override
	public String toString() {
		return "centerNode = " + center_node.getPos() + ", h-qual = " + hori_quality + ", v-qual = " + vert_quality
				+ ", quality = " + quality + ", distance = " + distance;
	}

	public void setX(float x) {
		this.center_node.getPos().x = x;
	}

}
