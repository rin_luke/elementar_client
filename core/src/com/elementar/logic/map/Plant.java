package com.elementar.logic.map;

import static com.elementar.logic.util.Shared.PPM;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.elementar.gui.util.VegetationSprite;
import com.elementar.logic.characters.abstracts.APhysicalElement;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.Shared.CategorySize;
import com.elementar.logic.util.userdata.PlantContacts;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class Plant extends APhysicalElement {

	protected Fixture sensor;
	protected CategorySize size;

	protected PlantContacts contacts;

	public Plant(World world, VegetationSprite sprite) {
		super(world);

		this.size = sprite.getSize();
		initPhysics(sprite.getX(), sprite.getY());

		contacts = new PlantContacts(sprite);

		sensor.setUserData(contacts);

		// override position to set world angle (third parameter)
		body.setTransform(sprite.getX(), sprite.getY(),
				MathUtils.degreesToRadians * sprite.getRotation());
	}

	@Override
	protected void initPhysics(float x, float y) {
		body_def.type = BodyType.StaticBody;
		body_def.allowSleep = true;
		body_def.fixedRotation = true;
		body_def.position.set(x, y);
		body = world.createBody(body_def);

		PolygonShape polygonShape = new PolygonShape();

		// define sensor fixture
		fixture_def.isSensor = true;
		fixture_def.shape = polygonShape;

		float offset = 5f, halfHeight = 5f / PPM;
		Vector2 centerVector = new Vector2();
		centerVector.x = 15f / PPM;
		switch (size) {
		case MEDIUM:
			halfHeight = (17 + offset) / PPM;
			centerVector.y = 20 / PPM;
			break;
		case SMALL:
			halfHeight = (7 + offset) / PPM;
			centerVector.y = 10 / PPM;
			break;

		}
		polygonShape.setAsBox(17 / PPM, halfHeight, centerVector, 0);
		sensor = body.createFixture(fixture_def);

		sensor.setFilterData(getCollisionFilter());

		polygonShape.dispose();
	}

	public void decrementAnimationTime(float delta) {
		contacts.decrementAnimationTime(delta);
	}

	@Override
	protected Filter getCollisionFilter() {
		Filter collisionFilter = new Filter();
		collisionFilter.categoryBits = CollisionData.BIT_PLANT;
		collisionFilter.maskBits = CollisionData.BIT_CHARACTER_PLANT;
		return collisionFilter;
	}

}
