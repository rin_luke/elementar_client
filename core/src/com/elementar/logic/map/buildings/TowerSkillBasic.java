package com.elementar.logic.map.buildings;

import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.IEmitter;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;
import com.elementar.logic.util.Shared.Team;

public class TowerSkillBasic extends ABuildingSkill {

	private AEmissionDef e1;

	public TowerSkillBasic(Tower tower, String unparsedSkinName, Team teamTarget) {
		super(tower, unparsedSkinName, teamTarget, 1.5f// cooldown
		);
	}

	@Override
	protected void defineEmissions() {

		String e1Path, e2Path;

		if (team_user == Team.TEAM_1) {
			e1Path = "graphic/particles/particle_building/building_tower_projectile_e1_team1.p";
			e2Path = "graphic/particles/particle_building/building_tower_projectile_e2_team1.p";
		} else {
			e1Path = "graphic/particles/particle_building/building_tower_projectile_e1_team2.p";
			e2Path = "graphic/particles/particle_building/building_tower_projectile_e2_team2.p";
		}

		// eMagical
		DefProjectile defProjectile = new DefProjectile(600,
				getCollisionFilter(CollisionData.BIT_CHARACTER_PROJECTILE));

		AEmissionDef e2 = new EmissionDefAngleDirected(e2Path, 4f, 4f, false, defProjectile, 1, 0,
				new ObstacleAlignment()).addEffect(new EffectDamage(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue() + CollisionKinds.AFTER_LIFETIME.getValue()));

		// e1, main projectile
		DefProjectile prototype = new DefProjectile(10000, getCollisionFilter(CollisionData.BIT_CHARACTER_PROJECTILE))
				.setVelocity(2f).addSuccessor(e2);

		e1 = new EmissionDefAngleDirected(e1Path, 2f, 2f, true, prototype, 1, 0, new FixedAlignment(90));

	}

	public void setPersecutee(IEmitter persecutee) {
		e1.getDefProjectile().setPersecutee(persecutee, e1, building.getSparseObject().pos);
	}

	@Override
	protected Emission create() {
		e1.setCenter(building.getSparseObject().pos);

		// PathFinder.getInstance().searchNodePath(
		// Graph.getNodeByPos(e1.getCenterPos()),
		// Graph.getNodeByPos(e1.getDefProjectile().getPersecutedFixture()
		// .getBody().getPosition()));

		return new Emission(e1, building);
	}
}
