package com.elementar.logic.map.buildings;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.elementar.data.container.util.CustomParticleEffect;
import com.elementar.gui.util.CustomSkeleton;
import com.elementar.gui.util.DamageDiffbar;
import com.elementar.gui.util.HasDamageDifference;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.characters.skills.ComboStorage;
import com.elementar.logic.emission.EmissionList;
import com.elementar.logic.emission.IEmitter;
import com.elementar.logic.emission.SparseProjectile;
import com.elementar.logic.emission.Trigger;
import com.elementar.logic.emission.def.IBonesModel;
import com.elementar.logic.emission.effect.AEffectTimed;
import com.elementar.logic.map.AMapElement;
import com.elementar.logic.util.SparseObject;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.Shared.Team;
import com.esotericsoftware.spine.Bone;

/**
 * Contains a skeleton. The method {@link #initPhysics(float, float)} is
 * {@link Deprecated}. Use instead {@link #init()}. Unlike {@link AMapElement}
 * you are responsible to create hitboxes. For that purpose call
 * {@link #createHitboxes()} separately.
 * 
 * @author lukassongajlo
 */
public abstract class ABuilding extends AMapElement implements IBonesModel, IEmitter, HasDamageDifference {

	/**
	 * we misuse viewcircle to check whether a {@link Tower} or a {@link Shrine}
	 * collides with an obstacle at the top
	 */
	private Vector2[] collision_rectangle;

	/**
	 * Holds an unique building id, will be sent over network
	 */
	protected SparseObject sparse_object;

	protected CustomSkeleton skeleton;

	protected Team team = Team.BOTH;

	protected Vector2 center_pos;

	protected short health_current, health_absolute;

	protected Location location;

	/**
	 * Holds triggers created on server side to create emissions at client side
	 */
	private LinkedList<Trigger> triggers = new LinkedList<Trigger>();

	/**
	 * Holds all active projectiles. {@link BuildingProjectiles} consist just a list
	 * of {@link SparseProjectile} and an id. So it is transmissible
	 */
	private BuildingProjectiles building_projectiles;

	private LinkedList<SparseProjectile> prev_sparse_projectiles = new LinkedList<>();

	private LinkedList<SparseProjectile> drawable_sparse_projectiles = new LinkedList<>();

	private EmissionList emission_list;

	protected ABuildingSkill skill_dying;
	private boolean prev_alive = true;

	/**
	 * Stores the health value of the last {@link #healthChanged()} call
	 */
	private short last_health_network;

	private short last_health_for_damage_diff;

	protected float rotation;

	/**
	 * Holds all rotation bones to simulate death explosion animation
	 */
	protected ArrayList<Bone> bones_for_death;

	protected CustomSkeleton skeleton_death;

	protected boolean attackable = false;

	// "serverside" because no sound should be played
	protected ComboStorage combo_container = new ComboStorage(0);

	private ArrayList<AEffectTimed> effects;

	private DamageDiffbar damage_diff_bar = new DamageDiffbar();

	private IEmitter slayer;

	/**
	 * You are responsible to create the hitboxes with {@link #createHitboxes()}
	 * 
	 * @param world
	 * @param element
	 */
	public ABuilding(World world, ABuilding element) {
		super(world, element);
		this.skeleton = element.skeleton;
	}

	/**
	 * start point, local vertices and global vertices can be defined in
	 * {@link #init()}
	 * 
	 * @param world
	 * @param skeleton
	 * @param rotation
	 */
	public ABuilding(World world, Location location, CustomSkeleton skeleton, Vector2 startPoint, float rotation) {
		super(world, null, null, null);
		this.location = location;
		this.skeleton = skeleton;
		this.rotation = rotation;
		start_point = startPoint;
		if (startPoint != null) {
			skeleton.setPosition(startPoint.x, startPoint.y);
			skeleton.updateWorldTransform();
		}

		// guaranteed that this field isn't null (for shrine). Has to be re-set
		sparse_object = new SparseObject();

		if (location != Location.SERVERSIDE)
			determineAnimations();
		init();
		makeCollisionRectangle();

		health_current = health_absolute = (short) initHealth();

		// id will set separately
		building_projectiles = new BuildingProjectiles(new LinkedList<SparseProjectile>());

		emission_list = new EmissionList(this);

		effects = new ArrayList<>();
	}

	private void makeCollisionRectangle() {
		Vector2 pos = new Vector2(start_point);
		// shift by 1.4 meters
		pos.y += 1.4f;

		float halfSideLength = 0.4f;
		// rectangle values by try and err
		collision_rectangle = new Vector2[] {
				// left bot
				Util.addVectors(pos, new Vector2(-halfSideLength, -halfSideLength)),
				// left top
				Util.addVectors(pos, new Vector2(-halfSideLength, halfSideLength)),
				// right top
				Util.addVectors(pos, new Vector2(halfSideLength, halfSideLength)),
				// right bot
				Util.addVectors(pos, new Vector2(halfSideLength, -halfSideLength)) };
	}

	@Deprecated
	@Override
	protected void initPhysics(float x, float y) {
	}

	protected abstract void init();

	protected abstract int initHealth();

	/**
	 * Let it empty when element hasn't a hitbox like {@link Shrine} for example
	 */
	public abstract void createHitboxes();

	public abstract void deactivateCollisions();

	/**
	 * Set your animations that you have declared as members in your subclass.
	 */
	protected abstract void determineAnimations();

	/**
	 * Updates emissions, dying skill, projectiles
	 */
	public void update() {

		LogicTier.updateProjectiles(building_projectiles.sparse_projectiles, prev_sparse_projectiles);

		// update effects caused by emissions
		Iterator<AEffectTimed> it = effects.iterator();
		while (it.hasNext() == true)
			it.next().update(it);

		emission_list.update();

		if (skill_dying != null) {
			skill_dying.update();
			if (isAlive() == false && prev_alive == true) {
				prev_alive = false;

				if (location != Location.CLIENTSIDE_MULTIPLAYER) {
					skill_dying.init();
				}
			}
		}

	}

	/**
	 * Returns the target id
	 * 
	 * @return
	 */
	public short getID() {
		return sparse_object.connection_id;
	}

	public void setID(int id) {
		building_projectiles.id = (short) id;
		sparse_object.connection_id = (short) id;
	}

	public SparseObject getSparseObject() {
		return sparse_object;
	}

	@Override
	public boolean isAlive() {
		return health_current > 0;
	}

	/**
	 * Returns an array of four vertices, surrounded a critical space of the
	 * building where no base or island should be. Otherwise the building's position
	 * is invalid.
	 * 
	 * @return
	 */
	public Vector2[] getCollisionRectangle() {
		return collision_rectangle;
	}

	public Vector2 getCenterPos() {
		return center_pos;
	}

	public short getHealthCurrent() {
		return health_current;
	}

	public short getHealthAbsolute() {
		return health_absolute;
	}

	public boolean healthChanged() {
		return last_health_network != health_current;
	}

	@Override
	public void setSlayer(IEmitter slayer) {
		this.slayer = slayer;
	}

	@Override
	public IEmitter getSlayer() {
		return slayer;
	}

	/**
	 * Sets the {@link #last_health_network} field to the current health, so the
	 * method {@link #healthChanged()} will reset
	 */
	public void updateLastHealth() {
		last_health_network = health_current;
	}

	public void setHealthCurrent(short health) {
		health_current = health;
	}

	@Override
	public Team getTeam() {
		return team;
	}

	@Override
	public LinkedList<Trigger> getTriggers() {
		return triggers;
	}

	@Override
	public World getWorld() {
		return world;
	}

	@Override
	public Location getLocation() {
		return location;
	}

	@Override
	public Vector2 getPos() {
		return sparse_object.pos;
	}

	@Deprecated
	@Override
	public Vector2 getCursorPos() {
		// just relevant for characters
		return new Vector2();
	}

	@Override
	public LinkedList<SparseProjectile> getSparseProjectiles() {
		return building_projectiles.sparse_projectiles;
	}

	@Override
	public EmissionList getEmissionList() {
		return emission_list;
	}

	@Override
	public void addEffect(AEffectTimed effect) {
		effects.add(effect);
	}

	public LinkedList<SparseProjectile> getPrevSparseProjectiles() {
		return prev_sparse_projectiles;
	}

	public LinkedList<SparseProjectile> getDrawableSparseProjectiles() {
		return drawable_sparse_projectiles;
	}

	public BuildingProjectiles getBuildingProjectiles() {
		return building_projectiles;
	}

	public float getRotation() {
		return rotation;
	}

	public boolean isAttackable() {
		return attackable;
	}

	public void setAttackable(boolean attackable) {
		this.attackable = attackable;
	}

	@Override
	public ComboStorage getComboStorage1() {
		return combo_container;
	}

	@Override
	public DamageDiffbar getDamageDiffBar() {
		return damage_diff_bar;
	}

	@Override
	public short getLastHealth() {
		return last_health_for_damage_diff;
	}

	@Override
	public void setLastHealth(short lastHealth) {
		this.last_health_for_damage_diff = lastHealth;

	}

	@Override
	public void increaseCrystals() {
		// no chrystals for buildings
	}

	@Override
	public CustomSkeleton getSkeleton() {
		return skeleton;
	}

	@Override
	public boolean isCombo1Stored() {
		return false;
	}

	@Override
	public boolean isCombo2Stored() {
		return false;
	}

	@Override
	public CustomParticleEffect getParticleEffectBySlot(String slot) {
		return null;
	}

	@Override
	public CustomParticleEffect getComboHandFrontParticleEffect() {
		return null;
	}

	@Override
	public CustomParticleEffect getComboHandBackParticleEffect() {
		return null;
	}

	@Override
	public ComboStorage getComboStorage2() {
		return null;
	}

	@Override
	public ComboStorage getComboStorageTransfer() {
		return null;
	}

}
