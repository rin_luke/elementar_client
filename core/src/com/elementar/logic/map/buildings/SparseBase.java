package com.elementar.logic.map.buildings;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.util.SparseObject;

public class SparseBase extends SparseObject {

	public SparseBase() {
		super();
	}

	/**
	 * 
	 * @param pos
	 */
	public SparseBase(Vector2 pos) {
		// will be set separately
		super((short) 0, pos, new Vector2());
	}

}
