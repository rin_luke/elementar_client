package com.elementar.logic.map.buildings;

import java.util.LinkedList;

import com.elementar.logic.emission.SparseProjectile;

/**
 * Help class to send the {@link SparseProjectile}s of a building over the
 * network. Beside the projectiles it holds an id to find the corresponding
 * building.
 * 
 * @author lukassongajlo
 * 
 */
public class BuildingProjectiles {

	public short id;
	public LinkedList<SparseProjectile> sparse_projectiles;

	public BuildingProjectiles() {
	}

	public BuildingProjectiles(LinkedList<SparseProjectile> projectiles) {
		sparse_projectiles = projectiles;
	}
}
