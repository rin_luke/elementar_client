package com.elementar.logic.map.buildings;

import com.badlogic.gdx.physics.box2d.Filter;
import com.elementar.logic.characters.skills.ASkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefDeathExplosionBuilding;
import com.elementar.logic.emission.effect.EffectDying;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.Util;

public class TowerDyingSkill extends ABuildingSkill {

	private AEmissionDef def_death2, def_death1;

	public TowerDyingSkill(Tower tower, String unparsedSkinName) {
		super(tower, unparsedSkinName, null, 0);
	}

	@Override
	protected void defineEmissions() {
		if (team_user != null) {
			// parse x out of "team_x", which can be 1 or 2
			float multiplier = 1.5f;

			Filter collisionFilter = getCollisionFilter(CollisionData.BIT_GROUND);

			DefProjectile DefProjectile2 = new DefProjectile(10000, collisionFilter).setVelocity(1.2f)
					.setVelocityVariation(0.8f).setGravityScale(0.9f).setPolygonShape(3).setFriction(1f);

			def_death2 = new EmissionDefDeathExplosionBuilding(
					"graphic/particles/particle_building/building_tower_death2_team" + Util.getTeamNumber(team_user)
							+ ".p",
					multiplier, multiplier, true, DefProjectile2, 6, 0).addEffect(new EffectDying());

			DefProjectile defProjectile1 = new DefProjectile(3000, ASkill.getGroundFilter());

			// death explosion
			def_death1 = new EmissionDefAngleDirected("graphic/particles/particle_building/building_tower_death1_team"
					+ Util.getTeamNumber(team_user) + ".p", multiplier, multiplier, true, defProjectile1, 1, 0,
					new FixedAlignment(0)).addSuccessorTimewise(0, def_death2);
		}
	}

	@Override
	protected Emission create() {

		/*
		 * tower muss hier gesetzt werden, da für den effect keine kollision vorher
		 * stattfand
		 */
		def_death2.setTarget(building);
		def_death2.setCenter(building.getCenterPos());
		def_death2.setBonesModel(building);
		/*
		 * set RTD (run time data), for explanation look at dropbox -> emission models
		 */
		def_death1.setCenter(building.getCenterPos());
		return new Emission(def_death1, building);
	}
}
