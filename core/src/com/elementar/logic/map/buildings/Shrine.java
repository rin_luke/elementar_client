package com.elementar.logic.map.buildings;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.elementar.data.container.AnimationContainer;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.Bone;

/**
 * Has a little triangular hitbox near the head to ensure that the shrine
 * doesn't collide with any island
 * 
 * @author lukassongajlo
 * 
 */
public class Shrine extends ABuilding {

	private ArrayList<Bone> exploding_bones = new ArrayList<>();

	private Animation animation;

	public Shrine(World world, Location location, Vector2 startpoint) {
		super(world, location, AnimationContainer.makeShrineAnimation(), startpoint, 0);

	}

	@Override
	protected int initHealth() {
		return 0;
	}

	@Override
	protected void init() {
		skeleton.updateWorldTransform();
	}

	@Override
	public void createHitboxes() {
		// no hitboxes
	}

	@Override
	protected void determineAnimations() {
		animation = skeleton.getData().findAnimation("shrine_idle");
	}

	public Animation getAnimation() {
		return animation;
	}

	@Override
	public short getID() {
		return 0; // no sparse object
	}

	@Override
	public void fillDeathBonesArray() {
		// empty

	}

	@Override
	public ArrayList<Bone> getDeathBones() {
		return exploding_bones;
	}

	@Override
	public boolean isFlip() {
		return false;
	}

	@Override
	public void deactivateCollisions() {
		// no collisions anyway
	}

}
