package com.elementar.logic.map.buildings;

import com.badlogic.gdx.physics.box2d.Filter;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.skills.ASkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefDeathExplosionBuilding;
import com.elementar.logic.emission.effect.EffectDying;
import com.elementar.logic.util.CollisionData;

public class BaseDyingSkill extends ABuildingSkill {

	public static final float TWEENING_TRANSITION_TIME = 0.8f;

	private AEmissionDef def_death2, def_death1;

	public BaseDyingSkill(Base base, String unparsedSkinName) {
		super(base, unparsedSkinName, null, 0);
	}

	@Override
	protected void defineEmissions() {
		if (team_user != null) {
			// parse x out of "team_x", which can be 1 or 2

			Filter collisionFilter = getCollisionFilter(CollisionData.BIT_GROUND);

			DefProjectile defProjectile2 = new DefProjectile(10000, collisionFilter).setVelocity(2f)
					.setVelocityVariation(0.7f).setGravityScale(0.5f).setPolygonShape(3).setFriction(1f);
			def_death2 = new EmissionDefDeathExplosionBuilding(
					"graphic/particles/particle_building/building_core_death2_team"
							+ team_user.name().substring(team_user.name().length() - 1) + ".p",
					3f, 3f, true, defProjectile2, 60, 0).addEffect(new EffectDying());

			DefProjectile defProjectile1 = new DefProjectile(10000, ASkill.getGroundFilter());

			// death explosion
			def_death1 = new EmissionDefAngleDirected(
					"graphic/particles/particle_building/building_core_death1_team"
							+ team_user.name().substring(team_user.name().length() - 1) + ".p",
					4.5f, 4.5f, true, defProjectile1, 1, 0, new FixedAlignment(0)).addSuccessorTimewise(0, def_death2);
		}

	}

	@Override
	protected Emission create() {
		def_death2.setCenter(building.getCenterPos());
		def_death2.setTarget(building);
		def_death2.setBonesModel(building);

		def_death1.setCenter(building.getCenterPos());
		return new Emission(def_death1, building);
	}

}
