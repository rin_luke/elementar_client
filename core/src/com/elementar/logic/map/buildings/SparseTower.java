package com.elementar.logic.map.buildings;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.util.SparseObject;

public class SparseTower extends SparseObject {

	public SparseTower() {
	}

	public SparseTower(Vector2 pos) {
		// id will set separately
		super((short) 0, pos, new Vector2());
	}

}
