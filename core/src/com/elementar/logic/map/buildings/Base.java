package com.elementar.logic.map.buildings;

import static com.elementar.logic.util.Shared.MAP_WIDTH;

import java.util.ArrayList;
import java.util.Arrays;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.ParticleContainer;
import com.elementar.data.container.util.CustomParticleEffect;
import com.elementar.gui.util.CustomSkeleton;
import com.elementar.gui.util.ShaderPolyBatch;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.map.generator.AVerticesGenerator;
import com.elementar.logic.map.util.BoundingBoxData;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.ViewCircle;
import com.elementar.logic.util.Shared.Team;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.Bone;
import com.esotericsoftware.spine.Slot;
import com.esotericsoftware.spine.attachments.Attachment;
import com.esotericsoftware.spine.attachments.BoundingBoxAttachment;

import box2dLight.PointLight;

/**
 * 
 * @author lukassongajlo
 * 
 */
public class Base extends ABuilding {

	// private Bone
	// inner_circle1,inner_circle1,inner_circle1,inner_circle1,inner_circle1,
	/**
	 * This point is also the root point of baseSkeleton
	 */
	public static final Vector2 LEFT_BASE_CENTER = new Vector2(-MAP_WIDTH * .605f, 0),
			RIGHT_BASE_CENTER = new Vector2(LEFT_BASE_CENTER.x * (-1f), 0);
	public static final float Y_SPAWN_SHIFT = 2;

	/**
	 * fog dispel radius spawn
	 */
	private static final float RADIUS_SPAWN = 5;

	private ViewCircle view_circle_spawn;

	private ArrayList<BoundingBoxData> bounding_boxes;

	private Animation animation_core;

	private ArrayList<Vector2> map_border_sequence;
	private float base_height;

	private PointLight[] lights;

	private Vector2 pos_spawn, pos_spawn_protection;

	private CustomParticleEffect effect_spawn, effect_spawn_protection;

	/**
	 * Copy constructor
	 * 
	 * @param world
	 * @param base
	 */
	public Base(World world, Base base) {
		super(world, base);

		this.health_current = base.health_current;
		this.health_absolute = base.health_absolute;

		this.bounding_boxes = base.bounding_boxes;
		this.animation_core = base.animation_core;
		this.team = base.team;
		// the copy doesn't use the sparse representation
		// this.sparse_base = base.sparse_base;

		createHitboxes();
	}

	/**
	 * @param world
	 * @param skeleton
	 */
	public Base(World world, Location location, CustomSkeleton skeleton) {
		// start point will be set in #init()
		super(world, location, skeleton, null, 0);
	}

	@Override
	protected int initHealth() {
		return 100;
	}

	@Override
	protected void init() {
		bounding_boxes = new ArrayList<BoundingBoxData>();
		team = skeleton == AnimationContainer.skeleton_base_right ? Team.TEAM_2 : Team.TEAM_1;

		if (team == Team.TEAM_2)
			start_point = RIGHT_BASE_CENTER;
		else
			start_point = LEFT_BASE_CENTER;

		skeleton.setPosition(start_point.x, start_point.y);

		// fill list
		for (Slot slot : skeleton.getSlots()) {
			Attachment attachment = slot.getAttachment();
			if (attachment instanceof BoundingBoxAttachment) {
				BoundingBoxAttachment bb = (BoundingBoxAttachment) attachment;
				if (team == Team.TEAM_1) {
					// here it's ok to add the original attachment, because of
					// no editing.
					bounding_boxes.add(new BoundingBoxData(slot.getBone(), bb));
				} else {
					// flip
					Vector2[] oldVertices = Util.convertFloatArrayIntoVector2Array(bb.getVertices());
					Vector2[] flippedVertices = new Vector2[oldVertices.length];
					for (int i = 0; i < oldVertices.length; i++)
						flippedVertices[i] = AVerticesGenerator.mirrorVertex(oldVertices[i], 0);

					/*
					 * davor hätte ich an dieser stelle einfach die vertices des spine bb
					 * attachments überschrieben.
					 */
					BoundingBoxAttachment flippedBoundingBoxAttachment = new BoundingBoxAttachment(bb.getName());
					flippedBoundingBoxAttachment.setVertices(Util.convertVector2ArrayIntoFloatArray(flippedVertices));
					bounding_boxes.add(new BoundingBoxData(slot.getBone(), flippedBoundingBoxAttachment));
				}
			}
		}

		for (BoundingBoxData bbData : bounding_boxes) {
			BoundingBoxAttachment bb = bbData.getBoundingBoxAttachment();
			if (bb.getName().equals("root_world_building_base_hitbox") == true) {
				// determine locals and globals
				local_vertices = Util.convertFloatArrayIntoVector2Array(bb.getVertices());
				global_vertices = Util.shiftVerticesTo(start_point, local_vertices);
			} else {
				// determine hitboxes
				Vector2[] vertices = Util.convertFloatArrayIntoVector2Array(bb.getVertices());

				/*
				 * shift each vertices list (bounding box) to its bone. Consider that
				 * bone.getX() depends on parent coords-system. Consider that some parent of a
				 * bone might be the root!
				 */
				Bone bone = bbData.getBone();
				vertices = Util.shiftVerticesTo(
						new Vector2(team == Team.TEAM_2 ? -bone.getX() - bone.getParent().getX()
								: bone.getX() + bone.getParent().getX(), bone.getY() + bone.getParent().getY()),
						vertices);

				// switch indices order, so vegetation get placed right.
				if (team == Team.TEAM_1) {
					ArrayList<Vector2> asList = new ArrayList<Vector2>(Arrays.asList(vertices));
					asList = AVerticesGenerator.reIndexation(0, asList, true);
					vertices = asList.toArray(new Vector2[asList.size()]);
				}
				bbData.setLocalHitbox(vertices);
				bbData.setGlobalHitbox(Util.shiftVerticesTo(start_point, vertices));

			}
		}

		skeleton.updateWorldTransform();

		createHitboxes();

		center_pos = new Vector2(start_point);
		pos_spawn = new Vector2(start_point);

		// define position of spawnProtection by bone
		Bone boneProtection = skeleton.findBone("world_building_base_spawnpit");
		pos_spawn_protection = new Vector2(boneProtection.getWorldX(), boneProtection.getWorldY());

		// spawn circle
		view_circle_spawn = new ViewCircle(pos_spawn_protection, RADIUS_SPAWN);

		// skill_dying
		skill_dying = new BaseDyingSkill(this, "");

		// prepare for death case
		fillDeathBonesArray();

	}

	public void setBaseParticleEffects() {
		// create and place particle effects
		effect_spawn = ParticleContainer.loadParticle(
				"graphic/particles/particle_building/building_base_spawn_team" + Util.getTeamNumber(team) + ".p");
		effect_spawn.scaleEffect(Shared.SCALE_BUILDINGS);
		effect_spawn.setPosition(pos_spawn.x, pos_spawn.y);

		effect_spawn_protection = ParticleContainer
				.loadParticle("graphic/particles/particle_building/building_base_spawnprotection_team"
						+ Util.getTeamNumber(team) + ".p");
		effect_spawn_protection.scaleEffect(Shared.SCALE_BUILDINGS);
		effect_spawn_protection.setPosition(pos_spawn_protection.x, pos_spawn_protection.y);

	}

	@Override
	public void createHitboxes() {
		body_def.type = BodyType.StaticBody;
		body_def.allowSleep = false;

		body_def.position.set(start_point);
		body = world.createBody(body_def);

		Fixture f;

		for (BoundingBoxData bb : bounding_boxes)
			if (bb.getLocalHitbox() != null) {

				if (bb.getName().equals("world_building_base_ground1_hitbox") == true) {
					// that's the hitbox which is a part of the mapborder!
					Vector2 maxPoint = new Vector2(0, -1000f), minPoint = new Vector2(0, 1000f);
					int indexMax = 100;
					Vector2 futureBorderVertex;
					for (int i = 0; i < bb.getGlobalHitbox().length; i++) {
						futureBorderVertex = bb.getGlobalHitbox()[i];
						if (futureBorderVertex.y < minPoint.y)
							minPoint = futureBorderVertex;
						if (futureBorderVertex.y > maxPoint.y) {
							maxPoint = futureBorderVertex;
							indexMax = i;
						}
					}
					base_height = maxPoint.y - minPoint.y;

					map_border_sequence = AVerticesGenerator.reIndexation(indexMax,
							Util.convertVector2ArrayIntoVector2ArrayList(bb.getGlobalHitbox()), team == Team.TEAM_2);

					if (team == Team.TEAM_1) {
						map_border_sequence = AVerticesGenerator.reIndexation(map_border_sequence.size() - 1,
								map_border_sequence, true);
					}
				} else {

					ChainShape chainShape = new ChainShape();
					chainShape.createLoop(bb.getLocalHitbox());
					fixture_def.shape = chainShape;
					fixture_def.friction = 0f;
					fixture_def.restitution = 0f;
					fixture_def.isSensor = false;
					f = body.createFixture(fixture_def);
					f.setFilterData(getCollisionFilter());

					sparse_object = new SparseBase(new Vector2(bb.getBone().getWorldX() + start_point.x,
							bb.getBone().getWorldY() + start_point.y));

					chainShape.dispose();
				}
			}

	}

	@Override
	public void fillDeathBonesArray() {
		bones_for_death = new ArrayList<Bone>();
	}

	@Override
	public boolean isFlip() {
		return skeleton.getFlipX();
	}

	@Override
	public ArrayList<Bone> getDeathBones() {
		return bones_for_death;
	}

	public void drawEffects(ShaderPolyBatch batch, float delta) {
		effect_spawn.draw(batch, delta);
		effect_spawn_protection.draw(batch, delta);
	}

	@Override
	protected void determineAnimations() {
		animation_core = skeleton.getData().findAnimation("base_idle");
	}

	/**
	 * Returns the global representation of the bounding box. This method is used
	 * for vegetation placement, for instance.
	 * 
	 * @param name
	 * @return
	 */
	public Vector2[] getBoundingBox(String name) {
		for (BoundingBoxData bb : bounding_boxes)
			if (bb.getName().equals(name) == true)
				return bb.getGlobalHitbox();
		return null;
	}

	public Animation getAnimationCore() {
		return animation_core;
	}

	public ArrayList<Vector2> getMapBorderSequence() {
		return map_border_sequence;
	}

	public float getBaseHeight() {
		return base_height;
	}

	public ViewCircle getViewCircleSpawn() {
		return view_circle_spawn;
	}

	public void setLights(PointLight... lights) {
		this.lights = lights;
	}

	public PointLight[] getLights() {
		return lights;
	}

	@Override
	public void deactivateCollisions() {
	}

	public Vector2 getPosSpawnProtection() {
		return pos_spawn_protection;
	}

}
