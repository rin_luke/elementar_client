package com.elementar.logic.map.buildings;

import java.util.ArrayList;
import java.util.Collections;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.util.CustomParticleEffect;
import com.elementar.data.container.util.ParticleEffect;
import com.elementar.gui.util.CustomSkeleton;
import com.elementar.gui.util.DamageDiffbar;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.OmniClient;
import com.elementar.logic.characters.ServerPhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.creeps.OmniCreep;
import com.elementar.logic.emission.IEmitter;
import com.elementar.logic.map.util.BoundingBoxData;
import com.elementar.logic.network.gameserver.GameserverLogic;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CreepAndTowerCastCallback;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.ViewCircle;
import com.elementar.logic.util.Shared.Team;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.Bone;
import com.esotericsoftware.spine.Slot;
import com.esotericsoftware.spine.attachments.Attachment;
import com.esotericsoftware.spine.attachments.BoundingBoxAttachment;

import box2dLight.PointLight;

public class Tower extends ABuilding {

	/**
	 * fog dispel radius for standard tower
	 */
	private static final float FOG_RADIUS_TOWER = 4;

	private static final float Y_SHIFT_ENERGY_BALL = 1.55f;

	/**
	 * in meter
	 */
	public static final float RADIUS_ATTACK = 5f;

	private Animation animation;
	private ArrayList<BoundingBoxData> bounding_boxes;

	private ViewCircle view_circle, attack_circle;

	private TowerSkillBasic skill_basic;

	private Comparator comparator;

	/**
	 * <code>null</code> if {@link #location} == {@link Location#SERVERSIDE}
	 */
	private CustomParticleEffect particle_effect;

	/**
	 * 
	 */
	private Vector2 ray_beginning_left, ray_beginning_right, ray_beginning_top,
			ray_beginning_bottom;

	private PointLight light;

	private float animation_time;

	private DamageDiffbar healthbar_white_sprite;

	private IEmitter current_target;

	/**
	 * 
	 * @param world
	 * @param startpoint
	 * @param team
	 * @param rotationTower
	 *            , contains the information whether it is a base tower or not
	 */
	public Tower(World world, Location location, Vector2 startpoint, Team team) {
		super(world, location, AnimationContainer.makeTowerAnimation(), startpoint, 0);

		this.team = team;
		Vector2 shift = new Vector2(0, Y_SHIFT_ENERGY_BALL);
		sparse_object = new SparseTower(new Vector2(start_point.x, start_point.y).add(shift));

		attackable = true;

		ray_beginning_top = new Vector2(start_point);
		shift.set(0, 1.9f);
		ray_beginning_top.add(shift);

		ray_beginning_bottom = new Vector2(start_point);
		shift.set(0, 0.8f);
		ray_beginning_bottom.add(shift);

		ray_beginning_right = new Vector2(start_point);
		shift.set(0.5f, 1.4f);
		ray_beginning_right.add(shift);

		ray_beginning_left = new Vector2(start_point);
		shift.set(-0.5f, 1.4f);
		ray_beginning_left.add(shift);

		/*
		 * center pos is relevant for pathfinding and death explosion
		 */
		center_pos = new Vector2(start_point);
		shift.set(0f, 0.7f);
		center_pos.add(shift);

		comparator = new Comparator();
		createHitboxes();

		Vector2 circlePoint = new Vector2(0, 1.7f);
		circlePoint.add(startpoint);

		view_circle = new ViewCircle(circlePoint, FOG_RADIUS_TOWER);
		attack_circle = new ViewCircle(circlePoint, RADIUS_ATTACK);

		skeleton_death = AnimationContainer.makeTowerAnimation();

		if (start_point != null)
			skeleton_death.setPosition(start_point.x, start_point.y);

		// prepare for death case
		fillDeathBonesArray();

		healthbar_white_sprite = new DamageDiffbar();

		skill_basic
				= new TowerSkillBasic(this, "", team == Team.TEAM_1 ? Team.TEAM_2 : Team.TEAM_1);

		skill_dying = new TowerDyingSkill(this, "");

	}

	@Override
	protected int initHealth() {
		return 100;
	}

	@Override
	public void fillDeathBonesArray() {
		bones_for_death = new ArrayList<Bone>();
		int numPieces = 27;

		for (int i = 1; i < numPieces; i++)
			bones_for_death.add(skeleton_death.findBone("tower_piece" + i));
	}

	@Override
	public boolean isFlip() {
		// doesn't matter because tower hasn't any view direction
		return skeleton_death.getFlipX();
	}

	@Override
	protected void init() {

		skeleton.getRootBone().setRotation(rotation);

		bounding_boxes = new ArrayList<BoundingBoxData>();
		for (Slot slot : skeleton.getSlots()) {
			Attachment attachment = slot.getAttachment();
			if (attachment instanceof BoundingBoxAttachment)
				bounding_boxes.add(
						new BoundingBoxData(slot.getBone(), (BoundingBoxAttachment) attachment));

		}

		skeleton.updateWorldTransform();
	}

	public void incAnimationTime(float delta) {
		animation_time += delta;
	}

	public float getAnimationTime() {
		return animation_time;
	}

	@Override
	public void createHitboxes() {

		// team is known here, so set skin. Default is team1
		if (team == Team.TEAM_2)
			skeleton.setSkin("world_tower_team2");

		body_def.type = BodyType.StaticBody;
		body_def.allowSleep = false;

		body_def.position.set(start_point);
		body = world.createBody(body_def);

		for (BoundingBoxData bbData : bounding_boxes) {
			BoundingBoxAttachment bb = bbData.getBoundingBoxAttachment();
			Vector2[] tempVertices = Util.convertFloatArrayIntoVector2Array(bb.getVertices());
			if (MathUtils.isZero(rotation) == false) {
				for (Vector2 vec : tempVertices)
					vec.rotate(rotation);
			}
			ChainShape chainShape = new ChainShape();
			chainShape.createLoop(tempVertices);

			global_vertices = Util.shiftVerticesTo(start_point, tempVertices);

			fixture_def.shape = chainShape;
			fixture_def.friction = 0f;
			fixture_def.restitution = 0f;

			Fixture f = body.createFixture(fixture_def);
			// use overridden getCollisionFilter method
			f.setUserData(this);
			f.setFilterData(getCollisionFilter());
			chainShape.dispose();
		}

	}

	@Override
	public void update() {
		// collect all players that are inside RADIUS_ATTACK?

		if (isAlive() == true) {
			// is the cooldown of the last skill over?
			if (skill_basic.cooldown_current <= 0) {

				// find new targets
				ArrayList<IEmitter> potentialTargets = new ArrayList<>();

				if (location == Location.SERVERSIDE)
					for (ServerPhysicalClient client : GameserverLogic.PLAYER_MAP.values()) {
						if (Util.circleContains(attack_circle,
								client.getHitbox().getPosition()) == true
								&& team != client.getMetaClient().team)
							potentialTargets.add(client);
					}

				else if (location == Location.CLIENTSIDE_TRAINING) {
					for (OmniClient client : LogicTier.WORLD_PLAYER_MAP.values()) {
						if (Util.circleContains(attack_circle,
								client.getPhysicalClient().getHitbox().getPosition()) == true
								&& team != client.getPhysicalClient().getMetaClient().team)
							potentialTargets.add(client.getPhysicalClient());
					}
					for (OmniCreep creep : LogicTier.CREEP_LIVING_LIST) {
						if (Util.circleContains(attack_circle,
								creep.getPhysicalCreep().getHitbox().getPosition()) == true)
							potentialTargets.add(creep.getPhysicalCreep());

					}
				}

				Collections.sort(potentialTargets, comparator);

				// first, test whether current target is still in range
				if (current_target != null) {
					if (potentialTargets.contains(current_target) == true) {
						if (castRay(current_target, ray_beginning_bottom) == false)
							if (castRay(current_target, ray_beginning_top) == false)
								if (castRay(current_target, ray_beginning_left) == false)
									if (castRay(current_target, ray_beginning_right) == false)
										current_target = null;
					} else
						current_target = null;
				}

				if (current_target == null)
					for (IEmitter iEmitter : potentialTargets) {
						/*
						 * we use 4 potential rays to sample the client. So the
						 * client should be unable to attack the tower without
						 * getting damage.
						 */
						if (castRay(iEmitter, ray_beginning_bottom) == false)
							if (castRay(iEmitter, ray_beginning_top) == false)
								if (castRay(iEmitter, ray_beginning_left) == false)
									castRay(iEmitter, ray_beginning_right);
					}
			}
		}

		skill_basic.update();

		super.update();

		skeleton.updateWorldTransform();
	}

	/**
	 * Returns <code>false</code> if the casted ray don't find a player- or
	 * creep-fixture
	 * 
	 * @param targetEmitter
	 * @param originPos
	 * @return
	 */
	private boolean castRay(IEmitter targetEmitter, Vector2 originPos) {

		if (targetEmitter.getTeam() != team) {
			if (Util.isNan(targetEmitter.getPos()) == false)
				if (originPos.dst2(targetEmitter.getPos()) < 0.01f) {
					current_target = targetEmitter;
					skill_basic.setPersecutee(targetEmitter);
					skill_basic.init();
				} else {
					CreepAndTowerCastCallback callback = new CreepAndTowerCastCallback(team);

					world.rayCast(callback, originPos, targetEmitter.getPos());

					if (callback.getFixtureTarget() != null
							&& targetEmitter == callback.getFixtureTarget().getUserData()) {
						current_target = targetEmitter;
						skill_basic.setPersecutee(targetEmitter);
						skill_basic.init();
						return true;
					}
				}
		}
		return false;
	}

	@Override
	public ArrayList<Bone> getDeathBones() {
		return bones_for_death;
	}

	@Override
	protected void determineAnimations() {
		animation = skeleton.getData().findAnimation("tower_idle");
	}

	/**
	 * Sets animation and resets the {@link #animation_time} to 0
	 * 
	 * @param name
	 * @return duration
	 */
	public float setAnimation(String name) {
		animation_time = 0;
		animation = skeleton.getData().findAnimation(name);
		return animation.getDuration();
	}

	public void setParticleEffect(CustomParticleEffect effect) {
		particle_effect = effect;
	}

	/**
	 * Returns a filter for getting damage
	 */
	@Override
	protected Filter getCollisionFilter() {
		Filter collisionFilter = new Filter();
		collisionFilter.categoryBits = CollisionData.BIT_BUILDING;
		collisionFilter.maskBits = CollisionData.BIT_PROJECTILE;
		return collisionFilter;
	}

	public Animation getAnimation() {
		return animation;
	}

	public ViewCircle getViewCircle() {
		return view_circle;
	}

	public ViewCircle getAttackCirle() {
		return attack_circle;
	}

	@Override
	public CustomSkeleton getSkeleton() {
		if (isAlive() == false)
			return skeleton_death;
		return super.getSkeleton();
	}

	public DamageDiffbar getHealthbarWhiteSprite() {
		return healthbar_white_sprite;
	}

	public ParticleEffect getParticleEffect() {
		return particle_effect;
	}

	public void setLight(PointLight light) {
		this.light = light;
	}

	public PointLight getLight() {
		return light;
	}

	@Override
	public void deactivateCollisions() {
		body.setActive(false);
	}

	public Vector2 getRayBeginningTop() {
		return ray_beginning_top;
	}

	public Vector2 getRayBeginningBot() {
		return ray_beginning_bottom;
	}

	public Vector2 getRayBeginningRight() {
		return ray_beginning_right;
	}

	public Vector2 getRayBeginningLeft() {
		return ray_beginning_left;
	}

	private class Comparator implements java.util.Comparator<IEmitter> {

		private Vector2 pos;

		public Comparator() {
			pos = sparse_object.pos;
		}

		@Override
		public int compare(IEmitter o1, IEmitter o2) {
			if (o1.getPos().dst2(pos) < o2.getPos().dst2(pos))
				return -1;
			return 1;
		}
	}

}
