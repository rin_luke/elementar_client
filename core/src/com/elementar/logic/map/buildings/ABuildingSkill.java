package com.elementar.logic.map.buildings;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.World;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.Team;
import com.esotericsoftware.spine.Skeleton;

/**
 * Derived from {@link logic.characters.skills.ASkill}, which is primary
 * designed for characters.
 * 
 * @author lukassongajlo
 * 
 */
public abstract class ABuildingSkill {

	protected ABuilding building;

	/**
	 * unparsed would be 'character_wind_skin0' and parsed just 'skin0'
	 */
	protected String skin_name_parsed;

	/**
	 * Skeleton is necessary to define {@link #animation_duration} in
	 * {@link #setAnimationDuration()}
	 */
	protected Skeleton skeleton;

	// next members are meta data of a skill

	protected float cooldown_current;

	/**
	 * if <code>true</code> the skill has no projectiles, otherwise it's an
	 * active skill which is combinable.
	 */
	protected boolean passive_skill;

	/**
	 * Each skill has an origin value (at the beginning, unchangeable) and an
	 * absolute, changeable value (might changes when the player gets a buff
	 * etc.)
	 * <p>
	 * in seconds.
	 */
	protected float cooldown_modifiable_absolute, cooldown_absolute;

	/**
	 * Holds the team membership info of the skill user
	 */
	protected Team team_user;

	/**
	 * Holds the team membership info of the target
	 */
	protected Team team_target;

	protected World world;

	protected Body body;

	public ABuildingSkill(ABuilding building, String unparsedSkinName, Team teamTarget,
			float cooldown) {
		cooldown_modifiable_absolute = cooldown_absolute = cooldown;
		this.building = building;

		team_target = teamTarget;

		world = building.getWorld();
		team_user = building.getTeam();
		body = building.getBody();

		// parse the unparsed skin name
		// skin_name_parsed = unparsedSkinName != null ? unparsedSkinName
		// .substring(unparsedSkinName.length() - 5,
		// unparsedSkinName.length()) : null;

		defineEmissions();
	}

	/**
	 * Create for each emission def a field.
	 */
	protected abstract void defineEmissions();

	/**
	 * Returns the first emission. If available, set the right center positions
	 * for the defined def-fields
	 * 
	 * 
	 * @return first emission
	 */
	protected abstract Emission create();

	public void init() {
		if (cooldown_current <= 0)
			startEmission();
	}

	/**
	 * Wrapper method for {@link #create()}. This method creates the first
	 * emission. It is called where the emission should start. For channel
	 * skills the emission has to begin instantly, for charge skills after the
	 * cast time has elapsed
	 * 
	 * @return
	 */
	protected Emission startEmission() {
		cooldown_current = cooldown_modifiable_absolute;
		Emission firstEmission = null;
		if (building.getLocation() != Location.CLIENTSIDE_MULTIPLAYER) {
			firstEmission = create();
			building.getEmissionList().add(firstEmission);
		}
		return firstEmission;
	}

	/**
	 * Delta time is not the frame time. As the world, skills got updated not
	 * framewise.
	 */
	public void update() {
		if (cooldown_current > 0)
			// decrease cooldown
			cooldown_current -= Shared.SEND_AND_UPDATE_RATE_WORLD;

	}

	/**
	 * 
	 * @param collisionObjects
	 *            , sum of all valid collision_bits of objects
	 * @return
	 */
	protected Filter getCollisionFilter(int collisionObjects) {
		Filter collisionFilter = new Filter();
		// setting category and mask bits
		collisionFilter.categoryBits = CollisionData.BIT_PROJECTILE;
		collisionFilter.maskBits = (short) collisionObjects;
		return collisionFilter;
	}

}
