package com.elementar.logic.map.generator;

import static com.elementar.logic.util.Shared.WORLD_VIEWPORT_HEIGHT;
import static com.elementar.logic.util.Shared.WORLD_VIEWPORT_WIDTH;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.map.MapManager;
import com.elementar.logic.map.buildings.Base;
import com.elementar.logic.map.util.Vector2IndexPair;
import com.elementar.logic.map.util.Vector4;
import com.elementar.logic.map.util.VerticesCorrespondings;
import com.elementar.logic.util.MyRandom;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;

/**
 * When the local_vertices variable holds more than 1000 vertices it leads to an
 * exception when DEBUG BOX2D-DRAW is active. Consider that while smoothing the
 * mapborder (there shouldnt be too much new generated vertices).
 * 
 * @author lukassongajlo
 * 
 */
public class MapBorderVerticesGenerator extends AVerticesGenerator {

	/**
	 * for smoothing
	 */
	private static final float MAPBORDER_CURVING_FACTOR = 0.2f;
	/**
	 * say how far the sampling algorithm goes into the map for detecting
	 * islands
	 */
	private static final float DIRAC_AMPLITUDE = Shared.MAP_HEIGHT;

	/**
	 * Once an intersection is detected, the ground goes to x percent in
	 * direction of the intersection to determine a new border point.
	 */
	private static final float BORDER_ELEVATION_IN_PERCENT = 0.25f;//23

	private static final float SAMPLE_RATE = 0.8f;

	/**
	 * A 'u'-shaped bound where each line is a sampling axis to detect
	 * surrounding islands. The sampling values build the map border.
	 */
	private Vector4 sampling_bound;

	/**
	 * x = whole width, y = whole height
	 */
	private Vector2 outer_bound;

	private ArrayList<Vector2> left_filling_vertices, left_filling_minimap_vertices, right_filling_vertices,
			right_filling_minimap_vertices;

	private ArrayList<VerticesCorrespondings> left_island_vertices_and_aabb;

	private MergePointComparator merge_comparator;

	private Vector2 sampled_point;

	private float sample_step;
	private ArrayList<VerticesCorrespondings> visual_intersections = new ArrayList<>();

	public ArrayList<VerticesCorrespondings> getVisualIntersections() {
		return visual_intersections;
	}

	public MapBorderVerticesGenerator(MyRandom myRandom) {
		super(myRandom);
		merge_comparator = new MergePointComparator();
	}

	/**
	 * Generates a border by using a sampling technique. This method samples
	 * along the bordersides and detects intersections with nearest islands.
	 * Stores the result in {@link AVerticesGenerator#local_vertices}. Note that
	 * the {@link AVerticesGenerator#start_point} is (0,0) so the
	 * {@link AVerticesGenerator#global_vertices} would be equals to the local
	 * vertices, but here I don't write on globals, so the size = 0
	 * 
	 * @param vec4
	 *            , inner bond, where x = left, y = right, z = top, w = bottom
	 * @param leftIslands
	 *            , vertices + AABB
	 * @param horizontalSegments
	 * @param baseRight
	 *            , to merge border with base
	 * @param baseLeft
	 *            , to merge border with base
	 * 
	 */
	public void generate(Vector4 innerBond, ArrayList<VerticesCorrespondings> leftIslands, Base baseLeft,
			Base baseRight) {
		super.generateRaw();
		this.setStartpoint(new Vector2(0, 0), false); // ...or true, doesn't
														// matter in this case
		sampling_bound = innerBond;
		left_island_vertices_and_aabb = leftIslands;

		ArrayList<Vector2> mirroredVertices;
		left_filling_vertices = new ArrayList<Vector2>();
		right_filling_vertices = new ArrayList<Vector2>();

		// __________________First merge process BEGIN_________

		createOneBorderSide(1);
		createOneBorderSide(2);
		createOneBorderSide(3);

		// extend the ends a little bit to allow merging
		local_vertices.get(0).set(0.5f, local_vertices.get(1).y - 0.05f);
		int localsEndIndex = local_vertices.size() - 1;
		local_vertices.get(localsEndIndex).set(0.5f, local_vertices.get(localsEndIndex - 1).y + 0.05f);
		// mirror and merge
		mirroredVertices = new ArrayList<Vector2>(local_vertices.size());
		for (Vector2 vec : local_vertices)
			mirroredVertices.add(mirrorVertex(vec, 0));
		local_vertices = AVerticesGenerator.reIndexation(0, local_vertices, true);
		merge(true, mirroredVertices);
		// _____________________First merge process DONE_________

		// ELIMINATE LOOPS
		while (AVerticesGenerator.eliminateInternalLoops(local_vertices) == true) {
			// stay in loop
		}
		// EQUALIZE
		FractalVerticesGenerator.equalizeAngle(local_vertices);

		// ____________UNTIL HERE THE RAW VERSION IS GENERATED.

		/*
		 * save horizontal segments for potential shrines or checkpoints EDIT:
		 * map border shouldn't have any towers or shrines
		 */
		// ArrayList<Vector2> copyLocals = new ArrayList<Vector2>();
		// for (Vector2 vec : local_vertices)
		// copyLocals.add(new Vector2(vec));
		// copyLocals = AVerticesGenerator.reIndexation(0, copyLocals, true);
		// horizontalSegments.saveSegments(copyLocals);

		// now the base has to be added to the border
		addBase(baseRight, true);
		addBase(baseLeft, false);

		// SMOOTH
		AVerticesGenerator.smoothCorners(1, local_vertices, MAPBORDER_CURVING_FACTOR, true);
		// REMOVE REDUNDANCE from corners
		synchronized (AVerticesGenerator.corners) {
			for (ArrayList<Vector2> corner : AVerticesGenerator.corners)
				local_vertices
						.removeAll(AVerticesGenerator.getRedundantVertices(REDUNDANCE_THRESHOLD_MAPBORDER, corner));
		}
		// REMOVE REDUNDANCE from vertices with really low differences (=2)
		AVerticesGenerator.eliminateRedundance(2f, local_vertices);

		// FIND MERGEPOINTS with y axis
		ArrayList<Vector2IndexPair> mergePoints = new ArrayList<Vector2IndexPair>();
		Vector2 axisPoint1 = new Vector2(0, sampling_bound.w - WORLD_VIEWPORT_HEIGHT);
		Vector2 axisPoint2 = new Vector2(0, sampling_bound.z + WORLD_VIEWPORT_HEIGHT);
		int index;
		for (int i = 1; i <= local_vertices.size(); i++) {
			Vector2 prevPoint = local_vertices.get(i - 1);
			Vector2 newPoint;
			if (i == local_vertices.size()) {
				index = 0;
				newPoint = local_vertices.get(0);
			} else {
				index = i;
				newPoint = local_vertices.get(i);
			}
			Vector2 mergePoint = new Vector2();
			if (Intersector.intersectSegments(prevPoint, newPoint, axisPoint1, axisPoint2, mergePoint))
				mergePoints.add(new Vector2IndexPair(mergePoint, index));

		}

		// it could be that index+1 lies on local_vertices.size()
		for (Vector2IndexPair pair : mergePoints)
			if (pair.index + 1 == local_vertices.size())
				pair.index = -1;

		// sort mergepoints by y value (descending)
		Collections.sort(mergePoints, new Comparator<Vector2IndexPair>() {
			@Override
			public int compare(Vector2IndexPair pair1, Vector2IndexPair pair2) {
				return pair1.vertex.y > pair2.vertex.y ? -1 : 1;
			}
		});

		/*
		 * 1.left exoskeleton, multiply with factor 2 because of base:
		 */
		createExoSkeletons(left_filling_vertices, right_filling_vertices, mergePoints,
				sampling_bound.y + WORLD_VIEWPORT_WIDTH * 2, sampling_bound.z + WORLD_VIEWPORT_HEIGHT);

		// minimap setup
		float xOffsetMinimap = WORLD_VIEWPORT_WIDTH * 0.6f;// 6;
		float yOffsetMinimap = WORLD_VIEWPORT_HEIGHT * 0.4f;
		left_filling_minimap_vertices = new ArrayList<>();
		right_filling_minimap_vertices = new ArrayList<>();
		createExoSkeletons(left_filling_minimap_vertices, right_filling_minimap_vertices, mergePoints,
				sampling_bound.y + xOffsetMinimap, sampling_bound.z + yOffsetMinimap);

		// used for the minimap
		outer_bound = new Vector2(2 * (sampling_bound.y + xOffsetMinimap), 2 * (sampling_bound.z + yOffsetMinimap));

		// fillings done!

		local_vertices = AVerticesGenerator.reIndexation(local_vertices.size() - 1, local_vertices, true);
	}

	private void createExoSkeletons(ArrayList<Vector2> leftVertices, ArrayList<Vector2> rightVertices,
			ArrayList<Vector2IndexPair> mergePoints, float xOffset, float yOffset) {

		/*
		 * 1.left exoskeleton, multiply with factor 2 for width, because of
		 * base:
		 */
		leftVertices.add(new Vector2(mergePoints.get(1).vertex));
		leftVertices.add(new Vector2(0, -yOffset));
		leftVertices.add(new Vector2(-xOffset, -yOffset));
		leftVertices.add(new Vector2(-xOffset, yOffset * 1f));
		leftVertices.add(new Vector2(0, yOffset * 1f));
		leftVertices.add(new Vector2(mergePoints.get(0).vertex));

		// consider: local_vertices spin = clockwise!
		// merge left exoSkeleton with left side of localVertices
		for (int i = mergePoints.get(0).index;;) {
			if (i == -1)
				i = local_vertices.size() - 1;
			leftVertices.add(local_vertices.get(i));
			i--;
			if (i == mergePoints.get(1).index + 1) {
				leftVertices.add(local_vertices.get(i));
				break;
			}
		}
		// mirror leftExoS.
		for (Vector2 vec : leftVertices)
			rightVertices.add(mirrorVertex(vec, 0));

	}

	private void addBase(Base base, boolean right) {

		/*
		 * find those indices to remove an existing mapborder part and replace
		 * it with base sequence
		 */
		int indexBegin = -1, indexEnd = -1;

		Vector2 mergePoint1 = base.getMapBorderSequence().get(0);
		Vector2 mergePoint2 = base.getMapBorderSequence().get(base.getMapBorderSequence().size() - 1);

		Vector2 topPoint, bottomPoint;

		if (mergePoint1.y > 0) {
			topPoint = mergePoint1;
			bottomPoint = mergePoint2;
		} else {
			topPoint = mergePoint2;
			bottomPoint = mergePoint1;
		}

		Vector2 next;

		/*
		 * border begins at the bottom-mid and goes clockwise. So the first base
		 * reached would the one on the left side
		 */

		// get the last indices that are in correct range
		for (int i = 0; i < local_vertices.size(); i++) {
			next = local_vertices.get(i);
			if (right == true) {
				if (next.x > 0) {
					if (next.x < topPoint.x && next.y > topPoint.y)
						indexBegin = i;
					else if (next.x < bottomPoint.x && next.y < bottomPoint.y)
						indexEnd = i;
				}
			} else {
				if (next.x < 0) {
					if (next.x > bottomPoint.x && next.y < bottomPoint.y)
						indexBegin = i;
					else if (next.x > topPoint.x && next.y > topPoint.y)
						indexEnd = i;
				}
			}

			if (indexBegin != -1 && indexEnd != -1)
				break;

		}

		// remove that part of the existing border where actually the base is
		for (int i = 0; i < indexEnd - indexBegin - 1; i++)
			local_vertices.remove(indexBegin + 1);

		// add base map border sequence to existing border
		local_vertices.addAll(indexBegin + 1, base.getMapBorderSequence());
	}

	/**
	 * 
	 * Creates one border side (1. top: from right to left, 2. from top to
	 * bottom or 3. bottom: from left to right)
	 * 
	 * @param angle
	 *            , 180 = case 1, 270 = case 2, 360 = case 3
	 */
	private void createOneBorderSide(int side) {

		// declare variables
		float bordersideLength = side == 2 ? Math.abs(sampling_bound.w - sampling_bound.z) : Math.abs(sampling_bound.x);

		/*
		 * Note that 3 is the sampling rate. The higher the rougher the
		 * sampling.
		 */
		int numberSteps = (int) (bordersideLength / SAMPLE_RATE);
		sample_step = bordersideLength / numberSteps;

		/*
		 * A list which holds all created points in row that are unmapped.
		 * Depending on side I have to rotate the points. At the end I append
		 * this list to the local_vertices list.
		 */
		ArrayList<VerticesCorrespondings> createdPoints = new ArrayList<VerticesCorrespondings>();

		Vector2 startSamplingPoint = getStartPointSampling(side);
		if (side == 1)
			createdPoints.add(new VerticesCorrespondings(null, startSamplingPoint));

		Vector2 diracPeak = getDiracPeak(side);
		Vector2 sampledPointWithDirac;

		// walking point on sampling bound starting at (0/0)
		Vector2 walkingPoint = new Vector2();
		/*
		 * increase walking point here, because I added above the
		 * startSamplingPoint, so the algorithm can move further on with the
		 * second sample.
		 */
		increaseWalking(side, sample_step, walkingPoint);

		ArrayList<VerticesCorrespondings> intersections = new ArrayList<VerticesCorrespondings>();

		for (int i = 0; i < numberSteps; i++) {
			// sampledPoint = startPoint + walkingPoint
			sampled_point = Util.addVectors(startSamplingPoint, walkingPoint);

			sampledPointWithDirac = Util.addVectors(sampled_point, diracPeak);
			for (VerticesCorrespondings triple : left_island_vertices_and_aabb) {

				// performance boost (check with aabb)
				if (Intersector.intersectSegmentPolygon(sampled_point, sampledPointWithDirac, triple.aabb) == false)
					continue;

				// created 'vertices' for convenience
				Vector2[] vertices = triple.vertices;

				for (int j = 1; j <= vertices.length; j++) {
					Vector2 prevPoint = vertices[j - 1];
					Vector2 newPoint;
					if (j == vertices.length)
						newPoint = vertices[0];
					else
						newPoint = vertices[j];

					Vector2 mergePoint = new Vector2();
					if (Intersector.intersectSegments(sampled_point, sampledPointWithDirac, prevPoint, newPoint,
							mergePoint))
						intersections.add(new VerticesCorrespondings(vertices, mergePoint));

				}

			}
			VerticesCorrespondings createdPoint = null;

			if (intersections.size() > 0) {
				// sort mergepoints by y value (descending)
				Collections.sort(intersections, merge_comparator);

				visual_intersections.addAll(intersections);

				float len = sampled_point.dst(intersections.get(0).vertex);
				if (len * (1 - BORDER_ELEVATION_IN_PERCENT) > MapManager.MIN_DISTANCE_MAPELEMENTS) {
					Vector2 platformBetweenSamplePointAndIntersection = Util.subVectors(intersections.get(0).vertex,
							sampled_point);
					createdPoint = new VerticesCorrespondings(intersections.get(0).vertices, Util.addVectors(
							sampled_point, platformBetweenSamplePointAndIntersection, BORDER_ELEVATION_IN_PERCENT));
				} else
					createdPoint = new VerticesCorrespondings(intersections.get(0).vertices, sampled_point);
			} else {
				/*
				 * skip sampledpoint: die folgende zeile auskommentiert um zu
				 * verhindern dass die erstellte border immer wieder zur
				 * sampling achse zurückfällt nur weil keine insel detektiert
				 * wurde. das würde unnatürlich aussehen.
				 */
				// createdPoint = sampledPoint;
			}
			if (createdPoint != null) {
				// prev vertices of correspondings might be null!
				if (createdPoints.size() > 0) {
					VerticesCorrespondings prev = createdPoints.get(createdPoints.size() - 1);
					VerticesCorrespondings current = createdPoint;
					
					boolean distanceUnderThreshold;
					float distance;

					// distance to first island
					distance = MapManager.getMinDistanceBetweenPlatformAndIsland(prev.vertices, prev.vertex,
							current.vertex);

					// if distance is under threshold after first check there's
					// no further need for checking the second island
					distanceUnderThreshold = distance < MapManager.MIN_DISTANCE_MAPELEMENTS;

					if (distanceUnderThreshold == false) {
						if (prev.vertices != current.vertices)
							distance = MapManager.getMinDistanceBetweenPlatformAndIsland(current.vertices, prev.vertex,
									current.vertex);
					}
					if (distance < MapManager.MIN_DISTANCE_MAPELEMENTS) {

						/*
						 * wenn die distanz zu klein war, muss der jetzige aber
						 * auch der vorherige auf ebene des samplepoints. nur so
						 * kann eine spätere intersection vermieden werden.
						 */
						/*
						 * TODO Nachtrag: es kann sein theoretisch, dass ein
						 * durchgang zwischen border und island trotz dieser
						 * maßnahme unpassierbar ist
						 */
						if (side == 2)
							prev.vertex.x = sampled_point.x;
						else
							prev.vertex.y = sampled_point.y;
						createdPoint.vertex = sampled_point;
					}
				}
			
				createdPoints.add(createdPoint);
			}

			walkingPoint = increaseWalking(side, sample_step, walkingPoint);

			intersections.clear();
		}
		handleSidesEncounterAtCorner(side, createdPoints);

		for (VerticesCorrespondings triple : createdPoints)
			local_vertices.add(triple.vertex);

	}

	private void handleSidesEncounterAtCorner(int side, ArrayList<VerticesCorrespondings> createdPoints) {
		/*
		 * title: handle corners, an den Übergängen zweier Seiten können sehr
		 * spitze Winkel entstehen (siehe Dropbox: Implementierung/Bug
		 * Collection/Bug_Mapborder_1). Dies tritt dann auf wenn zwei Seiten mit
		 * nahen Inseln so interagieren, dass sie sich fast überlappen.
		 */
		if (side == 2 || side == 3) {
			/*
			 * Ansatz: kein Punkt darf x bzw. y mäßig UNTER
			 * local_vertices.get(size-1) liegen, wobei das der letzte Punkt der
			 * vorherigen Seite ist
			 */
			// y or x value of previous side
			float prevSideValue = 0;
			int lastIndex = local_vertices.size() - 1;
			if (side == 2)
				prevSideValue = local_vertices.get(lastIndex).y;
			if (side == 3)
				prevSideValue = local_vertices.get(lastIndex).x;

			Iterator<VerticesCorrespondings> it = createdPoints.iterator();
			VerticesCorrespondings createdPoint;
			while (it.hasNext() == true) {
				createdPoint = it.next();
				if (side == 2)
					if (prevSideValue < createdPoint.vertex.y)
						it.remove();
				if (side == 3)
					if (prevSideValue > createdPoint.vertex.x)
						it.remove();
			}

			/*
			 * The connecting platform between two sides might intersect with
			 * islands
			 */
			Vector2 sidePoint1 = local_vertices.get(lastIndex), sidePoint2 = createdPoints.get(0).vertex;
			ArrayList<Vector2[]> intersectingIslands = new ArrayList<Vector2[]>();
			for (VerticesCorrespondings c : left_island_vertices_and_aabb) {
				Vector2[] island = c.vertices;
				for (int d = 1; d <= island.length; d++) {
					Vector2 point1 = island[d - 1];
					Vector2 point2;
					if (d == island.length)
						point2 = island[0];
					else
						point2 = island[d];

					if (Intersector.intersectSegments(sidePoint1, sidePoint2, point1, point2, null) == true) {
						intersectingIslands.add(island);
						break;
					}
				}
			}

			/*
			 * Walk on last side and create new points on its sample axis as
			 * long as there are no longer intersections between island and
			 * connecting platform
			 */
			if (intersectingIslands.size() > 0) {
				boolean stillIntersecting;
				Vector2 walkingPoint = new Vector2();
				Vector2 sampledPoint;
				int lastSide = side - 1;

				do {
					stillIntersecting = false;
					increaseWalking(lastSide, sample_step, walkingPoint);
					sampledPoint = Util.addVectors(sidePoint1, walkingPoint);
					for (int g = 0; g < intersectingIslands.size(); g++) {
						Vector2[] island = intersectingIslands.get(g);
						for (int d = 1; d <= island.length; d++) {
							Vector2 point1 = island[d - 1];
							Vector2 point2;
							if (d == island.length)
								point2 = island[0];
							else
								point2 = island[d];

							if (Intersector.intersectSegments(sampledPoint, sidePoint2, point1, point2, null) == true) {
								local_vertices.add(sampledPoint);
								stillIntersecting = true;
								break;
							}
						}
						if (stillIntersecting == true)
							break;
					}
				} while (stillIntersecting == true);
			}

		}
	}

	private Vector2 increaseWalking(int side, float sampleStep, Vector2 walkingPoint) {

		switch (side) {
		case 1:
			walkingPoint.set(walkingPoint.x - sampleStep, 0);
			break;
		case 2:
			walkingPoint.set(0, walkingPoint.y - sampleStep);
			break;
		case 3:
			walkingPoint.set(walkingPoint.x + sampleStep, 0);
			break;
		}
		return walkingPoint;
	}

	/**
	 * Based on {@link #sampling_bound} and {@link #DIRAC_AMPLITUDE}
	 * 
	 * @param side
	 * @return
	 */
	private Vector2 getStartPointSampling(int side) {
		Vector2 startPointSampling = new Vector2();
		switch (side) {
		case 1:
			startPointSampling.set(0, sampling_bound.z);
			break;
		case 2:
			startPointSampling.set(sampling_bound.x, sampling_bound.z);
			break;
		case 3:
			startPointSampling.set(sampling_bound.x, sampling_bound.w);
			break;
		}
		return startPointSampling;
	}

	private Vector2 getDiracPeak(int side) {
		Vector2 diracPeak = new Vector2();
		switch (side) {
		case 1:
			diracPeak.set(0, -1);
			break;
		case 2:
			diracPeak.set(1, 0);
			break;
		case 3:
			diracPeak.set(0, 1);
			break;
		}
		return diracPeak.setLength(DIRAC_AMPLITUDE);
	}

	public ArrayList<Vector2> getLeftFillingVertices() {
		return left_filling_vertices;
	}

	public ArrayList<Vector2> getRightFillingVertices() {
		return right_filling_vertices;
	}

	public ArrayList<Vector2> getLeftFillingMinimapVertices() {
		return left_filling_minimap_vertices;
	}

	public ArrayList<Vector2> getRightFillingMinimapVertices() {
		return right_filling_minimap_vertices;
	}

	/**
	 * Sampling bound is equal to the innerbound of the map
	 * 
	 * @return
	 */
	public Vector4 getSamplingBound() {
		return sampling_bound;
	}

	public Vector2 getOuterBound() {
		return outer_bound;
	}

	private class MergePointComparator implements Comparator<VerticesCorrespondings> {

		@Override
		public int compare(VerticesCorrespondings cor1, VerticesCorrespondings cor2) {
			return cor1.vertex.dst(sampled_point) < cor2.vertex.dst(sampled_point) ? -1 : 1;
		}
	}

}
