package com.elementar.logic.map.generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.util.MyRandom;
import com.elementar.logic.util.Util;

/**
 * Contains beside generating algorithms also static help method for vertices
 * handling.
 * 
 * @author lukassongajlo
 * 
 */
public abstract class AVerticesGenerator {
	/**
	 * Specifies at which angle two platform vectors can be summarized into one
	 * platform
	 * <p>
	 * Note: When islands threshold is to high, it might occurs that symmetrical
	 * islands lose the symmetrical character... so when 'centric' is true I use
	 * 0 instead {@link #REDUNDANCE_THRESHOLD_ISLANDS}
	 */
	protected static final float REDUNDANCE_THRESHOLD_MAPBORDER = 6f,
			REDUNDANCE_THRESHOLD_ISLANDS = 3f;
	protected Vector2 start_point;
	protected ArrayList<Vector2> local_vertices, global_vertices;
	protected int num_vertices, half_num_vertices;
	protected MyRandom my_random;

	/**
	 * this vector leads to a distance between the already existing island and
	 * the new generated one, which will be merged to the already exisiting
	 * part.
	 */
	protected Vector2 merge_shift_vector;

	/**
	 * Only needed for {@link FractalVerticesGenerator} to direct the next
	 * merged island in a certain direcion depending on the last angle
	 */
	private float last_merge_angle = 0;

	/**
	 * debug information
	 */
	public int mergeMoreIntersectionsThanTwo = 0, notEnoughVerticesBySmallIslands = 0,
			notEnoughVerticesByBigIslands = 0, eliminateInternalLoopsCount = 0, mergeCountPrint = 0;

	public AVerticesGenerator(MyRandom myRandom) {
		this.my_random = myRandom;
	}

	/**
	 * Creates a blank set of local vertices of an island (or mapborder). The
	 * term 'raw' in this case means that there are no smoothed corners. Note in
	 * addition that the {@link #global_vertices} aren't used in the 'raw'
	 * method as well.
	 * <p>
	 * After creating a raw version of an island or mapborder, the corners of
	 * {@link #local_vertices} get smoothed.
	 */
	public void generateRaw() {
		local_vertices = new ArrayList<Vector2>();
	}

	/**
	 * Sets {@link #start_point} and {@link #global_vertices} if the local
	 * vertices aren't null.
	 * 
	 * @param startPoint
	 * @param centered
	 */
	public void setStartpoint(Vector2 startPoint, boolean centered) {
		this.start_point = startPoint;
		if (centered == true)
			start_point.x = 0;

		// sets global vertices
		if (local_vertices != null)
			global_vertices = Util.shiftVerticesTo(start_point, local_vertices);
	}

	/**
	 * This method mirrors the given local and startpoint. Result will be used
	 * to recalculate the global vertices.
	 * 
	 * @param xPosOfYAxis
	 */
	public void generateMirror(int xPosOfYAxis) {
		ArrayList<Vector2> localVertices = local_vertices;
		local_vertices = new ArrayList<Vector2>(localVertices.size());
		for (int i = 0; i < localVertices.size(); i++)
			local_vertices.add(i, mirrorVertex(localVertices.get(i), 0));
		local_vertices = AVerticesGenerator.reIndexation(0, local_vertices, true);
		start_point = mirrorVertex(start_point, 0);

		global_vertices = Util.shiftVerticesTo(start_point, local_vertices);
	}

	/**
	 * Merges {@link AVerticesGenerator#local_vertices} with param. Note that
	 * local_vertices and param have to be clockwise indexed. Returns true if
	 * merge succeeded or if {@link AVerticesGenerator#local_vertices} is empty
	 * 
	 * @param symmetricalMerge
	 *            , true for create an centered, symmetrical island
	 * @return
	 */
	protected boolean merge(boolean symmetricalMerge, ArrayList<Vector2> vertices) {

		// merge makes sense if there are 2 vertices groups to merge.
		if (local_vertices.size() == 0) {
			local_vertices.addAll(vertices);
			return true;
		}

		// shift island
		Vector2 shiftVec;
		float angle = 0;
		if (symmetricalMerge == true)
			shiftVec = new Vector2(0, 0);
		else {
			angle = last_merge_angle == 0 ? my_random.random(360f)
					: my_random.random(last_merge_angle - 90, last_merge_angle + 90);
			float length = my_random.random(2/* /3.5f */, 5/* /3.5f */);
			shiftVec = Util.addVectors(merge_shift_vector, new Vector2(
					length * MathUtils.cosDeg(angle), length * MathUtils.sinDeg(angle)));
		}
		vertices = Util.shiftVerticesTo(
				shiftVec/*
						 * // * local_vertices.get( // * indexEdgeStart) //
						 */, vertices);
		int numberIntersections = 0;

		/*
		 * those integers are indices that points to a vertex wherein the next
		 * vertex is a mergepoint, 11 = island-first-mergep., 12 =
		 * island-second-mergep., 21 = local-first-mergep., 22 =
		 * local-second-mergep.
		 */
		int index11 = -1, index12 = -1, index21 = -1, index22 = -1;
		Vector2 mergePoint1 = new Vector2(), mergePoint2 = new Vector2();

		// Wahl der Schnittpunkte
		ArrayList<Vector2> mergePoints = new ArrayList<Vector2>();
		for (int i = 1; i <= vertices.size(); i++) {
			Vector2 prevPoint = vertices.get(i - 1);
			Vector2 newPoint;
			if (i == vertices.size())
				newPoint = vertices.get(0);
			else
				newPoint = vertices.get(i);
			for (int j = 1; j <= local_vertices.size(); j++) {
				Vector2 prevPoint2 = local_vertices.get(j - 1);
				Vector2 newPoint2;
				if (j == local_vertices.size())
					newPoint2 = local_vertices.get(0);
				else
					newPoint2 = local_vertices.get(j);

				Vector2 mergePoint = new Vector2();
				if (Intersector.intersectSegments(prevPoint, newPoint, prevPoint2, newPoint2,
						mergePoint)) {
					numberIntersections++;
					// System.out
					// .println("AbstractVerticesGenerator.merge() mergepoint =
					// "
					// + mergePoint
					// + "; first pair = "
					// + prevPoint
					// + ", "
					// + newPoint
					// + ", sec pair = "
					// + prevPoint2
					// + ", "
					// + newPoint2);
					mergePoints.add(mergePoint);
					if (index11 == -1)
						index11 = i - 1;
					if (index21 == -1)
						index21 = j - 1;
					index22 = j - 1;
					index12 = i - 1;
				}
			}
		}

		if (numberIntersections == 2) {
			mergePoint1 = mergePoints.get(0);
			mergePoint2 = mergePoints.get(1);
		} else {
			mergeMoreIntersectionsThanTwo++;
			return false;
		}

		/*
		 * die indices 11 und 12 werden immer richtig sortiert sein in min und
		 * max, weil ich die iteration oben mit den island_vertices beginne. Bei
		 * 21 und 22 ist dies nicht unbedingt der fall:
		 */

		// insert mergePoints der mergepoints
		index12++;
		vertices.add(index11 + 1, mergePoint1);
		vertices.add(index12 + 1, mergePoint2);

		if (index21 < index22) {
			index22++;
			local_vertices.add(index21 + 1, mergePoint1);
			local_vertices.add(index22 + 1, mergePoint2);
		} else {
			local_vertices.add(index22 + 1, mergePoint2);
			index21++;
			local_vertices.add(index21 + 1, mergePoint1);
		}

		vertices.removeAll(getRemoveList(vertices, index11, index12));
		local_vertices.removeAll(getRemoveList(local_vertices, index21, index22));

		int indexMergePoint = getIndexByVertex(mergePoint1, vertices);
		// Might occurs when a normal vertex is also a mergePoint (seed:12)
		if (indexMergePoint == -1)
			return false;
		int index = getIndexByVertex(mergePoint1, local_vertices);
		// Might occurs when a normal vertex is also a mergePoint (seed:12)
		if (index == -1)
			return false;
		vertices = reIndexation(indexMergePoint + 1, vertices, false);
		local_vertices.addAll(index, vertices);

		/*
		 * remove duplicates (normally mergepoints that got added in
		 * local_vertices and vertices). Consider that the algorithm below
		 * removes duplicates completely (both points). That's important in a
		 * case where you are going to use the merge()-algorithm twice (see
		 * mapborder generating). If the mergePoints from the first merge
		 * process aren't removed it would causes more mergePoints in second one
		 * (given that the same merge-axis is used).
		 */
		removeDuplicates(local_vertices);

		merge_shift_vector = shiftVec;
		last_merge_angle = angle;

		return true;
	}

	/**
	 * Help method, especially for {@link #merge(boolean, boolean, ArrayList)}.
	 * When duplicates are detected, both of them will be removed.
	 * 
	 * @param vertices
	 *            , a list where duplicates need be removed from
	 * @return
	 */
	protected static void removeDuplicates(ArrayList<Vector2> vertices) {
		ArrayList<Vector2> duplicates = new ArrayList<Vector2>();
		for (int i = 0; i < vertices.size(); i++) {
			Vector2 v1 = vertices.get(i);
			for (int j = 0; j < vertices.size(); j++) {
				if (i != j) {
					Vector2 v2 = vertices.get(j);
					if (v1.equals(v2))
						duplicates.add(v1);
				}
			}
		}
		vertices.removeAll(duplicates);

	}

	/**
	 * checks distances to each vertex of {@link #local_vertices} and whether
	 * it's box2d-conform.
	 * 
	 * @return
	 */
	protected boolean checkDistances() {
		for (Vector2 vertexLocal1 : local_vertices)
			for (Vector2 vertexLocal2 : local_vertices)
				if (vertexLocal1.equals(vertexLocal2) == false)
					if (vertexLocal1.dst2(vertexLocal2) < 0.000026f)
						return false;
		return true;
	}

	/**
	 * Help method for merge.
	 * 
	 * @param vertex
	 * @param vertices
	 * @return
	 */
	private int getIndexByVertex(Vector2 vertex, ArrayList<Vector2> vertices) {
		for (int i = 0; i < vertices.size(); i++)
			if (vertex.equals(vertices.get(i)))
				return i;
		return -1;
	}

	/**
	 * help method for merge.
	 * 
	 * @param vertices
	 * @param index1
	 * @param index2
	 * @return
	 */
	private ArrayList<Vector2> getRemoveList(ArrayList<Vector2> vertices, int index1, int index2) {

		ArrayList<Vector2> rmList = new ArrayList<Vector2>();

		/*
		 * Q: Which side of vertices should be removed? The shorter one. It
		 * might be the wrong decision but it's probably better to prune the
		 * side with less vertices.
		 */
		if (Math.abs(index2 - index1) < Math
				.abs(vertices.size() - 1 - Math.max(index2, index1) + Math.min(index1, index2)))
			// hier geht er auch rein wenn index2 == index1 ist, allerdings ohne
			// zu removen
			// laufe vorwärts von min nach max (kein Übersprung von size() nach
			// 0 oder vice versa)
			for (int j = Math.min(index1, index2) + 2; j < Math.max(index2, index1) + 1; j++)
				rmList.add(vertices.get(j));
		else {

			float limit;
			if (Math.max(index2, index1) + 2 == vertices.size())
				limit = vertices.size() - 1;
			else
				limit = Math.max(index2, index1) + 1;

			for (int k = Math.min(index1, index2);; k--) {
				if (k == -1)
					k = vertices.size() - 1;
				// Note: it's important that the break-if-clause comes AFTER the
				// k == -1 if-clause
				if (k == limit)
					break;
				rmList.add(vertices.get(k));
			}
		}
		return rmList;
	}

	/**
	 * http://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-
	 * polygon-points-are-in-clockwise-order
	 * 
	 * @param vertices
	 * @return
	 */
	protected static boolean isPolygonClockwise(ArrayList<Vector2> vertices) {
		float sum = 0;
		for (int k = 0; k < vertices.size(); k++) {
			Vector2 vertex = vertices.get(k), vertex2;
			if (k == vertices.size() - 1)
				vertex2 = vertices.get(0);
			else
				vertex2 = vertices.get(k + 1);

			sum += ((vertex2.x - vertex.x) * (vertex2.y + vertex.y));
		}
		return sum > 0;
	}

	/**
	 * Starting from indexStart the vertices array get re ordered in -reverse or
	 * not reverse- direction.
	 * 
	 * @param indexStart
	 * @param vertices
	 * @param reverse
	 * @return
	 */
	public static ArrayList<Vector2> reIndexation(int indexStart, ArrayList<Vector2> vertices,
			boolean reverse) {
		ArrayList<Vector2> copyVertices = new ArrayList<Vector2>(vertices.size());

		if (reverse == false)
			for (int i = 0; i < vertices.size(); i++) {
				if (indexStart > vertices.size() - 1)
					indexStart = 0;
				copyVertices.add(vertices.get(indexStart));
				indexStart++;
			}
		else {
			for (int i = 0; i < vertices.size(); i++) {
				if (indexStart < 0)
					indexStart = vertices.size() - 1;
				copyVertices.add(vertices.get(indexStart));
				indexStart--;
			}
		}

		return copyVertices;

	}

	/**
	 * 
	 * @param point
	 * @param xPosOfYAxis
	 * @return
	 */
	public static Vector2 mirrorVertex(Vector2 point, int xPosOfYAxis) {
		if (point.x < xPosOfYAxis)
			return new Vector2(point.x + 2 * Math.abs(point.x - xPosOfYAxis), point.y);
		return new Vector2(point.x - 2 * Math.abs(point.x - xPosOfYAxis), point.y);
	}

	/**
	 * if two successive platform have nearly the same angle, the second one can
	 * be deleted.
	 * 
	 * @param angleThreshold
	 * @param vertices
	 */
	protected static void eliminateRedundance(float angleThreshold, ArrayList<Vector2> vertices) {
		ArrayList<Vector2> rmListLocal = new ArrayList<Vector2>();

		for (int i = 2; i <= vertices.size() + 1; i++) {
			Vector2 first = vertices.get(i - 2);

			Vector2 second;
			Vector2 third;
			if (i == vertices.size() + 1) {
				third = vertices.get(1);
				second = vertices.get(0);
			} else {
				second = vertices.get(i - 1);
				if (i == vertices.size())
					third = vertices.get(0);
				else
					third = vertices.get(i);
			}
			Vector2 platformDirection1 = Util.subVectors(second, first);
			Vector2 platformDirection2 = Util.subVectors(third, second);

			if (Math.abs(
					platformDirection1.angle() - platformDirection2.angle()) < angleThreshold) {
				rmListLocal.add(second);
			}
		}

		vertices.removeAll(rmListLocal);
	}

	/**
	 * Doesn't eliminate redundant vertices but returns a list of redundant
	 * vertices.
	 * 
	 * @param vertices
	 * @return
	 */
	protected static ArrayList<Vector2> getRedundantVertices(float angleThreshold,
			ArrayList<Vector2> vertices) {
		ArrayList<Vector2> rmListLocal = new ArrayList<Vector2>();

		for (int i = 2; i <= vertices.size() + 1; i++) {
			Vector2 first = vertices.get(i - 2);

			Vector2 second;
			Vector2 third;
			if (i == vertices.size() + 1) {
				third = vertices.get(1);
				second = vertices.get(0);
			} else {
				second = vertices.get(i - 1);
				if (i == vertices.size())
					third = vertices.get(0);
				else
					third = vertices.get(i);
			}
			Vector2 platformDirection1 = Util.subVectors(second, first);
			Vector2 platformDirection2 = Util.subVectors(third, second);

			if (Math.abs(
					platformDirection1.angle() - platformDirection2.angle()) < angleThreshold) {
				rmListLocal.add(second);
			}
		}
		return rmListLocal;
	}

	/**
	 * Takes finished localVertices and elimates internal loops. Use this method
	 * in a loop as long as it returns true. When it returns false there is no
	 * longer any loop. Use this method before smoothing corners!
	 * 
	 * @param vertices
	 * @return
	 */
	protected static boolean eliminateInternalLoops(ArrayList<Vector2> vertices) {
		Vector2 startPlatform, endPlatform, startPlatform2, endPlatform2;
		for (int i = 1; i < vertices.size() - 1; i++) {
			startPlatform = vertices.get(i - 1);
			endPlatform = vertices.get(i);

			for (int j = i + 2; j <= vertices.size(); j++) {

				startPlatform2 = vertices.get(j - 1);
				if (j == vertices.size()) {
					if (i - 1 != 0) {
						endPlatform2 = vertices.get(0);
					} else
						break;
				} else
					endPlatform2 = vertices.get(j);

				if (Intersector.intersectSegments(startPlatform, endPlatform, startPlatform2,
						endPlatform2, null)) {

					if (Math.abs(j - i) < Math.abs(vertices.size() - j + i)) {
						// i-1 mit j verbinden:

						for (int k = 0; k < Math.abs(j - i); k++)
							vertices.remove(i);
					} else {
						// j-1 mit i verbinden:

						List<Vector2> rmListLocal = new ArrayList<Vector2>();
						int index = i - 1;
						for (int k = 0; k < Math.abs(vertices.size() - j + i); k++) {
							rmListLocal.add(vertices.get(index));
							index--;
							if (index < 0)
								index = vertices.size() - 1;
						}
						vertices.removeAll(rmListLocal);

					}
					return true;
				}
			}
		}
		return false;
	}

	protected static List<ArrayList<Vector2>> corners;

	{
		corners = Collections.synchronizedList(new ArrayList<ArrayList<Vector2>>());
	}

	/**
	 * Smoothes the corners with help of a parable function. Distance between
	 * each vertex should be a little bit higher than 0.2f. Recursive algorithm.
	 * If you want a list with all smoothed corners where each corner is itself
	 * a list of vector2's you this method with "withSavingCorners = true". Use
	 * then the static field {@link #corners} Note that the vertices param is
	 * not allowed to have duplicates (for example beginning = ending for box2d
	 * chain)
	 * 
	 * @param index
	 *            , begin with 1
	 * @param localVertices
	 * @param curvingFactor
	 *            , the higher the smoother the corners, up to 0.5f.
	 * @param withSavingCorners
	 *            if true all smoothed corners will be saved in
	 *            {@value #corners}.
	 */
	protected synchronized static void smoothCorners(int index, ArrayList<Vector2> localVertices,
			float curvingFactor, boolean withSavingCorners) {

		if (withSavingCorners == true)
			corners.clear();
		// To determine platforms
		Vector2 posBegin = null;
		Vector2 posEnd = null;
		Vector2 posEndNext = null;

		Vector2 platformDir = null, platformDirNext = null;

		if (index > localVertices.size())
			return;

		int i = index;

		// define posBegin
		posBegin = localVertices.get(i - 1);

		// define posEnd
		/*
		 * finallyRemovedIndex = index des Vertex, der "schlussendlich" entfernt
		 * werden muss. schlussendlich da er nicht unbedingt i sein muss sondern
		 * auch 0 sein kann
		 */
		int finallyRemovedIndex = i;
		if (i == localVertices.size()) {
			finallyRemovedIndex = 0;
			posEnd = localVertices.get(0);
		} else
			posEnd = localVertices.get(i);

		// define posEndNext
		if (i == localVertices.size() - 1)
			posEndNext = localVertices.get(0);
		else if (i == localVertices.size())
			posEndNext = localVertices.get(1);
		else
			posEndNext = localVertices.get(i + 1);

		// create platforms
		platformDir = Util.subVectors(posBegin, posEnd);
		platformDirNext = Util.subVectors(posEndNext, posEnd);

		// is angle between platform higher than 175, if so it's not necessary
		// to smooth the corners, so skip it
		if (Math.abs(platformDirNext.angle(platformDir)) > 175) {
			smoothCorners(i + 1, localVertices, curvingFactor, false);
			return;
		}

		/*
		 * now I choose a certain length of both platforms (cathetus') to decide
		 * how long the baseline should be. The baseline is the x-axis where we
		 * will define a parable over it (for now it isnt a horizontal line see
		 * below)
		 */
		float cathetus = Math.min(platformDir.len() * curvingFactor,
				platformDirNext.len() * curvingFactor);
		platformDir.setLength(cathetus);
		platformDirNext.setLength(cathetus);

		Vector2 baseLine = Util.subVectors(platformDirNext, platformDir);
		// I defined that 0.152f is the lowest length of a section of a bended
		// corner, skip when lower
		float lengthBetweenEachPoint = 0.125f; // davor 0.175f
		if (baseLine.len() < lengthBetweenEachPoint) {
			smoothCorners(i + 1, localVertices, curvingFactor, false);
			return;
		}
		/*
		 * nun brauch ich den teilenden Höhenvektor um die beiden
		 * platformdirection so drehen, dass ich eine quadratische funktion
		 * dazwischen legen kann
		 */
		Vector2 dividingHeightVector = Util.addVectors(platformDir, platformDirNext);
		dividingHeightVector.scl(0.25f);

		// now I map those points to a suitable coordinate system (where all
		// points can pass trough a parable functions)
		float mappingAngle = 90 - dividingHeightVector.angle();

		dividingHeightVector.rotate(mappingAngle);
		platformDir.rotate(mappingAngle);
		platformDirNext.rotate(mappingAngle);

		float d = dividingHeightVector.y;
		float a = (platformDir.y - d) / (platformDir.x * platformDir.x);

		// create array for new points on the parabola
		int numberOfParableLines = ((int) (baseLine.len() / lengthBetweenEachPoint));
		int numberOfParablePoints = numberOfParableLines - 1;
		ArrayList<Vector2> newPoints = new ArrayList<Vector2>(numberOfParablePoints);
		// rotated baseline is now a horizontal line
		baseLine = Util.subVectors(platformDirNext, platformDir);

		Vector2 newPoint = null, quadraticPoint = null;

		newPoints.add(platformDir);
		// ___
		for (int j = 1; j <= numberOfParablePoints; j++) {
			newPoint = Util.addVectors(platformDir, baseLine, (j * 1.0f) / numberOfParableLines);
			quadraticPoint = new Vector2(newPoint.x, quadraticBendingFunc(newPoint.x, a, d));
			newPoints.add(j, quadraticPoint);
		}

		newPoints.add(platformDirNext);

		// map back
		mappingAngle *= (-1);
		for (Vector2 vec : newPoints)
			vec.rotate(mappingAngle);
		for (Vector2 vec : newPoints)
			vec.set(Util.addVectors(posEnd, vec));
		// size increased (+6)
		localVertices.remove(finallyRemovedIndex);
		localVertices.addAll(finallyRemovedIndex, newPoints);
		corners.add(newPoints);
		// index increased (+7)
		smoothCorners(i + newPoints.size(), localVertices, curvingFactor, false);

	}

	private static float quadraticBendingFunc(float x, float a, float d) {
		return a * x * x + d;
	}

	/**
	 * Returns the local vertices of a generating process as a vector2 array.
	 * Box2d needs an array instead of a list.
	 * 
	 * @return
	 */
	public Vector2[] getLocalVertices() {
		return Util.convertVector2ArrayListIntoVector2Array(local_vertices);
	}

	public Vector2[] getGlobalVertices() {
		return Util.convertVector2ArrayListIntoVector2Array(global_vertices);
	}

	public ArrayList<Vector2> getGlobalVerticesAsList() {
		return global_vertices;
	}

	public Vector2 getStartPoint() {
		return start_point;
	}

}
