package com.elementar.logic.map.generator;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.map.MapManager.IslandSizes;
import com.elementar.logic.util.MyRandom;
import com.elementar.logic.util.Util;

public class FractalVerticesGenerator extends AVerticesGenerator {

	private ArrayList<Vector2> island_vertices;
	private int num_triangles_of_each_individual_island;
	/**
	 * 0.1f errors, previously = 0.2f for tests (smaller islands)
	 */
	private final float EDGE_LENGTH = 0.7f;
	private int num_merged_islands;

	private int mergeCount = 0;

	private static final float ISLAND_CURVING_FACTOR = 0.4f;

	/**
	 * if a island has a less number of vertices it is more likely that a
	 * triangle shape is the result. We want to avoid this.
	 */
	private static final int MINIMUM_NUM_VERTICES = 5;

	/**
	 * if true: islands will end up as symmetrical islands. For instance: Use
	 * this to create islands that should be in the map center for more
	 * fairness.
	 */
	private boolean centered = false;

	private boolean bonus_generation = false;

	public FractalVerticesGenerator(MyRandom random) {
		super(random);
	}

	/**
	 * Generates a 'raw' island, which means there are no smoothed corners and
	 * {@link AVerticesGenerator#global_vertices} is null. Returns false if the
	 * island generation/forming failed.
	 * 
	 * @param size
	 * @param centered
	 *            , <code>true</code> if the island should be generated in the
	 *            middle of the map
	 * @param fillingIsland
	 *            , <code>true</code> a small (small as possible) should be
	 *            generated to fill gaps inside the map
	 * @return
	 */
	public boolean generateRaw(IslandSizes size, boolean centered, boolean fillingIsland) {
		super.generateRaw();
		this.bonus_generation = fillingIsland;

		this.centered = centered;
		this.merge_shift_vector = new Vector2();

		num_merged_islands = getNumMergedIslandBy(size);

		mergeCount = 0;

		for (int j = 0; j < num_merged_islands;) {

			do {
				mergeCount++;
				island_vertices = new ArrayList<Vector2>();

				num_triangles_of_each_individual_island = getNumTrianglesBy(size);
				createStartTriangle();

				for (int i = 0; i < num_triangles_of_each_individual_island; i++) {
					addTriangle();
				}

				equalizeMinDistance(island_vertices);

				if (island_vertices.size() < MINIMUM_NUM_VERTICES) {
					if (size == IslandSizes.BIG)
						notEnoughVerticesByBigIslands++;
					if (size == IslandSizes.SMALL)
						notEnoughVerticesBySmallIslands++;
					continue;
				}

			} while (merge(false, island_vertices) == false);

			j++;
		}

		// die erfolgreichen merges werden nicht dazugezählt
		mergeCount = mergeCount - num_merged_islands;
		mergeCountPrint += mergeCount;

		island_vertices = null;

		float redundanceThreshold = getRedundanceThreshold();

		eliminateRedundance(redundanceThreshold, local_vertices);

		if (local_vertices.size() < MINIMUM_NUM_VERTICES) {
			if (size == IslandSizes.BIG)
				notEnoughVerticesByBigIslands++;
			if (size == IslandSizes.SMALL)
				notEnoughVerticesBySmallIslands++;
			return false;
		}

		/*
		 * TODO diese Methode kann fehlerhaft sein, deshalb muss ich am Ende die
		 * abfrage machen ob die locals mind. 4 ecken haben, sonst wärs glaub
		 * ok.
		 */
		while (eliminateInternalLoops(local_vertices) == true)
			eliminateInternalLoopsCount++;

		// spitze winkel abschlagen
		equalizeAngle(local_vertices);

		/*
		 * TODO diese Methode kann fehlerhaft sein, deshalb muss ich am Ende die
		 * abfrage machen ob die locals mind. 4 ecken haben, sonst wärs glaub
		 * ok. TODO ANMERKUNG: equalize KANN neue loops erzeugen daher muss ich
		 * hier ein // zweites mal loops killen
		 */
		while (eliminateInternalLoops(local_vertices) == true)
			eliminateInternalLoopsCount++;

		eliminateRedundance(redundanceThreshold, local_vertices);

		if (local_vertices.size() < MINIMUM_NUM_VERTICES) {
			if (size == IslandSizes.BIG)
				notEnoughVerticesByBigIslands++;
			if (size == IslandSizes.SMALL)
				notEnoughVerticesBySmallIslands++;
			return false;
		}

		if (AVerticesGenerator.isPolygonClockwise(local_vertices) == false)
			local_vertices = AVerticesGenerator.reIndexation(0, local_vertices, true);

		/*
		 * local-vertices is equalized. So if centered == true I can start with
		 * mirroring:
		 */
		if (this.centered == true)
			if (createSymmetricalIsland(size) == false) {
				return false;
			}
		// once the basic-shape is determined a check has to be applied whether
		// the minimal distance (box2d) is adhered to.
		if (checkDistances() == false)
			return false;

		return true;
	}

	private boolean createSymmetricalIsland(IslandSizes size) {
		// merge-process requires set island and local vertices. local
		// vertices are already set.
		island_vertices = new ArrayList<Vector2>(local_vertices.size());

		for (Vector2 vertex : local_vertices)
			island_vertices.add(mirrorVertex(vertex, 0));

		island_vertices = reIndexation(0, island_vertices, true);

		mergeCount++;

		return merge(true, island_vertices);
	}

	public void generateFinalized() {
		// makeTunnels(local_vertices);
		smoothCorners(1, local_vertices, ISLAND_CURVING_FACTOR, false);

		AVerticesGenerator.eliminateRedundance(AVerticesGenerator.REDUNDANCE_THRESHOLD_ISLANDS, local_vertices);

		global_vertices = Util.shiftVerticesTo(start_point, local_vertices);

	}

	/**
	 * If {@link #centered} is <code>true</code> the angle threshold should 0 to
	 * avoid destroying symmetry, otherwise it returns
	 * {@link AVerticesGenerator#REDUNDANCE_THRESHOLD_ISLANDS}
	 * 
	 * @return
	 */
	private float getRedundanceThreshold() {
		return centered == true ? 0 : AVerticesGenerator.REDUNDANCE_THRESHOLD_ISLANDS;
	}

	private int getNumMergedIslandBy(IslandSizes size) {
		int numMergedIslands = 1;
		switch (size) {
		case BIG:
			/*
			 * wenn ich die werte der nachfolgenden fehler hochtreibe, kann ich
			 * evtl. einen fehler erzeugen, den es zu beheben gilt. (merge
			 * method)
			 */
			numMergedIslands = my_random.random(4, 5);
			break;
		case SMALL:
			// it's more perfomant to use just one island for smalls
			numMergedIslands = my_random.random(2, 4);
			break;
		}

		return numMergedIslands;
	}

	/**
	 * Returns the number of triangles of each island part (where each part is a
	 * part of the merge process)
	 * 
	 * @param size
	 * @return
	 */
	private int getNumTrianglesBy(IslandSizes size) {
		int numTriangles = 10;
		switch (size) {
		case BIG:
			numTriangles = 10 + my_random.random(10);// my_random.random(40) +
														// 40;
			break;
		case SMALL:
			/*
			 * 10 is minimum. When bonus is active this generator will create
			 * the smallest island to complete the map
			 */
			numTriangles = 10 + (bonus_generation == true ? 0 : my_random.random(10));
			break;
		}

		return numTriangles;
	}

	private void createStartTriangle() {
		island_vertices.add(0, new Vector2(0, 0));

		// ______________________
		Vector2 firstEdge = new Vector2(EDGE_LENGTH * MathUtils.cosDeg(60), EDGE_LENGTH * MathUtils.sinDeg(60));
		island_vertices.add(1, firstEdge);
		// ______________________
		Vector2 secondEdge = new Vector2(EDGE_LENGTH * MathUtils.cosDeg(0), EDGE_LENGTH * MathUtils.sinDeg(0));
		island_vertices.add(2, secondEdge);

		// NICHT NÖTIG DA RANDOMISLAND LOOPED (chainloop(..))
		// local_vertices.add(3, localStartPoint);
		// global_vertices.add(3,
		// Util.addVectors(globalStartPoint, localStartPoint));

	}

	public void addTriangle() {

		// take a random edge
		int indexEdgeStart = my_random.random(0, island_vertices.size() - 1);

		int indexEdgeEnd;
		if (indexEdgeStart == island_vertices.size() - 1)
			indexEdgeEnd = 0;
		else
			indexEdgeEnd = indexEdgeStart + 1;

		Vector2 edge = Util.subVectors(island_vertices.get(indexEdgeEnd), island_vertices.get(indexEdgeStart));

		int angle = (int) edge.angle();
		if (angle % 60 != 0) {
			int toggle = 1;
			while (true) {
				if ((angle + toggle) % 60 == 0) {
					angle += toggle;
					break;
				}
				if ((angle - toggle) % 60 == 0) {
					angle -= toggle;
					break;
				}
				toggle++;
			}
		}
		createNewTriangle(angle, indexEdgeStart);
	}

	private void createNewTriangle(int angle, int indexEdgeStart) {
		Vector2 localStartPoint = island_vertices.get(indexEdgeStart);

		Vector2 firstEdge = new Vector2(EDGE_LENGTH * MathUtils.cosDeg(angle + 60),
				EDGE_LENGTH * MathUtils.sinDeg(angle + 60));

		Vector2 newPoint = Util.addVectors(localStartPoint, firstEdge);

		// is new Point already in use?
		float minDistance = 0.1f;
		int rmIndex = -1;
		for (int i = 0; i < island_vertices.size(); i++) {
			Vector2 vertex = island_vertices.get(i);
			if (vertex.dst(newPoint) < minDistance) {
				if (Math.abs(indexEdgeStart - i) == 1) {
					rmIndex = indexEdgeStart;
				}
				if (Math.abs(indexEdgeStart - i) == 2) {
					rmIndex = (indexEdgeStart + i) / 2;
				} else {
					if (indexEdgeStart == island_vertices.size() - 2 && i == 0)
						rmIndex = island_vertices.size() - 1;
					else if (indexEdgeStart == 0 && i == island_vertices.size() - 1)
						rmIndex = 0;
					else if (indexEdgeStart == 0 && i == island_vertices.size() - 2)
						rmIndex = island_vertices.size() - 1;
					else if (indexEdgeStart == island_vertices.size() - 1 && i == 1)
						rmIndex = 0;
					else
						return;
				}
				break;
			}
		}
		if (rmIndex != -1)
			island_vertices.remove(rmIndex);
		else
			island_vertices.add(indexEdgeStart + 1, newPoint);

	}

	// glätten
	// GLÄTTUNG: nur alle 2-3 vertices betrachten und dann
	// weitergehen, um höhle nicht zu vernichten...
	public void equalizeMinDistance(ArrayList<Vector2> localVertices) {
		// im prinzip suche ich ein lokales minimum

		int i = 0;
		int size;
		while (true) {

			size = localVertices.size();
			if (i > size - 1)
				break;

			Vector2 pairFirst = null, pairSecond = null;
			int indexFirst = -1, indexSecond = -1;
			float minDistance = Float.MAX_VALUE;
			for (int k = i; k < localVertices.size(); k++) {
				Vector2 vertex = localVertices.get(k);
				Vector2 edge1, edge2;
				if (k == localVertices.size() - 1)
					edge1 = Util.subVectors(localVertices.get(0), vertex);
				else
					edge1 = Util.subVectors(localVertices.get(k + 1), vertex);
				if (k == 0)
					edge2 = Util.subVectors(vertex, localVertices.get(localVertices.size() - 1));
				else
					edge2 = Util.subVectors(vertex, localVertices.get(k - 1));

				if (Math.abs(edge1.angle() - edge2.angle()) > 0.4) {
					// die winkel sind unterschiedlich

					if (pairFirst == null) {
						indexFirst = k;
						pairFirst = vertex;
					} else {
						if (k > indexFirst + 1) {
							float newDistance = pairFirst.dst(vertex);
							if (newDistance < minDistance) {
								minDistance = newDistance;
								pairSecond = localVertices.get(k);
							} else if (minDistance < Float.MAX_VALUE - 1 && newDistance > minDistance)
								break;

						}
					}
				} else {
					i++;
				}
			}
			boolean startRemoving = false;

			Iterator<Vector2> it = localVertices.iterator();
			Vector2 vertex;
			while (it.hasNext() == true) {
				vertex = it.next();

				if (vertex.equals(pairSecond))
					break;

				if (startRemoving == true)
					it.remove();

				if (vertex.equals(pairFirst))
					startRemoving = true;

			}

			i++;
			// return;
		}

	}

	static int angle_toleranz_first_equalize = 85;

	/**
	 * ich gestalte die Methode erstmal so, dass pro aufruf nur eine stelle
	 * behoben wird. das hat zur folge, dass ich nach einer behebung nicht
	 * wissen muss bei welchem vertex es weiter geht mit der suche. Eine
	 * behebung könnte nämlich zur folge haben, dass nachfolgende winkel, die
	 * ich als kritisch eingestuft hätte, nun nicht mehr kritisch sind
	 * 
	 * @param vertices
	 * @return
	 */
	public static void equalizeAngle(ArrayList<Vector2> vertices) {

		if (vertices.size() < MINIMUM_NUM_VERTICES)
			return;
		int angle;

		/**
		 * wenn ich bei k bin dann ist das wie:
		 * 
		 * __edge1___ . ___edge2__
		 */
		for (int k = 0; k < vertices.size(); k++) {
			Vector2 vertex = vertices.get(k);
			Vector2 edge2, edge1;
			if (k == 0)
				edge1 = Util.subVectors(vertex, vertices.get(vertices.size() - 1));
			else
				edge1 = Util.subVectors(vertex, vertices.get(k - 1));

			if (k == vertices.size() - 1)
				edge2 = Util.subVectors(vertices.get(0), vertex);
			else {
				edge2 = Util.subVectors(vertices.get(k + 1), vertex);
			}

			// bedenken: angle ist float
			angle = Math.round(edge2.angle(edge1));

			if (Math.abs(angle) >= angle_toleranz_first_equalize) {
				vertices.remove(vertex);
				equalizeAngle(vertices);
				break;
			}
		}
	}

}
