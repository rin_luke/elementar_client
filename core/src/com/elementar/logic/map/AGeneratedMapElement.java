package com.elementar.logic.map;

import com.badlogic.gdx.physics.box2d.World;
import com.elementar.logic.map.generator.AVerticesGenerator;

public abstract class AGeneratedMapElement extends AMapElement {

	public AGeneratedMapElement(World world, AGeneratedMapElement mapElement) {
		super(world, mapElement);
	}

	public AGeneratedMapElement(World world,
			AVerticesGenerator verticesGenerator) {
		super(world, verticesGenerator.getStartPoint(), verticesGenerator
				.getLocalVertices(), verticesGenerator.getGlobalVertices());
	}

}
