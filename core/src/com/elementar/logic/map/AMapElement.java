package com.elementar.logic.map;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.elementar.logic.characters.abstracts.APhysicalElement;
import com.elementar.logic.util.CollisionData;

public abstract class AMapElement extends APhysicalElement {

	protected Vector2[] local_vertices, global_vertices;
	protected Vector2 start_point;

	protected Fixture main_fixture;

	/**
	 * Copy constructor
	 * 
	 * @param world
	 * @param element
	 */
	public AMapElement(World world, AMapElement element) {
		this(world, element.start_point, element.local_vertices, element.global_vertices);
	}

	public AMapElement(World world, Vector2 startPoint, Vector2[] localVertices, Vector2[] globalVertices) {
		super(world);
		local_vertices = localVertices;
		global_vertices = globalVertices;
		start_point = startPoint;
		if (world != null && start_point != null)
			initPhysics(start_point.x, start_point.y);
	}

	@Override
	protected Filter getCollisionFilter() {
		// could collide with character and team1,team2 projectiles
		Filter collisionFilter = new Filter();
		collisionFilter.categoryBits = CollisionData.BIT_GROUND;
		collisionFilter.maskBits = CollisionData.BIT_CHARACTER_MOVEMENT;
		collisionFilter.maskBits |= CollisionData.BIT_PROJECTILE;
		collisionFilter.maskBits |= CollisionData.BIT_LIGHT;
		return collisionFilter;
	}

	public Vector2[] getLocalVertices() {
		return local_vertices;
	}

	public Vector2[] getGlobalVertices() {
		return global_vertices;
	}

	// TODO why do I need this
	public Vector2 getStartPoint() {
		return global_vertices[0];
	}

	public Vector2 getStartPoint2() {
		return start_point;
	}

}
