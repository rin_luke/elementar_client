package com.elementar.logic.map;

import static com.elementar.logic.util.Shared.DEBUG_MODE_ISLAND_REDUCE;
import static com.elementar.logic.util.Shared.MAP_HEIGHT;
import static com.elementar.logic.util.Shared.MAP_WIDTH;

import java.util.ArrayList;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.elementar.data.container.AnimationContainer;
import com.elementar.gui.abstracts.Loadable;
import com.elementar.gui.util.CustomSkeleton;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.map.buildings.ABuilding;
import com.elementar.logic.map.buildings.Base;
import com.elementar.logic.map.buildings.Shrine;
import com.elementar.logic.map.buildings.Tower;
import com.elementar.logic.map.generator.AVerticesGenerator;
import com.elementar.logic.map.generator.FractalVerticesGenerator;
import com.elementar.logic.map.generator.MapBorderVerticesGenerator;
import com.elementar.logic.map.pathfinding.Graph;
import com.elementar.logic.map.pathfinding.GraphGenerator;
import com.elementar.logic.map.pathfinding.LightCluster;
import com.elementar.logic.map.util.HorizontalSegments;
import com.elementar.logic.map.util.Vector4;
import com.elementar.logic.map.util.VerticesCorrespondings;
import com.elementar.logic.util.MyRandom;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.Shared.Team;
import com.elementar.logic.util.maps.LimitedHashMap;

public class MapManager extends Thread implements Loadable {

	/**
	 * Specifies how far is the minimal distance between islands, bases and map
	 * border to each other
	 */
	public static final float MIN_DISTANCE_MAPELEMENTS = 0.8f;
	/**
	 * Stores all island hitboxes and also both base hitboxes (enclosing edges,
	 * not every single element). The hitboxes are equivalent to the global
	 * vertices of the specific island or base.
	 * <p>
	 * When adding a new hitbox it adds its deep copy. That's necessary because
	 * we just want the blank representation of an island or base without any
	 * smoothed edges. Smoothed edges would slow down the
	 * {@link #isPositionInvalid()} check.
	 * <p>
	 * After the create-process this list get cleared.
	 */
	private ArrayList<Vector2[]> all_hitboxes = new DeepCopyVerticesList() {
		@Override
		public boolean add(Vector2[] e) {
			Vector2[] copy = new Vector2[e.length];
			System.arraycopy(e, 0, copy, 0, e.length);
			return super.add(copy);
		};
	};
	/**
	 * When a mapManager obj is used to create background islands the 2 base
	 * objs might be null
	 */
	private Base base_left, base_right;

	private LimitedHashMap<Integer, ABuilding> buildings;

	private FractalVerticesGenerator fractal_vertices_gen;
	private HorizontalSegments horizontal_segments;

	/**
	 * Stores all smoothed islands.
	 */
	private ArrayList<RandomIsland> islands;

	private MapBorder map_border;
	private MapBorderVerticesGenerator map_border_gen;
	private boolean mirror_map;
	private MyRandom my_random;
	int newIslandColliding = 0, underMinimalDistance = 0, externalUnderMinimalDistance = 0,
			mirrorHitMiddleLine = 0, crossesMapDimensions = 0;

	private final int NUM_ISLANDS_BIG = 2;

	private final int NUM_ISLANDS_SMALL = 4;
	// from that...
	private final int NUM_ISLANDS_SMALL_CENTERED = 2;

	// smallest possible islands
	private final int NUM_ISLANDS_SMALL_FILLING = 8;

	private final int NUM_SHRINES = 4, NUM_TOWERS = 8;

	private boolean running;

	private boolean failed = false;

	private long seed;

	/**
	 * Stores all shrine skeletons.
	 */
	private ArrayList<Shrine> shrines;

	private ArrayList<Tower> towers;

	private World world;

	private Location location;

	/**
	 * for light positioning
	 */
	private Graph graph;

	private ArrayList<LightCluster> light_clusters;

	private abstract class APositioningCheck {

		/**
		 * tries for positioning
		 */
		private int tries_positioning;

		/**
		 * Returns <code>false</code> if the positioning check failed. It failed
		 * when after a certain amount of tries the island still doesn't fit.
		 * 
		 * @param centered
		 * @param size
		 * @return
		 */
		boolean check(boolean centered, IslandSizes size) {
			// reset
			tries_positioning = 100;
			do {
				if (tries_positioning == 0)
					return false;
				tries_positioning--;
				fractal_vertices_gen.setStartpoint(getRandomPoint(), centered);
			} while (isPositionInvalid(centered, size) == true);
			return true;
		}

		/**
		 * Returns a random position within the map in a valid range. You are
		 * responsible to set the x and y value randomly.
		 * 
		 * @return
		 */
		abstract Vector2 getRandomPoint();

	}

	private class DeepCopyVerticesList extends ArrayList<Vector2[]> {
		@Override
		public boolean add(Vector2[] e) {
			return super.add(getVerticesDeepCopy(e));
		};
	}

	public static enum IslandSizes {
		BIG, SMALL
	}

	/**
	 * 
	 * @param mapManager
	 * @param world
	 */
	public static void createMapWithGivenParameters(MapManager mapManager, World world) {

		new MapBorder(world, mapManager.map_border_gen);

		new Base(world, mapManager.base_right);
		new Base(world, mapManager.base_left);

		for (RandomIsland island : mapManager.islands)
			new RandomIsland(world, island);

	}

	/**
	 * Result might be negative if platform intersects with island.
	 * 
	 * @param islandVertices
	 *            , allowed to be null
	 * @param platformPoint1
	 * @param platformPoint2
	 * @return
	 */
	public static float getMinDistanceBetweenPlatformAndIsland(Vector2[] islandVertices,
			Vector2 platformPoint1, Vector2 platformPoint2) {
		float distance = Float.MAX_VALUE;
		if (islandVertices != null)
			for (int k = 1; k <= islandVertices.length; k++) {
				// 1. DETERMINE SUCCESSIVELY ISLAND PLATFORM
				Vector2 prevPoint = islandVertices[k - 1];
				Vector2 newPoint;
				if (k == islandVertices.length)
					newPoint = islandVertices[0];
				else
					newPoint = islandVertices[k];

				// 2. CALC DISTANCE
				distance = Util.distanceSegments(platformPoint1, platformPoint2, prevPoint,
						newPoint);
				// if distance is under threshold there is no need for further
				// checking
				if (distance < MapManager.MIN_DISTANCE_MAPELEMENTS)
					return distance;
			}
		return distance;
	}

	private static Vector2[] getVerticesDeepCopy(Vector2[] e) {
		Vector2[] copy = new Vector2[e.length];
		System.arraycopy(e, 0, copy, 0, e.length);
		return copy;
	}

	public MapManager(World world, Location location, boolean mirrorMap, long seed) {
		this.seed = seed;
		this.running = true;
		this.world = world;
		this.mirror_map = mirrorMap;
		this.location = location;
	}

	@Override
	public void run() {
		createRandom();
		fractal_vertices_gen = new FractalVerticesGenerator(my_random);
		map_border_gen = new MapBorderVerticesGenerator(my_random);
		horizontal_segments = new HorizontalSegments();

		all_hitboxes.clear();

		createScene();

		if (running == true)
			// for managing network
			fillBuildingsStructure();

		dispose();
	}

	public void createRandom() {
		my_random = new MyRandom(seed);
	}

	/**
	 * First the bases are created, then islands and map border. Finally the
	 * buildings are created
	 */
	private void createScene() {

		// 1. create both bases
		base_right = createBase(AnimationContainer.skeleton_base_right);

		base_left = createBase(AnimationContainer.skeleton_base_left);

		// 2. islands
		int numBig = NUM_ISLANDS_BIG;
		int numSmall = NUM_ISLANDS_SMALL;

		if (DEBUG_MODE_ISLAND_REDUCE == true) {
			numBig = 1;
			numSmall = 0;
		}

		islands = new ArrayList<RandomIsland>(numSmall + numBig);

		System.out.println("MapManager.createScene()		Seed: " + seed);
		System.out.println("MapManager.createForeground()		num-BIG = " + numBig
				+ ", num-SMALL = " + numSmall + ", from that = " + NUM_ISLANDS_SMALL_CENTERED
				+ " centric smalls ");

		// centered islands
		// if (my_random.randomBoolean()) {
		// System.out.println("MapManager.createForeground() BEGIN CENTERED
		// BIG");
		// createIslands(IslandSizes.BIG, NUM_ISLANDS_BIG_CENTERED, true,
		// false);
		// numBig -= NUM_ISLANDS_BIG_CENTERED;
		// }

		System.out.println("MapManager.createForeground()		BEGIN NON-CENTRICS BIG");
		createIslands(IslandSizes.BIG, numBig, false, false);

		// centered small islands
		System.out.println("MapManager.createForeground()		BEGIN CENTERED SMALL");
		createIslands(IslandSizes.SMALL, NUM_ISLANDS_SMALL_CENTERED, true, false);
		numSmall -= NUM_ISLANDS_SMALL_CENTERED;

		System.out.println("MapManager.createForeground()		BEGIN NON-CENTRICS SMALL");
		createIslands(IslandSizes.SMALL, numSmall, false, false);

		System.out.println("MapManager.createForeground()		BEGIN BONUS WITH SMALLS");
		createIslands(IslandSizes.SMALL, NUM_ISLANDS_SMALL_FILLING, false, true);

		if (running == false)
			return;

		// 3. map border
		/*
		 * 3.1 determine allhitboxes on left side + their aabbs for better
		 * performance while generating border
		 */

		/*
		 * remove bases because they would disturb the map-border generation
		 * process since they are added to the wall
		 */
		all_hitboxes.remove(0);
		all_hitboxes.remove(0);

		ArrayList<VerticesCorrespondings> leftIslands = new ArrayList<VerticesCorrespondings>();
		for (Vector2[] hitbox : all_hitboxes) {

			Rectangle boundingRect = (new Polygon(Util.convertVector2ArrayIntoFloatArray(hitbox)))
					.getBoundingRectangle();

			ArrayList<Vector2> aabb = new ArrayList<Vector2>();
			// bottom left
			aabb.add(new Vector2(boundingRect.x, boundingRect.y));
			// bottom right
			aabb.add(new Vector2(boundingRect.x + boundingRect.width, boundingRect.y));
			// top right
			aabb.add(new Vector2(boundingRect.x + boundingRect.width,
					boundingRect.y + boundingRect.height));
			// top left
			aabb.add(new Vector2(boundingRect.x, boundingRect.y + boundingRect.height));

			if (boundingRect.x < 0)
				leftIslands.add(new VerticesCorrespondings(getVerticesDeepCopy(hitbox),
						Util.getPolygon(aabb)));
		}

		// 3.2 determine most left, right, top and bottom point
		float left = 0, right = 0, top = 0, bottom = 0;
		for (Vector2[] vertices : all_hitboxes) {
			for (Vector2 vertex : vertices) {
				if (vertex.x < left)
					left = vertex.x;
				if (vertex.y > top)
					top = vertex.y;
				if (vertex.y < bottom)
					bottom = vertex.y;
			}
		}

		right = left * (-1);
		/*
		 * now I have a bounding box where all islands lie in. add three times
		 * the minimal distance to all sides: ('three times' to avoid hard
		 * edges, twice is not enough)
		 */
		left -= 3 * MIN_DISTANCE_MAPELEMENTS;
		right += 3 * MIN_DISTANCE_MAPELEMENTS;
		top += 3 * MIN_DISTANCE_MAPELEMENTS;
		bottom -= 3 * MIN_DISTANCE_MAPELEMENTS;

		// that is the inner band of potential map border lines
		map_border_gen.generate(new Vector4(left, right, top, bottom), leftIslands, base_left,
				base_right);

		// create border with local_vertices (= global_vertices in this
		// case)
		map_border = new MapBorder(world, map_border_gen);

		graph = GraphGenerator.generate(all_hitboxes, map_border);

		light_clusters = graph.getLightClusters();

		horizontal_segments.saveSegments(
				Util.convertVector2ArrayIntoVector2ArrayList(map_border.local_vertices));

		if (running == false)
			return;

		do {
			do {

				if (towers != null)
					for (Tower t : towers)
						t.destroy();

				if (shrines != null)
					for (Shrine s : shrines)
						s.destroy();

				// 4. Create buildings
				shrines = new ArrayList<Shrine>(NUM_SHRINES);
				towers = new ArrayList<Tower>(NUM_TOWERS);

				horizontal_segments.begin();

				// createTowers();
				createShrine();

				horizontal_segments.end(my_random);

			} while (shrines.size() != NUM_SHRINES);// || towers.size() !=
													// NUM_TOWERS);

		} while (failed == true);
	}

	private void createShrine() {
		Vector2 next;
		int counterShrines = 0;
		while (true) {
			if (counterShrines < NUM_SHRINES) {

				next = horizontal_segments.nextShrine();

				if (next != null) {
					Shrine s1 = new Shrine(world, location, next);
					if (isBuildingTouchingIslandsOrBases(s1) == false) {
						shrines.add(s1);

						Shrine s2 = new Shrine(world, location,
								AVerticesGenerator.mirrorVertex(next, 0));
						shrines.add(s2);
						counterShrines += 2;
					}
				}
			}

			if (counterShrines >= NUM_SHRINES || horizontal_segments.isEmpty() == true)
				break;
		}
	}

	public Graph getGraph() {
		return graph;
	}

	private void createTowers() {
		Vector2 next;
		int counterTowers = 0;
		while (true) {

			if (counterTowers < NUM_TOWERS) {

				next = horizontal_segments.nextTower();

				if (next != null) {
					// determine team affiliation by position
					Team teamT1 = next.x < 0 ? Team.TEAM_1 : Team.TEAM_2;

					Team teamT2 = teamT1 == Team.TEAM_1 ? Team.TEAM_2 : Team.TEAM_1;

					Tower t1 = new Tower(world, location, next, teamT1);

					if (isBuildingTouchingIslandsOrBases(t1) == false) {
						towers.add(t1);

						// mirror tower
						towers.add(new Tower(world, location,
								AVerticesGenerator.mirrorVertex(next, 0), teamT2));

						counterTowers += 2;
					}

				}

			}

			if (counterTowers >= NUM_TOWERS || horizontal_segments.isEmpty() == true)
				break;
		}

	}

	private Base createBase(CustomSkeleton skeleton) {
		Base base = new Base(world, location, skeleton);
		all_hitboxes.add(base.global_vertices);

		return base;
	}

	/**
	 * 
	 * @param size
	 * @param numIslands
	 * @param centered
	 * @param fillingIslands
	 */
	private void createIslands(IslandSizes size, int numIslands, boolean centered,
			boolean fillingIslands) {

		APositioningCheck positioning = new APositioningCheck() {
			@Override
			Vector2 getRandomPoint() {
				Vector2 randomPoint = new Vector2();
				randomPoint.x = my_random.random(MAP_WIDTH / 2);
				randomPoint.y = my_random.random(MAP_HEIGHT) - MAP_HEIGHT / 2;
				return randomPoint;
			}
		};
		RandomIsland island;
		for (int i = 0; i < numIslands;) {
			if (running == false)
				break;
			Util.sleep(30);
			do {
				do {

				} while (fractal_vertices_gen.generateRaw(size, centered, fillingIslands) == false);
			} while (positioning.check(centered, size) == false);

			all_hitboxes.add(fractal_vertices_gen.getGlobalVertices());

			// save mirrored raw island.
			if (mirror_map == true && centered == false) {
				fractal_vertices_gen.generateMirror(0);
				all_hitboxes.add(fractal_vertices_gen.getGlobalVertices());
				fractal_vertices_gen.generateMirror(0);
			}

			// smooth island
			fractal_vertices_gen.generateFinalized();

			if (running == false)
				break;

			island = new RandomIsland(world, fractal_vertices_gen);

			// save horizontal segments for shrines and towers
			horizontal_segments.saveSegments(Util.convertVector2ArrayIntoVector2ArrayList(
					fractal_vertices_gen.getGlobalVertices()));

			islands.add(island);

			if (mirror_map == true && centered == false) {
				i++;
				fractal_vertices_gen.generateMirror(0);

				island = new RandomIsland(world, fractal_vertices_gen);
				islands.add(island);
			}

			i++;

		}
	}

	// TO DELETE

	private void dispose() {
		disposeRandom();
		all_hitboxes.clear();
	}

	public void disposeRandom() {
		my_random.dispose();
	}

	private void fillBuildingsStructure() {
		buildings = new LimitedHashMap<Integer, ABuilding>(2);// +
																// shrines.size());

		int index = 0;

		setBaseID(index, base_left);
		index++;
		setBaseID(index, base_right);
		index++;

		for (Tower tower : towers) {
			buildings.put(index, tower);
			tower.setID(index);
			index++;
		}

		// for (Shrine shrine : shrines) {
		// buildings.put(index, shrine);
		// shrine.setID(index);
		// index++;
		// }
	}

	// UNTIL HERE

	public Base getBaseLeft() {
		return base_left;
	}

	public Base getBaseRight() {
		return base_right;
	}

	public LimitedHashMap<Integer, ABuilding> getBuildings() {
		return buildings;
	}

	@Override
	public int getCurrentState() {
		if (islands == null)
			return 0;
		return islands.size();
	}

	@Override
	public int getEndState() {
		return NUM_ISLANDS_BIG + NUM_ISLANDS_SMALL + NUM_ISLANDS_SMALL_FILLING;
	}

	public ArrayList<RandomIsland> getIslands() {
		return islands;
	}

	public MapBorder getMapBorder() {
		return map_border;
	}

	/**
	 * Returns all shrines
	 * 
	 * @return
	 */
	public ArrayList<Shrine> getShrines() {
		return shrines;
	}

	/**
	 * Returns all towers (base-towers and regular towers) as one list
	 * 
	 * @return
	 */
	public ArrayList<Tower> getTowers() {
		return towers;
	}

	/**
	 * Checks whether the passed building intersects an island or one of the
	 * bases. For this check we can't use {@link #all_hitboxes} because that
	 * would be too inaccurate.
	 * 
	 * @param building
	 * @return <code>true</code> if passed building intersects with an island or
	 *         a base
	 */
	private boolean isBuildingTouchingIslandsOrBases(ABuilding building) {

		// islands
		for (RandomIsland island : islands) {
			Array<Vector2> globalVerticesIsland = new Array<Vector2>(island.global_vertices);
			for (Vector2 vertex : building.getCollisionRectangle())
				if (Intersector.isPointInPolygon(globalVerticesIsland, vertex)) {
					building.destroy();
					return true;
				}
		}

		// base left
		Array<Vector2> base1Vertices = new Array<Vector2>(base_left.global_vertices);
		for (Vector2 vertex : building.getCollisionRectangle())
			if (Intersector.isPointInPolygon(base1Vertices, vertex)) {
				building.destroy();
				return true;
			}

		// base right
		Array<Vector2> base2Vertices = new Array<Vector2>(base_right.global_vertices);
		for (Vector2 vertex : building.getCollisionRectangle())
			if (Intersector.isPointInPolygon(base2Vertices, vertex)) {
				building.destroy();
				return true;
			}
		return false;
	}

	@Override
	public boolean isCompleted() {
		return this.getState() == Thread.State.TERMINATED;
	}

	/**
	 * Checks whether the new island (stored in {@link #fractal_vertices_gen}
	 * .getGlobals()) is valid.
	 * 
	 * @param centered
	 * @param islandSize
	 * @return
	 */
	private boolean isPositionInvalid(boolean centered, IslandSizes islandSize) {

		// check whether each vertex of already existing hitbox is in new
		// island
		Array<Vector2> newIslandPoly = new Array<Vector2>(fractal_vertices_gen.getGlobalVertices());
		for (Vector2[] alreadyExistingHitbox : all_hitboxes)
			for (Vector2 vertex : alreadyExistingHitbox)
				if (Intersector.isPointInPolygon(newIslandPoly, vertex)) {
					newIslandColliding++;
					return true;
				}

		// check whether each vertex of the new Island is in a existing island
		Array<Vector2> existingIslandPoly;
		for (Vector2 newIslandVertex : fractal_vertices_gen.getGlobalVerticesAsList()) {
			for (Vector2[] alreadyExistingHitbox : all_hitboxes) {
				existingIslandPoly = new Array<Vector2>(alreadyExistingHitbox);
				if (Intersector.isPointInPolygon(existingIslandPoly, newIslandVertex)) {
					newIslandColliding++;
					return true;
				}
			}
		}
		Polygon newPolygon = null;
		if (fractal_vertices_gen.getGlobalVerticesAsList().size() >= 3)
			newPolygon = new Polygon(Util
					.convertVector2ArrayIntoFloatArray(fractal_vertices_gen.getGlobalVertices()));
		else
			return true;

		// check whether line-polygon intersections
		for (Vector2[] alreadyExistingHitbox : all_hitboxes) {
			Vector2 startPoint;
			Vector2 endPoint;
			for (int i = 1; i < alreadyExistingHitbox.length; i++) {
				startPoint = alreadyExistingHitbox[i - 1];
				endPoint = alreadyExistingHitbox[i];
				if (Intersector.intersectSegmentPolygon(startPoint, endPoint, newPolygon)) {
					newIslandColliding++;
					return true;
				}
			}

			if (Intersector.intersectSegmentPolygon(
					alreadyExistingHitbox[alreadyExistingHitbox.length - 1],
					alreadyExistingHitbox[0], newPolygon)) {
				newIslandColliding++;
				return true;
			}

		}

		// nun die minimalste Distanz zwischen 2 Vertices berechnen;
		float minDistance = Float.MAX_VALUE;
		Vector2 platformPoint1, platformPoint2;
		for (int j = 1; j <= fractal_vertices_gen.getGlobalVertices().length; j++) {
			// define platform
			platformPoint1 = fractal_vertices_gen.getGlobalVertices()[j - 1];
			if (j == fractal_vertices_gen.getGlobalVertices().length)
				platformPoint2 = fractal_vertices_gen.getGlobalVertices()[0];
			else
				platformPoint2 = fractal_vertices_gen.getGlobalVertices()[j];

			// iterate through all existing islands (their unsmoothed version)
			for (Vector2[] alreadyExistingHitbox : all_hitboxes) {
				minDistance = getMinDistanceBetweenPlatformAndIsland(alreadyExistingHitbox,
						platformPoint1, platformPoint2);
				if (minDistance < MIN_DISTANCE_MAPELEMENTS) {
					underMinimalDistance++;
					return true;
				}
			}
		}

		// if mirrorMap and not centric, the island isn't allowed to be in the
		// middle of the map
		if (mirror_map == true && centered == false) {
			// boundingBox lies in the left side of the map
			if (newPolygon.getBoundingRectangle().x < MIN_DISTANCE_MAPELEMENTS * 0.5f) {
				mirrorHitMiddleLine++;
				return true;
			}
		}

		/*
		 * islands aren't allowed to be outside of the map dimensions. otherwise
		 * they would potentially stretch the map
		 */
		// if (islandSize == IslandSizes.BIG)
		if (newPolygon.getBoundingRectangle().x
				+ newPolygon.getBoundingRectangle().width > MAP_WIDTH * 0.5f
				|| newPolygon.getBoundingRectangle().y < -MAP_HEIGHT * 0.5f
				|| newPolygon.getBoundingRectangle().y
						+ newPolygon.getBoundingRectangle().height > MAP_HEIGHT * 0.5f) {
			crossesMapDimensions++;
			return true;
		}

		return false;
	}

	/**
	 * 
	 * @return
	 */
	public String printVariables() {
		return "SEED = " + seed + "\n" + "newIslandColliding = " + newIslandColliding + "\n"
				+ "underMinimalDistance = " + underMinimalDistance + "\n" + "mirrorHitMiddleLine = "
				+ mirrorHitMiddleLine + "\n" + "crossesMapDimensions(stretchingMap) = "
				+ crossesMapDimensions + "\n"
				+ "fractal_vertices_gen.eliminateInternalLoopsCount = "
				+ fractal_vertices_gen.eliminateInternalLoopsCount + "\n"
				+ "fractal_vertices_gen.mergeMoreIntersectionsThanTwo = "
				+ fractal_vertices_gen.mergeMoreIntersectionsThanTwo + "\n"
				+ "fractal_vertices_gen.notEnoughVerticesByBigIslands = "
				+ fractal_vertices_gen.notEnoughVerticesByBigIslands + "\n"
				+ "fractal_vertices_gen.notEnoughVerticesBySmallIslands = "
				+ fractal_vertices_gen.notEnoughVerticesBySmallIslands + "\n";
	}

	public String printVariables2() {
		return "newIslandColliding = " + newIslandColliding + "\n" + "underMinimalDistance = "
				+ underMinimalDistance + "\n" + "mirrorHitMiddleLine = " + mirrorHitMiddleLine
				+ "\n" + "crossesMapDimensions = " + crossesMapDimensions;
	}

	public MyRandom getRandom() {
		return my_random;
	}

	private void setBaseID(int index, Base base) {
		buildings.put(index, base);
		base.setID(index);
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public boolean hasFailed() {
		return failed;
	}

	/**
	 * x = left (negative value), y = right (positive), z = top, w = bottom
	 * 
	 * @return
	 */
	public Vector4 getInnerBound() {
		return map_border_gen.getSamplingBound();
	}

	/**
	 * x = width, y = height
	 * 
	 * @return
	 */
	public Vector2 getOuterBound() {
		return map_border_gen.getOuterBound();
	}

	public ArrayList<LightCluster> getLightCluster() {
		return light_clusters;
	}

}