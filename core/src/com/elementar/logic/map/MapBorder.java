package com.elementar.logic.map;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.elementar.logic.map.generator.MapBorderVerticesGenerator;
import com.elementar.logic.map.util.Vector4;
import com.elementar.logic.util.Util;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.World;

public class MapBorder extends AGeneratedMapElement {

	/**
	 * x = left
	 * <p>
	 * y = right
	 * <p>
	 * z = top
	 * <p>
	 * w = bottom
	 */
	public Vector4 sampling;

	private MapBorderVerticesGenerator vertices_generator;

	public MapBorder(World world, MapBorderVerticesGenerator verticesGenerator) {
		super(world, verticesGenerator);
		this.vertices_generator = verticesGenerator;
		sampling = verticesGenerator.getSamplingBound();
	}

	public Vector2[] getLeftFillingVertices() {
		return Util.convertVector2ArrayListIntoVector2Array(vertices_generator.getLeftFillingVertices());
	}

	public Vector2[] getRightFillingVertices() {
		return Util.convertVector2ArrayListIntoVector2Array(vertices_generator.getRightFillingVertices());
	}

	public Vector2[] getLeftFillingMinimapVertices() {
		return Util.convertVector2ArrayListIntoVector2Array(vertices_generator.getLeftFillingMinimapVertices());
	}

	public Vector2[] getRightFillingMinimapVertices() {
		return Util.convertVector2ArrayListIntoVector2Array(vertices_generator.getRightFillingMinimapVertices());
	}

	@Override
	protected void initPhysics(float x, float y) {
		body_def.type = BodyType.StaticBody;
		body_def.allowSleep = false;
		body_def.position.set(start_point);

		ChainShape chain_shape = new ChainShape();

		chain_shape.createLoop(local_vertices);
		fixture_def.shape = chain_shape;
		fixture_def.friction = 0f;

		body = world.createBody(body_def);
		main_fixture = body.createFixture(fixture_def);

		main_fixture.setFilterData(getCollisionFilter());

		chain_shape.dispose();
	}

	public MapBorderVerticesGenerator getGen() {
		return vertices_generator;
	}

}
