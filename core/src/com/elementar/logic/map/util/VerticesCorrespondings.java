package com.elementar.logic.map.util;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

/**
 * Help class. This class is needed to assign a vertices array to 1. its aabb to
 * avoid double calculation for the aabb or to 2. a vertex, which is in current
 * case a sampled map border point to figure out which sample point belongs to
 * which island.
 * 
 * @author lukassongajlo
 * 
 */
public class VerticesCorrespondings {

	public Vector2[] vertices;
	/**
	 * Note that the aabb must be in form of a Polygon, because of
	 * {@link Intersector} class.
	 */
	public Polygon aabb;

	public Vector2 vertex;

	/**
	 * Useful to have for each vertices array a corresponding aabb
	 * 
	 * @param vertices
	 * @param aabb
	 */
	public VerticesCorrespondings(Vector2[] vertices, Polygon aabb) {
		this(vertices);
		this.aabb = aabb;
	}

	/**
	 * Useful to assign each vertices array (e.g. island) to a corresponding
	 * vertex. This vertex can be a sampled point when calculating the map
	 * border.
	 * 
	 * @param vertices
	 * @param aabb
	 * @param vertex
	 */
	public VerticesCorrespondings(Vector2[] vertices, Vector2 vertex) {
		this(vertices);
		this.vertex = vertex;
	}

	/**
	 * Common constructor for both public constructors
	 * 
	 * @param vertices
	 */
	private VerticesCorrespondings(Vector2[] vertices) {
		this.vertices = vertices;
	}

	@Override
	public String toString() {
		return "" + vertex;
	}
}
