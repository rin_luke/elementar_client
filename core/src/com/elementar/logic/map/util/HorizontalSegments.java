package com.elementar.logic.map.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.util.MyRandom;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.elementar.logic.util.lists.NotNullArrayList;

/**
 * This class holds all available horizontal platforms and those which are
 * already in use.
 * 
 * @author lukassongajlo
 * 
 */
public class HorizontalSegments {

	private NotNullArrayList<Segment> available_segments_copy = new NotNullArrayList<>();
	private NotNullArrayList<Segment> available_segments = new NotNullArrayList<>();
	private NotNullArrayList<Segment> used_segments_towers = new NotNullArrayList<>(),
			used_segments_shrines = new NotNullArrayList<>();

	private final float MIN_DISTANCE_BETWEEN_DIFFERENT_KINDS = 8f;
	private final float MIN_DISTANCE_BETWEEN_SAME_KINDS = 8f;

	private final float ANGLE_TOLERANCE = 22f;

	private float tower_x_bound, tower_y_bound;

	public HorizontalSegments() {
		tower_x_bound = (Shared.MAP_WIDTH * 0.5f) * 0.75f;
		tower_y_bound = (Shared.MAP_HEIGHT * 0.5f);
	}

	/**
	 * This method makes a copy of all available segments
	 */
	public void begin() {

		used_segments_shrines.clear();
		used_segments_towers.clear();

		available_segments_copy.clear();
		for (Segment segment : available_segments)
			available_segments_copy.add(segment);

	}

	/**
	 * This method fills the non-full available list, and shuffle the filled
	 * list to get a solution in the next run
	 */
	public void end(MyRandom myRandom) {
		available_segments.clear();
		for (Segment segment : available_segments_copy)
			available_segments.add(segment);
		Collections.shuffle(available_segments, myRandom.getRandom());

	}

	/**
	 * Returns the next valid position, which has a certain minimal distance to
	 * the used ones. In addition available segments that breaks the rule and
	 * are under the minimal distance will be removed.
	 * 
	 * @param lane
	 * @return
	 */

	public Vector2 nextTower() {
		NotNullArrayList<Segment> usedSegmentsSameKind = used_segments_towers;
		NotNullArrayList<Segment> usedSegmentsOtherKind = used_segments_shrines;

		Iterator<Segment> itAvailables = available_segments.iterator();

		Vector2 midPoint;
		Segment next;

		while (itAvailables.hasNext() == true) {
			next = itAvailables.next();
			midPoint = next.getMidPosition();
			itAvailables.remove();

			/*
			 * for towers we have additional constraints
			 */
			// is tower outside the boundary frame?
			if (midPoint.x > tower_x_bound || midPoint.y > tower_y_bound
					|| midPoint.y < -tower_y_bound)
				continue;

			// check distances for different kinds
			if (checkDistances(midPoint, usedSegmentsOtherKind,
					MIN_DISTANCE_BETWEEN_DIFFERENT_KINDS) == false)
				continue;

			// check distances for same kinds
			if (checkDistances(midPoint, usedSegmentsSameKind,
					MIN_DISTANCE_BETWEEN_SAME_KINDS) == false)
				continue;

			usedSegmentsSameKind.add(next);
			return midPoint;
		}

		return null;

	}

	/**
	 * Returns the next valid position, which has a certain minimal distance to
	 * the used ones. In addition available segments that breaks the rule and
	 * are under the minimal distance will be removed.
	 * 
	 * @param buildingType
	 * @return
	 */
	public Vector2 nextShrine() {

		NotNullArrayList<Segment> usedSegmentsSameKind = used_segments_shrines;
		NotNullArrayList<Segment> usedSegmentsOtherKind = used_segments_towers;

		Iterator<Segment> itAvailables = available_segments.iterator();

		Vector2 midPoint;
		Segment next;

		while (itAvailables.hasNext() == true) {
			next = itAvailables.next();
			itAvailables.remove();
			midPoint = next.getMidPosition();

			// check distances for same kinds
			if (checkDistances(midPoint, usedSegmentsSameKind,
					MIN_DISTANCE_BETWEEN_SAME_KINDS) == false)
				continue;

			// check distances for different kinds
			if (checkDistances(midPoint, usedSegmentsOtherKind,
					MIN_DISTANCE_BETWEEN_DIFFERENT_KINDS) == false)
				continue;

			usedSegmentsSameKind.add(next);
			return midPoint;
		}

		return null;

	}

	private boolean checkDistances(Vector2 midPoint, NotNullArrayList<Segment> usedSegments,
			float threshold) {

		for (Segment usedSegment : usedSegments)
			if (midPoint.dst(usedSegment.getMidPosition()) < threshold)
				return false;

		return true;
	}

	/**
	 * 
	 * @param copyLocals
	 */
	public void saveSegments(ArrayList<Vector2> copyLocals) {
		Vector2 segmentEnd;
		Vector2 segmentStart;
		Vector2 segment;
		float minX;
		// for half min distance we choose the bigger value to avoid closeness
		// in the middle
		float halfMinDistance = MIN_DISTANCE_BETWEEN_DIFFERENT_KINDS * 0.5f;
		for (int k = 1; k <= copyLocals.size(); k++) {
			segmentStart = copyLocals.get(k - 1);
			if (k == copyLocals.size())
				segmentEnd = copyLocals.get(0);
			else
				segmentEnd = copyLocals.get(k);
			minX = Math.min(segmentStart.x, segmentEnd.x);
			/*
			 * minX must be greater than half the min distance, so mirroring
			 * will be applied correctly.
			 */
			if (minX > halfMinDistance) {

				segment = Util.subVectors(segmentEnd, segmentStart);
				if (segment.angle() < ANGLE_TOLERANCE || segment.angle() > 360f - ANGLE_TOLERANCE) {
					/*
					 * TODO length might lead to maps where no platforms can be
					 * found
					 */
					// if (segment.len() > 0.4f) {
					available_segments.add(new Segment(segmentStart, segmentEnd));
					// }
				}
			}
		}

	}

	public boolean isEmpty() {
		return available_segments.size() == 0;
	}

	private class Segment {
		private Vector2 segment_start, segment_end;

		public Segment(Vector2 segmentStart, Vector2 segmentEnd) {
			this.segment_start = segmentStart;
			this.segment_end = segmentEnd;
		}

		/**
		 * Returns midPosition of the segment.
		 * 
		 * @return
		 */
		public Vector2 getMidPosition() {
			return Util.addVectors(segment_start, Util.subVectors(segment_end, segment_start),
					0.5f);
		}
	}

}
