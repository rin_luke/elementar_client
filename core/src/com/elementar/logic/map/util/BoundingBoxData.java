package com.elementar.logic.map.util;

import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.spine.Bone;
import com.esotericsoftware.spine.attachments.BoundingBoxAttachment;

/**
 * This class stores each bounding box given from Spine and the corresponding
 * name of it. The name is needed to define on which bounding box should be
 * placed vegetation.
 * <p>
 * The global hitbox is needed for vegetation placement
 * 
 * @author lukassongajlo
 * 
 */
public class BoundingBoxData {

	// for initiation, and rotation:
	protected Bone bone;
	protected BoundingBoxAttachment bounding_box_attachment;

	// for creating hitbox, place vegetation ...
	protected Vector2[] local_hitbox, global_hitbox;
	protected String name;

	/**
	 * 
	 * @param bone
	 * @param boundingBoxAttachment
	 */
	public BoundingBoxData(Bone bone,
			BoundingBoxAttachment boundingBoxAttachment) {
		this.bone = bone;
		this.bounding_box_attachment = boundingBoxAttachment;
		this.name = boundingBoxAttachment.getName();
	}

	public Bone getBone() {
		return bone;
	}

	public BoundingBoxAttachment getBoundingBoxAttachment() {
		return bounding_box_attachment;
	}

	public Vector2[] getLocalHitbox() {
		return local_hitbox;
	}

	public Vector2[] getGlobalHitbox() {
		return global_hitbox;
	}

	/**
	 * Returns the name of the bounding box
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	public void setLocalHitbox(Vector2[] vertices) {
		local_hitbox = vertices;
	}

	public void setGlobalHitbox(Vector2[] vertices) {
		global_hitbox = vertices;
	}
}
