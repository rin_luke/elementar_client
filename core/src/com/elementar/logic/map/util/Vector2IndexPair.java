package com.elementar.logic.map.util;

import com.badlogic.gdx.math.Vector2;

/**
 * Holds an index and the corresponding vector of a vertices list
 * 
 * @author lukassongajlo
 * 
 */
public class Vector2IndexPair {

	public int index;
	public Vector2 vertex;

	public Vector2IndexPair(Vector2 vertex, int index) {
		this.vertex = vertex;
		this.index = index;
	}

	@Override
	public String toString() {
		return "index = " + index + ", vertex = " + vertex;
	}
}