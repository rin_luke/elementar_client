package com.elementar.logic.map;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.elementar.logic.map.generator.AVerticesGenerator;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.World;

public class RandomIsland extends AGeneratedMapElement {

	/**
	 * 
	 * @param world
	 * @param island
	 */
	public RandomIsland(World world, RandomIsland island) {
		super(world, island);

	}

	/**
	 * @param world
	 * @param verticesGenerator
	 */
	public RandomIsland(World world, AVerticesGenerator verticesGenerator) {
		super(world, verticesGenerator);
	}

	@Override
	protected void initPhysics(float x, float y) {
		body_def.type = BodyType.StaticBody;
		body_def.allowSleep = false;

		body_def.position.set(start_point);

		ChainShape chainShape = new ChainShape();

		chainShape.createLoop(local_vertices);
		fixture_def.shape = chainShape;
		fixture_def.friction = 0f;
		fixture_def.restitution = 0f;

		body = world.createBody(body_def);
		main_fixture = body.createFixture(fixture_def);
		main_fixture.setFilterData(getCollisionFilter());

		chainShape.dispose();
	}
}
