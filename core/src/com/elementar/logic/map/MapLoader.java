package com.elementar.logic.map;

import com.badlogic.gdx.physics.box2d.World;
import com.elementar.gui.abstracts.Loadable;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;

/**
 * Thread-based
 * 
 * @author lukassongajlo
 * 
 */
public class MapLoader extends Thread implements Loadable {

	public static long SEED;

	private MapManager map_manager;

	/**
	 * just for debug and measure time
	 */
	private long start_time_generation;

	private Location location;

	public MapLoader(Location location) {
		this.location = location;
	}

	/**
	 * This method can just be call once. To use this method again you have to call
	 * {@link #stopLoading()} before or just reset your {@link #MapLoader()} -object
	 * ( with " = new Maploader() ").
	 * 
	 * @param world
	 * @param seed
	 */
	public void startLoadingOnce(World world, long seed) {

		if (getState() != State.NEW)
			return;

		boolean mirrorMap = Shared.DEBUG_MODE_MIRROR_MAP;
		// manager holds seed and map-border
		map_manager = new MapManager(world, location, mirrorMap, seed);

		SEED = seed;

		start_time_generation = System.currentTimeMillis();

		map_manager.start();

		start();

	}

	@Override
	public void run() {
		while (isCompleted() == false)
			Util.sleep(300);

		// PRINT
		System.out.println("MapLoader.run()  PRINT AUSGABE: ");
		System.out
				.println("Benötigte Zeit = " + ((System.currentTimeMillis() - start_time_generation) / 1000f) + " sec");
		System.out.println("MapLoader.run()  PRINT AUSGABE ENDE.");

	}

	public synchronized void stopLoading() {
		if (map_manager != null) {
			map_manager.setRunning(false);

			do {
			} while (map_manager.getState() != Thread.State.TERMINATED && map_manager.getState() != Thread.State.NEW);
			map_manager = null;

		}

	}

	public synchronized boolean hasFailed() {
		if (map_manager != null)
			return map_manager.hasFailed();
		return true;
	}

	public MapManager getMapManager() {
		return map_manager;
	}

	/**
	 * convenience method
	 * 
	 * @return
	 */
	public MapBorder getMapBorder() {
		return map_manager.getMapBorder();
	}

	@Override
	public int getEndState() {
		return map_manager.getEndState();
	}

	@Override
	public int getCurrentState() {
		return map_manager.getCurrentState();
	}

	@Override
	public boolean isCompleted() {
		// avoid crash in thread loop after stopProcess() is called.
		if (map_manager == null)
			return true;
		return map_manager.isCompleted() && map_manager.hasFailed() == false;
	}
}