package com.elementar.logic.characters;

import com.badlogic.gdx.physics.box2d.World;
import com.elementar.logic.characters.PhysicalClient.Location;

/**
 * Omni client holds all informations about physical behaviour, draw stuff and
 * meta data like gameclass
 * 
 * @author lukassongajlo
 * 
 */
public class OmniClient {

	private static short ID_TRAINING = 0;

	/**
	 * Holds {@link MetaClient} and {@link PhysicalClient}
	 */
	private PhysicalClient physical_client, para_physical_client;
	private SparseClient prev_client;
	private DrawableClient drawable_client;

	/**
	 * by default equals <code>false</code>
	 */
	private boolean is_dummy = false;

	/**
	 * Constructor for world characters
	 * 
	 * @param metaClient
	 * @param isDummy
	 * @param location
	 */
	public OmniClient(MetaClient metaClient, boolean isDummy, Location location) {
		prev_client = new SparseClient();
		para_physical_client = new PhysicalClient(metaClient, location);
		para_physical_client.setPredictionVerifier();
		physical_client = new PhysicalClient(metaClient, location);
		drawable_client = new DrawableClient(metaClient);
		if (location == Location.CLIENTSIDE_TRAINING) {
			physical_client.connection_id = drawable_client.connection_id = metaClient.connection_id = physical_client
					.getSparseClient().connection_id = ID_TRAINING;
			ID_TRAINING++;
		}
		is_dummy = isDummy;
	}

	/**
	 * Note that {@link #physical_client}'s metaClient is equal to
	 * {@link #drawable_client}'s metaClient
	 * 
	 * @return
	 */
	public MetaClient getMetaClient() {
		return physical_client.getMetaClient();
	}

	public PhysicalClient getPhysicalClient() {
		return physical_client;
	}

	public DrawableClient getDrawableClient() {
		return drawable_client;
	}

	public boolean isDummy() {
		return is_dummy;
	}

	/**
	 * Creates a hitbox and calls afterwards {@link #updateByMetaData()}
	 * 
	 * @param world     , for movement, vegetation, skills, client-prediction.
	 * @param paraWorld , for lights and client-prediction-correction
	 */
	public void init(World world, World paraWorld) {

		physical_client.setHitbox(world);
		para_physical_client.setHitbox(paraWorld);
		updateByMetaData();

	}

	public void destroyHitboxes() {
		if (physical_client != null && physical_client.getHitbox() != null)
			physical_client.getHitbox().destroy();
		if (para_physical_client != null && para_physical_client.getHitbox() != null)
			para_physical_client.getHitbox().destroy();

	}

	/**
	 * if the gameclass, skin index or team of the {@link MetaClient} object has
	 * changed, then {@link #physical_client} and {@link #drawable_client} must
	 * update
	 */
	private void updateByMetaData() {
		drawable_client.updateByMetaData(false);
		physical_client.updateByMetaData(physical_client.getHitbox(), drawable_client);

		// set 'pos' fields to avoid interpolating at the beginning
		drawable_client.pos.set(physical_client.getHitbox().getPosition());
		physical_client.pos.set(physical_client.getHitbox().getPosition());
		prev_client.pos.set(physical_client.getHitbox().getPosition());

		// might be null for gameclass scroller
		if (para_physical_client != null)
			para_physical_client.updateByMetaData(para_physical_client.getHitbox(), drawable_client);
	}

	public void setPosition(float x, float y) {
		drawable_client.setPosition(x, y);
		physical_client.setPosition(x, y);
	}

	public SparseClient getPrevClient() {
		return prev_client;
	}

	public PhysicalClient getParaPhysicalClient() {
		return para_physical_client;
	}

}
