package com.elementar.logic.characters;

import java.util.ArrayList;

import com.elementar.logic.network.requests.InputRequest;
import com.elementar.logic.network.response.InfoEmissionInterruption;
import com.elementar.logic.network.util.ReliabilitySystem;
import com.elementar.logic.util.maps.LIFOLimitedHashMap;

public class ServerPhysicalClient extends PhysicalClient {

	private ReliabilitySystem reliability_system = new ReliabilitySystem();

	/**
	 * necessary to check whether incoming {@link InputRequest} is new
	 */
	private LIFOLimitedHashMap<Integer, InputRequest> received_inputs;

	private ArrayList<InfoEmissionInterruption> emission_interruptions = new ArrayList<>(30);

	private long ping_start_time;

	private long ping_end_time;

	public ServerPhysicalClient(MetaClient metaClient) {
		super(metaClient, Location.SERVERSIDE);
		received_inputs = new LIFOLimitedHashMap<Integer, InputRequest>(100);
	}

	public ReliabilitySystem getReliabilitySystem() {
		return reliability_system;
	}

	public void setPingStart() {
		ping_start_time = System.currentTimeMillis();
	}

	public void setPingEnd() {
		ping_end_time = System.currentTimeMillis();
	}

	public short getPing() {
		return (short) (ping_end_time - ping_start_time);
	}

	@Override
	public void addInput(InputRequest req) {
		/*
		 * it is possible >1 incoming packets have the same message id, because of a
		 * lag. We want to process an input request only once, so we have to check
		 * whether the packet already got received.
		 */

		if (received_inputs.put(req.message_id, req) == null) {
			req.arrival_time = System.currentTimeMillis();
			super.addInput(req);
		}
	}

	/**
	 * 
	 * @param interruption
	 */
	public void addEmissionInterruption(InfoEmissionInterruption interruption) {
		emission_interruptions.add(interruption);
	}

	public ArrayList<InfoEmissionInterruption> getEmissionInterruptions() {
		return emission_interruptions;
	}

}