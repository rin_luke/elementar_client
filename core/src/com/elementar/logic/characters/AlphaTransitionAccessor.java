package com.elementar.logic.characters;

import aurelienribon.tweenengine.TweenAccessor;

public class AlphaTransitionAccessor implements TweenAccessor<AlphaTransition> {

	public static final int FADE_IN = 0;
	public static final int FADE_OUT = 1;

	@Override
	public int getValues(AlphaTransition alpha_transition, int tweenType, float[] returnValues) {

		switch (tweenType) {
		case FADE_IN:
		case FADE_OUT:
			returnValues[0] = alpha_transition.getValue();
			return 1;
		}

		return 0;
	}

	@Override
	public void setValues(AlphaTransition target, int tweenType, float[] newValues) {

		switch (tweenType) {
		case FADE_IN:
		case FADE_OUT:
			target.setValue(newValues[0]);
		}

	}
}