package com.elementar.logic.characters.util;

import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.AudioData;
import com.elementar.data.container.util.TweenableSound;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.util.Shared;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.Skeleton;

public class Landing {

	private PhysicalClient physical_client;

	private float elapsed_time;

	private float animation_duration_normal, animation_duration_stunned;

	private boolean flag;
	private boolean active;
	private TweenableSound landing_sound;

	public Landing(PhysicalClient physicalClient) {

		Skeleton tempSkeleton = AnimationContainer.makeCharacterAnimationForWorld();

		// set heavy duration
		Animation a = tempSkeleton.getData().findAnimation("character_land");
		animation_duration_normal = a.getDuration();

		// set stunned duration
		a = tempSkeleton.getData().findAnimation("character_land_stunned");
		animation_duration_stunned = a.getDuration();

		physical_client = physicalClient;
		landing_sound = new TweenableSound(AudioData.global_landing, true);
	}

	public void start() {
		elapsed_time = 0;
		flag = true;

		// sound effect
		if (physical_client.getLocation() != Location.SERVERSIDE) {
			// if (LogicTier.VIEW_HANDLER.isInside(center_pos) == true)
			landing_sound.playWithoutTween();
		}

	}

	public void updateSounds(float x, float y) {
		landing_sound.updateVolume(x, y);
	}

	public void update() {
		elapsed_time += Shared.SEND_AND_UPDATE_RATE_WORLD;

		float animationDuration = physical_client.isStunned() == true ? animation_duration_stunned
				: animation_duration_normal;

		if (elapsed_time < animationDuration && flag == true) {
			active = true;
			if (physical_client.isStunned() == true)
				physical_client.info_animations.track_2 = AnimationName.LAND_STUNNED;
			else
				physical_client.info_animations.track_2 = AnimationName.LAND;
		} else
			active = false;

	}

	public boolean isActive() {
		return active;
	}

	/**
	 * Interrupts landing process
	 */
	public void interrupt() {
		flag = false;
	}
}
