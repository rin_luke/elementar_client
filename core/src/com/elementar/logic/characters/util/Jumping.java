package com.elementar.logic.characters.util;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.elementar.data.container.AudioData;
import com.elementar.data.container.util.TweenableSound;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.util.Shared;

/**
 * This class handles player's jumping. After each
 * {@link #start(Body, boolean, Vector2)} call the jump begins and lasts as long
 * player pressed 'space' or the elapsed time since starting the jump is lower
 * than a threshold.
 * <p>
 * In addition, by contacting the head-contacts, jumping gets inactive. The same
 * for the second jump, but there is no break option with releasing SPACE.
 * <p>
 * The jump has to last at least #MINIMAL_TIME
 * 
 * @author lukassongajlo
 * 
 */
public class Jumping {
	/**
	 * If keyPressed and still in range of valid jump-time, the player is jumping,
	 * regardless any collisions (except head contacts). It will return
	 * <code>true</code> in this case, otherwise false;
	 */
	public boolean jump_active;
	public boolean is_first_jump, is_slide_jump;
	public long jump_elapsed_time_ms, jump_max_time_ms;

	/**
	 * in ms
	 */
	private static final long MAX_TIME_FIRSTJUMP = 450, MAX_TIME_SECONDJUMP = 210, MIN_TIME = 150;

	private PhysicalClient physical_client;
	private Body body;
	private Landing landing;
	private TweenableSound sound_firstjump, sound_secondjump, sound_ceilingjump;

	/**
	 * 
	 * @param physicalClient
	 * @param landing        , not allowed to be <code>null</code>
	 */
	public Jumping(PhysicalClient physicalClient, Landing landing) {
		this.physical_client = physicalClient;
		this.landing = landing;
		this.body = physicalClient.getHitbox().getBody();
		this.sound_firstjump = new TweenableSound(AudioData.global_jump, true);
		this.sound_secondjump = new TweenableSound(AudioData.global_doublejump, true);
		this.sound_ceilingjump = new TweenableSound(AudioData.global_climbing, true);
	}

	/**
	 * first jump:
	 * <p>
	 * if elapsed time is under a threshold AND space is pressed AND the jumping
	 * process is not externally interrupted ({@link #jump_active} != false) by
	 * contacting head with platform, then the jumping process can move on.
	 * <p>
	 * second jump:
	 * <p>
	 * Same as first jump but no break option and less duration
	 * <p>
	 * NOTE: Both jumps have a minimum time duration.
	 * 
	 * @param body
	 * @param keyPressed
	 */
	public void update(boolean keyPressed) {
		jump_elapsed_time_ms += Shared.SEND_AND_UPDATE_RATE_IN_MS;
		if (jump_active == true) {
			if (is_first_jump == true) {

				if ((jump_elapsed_time_ms < jump_max_time_ms && keyPressed == true
						|| jump_elapsed_time_ms < MIN_TIME)) {
					body.setGravityScale(0);

					physical_client.info_animations.track_0 = is_slide_jump == true ? AnimationName.JUMP_SLIDE
							: AnimationName.JUMP_FIRST;
				} else {
					// short jump, higher gravity!
					if (jump_elapsed_time_ms < MAX_TIME_FIRSTJUMP * 0.5f)
						body.setGravityScale(2f);
					else
						// long jump, regular gravity
						body.setGravityScale(1f);
					is_first_jump = false;
					jump_active = false;
				}
			} else {
				// second jump
				if ((jump_elapsed_time_ms < jump_max_time_ms)) {
					body.setGravityScale(0);
					physical_client.info_animations.track_0 = AnimationName.JUMP_SECOND;
				} else {
					body.setGravityScale(1f);
					jump_active = false;
				}
			}
		} else {
			/*
			 * If the player has chosen the short jump above, the gravity scale has to be
			 * reseted after a certain time. I tried 200ms
			 */
			if (body.getGravityScale() > 1.9f) {
				if (jump_elapsed_time_ms > jump_max_time_ms + 200)
					// reset gravity scale to regular
					body.setGravityScale(1f);
			} else
				body.setGravityScale(1f);

			is_first_jump = false;
		}
	}

	public void updateSounds(float x, float y) {
		sound_firstjump.updateVolume(x, y);
		sound_secondjump.updateVolume(x, y);
		sound_ceilingjump.updateVolume(x, y);
	}

	/**
	 * 
	 * @param body
	 * @param firstJump
	 * @param jumpImpulse , if x component > 0.1f player slides!
	 */
	public void start(boolean firstJump, Vector2 jumpImpulse) {

		landing.interrupt();
		is_first_jump = firstJump;

		is_slide_jump = physical_client.isSliding();

		// set max time
		if (is_first_jump == true)
			jump_max_time_ms = MAX_TIME_FIRSTJUMP;
		else
			jump_max_time_ms = MAX_TIME_SECONDJUMP;

		if (MathUtils.isZero(jumpImpulse.x) == false) {
			/*
			 * velocity compensation of x and y component, happens when player has slided
			 */
			physical_client.applyLinearImpulseToIdle();
			// small shift (0.1f) for overhangs to avoid head collision
			body.setTransform(body.getPosition().x + (0.1f * (jumpImpulse.x < 0 ? -1 : 1)), body.getPosition().y, 0);
		} else
			// velocity compensation just y component
			physical_client.applyLinearImpulse(new Vector2(0, -body.getLinearVelocity().y));

		// jump impulse
		physical_client.applyLinearImpulse(jumpImpulse);
		jump_elapsed_time_ms = 0;
		jump_active = true;

		// sound effect
		if (physical_client.getLocation() != Location.SERVERSIDE) {
			// if (LogicTier.VIEW_HANDLER.isInside(center_pos) == true)
			if (is_slide_jump == true)
				sound_ceilingjump.playWithoutTween();
			else if (is_first_jump == true)
				sound_firstjump.playWithoutTween();
			else
				sound_secondjump.playWithoutTween();

		}

	}

	@Override
	public String toString() {
		return "( isActive = " + jump_active + " | firstJump = " + is_first_jump + " )";
	}

}
