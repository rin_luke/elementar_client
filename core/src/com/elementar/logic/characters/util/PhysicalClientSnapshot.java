package com.elementar.logic.characters.util;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.characters.PhysicalClient;

/**
 * Contains all data of a physical client in a certain time. Useful to setup the
 * server sync client to correct the client-side prediction.
 * 
 * @author lukassongajlo
 *
 */
public class PhysicalClientSnapshot {

	public short current_actions, prev_actions;

	public Vector2 cursor_pos;

	public boolean body_active, rooted, disarmed, silenced, stunned, under_impulse, left_dominant,
			player_can_secondjump, is_holding, brake_enabled, prev_alive;

	public int platform_angle;

	public float elapsed_time, time, elapsed_time_sliding, death_time_current, death_time_absolute, health_regen,
			health_accu, mana_regen, mana_accu, vel_x_factor, gravity_scale, cooldown_skill0, cooldown_skill1,
			cooldown_skill2;

	// jumping variables begin
	public boolean jump_active, is_first_jump, is_slide_jump;
	public long jump_elapsed_time_ms, jump_max_time_ms;
	// jumping variables end

	/**
	 * feed data from client into a snapshot
	 * 
	 * @param client
	 */

	public PhysicalClientSnapshot(PhysicalClient client) {
		cursor_pos = new Vector2(client.cursor_pos);
		current_actions = client.getCurrentActions();
		prev_actions = client.getPrevActions();
		left_dominant = client.isLeftDominant();
		player_can_secondjump = client.getPlayerCanSecondjump();
		is_holding = client.isHolding();
		brake_enabled = client.isBrakeEnabled();
		// jumping variables begin
		jump_active = client.getJumping().jump_active;
		is_first_jump = client.getJumping().is_first_jump;
		is_slide_jump = client.getJumping().is_slide_jump;
		jump_elapsed_time_ms = client.getJumping().jump_elapsed_time_ms;
		jump_max_time_ms = client.getJumping().jump_max_time_ms;
		// jumping variables end
		platform_angle = client.getPlatformAngle();
		elapsed_time = client.getElapsedTime();
		elapsed_time_sliding = client.getElapsedTimeSliding();
		time = client.getTime();
		death_time_current = client.death_time_current;
		death_time_absolute = client.death_time_absolute;
		prev_alive = client.isPrevAlive();
		rooted = client.isRooted();
		disarmed = client.isDisarmed();
		silenced = client.isSilenced();
		under_impulse = client.isUnderImpulse();
		vel_x_factor = client.getVelXFactor();
		stunned = client.isStunned();
		health_regen = client.health_regen;
		health_accu = client.health_accu;
		mana_regen = client.mana_regen;
		mana_accu = client.mana_accu;
		gravity_scale = client.getBody().getGravityScale();
		body_active = client.getBody().isActive();
		cooldown_skill0 = client.cooldown_skill0;
		cooldown_skill1 = client.cooldown_skill1;
		cooldown_skill2 = client.cooldown_skill2;
	}

	/**
	 * copy constructor
	 * 
	 * @param snapshot
	 */
	public PhysicalClientSnapshot(PhysicalClientSnapshot snapshot) {
		cursor_pos = new Vector2(snapshot.cursor_pos);
		current_actions = snapshot.current_actions;
		prev_actions = snapshot.prev_actions;
		left_dominant = snapshot.left_dominant;
		player_can_secondjump = snapshot.player_can_secondjump;
		is_holding = snapshot.is_holding;
		brake_enabled = snapshot.brake_enabled;
		// jumping variables begin
		jump_active = snapshot.jump_active;
		is_first_jump = snapshot.is_first_jump;
		is_slide_jump = snapshot.is_slide_jump;
		jump_elapsed_time_ms = snapshot.jump_elapsed_time_ms;
		jump_max_time_ms = snapshot.jump_max_time_ms;
		// jumping variables end
		platform_angle = snapshot.platform_angle;
		elapsed_time = snapshot.elapsed_time;
		elapsed_time_sliding = snapshot.elapsed_time_sliding;
		time = snapshot.time;
		death_time_current = snapshot.death_time_current;
		death_time_absolute = snapshot.death_time_absolute;
		prev_alive = snapshot.prev_alive;
		rooted = snapshot.rooted;
		disarmed = snapshot.disarmed;
		under_impulse = snapshot.under_impulse;
		vel_x_factor = snapshot.vel_x_factor;
		stunned = snapshot.stunned;
		health_regen = snapshot.health_regen;
		health_accu = snapshot.health_accu;
		mana_regen = snapshot.mana_regen;
		mana_accu = snapshot.mana_accu;
		gravity_scale = snapshot.gravity_scale;
		body_active = snapshot.body_active;
		cooldown_skill0 = snapshot.cooldown_skill0;
		cooldown_skill1 = snapshot.cooldown_skill1;
		cooldown_skill2 = snapshot.cooldown_skill2;

	}

}
