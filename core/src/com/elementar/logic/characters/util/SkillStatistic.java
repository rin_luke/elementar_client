package com.elementar.logic.characters.util;

public class SkillStatistic<T> {

	private String key;
	private T value;
	private String unit;

	public SkillStatistic(String key, T value, String unit) {
		this(key, value);
		this.unit = unit;
	}

	public SkillStatistic(String key, T value) {
		this.key = key;
		this.value = value;
		this.unit = "";
	}

	public String getKey() {
		return key;
	}

	public T getValue() {
		return value;
	}

	public String getUnit() {
		return unit;
	}
}
