package com.elementar.logic.characters;

import com.elementar.data.container.GameclassContainer;
import com.elementar.data.container.util.Gameclass;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.logic.network.gameserver.GameserverLogic.ServerState;
import com.elementar.logic.util.Shared.ClientState;
import com.elementar.logic.util.Shared.Team;

/**
 * Describes the clients metadata, which is needed especially in the lobby.
 * 
 * @author lukassongajlo
 * 
 */
public class MetaClient {

	// data which will not be send frequently
	public short connection_id;
	public String name;

	public int slot_index = -1;
	public GameclassName gameclass_name;
	public transient Gameclass gameclass;
	public int skin_index;
	public Team team = null;
	
	public short ping;

	public String chat_message = "";

	public ClientState client_state;

	public GameclassName pre_selected_gameclass;

	public short health_absolute, mana_absolute;

	/**
	 * Not in use
	 */
	public MetaClient() {

	}

	/**
	 * 
	 * @param gameclassName
	 *            , as enum
	 * @param skinIndex
	 */
	public MetaClient(GameclassName gameclassName, int skinIndex) {
		setGameclass(gameclassName);
		skin_index = skinIndex;
	}

	/**
	 * 
	 * @param name
	 * @param gameclassName
	 * @param skinIndex
	 * @param team
	 */
	public MetaClient(String name, GameclassName gameclassName, int skinIndex, Team team) {
		this(gameclassName, skinIndex);
		this.team = team;
		this.name = name;
	}

	/**
	 * slot and team haven't be assigned yet.
	 * 
	 * @param connectionID
	 * @param name
	 * @param slotIndex
	 */
	public MetaClient(int connectionID, String name) {
		connection_id = (short) connectionID;
		this.name = name;
	}

	/**
	 * After {@link ServerState#POST_MATCH} for resetting the client
	 * 
	 * @param client
	 */
	public MetaClient(MetaClient client) {
		connection_id = client.connection_id;
		name = client.name;
		team = client.team;
		slot_index = client.slot_index;
	}

	public void setGameclass(GameclassName gameclassName) {
		if (gameclassName != null) {
			gameclass_name = gameclassName;
			gameclass = GameclassContainer.filterByName(gameclassName);
		}
	}

}
