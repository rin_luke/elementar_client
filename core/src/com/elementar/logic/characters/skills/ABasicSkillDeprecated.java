package com.elementar.logic.characters.skills;

import java.util.ArrayList;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.util.CollisionData;

/**
 * Basic skills will be initiated in keyPressed instead of onetime-key event
 * like other skills. Their skill index is 0.
 * 
 * @author lukassongajlo
 * 
 */
public abstract class ABasicSkillDeprecated extends AChargeSkill {

	protected DefProjectile projectile_prototype_e1;
	protected AEmissionDef e1, e2;

	/**
	 * 
	 * @param physicalClient
	 * @param unparsedSkinName
	 * @param skillThumbnail
	 */
	public ABasicSkillDeprecated(MetaSkill metaSkill, int p1Version, PhysicalClient physicalClient,
			String unparsedSkinName) {
		super(metaSkill, physicalClient, null, unparsedSkinName);
		setFirerate(3);// just for tests

		projectile_prototype_e1.addSuccessor(e2);
	}

	// @Override
	// public boolean init() {
	//
	// if (physical_client.isEnabledMovement() == true)
	// if (cooldown_current <= 0)
	// if (physical_client.getCurrentSkill() != null) {
	// if (isQueuedSkillThresholdExceeded() == true)
	// physical_client.setQueuedSkill(this);
	// } else
	// return applySkill();
	// // no error message when cooldown hasn't expired yet
	// return false;
	//
	// }

	@Override
	protected void defineEmissions() {
		projectile_prototype_e1 = new DefProjectile(30000,
				getCollisionFilterWithProjGround(
						CollisionData.BIT_GROUND + CollisionData.BIT_BUILDING + CollisionData.BIT_CHARACTER_PROJECTILE))
								.setVelocity(2f)
								// 0f)
								.addSuccessor(getCrumblingStones());
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {

		combo_defs.add(e1);
		combo_defs.add(e2);
	}

	@Override
	protected void defineChargeEmission() {
		// TODO maybe we'll add an emission for basic attacks as well.
	}

	/**
	 * By default the particle effect will be drawn behind
	 * 
	 * @param internalPath
	 * @param radiusMultiplier
	 * @param sound
	 * @return
	 */
	// protected AEmissionDef getCrumblingStonesAtBuildings() {
	//
	// DefProjectile defProjectile = new DefProjectile(2000,
	// getGroundFilter()).setVelocity(0.4f)
	// .setVelocityVariation(0.8f).setGravityScale(0.4f).setTriangleShape(60)
	// .setFriction(0.7f);
	// return new EmissionDefAngleRanged(
	// "graphic/particles/particle_global/global_dirt_explosion1_building.p",
	// 1f, 1f, true,
	// defProjectile, 8, 0, 130, 15, new RandomOrder(), new
	// FixedAlignment(true))
	// .setOffsetToMinimum()
	// .setStartConditions(new StartConditions(CollisionData.BIT_BUILDING,
	// CollisionKinds.CAST_ON_MATE.getValue(),
	// CollisionAddsBuilding.BOTH.getValue()))
	// .setDecorative().setSound(AudioData.dirt_explosion1);
	// }
}
