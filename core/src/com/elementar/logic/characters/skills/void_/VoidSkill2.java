package com.elementar.logic.characters.skills.void_;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.AEffect;
import com.elementar.logic.emission.effect.EffectBlackhole;
import com.elementar.logic.emission.effect.EffectSilence;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class VoidSkill2 extends AMainSkill {

	private final int DELAY_POS_CHANGE = 700;
	private final float MIN_RADIUS = 10, MAX_RADIUS = 20;
	private final int LIFETIME_BUILDUP_PROJECTILE = 1000;

	private AEmissionDef e0_empty, e1_buildup, e2_blackhole_in, e3_blackhole_out, e4_feedback_enemy_in,
			e5_feedback_enemy_out, e6_feedback_ally_in, e7_feedback_ally_out, e8_close_blackhole;

	private EffectBlackhole effect_blackhole_enemy, effect_blackhole_ally;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public VoidSkill2(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/void_skill2_charge_" + skin_name_parsed + ".p", 1f, 3f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		DefProjectile defProjectileE8 = new DefProjectile(1000, getNeutralFilter());

		e8_close_blackhole = new EmissionDefAngleDirected("graphic/particles/particle_skill/void_skill2_e8_empty.p", 1f,
				1f, false, defProjectileE8, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_EMITTER.getValue()))
						.setDestroyPrevProjectileAfterPlayer(true);

		// e7 feedback ally out
		DefProjectile defProjectileE7 = new DefProjectile(2000, getNeutralFilter());

		e7_feedback_ally_out = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill2_e7_" + skin_name_parsed + "_feedback_ally_out.p", 1f, 1f,
				false, defProjectileE7, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_INCLUDED.getValue()))
						.setSound(AudioData.void_skill2_e7).setAttached().setDelayStart(DELAY_POS_CHANGE);

		// e6 feedback ally in
		DefProjectile defProjectileE6 = new DefProjectile(DELAY_POS_CHANGE, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET).addSuccessor(e7_feedback_ally_out);

		e6_feedback_ally_in = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill2_e6_" + skin_name_parsed + "_feedback_ally_in.p", 1f, 1f,
				false, defProjectileE6, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_INCLUDED.getValue()))
						.setAttached().setSound(AudioData.void_skill2_e6);
		short poolIDE6 = e6_feedback_ally_in.getPoolID();
		e6_feedback_ally_in.addEffect(
				effect_blackhole_ally = new EffectBlackhole(poolIDE6, DELAY_POS_CHANGE, MIN_RADIUS, MAX_RADIUS));

		// e5 feedback enemy out
		DefProjectile defProjectileE5 = new DefProjectile(2000, getNeutralFilter());

		e5_feedback_enemy_out = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill2_e5_" + skin_name_parsed + "_feedback_enemy_out.p", 1f, 1f,
				false, defProjectileE5, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setSound(AudioData.void_skill2_e5).setAttached().setDelayStart(DELAY_POS_CHANGE);

		// e4 feedback enemy in
		DefProjectile defProjectileE4 = new DefProjectile(DELAY_POS_CHANGE, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e4_feedback_enemy_in = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill2_e4_" + skin_name_parsed + "_feedback_enemy_in.p", 1f, 1f,
				false, defProjectileE4, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setAttached().setSound(AudioData.void_skill2_e4);
		short poolIDE4 = e4_feedback_enemy_in.getPoolID();
		e4_feedback_enemy_in.addEffect(
				effect_blackhole_enemy = new EffectBlackhole(poolIDE4, DELAY_POS_CHANGE, MIN_RADIUS, MAX_RADIUS))
				.addEffect(new EffectSilence(poolIDE4, meta_skill.getFeedbacks().get(0).getSilenceDuration()));

		// e3, blackhole out
		DefProjectile defProjectileE3 = new DefProjectile(20000, getNeutralFilter());

		e3_blackhole_out = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill2_e3_" + skin_name_parsed + "_blackhole_out.p", .1f, 1.8f,
				true, defProjectileE3, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(0, CollisionKinds.AFTER_LIFETIME.getValue()))
						.setSound(AudioData.void_skill2_e3);

		// e2, blackhole in
		DefProjectile defProjectileE2 = new DefProjectile(20000,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE))
						.addSuccessor(e4_feedback_enemy_in).addSuccessor(e5_feedback_enemy_out)
						.addSuccessor(e6_feedback_ally_in).addSuccessor(e7_feedback_ally_out)
						.addSuccessor(e8_close_blackhole).setFlyThroughTargets();

		e2_blackhole_in = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill2_e2_" + skin_name_parsed + "_blackhole_in.p", 18f, 1.8f,
				true, defProjectileE2, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(0, CollisionKinds.AFTER_LIFETIME.getValue()))
						.setSound(AudioData.void_skill2_e2, true);

		// e1, buildup projectile for blackhole
		DefProjectile defProjectileE1 = new DefProjectile(2000, getNeutralFilter());

		e1_buildup = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill2_e1_" + skin_name_parsed + "_buildup.p", 1f, 1f, true,
				defProjectileE1, 1, 0, new FixedAlignment(0));

		DefProjectile defProjectileE0 = new DefProjectile(LIFETIME_BUILDUP_PROJECTILE, getNeutralFilter())
				.addSuccessor(e2_blackhole_in).addSuccessor(e3_blackhole_out).setFlyThroughTargets();

		e0_empty = new EmissionDefAngleDirected("graphic/particles/particle_skill/void_skill2_e0_empty.p", 1f, 3f,
				false, defProjectileE0, 1, 0, new FixedAlignment(0)).addSuccessorTimewise(0, e1_buildup)
						.setSound(AudioData.void_skill2_e1);

	}

	@Override
	protected Emission createFirstEmission() {

		e1_buildup.setCenter(cursor_pos);
		e0_empty.setCenter(cursor_pos);

		Vector2 wormholeOut = effect_blackhole_enemy.calcWormholeOutPos(physical_client);
		effect_blackhole_ally.setWormholePos(wormholeOut);

		/*
		 * if this skill is combining, the upcoming effects are located in the
		 * merged_effects field and we have to align/concentrate them to one point.
		 */
		for (AEffect effect : e4_feedback_enemy_in.getRuntimeData().getMergedEffects())
			if (effect instanceof EffectBlackhole)
				((EffectBlackhole) effect).setWormholePos(wormholeOut);
		for (AEffect effect : e6_feedback_ally_in.getRuntimeData().getMergedEffects())
			if (effect instanceof EffectBlackhole)
				((EffectBlackhole) effect).setWormholePos(wormholeOut);

		e3_blackhole_out.setCenter(wormholeOut);

		return new Emission(e0_empty, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_buildup);
		emissions.add(e2_blackhole_in);
		emissions.add(e4_feedback_enemy_in);
		emissions.add(e5_feedback_enemy_out);
		emissions.add(e6_feedback_ally_in);
		emissions.add(e7_feedback_ally_out);
	}

}
