package com.elementar.logic.characters.skills.void_;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.math.Vector2;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.ABasicSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.characters.skills.MyRayCastCallback;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class VoidSkill0 extends ABasicSkill {

	private AEmissionDef e1_first_slow_projectile, e2_second_fast_projectile, e3_portal_in, e4_portal_out,
			e5_feedback_ground_enemy_first, e6_feedback_ground_enemy_second;

	private ArrayList<Emission> emission_list;

	public VoidSkill0(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
		emission_list = new ArrayList<>();
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getGroundFilter())
				.setFlyThroughTargets();
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/void_skill0_charge_" + skin_name_parsed + ".p", 1f, 2f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		float multiplier = 2.5f;

		DefProjectile defProjectileE6 = new DefProjectile(1000, getNeutralFilter());

		e6_feedback_ground_enemy_second = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill0_e6_" + skin_name_parsed
						+ "_feedback_ground_enemy_second.p",
				multiplier, multiplier, true, defProjectileE6, 1, 0, new ObstacleAlignment())
						.setSound(AudioData.void_skill0_e6)
						.setStartConditions(
								new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_GROUND,
										CollisionKinds.CAST_ON_ENEMY.getValue()))
						.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(1).getDamage()));

		DefProjectile defProjectileE5 = new DefProjectile(1000, getNeutralFilter());

		e5_feedback_ground_enemy_first = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill0_e5_" + skin_name_parsed
						+ "_feedback_ground_enemy_first.p",
				multiplier, multiplier, true, defProjectileE5, 1, 0, new ObstacleAlignment())
						.setSound(AudioData.void_skill0_e5)
						.setStartConditions(
								new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_GROUND,
										CollisionKinds.CAST_ON_ENEMY.getValue()))
						.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(0).getDamage()));

		// alignments of e2, e3 and e4 will be set later
		DefProjectile defProjectileE2 = new DefProjectile(1000,
				getCollisionFilterWithProjGround(CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_GROUND))
						.setVelocity(4f).addSuccessor(e6_feedback_ground_enemy_second);

		e2_second_fast_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill0_e2_" + skin_name_parsed + "_main_projectile_fast.p", 3.5f,
				1.5f, true, defProjectileE2, 1, 0, null).setSound(AudioData.void_skill0_e2);

		DefProjectile defProjectileE4 = new DefProjectile(1000, getNeutralFilter());

		e4_portal_out = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill0_e4_" + skin_name_parsed + "_portal_out.p", multiplier,
				multiplier, true, defProjectileE4, 1, 0, null).addSuccessorTimewise(0, e2_second_fast_projectile);

		DefProjectile defProjectileE3 = new DefProjectile(1000, getNeutralFilter());

		e3_portal_in = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill0_e3_" + skin_name_parsed + "_portal_in.p", multiplier,
				multiplier, true, defProjectileE3, 1, 0, null);

		DefProjectile defProjectileE1 = new DefProjectile(5000,
				getCollisionFilterWithProjGround(CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_GROUND))
						.setVelocity(1.5f).addSuccessor(e5_feedback_ground_enemy_first);

		e1_first_slow_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill0_e1_" + skin_name_parsed + "_main_projectile_slow.p", 4f,
				1.5f, true, defProjectileE1, 1, 0, new FixedCursorAlignment()).setOffset(0.15f)
						.setSound(AudioData.void_skill0_e1);

	}

	@Override
	protected Emission createFirstEmission() {

		e1_first_slow_projectile.setCenter(player_pos);

		Emission e = new Emission(e1_first_slow_projectile, physical_client);
		emission_list.add(e);

		return e;
	}

	@Override
	public void release() {

		super.release();

		// by raycasting
		e4_portal_out.setCenter(MyRayCastCallback.calcClosestOutPos(physical_client, physical_client.cursor_pos));

		Emission e;
		int projectileIndex = 0;

		// portal-out fire rate
		int cooldownMS = getDelay();

		e3_portal_in.setOffset(0f);

		Iterator<Emission> it = emission_list.iterator();
		while (it.hasNext() == true) {
			e = it.next();
			if (e.getEmittedProjectiles().size() > 0) {

				Projectile p = e.getEmittedProjectiles().get(0);
				if (p.isAlive() == true) {

					p.setLifeTime(0);

					FixedAlignment fixedAlignment = new FixedAlignment((short) p.getAngleBeginning());

					e3_portal_in.setCenter(p.getPosition());
					e3_portal_in.setAlignment(fixedAlignment);
					// create e3
					physical_client.getEmissionList()
							.add(new Emission(e3_portal_in, physical_client, new Vector2(e3_portal_in.getCenterPos())));

					e4_portal_out.setDelayStart(projectileIndex * cooldownMS);
					e4_portal_out.setAlignment(fixedAlignment);
					e2_second_fast_projectile.setAlignment(fixedAlignment);
					physical_client.getEmissionList().add(new Emission(e4_portal_out, physical_client));
					projectileIndex++;
				}
			}
			it.remove();
		}

	}

	private int getDelay() {
		return (int) (cooldown_modifiable_absolute * 1000f) / 4;
	}

}
