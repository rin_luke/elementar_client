package com.elementar.logic.characters.skills.void_;

import java.util.ArrayList;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectImpulseEmitterFollowing;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class VoidSkill1 extends AMainSkill {

	private AEmissionDef e1_main_projectile, e2_caster, e3_feedback_enemy, e4_feedback_ground;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public VoidSkill1(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/void_skill1_charge_" + skin_name_parsed + ".p", 1f, 3f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e4 feedback ground
		DefProjectile defProjectileE4 = new DefProjectile(1200, getNeutralFilter());
		e4_feedback_ground = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill1_e4_" + skin_name_parsed + "_feedback_ground.p", 1f, 2f,
				true, defProjectileE4, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setSound(AudioData.leaf_skill2_e2);

		// e2 at caster
		DefProjectile defProjectileE2 = new DefProjectile(meta_skill.getFeedbacks().get(0).getDuration(),
				getNeutralFilter()).setAttachmentOption(AttachmentOptionProjectile.AT_EMITTER);

		e2_caster = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill1_e2_" + skin_name_parsed + "_caster.p", 1f, 1f, true,
				defProjectileE2, 1, 0, new FixedAlignment(0)).setSound(AudioData.void_skill1_e2, true);

		// e3 feedback enemy
		DefProjectile defProjectileE3 = new DefProjectile(meta_skill.getFeedbacks().get(0).getDuration(),
				getNeutralFilter()).setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill1_e3_" + skin_name_parsed + "_feedback_enemy.p", .1f, 1f,
				true, defProjectileE3, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setSound(AudioData.void_skill1_e3, true).addSuccessorTimewise(0, e2_caster);
		short poolIDE3 = e3_feedback_enemy.getPoolID();
		e3_feedback_enemy.addEffect(new EffectImpulseEmitterFollowing(poolIDE3, defProjectileE3.getLifeTime(), 2));

		// e1 main projectile
		DefProjectile defProjectileE1 = new DefProjectile(1000,
				getCollisionFilterWithProjGround(CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_GROUND))
						.addSuccessor(e3_feedback_enemy).addSuccessor(e4_feedback_ground).setVelocity(7f);

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/void_skill1_e1_" + skin_name_parsed + "_main_projectile.p", 2.5f, 2f,
				false, defProjectileE1, 1, 0, new FixedCursorAlignment()).setSound(AudioData.void_skill1_e1);

	}

	@Override
	protected Emission createFirstEmission() {

		e1_main_projectile.setCenter(player_pos);

		return new Emission(e1_main_projectile, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_main_projectile);
		emissions.add(e2_caster);
		emissions.add(e3_feedback_enemy);
		emissions.add(e4_feedback_ground);
	}

}