package com.elementar.logic.characters.skills.toxic;

import java.util.ArrayList;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.CursorFollowingWhileEmittingAlignment;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.emission.effect.EffectHealthChangeOverTime;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class ToxicSkill2 extends AMainSkill {

	public static int DOT_DAMAGE_PER_SEC = -3;
	public static int DOT_TIME_DURATION_IN_MS = 3000;

	private AEmissionDef e1_main_projectile, e2_outburst, e3_outburst_empty, e4_explosion, e5_feedback_enemy;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public ToxicSkill2(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
		
		DOT_DAMAGE_PER_SEC = meta_skill.getFeedbacks().get(0).getDotAmountPerSec();
		DOT_TIME_DURATION_IN_MS = meta_skill.getFeedbacks().get(0).getDotDuration();
		
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/toxic_skill2_charge_" + skin_name_parsed + ".p", 1, 1.5f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e5 feedback
		DefProjectile defProjectileE5 = new DefProjectile(meta_skill.getFeedbacks().get(0).getDotDuration(), getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e5_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/toxic_skill2_e5_feedback_enemy.p", 2f, 1.5f, false, defProjectileE5,
				1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setDestroyPrevProjectileAfterGround(false);
		short poolIDE5 = e5_feedback_enemy.getPoolID();
		e5_feedback_enemy
				.addEffect(new EffectHealthChangeOverTime(poolIDE5, defProjectileE5.getLifeTime(), meta_skill.getFeedbacks().get(0).getDotAmountPerSec())
						.setStackable().setThumbnail(getThumbnailInWorld(), true)
						.setAnimationColorChange(AnimationName.COLOR_TOXIC))
				.addEffect(new EffectHealthChangeOverTime(poolIDE5, defProjectileE5.getLifeTime(), meta_skill.getFeedbacks().get(0).getDotAmountPerSec())
						.setStackable().setThumbnail(getThumbnailInWorld(), true)
						.setAnimationColorChange(AnimationName.COLOR_TOXIC))
				.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(0).getDamage()));

		// e4 explosion
		DefProjectile defProjectileE4 = new DefProjectile(1000,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE)).setFlyThroughTargets()
						.addSuccessor(e5_feedback_enemy);

		e4_explosion = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/toxic_skill2_e4_" + skin_name_parsed + "_direct_hit_explosion.p", 40,
				2.5f, false, defProjectileE4, 1, 0, new FixedAlignment(90))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setSound(AudioData.toxic_skill2_e4);

		// e3 outburst empty
		DefProjectile defProjectileE3 = new DefProjectile(300,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE)).setFlyThroughTargets()
						.addSuccessor(e5_feedback_enemy).setVelocity(7f);

		e3_outburst_empty = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/toxic_skill2_e3_outburst_empty.p", 15, 1, false, defProjectileE3, 1,
				0, new ObstacleAlignment()).setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0));

		// e2 outburst
		DefProjectile defProjectileE2 = new DefProjectile(1000, getNeutralFilter());

		e2_outburst = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/toxic_skill2_e2_" + skin_name_parsed + "_outburst.p", 1f, 2.1f, false,
				defProjectileE2, 1, 0, new ObstacleAlignment()).setPersecuteeAfterCollision()
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setSound(AudioData.toxic_skill2_e2);

		// e1 main projectile
		DefProjectile defProjectileE1 = new DefProjectile(5000,
				getCollisionFilterWithProjGround(CollisionData.BIT_GROUND + CollisionData.BIT_CHARACTER_PROJECTILE))
						.setGravityScale(1f).setVelocity(8f).addSuccessor(e2_outburst).addSuccessor(e3_outburst_empty)
						.addSuccessor(e4_explosion).setStopAfterGroundCollision();

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/toxic_skill2_e1_" + skin_name_parsed + "_main_projectile.p", 2f, 1.5f,
				false, defProjectileE1, 1, 0, new CursorFollowingWhileEmittingAlignment())
						.setSound(AudioData.toxic_skill2_e1);

	}

	@Override
	protected Emission createFirstEmission() {
		e1_main_projectile.setCenter(player_pos);
		return new Emission(e1_main_projectile, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_main_projectile);
		emissions.add(e2_outburst);
		emissions.add(e4_explosion);
		emissions.add(e5_feedback_enemy);
	}

	public short getPoolIDE5() {
		return e5_feedback_enemy.getPoolID();
	}

}