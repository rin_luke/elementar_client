package com.elementar.logic.characters.skills.toxic;

import java.util.ArrayList;

import com.badlogic.gdx.physics.box2d.Filter;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefAngleRanged;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectEmpty;
import com.elementar.logic.emission.effect.EffectHealthChangeOverTime;
import com.elementar.logic.emission.order.SequentialOrder;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class ToxicSkill1 extends AMainSkill {

	private EffectHealthChangeOverTime dot_effect;

	private AEmissionDef e1_main_projectile, e2_explosion, e3_ground_toxic, e4_feedback_enemy, e5_feedback_ally;

	/**
	 * 
	 * @param toxicSkill2
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public ToxicSkill1(ToxicSkill2 toxicSkill2, MetaSkill metaSkill, PhysicalClient physicalClient,
			DrawableClient drawableClient, String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);

		/*
		 * override thumbnail sprite and poolID of the dot effect + slow effect by
		 * ToxicSkill2 to enable stacking the same effect between skills
		 */
		dot_effect.setThumbnail(toxicSkill2.getThumbnailInWorld(), true);
		dot_effect.setPoolID(toxicSkill2.getPoolIDE5());
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/toxic_skill1_charge_" + skin_name_parsed + ".p", 1, 1.5f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e5 ally feedback empty
		DefProjectile defProjectileE5 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e5_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/toxic_skill1_e5_" + skin_name_parsed + "_feedback_ally.p", 1f, 1f,
				false, defProjectileE5, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_EXCLUDED.getValue()))
						.addEffect(new EffectEmpty());

		// e4 enemy feedback
		DefProjectile defProjectileE4 = new DefProjectile(meta_skill.getFeedbacks().get(0).getDotDuration(),
				getNeutralFilter()).setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e4_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/toxic_skill1_e4_" + skin_name_parsed + "_feedback_enemy.p", 1f, 1f,
				false, defProjectileE4, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setSound(AudioData.toxic_skill1_e4);

		e4_feedback_enemy
				// poolID will be overridden (see constructor)
				.addEffect(dot_effect = new EffectHealthChangeOverTime((short) -1, defProjectileE4.getLifeTime(),
						ToxicSkill2.DOT_DAMAGE_PER_SEC).setAnimationColorChange(AnimationName.COLOR_TOXIC)
								.setStackable());

		// e3 ground toxic
		Filter filter = getCollisionFilterWithoutProjGround(
				CollisionData.BIT_TOXIC_SKILL1_PROJECTILE + CollisionData.BIT_CHARACTER_PROJECTILE);
		filter.categoryBits |= CollisionData.BIT_TOXIC_SKILL1_PROJECTILE;
		DefProjectile defProjectileE3 = new DefProjectile(10000, filter).addSuccessor(e4_feedback_enemy)
				.addSuccessor(e5_feedback_ally).setFlyThroughTargets().setOneSidedCancellation()
				.setRectangleShape(0.7f, 0.2f);

		e3_ground_toxic = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/toxic_skill1_e3_" + skin_name_parsed + "_ground_toxic.p", 5f, 2.5f,
				true, defProjectileE3, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setRemoveFromSucclistAfterGroundCollision(false).setOffset(-0.1f);

		// e2 explosion
		DefProjectile defProjectileE2 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_EMITTER);
		e2_explosion = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/toxic_skill1_e2_" + skin_name_parsed + "_explosion.p", 1f, 1.5f,
				false, defProjectileE2, 1, 0, new FixedAlignment(0)).setSound(AudioData.toxic_skill1_e2);

		// e1 empty
		DefProjectile defProjectileE1 = new DefProjectile(400,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_GROUND + CollisionData.BIT_CHARACTER_PROJECTILE))
						.setVelocity(9f).addSuccessor(e3_ground_toxic).addSuccessor(e4_feedback_enemy)
						.addSuccessor(e5_feedback_ally).setFlyThroughTargets();

		e1_main_projectile = new EmissionDefAngleRanged("graphic/particles/particle_skill/toxic_skill1_e1_empty.p", 1f,
				1f, true, defProjectileE1, 12, 0, 360f, 12, new SequentialOrder(), new FixedAlignment(-90))
						.addSuccessorTimewise(0, e2_explosion);
	}

	@Override
	protected Emission createFirstEmission() {
		e1_main_projectile.setCenter(player_pos);
		return new Emission(e1_main_projectile, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e2_explosion);
		emissions.add(e3_ground_toxic);
		emissions.add(e4_feedback_enemy);
		emissions.add(e5_feedback_ally);
	}

}