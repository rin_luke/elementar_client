package com.elementar.logic.characters.skills.toxic;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.skills.ABasicSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.CursorFollowingWhileEmittingAlignment;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectHealthChangeOverTime;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class ToxicSkill0 extends ABasicSkill {

	private AEmissionDef e1_main_projectile, e2_bridge, e3_ground, e4_feedback_enemy;

	private EffectHealthChangeOverTime dot_effect;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public ToxicSkill0(ToxicSkill2 toxicSkill2, MetaSkill metaSkill, PhysicalClient physicalClient,
			DrawableClient drawableClient, String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);

		/*
		 * override thumbnail sprite and poolID of the dot effect + slow effect by
		 * ToxicSkill2 to enable stacking the same effect between skills
		 */
		dot_effect.setThumbnail(toxicSkill2.getThumbnailInWorld(), true);
		dot_effect.setPoolID(toxicSkill2.getPoolIDE5());

	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getGroundFilter())
				.setFlyThroughTargets();
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/toxic_skill0_charge_" + skin_name_parsed + ".p", 1, 1.5f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// life time will be overridden
		DefProjectile defProjectileE4Dot = new DefProjectile(ToxicSkill2.DOT_TIME_DURATION_IN_MS, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e4_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/toxic_skill0_e4_" + skin_name_parsed + "_feedback_enemy_ot.p", 2f,
				1.5f, false, defProjectileE4Dot, 1, 0, new FixedAlignment(90))
						.setStartConditions(new StartConditions(0, CollisionKinds.AFTER_LIFETIME.getValue()))
						.setAttached();
		e4_feedback_enemy
				// poolID will be overridden (see constructor)
				.addEffect(dot_effect = new EffectHealthChangeOverTime((short) -1, defProjectileE4Dot.getLifeTime(),
						ToxicSkill2.DOT_DAMAGE_PER_SEC).setAnimationColorChange(AnimationName.COLOR_TOXIC)
								.setStackable());

		// e3 ground
		e3_ground = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/toxic_skill0_e3_" + skin_name_parsed + "_feedback_ground.p", 2f, 1.5f,
				true, new DefProjectile(2000, getNeutralFilter()), 1, 0, new ObstacleAlignment())
						.setSound(AudioData.toxic_skill0_e3)
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0));

		// e2, bridge emission
		int transitionTime = 400;
		DefProjectile defProjectileE2 = new DefProjectile(transitionTime, getNeutralFilter()).setVelocity(2f)
				.addSuccessor(e4_feedback_enemy);

		e2_bridge = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/toxic_skill0_e2_" + skin_name_parsed + "_bridge.p", 1f, 1f, false,
				defProjectileE2, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setPersecuteeAfterCollision().setSound(AudioData.toxic_skill0_e2);

		// e1, main projectile
		DefProjectile defProjectileE1 = new DefProjectile(6000,
				getCollisionFilterWithProjGround(CollisionData.BIT_GROUND + CollisionData.BIT_CHARACTER_PROJECTILE))
						.setGravityScale(1f).setVelocity(8f)
						.addSuccessor(getCrumblingStones().setDestroyPrevProjectileAfterGround(false))
						.addSuccessor(e2_bridge.setDestroyPrevProjectileAfterGround(false))
						.addSuccessor(e3_ground.setDestroyPrevProjectileAfterGround(false))
						.setStopAfterGroundCollision();

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/toxic_skill0_e1_" + skin_name_parsed + "_main_projectile.p", 2f, 1.5f,
				true, defProjectileE1, 1, 0, new CursorFollowingWhileEmittingAlignment())
						.setSound(AudioData.toxic_skill0_e1);

	}

	@Override
	protected Emission createFirstEmission() {
		e1_main_projectile.setCenter(player_pos);
		return new Emission(e1_main_projectile, physical_client);
	}

}
