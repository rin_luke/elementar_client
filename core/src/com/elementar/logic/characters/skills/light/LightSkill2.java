package com.elementar.logic.characters.skills.light;

import java.util.ArrayList;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectHeal;
import com.elementar.logic.emission.effect.EffectLightPort;
import com.elementar.logic.emission.effect.EffectSilence;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class LightSkill2 extends AMainSkill {

	private static final int PORT_DURATION_MS = 600;

	private AEmissionDef e1_portal_creation, e2_portal_idle, e3_feedback_enemy_in, e4_feedback_enemy_out,
			e5_feedback_ally_in, e6_feedback_ally_out;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public LightSkill2(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {

		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/light_skill2_charge_" + skin_name_parsed + ".p", 1f, 1f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e6, feedback ally out
		DefProjectile defProjectileE6 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e6_feedback_ally_out = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/light_skill2_e6_" + skin_name_parsed + "_feedback_ally_out.p", 1, 1f,
				false, defProjectileE6, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_EXCLUDED.getValue()))
						.setSound(AudioData.light_skill2_e6).setAttached().setDelayStart(PORT_DURATION_MS);

		// e5, feedback ally in
		DefProjectile defProjectileE5 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET).addSuccessor(e6_feedback_ally_out);

		e5_feedback_ally_in = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/light_skill2_e5_" + skin_name_parsed + "_feedback_ally_in.p", 1, 1f,
				false, defProjectileE5, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_INCLUDED.getValue()))
						.setDestroyPrevProjectileAfterPlayer(true).setDestroyPrevProjectileAfterGround(false)
						.setSound(AudioData.light_skill2_e5).setAttached();
		short poolIDE5 = e5_feedback_ally_in.getPoolID();
		e5_feedback_ally_in.addEffect(new EffectLightPort(poolIDE5, PORT_DURATION_MS))
				.addEffect(new EffectHeal(meta_skill.getFeedbacks().get(0).getHeal()));

		// e4, feedback enemy out
		DefProjectile defProjectileE4 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e4_feedback_enemy_out = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/light_skill2_e4_" + skin_name_parsed + "_feedback_enemy_out.p", 1, 1f,
				false, defProjectileE4, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setSound(AudioData.light_skill2_e4).setAttached().setDelayStart(PORT_DURATION_MS);

		// e3, feedback enemy in
		DefProjectile defProjectileE3 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_enemy_in = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/light_skill2_e3_" + skin_name_parsed + "_feedback_enemy_in.p", 1,
				1.5f, false, defProjectileE3, 1, 0, new FixedAlignment(0))
						.setSound(AudioData.light_skill2_e3)
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setDestroyPrevProjectileAfterPlayer(true).setDestroyPrevProjectileAfterGround(false)
						.setAttached();
		short poolIDE3 = e3_feedback_enemy_in.getPoolID();
		e3_feedback_enemy_in.addEffect(new EffectLightPort(poolIDE3, PORT_DURATION_MS))
				.addEffect(new EffectSilence(poolIDE3, meta_skill.getFeedbacks().get(1).getSilenceDuration())
						.setDelayMS(PORT_DURATION_MS));

		// e2, portal idle
		DefProjectile defProjectileE2 = new DefProjectile(10000,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE))
						.addSuccessor(e3_feedback_enemy_in).addSuccessor(e4_feedback_enemy_out)
						.addSuccessor(e5_feedback_ally_in).addSuccessor(e6_feedback_ally_out);

		e2_portal_idle = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/light_skill2_e2_" + skin_name_parsed + "_portal_idle.p", 10f, 2f,
				false, defProjectileE2, 1, 0, new FixedCursorAlignment())
						.setStartConditions(
								new StartConditions(CollisionData.BIT_GROUND, CollisionKinds.AFTER_LIFETIME.getValue()))
						.setSound(AudioData.light_skill2_e2, true);

		DefProjectile defProjectileE1 = new DefProjectile(1000,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_GROUND))
						.setVelocity(7.5f).setRangeUntilStop(6f)/* artificial offset */.setContinuousScaling(10f, 1000)
						.setStopAfterGroundCollision().setStopUntilReachedCursor().addSuccessor(e2_portal_idle)
						.addSuccessor(e3_feedback_enemy_in).addSuccessor(e4_feedback_enemy_out)
						.addSuccessor(e5_feedback_ally_in).addSuccessor(e6_feedback_ally_out)
						.setTimeUntilEmitterCollision(300);

		e1_portal_creation = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/light_skill2_e1_" + skin_name_parsed + "_portal_creation.p", 0.1f, 2f,
				false, defProjectileE1, 1, 0, new FixedCursorAlignment()).setSound(AudioData.light_skill2_e1)
						.setOffset(0.1f);
	}

	@Override
	protected Emission createFirstEmission() {

		e1_portal_creation.setCenter(player_pos);

		return new Emission(e1_portal_creation, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_portal_creation);
		emissions.add(e2_portal_idle);
		emissions.add(e3_feedback_enemy_in);
		emissions.add(e4_feedback_enemy_out);
		emissions.add(e5_feedback_ally_in);
		emissions.add(e6_feedback_ally_out);
	}

}
