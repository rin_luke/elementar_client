package com.elementar.logic.characters.skills.light;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.characters.skills.MyRayCastCallback;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.emission.effect.EffectHeal;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class LightSkill1 extends AMainSkill {

	private AEmissionDef e1_entry, e2_exit, e3_feedback_enemy, e4_feedback_ally;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public LightSkill1(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {

		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/light_skill2_charge_" + skin_name_parsed + ".p", 1f, 1f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e4, feedback ally
		DefProjectile defProjectileE4 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e4_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/light_skill1_e4_" + skin_name_parsed + "_feedback_ally.p", 1, 1f,
				false, defProjectileE4, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_INCLUDED.getValue()))
						.setSound(AudioData.light_skill2_e4)
						.addEffect(new EffectHeal(meta_skill.getFeedbacks().get(0).getHeal()));

		// e3, feedback enemy
		DefProjectile defProjectileE3 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/light_skill1_e3_" + skin_name_parsed + "_feedback_enemy.p", 1, 1f,
				false, defProjectileE3, 1, 0, new FixedAlignment(0))
						.setSound(AudioData.light_skill2_e3)
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(1).getDamage()));

		// e2, at exit
		DefProjectile defProjectileE2 = new DefProjectile(500,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE))
						.addSuccessor(e3_feedback_enemy).addSuccessor(e4_feedback_ally).setFlyThroughTargets();

		e2_exit = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/light_skill1_e2_" + skin_name_parsed + "_exit.p", 15f, 1f, false,
				defProjectileE2, 1, 0, new FixedAlignment(90)).setSound(AudioData.light_skill1_e2);

		// e1, at entry
		DefProjectile defProjectileE1 = new DefProjectile(500,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE))
						.addSuccessor(e3_feedback_enemy).addSuccessor(e4_feedback_ally).setFlyThroughTargets();

		e1_entry = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/light_skill1_e1_" + skin_name_parsed + "_entry.p", 1f, 2f, false,
				defProjectileE1, 1, 0, new FixedAlignment(90)).setSound(AudioData.light_skill1_e1)
						.addSuccessorTimewise(0, e2_exit);
	}

	@Override
	protected Emission createFirstEmission() {

		// find center pos that is not inside of an island
		Vector2 centerPos = MyRayCastCallback.calcClosestOutPos(physical_client, physical_client.cursor_pos);
		e2_exit.setCenter(centerPos);

		e1_entry.setCenter(new Vector2(player_pos));

		physical_client.setPosition(centerPos.x, centerPos.y);

		return new Emission(e1_entry, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_entry);
		emissions.add(e2_exit);
		emissions.add(e3_feedback_enemy);
		emissions.add(e4_feedback_ally);
	}

}
