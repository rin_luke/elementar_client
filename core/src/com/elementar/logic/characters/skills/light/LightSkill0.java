package com.elementar.logic.characters.skills.light;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.ABasicSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.CursorFollowingWhileEmittingAlignment;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.emission.effect.EffectHeal;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class LightSkill0 extends ABasicSkill {

	private AEmissionDef e1_main_projectile, e2_ground, e3_feedback_enemy, e4_feedback_ally;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public LightSkill0(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getGroundFilter())
				.setFlyThroughTargets();
		charge_def = new EmissionDefBoneFollowing("graphic/particles/particle_skill/light_skill0_charge_skin0.p", 1, 1f,
				false, prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e4, feedback at ally
		DefProjectile defProjectileE4 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e4_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/light_skill0_e4_" + skin_name_parsed + "_feedback_ally.p", 1, 0.7f,
				false, defProjectileE4, 1, 0, new FixedAlignment(0))
						.setSound(AudioData.light_skill0_e3)
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_EXCLUDED.getValue()))
						.addEffect(new EffectHeal(meta_skill.getFeedbacks().get(0).getHeal()))
						.setDestroyPrevProjectileAfterGround(false);

		// e3, feedback at enemy
		DefProjectile defProjectileE3 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/light_skill0_e3_" + skin_name_parsed + "_feedback_enemy.p", 1, 2,
				false, defProjectileE3, 1, 0, new FixedAlignment(0))
						.setSound(AudioData.light_skill0_e3)
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(1).getDamage()))
						.setDestroyPrevProjectileAfterGround(false);

		// e2, ground collision
		DefProjectile defProjectileE2 = new DefProjectile(1000, getNeutralFilter());

		e2_ground = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/light_skill0_e2_" + skin_name_parsed + "_feedback_ground.p", 1, 1f,
				false, defProjectileE2, 1, 0, new ObstacleAlignment()).setSound(AudioData.light_skill2_e2)
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setRemoveFromSucclistAfterGroundCollision(false).setDestroyPrevProjectileAfterGround(false)
						.setSound(AudioData.light_skill0_e2);

		// e1, main projectile
		DefProjectile defProjectileE1 = new DefProjectile(2000,
				getCollisionFilterWithProjGround(CollisionData.BIT_GROUND + CollisionData.BIT_CHARACTER_PROJECTILE))
						.setVelocity(7f).setRestitution(1).addSuccessor(e2_ground).addSuccessor(e3_feedback_enemy)
						.addSuccessor(e4_feedback_ally);

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/light_skill0_e1_" + skin_name_parsed + "_main_projectile.p", 1.6f,
				0.6f, true, defProjectileE1, 1, 0, new CursorFollowingWhileEmittingAlignment())
						.setSound(AudioData.light_skill0_e1);

	}

	@Override
	protected Emission createFirstEmission() {
		e1_main_projectile.setCenter(player_pos);
		return new Emission(e1_main_projectile, physical_client);
	}

}
