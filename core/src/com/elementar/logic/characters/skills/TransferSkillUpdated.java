package com.elementar.logic.characters.skills;

import com.badlogic.gdx.math.Vector2;
import com.elementar.gui.util.CurvedTrajectory;
import com.elementar.gui.util.CurvedTrajectoryAccessor;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.util.Shared;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

/**
 * The transfer-skill has an emission which holds a special particle effect with
 * non-visual emitters but with a certain combining-behaviour. Particle effects
 * of the transferring skill will be combined with the first emission of the
 * corresponding skill and represent the skill in compressed way for sending it
 * to mates.
 * 
 * @author lukassongajlo
 *
 */
public class TransferSkillUpdated extends TransferSkill {

	private static final float ABSORBING_TIME = 1f;

	public static final float W2_AMPLITUDE = 0.8f;

	private final TweenCallback w2_callback = new TweenCallback() {
		@Override
		public void onEvent(int type, BaseTween<?> source) {

			CurvedTrajectory trajectory = (CurvedTrajectory) source.getUserData();

			Tween.to(trajectory, CurvedTrajectoryAccessor.W2_BACK, ABSORBING_TIME * 0.5f).target(0f)
					.ease(TweenEquations.easeOutQuad).start(tween_manager);
		}
	};

	private TweenManager tween_manager = new TweenManager();

	public TransferSkillUpdated(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient) {
		super(metaSkill, physicalClient, drawableClient);
	}

	@Override
	public void update() {
		super.update();

		tween_manager.update(Shared.SEND_AND_UPDATE_RATE_WORLD);

	}

	@Override
	protected Emission createFirstEmission() {

		if (e0_thumbnail == null)
			return null;

		e0_thumbnail.setCenter(player_pos);
		e1.setCenter(player_pos);
		e2_at_ally.setCenter(player_pos);
//		e2_at_ally.setTarget(ally);

		e1.setTarget(ally);
		e1.setAbsorber(ally, ABSORBING_TIME);
		e1.setPersecutee(ally);

		e0_thumbnail.setAbsorber(ally, ABSORBING_TIME);
		e0_thumbnail.setPersecutee(ally);
		e0_thumbnail.setTarget(ally);

		CurvedTrajectory trajectory = new CurvedTrajectory(new Vector2(player_pos), ally);

		e0_thumbnail.getDefProjectile().setTrajectory(trajectory);
		e1.getDefProjectile().setTrajectory(trajectory);

		Tween.to(trajectory, CurvedTrajectoryAccessor.W1, ABSORBING_TIME).target(1f).ease(TweenEquations.easeNone)
				.start(tween_manager);

		Tween.to(trajectory, CurvedTrajectoryAccessor.W2, ABSORBING_TIME * 0.5f).target(W2_AMPLITUDE)
				.ease(TweenEquations.easeOutQuad).setUserData(trajectory).setCallback(w2_callback).start(tween_manager);

		return new Emission(e0_thumbnail, physical_client);

	}

}
