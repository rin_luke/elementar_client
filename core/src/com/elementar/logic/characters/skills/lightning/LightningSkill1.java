package com.elementar.logic.characters.skills.lightning;

import java.util.ArrayList;

import com.badlogic.gdx.physics.box2d.Filter;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.emission.effect.EffectEmpty;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class LightningSkill1 extends AMainSkill {

	private static final int EXPANSION_TIME_MS = 1500;

	private AEmissionDef e1_main_projectile, e2_feedback_enemy, e3_feedback_ally;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public LightningSkill1(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {

		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/lightning_skill1_charge_" + skin_name_parsed + ".p", 1f, 2f, false,
				prototypeCharge, "character_weapon_front1");

	}

	@Override
	protected void defineEmissions() {

		// e3, feedback ally
		DefProjectile defProjectileE3 = new DefProjectile(2000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/lightning_skill1_e3_" + skin_name_parsed + "_feedback_ally.p", 1, 2f,
				false, defProjectileE3, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_EXCLUDED.getValue()))
						.addEffect(new EffectEmpty());

		// e2, feedback enemy
		DefProjectile defProjectileE2 = new DefProjectile(2000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e2_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/lightning_skill1_e2_" + skin_name_parsed + "_feedback_enemy.p", 1, 2f,
				false, defProjectileE2, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(0).getDamage()))
						.setSound(AudioData.lightning_skill1_e2);

		// e1, main projectile
		Filter filter = getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE);

		DefProjectile defProjectileE1 = new DefProjectile(EXPANSION_TIME_MS, filter).addSuccessor(e2_feedback_enemy)
				.addSuccessor(e3_feedback_ally).setContinuousScaling(55, EXPANSION_TIME_MS)
				.setAttachmentOption(AttachmentOptionProjectile.AT_EMITTER).setFlyThroughTargets();

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/lightning_skill1_e1_" + skin_name_parsed + "_main_projectile.p", 1f,
				7f, true, defProjectileE1, 1, 0, new FixedAlignment(90)).setSound(AudioData.lightning_skill1_e1)
						.setOneTouchMechanism();

	}

	@Override
	protected Emission createFirstEmission() {
		e1_main_projectile.setCenter(player_pos);
		return new Emission(e1_main_projectile, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_main_projectile);
		emissions.add(e2_feedback_enemy);
		emissions.add(e3_feedback_ally);
	}

}