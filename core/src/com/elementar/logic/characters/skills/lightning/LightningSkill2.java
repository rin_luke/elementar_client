package com.elementar.logic.characters.skills.lightning;

import java.util.ArrayList;

import com.badlogic.gdx.physics.box2d.Filter;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AImpulseSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.emission.effect.EffectEmpty;
import com.elementar.logic.emission.effect.EffectImpulseCursorOnetime;
import com.elementar.logic.emission.effect.EffectStun;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class LightningSkill2 extends AImpulseSkill {

	private final int FLY_TIME = 500;

	private AEmissionDef e1_main_projectile, e2_feedback_enemy, e3_feedback_ally;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public LightningSkill2(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {

		DefProjectile prototypeCharge = new DefProjectile(FLY_TIME, getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/lightning_skill2_charge_" + skin_name_parsed + "_dash.p", 1f, 2f,
				false, prototypeCharge, "character_torso").setSound(AudioData.lightning_skill2_e1);

		short poolID = charge_def.getPoolID();
		charge_def.addEffect(new EffectImpulseCursorOnetime(poolID, FLY_TIME, 10).setMovementImpulse());
		charge_def.setTarget(physical_client);

		// do not add recoil effects
		recoil_flag = false;

	}

	@Override
	protected void defineEmissions() {

		// e3, feedback ally
		DefProjectile defProjectileE3 = new DefProjectile(2000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/lightning_skill2_e3_" + skin_name_parsed + "_feedback_ally.p", 1, 2f,
				false, defProjectileE3, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_EXCLUDED.getValue()))
						.addEffect(new EffectEmpty());

		// e2, feedback enemy
		DefProjectile defProjectileE2 = new DefProjectile(meta_skill.getFeedbacks().get(0).getStunDuration(),
				getNeutralFilter()).setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e2_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/lightning_skill2_e2_" + skin_name_parsed + "_feedback_enemy.p", 1, 2f,
				false, defProjectileE2, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setSound(AudioData.lightning_skill2_e2, true);
		short poolIDE2 = e2_feedback_enemy.getPoolID();
		e2_feedback_enemy.addEffect(
				new EffectStun(poolIDE2, defProjectileE2.getLifeTime()).setThumbnail(getThumbnailInWorld(), true))
				.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(0).getDamage()));

		// e1, main projectile
		Filter filter = getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE);

		DefProjectile defProjectileE1 = new DefProjectile(FLY_TIME, filter).addSuccessor(e2_feedback_enemy)
				.addSuccessor(e3_feedback_ally).setAttachmentOption(AttachmentOptionProjectile.AT_EMITTER)
				.setFlyThroughTargets();

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/lightning_skill2_e1_empty.p", 10.2f, 2f, true, defProjectileE1, 1, 0,
				new FixedAlignment(90));

	}

	@Override
	protected Emission createFirstEmission() {
		e1_main_projectile.setCenter(player_pos);
		return new Emission(e1_main_projectile, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(charge_def);
		emissions.add(e2_feedback_enemy);
		emissions.add(e3_feedback_ally);
	}

}