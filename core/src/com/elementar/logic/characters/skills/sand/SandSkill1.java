package com.elementar.logic.characters.skills.sand;

import java.util.ArrayList;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.characters.skills.MyRayCastCallback;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.emission.util.TargetFinding;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class SandSkill1 extends AMainSkill {

	private final int NUMBER_PROJECTILES = 20;
	private final int TOWER_BUILDUP = 2000;
	private final int TOWER_DURATION = 8000;

	private AEmissionDef e0_buildup, e1_tower, e2_projectile, e3_feedback_ground, e4_feedback_enemy;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public SandSkill1(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/sand_skill1_charge_" + skin_name_parsed + ".p", 1f, 1f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e4 feedback enemy
		DefProjectile defProjectileE4 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e4_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/sand_skill1_e4_" + skin_name_parsed + "_feedback_enemy.p", 1f, 5f,
				false, defProjectileE4, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setSound(AudioData.sand_skill1_e4)
						.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(0).getDamage()));

		// e3 feedback ground
		DefProjectile defProjectileE3 = new DefProjectile(1000, getNeutralFilter());

		e3_feedback_ground = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/sand_skill1_e3_" + skin_name_parsed + "_feedback_ground.p", 1f, 3f,
				false, defProjectileE3, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setRemoveFromSucclistAfterGroundCollision(false).setSound(AudioData.sand_skill1_e3);

		// e2 projectile
		DefProjectile defProjectileE2 = new DefProjectile(1000,
				getCollisionFilterWithProjGround(CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_GROUND))
						.setVelocity(6f).addSuccessor(e3_feedback_ground).addSuccessor(e4_feedback_enemy)
						.setTargetFinding(new TargetFinding(5f));

		e2_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/sand_skill1_e2_" + skin_name_parsed + "_projectile.p", 1f, 3f, true,
				defProjectileE2, NUMBER_PROJECTILES, TOWER_DURATION, new FixedAlignment(0))
						.setSound(AudioData.sand_skill1_e2);

		// e1 tower
		DefProjectile defProjectileE1 = new DefProjectile(TOWER_DURATION, getNeutralFilter());

		e1_tower = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/sand_skill1_e1_" + skin_name_parsed + "_tower.p", 1f, 3f, false,
				defProjectileE1, 1, 0, new FixedAlignment(0)).addSuccessorTimewise(0, e2_projectile)
						.setStartConditions(new StartConditions(0, CollisionKinds.AFTER_LIFETIME.getValue()));

		// e0 main projectile
		DefProjectile defProjectileE0 = new DefProjectile(TOWER_BUILDUP, getNeutralFilter()).addSuccessor(e1_tower);

		e0_buildup = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/sand_skill1_e0_" + skin_name_parsed + "_tower_buildup.p", 1f, 3f,
				false, defProjectileE0, 1, 0, new FixedCursorAlignment()).setSound(AudioData.sand_skill1_e0);

	}

	@Override
	protected Emission createFirstEmission() {

		e0_buildup.setCenter(MyRayCastCallback.calcClosestOutPos(physical_client, physical_client.cursor_pos));

		return new Emission(e0_buildup, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e0_buildup);
		emissions.add(e1_tower);
		emissions.add(e2_projectile);
		emissions.add(e3_feedback_ground);
		emissions.add(e4_feedback_enemy);
	}

}