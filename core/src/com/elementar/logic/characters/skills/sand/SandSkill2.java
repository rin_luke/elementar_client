package com.elementar.logic.characters.skills.sand;

import java.util.ArrayList;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDisarmed;
import com.elementar.logic.emission.effect.EffectEmpty;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;
import com.badlogic.gdx.physics.box2d.Filter;

public class SandSkill2 extends AMainSkill {

	private AEmissionDef e1_shield, e3_feedback_ally, e2_feedback_enemy;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public SandSkill2(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing("graphic/particles/particle_skill/sand_skill2_charge_empty.p", 1f, 3f,
				false, prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e3 feedback ally
		DefProjectile defProjectileE3 = new DefProjectile(2500, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/sand_skill2_e3_" + skin_name_parsed + "_feedback_ally.p", 1f, 2f,
				false, defProjectileE3, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_EXCLUDED.getValue()))
						.addEffect(new EffectEmpty());

		// e2 feedback enemy
		DefProjectile defProjectileE2 = new DefProjectile(meta_skill.getFeedbacks().get(0).getDisarmedDuration(),
				getNeutralFilter()).setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e2_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/sand_skill2_e2_" + skin_name_parsed + "_feedback_enemy.p", 1f, 1f,
				false, defProjectileE2, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()));
		short poolIDE3 = e2_feedback_enemy.getPoolID();
		e2_feedback_enemy.addEffect(
				new EffectDisarmed(poolIDE3, defProjectileE2.getLifeTime()).setThumbnail(getThumbnailInWorld(), true));

		// e1 sand shield
		Filter collisionFilter = getCollisionFilterWithProjGround(
				CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_PROJECTILE);
		collisionFilter.categoryBits |= CollisionData.BIT_PROJ_GROUND;

		DefProjectile defProjectileE1 = new DefProjectile(4000, collisionFilter).setRectangleShape(0.15f, 2.45f)
				.setRangeUntilStop(3.5f).setStopUntilReachedCursor().setVelocity(6f).setBodyType(BodyType.KinematicBody)
				.setFlyThroughTargets().addSuccessor(e3_feedback_ally).addSuccessor(e2_feedback_enemy);

		e1_shield = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/sand_skill2_e1_" + skin_name_parsed + "_main_projectile.p", 1f, 2f,
				false, defProjectileE1, 1, 0, new FixedCursorAlignment()).setSound(AudioData.sand_skill2_e1);

	}

	@Override
	protected Emission createFirstEmission() {

		e1_shield.setCenter(player_pos);

		return new Emission(e1_shield, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_shield);
		emissions.add(e2_feedback_enemy);
		emissions.add(e3_feedback_ally);
	}

}