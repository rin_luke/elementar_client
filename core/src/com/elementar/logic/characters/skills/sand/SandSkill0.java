package com.elementar.logic.characters.skills.sand;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.ABasicSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class SandSkill0 extends ABasicSkill {

	private AEmissionDef e3_feedback_enemy, e2_feedback_ground, e1_main_projectile;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public SandSkill0(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/sand_skill0_charge_" + skin_name_parsed + ".p", 1f, 1f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e3 feedback enemy
		DefProjectile defProjectileE3 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/sand_skill0_e3_" + skin_name_parsed + "_feedback_enemy.p", 1f, 3f,
				false, defProjectileE3, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setSound(AudioData.sand_skill0_e3).addEffect(new EffectDamage(meta_skill.getFeedbacks().get(0).getDamage()));

		// e2 feedback ground
		DefProjectile defProjectileE2 = new DefProjectile(2000, getNeutralFilter());

		e2_feedback_ground = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/sand_skill0_e2_" + skin_name_parsed + "_feedback_ground.p", 1f, 2f,
				false, defProjectileE2, 1, 0, new ObstacleAlignment())
						.setStartConditions(
								new StartConditions(CollisionData.BIT_GROUND, CollisionKinds.AFTER_LIFETIME.getValue()))
						.setSound(AudioData.sand_skill0_e2);

		// e1
		DefProjectile defProjectileE1 = new DefProjectile(800,
				getCollisionFilterWithProjGround(CollisionData.BIT_GROUND + CollisionData.BIT_CHARACTER_PROJECTILE))
						.addSuccessor(e3_feedback_enemy).addSuccessor(e2_feedback_ground).setVelocity(9f);

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/sand_skill0_e1_" + skin_name_parsed + "_main_projectile.p", 2.5f, 1f,
				false, defProjectileE1, 1, 0, new FixedCursorAlignment()).setSound(AudioData.sand_skill0_e1);

	}

	@Override
	protected Emission createFirstEmission() {

		e1_main_projectile.setCenter(player_pos);

		return new Emission(e1_main_projectile, physical_client);
	}

}
