package com.elementar.logic.characters.skills;

import java.util.ArrayList;

import com.badlogic.gdx.physics.box2d.Filter;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefDeathExplosionCharacter;
import com.elementar.logic.emission.effect.EffectConsuming;
import com.elementar.logic.emission.effect.EffectDeathTime;
import com.elementar.logic.emission.effect.EffectDying;
import com.elementar.logic.emission.effect.EffectResurrection;
import com.elementar.logic.emission.order.RandomOrder;
import com.elementar.logic.util.CollisionData;

/**
 * 
 * @author lukassongajlo
 * 
 */
public class DyingSkill extends AChargeSkill {

	private AEmissionDef e1;
	private AEmissionDef def_death2, def_death1, def_water_resurrection;

	private boolean resurrecting = false;

	/**
	 * 
	 * @param drawableClient
	 * @param physicalClient
	 * @param unparsedSkinName
	 */
	public DyingSkill(DrawableClient drawableClient, PhysicalClient physicalClient, String unparsedSkinName) {
		super(null, physicalClient, drawableClient, unparsedSkinName);
	}

	@Override
	protected void defineChargeEmission() {

	}

	@Override
	protected void defineEmissions() {
		if (team_user != null) {
			// parse x out of "team_x", which can be 1 or 2
			float multiplier = 1.5f;

			Filter collisionFilter = new Filter();
			collisionFilter.categoryBits = CollisionData.BIT_CHARACTER_MOVEMENT | CollisionData.BIT_CHARACTER_PLANT;
			collisionFilter.maskBits = CollisionData.BIT_GROUND | CollisionData.BIT_PLANT;

			DefProjectile defProjectile2 = new DefProjectile(500, collisionFilter).setVelocity(1.4f)
					.setGravityScale(0.4f).setFriction(0.1f);

			def_death2 = new EmissionDefDeathExplosionCharacter(
					"graphic/particles/particle_global/global_death2_empty.p", multiplier, true, defProjectile2, 12, 0,
					220, 20, new RandomOrder(), new FixedAlignment(physical_client.getPlatformAngle() + 90))
							.addEffect(new EffectDying()).addEffect(new EffectConsuming(EffectDeathTime.class))
							.setTriggerEmissionAllowed();

			DefProjectile defProjectile1 = new DefProjectile(3000, getGroundFilter());

			// death explosion
			def_death1 = new EmissionDefAngleDirected(
					"graphic/particles/particle_global/global_death1_team"
							+ team_user.name().substring(team_user.name().length() - 1) + ".p",
					multiplier, multiplier, false, defProjectile1, 1, 0, new FixedAlignment(0))
							.setSound(AudioData.global_death).addSuccessorTimewise(0, def_death2);

			// water resurrection
			DefProjectile defProjectileWaterRes = new DefProjectile(2000, getNeutralFilter());

			def_water_resurrection = new EmissionDefAngleDirected(
					"graphic/particles/particle_global/global_death_water_resurrection.p", 1, 3, false,
					defProjectileWaterRes, 1, 0, new FixedAlignment(0))
							.addEffect(new EffectConsuming(EffectResurrection.class));
		}
	}

	@Override
	public boolean init() {
		// there are no constraints for the dying skill to happen
//		physical_client.setCurrentSkill(this);
		startFirstEmission();
		return true;
	}

	@Override
	protected Emission createFirstEmission() {
		if (resurrecting == true) {

			// reset flag
			resurrecting = false;

			e1 = def_water_resurrection;

		} else {
			e1 = def_death1;

			def_death2.setBonesModel(drawable_client);
			def_death2.setTarget(physical_client);
			def_death2.setCenter(player_pos);
//			def_death2.setTransmissible(false);
		}

		e1.setCenter(player_pos);
		e1.setTarget(physical_client);
//		e1.setTransmissible(false);

		return new Emission(e1, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {

	}

	/**
	 * 
	 * @param effectResurrection
	 */
	public void setResurrecting(boolean resurrecting) {
		this.resurrecting = resurrecting;
	}

}
