package com.elementar.logic.characters.skills.ice;

import java.util.ArrayList;

import com.badlogic.gdx.physics.box2d.Filter;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefAngleRanged;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectEmpty;
import com.elementar.logic.emission.effect.EffectVelChange;
import com.elementar.logic.emission.order.RandomOrder;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class IceSkill1 extends AMainSkill {

	private final int NUMBER_CONE_SHARDS = 5;

	private final int CONE_DURATION_MS = 1000;

	private AEmissionDef e1_main_projectile, e2_cone, e3_ground_frozen, e4_feedback_enemy, e5_feedback_ally;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public IceSkill1(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/ice_skill1_charge_" + skin_name_parsed + ".p", 1, 1.5f, false,
				prototypeCharge, "character_weapon_front2");
	}

	@Override
	protected void defineEmissions() {

		// e5 ally feedback empty
		DefProjectile defProjectileE5 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e5_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/ice_skill1_e5_" + skin_name_parsed + "_feedback_ally.p", 1f, 1f,
				false, defProjectileE5, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_EXCLUDED.getValue()))
						.addEffect(new EffectEmpty());

		// e4 enemy feedback
		DefProjectile defProjectileE4 = new DefProjectile(meta_skill.getFeedbacks().get(0).getSlowDuration(),
				getNeutralFilter()).setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e4_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/ice_skill1_e4_" + skin_name_parsed + "_feedback_enemy.p", 1f, 1f,
				false, defProjectileE4, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setSound(AudioData.ice_skill1_e4);
		short poolIDE4 = e4_feedback_enemy.getPoolID();
		e4_feedback_enemy.addEffect(new EffectVelChange(poolIDE4, meta_skill.getFeedbacks().get(0).getSlowAmount(),
				defProjectileE4.getLifeTime()).setThumbnail(getThumbnailInWorld(), true)
						.setAnimationColorChange(AnimationName.COLOR_ICE));

		// e3 ground frozen
		Filter filter = getCollisionFilterWithoutProjGround(
				CollisionData.BIT_ICE_SKILL0_PROJECTILE + CollisionData.BIT_CHARACTER_PROJECTILE);
		filter.categoryBits |= CollisionData.BIT_ICE_SKILL0_PROJECTILE;
		DefProjectile defProjectileE3 = new DefProjectile(10000, filter).addSuccessor(e4_feedback_enemy)
				.addSuccessor(e5_feedback_ally).setFlyThroughTargets().setOneSidedCancellation()
				.setRectangleShape(0.7f, 0.1f);

		e3_ground_frozen = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/ice_skill1_e3_" + skin_name_parsed + "_ground_frozen.p", 5f, 2.5f,
				true, defProjectileE3, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setRemoveFromSucclistAfterGroundCollision(false).setOffset(-0.1f)
						.setSound(AudioData.ice_skill1_e3);

		// e2 aura
		DefProjectile defProjectileE2 = new DefProjectile(CONE_DURATION_MS, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_EMITTER).setFlyThroughTargets();
		e2_cone = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/ice_skill1_e2_" + skin_name_parsed + "_cone.p", 20f, 2.5f, false,
				defProjectileE2, 1, 0, new FixedCursorAlignment()).setSound(AudioData.ice_skill1_e2);

		// e1 empty main projectile
		DefProjectile defProjectileE1 = new DefProjectile(300,
				getCollisionFilterWithProjGround(CollisionData.BIT_GROUND + CollisionData.BIT_CHARACTER_PROJECTILE))
						.setVelocity(15f).addSuccessor(e3_ground_frozen).addSuccessor(e4_feedback_enemy)
						.addSuccessor(e5_feedback_ally).setFlyThroughTargets();

		e1_main_projectile = new EmissionDefAngleRanged(
				"graphic/particles/particle_skill/ice_skill1_e1_" + skin_name_parsed + "_main_projectile.p", 1f, 1f,
				true, defProjectileE1, NUMBER_CONE_SHARDS, 250, 40f, NUMBER_CONE_SHARDS, new RandomOrder(),
				new FixedCursorAlignment()).addSuccessorTimewise(0, e2_cone);
	}

	@Override
	protected Emission createFirstEmission() {
		e1_main_projectile.setCenter(player_pos);
		return new Emission(e1_main_projectile, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e2_cone);
		emissions.add(e3_ground_frozen);
		emissions.add(e4_feedback_enemy);
		emissions.add(e5_feedback_ally);
	}

}
