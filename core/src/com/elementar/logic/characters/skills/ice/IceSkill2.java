package com.elementar.logic.characters.skills.ice;

import java.util.ArrayList;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.alignment.SteeringAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class IceSkill2 extends AMainSkill {

	private AEmissionDef e4_feedback_enemy, e3_explosion, e2_spear_ground, e1_main_projectile;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public IceSkill2(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/ice_skill2_charge_" + skin_name_parsed + ".p", 1f, 3f, false,
				prototypeCharge, "character_weapon_front2");
	}

	@Override
	protected void defineEmissions() {

		/*
		 * e4, feedback. Currently empty.
		 */
		DefProjectile defProjectileE4 = new DefProjectile(3000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e4_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/ice_skill2_e4_" + skin_name_parsed + "_feedback_enemy.p", 1f, 2f,
				false, defProjectileE4, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setDestroyPrevProjectileAfterGround(false)
						.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(0).getDamage()));

		/*
		 * e3, explosion after spear is stucked in the ground for a while (after life)
		 * or after contact with enemy
		 */
		DefProjectile defProjectileE3 = new DefProjectile(1000,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE))
						.addSuccessor(e4_feedback_enemy).setFlyThroughTargets();

		e3_explosion = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/ice_skill2_e3_" + skin_name_parsed + "_spear_explosion.p", 30f, 4f,
				false, defProjectileE3, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue() + CollisionKinds.AFTER_LIFETIME.getValue()))
						.setOneTouchMechanism().setSound(AudioData.ice_skill2_e3);

		DefProjectile defProjectileE2 = new DefProjectile(1000, getNeutralFilter()).setRectangleShape(3, 2)
				.setOffsetShape(2f);

		e2_spear_ground = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/ice_skill2_e2_" + skin_name_parsed + "_spear_ground.p", 1f, 2f, false,
				defProjectileE2, 1, 0, new SteeringAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setSound(AudioData.ice_skill2_e2);

		// e1, ice projectile
		DefProjectile defProjectileE1 = new DefProjectile(5000,
				getCollisionFilterWithProjGround(CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_GROUND))
						.addSuccessor(e2_spear_ground).addSuccessor(e3_explosion).setVelocity(11f)
						.setStopAfterGroundCollision();

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/ice_skill2_e1_" + skin_name_parsed + "_spear.p", 3f, 2f, false,
				defProjectileE1, 1, 0, new FixedCursorAlignment()).setSound(AudioData.ice_skill2_e1).setOffset(0.1f);

	}

	@Override
	protected Emission createFirstEmission() {
		e1_main_projectile.setCenter(player_pos);
		return new Emission(e1_main_projectile, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_main_projectile);
		emissions.add(e2_spear_ground);
		emissions.add(e3_explosion);
		emissions.add(e4_feedback_enemy);
	}

}