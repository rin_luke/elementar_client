package com.elementar.logic.characters.skills.ice;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.physics.box2d.Filter;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.ABasicSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.Projectile;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class IceSkill0 extends ABasicSkill {

	private static float VELOCITY = 8f;

	private AEmissionDef e1_main_projectile, e2_collision, e3_feedback_enemy;

	private ArrayList<Emission> emission_list;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public IceSkill0(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
		emission_list = new ArrayList<>();
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/ice_skill0_charge_" + skin_name_parsed + ".p", 1f, 3f, false,
				prototypeCharge, "character_weapon_front2");
	}

	@Override
	protected void defineEmissions() {

		// e3, feedback when E1 projectile hits enemy
		DefProjectile defProjectileE3 = new DefProjectile(2000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/ice_skill0_e3_" + skin_name_parsed + "_feedback_enemy.p", 1, 3f, true,
				defProjectileE3, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(0).getDamage()));

		// e2, feedback when E1 projectile hits enemy
		DefProjectile defProjectileE2 = new DefProjectile(1000, getNeutralFilter());

		e2_collision = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/ice_skill0_e2_" + skin_name_parsed + "_collision.p", 1, 1f, false,
				defProjectileE2, 1, 0, new ObstacleAlignment())
						.setSound(AudioData.ice_skill0_e2)
						.setStartConditions(
								new StartConditions(CollisionData.BIT_GROUND + CollisionData.BIT_CHARACTER_PROJECTILE,
										CollisionKinds.CAST_ON_ENEMY.getValue()
												+ CollisionKinds.AFTER_LIFETIME.getValue()))
						.setRemoveFromSucclistAfterGroundCollision(false);

		// e1, ice projectile
		Filter filter = getCollisionFilterWithProjGround(CollisionData.BIT_ICE_SKILL0_PROJECTILE
				+ CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_GROUND);
		filter.categoryBits |= CollisionData.BIT_ICE_SKILL0_PROJECTILE;

		DefProjectile defProjectileE1 = new DefProjectile(10000, filter).addSuccessor(e2_collision)
				.addSuccessor(e3_feedback_enemy).setVelocity(VELOCITY).setTimeUntilStop(210).setRangeUntilDeath(4f);

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/ice_skill0_e1_" + skin_name_parsed + "_main_projectile.p", 2f, 1.5f,
				false, defProjectileE1, 1,0, new FixedCursorAlignment())
						.setSound(AudioData.ice_skill0_e1);
	}

	@Override
	protected Emission createFirstEmission() {

		e1_main_projectile.setCenter(player_pos);
		Emission e = new Emission(e1_main_projectile, physical_client);
		emission_list.add(e);

		return e;

	}

	@Override
	public void release() {

		// following sets release flag
		super.release();

		Iterator<Emission> it = emission_list.iterator();
		Emission e;
		while (it.hasNext() == true) {
			e = it.next();
			e.getDef().getDefProjectile().setTimeUntilStop(Integer.MAX_VALUE);
			if (e.getEmittedProjectiles().size() > 0) {
				for (Projectile p : e.getEmittedProjectiles())
					p.setVelocity(p.getBody(), VELOCITY);

			}
			it.remove();
		}
	}
}
