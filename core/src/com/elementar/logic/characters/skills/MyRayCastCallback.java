package com.elementar.logic.characters.skills;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.Util;

public class MyRayCastCallback implements RayCastCallback {

	private float fraction_lowest;
	private int ray_collision_count = 0;
	private Vector2 normal = new Vector2();

	private MyRayCastCallback() {

	}

	@Override
	public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {

		if ((fixture.getFilterData().categoryBits & CollisionData.BIT_GROUND) != 0) {

			ray_collision_count++;

			if (fraction_lowest > fraction) {
				fraction_lowest = fraction;
				this.normal.set(normal);
			}

			return 1;
		}

		return -1;
	}

	private void reset() {
		fraction_lowest = Float.MAX_VALUE;
		ray_collision_count = 0;
		normal.set(0, 0);
	}

	private boolean isPositionOutside() {
		if ((ray_collision_count % 2) == 0)
			// even
			return true;
		else
			// odd
			return false;
	}

	/**
	 * 
	 * @param world
	 * @param sourcePos, assumption: this point is always outside of an island.
	 * @param outPos
	 * @return
	 */
	private static Vector2 calcClosestOutPos(World world, Vector2 sourcePos, Vector2 outPos) {

		MyRayCastCallback cb = new MyRayCastCallback();

		world.rayCast(cb, sourcePos, outPos);

		Vector2 closestOutPos = new Vector2();

		float offset = 0.5f;

		if (cb.isPositionOutside() == false) {

			/*
			 * we take this point in case the next rays don't find any other point
			 */
			closestOutPos = Util.addVectors(sourcePos,
					Util.getPolarCoords((sourcePos.dst(outPos) * cb.fraction_lowest) - offset,
							Util.getAngleBetweenTwoPoints(sourcePos, outPos)));

			int numberRays = 6;
			int rayLength = 5;
			short angle = Util.getAngleBetweenTwoPoints(sourcePos, outPos);
			int segment = 360 / numberRays;

			// find lowest fraction
			float lowestFraction = Float.MAX_VALUE;
			float resultingAngle = 0;

			for (int i = 0; i < numberRays; i++) {
				cb.reset();

				world.rayCast(cb, outPos, Util.addVectors(outPos, Util.getPolarCoords(rayLength, angle + i * segment)));

				if (cb.fraction_lowest < lowestFraction) {
					lowestFraction = cb.fraction_lowest;
					resultingAngle = angle + i * segment;
				}

			}

			Vector2 potentialSolution = Util.addVectors(outPos,
					Util.getPolarCoords((rayLength * lowestFraction) + offset, resultingAngle));

			if (closestOutPos.dst(outPos) > potentialSolution.dst(outPos))
				closestOutPos.set(potentialSolution);

		} else {
			/*
			 * here we need to check whether the cursor pos is too close to an island. To
			 * accomplish that we use also ray casting in several directions
			 */
			int numberRays = 6;
			float rayLength = offset;
			int segment = 360 / numberRays;
			Vector2 offsetVector;
			for (int i = 0; i < numberRays; i++) {
				cb.reset();

				offsetVector = Util.getPolarCoords(rayLength, i * segment);

				world.rayCast(cb, outPos, Util.addVectors(outPos, offsetVector));

				if (cb.fraction_lowest < Float.MAX_VALUE) {
					/*
					 * if there's a collision we walk in opposite direction
					 */
					cb.normal.setLength(offset);
					offsetVector.setLength(offset * cb.fraction_lowest);
					return Util.addVectors(Util.addVectors(outPos, offsetVector), cb.normal);
				}

			}
			closestOutPos.set(outPos);
		}

		return closestOutPos;

	}

	public static Vector2 calcClosestOutPos(PhysicalClient physicalClient, Vector2 outPos) {
		return calcClosestOutPos(physicalClient.getWorld(), physicalClient.pos, outPos);
	}

}
