package com.elementar.logic.characters.skills.chaos;

import java.util.ArrayList;

import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AChargeSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.def.AEmissionDef;

public class ChaosSkill1 extends AChargeSkill {

	public ChaosSkill1(MetaSkill metaSkill, PhysicalClient physicalClient, String unparsedSkinName) {
		super(metaSkill,physicalClient, null, unparsedSkinName);
	}

	@Override
	protected void defineChargeEmission() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void defineEmissions() {

	}

	@Override
	protected Emission createFirstEmission() {
		return null;
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		// TODO Auto-generated method stub
	}

}