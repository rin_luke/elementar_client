package com.elementar.logic.characters.skills.chaos;

import com.badlogic.gdx.physics.box2d.Filter;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.ABasicSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.characters.skills.MyRayCastCallback;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.emission.effect.EffectHeal;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class ChaosSkill0 extends ABasicSkill {

	private AEmissionDef e4_feedback_dmg, e3_feedback_heal, e2_bubble, e1_build_up_bubble;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public ChaosSkill0(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile(1150, getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/water_skill0_charge_" + skin_name_parsed + ".p", 1f, 3f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e4, dmg feedback
		DefProjectile defProjectileE4 = new DefProjectile(1000, getNeutralFilter());

		e4_feedback_dmg = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill0_e4_" + skin_name_parsed + "_feedback_enemy.p", 1f, 1f,
				false, defProjectileE4, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.addEffect(new EffectDamage(-20));

		// e3, heal feedback
		DefProjectile defProjectileE3 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_heal = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill0_e3_" + skin_name_parsed + "_feedback_ally.p", 1f, 3f,
				false, defProjectileE3, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_EXCLUDED.getValue()))
						.setAttached().addEffect(new EffectHeal((short) 20));

		// e2, bubble
		Filter f = getCollisionFilterWithProjGround(CollisionData.BIT_CHARACTER_PROJECTILE);

		DefProjectile defProjectileE2 = new DefProjectile(20000, f).addSuccessor(e3_feedback_heal)
				.addSuccessor(e4_feedback_dmg);

		e2_bubble = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill0_e2_" + skin_name_parsed + "_bubble.p", 3f, 3.5f, false,
				defProjectileE2, 1, 0, new ObstacleAlignment()).setStartConditions(
						new StartConditions(CollisionData.BIT_GROUND, CollisionKinds.AFTER_LIFETIME.getValue()));

		// e1, buildup projectile for bubble
		DefProjectile defProjectileE1 = new DefProjectile(1150, getNeutralFilter()).addSuccessor(e2_bubble)
				.setFlyThroughTargets();

		e1_build_up_bubble = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill0_e1_" + skin_name_parsed + "_buildup_bubble.p", 1f, 3f,
				false, defProjectileE1, 1, 0, new FixedAlignment(0));

	}

	@Override
	protected Emission createFirstEmission() {

		e1_build_up_bubble.setCenter(MyRayCastCallback.calcClosestOutPos(physical_client, physical_client.cursor_pos));

		return new Emission(e1_build_up_bubble, physical_client);
	}

}
