package com.elementar.logic.characters.skills;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.ParticleContainer;
import com.elementar.data.container.util.CustomParticleEffect;
import com.elementar.data.container.util.Gameclass.GameclassName;
import com.elementar.gui.modules.ingame.APlayerUIModule;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.ExecutionMode;

public class MetaSkill {

	public static float THUMBNAIL_DIMENSIONS_SELECTION = 100f, THUMBNAIL_DIMENSIONS_WORLD = 108f;

	private static int RECENT_ID = 0;

	private int meta_skill_id;
	private Sprite thumbnail_world_big, thumbnail_world_comboslot, thumbnail_selection, thumbnail_after_absorption;

	private GameclassName gameclass_name;

	private CustomParticleEffect combohand_front_particle, combohand_back_particle;

	private String order;

	private String name_key;
	private String description_key;
	private int p1_version;
	private float p1_time_scale;
	private float cooldown;
	private int mana_cost;
	private float combo_factor_prim, combo_factor_added;

	private ArrayList<MetaFeedback> feedbacks = new ArrayList<>();

	public MetaSkill() {

		meta_skill_id = RECENT_ID;
		RECENT_ID++;

	}

	/**
	 * constructor for {@link TransferSkill}
	 * 
	 * @param gameclassName
	 * @param order
	 */
	public MetaSkill(GameclassName gameclassName, String order) {
		this();
		gameclass_name = gameclassName;
		this.order = order;
	}

	public void loadThumbnail(String skillThumbnailString) {

		if (Shared.EXECUTION_MODE != ExecutionMode.CLIENT)
			// no visuals needed
			return;

		// load thumbnails
		if (skillThumbnailString != null) {
			skillThumbnailString = "characters/particle_sprites/" + skillThumbnailString;

			// set thumbnails
			thumbnail_world_big = AnimationContainer.createCharacterSprite(skillThumbnailString);
			thumbnail_world_big.setSize(THUMBNAIL_DIMENSIONS_WORLD * APlayerUIModule.SCALE_UI,
					THUMBNAIL_DIMENSIONS_WORLD * APlayerUIModule.SCALE_UI);

			thumbnail_world_comboslot = AnimationContainer.createCharacterSprite(skillThumbnailString);
			thumbnail_world_comboslot.setSize(65 * APlayerUIModule.SCALE_UI, 65 * APlayerUIModule.SCALE_UI);

			thumbnail_selection = AnimationContainer.createCharacterSprite(skillThumbnailString);
			thumbnail_selection.setSize(THUMBNAIL_DIMENSIONS_SELECTION, THUMBNAIL_DIMENSIONS_SELECTION);
			thumbnail_selection.setOriginCenter();

			thumbnail_after_absorption = AnimationContainer.createCharacterSprite(skillThumbnailString);
			thumbnail_after_absorption.setSize(THUMBNAIL_DIMENSIONS_WORLD * 1.2f, THUMBNAIL_DIMENSIONS_WORLD * 1.2f);
			thumbnail_after_absorption.setAlpha(0f);
		}
	}

	public void loadComboHandEffects(String comboHandPath) {

		if (Shared.EXECUTION_MODE != ExecutionMode.CLIENT)
			// no visuals needed
			return;

		// load particle effect prototypes
		if (comboHandPath != null) {
			combohand_front_particle = ParticleContainer.loadCombohandParticle(
					"graphic/particles/particle_skill/" + comboHandPath, gameclass_name, "character_hand_front_combo",
					false);

			combohand_back_particle = ParticleContainer.loadCombohandParticle(
					"graphic/particles/particle_skill/" + comboHandPath, gameclass_name, "character_hand_back_combo",
					false);
		}
	}

	public int getMetaSkillID() {
		return meta_skill_id;
	}

	public Sprite getThumbnailWorldBig() {
		return thumbnail_world_big;
	}

	public Sprite getThumbnailSelection() {
		return thumbnail_selection;
	}

	public Sprite getThumbnailWorldComboslot() {
		return thumbnail_world_comboslot;
	}

	public Sprite getThumbnailAfterAbsorption() {
		return thumbnail_after_absorption;
	}

	public CustomParticleEffect getCombohandFrontEffect() {
		return combohand_front_particle;
	}

	public CustomParticleEffect getCombohandBackEffect() {
		return combohand_back_particle;
	}

	public void setNameKey(String nameKey) {
		this.name_key = nameKey;
	}

	public String getNameKey() {
		return name_key;
	}

	public void setCooldown(float cooldown) {
		this.cooldown = cooldown;
	}

	public float getCooldown() {
		return cooldown;
	}

	public void setManaCost(int manaCost) {
		this.mana_cost = manaCost;
	}

	public int getManaCost() {
		return mana_cost;
	}

	public void setP1TimeScale(float timeScaleP1) {
		this.p1_time_scale = timeScaleP1;
	}

	public float getP1TimeScale() {
		return p1_time_scale;
	}

	public void setP1Version(int p1Version) {
		this.p1_version = p1Version;
	}

	public int getP1Version() {
		return p1_version;
	}

	public void setComboFactorPrim(float comboFactorPrim) {
		this.combo_factor_prim = comboFactorPrim;
	}

	public float getComboFactorPrim() {
		return combo_factor_prim;
	}

	public void setComboFactorAdded(float comboFactorAdded) {
		this.combo_factor_added = comboFactorAdded;
	}

	public float getComboFactorAdded() {
		return combo_factor_added;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getOrder() {
		return order;
	}

	public void setGameclassName(GameclassName gameclassName) {
		gameclass_name = gameclassName;
	}

	public GameclassName getGameclassName() {
		return gameclass_name;
	}

	public ArrayList<MetaFeedback> getFeedbacks() {
		return feedbacks;
	}

	public void setDescriptionKey(String descriptionKey) {
		description_key = descriptionKey;
	}

	public String getDescriptionKey() {
		return description_key;
	}

}
