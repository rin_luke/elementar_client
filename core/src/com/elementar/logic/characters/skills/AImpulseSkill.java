package com.elementar.logic.characters.skills;

import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;

public abstract class AImpulseSkill extends AMainSkill {

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param unparsedSkinName
	 */
	public AImpulseSkill(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String unparsedSkinName) {
		super(metaSkill, physicalClient, drawableClient, unparsedSkinName);
	}

}
