package com.elementar.logic.characters.skills.stone;

import java.util.ArrayList;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.CursorFollowingWhileEmittingAlignment;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.emission.effect.EffectStun;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class StoneSkill2 extends AMainSkill {

	private AEmissionDef e0_ground_sensor, e1_outburst, e2_main_projectile, e3_big_stone_feedback_ground,
			e4_big_stone_explosion, e5_big_stone_feedback_ally, e6_big_stone_feedback_enemy;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public StoneSkill2(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/stone_skill2_charge_" + skin_name_parsed + ".p", 1f, 3f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e6 feedback at enemy after big stone explosion
		DefProjectile defProjectileE6 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e6_big_stone_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/stone_skill2_e6_" + skin_name_parsed + "_big_stone_feedback_enemy.p",
				1, 4, false, defProjectileE6, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()));
		short poolIDE6 = e6_big_stone_feedback_enemy.getPoolID();
		e6_big_stone_feedback_enemy.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(0).getDamage()))
				.addEffect(new EffectStun(poolIDE6, meta_skill.getFeedbacks().get(0).getStunDuration())
						.setThumbnail(getThumbnailInWorld(), true));

		// e5 feedback ally after big stone explosion
		DefProjectile defProjectileE5 = new DefProjectile(1000, getNeutralFilter());

		e5_big_stone_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/stone_skill2_e5_" + skin_name_parsed + "_big_stone_feedback_ally.p",
				1, 1, false, defProjectileE5, 1, 0, new FixedAlignment(90))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_INCLUDED.getValue()));

		// e4 big stone explosion
		DefProjectile defProjectileE4 = new DefProjectile(1000,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE)).setFlyThroughTargets()
						.addSuccessor(e6_big_stone_feedback_enemy).addSuccessor(e5_big_stone_feedback_ally);

		e4_big_stone_explosion = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/stone_skill2_e4_" + skin_name_parsed + "_big_stone_explosion.p", 45,
				6, false, defProjectileE4, 1, 0, new FixedAlignment(90))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_INCLUDED.getValue()
										+ CollisionKinds.CAST_ON_ENEMY.getValue()
										+ CollisionKinds.AFTER_LIFETIME.getValue()))
//						.addSuccessorTimewise(0, e7_small_stones)
						.setSound(AudioData.stone_skill2_e4);

		// e3 feedback when big stone hits the ground
		DefProjectile defProjectileE3 = new DefProjectile(1000, getNeutralFilter());

		e3_big_stone_feedback_ground = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/stone_skill2_e3_" + skin_name_parsed + "_big_stone_feedback_ground.p",
				1, 1.5f, false, defProjectileE3, 1, 0, new ObstacleAlignment())

						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setRemoveFromSucclistAfterGroundCollision(false).setSound(AudioData.stone_skill2_e3);

		// e2 big stone
		DefProjectile defProjectileE2 = new DefProjectile(5000,
				getCollisionFilterWithProjGround(CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_GROUND))
						.setVelocity(3f).setGravityScale(0.6f).setRestitution(0.3f).setFriction(0.4f)// .setPolygonShape(6)
						.addSuccessor(e4_big_stone_explosion.setDestroyPrevProjectileAfterGround(false))
						.addSuccessor(e3_big_stone_feedback_ground.setDestroyPrevProjectileAfterGround(false));

		e2_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/stone_skill2_e2_" + skin_name_parsed + "_main_projectile.p", 9f, 2.1f,
				true, defProjectileE2, 1, 0, new ObstacleAlignment()).setPersecuteeAfterCollision()
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0));

		// e1 outburst after ground sensor collides with ground
		DefProjectile defProjectileE1 = new DefProjectile(1000,
				getCollisionFilterWithProjGround(CollisionData.BIT_CHARACTER_PROJECTILE)).setContinuousScaling(3f, 400);

		e1_outburst = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/stone_skill2_e1_" + skin_name_parsed + "_outburst.p", 2f, 2f, false,
				defProjectileE1, 1, 0, new ObstacleAlignment()).setSound(AudioData.stone_skill2_e1)
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0));

		// e0 detecting sensor
		DefProjectile defProjectileE0 = new DefProjectile(5000,
				getCollisionFilterWithProjGround(CollisionData.BIT_GROUND)).setVelocity(20f)
						.addSuccessor(e2_main_projectile).addSuccessor(e1_outburst).setStopAfterGroundCollision();

		e0_ground_sensor = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/stone_skill2_e0_" + skin_name_parsed + "_ground_sensor.p", 0.5f, 0.5f,
				false, defProjectileE0, 1, 0, new CursorFollowingWhileEmittingAlignment())
						.setSound(AudioData.stone_skill2_e0);

	}

	@Override
	protected Emission createFirstEmission() {
		e0_ground_sensor.setCenter(player_pos);
		return new Emission(e0_ground_sensor, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_outburst);
		emissions.add(e2_main_projectile);
		emissions.add(e3_big_stone_feedback_ground);
		emissions.add(e4_big_stone_explosion);
		emissions.add(e5_big_stone_feedback_ally);
		emissions.add(e6_big_stone_feedback_enemy);
	}

}