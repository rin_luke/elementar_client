package com.elementar.logic.characters.skills.stone;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.ABasicSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class StoneSkill0 extends ABasicSkill {

	private AEmissionDef e2_feedback_enemy, e1_punch;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public StoneSkill0(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/stone_skill0_charge_" + skin_name_parsed + ".p", 1f, 4f, false,
				prototypeCharge, "character_hand_front").setSound(AudioData.stone_skill0_e1);
	}

	@Override
	protected void defineEmissions() {

		// e2 feedback enemy
		DefProjectile defProjectileE2 = new DefProjectile(1000, getNeutralFilter());

		e2_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/stone_skill0_e2_" + skin_name_parsed + "_feedback_enemy.p", 1f, 2f,
				false, defProjectileE2, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setDestroyPrevProjectileAfterGround(false).setSound(AudioData.stone_skill0_e2)
						.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(0).getDamage()));

		// e1
		DefProjectile defProjectileE1 = new DefProjectile(300,
				getCollisionFilterWithProjGround(CollisionData.BIT_CHARACTER_PROJECTILE)).setRectangleShape(1.5f, 1f)
						.addSuccessor(e2_feedback_enemy).setFlyThroughTargets()
						.setAttachmentOption(AttachmentOptionProjectile.AT_EMITTER).setOffsetAttached(1f);

		e1_punch = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/stone_skill0_e1_" + skin_name_parsed + "_main_projectile.p", 12f,
				2.5f, false, defProjectileE1, 1, 0, new FixedCursorAlignment());

	}

	@Override
	protected Emission createFirstEmission() {

		e1_punch.setCenter(player_pos);

		return new Emission(e1_punch, physical_client);
	}

}
