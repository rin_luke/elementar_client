package com.elementar.logic.characters.skills.stone;

import java.util.ArrayList;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.CursorFollowingWhileEmittingAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectEmpty;
import com.elementar.logic.emission.effect.EffectStun;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;
import com.badlogic.gdx.physics.box2d.Filter;

public class StoneSkill1 extends AMainSkill {

	private final short DURATION_WALL = 8000;
	private final float WALL_WIDTH = 2.4f;
	private final float WALL_HEIGHT = 0.1f;

	private AEmissionDef e0_ground_sensor, e1_wall_visible, e2_wall_static_empty, e3_feedback_enemy, e4_feedback_ally;

	public StoneSkill1(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/stone_skill1_charge_" + skin_name_parsed + ".p", 1f, 3f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e4 feedback ally
		DefProjectile defProjectileE4 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e4_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/stone_skill1_e4_" + skin_name_parsed + "_feedback_ally.p", 1f, 1f,
				false, defProjectileE4, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_INCLUDED.getValue()))
						.addEffect(new EffectEmpty());

		// e3 feedback enemy
		DefProjectile defProjectileE3 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/stone_skill1_e3_" + skin_name_parsed + "_feedback_enemy.p", 1f, 1f,
				false, defProjectileE3, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()));
		short poolIDE3 = e3_feedback_enemy.getPoolID();
		e3_feedback_enemy.addEffect(
				new EffectStun(poolIDE3, meta_skill.getFeedbacks().get(0).getStunDuration()).setOnlyWithCombo());

		// e2 feedback ground
		Filter f = new Filter();
		f.categoryBits = CollisionData.BIT_GROUND;
		f.maskBits = CollisionData.BIT_CHARACTER_MOVEMENT;
		f.maskBits |= CollisionData.BIT_PROJECTILE;
		f.maskBits |= CollisionData.BIT_LIGHT;

		DefProjectile defProjectileE2 = new DefProjectile(DURATION_WALL, f).setBodyType(BodyType.StaticBody)
				.setRectangleShape(WALL_WIDTH, WALL_HEIGHT).setOffsetShape(WALL_WIDTH * 0.5f).setRoundedRect();

		e2_wall_static_empty = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/stone_skill1_e2_ground_empty.p", 1f, 1f, false, defProjectileE2, 1, 0,
				new ObstacleAlignment())// .setSound(AudioData.stone_skill2_e1)
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setTriggerEmissionAllowed();

		// e1 main projectile
		DefProjectile defProjectileE1 = new DefProjectile(DURATION_WALL,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE))
						.addSuccessor(e3_feedback_enemy).addSuccessor(e4_feedback_ally)
						.setRectangleShape(WALL_WIDTH + 0.1f, WALL_HEIGHT + 0.1f).setOffsetShape(WALL_WIDTH * 0.5f)
						.setFlyThroughTargets();

		e1_wall_visible = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/stone_skill1_e1_" + skin_name_parsed + "_wall.p", 1f, 1.2f, true,
				defProjectileE1, 1, 0, new ObstacleAlignment())// .setSound(AudioData.stone_skill2_e0)
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setSound(AudioData.stone_skill1_e1);

		// e0 detecting sensor
		DefProjectile defProjectileE0 = new DefProjectile(5000,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_GROUND)).setVelocity(20f)
						.addSuccessor(e1_wall_visible).addSuccessor(e2_wall_static_empty).setStopAfterGroundCollision();

		e0_ground_sensor = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/stone_skill1_e0_" + skin_name_parsed + "_ground_sensor.p", 0.5f, 0.5f,
				false, defProjectileE0, 1, 0, new CursorFollowingWhileEmittingAlignment())
						.setSound(AudioData.stone_skill1_e0);

	}

	@Override
	protected Emission createFirstEmission() {
		e0_ground_sensor.setCenter(player_pos);
		return new Emission(e0_ground_sensor, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_wall_visible);
		emissions.add(e3_feedback_enemy);
		emissions.add(e4_feedback_ally);
	}

}