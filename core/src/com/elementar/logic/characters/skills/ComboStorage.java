package com.elementar.logic.characters.skills;

import java.util.ArrayList;

import com.elementar.data.container.AudioData;
import com.elementar.data.container.util.TweenableSound;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.effect.AEffect;

/**
 * 
 * @author lukassongajlo
 *
 */
public class ComboStorage extends SparseComboStorage {

	/**
	 * List of emission definitions defined by the skill.
	 */
	private ArrayList<AEmissionDef> combo_emissions;

	private boolean storing = false;

	private int index;

	private AMainSkill skill;

	public ComboStorage(int index) {
		clear();
		this.index = index;
	}

	/**
	 * For debugging purposes. Each combostorage (transfer, 1, 2) gets an index to
	 * distinguish them easier.
	 * 
	 * @return
	 */
	public int getIndex() {
		return index;
	}

	public void update(float delta) {

	}

	/**
	 * There are two cases where the combo storage got filled with a skill. First
	 * when a skill has to be transferred to an ally and second when the ally starts
	 * his combination.
	 * 
	 * @param skill
	 * 
	 */
	public void setSkill(ASkill skill) {

		storing = true;

		this.skill = (AMainSkill) skill;
		combo_emissions = skill.getComboEmissions();

		String effects = "";
		for (AEmissionDef def : combo_emissions)
			if (def.isFeedback() == true)
				for (AEffect effect : def.getEffects())
					effects += "\t\t" + effect.toString() + "\n";

		System.out.println("ComboStorage.setSkill()		\n" + effects);

		meta_skill_id = (byte) skill.getMetaSkill().getMetaSkillID();

		// serverside && firstProjectile == true
//		if (location != Location.SERVERSIDE)
//			combo_stored_sound.loopWithoutTween();

	}

	public ArrayList<AEmissionDef> getComboEmissions() {
		return combo_emissions;
	}

	public void clear() {
		combo_emissions = null;
		duration_ratio = -1;
		storing = false;
		meta_skill_id = (byte) -1;
	}

	@Override
	public boolean isStoring() {
		return storing;
	}

	public AMainSkill getSkill() {
		return skill;
	}

	/**
	 * Returns <code>true</code> if a combination with a skill hasn't happened yet.
	 * Once a merge happened this method returns <code>false</code>.
	 * 
	 * @return
	 */
	// public void init() {
	//
	// if (location != Location.SERVERSIDE)
	// combo_stored_sound.stop();
	//
	// }

}
