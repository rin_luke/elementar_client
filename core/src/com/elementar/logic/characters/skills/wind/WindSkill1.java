package com.elementar.logic.characters.skills.wind;

import java.util.ArrayList;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectEmpty;
import com.elementar.logic.emission.effect.EffectImpulsePrevProj;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class WindSkill1 extends AMainSkill {

	private AEmissionDef e1_main_projectile, e2_feedback_enemy, e3_feedback_ally;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public WindSkill1(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/wind_skill1_charge_" + skin_name_parsed + ".p", 1f, 1f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e3, feedback ally
		DefProjectile defProjectileE3 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/wind_skill1_e3_" + skin_name_parsed + "_feedback_ally.p", 1f, 1f,
				false, defProjectileE3, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_EXCLUDED.getValue()))
						.addEffect(new EffectEmpty());

		// e2, feedback at enemy
		DefProjectile defProjectileE2 = new DefProjectile(meta_skill.getFeedbacks().get(0).getDuration(),
				getNeutralFilter()).setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e2_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/wind_skill1_e2_" + skin_name_parsed + "_feedback_enemy.p", 1f, 1f,
				false, defProjectileE2, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()));
		short poolIDE2 = e2_feedback_enemy.getPoolID();
		e2_feedback_enemy.addEffect(new EffectImpulsePrevProj(poolIDE2, defProjectileE2.getLifeTime()));

		// e1, main projectile
		DefProjectile defProjectileE1 = new DefProjectile(1000,
				getCollisionFilterWithProjGround(CollisionData.BIT_CHARACTER_PROJECTILE)).setVelocity(6f)
						.addSuccessor(e2_feedback_enemy).setFlyThroughTargets();

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/wind_skill1_e1_" + skin_name_parsed + "_main_projectile.p", 13f, 3f,
				true, defProjectileE1, 1, 0, new FixedCursorAlignment()).setSound(AudioData.wind_skill1_e1);

	}

	@Override
	protected Emission createFirstEmission() {
		e1_main_projectile.setCenter(player_pos);
		return new Emission(e1_main_projectile, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_main_projectile);
		emissions.add(e2_feedback_enemy);
		emissions.add(e3_feedback_ally);
	}

}
