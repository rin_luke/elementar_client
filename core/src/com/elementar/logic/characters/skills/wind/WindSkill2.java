package com.elementar.logic.characters.skills.wind;

import java.util.ArrayList;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AImpulseSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.CursorFollowingWhileEmittingAlignment;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDisarmed;
import com.elementar.logic.emission.effect.EffectEmpty;
import com.elementar.logic.emission.effect.EffectImpulseCursorFollowing;
import com.elementar.logic.emission.effect.EffectImpulseY;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class WindSkill2 extends AImpulseSkill {

	private AEmissionDef e1_main_projectile, e2_feedback_enemy, e3_feedback_ally;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public WindSkill2(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getGroundFilter())
				.setFlyThroughTargets();
		charge_def = new EmissionDefBoneFollowing("graphic/particles/particle_skill/wind_skill2_charge_empty.p", 1f, 1f,
				false, prototypeCharge, "character_hand_front");
		charge_def.setAlignment(new CursorFollowingWhileEmittingAlignment());

		short poolID = charge_def.getPoolID();

		charge_def.addEffect(
				new EffectImpulseCursorFollowing(poolID, meta_skill.getFeedbacks().get(0).getDisarmedDuration(), 5f)
						.setMovementImpulse());
		charge_def.setTarget(physical_client);

		recoil_flag = false;

	}

	@Override
	protected void defineEmissions() {

		// e3, feedback ally
		DefProjectile defProjectileE3 = new DefProjectile(meta_skill.getFeedbacks().get(0).getDisarmedDuration(),
				getNeutralFilter()).setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/wind_skill2_e3_" + skin_name_parsed + "_feedback_ally.p", 1, 2, false,
				defProjectileE3, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_EXCLUDED.getValue()))
						.addEffect(new EffectEmpty());

		// e2, feedback enemy
		DefProjectile defProjectileE2 = new DefProjectile(meta_skill.getFeedbacks().get(0).getDisarmedDuration(),
				getNeutralFilter()).setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e2_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/wind_skill2_e2_" + skin_name_parsed + "_feedback_enemy.p", 1, 2,
				false, defProjectileE2, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setSound(AudioData.wind_skill2_e2);
		short poolIDE2 = e2_feedback_enemy.getPoolID();
		e2_feedback_enemy.addEffect(new EffectImpulseY(poolIDE2, defProjectileE2.getLifeTime(), 2)).addEffect(
				new EffectDisarmed(poolIDE2, defProjectileE2.getLifeTime()).setThumbnail(getThumbnailInWorld(), true));

		// e1
		DefProjectile defProjectileE1 = new DefProjectile(meta_skill.getFeedbacks().get(0).getDisarmedDuration(),
				getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE))
						.addSuccessor(e2_feedback_enemy).addSuccessor(e3_feedback_ally)
						.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET).setFlyThroughTargets()
						.setVelocity(0.02f);

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/wind_skill2_e1_" + skin_name_parsed + "_main_projectile.p", 10f, 2f,
				false, defProjectileE1, 1, 0, new CursorFollowingWhileEmittingAlignment()).setOneTouchMechanism()
						.setSound(AudioData.wind_skill2_e1);
	}

	@Override
	protected Emission createFirstEmission() {
		e1_main_projectile.setCenter(player_pos);
		e1_main_projectile.setTarget(physical_client);
		return new Emission(e1_main_projectile, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_main_projectile);
		emissions.add(e2_feedback_enemy);
		emissions.add(e3_feedback_ally);
	}

}
