package com.elementar.logic.characters.skills;

import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.effect.EffectVelChange;
import com.elementar.logic.emission.effect.EffectYChargeCorrection;
import com.esotericsoftware.spine.Animation;

/**
 * For all charging skills
 * 
 * @author lukassongajlo
 * 
 */
public abstract class AChargeSkill extends ASkill {

	protected AEmissionDef charge_def;

	protected boolean release_flag = false;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param unparsedSkinName
	 */
	public AChargeSkill(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String unparsedSkinName) {
		super(metaSkill, physicalClient, drawableClient, unparsedSkinName);

		/*
		 * each charge definition has a life time which is as long as the animation. We
		 * add some additional life time (=300) to improve UX.
		 */
		if (charge_def == null)
			return;
		charge_def.setTransmissible(false);

		if (recoil_flag == false)
			return;

		charge_def.getDefProjectile().setLifeTime((int) (animation_duration * 1000) + 300);

		short poolID = charge_def.getPoolID();

		// y dir
		charge_def.addEffect(new EffectYChargeCorrection());
		// x dir
		charge_def.addEffect(new EffectVelChange(poolID, -0.4f, 440));
		charge_def.setTarget(physical_client);
	}

	@Override
	public boolean init() {

		// reset release flag
		release_flag = false;

		if (super.init() == false)
			return false;

		if (charge_def == null)
			return false;

		/*
		 * for charge, the center pos is necessary for playing sound correctly
		 */
		charge_def.setCenter(player_pos);

		charge_def.setBonesModel(drawable_client);

		Emission e = new Emission(charge_def, physical_client);
		physical_client.getEmissionList().add(e);

		/*
		 * there are skills like lightningSkill0 which would unfold an actual emission
		 * tree, on both side (server- and clientside). As a result we don't see double
		 * charge projectiles (since transmissible = false) but double e1, e2, etc. To
		 * avoid we remove successors of the client sided charge emission.
		 */
		/*
		 * EDIT: the following code makes sense, yet we need to unfold the charge tree
		 * for DyingSkill's emissions and LightningSkill0's charge emission does not
		 * have successors anymore. So the code is deprecated.
		 */
//		if (physical_client.getLocation() == Location.CLIENTSIDE_MULTIPLAYER) {
//			e.getDef().getSuccessorsTimewise().clear();
//			e.getDef().getDefProjectile().getSuccessors().clear();
//		}
		return true;

	}

	/**
	 * returns charge emission.
	 */
	public Emission executeChargeEmission() {
		if (charge_def == null)
			return null;

		useComboStorages();

		/*
		 * for charge, the center pos is necessary for playing sound correctly
		 */
		charge_def.setCenter(player_pos);

		charge_def.setBonesModel(drawable_client);

		Emission e = null;
		physical_client.getEmissionList().add(e = new Emission(charge_def, physical_client));

		return e;
	}

	@Override
	protected void setAnimationDuration() {
		/*
		 * charging skills are made of 2 phases (p1 and p2). p1 animation is as long as
		 * the cast time and p2 duration is calculated as you can see below
		 */
		Animation a1 = skeleton.getData().findAnimation("character_charge_p1_" + p1_version);
		if (a1 == null)
			return;
		animation_duration = a1.getDuration() / time_scale_p1;

	}

	protected void releaseExecution() {

	}

	@Override
	protected void updateTrack1() {
		physical_client.info_animations.track_1 = AnimationName.P1_CHARGE;

	}

	public void setFirerate(float firerate) {
		cooldown_modifiable_absolute = cooldown_absolute / firerate;
		time_scale_p1 = firerate * time_scale_p1_orig;
		setAnimationDuration();
	}

	public void release() {
		release_flag = true;
	}

}
