package com.elementar.logic.characters.skills;

import java.util.ArrayList;

import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.emission.def.AEmissionDef;

public abstract class ABasicSkill extends AChargeSkill {

	public ABasicSkill(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String unparsedSkinName) {
		super(metaSkill, physicalClient, drawableClient, unparsedSkinName);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {

	}

}