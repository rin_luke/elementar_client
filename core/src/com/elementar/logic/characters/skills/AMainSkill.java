package com.elementar.logic.characters.skills;

import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;

public abstract class AMainSkill extends AChargeSkill {

	/**
	 * combo scaling factor when this is the primary skill. If this scaling factor
	 * gets coupled with the combo scaling factor of the added skill we get a ratio
	 * to scale the added effects.
	 */
	protected float combo_factor_prim = 1;

	/**
	 * combo scaling factor when this is the added skill. If this scaling factor
	 * gets coupled with the combo scaling factor of the primary skill we get a
	 * ratio to scale the added effects.
	 */
	protected float combo_factor_added = 1;

	public AMainSkill(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String unparsedSkinName) {
		super(metaSkill, physicalClient, drawableClient, unparsedSkinName);

		show_cooldown_err = true;

		if (metaSkill != null) {
			combo_factor_prim = metaSkill.getComboFactorPrim();
			combo_factor_added = metaSkill.getComboFactorAdded();
		}
	}

	public float getComboFactorPrim() {
		return combo_factor_prim;
	}

	public float getComboFactorAdded() {
		return combo_factor_added;
	}

}
