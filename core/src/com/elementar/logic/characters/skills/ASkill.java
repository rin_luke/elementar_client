
package com.elementar.logic.characters.skills;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.World;
import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.AudioData;
import com.elementar.data.container.ParticleContainer;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.characters.skills.light.LightSkill2;
import com.elementar.logic.characters.skills.lightning.LightningSkill2;
import com.elementar.logic.characters.skills.sand.SandSkill2;
import com.elementar.logic.characters.skills.void_.VoidSkill0;
import com.elementar.logic.characters.skills.wind.WindSkill2;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.EmissionRuntimeData.ComboType;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleRanged;
import com.elementar.logic.emission.effect.AEffect;
import com.elementar.logic.emission.effect.AEffectTimed;
import com.elementar.logic.emission.effect.EffectEnableCombining;
import com.elementar.logic.emission.order.RandomOrder;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.ErrorMessage;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Shared.Team;
import com.esotericsoftware.spine.Skeleton;

/**
 * See also "skill_ingredient_list.pdf" in dropbox
 * 
 * Each skill might have several timelines running parallel (imagine the player
 * activates they basic attack in a row). Each timeline is called -skill unit-
 * and is stored in {@link #skill_units}.
 * 
 * @author lukassongajlo
 * 
 */
public abstract class ASkill {

	private static final int ARTIFICIAL_TRAINING_DELAY = 50;

	protected MetaSkill meta_skill;

	protected ArrayList<AEmissionDef> combo_defs;

	protected DrawableClient drawable_client;

	protected int p1_version = -1;

	/**
	 * in seconds
	 */
	protected float animation_duration;

	/**
	 * unparsed would be 'character_wind_skin0' and parsed just 'skin0'
	 */
	protected String skin_name_parsed;

	/**
	 * Skeleton is necessary to define {@link #animation_duration} in
	 * {@link #setAnimationDuration()}
	 */
	protected Skeleton skeleton;

	protected float time_scale_p1;

	protected float time_scale_p1_orig;

	/**
	 * if <code>true</code> the player will brake after activating the skill
	 */
	protected boolean walking_brake = false;

	protected int table_skill_index;

	protected float cooldown_current;

	protected float cooldown_time_stamp;

	protected short mana_cost;

	/**
	 * Each skill has an absolute origin value (at the beginning, unchangeable) and
	 * an absolute, changeable value (might changes when the player gets a buff
	 * etc.)
	 * <p>
	 * in seconds.
	 */
	protected float cooldown_modifiable_absolute, cooldown_absolute;

	/**
	 * Holds the team membership info of the skill user
	 */
	protected Team team_user;

	protected World world;

	protected Vector2 player_pos, player_vel, cursor_pos;

	protected short angle_between_player_and_cursor;

	protected PhysicalClient physical_client;

	protected Body body;

	/**
	 * in seconds
	 */
	protected float elapsed_time;

	protected boolean show_cooldown_err = false;

	protected boolean recoil_flag = true;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param unparsedSkinName
	 */
	public ASkill(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String unparsedSkinName) {

		meta_skill = metaSkill;

		drawable_client = drawableClient;

		if (metaSkill != null) {

			p1_version = metaSkill.getP1Version();

			mana_cost = (short) metaSkill.getManaCost();

			cooldown_modifiable_absolute = cooldown_absolute = metaSkill.getCooldown();

			time_scale_p1_orig = time_scale_p1 = metaSkill.getP1TimeScale();

		}
		skeleton = AnimationContainer.makeCharacterAnimationForWorld();
		setAnimationDuration();

		physical_client = physicalClient;
		if (physicalClient != null) {
			world = physicalClient.getHitbox().getWorld();
			team_user = physicalClient.getHitbox().getTeam();
			body = physicalClient.getHitbox().getBody();
			player_pos = physicalClient.pos;
			player_vel = physicalClient.vel;
			cursor_pos = physicalClient.cursor_pos;
		}

		// parse the unparsed skin name
		skin_name_parsed = unparsedSkinName != null
				? unparsedSkinName.substring(unparsedSkinName.length() - 5, unparsedSkinName.length())
				: null;
		// on server side
		if (skin_name_parsed == null && physicalClient != null)
			skin_name_parsed = "skin" + physical_client.getMetaClient().skin_index;

		defineChargeEmission();
		defineEmissions();

		// combo stuff
		addComboEmissions(combo_defs = new ArrayList<>());
	}

	/**
	 * Defined combinable emission hierarchy, where the adding order has to be
	 * descending.
	 * 
	 * @param emissions
	 */
	protected abstract void addComboEmissions(ArrayList<AEmissionDef> emissions);

	/**
	 * sets the {@link #charge_def} field. This is an emission definition which gets
	 * created on both client- and server side but will not be transmitted.
	 */
	protected abstract void defineChargeEmission();

	/**
	 * Create for each emission def a field.
	 */
	protected abstract void defineEmissions();

	/**
	 * Returns the first emission. If available, set the right center positions for
	 * the defined def-fields
	 * 
	 * 
	 * @return first emission
	 */
	protected abstract Emission createFirstEmission();

	/**
	 * depending on {@link #p1_version} and {@link #time_scale_p1} the field
	 * {@link #animation_duration} has to be defined
	 */
	protected abstract void setAnimationDuration();

	/**
	 * @return <code>true</code> if initiation was successful
	 * 
	 */
	public boolean init() {

		if (physical_client.isPredictionVerifier() == true)
			return false;

		if (physical_client.isStunned() == true || (physical_client.isDisarmed() == true && this instanceof ABasicSkill)
				|| (physical_client.isSilenced() && this instanceof AMainSkill))
			return false;

		if (physical_client.isRooted() == true && this instanceof AImpulseSkill)
			return false;

		if (physical_client.getCurrentSkill() == this)
			return false;

		if (physical_client.mana_current < mana_cost) {
			// not enough mana
			physical_client.setErrorMessage(ErrorMessage.notEnoughMana);
			return false;
		}

		if (cooldown_current > 0) {
			
			System.out.println("ASkill.init()						INIT 222, cooldown constraint, loc = "+physical_client.getLocation());
			
			// cooldown active
			if (show_cooldown_err == true)
				physical_client.setErrorMessage(ErrorMessage.abilityOnCooldown);
			return false;
		}

		if (physical_client.getCurrentSkill() != null)
			return false;

		// apply skill
		System.out.println("ASkill.init()			INIT set current skill, loc = " + physical_client.getLocation()
				+ ", cooldown current = " + cooldown_current + ", isZero = " + MathUtils.isZero(cooldown_current));
		physical_client.setCurrentSkill(this);
		if (startFirstEmission() != null)
			physical_client.mana_current -= mana_cost;
		elapsed_time = 0;

		return true;

	}

	/**
	 * Wrapper method for {@link #createFirstEmission()}. This method creates the
	 * first emission. For channel skills the emission has to begin immediately, for
	 * charge skills after the cast time has elapsed
	 * 
	 * @return
	 */
	protected Emission startFirstEmission() {

		// start cooldown
		cooldown_current = cooldown_modifiable_absolute;
		cooldown_time_stamp = physical_client.getTime();

		useComboStorages();

		if (physical_client.getLocation() == Location.CLIENTSIDE_MULTIPLAYER)
			return null;

		Emission firstEmission = createFirstEmission();

		if (firstEmission != null) {
			/*
			 * dying skill: we need immediate response for death to avoid artificially
			 * looking behaviour. VoidSkillBasic: sometimes we'd experience a remaining
			 * projectile.
			 */
			if (physical_client.getLocation() == Location.CLIENTSIDE_TRAINING && this instanceof VoidSkill0 == false
					&& this instanceof DyingSkill == false && this instanceof LightningSkill2 == false
					&& this instanceof LightSkill2 == false && this instanceof SandSkill2 == false
					&& this instanceof WindSkill2 == false)
				firstEmission.getDef().setDelayStart(ARTIFICIAL_TRAINING_DELAY);
			physical_client.getEmissionList().add(firstEmission);
		}
		return firstEmission;

	}

	protected void useComboStorages() {

		if (this instanceof AMainSkill == false)
			return;

		// only main skills are combinable

		boolean isStoring1 = physical_client.getComboStorage1().isStoring();
		boolean isStoring2 = physical_client.getComboStorage2().isStoring();
		/*
		 * set combining flag for upcoming emissions depending on storage
		 */
		ComboType type = (isStoring1 == true || isStoring2 == true) ? ComboType.REGULAR : null;
		for (AEmissionDef def : combo_defs) {

			// reset merged pools and merged effects
			def.getRuntimeData().reset();
			def.getRuntimeData().setComboType(type);

		}

		if (isStoring1 == true || isStoring2 == true) {

			assignPools(combo_defs, physical_client.getComboStorage1());
			assignPools(combo_defs, physical_client.getComboStorage2());

			// print assignments
			for (AEmissionDef def : combo_defs) {
				System.out.println("ASkill.useComboStorages() 	print merged poolIDs, prim_pool = " + def.getPoolID()
						+ " (" + ParticleContainer.getDef((int) def.getPoolID()).getInternalPathParsed() + ")");
				for (Short secondaryPoolID : def.getRuntimeData().getMergedPools()) {
					System.out.println("ASkill.useComboStorages() \t \t \t \t  sec_pool = " + secondaryPoolID + " ("
							+ ParticleContainer.getDef((int) secondaryPoolID).getInternalPathParsed() + ")");
				}
			}

			// setup combo plan for effects
			System.out.println("ASkill.useComboStorages()	... Alle Emissions-Andockstellen für Effekte auflisten:");
			for (AEmissionDef def : combo_defs) {
				// copy own effects

				if (def.isFeedback() == false)
					continue;

				System.out.println("ASkill.useComboStorages()			...Emission zum Andocken = " + def.getPoolID()
						+ ", hat folgende Effekte:");
				for (AEffect origEffect : def.getEffects()) {
					def.getRuntimeData().getMergedEffects().add(origEffect.makeCopy());
					System.out.println("ASkill.useComboStorages()				...Effekt: " + origEffect);
				}
			}

			assignEffects(physical_client.getComboStorage1());
			assignEffects(physical_client.getComboStorage2());

			// print combo plan
			for (AEmissionDef def : combo_defs) {
				// copy own effects

				if (def.isFeedback() == false)
					continue;

				System.out.println("ASkill.useComboStorages() 	print merged effects, pool = " + def.getPoolID());
				for (AEffect effect : def.getRuntimeData().getMergedEffects()) {
					System.out.println("ASkill.useComboStorages() \t  effect = " + effect);
				}
			}

		}

		// consumption of combination
		for (AEffectTimed effect : physical_client.getEffectsTimed())
			if (effect instanceof EffectEnableCombining)
				((EffectEnableCombining) effect).killEffect();

	}

	protected void assignPools(ArrayList<AEmissionDef> primDefs, ComboStorage comboStorage) {

		if (comboStorage == null)
			return;

		if (comboStorage.isStoring() == false)
			return;

		ArrayList<Short> addedPoolIDs = new ArrayList<>();

		for (AEmissionDef primDef : primDefs) {

			// find corresponding pool id
			ArrayList<Short> secPoolIDs = primDef.getRuntimeData().getMergedPools();
			boolean added = false;
			short lastValidID = -1;

			for (AEmissionDef secDef : comboStorage.getComboEmissions()) {

				if (primDef.isFeedback() != secDef.isFeedback())
					continue;

				short secPoolID = secDef.getPoolID();

				if (primDef.isFeedback() == true) {
					// feedback
					if (primDef.getTargetType() != secDef.getTargetType())
						continue;

				} else {

					lastValidID = secDef.getPoolID();

					// non-feedback
					if (addedPoolIDs.contains(secPoolID) == true)
						continue;

				}

				added = true;
				addedPoolIDs.add(secPoolID);
				secPoolIDs.add(secPoolID);
				break;

			}

			if (added == false && lastValidID != -1)
				/*
				 * only triggers for non-feedback emissions
				 */
				secPoolIDs.add(lastValidID);

		}
	}

	/**
	 * 
	 * @param mergedPools
	 * @param mergedEffects
	 * @param comboStorage
	 */
	protected void assignEffects(ComboStorage comboStorage) {

		if (comboStorage == null)
			return;

		if (comboStorage.isStoring() == false)
			return;

		float scaleFactor = comboStorage.getSkill().getComboFactorAdded() / ((AMainSkill) this).getComboFactorPrim();

		for (AEmissionDef primDef : combo_defs)

			for (Short secPoolID : primDef.getRuntimeData().getMergedPools())
				for (AEmissionDef secDef : comboStorage.getComboEmissions()) {
					if (secDef.isFeedback() == false)
						continue;
					if (secPoolID == secDef.getPoolID())

						// add all effects
						for (AEffect effect : secDef.getEffects()) {

							AEffect copiedEffect = effect.makeCopy();
							// scale depending on ratio
							copiedEffect.scaleCombo(scaleFactor);
							primDef.getRuntimeData().getMergedEffects().add(copiedEffect);
						}
				}

	}

	/**
	 * Delta time is not the frame time. As the world, skills got updated not
	 * framewise.
	 */
	public void update() {

		if (physical_client.getCurrentSkill() == this) {

			if (isAnimationComplete() == false) {
				// casting phase (animation)
				updateTrack1();
				physical_client.info_animations.current_table_skill_index = table_skill_index;
			}

			// only increase time for currently applied skill
			elapsed_time += Shared.SEND_AND_UPDATE_RATE_WORLD;
		}

		if (cooldown_current > 0) {

			/*
			 * update cooldown only if the casting phase has passed + the cooldown is
			 * greater than 0.
			 */
			// cooldown = cooldownAbs - elapsed time
			// elapsed time = current time stamp - last time stamp
			cooldown_current = cooldown_modifiable_absolute - (physical_client.getTime() - cooldown_time_stamp);

			System.out.println("ASkill.update()						cooldown_current = " + cooldown_current + ", loc = "
					+ physical_client.getLocation());
		}

	}

	protected abstract void updateTrack1();

	/**
	 * Creates and returns emission def for crumbling stones (those for after
	 * touching ground)
	 * 
	 * @return
	 */
	protected AEmissionDef getCrumblingStones() {

		DefProjectile defProjectile = new DefProjectile(2000, getGroundFilter()).setVelocity(0.4f)
				.setVelocityVariation(0.8f).setGravityScale(0.4f).setPolygonShape(3).setFriction(0.7f);

		return new EmissionDefAngleRanged("graphic/particles/particle_global/global_dirt_explosion1.p", 1f, 2f, true,
				defProjectile, 8, 0, 130, 15, new RandomOrder(), new ObstacleAlignment()).setOffset(0.08f)
						.setDestroyPrevProjectileAfterGround(false)
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setSound(AudioData.dirt_explosion1).setTransmissible(false).setTriggerEmissionAllowed();
	}

	public void cancel() {
		physical_client.info_animations.track_1 = AnimationName.P3;
	}

	/**
	 * 
	 * Returns a filter for projectiles. A filter holds category bits ("I am...")
	 * and mask bits (", and I collide with..."). Since this filter is for
	 * projectiles the category bits are always set to
	 * {@link CollisionData#BIT_PROJECTILE}. The mask bits are variable but this
	 * method automatically includes {@link CollisionData#BIT_PROJ_GROUND} as one
	 * part of the mask bits. So we assume every projectile should collide with
	 * {@link SandSkill2} shield for example.
	 * 
	 * categoryBits: "I'm a projectile".
	 * 
	 * @param maskBits sum of all valid collision_bits
	 * @return
	 */
	public static Filter getCollisionFilterWithProjGround(int maskBits) {
		Filter collisionFilter = new Filter();
		// setting category and mask bits
		collisionFilter.categoryBits = CollisionData.BIT_PROJECTILE;
		collisionFilter.maskBits = (short) maskBits;
		collisionFilter.maskBits |= CollisionData.BIT_PROJ_GROUND;

		return collisionFilter;
	}

	public static Filter getCollisionFilterWithoutProjGround(int maskBits) {
		Filter collisionFilter = new Filter();
		// setting category and mask bits
		collisionFilter.categoryBits = CollisionData.BIT_PROJECTILE;
		collisionFilter.maskBits = (short) maskBits;

		return collisionFilter;
	}

	/**
	 * bodies with that filter just collide with ground
	 * 
	 * @return
	 */
	public static Filter getGroundFilter() {
		Filter collisionFilter = new Filter();
		collisionFilter.categoryBits = CollisionData.BIT_CHARACTER_MOVEMENT;
		collisionFilter.maskBits = CollisionData.BIT_GROUND;
		return collisionFilter;
	}

	/**
	 * Returns a filter to collide with nothing
	 * 
	 * @return
	 */
	public static Filter getNeutralFilter() {
		Filter collisionFilter = new Filter();
		collisionFilter.categoryBits = 0;
		collisionFilter.maskBits = 0;
		return collisionFilter;
	}

	/**
	 * 
	 * @param client
	 * @param against , if <code>true</code> this method returns the opposite team
	 * @return
	 */
	public static Team getTeamTarget(PhysicalClient client, boolean against) {
		if (client != null) {
			if (against == true)
				// opposite team
				return client.getHitbox().getTeam() == Team.TEAM_1 ? Team.TEAM_2 : Team.TEAM_1;
			else
				// same team
				return client.getHitbox().getTeam();
		}
		return null;
	}

	public Vector2 getPlayerPos() {
		return player_pos;
	}

	public Vector2 getPlayerVel() {
		return player_vel;
	}

	public int getP1Version() {
		return p1_version;
	}

	/**
	 * Just needed for charge skills
	 * 
	 * @return
	 */
	public float getTimeScaleP1() {
		return time_scale_p1;
	}

	/**
	 * Returns the current absolute cooldown value of this skill, not the remaining
	 * cooldown.
	 * 
	 * @return
	 */
	public float getCooldownModifiableAbsolute() {
		return cooldown_modifiable_absolute;
	}

	public void setCooldownCurrent(float cooldown) {
		cooldown_current = cooldown;
	}

	public float getCooldownCurrent() {
		return cooldown_current;
	}

	public float getCooldownRatio(float cooldownCurrent) {
		return cooldownCurrent / cooldown_modifiable_absolute;
	}

	public boolean isAnimationComplete() {
		return elapsed_time > animation_duration;
	}

	public PhysicalClient getPhysicalClient() {
		return physical_client;
	}

	public DrawableClient getDrawableClient() {
		return drawable_client;
	}

	public Sprite getThumbnailInWorld() {
		return meta_skill.getThumbnailWorldBig();
	}

	public ArrayList<AEmissionDef> getComboEmissions() {
		return combo_defs;
	}

	public boolean isWalkingBrake() {
		return walking_brake;
	}

	public void setTableSkillIndex(int tableIndex) {
		table_skill_index = tableIndex;
	}

	public MetaSkill getMetaSkill() {
		return meta_skill;
	}

	public short getManaCost() {
		return mana_cost;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

}
