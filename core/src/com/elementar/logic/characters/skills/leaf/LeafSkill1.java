package com.elementar.logic.characters.skills.leaf;

import java.util.ArrayList;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.emission.effect.EffectEmpty;
import com.elementar.logic.emission.effect.EffectImpulseToEmission;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class LeafSkill1 extends AMainSkill {

	private AEmissionDef e1_main_projectile, e2_ground_collision, e3_thorns_char, e4_feedback_enemy, e5_feedback_ally;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public LeafSkill1(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/leaf_skill2_charge_" + skin_name_parsed + ".p", 1, 1.5f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e5, feedback ally
		DefProjectile defProjectileE5 = new DefProjectile(2000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e5_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/leaf_skill1_e5_" + skin_name_parsed + "_feedback_ally.p", 1, 2.2f,
				false, defProjectileE5, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_EXCLUDED.getValue()))
						.addEffect(new EffectEmpty());

		// e4, enemy feedback
		DefProjectile defProjectileE4 = new DefProjectile(2000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e4_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/leaf_skill1_e4_" + skin_name_parsed + "_feedback_enemy.p", 1f, 2.2f,
				false, defProjectileE4, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setSound(AudioData.leaf_skill1_e4)
						.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(0).getDamage()));

		// e3 thorns at character
		DefProjectile defProjectileE3 = new DefProjectile(700,
				getCollisionFilterWithProjGround(CollisionData.BIT_CHARACTER_PROJECTILE)).setFlyThroughTargets()
						.addSuccessor(e4_feedback_enemy).addSuccessor(e5_feedback_ally)
						.setAttachmentOption(AttachmentOptionProjectile.AT_EMITTER).setOffsetAttached(0.2f);
		e3_thorns_char = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/leaf_skill1_e3_" + skin_name_parsed + "_thorns_char.p", 10f, 1.7f,
				false, defProjectileE3, 1, 0, new FixedAlignment(90)).setSound(AudioData.leaf_skill1_e3, true);

		// e2 ground collision
		DefProjectile defProjectileE2 = new DefProjectile(2000, getNeutralFilter());
		e2_ground_collision = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/leaf_skill1_e2_" + skin_name_parsed + "_ground_collision.p", 1f, 2f,
				true, defProjectileE2, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setSound(AudioData.leaf_skill1_e2).addSuccessorTimewise(0, e3_thorns_char);
		short poolIDE2 = e2_ground_collision.getPoolID();
		e2_ground_collision.addEffect(new EffectImpulseToEmission(poolIDE2, 350).setMovementImpulse());

		// e1, main projectile
		DefProjectile defProjectileE1 = new DefProjectile(450,
				getCollisionFilterWithProjGround(CollisionData.BIT_GROUND)).addSuccessor(e2_ground_collision)
						.setVelocity(17f).setFlyThroughTargets().setStopAfterGroundCollision();

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/leaf_skill1_e1_" + skin_name_parsed + "_main_projectile.p", 2f, 1.5f,
				true, defProjectileE1, 1, 0, new FixedCursorAlignment()).setSound(AudioData.leaf_skill1_e1, true);
	}

	@Override
	protected Emission createFirstEmission() {

		e2_ground_collision.setTarget(physical_client);

		e1_main_projectile.setCenter(player_pos);
		return new Emission(e1_main_projectile, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_main_projectile);
		emissions.add(e2_ground_collision);
		emissions.add(e3_thorns_char);
		emissions.add(e4_feedback_enemy);
		emissions.add(e5_feedback_ally);
	}

}
