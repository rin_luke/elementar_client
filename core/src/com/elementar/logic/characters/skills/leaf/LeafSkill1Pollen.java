package com.elementar.logic.characters.skills.leaf;

import java.util.ArrayList;

import com.badlogic.gdx.physics.box2d.Filter;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AChargeSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefCloud;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class LeafSkill1Pollen extends AChargeSkill {

	private AEmissionDef e1, e2;

	public LeafSkill1Pollen(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String unparsedSkinName) {
		super(metaSkill, physicalClient, drawableClient, unparsedSkinName);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		combo_defs.add(e1);
		combo_defs.add(e2);
	}

	@Override
	protected void defineEmissions() {

		DefProjectile prototypeE2 = new DefProjectile(30000,
				getCollisionFilterWithProjGround(CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_GROUND))
						.setVelocity(3f)
						.addSuccessor(new EmissionDefAngleDirected("graphic/particles/particle_global/empty_particle.p",
								1f, 1f, false, new DefProjectile(0, new Filter()), 1, 0, new FixedAlignment(0))
										.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0)));

		e2 = new EmissionDefCloud("graphic/particles/particle_skill/leaf_skill1_e2_skin0.p", 2, 6f, false, prototypeE2,
				5, 0.6f)
						.setStartConditions(new StartConditions(
								CollisionData.BIT_BUILDING + CollisionData.BIT_GROUND
										+ CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.AFTER_LIFETIME.getValue() + CollisionKinds.CAST_ON_ENEMY.getValue()));

		Filter collisionFilter = getCollisionFilterWithProjGround(
				CollisionData.BIT_GROUND + CollisionData.BIT_BUILDING + CollisionData.BIT_CHARACTER_PROJECTILE);

		DefProjectile prototypeE1 = new DefProjectile(5000, collisionFilter).setVelocity(3f).setGravityScale(0.4f)
				.addSuccessor(e2);

		e1 = new EmissionDefAngleDirected("graphic/particles/particle_skill/leaf_skill1_e1_skin0.p", 1f, 1f, false,
				prototypeE1, 1, 0, new FixedCursorAlignment());

	}

	@Override
	protected void defineChargeEmission() {

	}

	@Override
	protected Emission createFirstEmission() {
		e1.setCenter(player_pos);

		return new Emission(e1, physical_client);
	}

}
