package com.elementar.logic.characters.skills.leaf;

import java.util.ArrayList;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.CursorFollowingWhileEmittingAlignment;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefAngleRanged;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.emission.effect.EffectHealthChangeOverTime;
import com.elementar.logic.emission.effect.EffectRoot;
import com.elementar.logic.emission.order.SequentialOrder;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class LeafSkill2 extends AMainSkill {

	private final int NUMBER_THORNS = 10;

	private AEmissionDef e1_main_projectile, e2_ground_feedback, e3_feedback_enemy, e4_seeds_empty, e5_thorns,
			e6_thorns_feedback_enemy;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public LeafSkill2(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/leaf_skill2_charge_" + skin_name_parsed + ".p", 1, 1.5f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e6, thorns feedback
		DefProjectile defProjectileE6 = new DefProjectile(meta_skill.getFeedbacks().get(0).getDuration(),
				getNeutralFilter()).setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e6_thorns_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/leaf_skill2_e6_" + skin_name_parsed + "_thorns_feedback_enemy_ot.p",
				1f, 1f, false, defProjectileE6, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setSound(AudioData.leaf_skill2_e6);
		short poolIDE6 = e6_thorns_feedback_enemy.getPoolID();
		e6_thorns_feedback_enemy
				.addEffect(new EffectRoot(poolIDE6, defProjectileE6.getLifeTime()).setThumbnail(getThumbnailInWorld(),
						true))
				.addEffect(new EffectHealthChangeOverTime(poolIDE6, defProjectileE6.getLifeTime(),
						meta_skill.getFeedbacks().get(0).getDotAmountPerSec()));

		// e5, thorns
		DefProjectile defProjectileE5 = new DefProjectile(10000,
				getCollisionFilterWithProjGround(CollisionData.BIT_CHARACTER_PROJECTILE))
						.addSuccessor(e6_thorns_feedback_enemy);

		e5_thorns = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/leaf_skill2_e5_" + skin_name_parsed + "_thorns.p", 2f, 1.5f, false,
				defProjectileE5, 1, 0, new ObstacleAlignment()).setOffset(0.2f)
						.setRemoveFromSucclistAfterGroundCollision(false)
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0)).setOneTouchMechanism()
						.setSound(AudioData.leaf_skill2_e5);

		// e4, seeds empty
		DefProjectile defProjectileE4 = new DefProjectile(400,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_GROUND)).setVelocity(9f).addSuccessor(e5_thorns);
		e4_seeds_empty = new EmissionDefAngleRanged(
				"graphic/particles/particle_skill/leaf_skill2_e4_" + skin_name_parsed + "_seeds_empty.p", 1f, 1f, true,
				defProjectileE4, NUMBER_THORNS, 0, 360f, NUMBER_THORNS, new SequentialOrder(), new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0)).setOffset(0.5f);

		// e3, enemy feedback, root + dot
		DefProjectile defProjectileE3 = new DefProjectile(meta_skill.getFeedbacks().get(1).getDuration(),
				getNeutralFilter()).setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/leaf_skill2_e3_" + skin_name_parsed + "_feedback_enemy_ot.p", 1f, 1f,
				false, defProjectileE3, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setSound(AudioData.leaf_skill2_e3);
		short poolIDE3 = e3_feedback_enemy.getPoolID();
		e3_feedback_enemy
				.addEffect(new EffectRoot(poolIDE3, defProjectileE3.getLifeTime()).setThumbnail(getThumbnailInWorld(),
						true))
				.addEffect(new EffectHealthChangeOverTime(poolIDE6, defProjectileE3.getLifeTime(),
						meta_skill.getFeedbacks().get(0).getDotAmountPerSec()));

		// e2 ground explosion
		DefProjectile defProjectileE2 = new DefProjectile(1200, getNeutralFilter());
		e2_ground_feedback = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/leaf_skill2_e2_" + skin_name_parsed + "_feedback_ground.p", 1f, 2f,
				true, defProjectileE2, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setSound(AudioData.leaf_skill2_e2);
		// e1, main projectile
		DefProjectile defProjectileE1 = new DefProjectile(3000,
				getCollisionFilterWithoutProjGround(
						CollisionData.BIT_GROUND + CollisionData.BIT_BUILDING + CollisionData.BIT_CHARACTER_PROJECTILE))
								.setVelocity(10f).addSuccessor(getCrumblingStones()).addSuccessor(e2_ground_feedback)
								.addSuccessor(e4_seeds_empty).addSuccessor(e3_feedback_enemy)
								.setStopAfterGroundCollision();

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/leaf_skill2_e1_" + skin_name_parsed + "_main_projectile.p", 2f, 1.5f,
				true, defProjectileE1, 1, 0, new CursorFollowingWhileEmittingAlignment())
						.setSound(AudioData.leaf_skill2_e1);
	}

	@Override
	protected Emission createFirstEmission() {
		e1_main_projectile.setCenter(player_pos);
		return new Emission(e1_main_projectile, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_main_projectile);
		emissions.add(e2_ground_feedback);
		emissions.add(e3_feedback_enemy);
		emissions.add(e5_thorns);
		emissions.add(e6_thorns_feedback_enemy);
	}

}
