package com.elementar.logic.characters.skills.leaf;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.ABasicSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class LeafSkill0 extends ABasicSkill {

	private AEmissionDef e2_feedback_enemy, e1_main_projectile;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public LeafSkill0(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getGroundFilter())
				.setFlyThroughTargets();
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/leaf_skill0_charge_" + skin_name_parsed + ".p", 1f, 2f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e2 feedback enemy
		DefProjectile defProjectileE2 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e2_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/leaf_skill0_e2_" + skin_name_parsed + "_feedback_enemy.p", 1f, 1.4f,
				false, defProjectileE2, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setSound(AudioData.leaf_skill0_e2)
						.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(0).getDamage()));

		// e1
		DefProjectile defProjectileE1 = new DefProjectile(250,
				getCollisionFilterWithProjGround(CollisionData.BIT_CHARACTER_PROJECTILE))
						.addSuccessor(e2_feedback_enemy).setFlyThroughTargets().setRectangleShape(2.5f, 0.8f)
						.setAttachmentOption(AttachmentOptionProjectile.AT_EMITTER).setOffsetAttached(1f);

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/leaf_skill0_e1_" + skin_name_parsed + "_main_projectile.p", 3f, 1.8f,
				false, defProjectileE1, 1, 0, new FixedCursorAlignment()).setSound(AudioData.leaf_skill0_e1);

	}

	@Override
	protected Emission createFirstEmission() {
		e1_main_projectile.setTarget(physical_client);
		e1_main_projectile.setCenter(player_pos);
		return new Emission(e1_main_projectile, physical_client);
	}

}
