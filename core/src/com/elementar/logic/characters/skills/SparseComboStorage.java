package com.elementar.logic.characters.skills;

public class SparseComboStorage {

	/**
	 * useful to get the corresponding {@link MetaSkill} object immediately.
	 */
	public byte meta_skill_id = -1;

	/**
	 * relation between time left and max combining time
	 */
	public float duration_ratio = -1;

	public SparseComboStorage() {

	}

	public SparseComboStorage(int skillID, float durationRatio) {
		meta_skill_id = (byte) skillID;
		duration_ratio = durationRatio;
	}

	public void set(SparseComboStorage sparseComboStorage) {
		meta_skill_id = sparseComboStorage.meta_skill_id;
		duration_ratio = sparseComboStorage.duration_ratio;
	}

	public boolean isStoring() {
		return duration_ratio > 0f;
	}

}
