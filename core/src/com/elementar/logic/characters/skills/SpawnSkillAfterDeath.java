package com.elementar.logic.characters.skills;

import java.util.ArrayList;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.effect.EffectSpawnAfterDeath;
import com.elementar.logic.util.CollisionData.CollisionKinds;

/**
 * triggers after death time elapsed
 * 
 * @author lukassongajlo
 * 
 */
public class SpawnSkillAfterDeath extends AChannelSkill {

	/**
	 * in seconds
	 */
	private static final int INVINCIBLE_TIME_MS_AFTER_SPAWN = 5000;

	private EmissionDefAngleDirected e1;

	public SpawnSkillAfterDeath(PhysicalClient physicalClient, String unparsedSkinName) {
		super(null, physicalClient, null, unparsedSkinName);
	}

	@Override
	public boolean init() {
		physical_client.setCurrentSkill(this);
		startFirstEmission();
		return true;
	}

	@Override
	protected void defineEmissions() {

		if (team_user != null) {

			DefProjectile defProjectile = new DefProjectile(INVINCIBLE_TIME_MS_AFTER_SPAWN, getNeutralFilter())
					.setAttachmentOption(AttachmentOptionProjectile.AT_EMITTER).setOffsetAttached(0.08f);

			e1 = new EmissionDefAngleDirected(
					"graphic/particles/particle_global/global_player_respawn_team"
							+ team_user.name().substring(team_user.name().length() - 1) + ".p",
					1f, 0.9f, false, defProjectile, 1, 0, new FixedAlignment(90))
							.setStartConditions(new StartConditions(0, CollisionKinds.AFTER_LIFETIME.getValue()))
							.setSound(AudioData.player_spawn);
			short poolID = e1.getPoolID();
			e1.addEffect(new EffectSpawnAfterDeath(poolID, defProjectile.getLifeTime()));
			// .setThumbnail(getThumbnailInWorld(), true));

		}
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
	}

	@Override
	protected Emission createFirstEmission() {
		e1.setCenter(player_pos);
		e1.setTarget(physical_client);
		return new Emission(e1, physical_client);
	}

}
