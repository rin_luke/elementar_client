package com.elementar.logic.characters.skills;

import java.util.ArrayList;

import com.elementar.logic.characters.util.SkillStatistic;

public class MetaFeedback implements Comparable<MetaFeedback> {

	private int index = 0;

	private int damage, heal, dot_amount_per_sec, hot_amount_per_sec;

	/**
	 * in ms
	 */
	private int slow_duration, disarmed_duration, root_duration, stun_duration, silence_duration, dot_duration,
			hot_duration, duration;

	/**
	 * e.g. -0.6f
	 */
	private float slow_amount;

	private ArrayList<SkillStatistic> stats = new ArrayList<>();

	public MetaFeedback() {
	}

	public ArrayList<SkillStatistic> getStats() {
		return stats;
	}

	@Override
	public int compareTo(MetaFeedback feedback) {
		return feedback.index > index ? -1 : 1;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public void setSlowAmount(String key, float slowAmount) {
		this.slow_amount = slowAmount;
		stats.add(new SkillStatistic<Integer>(key, (int) (slowAmount * 100 * -1), "%"));
	}

	public float getSlowAmount() {
		return slow_amount;
	}

	public void setSlowDuration(String key, int slowDuration) {
		this.slow_duration = slowDuration;
		stats.add(new SkillStatistic<Float>(key, (float) (slowDuration / 1000f)));
	}

	public int getSlowDuration() {
		return slow_duration;
	}

	public void setDamage(String key, int damage) {
		this.damage = damage;
		stats.add(new SkillStatistic<Integer>(key, damage * -1));
	}

	public int getDamage() {
		return damage;
	}

	public void setHeal(String key, int heal) {
		this.heal = heal;
		stats.add(new SkillStatistic<Integer>(key, heal));
	}

	public int getHeal() {
		return heal;
	}

	public void setDisarmedDuration(String key, int disarmedDuration) {
		this.disarmed_duration = disarmedDuration;
		stats.add(new SkillStatistic<Float>(key, (float) (disarmedDuration / 1000f)));
	}

	public int getDisarmedDuration() {
		return disarmed_duration;
	}

	public void setRootDuration(String key, int rootDuration) {
		this.root_duration = rootDuration;
		stats.add(new SkillStatistic<Float>(key, (float) (rootDuration / 1000f)));
	}

	public int getRootDuration() {
		return root_duration;
	}

	public void setStunDuration(String key, int stunDuration) {
		this.stun_duration = stunDuration;
		stats.add(new SkillStatistic<Float>(key, (float) (stunDuration / 1000f)));
	}

	public int getStunDuration() {
		return stun_duration;
	}

	public void setSilenceDuration(String key, int silenceDuration) {
		this.silence_duration = silenceDuration;
		stats.add(new SkillStatistic<Float>(key, (float) (silenceDuration / 1000f)));
	}

	public int getSilenceDuration() {
		return silence_duration;
	}

	public void setDotAmountPerSec(String key, int dotAmountPerSec) {
		this.dot_amount_per_sec = dotAmountPerSec;
		stats.add(new SkillStatistic<Integer>(key, dotAmountPerSec));
	}

	public int getDotAmountPerSec() {
		return dot_amount_per_sec;
	}

	public void setDotDuration(String key, int dotDuration) {
		this.dot_duration = dotDuration;
		stats.add(new SkillStatistic<Float>(key, (float) (dotDuration / 1000f)));
	}

	public int getDotDuration() {
		return dot_duration;
	}

	public void setHotAmountPerSec(String key, int hotAmountPerSec) {
		this.hot_amount_per_sec = hotAmountPerSec;
		stats.add(new SkillStatistic<Integer>(key, hotAmountPerSec));
	}

	public int getHotAmountPerSec() {
		return hot_amount_per_sec;
	}

	public void setHotDuration(String key, int hotDuration) {
		this.hot_duration = hotDuration;
		stats.add(new SkillStatistic<Float>(key, (float) (hotDuration / 1000f)));
	}

	public int getHotDuration() {
		return hot_duration;
	}

	public void setDuration(String key, int duration) {
		this.duration = duration;
		stats.add(new SkillStatistic<Float>(key, (float) (duration / 1000f)));
	}

	public int getDuration() {
		return duration;
	}

}
