package com.elementar.logic.characters.skills.water;

import java.util.ArrayList;

import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectEmpty;
import com.elementar.logic.emission.effect.EffectHeal;
import com.elementar.logic.emission.effect.EffectHealthChangeOverTime;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class WaterSkill2 extends AMainSkill {

	private AEmissionDef e1_empty_global, e3_feedback_ally, e2_feedback_enemy;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public WaterSkill2(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/water_skill2_charge_" + skin_name_parsed + ".p", 1f, 3f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e3, ally feedback
		DefProjectile defProjectileE3 = new DefProjectile(meta_skill.getFeedbacks().get(0).getHotDuration(),
				getNeutralFilter()).setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill2_e3_" + skin_name_parsed + "_feedback_ally.p", 1f, 2f,
				false, defProjectileE3, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_INCLUDED.getValue()))
						.setSound(AudioData.water_skill2_e3, true);

		short poolIDE3 = e3_feedback_ally.getPoolID();

		e3_feedback_ally.addEffect(new EffectHealthChangeOverTime(poolIDE3, defProjectileE3.getLifeTime(),
				meta_skill.getFeedbacks().get(0).getHotAmountPerSec()).setThumbnail(getThumbnailInWorld(), false))
				.addEffect(new EffectHeal(meta_skill.getFeedbacks().get(0).getHeal()));

		// e2, enemy feedback
		DefProjectile defProjectileE2 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e2_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill2_e2_" + skin_name_parsed + "_feedback_enemy.p", 1f, 2f,
				false, defProjectileE2, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.addEffect(new EffectEmpty()).setSound(AudioData.water_skill2_e2, true);

		// e1, empty global
		DefProjectile defProjectileE1 = new DefProjectile(2000,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE)).setFlyThroughTargets()
						.addSuccessor(e2_feedback_enemy).addSuccessor(e3_feedback_ally);

		e1_empty_global = new EmissionDefAngleDirected("graphic/particles/particle_skill/water_skill2_e1_empty.p",
				1000f, 1f, false, defProjectileE1, 1, 0, new FixedAlignment(0)).setSound(AudioData.water_skill2_e1);

	}

	@Override
	protected Emission createFirstEmission() {

		e1_empty_global.setCenter(player_pos);

		return new Emission(e1_empty_global, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_empty_global);
		emissions.add(e3_feedback_ally);
		emissions.add(e2_feedback_enemy);
	}

}
