package com.elementar.logic.characters.skills.water;

import com.badlogic.gdx.physics.box2d.Filter;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.ABasicSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.emission.effect.EffectHeal;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class WaterSkill0 extends ABasicSkill {

	private AEmissionDef e1_main_projectile_phase1, e2_feedback_ground_phase1, e1_main_projectile_phase2,
			e2_feedback_ground_phase2, e3_feedback_enemy, e4_feedback_ally;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public WaterSkill0(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getGroundFilter())
				.setFlyThroughTargets();
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/water_skill0_charge_" + skin_name_parsed + ".p", 1, 1f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e4, feedback at ally
		DefProjectile defProjectileE4 = new DefProjectile(2000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e4_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill0_e4_" + skin_name_parsed + "_feedback_ally.p", 1, 4,
				false, defProjectileE4, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_INCLUDED.getValue()))
						.addEffect(new EffectHeal(meta_skill.getFeedbacks().get(1).getHeal()))
						.setSound(AudioData.water_skill0_e3);

		// e3, feedback at enemy
		DefProjectile defProjectileE3 = new DefProjectile(2000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e3_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill0_e3_" + skin_name_parsed + "_feedback_enemy.p", 1, 4,
				false, defProjectileE3, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.addEffect(new EffectDamage(meta_skill.getFeedbacks().get(0).getDamage()))
						.setSound(AudioData.water_skill0_e2);

		/*
		 * why two phases? If the water character wants to heal itself it directs the
		 * shot to the ground but since no second collision is detected we need to
		 * create a second projectile to trigger a collision.
		 */

		// PHASE 2

		// e2 feedback ground
		DefProjectile defProjectileE2P2 = new DefProjectile(2000, getNeutralFilter());

		e2_feedback_ground_phase2 = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill0_e2_" + skin_name_parsed + "_feedback_ground_phase2.p", 1,
				1, false, defProjectileE2P2, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setRemoveFromSucclistAfterGroundCollision(false).addEffect(new EffectDamage(-5))
						.setSound(AudioData.water_skill1_e4);

		// e1, single bubble
		Filter filter = getCollisionFilterWithProjGround(
				CollisionData.BIT_WATER_DROP + CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_GROUND);
		filter.categoryBits |= CollisionData.BIT_WATER_DROP;
		DefProjectile defProjectileE1P2 = new DefProjectile(3000, filter).setVelocity(1f).setGravityScale(1f)
				.setRestitution(0.3f).setFriction(0.4f)
				.addSuccessor(e2_feedback_ground_phase2.setDestroyPrevProjectileAfterGround(false))
				.addSuccessor(e3_feedback_enemy.setDestroyPrevProjectileAfterGround(false))
				.addSuccessor(e4_feedback_ally.setDestroyPrevProjectileAfterGround(false));

		e1_main_projectile_phase2 = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill0_e1_" + skin_name_parsed + "_main_projectile_phase2.p",
				1f, 1.6f, false, defProjectileE1P2, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0));

		// PHASE 1

		// e2 feedback ground
		DefProjectile defProjectileE2P1 = new DefProjectile(2000, getNeutralFilter());

		e2_feedback_ground_phase1 = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill0_e2_" + skin_name_parsed + "_feedback_ground_phase1.p", 1,
				1, false, defProjectileE2P1, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setRemoveFromSucclistAfterGroundCollision(true).addEffect(new EffectDamage(-5))
						.setSound(AudioData.water_skill1_e4);

		// e1, single bubble
		Filter filterP1 = getCollisionFilterWithProjGround(
				CollisionData.BIT_WATER_DROP + CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_GROUND);
		filterP1.categoryBits |= CollisionData.BIT_WATER_DROP;
		DefProjectile defProjectileE1P1 = new DefProjectile(3000, filterP1).setVelocity(8f).setGravityScale(1f)
				.setTimeUntilEmitterCollision(300).setRestitution(0.3f).setFriction(0.4f)
				.addSuccessor(e1_main_projectile_phase2)
				.addSuccessor(e2_feedback_ground_phase2.setDestroyPrevProjectileAfterGround(false))
				.addSuccessor(e3_feedback_enemy.setDestroyPrevProjectileAfterGround(false))
				.addSuccessor(e4_feedback_ally.setDestroyPrevProjectileAfterGround(false));

		e1_main_projectile_phase1 = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill0_e1_" + skin_name_parsed + "_main_projectile_phase1.p",
				1f, 1.6f, false, defProjectileE1P1, 1, 0, new FixedCursorAlignment())
						.setSound(AudioData.water_skill0_e1);

	}

	@Override
	protected Emission createFirstEmission() {
		e1_main_projectile_phase1.setCenter(player_pos);
		return new Emission(e1_main_projectile_phase1, physical_client);
	}

}
