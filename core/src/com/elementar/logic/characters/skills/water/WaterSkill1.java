package com.elementar.logic.characters.skills.water;

import java.util.ArrayList;

import com.badlogic.gdx.physics.box2d.Filter;
import com.elementar.data.container.AudioData;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.skills.AMainSkill;
import com.elementar.logic.characters.skills.MetaSkill;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.alignment.ObstacleAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefAngleRanged;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.effect.EffectDamage;
import com.elementar.logic.emission.effect.EffectHeal;
import com.elementar.logic.emission.order.RandomOrder;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.CollisionData.CollisionKinds;

public class WaterSkill1 extends AMainSkill {

	private final int NUMBER_DROPS = 10;
	private final int FOUNTAIN_DURATION = 5000;
	private final int DROP_LIFETIME = 20000;

	private AEmissionDef e1_main_projectile, e2_fountain, e3_fountain_drop, e4_drop_feedback_ground, e5_feedback_enemy,
			e6_feedback_ally;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 * @param skinName
	 */
	public WaterSkill1(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String skinName) {
		super(metaSkill, physicalClient, drawableClient, skinName);
	}

	@Override
	protected void defineChargeEmission() {
		DefProjectile prototypeCharge = new DefProjectile((int) (animation_duration * 1000), getNeutralFilter());
		charge_def = new EmissionDefBoneFollowing(
				"graphic/particles/particle_skill/water_skill1_charge_" + skin_name_parsed + ".p", 1f, 3f, false,
				prototypeCharge, "character_hand_front");
	}

	@Override
	protected void defineEmissions() {

		// e6, ally feedback
		DefProjectile defProjectileE6 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e6_feedback_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill1_e6_" + skin_name_parsed + "_feedback_ally.p", 1f, 4f,
				false, defProjectileE6, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ALLY_EMITTER_INCLUDED.getValue()))
						.setAttached().addEffect(new EffectHeal(meta_skill.getFeedbacks().get(1).getHeal()))
						.setSound(AudioData.water_skill1_e6);

		// e5, enemy feedback
		DefProjectile defProjectileE5 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e5_feedback_enemy = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill1_e5_" + skin_name_parsed + "_feedback_enemy.p", 1f, 4f,
				false, defProjectileE5, 1, 0, new FixedAlignment(0))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.CAST_ON_ENEMY.getValue()))
						.setAttached().addEffect(new EffectDamage(meta_skill.getFeedbacks().get(0).getDamage()))
						.setSound(AudioData.water_skill1_e5);

		// e4, ally feedback excluding emitter
		DefProjectile defProjectileE4 = new DefProjectile(1000, getNeutralFilter());

		e4_drop_feedback_ground = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill1_e4_" + skin_name_parsed + "_drop_feedback_ground.p", 1f,
				4.5f, true, defProjectileE4, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setRemoveFromSucclistAfterGroundCollision(false).setSound(AudioData.water_skill1_e4);

		// e3, fountain drop
		Filter filter = getCollisionFilterWithProjGround(
				CollisionData.BIT_WATER_DROP + CollisionData.BIT_CHARACTER_PROJECTILE + CollisionData.BIT_GROUND);
		filter.categoryBits |= CollisionData.BIT_WATER_DROP;

		DefProjectile defProjectileE3 = new DefProjectile(DROP_LIFETIME, filter).setGravityScale(1f).setVelocity(5f)
				.setVelocityVariation(0.2f)// .setPolygonShape(6)
				.setRestitution(0.3f).setFriction(0.4f)
				.addSuccessor(e4_drop_feedback_ground.setDestroyPrevProjectileAfterGround(false))
				.addSuccessor(e5_feedback_enemy.setDestroyPrevProjectileAfterGround(false))
				.addSuccessor(e6_feedback_ally.setDestroyPrevProjectileAfterGround(false));

		e3_fountain_drop = new EmissionDefAngleRanged(
				"graphic/particles/particle_skill/water_skill1_e3_" + skin_name_parsed + "_fountain_drop.p", 2.5f, 2f,
				false, defProjectileE3, NUMBER_DROPS, FOUNTAIN_DURATION, 80, 10, new RandomOrder(),
				new ObstacleAlignment()).setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setOffset(0.4f).setSound(AudioData.water_skill1_e3);

		// e2, fountain
		DefProjectile defProjectileE2 = new DefProjectile(FOUNTAIN_DURATION, getNeutralFilter());

		e2_fountain = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill1_e2_" + skin_name_parsed + "_fountain.p", 1f, 1.6f, false,
				defProjectileE2, 1, 0, new ObstacleAlignment())
						.setStartConditions(new StartConditions(CollisionData.BIT_GROUND, 0))
						.setSound(AudioData.water_skill1_e2);

		// e1, main projectile
		DefProjectile defProjectileE1 = new DefProjectile(10000,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_GROUND)).setGravityScale(1f).setVelocity(5f)
						.addSuccessor(e2_fountain).addSuccessor(e3_fountain_drop);

		e1_main_projectile = new EmissionDefAngleDirected(
				"graphic/particles/particle_skill/water_skill1_e1_" + skin_name_parsed + "_main_projectile.p", 2f, 1.5f,
				true, defProjectileE1, 1, 0, new FixedCursorAlignment()).setSound(AudioData.water_skill1_e1);

	}

	@Override
	protected Emission createFirstEmission() {

		e1_main_projectile.setCenter(player_pos);

		return new Emission(e1_main_projectile, physical_client);
	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1_main_projectile);
		emissions.add(e2_fountain);
		emissions.add(e3_fountain_drop);
		emissions.add(e4_drop_feedback_ground);
		emissions.add(e5_feedback_enemy);
		emissions.add(e6_feedback_ally);
	}

}
