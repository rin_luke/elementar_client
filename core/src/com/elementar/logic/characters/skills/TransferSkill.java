package com.elementar.logic.characters.skills;

import java.util.ArrayList;

import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.Location;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.IEmitter;
import com.elementar.logic.emission.StartConditions;
import com.elementar.logic.emission.DefProjectile.AttachmentOptionProjectile;
import com.elementar.logic.emission.DefProjectile.CollectableType;
import com.elementar.logic.emission.EmissionRuntimeData.ComboType;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.alignment.FixedCursorAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.effect.EffectEnableCombining;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.ErrorMessage;
import com.elementar.logic.util.CollisionData.CollisionKinds;

/**
 * The transfer-skill has an emission which holds a special particle effect with
 * non-visual emitters but with a certain combining-behaviour. Particle effects
 * of the transferring skill will be combined with the first emission of the
 * corresponding skill and represent the skill in compressed way for sending it
 * to mates.
 * 
 * @author lukassongajlo
 *
 */
public class TransferSkill extends AChargeSkill {

	protected static final float VELOCITY_E0_E1 = 5f;

	protected ASkill corresponding_skill;
	protected AEmissionDef e0_thumbnail, e1, e2_at_ally;

	protected IEmitter ally;

	/**
	 * 
	 * @param metaSkill
	 * @param physicalClient
	 * @param drawableClient
	 */
	public TransferSkill(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient) {
		super(metaSkill, physicalClient, drawableClient, null);

	}

	@Override
	protected void defineChargeEmission() {

	}

	@Override
	protected void defineEmissions() {

		String skillOrder = meta_skill.getOrder();

		DefProjectile defProjectileE2 = new DefProjectile(1000, getNeutralFilter())
				.setAttachmentOption(AttachmentOptionProjectile.AT_TARGET);

		e2_at_ally = new EmissionDefAngleDirected(
				"graphic/particles/particle_global/global_combo_sending_e2_" + skillOrder + ".p", 1, 0.75f, false,
				defProjectileE2, 1, 0, new FixedAlignment(90))
						.setStartConditions(new StartConditions(CollisionData.BIT_CHARACTER_PROJECTILE,
								CollisionKinds.AFTER_ABSORPTION.getValue()))
						.addEffect(new EffectEnableCombining(skillOrder));

		DefProjectile defProjectileE1 = new DefProjectile(Integer.MAX_VALUE,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE)).setVelocity(VELOCITY_E0_E1)
						.setCollectable(CollectableType.FOR_COMBO).addSuccessor(e2_at_ally);

		e1 = new EmissionDefAngleDirected("graphic/particles/particle_global/global_combo_sending_e1.p", 1f, 2f, true,
				defProjectileE1, 1, 0, new FixedCursorAlignment());

		DefProjectile defProjectileE0 = new DefProjectile(Integer.MAX_VALUE,
				getCollisionFilterWithoutProjGround(CollisionData.BIT_CHARACTER_PROJECTILE)).setVelocity(VELOCITY_E0_E1)
						.setCollectable(CollectableType.FOR_COMBO);

		e0_thumbnail = new EmissionDefAngleDirected(
				"graphic/particles/particle_global/global_combo_sending_thumbnail_"
						+ meta_skill.getGameclassName().toString().toLowerCase() + "_" + skillOrder + ".p",
				1f, 4f, false, defProjectileE0, 1, 0, new FixedAlignment(0)).setRotatable(false).addSuccessorTimewise(0,
						e1);

	}

	@Override
	protected void addComboEmissions(ArrayList<AEmissionDef> emissions) {
		emissions.add(e1);
		emissions.add(e2_at_ally);
	}

	@Override
	public boolean init() {

		if (physical_client.mana_current < corresponding_skill.mana_cost) {
			// not enough mana
			physical_client.setErrorMessage(ErrorMessage.notEnoughMana);
			return false;
		}

		return super.init();
	}

	@Override
	protected Emission startFirstEmission() {

		// start cooldown of transfer skill
		cooldown_current = cooldown_modifiable_absolute;
		cooldown_time_stamp = physical_client.getTime();

		// start cooldown of corresponding skill
		corresponding_skill.cooldown_current = corresponding_skill.cooldown_modifiable_absolute;
		corresponding_skill.cooldown_time_stamp = physical_client.getTime();

		if (physical_client.getLocation() == Location.CLIENTSIDE_MULTIPLAYER)
			return null;

		physical_client.mana_current -= corresponding_skill.mana_cost;

		/*
		 * here the combo storage get filled by the transfer skill's correspondent which
		 * is also a skill by the transfer skill's user
		 */
		/*
		 * I had to override this method to set the corresponding skill manually,
		 * like...
		 */
		System.out.println("TransferSkill.startFirstEmission()			set transfer combo storage...");
		physical_client.getComboStorageTransfer().setSkill(corresponding_skill);

		// setup combo plan for pools
		for (AEmissionDef def : combo_defs) {
			def.getRuntimeData().reset();
			def.getRuntimeData().setComboType(ComboType.TRANSFERRING);
		}

		assignPools(combo_defs, physical_client.getComboStorageTransfer());

		// boolean combineFlag =
		// physical_client.getComboStorage().init(true);

		// set combining flag for upcoming emissions

		Emission firstEmission = createFirstEmission();

		physical_client.getEmissionList().add(firstEmission);

		return firstEmission;

	}

	@Override
	protected Emission createFirstEmission() {

		if (e1 == null)
			return null;

		e1.setCenter(player_pos);
		e2_at_ally.setCenter(ally.getPos());
//		e2_at_ally.setTarget(ally);

		e1.setTarget(ally);
		e1.setAbsorber(ally);
		e1.setPersecutee(ally);

		e0_thumbnail.setCenter(player_pos);
		e0_thumbnail.setAbsorber(ally);
		e0_thumbnail.setPersecutee(ally);
		e0_thumbnail.setTarget(ally);

		return new Emission(e0_thumbnail, physical_client);
	}

	public void setCorrespondingSkill(ASkill correspondingSkill) {
		corresponding_skill = correspondingSkill;
	}

	public void setAlly(IEmitter ally) {
		this.ally = ally;
	}

}
