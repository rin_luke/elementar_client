package com.elementar.logic.characters.skills;

import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.emission.Emission;

/**
 * 
 * For channeling skills the fields {@link ASkill#time_scale_p1},
 * {@link ASkill#time_scale_p2} and {@link ASkill#immutable_time_scale_p2} won't
 * be used at all. The animation duration depends only on cast time.
 * 
 * @author lukassongajlo
 * 
 */
public abstract class AChannelSkill extends ASkill {
	/**
	 * this variable is needed for channel skills, because while executing the skill
	 * the player could be interrupted or cancelled his skill, so the emission must
	 * be interrupted as well
	 */
	protected Emission first_emission;

	public AChannelSkill(MetaSkill metaSkill, PhysicalClient physicalClient, DrawableClient drawableClient,
			String unparsedSkinName) {
		super(metaSkill, physicalClient, drawableClient, unparsedSkinName);
	}

	@Override
	protected void defineChargeEmission() {

	}

	@Override
	protected Emission startFirstEmission() {
		return first_emission = super.startFirstEmission();
	}

	@Override
	public boolean init() {
		if (super.init() == true) {
			// emission has to start immediately
			physical_client.mana_current -= mana_cost;
			startFirstEmission();
			return true;
		}
		return false;
	}

	@Override
	protected void setAnimationDuration() {
		animation_duration = 0;
	}

	@Override
	public void cancel() {
		super.cancel();
		// might be null if the clients location is CLIENTSIDE_MULTIPLAYER
		if (first_emission != null)
			first_emission.setInterrupted(true);
	}

	/**
	 * For channel skill, {@link ASkill#cast_time} is equals to
	 * {@link ASkill#animation_duration}
	 */
	@Override
	protected void updateTrack1() {
		physical_client.info_animations.track_1 = AnimationName.P1_CHANNEL;
	}

}
