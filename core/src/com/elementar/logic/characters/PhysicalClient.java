package com.elementar.logic.characters;

import static com.elementar.logic.util.Shared.CHARACTER_MAX_VELOCITY_X;
import static com.elementar.logic.util.Shared.SEND_AND_UPDATE_RATE_WORLD;
import static com.elementar.logic.util.Shared.WALKABLE_ANGLE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.elementar.data.container.AudioData;
import com.elementar.data.container.KeysConfiguration.WorldAction;
import com.elementar.data.container.util.TweenableSound;
import com.elementar.gui.modules.roots.WorldOnlineRoot;
import com.elementar.gui.modules.roots.WorldTrainingRoot;
import com.elementar.logic.LogicTier;
import com.elementar.logic.characters.abstracts.AClient;
import com.elementar.logic.characters.skills.AChargeSkill;
import com.elementar.logic.characters.skills.ASkill;
import com.elementar.logic.characters.skills.ComboStorage;
import com.elementar.logic.characters.skills.DyingSkill;
import com.elementar.logic.characters.skills.TransferSkill;
import com.elementar.logic.characters.util.Jumping;
import com.elementar.logic.characters.util.Landing;
import com.elementar.logic.characters.util.PhysicalClientSnapshot;
import com.elementar.logic.emission.DefProjectile;
import com.elementar.logic.emission.Emission;
import com.elementar.logic.emission.EmissionList;
import com.elementar.logic.emission.IEmitter;
import com.elementar.logic.emission.SparseProjectile;
import com.elementar.logic.emission.Trigger;
import com.elementar.logic.emission.alignment.FixedAlignment;
import com.elementar.logic.emission.def.AEmissionDef;
import com.elementar.logic.emission.def.EmissionDefAngleDirected;
import com.elementar.logic.emission.def.EmissionDefBoneFollowing;
import com.elementar.logic.emission.def.AEmissionDef.ParticleEffectCategory;
import com.elementar.logic.emission.effect.AEffectTimed;
import com.elementar.logic.emission.effect.AEffectWithNumber;
import com.elementar.logic.emission.effect.EffectEnableCombining;
import com.elementar.logic.emission.effect.EffectResurrection;
import com.elementar.logic.map.buildings.Base;
import com.elementar.logic.network.gameserver.GameserverLogic;
import com.elementar.logic.network.requests.InputRequest;
import com.elementar.logic.util.ErrorMessage;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.SparseObject;
import com.elementar.logic.util.ViewCircle;
import com.elementar.logic.util.Shared.Team;
import com.elementar.logic.util.lists.DiscardingLimitedArrayList;
import com.elementar.logic.util.lists.NotNullArrayList;
import com.elementar.logic.util.userdata.SeparatorContacts;

//Note: Es wird dafür gesorgt, dass das Feld 'character' nicht null ist. Daher entfallen entsprechende null checks
public class PhysicalClient extends AClient implements IEmitter {

	public static final float PLAYER_VIEW_RADIUS = 5f;

	public static final int SKILL_0_INDEX = 2, SKILL_1_INDEX = 3, SKILL_2_INDEX = 4;

	/**
	 * describes how fast the max velocity is reached. Each simulation step the
	 * current velocity is influenced by this value. The inertia must have a minimum
	 * because on certain walkable angles a lower inertia leads to a downwarding
	 * spiral
	 */
	private static final float CHARACTER_INERTIA_VELOCITY_X = CHARACTER_MAX_VELOCITY_X / 1.2f;

	private static final float AIR_DRAG_DEFAULT = 0.87f;
	private static final float AIR_DRAG_SLIDING_GROUND = 0.3f;
	private static final float AIR_DRAG_SLIDING_CEILING = 5f;// 6f;

	/**
	 * Used for basic jumps on walkable platforms
	 */
	private static final float JUMP_IMPULSE_Y = 3.6f;

	/**
	 * jump impulse while ceiling sliding sensor are active
	 */
	private static final Vector2 JUMP_IMPULSE_CEILING = new Vector2(1.2f, // 2.5f,
			JUMP_IMPULSE_Y - 0.8f);

	/**
	 * solange man unter der walking delay ist funktioniert laufen nicht. das ist
	 * eigentlich erst nach dem jumpen der fall, wenn man davor geslidet hat.
	 */
	private static final float WALKING_DELAY_AFTER_SLIDEJUMP = 0.2f;

	/**
	 * in secs
	 */
	private static final float STICKING_DELAY_ON_SLIDABLE = 0.1f;

	private Location location;

	private PhysicalHitbox physical_hitbox;

	/**
	 * This field stores the last number of foot contacts to recognize a difference
	 * between falling and be on the ground. So the landing animation can start.
	 */
	private int num_contacts_landing;
	/**
	 * This field stores whether a player can jump a second time. A variable is
	 * necessary, because I need to store the information that a player already jump
	 * a second time.
	 */
	private boolean player_can_secondjump;

	private boolean is_holding;

	/**
	 * this field is true when A or D is released, so potentially the player might
	 * brake. This field is set to <code>false</code>, when jumping begins.
	 */
	private boolean brake_enabled = true;

	private Jumping jumping;
	private Landing landing;

	private int platform_angle;

	private float elapsed_time = 0;
	private float elapsed_time_sliding = 0;

	/**
	 * local time or gameserver time, depends on {@link #location}
	 */
	private float time;

	public float death_time_current, death_time_absolute;

	protected DiscardingLimitedArrayList<InputRequest> input_queue;

	private IEmitter slayer;

	private ASkill current_skill;

	private EmissionList emission_list;

	/**
	 * this variable is needed to recognize the transition between 'alive' and
	 * 'dead'
	 */
	private boolean prev_alive = true;

	private boolean stunned = false, rooted = false, disarmed = false, silenced = false, under_impulse = false;

	private SparseClient my_sparse_client;
	private short id_combo_ally;

	/**
	 * Holds triggers created on server side to create emissions at client side
	 */
	private LinkedList<Trigger> triggers = new LinkedList<Trigger>();

	private ViewCircle view_circle;

	/**
	 * <code>false</code> if the player isn't in one of the enemies view circles,
	 * otherwise <code>true</code>
	 */
	private boolean visible = true;

	private ComboStorage combo_storage_transfer, combo_storage1, combo_storage2;

	private float vel_x_factor;

	/**
	 * holds all active effects that aren't one time effects. Timed effects have an
	 * apply-tickrate.
	 */
	private ArrayList<AEffectTimed> effects_timed;

	private ArrayList<AEffectWithNumber> effects_with_number;

	private int kill_count = 0;
	private int death_count = 0;
	private int assist_count = 0;
	private int crystal_count = 0;

	private TweenableSound sound_slide, sound_walk, sound_crystal_collected;

	private ErrorMessage error_message;

	public float health_regen, health_accu;
	public float mana_regen, mana_accu;

	private EmissionDefBoneFollowing walk_emission_def, slide_emission_def, land_emission_def, slide_jump_emission_def;
	private EmissionDefAngleDirected doublejump_emission_def;
	private Emission current_movement_emission;
	private AnimationName last_track_0;

	private int last_processed_input_id;

	private boolean prediction_verifier = false;

	private IEmitter potential_combo_ally;

	public static enum Location {
		CLIENTSIDE_MULTIPLAYER, CLIENTSIDE_TRAINING, SERVERSIDE
	}

	/**
	 * Describes all available animations.
	 * 
	 * @author lukassongajlo
	 * 
	 */
	public static enum AnimationName {
		WALK, JUMP_FIRST, JUMP_SECOND, LAND, LAND_STUNNED, CREEP_FIGHT_STANCE_ON, CREEP_FIGHT_STANCE_OFF, FALL, IDLE,
		STUNNED, RECEIVE_DAMAGE, BLACKHOLE, LIGHTPORT, COLOR_ICE, COLOR_TOXIC, COLOR_DEFAULT, HOLD_GROUND_LEFT,
		HOLD_GROUND_RIGHT, HOLD_CEILING_LEFT, HOLD_CEILING_RIGHT, SLIDE_GROUND_RIGHT, SLIDE_GROUND_LEFT,
		SLIDE_CEILING_RIGHT, SLIDE_CEILING_LEFT, P1_CHARGE, P1_CHANNEL, P3, DEATH_EXPLOSION, JUMP_SLIDE, ALPHA_FADE_IN,
		ALPHA_FADE_OUT;
	}

	/**
	 * This constructor is called during join process. The physical character are
	 * set as soon as the player is within the world.
	 * 
	 * @param metaClient
	 */
	public PhysicalClient(MetaClient metaClient, Location loc) {
		super(metaClient);
		input_queue = new DiscardingLimitedArrayList<InputRequest>(10) {
			@Override
			public String toString() {
				String s = "";
				Iterator<InputRequest> it = iterator();
				while (it.hasNext() == true) {
					InputRequest req = it.next();
					s += " [ mesID = " + req.message_id + ", arrTime = " + req.arrival_time + " ] \n";
				}
				return s;
			}
		};

		emission_list = new EmissionList(this);
		location = loc;
		combo_storage_transfer = new ComboStorage(0);
		combo_storage1 = new ComboStorage(1);
		combo_storage2 = new ComboStorage(2);
		my_sparse_client = new SparseClient();
		death_time_current = 0;
		death_time_absolute = 5f;
		// radius = 5
		view_circle = new ViewCircle(pos, PLAYER_VIEW_RADIUS);
		effects_timed = new ArrayList<>();
		effects_with_number = new ArrayList<>();
		sound_slide = new TweenableSound(AudioData.global_sliding, true);
		sound_walk = new TweenableSound(AudioData.global_walk, true);
		sound_crystal_collected = new TweenableSound(AudioData.global_crystal_collected, true);
	}

	@Override
	public void update() {

		synchronized (input_queue) {
			/*
			 * TODO ZEITABGLEICH (reconciliation): Mirko hatte vorgeschlagen, dass man
			 * irgendeinen Zeitwert einfügen sollte um anfängliche input-unterschiede
			 * auszumerzen. Bin allerdings noch etwas überfragt wie das gehen soll. Im
			 * Moment prune ich nur, wenn es zu viele Pakete werden. Eine Idee, die ich noch
			 * nicht ausprobiert hab, wäre im Vorfeld eines update()-Aufrufs (das erste nach
			 * dem synchronized zB) alle Pakete zu ermitteln, die zeitlich gesehen "zu alt"
			 * sind zu streichen.
			 */
			/*
			 * TODO ich hab auch festgestellt, dass sobald mehr Projektile im Spiel sind,
			 * lags beginnen. Evtl wäre es sinnvoll die Pakete noch weiter zu reduzieren von
			 * ihrer Größe, auch wenn das eigentlich in Ordnung zu sein scheint. Außerdem
			 * gerate ich beim Testen noch in eine Correction spirale, d.h. ich beginne mit
			 * Korrekturen, aber höre solange nicht damit auf bis ich stehen bleibe.
			 * Interessant wäre sicher nochmal zu schauen, ob eine Correction wirklich
			 * gerechtfertigt ist und vllt auch den Anfang einer solchen Spirale zu
			 * emitteln. Dafür muss ich die Ausgaben entsprechend stark reduzieren, sodass
			 * die Konsole frei bleibt von unnötigen Ausgaben.
			 */
			if (input_queue.size() > 5) {
				// windForwards = true;
				input_queue.clear();
			}
			// if (input_queue.size() < 3)
			// windForwards = false;

			if (input_queue.size() > 0) {

				if (System.currentTimeMillis() - 40 > input_queue.get(0).arrival_time
						|| location == Location.CLIENTSIDE_MULTIPLAYER) {

					InputRequest tempInput = input_queue.remove(0);

					/*
					 * make copy of non processed input before processing it (on server side)
					 */
					if (location == Location.SERVERSIDE) {
						redirected_inputs.add(new InputRequest(tempInput));
						meta_client.chat_message = tempInput.chat_message;
					}
					last_processed_input_id = tempInput.message_id;

					// update to most recent keys
					current_actions = tempInput.non_processed_input;

					// might be null
					id_combo_ally = tempInput.id_combo_ally;

					cursor_pos.set(tempInput.cursor_pos);

				} else
					return;
			}

		}

		// key down's & key up's
		super.update();

		if (Shared.DEBUG_MODE_SYSOS_NETCODE_CLIENT == true)
			if (location == Location.SERVERSIDE)
				System.out.println("PhysicalClient.update()			SERVER, left_dominant = " + left_dominant
						+ ", id = " + last_processed_input_id);
			else
				System.out.println("PhysicalClient.update()			CLIENT, left_dominant = " + left_dominant);

		updateCharacterAttributes();
		elapsed_time += Shared.SEND_AND_UPDATE_RATE_WORLD;

		// sort and update effects caused by emissions
		Collections.sort(effects_timed);
		Iterator<AEffectTimed> it = effects_timed.iterator();
		while (it.hasNext() == true)
			it.next().update(it);

		for (ASkill skill : skills)
			skill.update();

		if (isAlive() == true) {

			prev_alive = true;

			if (isCurrentActionActive(WorldAction.SWITCH_COMBO_MODE) == true)

				// find ally by cursor pos
				potential_combo_ally = findComboAllyByCursor();

			else
				potential_combo_ally = null;

			if (hasFinishedCurrentSkill() == true) {
				/*
				 * assign P3 when queued skill is null and the current skill has finished its
				 * animation
				 */
				info_animations.track_1 = AnimationName.P3;

				current_skill = null;
			}

			// update regen of health and mana

			float health_regen_base = 0;

//			if (base_circle != null)
//				if (Util.circleContains(base_circle, pos) == true)
//					// factor 14 in base
//					health_regen_base = 14 * health_regen;

			health_accu += (health_regen + health_regen_base);
			while (health_accu >= 1) {
				health_current += 1;
				if (health_current >= meta_client.health_absolute)
					health_current = meta_client.health_absolute;
				health_accu -= 1;
			}

			mana_accu += mana_regen;
			while (mana_accu >= 1) {
				mana_current += 1;
				if (mana_current >= meta_client.mana_absolute)
					mana_current = meta_client.mana_absolute;
				mana_accu -= 1;

			}

//			while (elapsed_time_sliding >= 0.2f) {
//
//				elapsed_time_sliding -= 0.2f;
//
//				// add mana
//				EffectMana effectMana = new EffectMana(3);
//				effectMana.setTarget(this);
//				effectMana.apply();
//			}

			// set air drag while sliding
			if (is_holding == false) {
				if (isSliding() == true && jumping.jump_active == false) {
					if (isMoving() == true) {

						// inc slide timer
//						elapsed_time_sliding += Shared.SEND_AND_UPDATE_RATE_WORLD;
						if (isSlidingGroundLeft() == true || isSlidingGroundRight() == true) {
							physical_hitbox.getBody().setLinearDamping(AIR_DRAG_SLIDING_GROUND);
						} else
							physical_hitbox.getBody().setLinearDamping(AIR_DRAG_SLIDING_CEILING);
					} else
						physical_hitbox.getBody().setLinearDamping(AIR_DRAG_DEFAULT);
				} else {

					elapsed_time_sliding = 0;

					physical_hitbox.getBody().setLinearDamping(AIR_DRAG_DEFAULT);
				}
			} else {
				// holding == true
				if (jumping.jump_active == false && under_impulse == false) {
					/*
					 * jumping and other impulses (from skills) are higher prioritized.
					 */
					physical_hitbox.getBody().setLinearDamping(400000f);
				}
			}

			if (is_holding == false)
				continousWalkingUpdate();

			// stands the player in one another?
			if (MathUtils.isZero(vel.len2()) == true) {
				for (PhysicalClient otherClient : physical_hitbox.separator_contacts.getListClients())
					if (MathUtils.isZero(otherClient.vel.len2()) == true) {
						// add random number to avoid the "temp = 0" -case
						float temp = pos.x - otherClient.pos.x + MathUtils.random(-0.001f, 0.001f);
						temp = 2 * SeparatorContacts.SEPARATOR_RADIUS * (temp < 0 ? -1 : 1) - temp;
						applyLinearImpulse(new Vector2(temp * 1.5f, 0.1f));
					}
				for (SparseObject otherCreep : physical_hitbox.separator_contacts.getListCreeps())
					if (MathUtils.isZero(otherCreep.vel.len2()) == true) {
						// add random number to avoid the "temp = 0" -case
						float temp = pos.x - otherCreep.pos.x + MathUtils.random(-0.001f, 0.001f);
						temp = 2 * SeparatorContacts.SEPARATOR_RADIUS * (temp < 0 ? -1 : 1) - temp;
						applyLinearImpulse(new Vector2(temp * 1.5f, 0f));
					}
			}
			if (jumping.jump_active == false) {
				// change animation only when jump is not active
				if (isLanding() == true)
					landing.start();
				// animation_track_0 = AnimationName.LAND;
				else if (isFreeFalling() == true) {
					info_animations.track_0 = AnimationName.FALL;
				} else if (physical_hitbox.underside_contacts.isTouching() == true
						&& info_animations.track_0 != AnimationName.WALK)
					info_animations.track_0 = AnimationName.IDLE;

				// update landing (has to be called here, because of order)
				landing.update();

				if (is_holding == false) {
					if (isMoving() == true) {
						if (isSlidingCeilingLeft() == true)
							info_animations.track_0 = AnimationName.SLIDE_CEILING_LEFT;
						else if (isSlidingCeilingRight() == true)
							info_animations.track_0 = AnimationName.SLIDE_CEILING_RIGHT;
						else if (isSlidingGroundLeft() == true)
							info_animations.track_0 = AnimationName.SLIDE_GROUND_LEFT;
						else if (isSlidingGroundRight() == true)
							info_animations.track_0 = AnimationName.SLIDE_GROUND_RIGHT;
					} else {
						// vel == 0, but not holding
						if (isSlidingCeilingLeft() == true)
							info_animations.track_0 = AnimationName.HOLD_CEILING_LEFT;
						else if (isSlidingCeilingRight() == true)
							info_animations.track_0 = AnimationName.HOLD_CEILING_RIGHT;
						else if (isSlidingGroundLeft() == true)
							info_animations.track_0 = AnimationName.HOLD_GROUND_LEFT;
						else if (isSlidingGroundRight() == true)
							info_animations.track_0 = AnimationName.HOLD_GROUND_RIGHT;
					}
				} else {
					if (isSlidingCeilingLeft() == true)
						info_animations.track_0 = AnimationName.HOLD_CEILING_LEFT;
					else if (isSlidingCeilingRight() == true)
						info_animations.track_0 = AnimationName.HOLD_CEILING_RIGHT;
					else if (isSlidingGroundLeft() == true)
						info_animations.track_0 = AnimationName.HOLD_GROUND_LEFT;
					else if (isSlidingGroundRight() == true)
						info_animations.track_0 = AnimationName.HOLD_GROUND_RIGHT;
				}
			}

			if (isUnderImpulse() == true)
				info_animations.track_0 = AnimationName.FALL;

			// override if player is stunned
			if (stunned == true && landing.isActive() == false)
				info_animations.track_0 = AnimationName.STUNNED;

		}

		// set flag from dead to alive
		boolean fromDeadToAlive = death_time_current > 1 && death_time_current - Shared.SEND_AND_UPDATE_RATE_WORLD <= 1;
		if (location != Location.CLIENTSIDE_MULTIPLAYER) {

			death_time_current -= Shared.SEND_AND_UPDATE_RATE_WORLD;

		}
		if (fromDeadToAlive == true) {

			if (match_finished == false) {
				cancelCurrentSkill();
				spawn_skill_after_death.init();
			}
		}

		// update sounds
		if (location != Location.SERVERSIDE) {
			if (is_holding == false && isSliding() == true && jumping.jump_active == false && isMoving() == true) {
				AudioData.loopSoundWithFadeIn(sound_slide, 1f);
			} else
				AudioData.pauseSoundWithFadeOut(sound_slide, 1f);

			if (info_animations.track_0 == AnimationName.WALK)
				AudioData.loopSoundWithFadeIn(sound_walk, 1f);
			else
				AudioData.pauseSoundWithFadeOut(sound_walk, 1f);

			// update volumes
			sound_walk.updateVolume(pos.x, pos.y);
			sound_slide.updateVolume(pos.x, pos.y);

			jumping.updateSounds(pos.x, pos.y);
			landing.updateSounds(pos.x, pos.y);

		}

	}

	private IEmitter findComboAllyByCursor() {

		IEmitter ally = null;

		if (location == Location.SERVERSIDE)
			for (ServerPhysicalClient client : GameserverLogic.PLAYER_MAP.values()) {

				if (client == this)
					continue;

				if (client.getHitbox() == null)
					continue;

				if (client.getTeam() == meta_client.team) {
					if (ally == null)
						ally = client;
					else {
						if (cursor_pos.dst(client.pos) < cursor_pos.dst(ally.getPos()))
							ally = client;
					}
				}
			}

		else {
			for (OmniClient omniClient : LogicTier.WORLD_PLAYER_MAP.values()) {

				PhysicalClient client = omniClient.getPhysicalClient();

				if (client == this)
					continue;

				if (client.getHitbox() == null)
					continue;

				if (client.getTeam() == meta_client.team) {
					if (ally == null)
						ally = client;
					else {
						if (cursor_pos.dst(client.pos) < cursor_pos.dst(ally.getPos()))
							ally = client;
					}
				}
			}
		}

		return ally;
	}

	/**
	 * After a world step the physical_character (with body member) holds new values
	 * about position and velocity. We need to update the position and velocity of
	 * this class ( {@link PhysicalClient} ).
	 */
	public void updateAfterStep() {
		pos.set(physical_hitbox.getPosition());
		vel.set(physical_hitbox.getVelocity());

		emission_list.update();

		// the player might got a deadly shot in step()
		if (isAlive() == false && prev_alive == true) {

			prev_alive = false;
			// any resurrection effects active?
			for (AEffectTimed effect : effects_timed)
				if (effect instanceof EffectResurrection)
					/*
					 * setting this flag will lead to initiate the right emission tree in
					 * DyingSkill.
					 */
					dying_skill.setResurrecting(true);

			cancelCurrentSkill();

			dying_skill.init();
		}

		// current_skill_animation = null;
	}

	/**
	 * Sets the attributes depending on the gameclass which has to be set
	 * previously. Note that the {@link #physical_hitbox} must not be
	 * <code>null</code>
	 * 
	 * @param physicalHitbox
	 * @param drawableClient , optional
	 */
	public void updateByMetaData(PhysicalHitbox physicalHitbox, DrawableClient drawableClient) {
		if (physicalHitbox != null) {

			/*
			 * if player changes its gameclass by scroller (training)
			 */
			cancelCurrentSkill();

			walk_emission_def = new EmissionDefBoneFollowing("graphic/particles/particle_global/global_walk.p", 1, 1,
					false,
					new DefProjectile(Integer.MAX_VALUE, ASkill.getNeutralFilter()).setInterruptibleByEmission(true),
					"character_torso").setTransmissible(false)
							.setParticleEffectCategory(ParticleEffectCategory.MOVEMENT);
			walk_emission_def.setBonesModel(drawableClient);
			slide_emission_def = new EmissionDefBoneFollowing("graphic/particles/particle_global/global_slide.p", 1, 1,
					false,
					new DefProjectile(Integer.MAX_VALUE, ASkill.getNeutralFilter()).setInterruptibleByEmission(true),
					"character_hand_back").setTransmissible(false)
							.setParticleEffectCategory(ParticleEffectCategory.MOVEMENT);
			slide_emission_def.setBonesModel(drawableClient);
			land_emission_def = new EmissionDefBoneFollowing("graphic/particles/particle_global/global_land.p", 1, 1,
					false,
					new DefProjectile(Integer.MAX_VALUE, ASkill.getNeutralFilter()).setInterruptibleByEmission(true),
					"character_torso").setTransmissible(false)
							.setParticleEffectCategory(ParticleEffectCategory.MOVEMENT);
			land_emission_def.setBonesModel(drawableClient);
			doublejump_emission_def = new EmissionDefAngleDirected(
					"graphic/particles/particle_global/global_doublejump.p", 1, 1, false,
					new DefProjectile(Integer.MAX_VALUE, ASkill.getNeutralFilter()).setInterruptibleByEmission(true), 1,
					0, new FixedAlignment(0)).setTransmissible(false)
							.setParticleEffectCategory(ParticleEffectCategory.MOVEMENT);
			slide_jump_emission_def = new EmissionDefBoneFollowing(
					"graphic/particles/particle_global/global_slide_jump.p", 1, 1, false,
					new DefProjectile(Integer.MAX_VALUE, ASkill.getNeutralFilter()).setInterruptibleByEmission(true),
					"character_torso").setTransmissible(false)
							.setParticleEffectCategory(ParticleEffectCategory.MOVEMENT);
			slide_jump_emission_def.setBonesModel(drawableClient);

			skills = getGameclass().getSkills(drawableClient, this, null);

			vel_x_factor = getGameclass().initVelXFactor();

			spawn_skill_after_death = skills.get(0);
			dying_skill = (DyingSkill) skills.get(1);
			skill_0 = (AChargeSkill) skills.get(2);
			skill_1 = (AChargeSkill) skills.get(3);
			skill_2 = (AChargeSkill) skills.get(4);
			transfer_skill_1 = (TransferSkill) skills.get(skills.size() - 2);
			transfer_skill_2 = (TransferSkill) skills.get(skills.size() - 1);

			Vector2 healthMana = getGameclass().getStartHealthMana();
			meta_client.health_absolute = (short) healthMana.x;
			meta_client.mana_absolute = (short) healthMana.y;

			Vector2 healthManaRegen = getGameclass().getStartHealthManaRegen();
			health_regen = healthManaRegen.x * meta_client.health_absolute * SEND_AND_UPDATE_RATE_WORLD;
			mana_regen = healthManaRegen.y * meta_client.mana_absolute * SEND_AND_UPDATE_RATE_WORLD;

			setAlive();
		}
	}

	/**
	 * returns <code>true</code> if current velocity's vector length is greater than
	 * 0.
	 * 
	 * @return
	 */
	private boolean isMoving() {
		return physical_hitbox.getBody().getLinearVelocity().len() > 0.01f;
	}

	/**
	 * This method update all member of this class including animation!
	 */
	private void updateCharacterAttributes() {

		/*
		 * player_can_secondjump: nicht nur nach erster jump sondern auch wenn man in
		 * der luft ist ohne davor gesprungen zu sein (von einer platform runterlaufen):
		 */
		player_can_secondjump = resetSecondJump() == true ? true : player_can_secondjump;

		platform_angle = physical_hitbox.underside_contacts.getPlatformAngle();

		// pos.set(physical_hitbox.getPosition());

		jumping.update(isCurrentActionActive(WorldAction.JUMP));

		sparse_combo_storage1.meta_skill_id = combo_storage1.meta_skill_id;
		sparse_combo_storage1.duration_ratio = combo_storage1.duration_ratio;

		sparse_combo_storage2.meta_skill_id = combo_storage2.meta_skill_id;
		sparse_combo_storage2.duration_ratio = combo_storage2.duration_ratio;

		// TODO beim jumping sehe ich noch probleme fürs predicten!

	}

	private void continousWalkingUpdate() {
		if (isMovementEnabled() == true) {
			boolean moveLeft = isCurrentActionActive(WorldAction.MOVE_LEFT);
			boolean moveRight = isCurrentActionActive(WorldAction.MOVE_RIGHT);

			if (moveLeft == false && moveRight == true && match_finished == false)
				walkingRoutine(false);

			else if (moveRight == false && moveLeft == true && match_finished == false)
				walkingRoutine(true);

			else if (moveRight == true && moveLeft == true && match_finished == false)
				if (left_dominant)
					walkingRoutine(true);
				else
					walkingRoutine(false);
			else {
				/*
				 * neither A nor D, den fall würde ich lieber hier behandeln, anstatt im
				 * contactlistener, denn der kontakt listener macht nur aussagen, wenn ein
				 * kontakt vorhanden ist. das führt dann in der luft zu fehlverhalten.
				 */
				if (isSliding() == false)
					if (brake_enabled == true)
						applyLinearImpulse(new Vector2(-physical_hitbox.getBody().getLinearVelocity().x, 0));
			}
		} else
		// brake
		if (isSliding() == false)
			if (jumping.jump_active == false)
				applyLinearImpulse(new Vector2(-physical_hitbox.getBody().getLinearVelocity().x, 0));

	}

	/**
	 * This method handles walking, and its animation state if player has contact to
	 * the walkable ground with underside or with foot sensors. There are two places
	 * where I say "current_stance_animation = AnimationName.WALK;"
	 * <p>
	 * 
	 * @param left
	 */
	private void walkingRoutine(boolean left) {

		/*
		 * desiredVelX will be split into a x- and y component, when walking on an
		 * inclined. Might be negative. Usage: first we determine the desiredVelX,
		 * including inertia. Then we look how much we are away from the current
		 * velocity x, the difference will be applied with an impulse. When you apply
		 * the whole negative velocity (new Vector2(-vel.x, -vel.y)) you remove the
		 * velocity completely. When you apply 0 you say, that the current velocity
		 * should be maintained.
		 */
		float desiredVelX = 0;
		/*
		 * y component of desired velocity
		 */
		float desiredVelY = 0;

		// last veloctiy data
		float currentVelX = physical_hitbox.getBody().getLinearVelocity().x,
				currentVelY = physical_hitbox.getBody().getLinearVelocity().y;

		float velMovementFactor = vel_x_factor;

		// step by step increasing for inertia/acceleration
		if (left == true) {

			desiredVelX = Math.max(currentVelX - CHARACTER_INERTIA_VELOCITY_X * velMovementFactor,
					-CHARACTER_MAX_VELOCITY_X * velMovementFactor);
		} else {

			desiredVelX = Math.min(currentVelX + CHARACTER_INERTIA_VELOCITY_X * velMovementFactor,
					CHARACTER_MAX_VELOCITY_X * velMovementFactor);
		}

		if (physical_hitbox.underside_contacts.isTouching() == true) {
			if (isSliding() == false) {
				if (jumping.jump_active == false) {

					info_animations.track_0 = AnimationName.WALK;

					if (current_skill != null && current_skill.isWalkingBrake() == true) {
						applyLinearImpulse(new Vector2(physical_hitbox.getBody().getLinearVelocity().x * 0.75f
								- physical_hitbox.getBody().getLinearVelocity().x, 0));
					} else {
						desiredVelY = getCorrespondingVelY(desiredVelX, platform_angle);
						applyLinearImpulse(new Vector2(desiredVelX - currentVelX, desiredVelY - currentVelY));
					}
				}
			} else if (elapsed_time > STICKING_DELAY_ON_SLIDABLE)
				if (jumping.jump_active == false) {

					// hochlaufen stoppen

					if (isSlidingLeft() == true) {
						if (isCurrentActionActive(WorldAction.MOVE_LEFT) == true && left_dominant == true)
							return;
					}

					if (isSlidingRight() == true) {
						if (isCurrentActionActive(WorldAction.MOVE_RIGHT) == true && left_dominant == false)
							return;
					}

					applyLinearImpulse(new Vector2(desiredVelX - currentVelX, 0));
				}
		} else {
			// not touching underside
			// A or D pressed
			/*
			 * hier geht es darum zu entscheiden was direkt nach einem sprung passieren
			 * soll. Ein sprung kann impulse nach rechts oder links haben, diesen will ich
			 * hier nicht überschreiben! sondern eher dazu addieren.
			 */

			if (jumping.jump_active == true) {

				// handle case when player jumps from a slidable platform
				if (left == true && currentVelX > 0 && elapsed_time < WALKING_DELAY_AFTER_SLIDEJUMP)
					return;

				if (left == false && currentVelX < 0 && elapsed_time < WALKING_DELAY_AFTER_SLIDEJUMP)
					return;

				if (current_skill != null && current_skill.isWalkingBrake() == true)
					applyLinearImpulse(new Vector2(physical_hitbox.getBody().getLinearVelocity().x * 0.9f
							- physical_hitbox.getBody().getLinearVelocity().x, 0));
				else
					applyLinearImpulse(new Vector2(desiredVelX - currentVelX, 0));

			} else {
				/*
				 * ceiling,walk hüpfer, slide hüpfer, jump2
				 */

				// not underside touching and not first jump

				// damit halte ich mich während des delays noch am sliding
				// (ceilling)
				if (elapsed_time < STICKING_DELAY_ON_SLIDABLE) {
					if (isSlidingCeilingLeft() == true && left == false) {

						desiredVelX = Math.min(currentVelX - CHARACTER_INERTIA_VELOCITY_X * velMovementFactor,
								-CHARACTER_MAX_VELOCITY_X * velMovementFactor);
						applyLinearImpulse(new Vector2(desiredVelX - currentVelX, 0));

						return;
					}
					if (isSlidingCeilingRight() == true && left == true) {

						desiredVelX = Math.max(currentVelX + CHARACTER_INERTIA_VELOCITY_X * velMovementFactor,
								CHARACTER_MAX_VELOCITY_X * velMovementFactor);

						applyLinearImpulse(new Vector2(desiredVelX - currentVelX, 0));

						return;
					}

				}

				if (physical_hitbox.foot_contacts.isTouching() == true && isSliding() == false) {

					info_animations.track_0 = AnimationName.WALK;
					return;
				}

				if (current_skill != null && current_skill.isWalkingBrake() == true)
					applyLinearImpulse(new Vector2(physical_hitbox.getBody().getLinearVelocity().x * 0.9f
							- physical_hitbox.getBody().getLinearVelocity().x, 0));
				else
					// for falling and second jump
					applyLinearImpulse(new Vector2(desiredVelX - currentVelX, 0));
			}
		}
	}

	private void startMovementEmission(AEmissionDef movementEmissionDef) {
		if (current_movement_emission != null)
			current_movement_emission.setInterrupted(true);
		if (movementEmissionDef == null)
			return;
		movementEmissionDef.setCenter(pos);
		current_movement_emission = new Emission(movementEmissionDef, this);
		emission_list.add(current_movement_emission);

	}

	/**
	 * percent = 100%
	 */
	public void applyLinearImpulseToIdle() {
		applyLinearImpulseToIdle(1f);
	}

	public void applyLinearImpulseToIdle(float percent) {
		applyLinearImpulse(new Vector2(-physical_hitbox.getBody().getLinearVelocity().x * percent,
				-physical_hitbox.getBody().getLinearVelocity().y * percent));
	}

	public void applyLinearImpulse(Vector2 impulse) {
		physical_hitbox.getBody().applyLinearImpulse(impulse, physical_hitbox.getBody().getWorldCenter(), false);
	}

	/**
	 * returns vel y depending on platform angle and the corresponding velX
	 * 
	 * @param velX
	 * @param platformAngle
	 * @return
	 */
	public static float getCorrespondingVelY(float velX, int platformAngle) {

		if (isSlidableByAngle(platformAngle) == true)
			return 0;

		float desiredVelY = (float) (velX * Math.tan(platformAngle * MathUtils.degreesToRadians));

		if (platformAngle > 90)
			if (velX > 0)
				if (desiredVelY > 0)
					desiredVelY *= -1;

		/*
		 * nachfolgende zeile führte auf abrundungen zu leichtem abheben. man schlittert
		 * förmlich über die platformen
		 */
		// desiredVelY -= ANTI_GRAVITY;

		return desiredVelY;
	}

	/**
	 * Checks all 4 sensors
	 * 
	 * 
	 * @return
	 */
	public boolean isSliding() {
		return isSlidingLeft() || isSlidingRight();
	}

	/**
	 * Checks both left sensors
	 * 
	 * @return
	 */
	private boolean isSlidingLeft() {
		return isSlidingCeilingLeft() || isSlidingGroundLeft();
	}

	/**
	 * Checks both right sensors
	 * 
	 * @return
	 */
	private boolean isSlidingRight() {
		return isSlidingCeilingRight() || isSlidingGroundRight();
	}

	public boolean isSlidingCeilingRight() {
		return physical_hitbox.right_top_contacts.isTouching() == true;
	}

	private boolean isSlidingGroundRight() {
		return physical_hitbox.right_bottom_contacts.isTouching() == true;
	}

	public boolean isSlidingCeilingLeft() {
		return physical_hitbox.left_top_contacts.isTouching() == true;
	}

	private boolean isSlidingGroundLeft() {
		return physical_hitbox.left_bottom_contacts.isTouching() == true;
	}

	/**
	 * Returns <code>true</code> if any sensor touches the ground
	 * 
	 * @return
	 */
	public boolean isTouching() {
		// avoid code duplication
		return resetSecondJump();
	}

	/**
	 * Returns <code>true</code> if angle is for slidable platforms.
	 * 
	 * @param angle
	 * @return
	 */
	public static boolean isSlidableByAngle(int angle) {
		/*
		 * the check before assumed that the angle will be minimized (minimized means
		 * that greater angle values will be reduced, so all angles will be flipped to
		 * one side of the unit circle
		 */
		// return angle >= WALKABLE_ANGLE && angle <= 180 - WALKABLE_ANGLE;

		/*
		 * unlike previously we check now both sides of the unit circle, because the
		 * passed angle won't be minimized before. The commented part will increase the
		 * amount of nonwalk- platforms
		 */
		return angle >= WALKABLE_ANGLE && angle <= 180 - WALKABLE_ANGLE // - 10
				|| angle <= 360 - WALKABLE_ANGLE && angle >= 180 + WALKABLE_ANGLE;// +
																					// 10;
	}

	/**
	 * Return might be <code>null</code> if team is <code>null</code>
	 * 
	 * @return
	 */
	public Vector2 getSpawnPos() {

		Vector2 spawnPos = null;

		if (physical_hitbox.team == Team.TEAM_1)
			spawnPos = new Vector2(Base.LEFT_BASE_CENTER);
		else if (physical_hitbox.team == Team.TEAM_2)
			spawnPos = new Vector2(Base.RIGHT_BASE_CENTER);

		if (spawnPos != null)
			// shift in y dir
			spawnPos.y = Base.Y_SPAWN_SHIFT;

		return spawnPos;
	}

	private boolean playerCanFirstJump() {
		return physical_hitbox.foot_contacts.isTouching() || isSliding();

	}

	/**
	 * the "player_can_secondjump"-field resets to 'true' if one of the lower
	 * sensors touches the ground.
	 * 
	 * @return
	 */
	private boolean resetSecondJump() {
		return physical_hitbox.left_bottom_contacts.isTouching() || physical_hitbox.right_bottom_contacts.isTouching()
				|| physical_hitbox.foot_contacts.isTouching() || physical_hitbox.right_top_contacts.isTouching()
				|| physical_hitbox.left_top_contacts.isTouching();
	}

	public void setupBySnapshot(PhysicalClientSnapshot snapshot) {
		brake_enabled = snapshot.brake_enabled;
		current_actions = snapshot.current_actions;
		getBody().setGravityScale(snapshot.gravity_scale);
		getBody().setActive(snapshot.body_active);
		cursor_pos = new Vector2(snapshot.cursor_pos);
		death_time_absolute = snapshot.death_time_absolute;
		death_time_current = snapshot.death_time_current;
		elapsed_time = snapshot.elapsed_time;
		elapsed_time_sliding = snapshot.elapsed_time_sliding;
		health_accu = snapshot.health_accu;
		health_regen = snapshot.health_regen;
		jumping.is_first_jump = snapshot.is_first_jump;
		jumping.is_slide_jump = snapshot.is_slide_jump;
		jumping.jump_elapsed_time_ms = snapshot.jump_elapsed_time_ms;
		jumping.jump_max_time_ms = snapshot.jump_max_time_ms;
		is_holding = snapshot.is_holding;
		left_dominant = snapshot.left_dominant;
		mana_accu = snapshot.mana_accu;
		mana_regen = snapshot.mana_regen;
		platform_angle = snapshot.platform_angle;
		player_can_secondjump = snapshot.player_can_secondjump;
		prev_actions = snapshot.prev_actions;
		prev_alive = snapshot.prev_alive;
		time = snapshot.time;
		vel_x_factor = snapshot.vel_x_factor;
		stunned = snapshot.stunned;
		rooted = snapshot.rooted;
		disarmed = snapshot.disarmed;
		silenced = snapshot.silenced;
		under_impulse = snapshot.under_impulse;
		cooldown_skill0 = snapshot.cooldown_skill0;
		cooldown_skill1 = snapshot.cooldown_skill1;
		cooldown_skill2 = snapshot.cooldown_skill2;
	}

	/**
	 * Checks whether there is a difference between the number of contacts of the
	 * foot sensor LAST time and now. If the last number of contacts are zero and
	 * the current one is higher than 0 (isTouching the ground) the player is
	 * landing.
	 * 
	 * @return
	 */
	private boolean isLanding() {
		boolean playerIsLanding = false;
		if (physical_hitbox.landing_contacts.isTouching() == true && num_contacts_landing == 0
				&& physical_hitbox.landing_contacts.getVelY() <= 0)
			playerIsLanding = true;
		num_contacts_landing = physical_hitbox.landing_contacts.getNumContacts();
		return playerIsLanding;
	}

	/**
	 * To determine animation.
	 * 
	 * @return
	 */
	public boolean isFreeFalling() {
		return physical_hitbox.head_contacts.isTouching() == false
				&& physical_hitbox.left_bottom_contacts.isTouching() == false
				&& physical_hitbox.right_bottom_contacts.isTouching() == false
				&& physical_hitbox.left_top_contacts.isTouching() == false
				&& physical_hitbox.right_top_contacts.isTouching() == false
				&& physical_hitbox.underside_contacts.isTouching() == false
				&& physical_hitbox.foot_contacts.isTouching() == false
				&& physical_hitbox.getBody().getLinearVelocity().y <= 0;
	}

	/**
	 * movement is enabled if {@link #stunned}, {@link #rooted} and
	 * {@link #under_impulse} are equal <code>false</code>
	 * 
	 * @return
	 */
	private boolean isMovementEnabled() {
		return stunned == false && rooted == false && under_impulse == false;
	}

	@Override
	public void addEffect(AEffectTimed newEffect) {

		effects_timed.add(newEffect);
		// sort by priority
		Collections.sort(effects_timed);

		if (newEffect instanceof EffectEnableCombining)
			/*
			 * multiple EffectEnableCombining effects will be handled in apply()
			 */
			return;

		Iterator<AEffectTimed> it = effects_timed.iterator();

		while (it.hasNext() == true) {
			AEffectTimed oldEffect = it.next();

			if (newEffect != oldEffect) {

				if (newEffect.isStackable() == true) {

					if (newEffect.getClass().equals(oldEffect.getClass()) == true
							&& newEffect.getPoolID() == oldEffect.getPoolID())
						oldEffect.reset();

					continue;
				}

				/*
				 * each effect has an unique pool-ID given by its corresponding emission. So if
				 * two effect-objects are derived from the same class AND has the same unique
				 * pool ID then we remove the one with the shorter remaining duration.
				 */
				if (newEffect.getClass().equals(oldEffect.getClass()) == false)
					continue;

				if (newEffect.getPoolID() != oldEffect.getPoolID())
					continue;

				/*
				 * if both effects are different objects but both are equally in classes and
				 * pool ID then we kill the older one because its duration is guaranteed less
				 * than the new incoming one.
				 */

				oldEffect.killEffect();
			}
		}
	}

	public void addEffectWithNumber(AEffectWithNumber effect) {
		effects_with_number.add(effect);
	}

	public void addInput(InputRequest req) {
		synchronized (input_queue) {
			input_queue.add(req);
		}
	}

	/**
	 * adds new input requests to the {@link #input_queue}.
	 * 
	 * @param nonProcessedInputs, list of {@link InputRequest}
	 */
	public void addToInputQueue(NotNullArrayList<InputRequest> nonProcessedInputs) {

		for (int i = 0; i < nonProcessedInputs.size(); i++)
			addInput(new InputRequest(nonProcessedInputs.get(i)));

	}

	@Override
	protected void onetimeActionInactive(WorldAction action) {
		super.onetimeActionInactive(action);

		switch (action) {
		case MOVE_LEFT:
			if (isSliding() == true)
				elapsed_time = 0;

			brake_enabled = true;

			break;
		case MOVE_RIGHT:
			if (isSliding() == true)
				elapsed_time = 0;

			brake_enabled = true;

			break;
		case HOLD:
			is_holding = false;
			break;
		case EXECUTE_SKILL0:
			skill_0.release();
			break;
		case EXECUTE_SKILL1:
			skill_1.release();
			break;
		case EXECUTE_SKILL2:
			skill_2.release();
			break;
		}
	}

	@Override
	protected void onetimeActionActive(WorldAction action) {
		super.onetimeActionActive(action);

		if (isAlive() == false)
			return;

		// alive == true

		switch (action) {

		case MOVE_LEFT:

			if (isMovementEnabled() == true)
				if (isSliding() == true)
					elapsed_time = 0;

			break;
		case MOVE_RIGHT:

			if (isMovementEnabled() == true) {
				if (isSliding() == true)
					elapsed_time = 0;
			}
			break;
		case JUMP:
			if (isMovementEnabled() == true) {
				physical_hitbox.getBody().setLinearDamping(AIR_DRAG_DEFAULT);

				if (playerCanFirstJump() == true) {
					jumping.start(true, calcJumpImpulse());
					player_can_secondjump = true;
				} else if (player_can_secondjump == true) {
					jumping.start(false, calcJumpImpulse());
					player_can_secondjump = false; // just for physical
													// stuff
				}
			}
			break;

		case EXECUTE_TRANSFER_SKILL1:
			initSkill(action, skill_1, transfer_skill_1, true);
			break;

		case EXECUTE_TRANSFER_SKILL2:
			initSkill(action, skill_2, transfer_skill_2, true);
			break;
		}

	}

	@Override
	protected void continuousAction(WorldAction action) {

		if (isAlive() == false)
			return;

		// alive == true

		switch (action) {

		case EXECUTE_SKILL0:
			if (location == Location.SERVERSIDE) {
				/*
				 * clientside skill needs to be successful in order to initiate the skill on
				 * server side
				 */
				if ((WorldAction.SKILL0_SUCCESSFUL_CLIENTSIDE.getValue() & current_actions) == 0)
					return;

			}
			if (location == Location.CLIENTSIDE_MULTIPLAYER) {
				if (LogicTier.getMyClient() != null && LogicTier.getMyClient().getPhysicalClient() != this) {
					if ((WorldAction.SKILL0_SUCCESSFUL_CLIENTSIDE.getValue() & current_actions) != 0) {
						/*
						 * We land here if this is a character is not my own character and we are in a
						 * multiplayer scenario. So a redirected input had the information to execute a
						 * skill and on the server this skill was already successful. So we want to
						 * initiate the skill's charge emission without anything else.
						 */
						skill_0.executeChargeEmission();
						return;
					}
				}
			}

			initSkill(action, skill_0, null, false);
			break;

		case EXECUTE_SKILL1:
			if (location == Location.SERVERSIDE) {
				/*
				 * clientside skill needs to be successful in order to initiate the skill on
				 * server side
				 */
				if ((WorldAction.SKILL1_SUCCESSFUL_CLIENTSIDE.getValue() & current_actions) == 0)
					return;
			} else if (location == Location.CLIENTSIDE_MULTIPLAYER) {
				if (LogicTier.getMyClient() != null && LogicTier.getMyClient().getPhysicalClient() != this) {
					if ((WorldAction.SKILL1_SUCCESSFUL_CLIENTSIDE.getValue() & current_actions) != 0) {
						/*
						 * We land here if this is a character is not my own character and we are in a
						 * multiplayer scenario. So a redirected input had the information to execute a
						 * skill and on the server this skill was already successful. So we want to
						 * initiate the skill's charge emission without anything else.
						 */
						skill_1.executeChargeEmission();
						return;
					}
				}
			}

			initSkill(action, skill_1, null, false);
			break;

		case EXECUTE_SKILL2:
			if (location == Location.SERVERSIDE) {
				/*
				 * clientside skill needs to be successful in order to initiate the skill on
				 * server side
				 */
				if ((WorldAction.SKILL2_SUCCESSFUL_CLIENTSIDE.getValue() & current_actions) == 0)
					return;
			} else if (location == Location.CLIENTSIDE_MULTIPLAYER) {
				if (LogicTier.getMyClient() != null && LogicTier.getMyClient().getPhysicalClient() != this) {
					if ((WorldAction.SKILL2_SUCCESSFUL_CLIENTSIDE.getValue() & current_actions) != 0) {
						/*
						 * We land here if this is a character is not my own character and we are in a
						 * multiplayer scenario. So a redirected input had the information to execute a
						 * skill and on the server this skill was already successful. So we want to
						 * initiate the skill's charge emission without anything else.
						 */
						skill_2.executeChargeEmission();
						return;
					}
				}
			}

			initSkill(action, skill_2, null, false);
			break;

		case HOLD:
			if (isMovementEnabled() == true) {
				if (isSliding() == true)
					is_holding = true;
				else
					is_holding = false;
				break;
			} else
				is_holding = false;
		}

	}

	/**
	 * this method initiates the passed skill unless the comboMode is not active
	 * 
	 * @param skill
	 */
	private void initSkill(WorldAction action, ASkill skill, TransferSkill transferSkill, boolean comboMode) {

		if (comboMode == false) {
			// normal skill

			if (skill.init() == false) {
				/*
				 * if skill initiation wasn't successful we don't want to send the command to
				 * the game server since on server side we maybe have enough mana to initiate a
				 * skill while on client side we don't. Otherwise this would lead to an E1
				 * projectile from the server and not seeing any skill animation on client side.
				 */
				if (action == WorldAction.EXECUTE_SKILL2)
					setCurrentAction(WorldAction.SKILL2_SUCCESSFUL_CLIENTSIDE, false);
				if (action == WorldAction.EXECUTE_SKILL1)
					setCurrentAction(WorldAction.SKILL1_SUCCESSFUL_CLIENTSIDE, false);
				if (action == WorldAction.EXECUTE_SKILL0)
					setCurrentAction(WorldAction.SKILL0_SUCCESSFUL_CLIENTSIDE, false);
			} else {
				// skill successful
				if (action == WorldAction.EXECUTE_SKILL2)
					setCurrentAction(WorldAction.SKILL2_SUCCESSFUL_CLIENTSIDE, true);
				if (action == WorldAction.EXECUTE_SKILL1)
					setCurrentAction(WorldAction.SKILL1_SUCCESSFUL_CLIENTSIDE, true);
				if (action == WorldAction.EXECUTE_SKILL0)
					setCurrentAction(WorldAction.SKILL0_SUCCESSFUL_CLIENTSIDE, true);
			}

			return;
		}

		// combo mode active

		if (transferSkill == null)
			return;

		if (skill.getCooldownCurrent() > 0) {
			setErrorMessage(ErrorMessage.abilityOnCooldown);
			return;
		}

		if (potential_combo_ally == null) {
			setErrorMessage(ErrorMessage.noAllyAround);
			return;
		}

		transferSkill.setCorrespondingSkill(skill);
		transferSkill.setAlly(potential_combo_ally);
		transferSkill.init();
//
//		if (id_combo_ally == -1)
//			return;
//
//		if (skill.getCooldownCurrent() > 0) {
//			setErrorMessage(ErrorMessage.cooldownActive);
//			return;
//		}
//
//		IEmitter ally = null;
//
//		/*
//		 * find ally by game class name
//		 */
//		if (location == Location.SERVERSIDE)
//			ally = GameserverLogic.PLAYER_MAP.get(id_combo_ally);
//		else {
//			OmniClient client = LogicTier.WORLD_PLAYER_MAP.get(id_combo_ally);
//			if (client == null)
//				return;
//			ally = client.getPhysicalClient();
//		}
//
//		if (ally == null)
//			return;
//
//		if (ally != this) {
//
//			if (ally.getTeam() == getTeam()) {
//				transferSkill.setCorrespondingSkill(skill);
//				transferSkill.setAlly(ally);
//				transferSkill.init();
//			}
//		}

	}

	/**
	 * Returns a vector2 impulse for jump. If player is on a walkable platform the
	 * impulse is just straight upwards, otherwise on a slidable platform the
	 * impulse gets a non-zero x component (except platforms for ground slides).
	 * 
	 * @return
	 */
	private Vector2 calcJumpImpulse() {

		/*
		 * just walk and slide ground will be handled by straight impulse in y
		 * direction.
		 */

		Vector2 impulse = new Vector2(0, JUMP_IMPULSE_Y);

		if (isSlidingCeilingLeft() == true) {
			impulse.set(JUMP_IMPULSE_CEILING);
			elapsed_time = 0;
		} else if (isSlidingCeilingRight() == true) {
			impulse.set(JUMP_IMPULSE_CEILING);
			impulse.x *= (-1);
			elapsed_time = 0;
		} else if (isSlidingGroundLeft() == true) {
			elapsed_time = 0;
			// impulse just upwards
		} else if (isSlidingGroundRight() == true) {
			elapsed_time = 0;
			// impulse just upwards
		}

		return impulse;
	}

	/**
	 * This method needs to know the team membership. Look at the {@link MetaClient}
	 * object of this class and set its field before you call this method
	 * 
	 * @param world
	 */
	public void setHitbox(World world) {
		if (meta_client.team != null) {
			physical_hitbox = new PhysicalHitbox(world, this, meta_client.team);

			num_contacts_landing = physical_hitbox.foot_contacts.getNumContacts();

			landing = new Landing(this);
			jumping = new Jumping(this, landing);

			// these two fixtures need to have the jumping object
			physical_hitbox.underside_contacts.setPhysicalGameclient(this);
			physical_hitbox.mid_top_contacts.setJumping(jumping);
		}
	}

	/**
	 * Forced termination of the {@link #current_skill}
	 */
	public void cancelCurrentSkill() {
		if (current_skill != null)
			current_skill.cancel();
		current_skill = null;
	}

	public PhysicalHitbox getHitbox() {
		return physical_hitbox;
	}

	/**
	 * Default = 1f. Use this multiplier for velocity while walking and jumping
	 * 
	 * @return
	 */
	public float getVelXFactor() {
		return vel_x_factor;
	}

	public void setVelXFactor(float factor) {
		vel_x_factor = factor;
	}

	/**
	 * Cannot be called in {@link #update()} because multiple calls would override
	 * those fields
	 */
	public void resetOutsidePhysics() {

		if (hasNewAnimationStarted(AnimationName.WALK)) {
			startMovementEmission(walk_emission_def);
		}
		if (hasNewAnimationStarted(AnimationName.SLIDE_CEILING_LEFT)
				|| hasNewAnimationStarted(AnimationName.SLIDE_CEILING_RIGHT)
				|| hasNewAnimationStarted(AnimationName.SLIDE_GROUND_LEFT)
				|| hasNewAnimationStarted(AnimationName.SLIDE_GROUND_RIGHT))
			startMovementEmission(slide_emission_def);

		if (hasNewAnimationStarted(AnimationName.LAND)) {
			startMovementEmission(land_emission_def);
		}

		if (hasNewAnimationStarted(AnimationName.JUMP_SECOND)) {
			startMovementEmission(doublejump_emission_def);
		}
		if (hasNewAnimationStarted(AnimationName.JUMP_SLIDE) || hasNewAnimationStarted(AnimationName.JUMP_FIRST)) {
			if (jumping.is_slide_jump == true)
				startMovementEmission(slide_jump_emission_def);

		}

		if (hasNewAnimationStarted(AnimationName.IDLE) || hasNewAnimationStarted(AnimationName.FALL)
				|| hasNewAnimationStarted(AnimationName.HOLD_CEILING_LEFT)
				|| hasNewAnimationStarted(AnimationName.HOLD_CEILING_RIGHT)
				|| hasNewAnimationStarted(AnimationName.HOLD_GROUND_LEFT)
				|| hasNewAnimationStarted(AnimationName.HOLD_GROUND_RIGHT))
			startMovementEmission(null);

		if (info_animations.track_0 != null)
			last_track_0 = info_animations.track_0;

		// reset all animation fields, so no animation is permanent active.
		info_animations.clear();

		triggers.clear();
	}

	private boolean hasNewAnimationStarted(AnimationName newAnimation) {
		return last_track_0 != newAnimation && info_animations.track_0 == newAnimation;
	}

	/**
	 * Returns <code>true</code> if the player has finished the current skill
	 * animation. Otherwise <code>false</code> (and when current skill is equal to
	 * <code>null</code>)
	 * 
	 * @return
	 */
	private boolean hasFinishedCurrentSkill() {
		// no current skill => return false.
		if (current_skill == null)
			return false;

		return current_skill.isAnimationComplete() == true;
	}

	public Jumping getJumping() {
		return jumping;
	}

	/**
	 * Called when underside contacts the ground.
	 * 
	 * @param brakeEnabled
	 */
	public void setBrakeEnabled(boolean brakeEnabled) {
		this.brake_enabled = brakeEnabled;
	}

	public boolean isBrakeEnabled() {
		return brake_enabled;
	}

	@Override
	public void setPosition(float x, float y) {
		pos.set(x, y);
		physical_hitbox.setPosition(x, y);
	}

	public void setCurrentSkill(ASkill skill) {
		this.current_skill = skill;
	}

	/**
	 * Might be <code>null</code>
	 * 
	 * @return
	 */
	public ASkill getCurrentSkill() {
		return current_skill;
	}

	public int getPlatformAngle() {
		return platform_angle;
	}

	/**
	 * Returns a sparse representation of {@link PhysicalClient} to transmit its
	 * information from server to clients (and vice versa) frequently
	 * 
	 * @return {@link SparseClient}
	 */
	public SparseClient getSparseClient() {

		player_infos = new ArrayList<>();

		float cooldownSkill0 = skill_0 != null ? skill_0.getCooldownCurrent() : 0,
				cooldownSkill1 = skill_1 != null ? skill_1.getCooldownCurrent() : 0,
				cooldownSkill2 = skill_2 != null ? skill_2.getCooldownCurrent() : 0;

		my_sparse_client.set(meta_client.connection_id, pos, vel, cursor_pos, health_current, mana_current,
				sparse_projectiles, redirected_inputs, sparse_combo_storage1, sparse_combo_storage2, player_infos,
				cooldownSkill0, cooldownSkill1, cooldownSkill2);
		return my_sparse_client;
	}

	public void setAlive() {

		health_current = meta_client.health_absolute;
		mana_current = meta_client.mana_absolute;
		physical_hitbox.getBody().setActive(true);
	}

	/**
	 * Returns <code>true</code> if the player's been dead for 3 seconds, otherwise
	 * <code>false</code>
	 * 
	 * @return
	 */
	public boolean isRecentlyDead() {
		return death_time_absolute - 3 < death_time_current;
	}

	public ViewCircle getViewCircle() {
		return view_circle;
	}

	/**
	 * 
	 * @param visible
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * Returns all current active timed effects
	 * 
	 * @return
	 */
	public ArrayList<AEffectTimed> getEffectsTimed() {
		return effects_timed;
	}

	/**
	 * Returns all effects of which we want to display the damage or healing number
	 * 
	 * @return
	 */
	public ArrayList<AEffectWithNumber> getEffectsWithNumber() {
		return effects_with_number;
	}

	@Override
	public LinkedList<Trigger> getTriggers() {
		return triggers;
	}

	@Override
	public World getWorld() {
		return physical_hitbox.getWorld();
	}

	@Override
	public Location getLocation() {
		return location;
	}

	@Override
	public Vector2 getPos() {
		return pos;
	}

	@Override
	public Vector2 getCursorPos() {
		return cursor_pos;
	}

	/**
	 * Returns the current interpolated time depending on {@link #location}. In
	 * {@link WorldOnlineRoot} it's the gameserver time, in
	 * {@link WorldTrainingRoot} it's the local time. Useful to update cooldowns
	 * without sending the cooldown of each skill.
	 * 
	 * @return
	 */
	public float getTime() {
		return time;
	}

	/**
	 * Returns the id of the last processed {@link InputRequest}
	 * 
	 * @return
	 */
	public int getLastProcessedInputID() {
		return last_processed_input_id;
	}

	@Override
	public LinkedList<SparseProjectile> getSparseProjectiles() {
		return sparse_projectiles;
	}

	@Override
	public EmissionList getEmissionList() {
		return emission_list;
	}

	@Override
	public Team getTeam() {
		return meta_client.team;
	}

	@Override
	public ComboStorage getComboStorageTransfer() {
		return combo_storage_transfer;
	}

	@Override
	public ComboStorage getComboStorage1() {
		return combo_storage1;
	}

	@Override
	public ComboStorage getComboStorage2() {
		return combo_storage2;
	}

	@Override
	public Body getBody() {
		return physical_hitbox.getBody();
	}

	/**
	 * If <code>true</code> is passed the movement is going to be disabled.
	 * 
	 * @param rooted
	 */
	public void setRooted(boolean rooted) {
		this.rooted = rooted;
	}

	public boolean isRooted() {
		return rooted;
	}

	public void setUnderImpulse(boolean underImpulse) {
		this.under_impulse = underImpulse;
	}

	public boolean isUnderImpulse() {
		return under_impulse;
	}

	/**
	 * disarmed means the character is not enable to cast basic skills.
	 * 
	 * @param disarmed
	 */
	public void setDisarmed(boolean disarmed) {
		this.disarmed = disarmed;
	}

	/**
	 * silenced means the character is not enable to cast main skills.
	 * 
	 * @param silenced
	 */
	public void setSilenced(boolean silenced) {
		this.silenced = silenced;
	}

	/**
	 * Returns the recent error message which describes an error inside the world
	 * 
	 * @return
	 */
	public ErrorMessage getErrorMessage() {
		return error_message;
	}

	public int getKillCount() {
		return kill_count;
	}

	public int getDeathCount() {
		return death_count;
	}

	public int getAssistCount() {
		return assist_count;
	}

	public DiscardingLimitedArrayList<InputRequest> getInputQueue() {
		return input_queue;
	}

	@Override
	public IEmitter getSlayer() {
		return slayer;
	}

	public boolean getPlayerCanSecondjump() {
		return player_can_secondjump;
	}

	public float getElapsedTime() {
		return elapsed_time;
	}

	public float getElapsedTimeSliding() {
		return elapsed_time_sliding;
	}

	public boolean isDisarmed() {
		return disarmed;
	}

	public boolean isSilenced() {
		return silenced;
	}

	public boolean isHolding() {
		return is_holding;
	}

	public boolean isPrevAlive() {
		return prev_alive;
	}

	/**
	 * <code>false</code> by default. On client side there's a
	 * {@link PhysicalClient} client in a parallel world to verify the client side
	 * prediction. For that client we don't want to initiate skills.
	 * 
	 * @return
	 */
	public boolean isPredictionVerifier() {
		return prediction_verifier;
	}

	/**
	 * Returns whether this player is inside of enemies view circles
	 * 
	 * @return
	 */
	public boolean isVisible() {
		return visible;
	}

	public boolean isStunned() {
		return stunned;
	}

	@Override
	public void increaseCrystals() {

		/*
		 * TODO note that this not gonna happen in a network simulation, because
		 * increaseCrystals is just called in physics and physics is not be used in
		 * network
		 */
		if (location != Location.SERVERSIDE) {
			sound_crystal_collected.updateVolume(pos.x, pos.y);
			AudioData.playEffect(sound_crystal_collected.getSoundObj());
		}
		crystal_count++;
	}

	public int getCrystalCount() {
		return crystal_count;
	}

	/**
	 * Sets the current error message which describes an error inside the world
	 * 
	 * @param errorMessage
	 */
	public void setErrorMessage(ErrorMessage errorMessage) {
		this.error_message = errorMessage;
	}

	public void setIDComboAlly(short idComboAlly) {
		id_combo_ally = idComboAlly;
	}

	public void setLeftDominant(boolean leftDominant) {
		left_dominant = leftDominant;
	}

	/**
	 * sets {@link #prediction_verifier} to <code>true</code>.
	 */
	public void setPredictionVerifier() {
		prediction_verifier = true;
	}

	@Override
	public void setSlayer(IEmitter slayer) {
		this.slayer = slayer;
	}

	public void setStunned(boolean stunned) {
		this.stunned = stunned;
	}

	/**
	 * Sets the current world time. Useful to update cooldowns without sending the
	 * cooldown of each skill.
	 * 
	 * @param time
	 */
	public void setTime(float time) {
		this.time = time;
	}

	public IEmitter getPotentialComboAlly() {
		return potential_combo_ally;
	}

}