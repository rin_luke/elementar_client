package com.elementar.logic.characters.abstracts;

import java.util.ArrayList;

import com.elementar.data.container.KeysConfiguration.WorldAction;
import com.elementar.data.container.util.Gameclass;
import com.elementar.logic.characters.DrawableClient;
import com.elementar.logic.characters.MetaClient;
import com.elementar.logic.characters.PhysicalClient;
import com.elementar.logic.characters.SparseClient;
import com.elementar.logic.characters.skills.AChargeSkill;
import com.elementar.logic.characters.skills.ASkill;
import com.elementar.logic.characters.skills.DyingSkill;
import com.elementar.logic.characters.skills.TransferSkill;
import com.elementar.logic.network.response.InfoAnimations;

public abstract class AClient extends SparseClient {

	/**
	 * represented as bit-field
	 */
	protected short current_actions, prev_actions;

	/**
	 * this field is necessary to handle the case when the
	 * {@link WorldAction#MOVE_LEFT} and {@link WorldAction#MOVE_RIGHT} is active.
	 * <code>true</code> if the last active action is {@link WorldAction#MOVE_LEFT},
	 * so move-left is dominant towards move-right. <code>false</code> if the last
	 * active action is {@link WorldAction#MOVE_RIGHT}, so move-right is dominant
	 * towards move-left.
	 */
	protected boolean left_dominant = false;

	/**
	 * {@link PhysicalClient} needs {@link #gameclass_name} for some physical values
	 * like velocity factor
	 * <p>
	 * And {@link DrawableClient} for visual information.
	 */
	protected final MetaClient meta_client;

	/**
	 * this field will be set to <code>true</code> when the match is over
	 */
	protected boolean match_finished = false;

	protected ArrayList<ASkill> skills;
	protected ASkill spawn_skill_after_death;
	protected DyingSkill dying_skill;

	protected TransferSkill transfer_skill_1;
	protected TransferSkill transfer_skill_2;

	protected AChargeSkill skill_0, skill_1, skill_2;

	public InfoAnimations info_animations = new InfoAnimations();

	/**
	 * 
	 * @param metaClient
	 */
	public AClient(MetaClient metaClient) {
		super(metaClient.connection_id);

		health_current = metaClient.health_absolute = 1;
		mana_current = metaClient.mana_absolute = 0;
		skills = new ArrayList<>();
		meta_client = metaClient;

		// all bits set to 0
		current_actions = 0;
		prev_actions = 0;
	}

	/**
	 * update actions that were triggered by (un)pressed keys
	 */
	public void update() {
		updateActions();
	}

	private void updateActions() {

		for (WorldAction action : WorldAction.values()) {
			boolean prevActive = (action.getValue() & prev_actions) != 0;
			boolean currentActive = (action.getValue() & current_actions) != 0;

			if (match_finished == false) {
				if (currentActive == true)
					continuousAction(action);

				if (prevActive != currentActive)
					onetimeAction(prevActive, currentActive, action);

			}
			// override prev pressed
			if (currentActive == true)
				// set bit to 1
				prev_actions |= 1 << action.getIndex();
			else
				// set bit to 0
				prev_actions &= ~(1 << action.getIndex());

		}
	}

	private void onetimeAction(boolean prevActive, boolean currentActive, WorldAction action) {
		// it's guaranteed that prev != current
		if (prevActive == true)
			onetimeActionInactive(action);
		if (currentActive == true)
			onetimeActionActive(action);
	}

	public void setFirerate(float firerate) {
		skill_0.setFirerate(firerate);
		skill_1.setFirerate(firerate);
		skill_2.setFirerate(firerate);
	}

	public void setMatchFinished() {
		match_finished = true;
		// setting actions to inactive
		current_actions = 0;
		prev_actions = 0;
	}

	/**
	 * for continuous active actions
	 * 
	 * @param action
	 */
	protected abstract void continuousAction(WorldAction action);

	protected abstract void setPosition(float x, float y);

	/**
	 * Setting {@link #left_dominant} for child classes {@link DrawableClient} and
	 * {@link PhysicalClient}
	 * 
	 * @param action
	 */
	protected void onetimeActionActive(WorldAction action) {
		switch (action) {
		case MOVE_LEFT:
			left_dominant = true;
			break;
		case MOVE_RIGHT:
			left_dominant = false;
			break;
		}
	}

	/**
	 * Setting {@link #left_dominant} for child classes {@link DrawableClient} and
	 * {@link PhysicalClient}
	 * 
	 * @param action
	 */
	protected void onetimeActionInactive(WorldAction action) {
		switch (action) {
		case MOVE_LEFT:
			left_dominant = false;
			break;
		case MOVE_RIGHT:
			left_dominant = true;
			break;
		}
	}

	/**
	 * 
	 * @param action
	 * @return
	 */
	public boolean isCurrentActionActive(WorldAction action) {
		return (action.getValue() & current_actions) != 0;
	}

	/**
	 * 
	 * @param action
	 * @param active
	 */
	public void setCurrentAction(WorldAction action, boolean active) {

		if (action == null)
			return;

		if (active == true)
			// set bit to 1
			current_actions |= 1 << action.getIndex();
		else
			// set bit to 0
			current_actions &= ~(1 << action.getIndex());
	}

	/**
	 * 
	 * @param currentActions
	 */
	public void setCurrentActions(short currentActions) {
		this.current_actions = currentActions;
	}

	/**
	 * 
	 * @param prevActions
	 */
	public void setPrevActions(short prevActions) {
		this.prev_actions = prevActions;
	}

	/**
	 * Returns the bit-field of the previous actions
	 * 
	 * @return
	 */
	public short getPrevActions() {
		return prev_actions;
	}

	/**
	 * Returns the mapping of the current active or inactive actions
	 * 
	 * @return
	 */
	public short getCurrentActions() {
		return current_actions;
	}

	public boolean isLeftDominant() {
		return left_dominant;
	}

	public ArrayList<ASkill> getSkills() {
		return skills;
	}

	/**
	 * Shortcut method
	 * 
	 * @return
	 */
	public Gameclass getGameclass() {
		return meta_client.gameclass;
	}

	public MetaClient getMetaClient() {
		return meta_client;
	}

	public boolean isAlive() {
		return health_current > 0;
	}

	public ASkill getSkill0() {
		return skill_0;
	}

	public ASkill getSkill1() {
		return skill_1;
	}

	public ASkill getSkill2() {
		return skill_2;
	}

}
