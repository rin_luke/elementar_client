package com.elementar.logic.characters.abstracts;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public abstract class APhysicalElement {

	protected World world;
	protected BodyDef body_def = new BodyDef();
	protected FixtureDef fixture_def = new FixtureDef();
	protected Body body;

	public APhysicalElement(World world) {
		this.world = world;
	}

	protected abstract void initPhysics(float x, float y);

	protected abstract Filter getCollisionFilter();

	public Body getBody() {
		return body;
	}

	public void destroy() {
		if (body != null) {
			world.destroyBody(body);
			body = null;
		}
	}

	public Vector2 getVelocity() {
		return body.getLinearVelocity();
	}

	public Vector2 getPosition() {
		return body.getPosition();
	}

	public void setPosition(float x, float y) {
		if (body != null)
			body.setTransform(x, y, 0);
	}

	public World getWorld() {
		return world;
	}
}
