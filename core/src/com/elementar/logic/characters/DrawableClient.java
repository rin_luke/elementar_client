package com.elementar.logic.characters;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.elementar.data.container.AnimationContainer;
import com.elementar.data.container.ParticleContainer;
import com.elementar.data.container.KeysConfiguration.WorldAction;
import com.elementar.data.container.util.CustomParticleEffect;
import com.elementar.data.container.util.Gameclass;
import com.elementar.data.container.util.GameclassParticleEffect;
import com.elementar.data.container.util.GameclassSkin;
import com.elementar.gui.abstracts.AGameclassSelectionModule;
import com.elementar.gui.abstracts.ARootWorldModule;
import com.elementar.gui.util.CustomSkeleton;
import com.elementar.gui.util.DamageDiffbar;
import com.elementar.gui.util.HasDamageDifference;
import com.elementar.gui.util.SkillThumbnailMovement;
import com.elementar.gui.widgets.CustomSpineWidget;
import com.elementar.gui.widgets.SkillButton.ComboFeedbackAnimation;
import com.elementar.logic.characters.PhysicalClient.AnimationName;
import com.elementar.logic.characters.abstracts.AClient;
import com.elementar.logic.characters.skills.AChargeSkill;
import com.elementar.logic.characters.skills.ASkill;
import com.elementar.logic.characters.skills.SparseComboStorage;
import com.elementar.logic.emission.def.IBonesModel;
import com.elementar.logic.util.Shared;
import com.elementar.logic.util.Util;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationState.TrackEntry;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.Bone;
import com.esotericsoftware.spine.SkeletonRenderer;

public class DrawableClient extends AClient implements IBonesModel, HasDamageDifference {

	private static final int TRACK_0 = 0, TRACK_1 = 1, TRACK_2 = 2, TRACK_3 = 3, TRACK_4 = 4;

	private static final float MIX_TIME = 0.25f;

	private MyAnimation IDLE_ANIMATION, WALK_WITH_LEGS_ANIMATION, WALK_WITHOUT_LEGS_ANIMATION, WALK_BACK_ANIMATION,
			JUMP_FIRST_ANIMATION, JUMP_SECOND_ANIMATION, FALL_ANIMATION, LAND_ANIMATION, LAND_STUNNED_ANIMATION,
			RECEIVE_DAMAGE_ANIMATION, DEATH_EXPLOSION_ANIMATION, HOLD_GROUND_LEFT_ANIMATION,
			HOLD_CEILING_LEFT_ANIMATION, HOLD_GROUND_RIGHT_ANIMATION, HOLD_CEILING_RIGHT_ANIMATION,
			SLIDE_GROUND_LEFT_ANIMATION, SLIDE_CEILING_LEFT_ANIMATION, SLIDE_GROUND_RIGHT_ANIMATION,
			SLIDE_CEILING_RIGHT_ANIMATION, JUMP_FIRST_SLIDE_ANIMATION, ALPHA_FADE_IN_ANIMATION,
			ALPHA_FADE_OUT_ANIMATION, STUNNED_ANIMATION;

	/**
	 * skill animations.
	 */
	private MyAnimation EFFECT_BLACKHOLE_ANIMATION, EFFECT_LIGHTPORT_ANIMATION, EFFECT_COLOR_ICE_ANIMATION,
			EFFECT_COLOR_TOXIC_ANIMATION, EFFECT_COLOR_DEFAULT_ANIMATION;

	/**
	 * charge P1 shows pulling the arm back and forward
	 * <p>
	 * channel P1 shows pushing the arm forward
	 */
	private ArrayList<MyAnimation> charge_p1_animations, channel_p1_animations;

	/**
	 * P3 shows the character in a neutral pose. It kicks in after P1 (charge) or P1
	 * (channel) are done. The interpolation between those P's and P3 will show how
	 * the player pull its front arm back (that's the typical case if no further
	 * skill is casted). Otherwise, when a skill is queued and waits to its release,
	 * the interpolation is between a P (as specified above) and the next P1 (of the
	 * queued skill)
	 */
	private MyAnimation P3_ANIMATION;

	private GameclassSkin gameclass_skin;

	private CustomSkeleton skeleton;

	/**
	 * Map with slotname as Key and particle effect as value.
	 */
	protected HashMap<String, GameclassParticleEffect> particle_effects_filtered = new HashMap<>();

	private CustomParticleEffect head_particle_effect, hand_front_particle_effect, hand_back_particle_effect,
			torso_particle_effect, shoulder_front_particle_effect, shoulder_back_particle_effect,
			hand_front_combo_particle_effect, hand_back_combo_particle_effect;

	private Bone head_bone_flip, hand_front_bone_flip, hand_back_bone_flip, torso_bone_flip, shoulder_front_bone_flip,
			shoulder_back_bone_flip, leg_front_bone_flip, leg_back_bone_flip;

	private Bone head_bone_rotation, hand_front_bone_rotation, hand_back_bone_rotation, torso_bone_rotation,
			shoulder_front1_bone_rotation, shoulder_front2_bone_rotation, shoulder_back1_bone_rotation,
			shoulder_back2_bone_rotation, leg_front1_bone_rotation, leg_back1_bone_rotation, leg_front2_bone_rotation,
			leg_back2_bone_rotation;

	private MyAnimationState animation_state;

	private MyAnimation current_track0_animation, current_track1_animation, current_track2_animation,
			current_track3_animation, current_track4_animation;

	/**
	 * Animations hold a number_frames_till_change field. When an animation reaches
	 * that number, a change occurs. The frame_counter (which is necessary for
	 * comparison between frame_counter and number_frames_till_change) has to be
	 * reseted if a continuous chain of {@link #setCurrentAnimation(AnimationName)}
	 * is ripped.
	 * <p>
	 * Example: I walk and sometimes I get a signal for sliding, so the sliding
	 * animation plays. It could be that this signal shouldnt be recognize, so I set
	 * the number of frames until a change to 2. When a chain of
	 * {@link #setCurrentAnimation(AnimationName)} is n (here 2)-times called with
	 * the same animation as parameter, the animation is allowed to change. If the
	 * chain got ripped by an other animation ALL frame_counter should be reset to
	 * 0. That's the reason why I need this animation_list.
	 */
	private ArrayList<MyAnimation> animation_list = new ArrayList<MyAnimation>();

	/**
	 * Default value is <code>false</code>. If <code>true</code>, the animations use
	 * this field to force all animations in cursor following mode. Set
	 * {@link #cursor_dominant} to <code>true</code> if the animation should ignore
	 * A, D pressing.
	 */
	private boolean cursor_dominant = false;

	private short rotation_angle;

	/**
	 * Holds all rotation bones to simulate death explosion animation
	 */
	private ArrayList<Bone> bones_for_death;

	private Vector2 camera_shift_by_view_direction = new Vector2();

	/**
	 * This flag is set when a change is recognized in
	 * {@link SparseComboStorage#is_storing}. If <code>true</code> we know that the
	 * player has absorbed a skill recently.
	 */
	private boolean absorption_change1 = false, absorption_change2 = false;

	private short last_health;

	private DamageDiffbar damage_diff_bar = new DamageDiffbar();

	private SkillThumbnailMovement skill_thumbnail_movement1 = new SkillThumbnailMovement();
	private SkillThumbnailMovement skill_thumbnail_movement2 = new SkillThumbnailMovement();
	private AlphaTransition skill_thumbnail_alpha_transition1 = new AlphaTransition();
	private AlphaTransition skill_thumbnail_alpha_transition2 = new AlphaTransition();

	private AnimationStateData animation_state_data;

	private boolean has_legs;

	private CustomSpineWidget spine_combo_feedback;

	/**
	 * Constructor for {@link AGameclassSelectionModule}
	 * 
	 * @param gameclass
	 * @param skinIndex
	 * @param skeleton
	 */
	public DrawableClient(MetaClient metaClient, CustomSkeleton skeleton, boolean flip) {
		super(metaClient);

		this.charge_p1_animations = new ArrayList<MyAnimation>();
		this.channel_p1_animations = new ArrayList<MyAnimation>();

		this.skeleton = skeleton;
		skeleton.setX(pos.x);
		skeleton.setY(pos.y);
		skeleton.setFlipX(flip);
		skeleton.updateWorldTransform();

		animation_state_data = new AnimationStateData(skeleton.getData());
		// global mixtimes

		animation_state_data.setDefaultMix(MIX_TIME / 2f);
		// data.setMix(LAND_ANIMATION.animation, WALK_ANIMATION.animation,
		// 0.5f);
		// data.setMix(LAND_ANIMATION.animation, IDLE_ANIMATION.animation,
		// 0.5f);

		initSkillAnimations();
		initAnimations();

		// set mix time
		animation_state_data.setMix(JUMP_FIRST_SLIDE_ANIMATION.animation, SLIDE_CEILING_LEFT_ANIMATION.animation, 0.1f);
		animation_state_data.setMix(JUMP_FIRST_SLIDE_ANIMATION.animation, SLIDE_CEILING_RIGHT_ANIMATION.animation,
				0.1f);

		animation_state = new MyAnimationState(animation_state_data);

		head_bone_flip = skeleton.findBone("character_head_flip");
		head_bone_rotation = skeleton.findBone("character_head_rotation");

		hand_front_bone_flip = skeleton.findBone("character_hand_front_flip");
		hand_front_bone_rotation = skeleton.findBone("character_hand_front_rotation");

		hand_back_bone_flip = skeleton.findBone("character_hand_back_flip");
		hand_back_bone_rotation = skeleton.findBone("character_hand_back_rotation");

		torso_bone_flip = skeleton.findBone("character_torso_flip");
		torso_bone_rotation = skeleton.findBone("character_torso_rotation");

		shoulder_front_bone_flip = skeleton.findBone("character_shoulder_front_flip");
		shoulder_front1_bone_rotation = skeleton.findBone("character_shoulder_front1_rotation");
		shoulder_front2_bone_rotation = skeleton.findBone("character_shoulder_front2_rotation");

		shoulder_back_bone_flip = skeleton.findBone("character_shoulder_back_flip");
		shoulder_back1_bone_rotation = skeleton.findBone("character_shoulder_back1_rotation");
		shoulder_back2_bone_rotation = skeleton.findBone("character_shoulder_back2_rotation");

		leg_front_bone_flip = skeleton.findBone("character_leg_front_flip");
		leg_front1_bone_rotation = skeleton.findBone("character_leg_front1_rotation");
		leg_front2_bone_rotation = skeleton.findBone("character_leg_front2_rotation");

		leg_back_bone_flip = skeleton.findBone("character_leg_back_flip");
		leg_back1_bone_rotation = skeleton.findBone("character_leg_back1_rotation");
		leg_back2_bone_rotation = skeleton.findBone("character_leg_back2_rotation");

		// prepare for death case
		fillDeathBonesArray();

		// init currentStanceAnimation
		current_track0_animation = IDLE_ANIMATION;
		animation_state.setAnimation(current_track0_animation);
		current_track0_animation.begin();

		setCurrentAnimation(AnimationName.P3);

		// has an effect if getGameclass() isn't null
		updateByMetaData(flip);

		CustomSkeleton skeletonComboFeedback = AnimationContainer.makeAnimation(AnimationContainer.atlas_characters,
				Gdx.files.internal("graphic/animation_combo_feedback_char.json"), Shared.SCALE_CHARACTER_INGAME);
		skeletonComboFeedback.setSkin("combo_feedback_char");
		spine_combo_feedback = new CustomSpineWidget(skeletonComboFeedback, "", false);
		// empty
		spine_combo_feedback.setAnimation(ComboFeedbackAnimation.POPOUT.getName());
		spine_combo_feedback.setAnimationTime(Float.MAX_VALUE);

	}

	/**
	 * If some information like gameclass and/or skinIndex isn't known yet, call
	 * {@link #updateByMetaData()} as soon as possible.
	 * <p>
	 * Constructor for world
	 * 
	 * @param metaClient
	 */
	public DrawableClient(MetaClient metaClient) {
		this(metaClient, AnimationContainer.makeCharacterAnimationForWorld(), false);
	}

	private final float MIX_TIME_SKILLS = 0.3f;

	/**
	 * Loads skill animation by skills of the specific character
	 */
	private void initSkillAnimations() {

		P3_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_p3"), TRACK_1, false, 0);

		// init charge and channel animations
		int numberChargeVersions = 8;
		MyAnimation chargeP1Animation;
		// fill charge animation list
		for (int i = 0; i < numberChargeVersions; i++) {
			chargeP1Animation = new MyAnimation(skeleton.getData().findAnimation("character_charge_p1_" + i), TRACK_1,
					false, 3);
			charge_p1_animations.add(chargeP1Animation);
			// define mix times between animations
			animation_state_data.setMix(chargeP1Animation.animation, P3_ANIMATION.animation, 0.4f);
			animation_state_data.setMix(P3_ANIMATION.animation, chargeP1Animation.animation, 0.2f);
		}

		int numberChannelVersions = 1;
		MyAnimation channelP1Animation;
		// fill channel animation list
		for (int i = 0; i < numberChannelVersions; i++) {
			channelP1Animation = new MyAnimation(skeleton.getData().findAnimation("character_channel_p1_" + i), TRACK_1,
					false, 3);
			channel_p1_animations.add(channelP1Animation);

			// define mix times between animations
			animation_state_data.setMix(channelP1Animation.animation, P3_ANIMATION.animation,
					P3_ANIMATION.animation.getDuration());
			animation_state_data.setMix(P3_ANIMATION.animation, channelP1Animation.animation, MIX_TIME_SKILLS);
		}

		/*
		 * set the mix time for all end parts of skills (p1 charge and p1 channel) to
		 * the beginning parts (p1 charge and p1 channel)
		 */
		for (MyAnimation p1Charge : charge_p1_animations) {
			for (MyAnimation p1ChargeX : charge_p1_animations)
				animation_state_data.setMix(p1Charge.animation, p1ChargeX.animation, MIX_TIME_SKILLS);
			for (MyAnimation p1Channel : channel_p1_animations)
				animation_state_data.setMix(p1Charge.animation, p1Channel.animation, MIX_TIME_SKILLS);
		}

		for (MyAnimation p1Channel : channel_p1_animations) {
			for (MyAnimation p1Charge : charge_p1_animations)
				animation_state_data.setMix(p1Channel.animation, p1Charge.animation, MIX_TIME_SKILLS);
			for (MyAnimation p1ChannelX : channel_p1_animations)
				animation_state_data.setMix(p1Channel.animation, p1ChannelX.animation, MIX_TIME_SKILLS);
		}
	}

	private void initAnimations() {
		IDLE_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_idle"), TRACK_0, true, 0);
		WALK_WITH_LEGS_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_walk"), TRACK_0, true,
				3);
		WALK_WITHOUT_LEGS_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_walk_float"), TRACK_0,
				true, 3);

		WALK_BACK_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_walk_back"), TRACK_0, true,
				3);

		JUMP_FIRST_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_jump0"), TRACK_0, false, 3);
		JUMP_SECOND_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_jump1"), TRACK_0, false, 3);
		FALL_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_fall"), TRACK_0, true, 3);

		LAND_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_land"), TRACK_2, false, 3) {
			@Override
			public boolean runThrough(MyAnimation nextAnimation) {
				return true;
			}
		};

		LAND_STUNNED_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_land_stunned"), TRACK_0,
				false, 3) {
			@Override
			public boolean runThrough(MyAnimation nextAnimation) {
				return true;
			}
		};
		// {
		//
		// @Override
		// public boolean runThrough(MyAnimation nextAnimation) {
		// return true;
		// }
		// };
		STUNNED_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_stunned"), TRACK_0, true, 0);

		// parameter noch unsicher!
		RECEIVE_DAMAGE_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_receive_damage"),
				TRACK_2, false, 0) {
			@Override
			public boolean runThrough(MyAnimation nextAnimation) {
				return false;
			}
		};

		EFFECT_BLACKHOLE_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("effect_teleport"), TRACK_3,
				false, 0);

		EFFECT_LIGHTPORT_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("effect_teleport"), TRACK_3,
				false, 0);

		// slow effect
		EFFECT_COLOR_ICE_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("effect_color_ice"), TRACK_3,
				true, 0);

		// dot effect
		EFFECT_COLOR_TOXIC_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("effect_color_toxic"), TRACK_3,
				true, 0);

		EFFECT_COLOR_DEFAULT_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("effect_color_default"),
				TRACK_3, false, 0);

		// has transparency changes => Track 3
		DEATH_EXPLOSION_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_death_explosion"),
				TRACK_4, false, 0);

		ALPHA_FADE_IN_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("alpha_fade_in"), TRACK_4, false, 0);

		ALPHA_FADE_OUT_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("alpha_fade_out"), TRACK_4, false,
				0);

		// ground
		SLIDE_GROUND_LEFT_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_slide_ground"),
				TRACK_0, true, 3) {
			@Override
			public void flipSpecific(boolean flip) {
				flipFrontHandBone(flip);
				flipTorsoBone(flip);
				flipHeadBone(flip);

				flipShoulderFrontBone(flip);
				flipShoulderBackBone(flip);

				flipLegFrontBone(false);
				flipLegBackBone(false);

				flipBackHandBone(false);
			}
		};
		SLIDE_GROUND_RIGHT_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_slide_ground"),
				TRACK_0, true, 3) {
			@Override
			public void flipSpecific(boolean flip) {
				flipFrontHandBone(flip);
				flipTorsoBone(flip);
				flipHeadBone(flip);

				flipShoulderFrontBone(flip);
				flipShoulderBackBone(flip);

				flipLegFrontBone(true);
				flipLegBackBone(true);

				flipBackHandBone(true);
			}
		};

		// ceiling
		SLIDE_CEILING_LEFT_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_slide_ceiling"),
				TRACK_0, true, 3) {
			@Override
			public void flipSpecific(boolean flip) {
				flipFrontHandBone(flip);
				flipTorsoBone(flip);
				flipHeadBone(flip);

				flipShoulderFrontBone(flip);
				flipShoulderBackBone(flip);

				flipLegFrontBone(true);
				flipLegBackBone(true);

				flipBackHandBone(true);
			}
		};
		SLIDE_CEILING_RIGHT_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_slide_ceiling"),
				TRACK_0, true, 3) {
			@Override
			public void flipSpecific(boolean flip) {
				flipFrontHandBone(flip);
				flipTorsoBone(flip);
				flipHeadBone(flip);

				flipShoulderFrontBone(flip);
				flipShoulderBackBone(flip);

				flipLegFrontBone(false);
				flipLegBackBone(false);

				flipBackHandBone(false);
			}

		};

		// ground
		HOLD_GROUND_LEFT_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_hold_ground"), TRACK_0,
				true, 3) {
			@Override
			public void flipSpecific(boolean flip) {
				flipFrontHandBone(flip);
				flipTorsoBone(flip);
				flipHeadBone(flip);

				flipShoulderFrontBone(flip);
				flipShoulderBackBone(flip);

				flipLegFrontBone(false);
				flipLegBackBone(false);

				flipBackHandBone(false);
			}
		};
		HOLD_GROUND_RIGHT_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_hold_ground"),
				TRACK_0, true, 3) {
			@Override
			public void flipSpecific(boolean flip) {
				flipFrontHandBone(flip);
				flipTorsoBone(flip);
				flipHeadBone(flip);

				flipShoulderFrontBone(flip);
				flipShoulderBackBone(flip);

				flipLegFrontBone(true);
				flipLegBackBone(true);

				flipBackHandBone(true);
			}
		};

		// ceiling
		HOLD_CEILING_LEFT_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_hold_ceiling"),
				TRACK_0, true, 3) {
			@Override
			public void flipSpecific(boolean flip) {
				flipFrontHandBone(flip);
				flipTorsoBone(flip);
				flipHeadBone(flip);

				flipShoulderFrontBone(flip);
				flipShoulderBackBone(flip);

				flipLegFrontBone(true);
				flipLegBackBone(true);

				flipBackHandBone(true);
			}
		};
		HOLD_CEILING_RIGHT_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_hold_ceiling"),
				TRACK_0, true, 3) {
			@Override
			public void flipSpecific(boolean flip) {
				flipFrontHandBone(flip);
				flipTorsoBone(flip);
				flipHeadBone(flip);

				flipShoulderFrontBone(flip);
				flipShoulderBackBone(flip);

				flipLegFrontBone(false);
				flipLegBackBone(false);

				flipBackHandBone(false);
			}

		};

		JUMP_FIRST_SLIDE_ANIMATION = new MyAnimation(skeleton.getData().findAnimation("character_slide_jump"), TRACK_0,
				false, 3);

	}

	@Override
	protected void setPosition(float x, float y) {
		pos.set(x, y);
		skeleton.setPosition(x, y);
	}

	/**
	 * 
	 * @param delta
	 * @param isVisible
	 */
	public void animationRoutine(float delta, boolean isVisible) {

		animation_state.update(delta);

		animation_state.apply(skeleton);

		TrackEntry track = animation_state.getCurrent(TRACK_1);
		if (track != null) {
			if (track.isComplete() == true && track.getAnimation() != P3_ANIMATION.animation) {
				// stretch endtime as long there is no other signal
				current_track1_animation.duration_extension += track.getTimeScale() * delta;
				track.setAnimationEnd(track.getAnimationEnd() + current_track1_animation.duration_extension);
			}

			if (track.isComplete() == true && track.getAnimation() == P3_ANIMATION.animation) {
				setCursorDominant(false);
				// current_track_1_animation = null;
				// animation_state.clearTrack(TRACK_1);

			}
		}

		track = animation_state.getCurrent(TRACK_2);
		if (track != null) {
			if (track.isComplete() == true) {
				current_track2_animation = null;
				animation_state.clearTrack(TRACK_2);
			}
		}

		track = animation_state.getCurrent(TRACK_3);
		if (track != null) {
			if (track.isComplete() == true) {
				current_track3_animation = null;
				animation_state.clearTrack(TRACK_3);
			}
		}

		track = animation_state.getCurrent(TRACK_4);
		if (track != null) {
			if (track.isComplete() == true) {
				current_track4_animation = null;
				animation_state.clearTrack(TRACK_4);
			}
		}

		/*
		 * Now handle Rotation
		 */

		if (isAlive() == true) {
			// rotate bones
			if (current_track0_animation != STUNNED_ANIMATION && current_track0_animation != LAND_STUNNED_ANIMATION) {
				head_bone_rotation.setRotation(rotation_angle);
				// front hand same angle as head (see hit animation)
				torso_bone_rotation.setRotation(rotation_angle * 0.15f);

				String track0Animation = current_track0_animation.animation.getName();
				String track1Animation = current_track1_animation.animation.getName();

				if (track0Animation.equals("character_jump0") == false
						&& track0Animation.equals("character_jump1") == false) {
					if (track0Animation.equals("character_slide_ground") == false
							&& track0Animation.equals("character_slide_ceiling") == false
							&& track0Animation.equals("character_hold_ground") == false
							&& track0Animation.equals("character_hold_ceiling") == false) {
						hand_back_bone_rotation.setRotation(rotation_angle * 0.4f);
					}
					hand_front_bone_rotation.setRotation(rotation_angle);
				}
			}
		}

		// rotate particle effects depending on bone rotation

		if (sparse_combo_storage1.isStoring() == true && sparse_combo_storage1.meta_skill_id != -1) {

			hand_front_combo_particle_effect = Gameclass.ALL_META_SKILLS.get((int) sparse_combo_storage1.meta_skill_id)
					.getCombohandFrontEffect();
			hand_front_combo_particle_effect.setRotation(hand_front_bone_rotation.getRotation());
		}

		if (sparse_combo_storage2.isStoring() == true && sparse_combo_storage2.meta_skill_id != -1) {

			hand_back_combo_particle_effect = Gameclass.ALL_META_SKILLS.get((int) sparse_combo_storage2.meta_skill_id)
					.getCombohandBackEffect();
			hand_back_combo_particle_effect.setRotation(hand_back_bone_rotation.getRotation());

		}

		if (hand_front_particle_effect != null)
			hand_front_particle_effect.setRotation(hand_front_bone_rotation.getRotation());

		if (hand_back_particle_effect != null)
			hand_back_particle_effect.setRotation(hand_back_bone_rotation.getRotation());

		if (head_particle_effect != null)
			head_particle_effect.setRotation(head_bone_rotation.getRotation());

		if (torso_particle_effect != null)
			torso_particle_effect.setRotation(torso_bone_rotation.getRotation());

		if (shoulder_front_particle_effect != null)
			shoulder_front_particle_effect.setRotation(torso_bone_rotation.getRotation());

		if (shoulder_back_particle_effect != null)
			shoulder_back_particle_effect.setRotation(torso_bone_rotation.getRotation());

		skeleton.updateWorldTransform();

	}

	/**
	 * Used to flip the local drawable client by cursor angle
	 * <p>
	 * First step: determine angle that might have intrinsic information about how
	 * to flip the drawable client (that's the case when rotationAngle is outside
	 * the range of 90 to -90).
	 * <p>
	 * Second step: After flipping within {@link #flipByCursorByAngle(short)}, the
	 * {@link SparseClient#cursor_pos} field of drawableClient will be set with the
	 * intrinsic-free angle.
	 * <p>
	 * Note: The {@link #rotation_angle} is still saved, it can be send via network,
	 * so other clients have also the flipping information.
	 * <p>
	 * This method returns also the camera shift vector modified by view direction.
	 * Depending on where the cursor is the camera shifts a little bit. The
	 * returning vector is only needed for {@link ARootWorldModule}.
	 */
	public Vector2 flipByCursor() {

		if (match_finished == false) {

			rotation_angle = Util.getAngleBetweenTwoPoints(pos, cursor_pos);

			flipByCursorByAngle();

			// 20 seems to be a good smooth factor
			camera_shift_by_view_direction.set(Util.subVectors(cursor_pos, pos)).setLength(cursor_pos.dst(pos) / 20);
		} else
			camera_shift_by_view_direction.setLength(0);

		return camera_shift_by_view_direction;

	}

	private void flipByCursorByAngle() {

		if (rotation_angle <= 90 && rotation_angle > 60)
			rotation_angle = 60;
		else if (rotation_angle < 120 && rotation_angle > 90)
			rotation_angle = 120;
		else if (rotation_angle < -30 && rotation_angle >= -90)
			rotation_angle = -30;
		else if (rotation_angle < -90 && rotation_angle > -150)
			rotation_angle = -150;

		if (rotation_angle < 90 && rotation_angle > -90) {
			if (current_track1_animation != null)
				current_track1_animation.flipSkeletonCursor(false);
			current_track0_animation.flipSkeletonCursor(false);
			// skill_preview_sprite.setFlipX(false);
		} else {
			if (current_track1_animation != null)
				current_track1_animation.flipSkeletonCursor(true);
			current_track0_animation.flipSkeletonCursor(true);
			// skill_preview_sprite.setFlipX(true);
			rotation_angle = Util.switchAngleUnitCircle(rotation_angle);

		}

	}

	/**
	 * Returns an appropriate {@link GameclassParticleEffect} of
	 * {@link #particle_effects_filtered}. The map
	 * {@link #particle_effects_filtered} is prepared and holds all particle effects
	 * of the chosen game class and appropriate skin.
	 * 
	 * This method is used to find the particle effect in
	 * {@link SkeletonRenderer#draw(com.elementar.gui.util.ShaderPolyBatch, IBonesModel)}
	 * 
	 * @param slot
	 * @return
	 */
	@Override
	public CustomParticleEffect getParticleEffectBySlot(String slot) {
		if (isAlive() == true)
			return particle_effects_filtered.get(slot);
		return null;
	}

	/**
	 * Updates skeleton's skin, particle effects and skill information by meta data
	 * (gameclass and skinIndex)
	 * 
	 */
	public void updateByMetaData(boolean flip) {
		if (getGameclass() != null) {

			setSkin(getGameclass().getName(), meta_client.skin_index, flip);

			// load skills
			skills = getGameclass().getSkills(this, null, skeleton.getSkin().getName());

			skill_0 = (AChargeSkill) skills.get(2);
			skill_1 = (AChargeSkill) skills.get(3);
			skill_2 = (AChargeSkill) skills.get(4);

			has_legs = skeleton.findSlot("leg_front2").getAttachment() != null;
		}
	}

	public void setSkin(String name, int skinIndex, boolean flip) {

		skeleton.setSkin("character_" + name + "_skin" + skinIndex);
		skeleton.setSlotsToSetupPose();
		particle_effects_filtered = ParticleContainer.getParticleEffectsByGameclassAndSkin(getGameclass(),
				skeleton.getSkin().getName());

		for (GameclassParticleEffect effect : particle_effects_filtered.values())
			effect.setFlipX(flip);

		/*
		 * for flipping more particular I need for each bone a particle effect
		 */

		hand_back_particle_effect = getParticleEffectBySlot("character_hand_back");
		hand_front_particle_effect = getParticleEffectBySlot("character_hand_front");
		head_particle_effect = getParticleEffectBySlot("character_head");
		torso_particle_effect = getParticleEffectBySlot("character_torso");
		shoulder_back_particle_effect = getParticleEffectBySlot("character_shoulder_back");
		shoulder_front_particle_effect = getParticleEffectBySlot("character_shoulder_front");

	}

	/**
	 * Note for me: This method doesn't take care whether the member animation
	 * fields are null or not.
	 * 
	 * @param animationName
	 */
	public void setCurrentAnimation(AnimationName animationName) {

		MyAnimation newAnimation = null;
		if (animationName == null)
			return;

		if (current_track4_animation == DEATH_EXPLOSION_ANIMATION)
			return;

		switch (animationName) {
		case FALL:
			newAnimation = FALL_ANIMATION;
			break;
		case IDLE:
			newAnimation = IDLE_ANIMATION;
			break;
		case JUMP_FIRST:
			newAnimation = JUMP_FIRST_ANIMATION;
			break;
		case JUMP_SLIDE:
			newAnimation = JUMP_FIRST_SLIDE_ANIMATION;
			break;
		case JUMP_SECOND:
			newAnimation = JUMP_SECOND_ANIMATION;
			break;
		case STUNNED:
			newAnimation = STUNNED_ANIMATION;
			break;
		case LAND:
			newAnimation = LAND_ANIMATION;
			break;
		case LAND_STUNNED:
			newAnimation = LAND_STUNNED_ANIMATION;
			break;
		case WALK:

			boolean moveLeft = isCurrentActionActive(WorldAction.MOVE_LEFT);
			boolean flip = head_bone_flip.getScaleX() > 0;

			newAnimation = has_legs == true ? WALK_WITH_LEGS_ANIMATION : WALK_WITHOUT_LEGS_ANIMATION;
			if ((moveLeft == false && flip == false) || (moveLeft == true && flip == true)) {
				newAnimation = WALK_BACK_ANIMATION;
			}

			if (animation_state.getCurrent(newAnimation.track_index) != null && (animation_state
					.getCurrent(newAnimation.track_index).getAnimation() == WALK_WITH_LEGS_ANIMATION.animation
					|| animation_state.getCurrent(newAnimation.track_index)
							.getAnimation() == WALK_WITHOUT_LEGS_ANIMATION.animation
					|| animation_state.getCurrent(newAnimation.track_index)
							.getAnimation() == WALK_BACK_ANIMATION.animation)) {
				float timeScale = Math.abs(vel.x / Shared.CHARACTER_MAX_VELOCITY_X);

				if (timeScale >= 0.1f)
					animation_state.getCurrent(newAnimation.track_index).setTimeScale(timeScale);
				else
					animation_state.getCurrent(newAnimation.track_index).setTimeScale(0.1f);
			}
			break;
		case P1_CHARGE:
			newAnimation = getCurrentChargeP1Animation();
			break;
		case P1_CHANNEL:
			newAnimation = getCurrentChannelP1Animation();
			break;
		case P3:
			newAnimation = P3_ANIMATION;
			break;
		case RECEIVE_DAMAGE:
			newAnimation = RECEIVE_DAMAGE_ANIMATION;
			break;
		case LIGHTPORT:
			newAnimation = EFFECT_LIGHTPORT_ANIMATION;
			break;
		case BLACKHOLE:
			newAnimation = EFFECT_BLACKHOLE_ANIMATION;
			break;
		case COLOR_ICE:
			newAnimation = EFFECT_COLOR_ICE_ANIMATION;
			break;
		case COLOR_TOXIC:
			newAnimation = EFFECT_COLOR_TOXIC_ANIMATION;
			break;
		case COLOR_DEFAULT:
			if (isAlive() == true)
				newAnimation = EFFECT_COLOR_DEFAULT_ANIMATION;
			// otherwise alpha fade in will reset colors
			break;
		case DEATH_EXPLOSION:

			// clear tracks
			animation_state.clearTrack(TRACK_0);
			/*
			 * the following is hack. We need to ensure that track0 is not set to
			 * 'fall_animation' because otherwise the fall animation wouldn't kick in (see
			 * if clause below for track0). The fall animation needs to kick in to set the
			 * body parts back to their original pose.
			 */
			current_track0_animation = WALK_WITH_LEGS_ANIMATION;
			animation_state.clearTrack(TRACK_1);
			animation_state.clearTrack(TRACK_2);
			animation_state.clearTrack(TRACK_3);
			animation_state.clearTrack(TRACK_4);

			/*
			 * following line prevents maintaining the skill-animation pose after getting
			 * killed while casting
			 */
			skeleton.setBonesToSetupPose();

			newAnimation = DEATH_EXPLOSION_ANIMATION;
			break;
		case ALPHA_FADE_IN:
			newAnimation = ALPHA_FADE_IN_ANIMATION;
			break;
		case ALPHA_FADE_OUT:
			// for example invisibility
			newAnimation = ALPHA_FADE_OUT_ANIMATION;
			break;
		case HOLD_GROUND_LEFT:
			newAnimation = HOLD_GROUND_LEFT_ANIMATION;
			break;
		case HOLD_GROUND_RIGHT:
			newAnimation = HOLD_GROUND_RIGHT_ANIMATION;
			break;
		case HOLD_CEILING_LEFT:
			newAnimation = HOLD_CEILING_LEFT_ANIMATION;
			break;
		case HOLD_CEILING_RIGHT:
			newAnimation = HOLD_CEILING_RIGHT_ANIMATION;
			break;
		case SLIDE_GROUND_LEFT:
			newAnimation = SLIDE_GROUND_LEFT_ANIMATION;
			break;
		case SLIDE_GROUND_RIGHT:
			newAnimation = SLIDE_GROUND_RIGHT_ANIMATION;
			break;
		case SLIDE_CEILING_LEFT:
			newAnimation = SLIDE_CEILING_LEFT_ANIMATION;
			break;
		case SLIDE_CEILING_RIGHT:
			newAnimation = SLIDE_CEILING_RIGHT_ANIMATION;
			break;
		}

		if (newAnimation != null) {

			if (newAnimation.track_index == TRACK_0 && newAnimation != current_track0_animation) {
				current_track0_animation = changeAnimationTo(current_track0_animation, newAnimation);
			} else if (newAnimation.track_index == TRACK_1 && newAnimation != current_track1_animation) {

				setCursorDominant(true);

				current_track1_animation = changeAnimationTo(current_track1_animation, newAnimation);
			} else if (newAnimation.track_index == TRACK_2) {
				current_track2_animation = changeAnimationTo(current_track2_animation, newAnimation);
			} else if (newAnimation.track_index == TRACK_3) {
				// track 3, color change for over time effects
				current_track3_animation = changeAnimationTo(current_track3_animation, newAnimation);
			} else if (newAnimation.track_index == TRACK_4) {
				// track 4, visibility
				current_track4_animation = changeAnimationTo(current_track4_animation, newAnimation);
			}
		}

	}

	private MyAnimation getCurrentChargeP1Animation() {
		if (info_animations.current_table_skill_index == -1)
			return null;
		int index = info_animations.current_table_skill_index;
		MyAnimation p1Animation = null;
		float timeScale = 1;

		if (index < skills.size()) {
			ASkill skill = skills.get(index);

			// e.g. dying skill has -1
			if (skill.getP1Version() < 0)
				return null;
			p1Animation = charge_p1_animations.get(skill.getP1Version());
			timeScale = skill.getTimeScaleP1();

			animation_state_data.setMix(p1Animation.animation, P3_ANIMATION.animation, 0.1f);
			// 1.6f/timeScale);
			animation_state_data.setMix(P3_ANIMATION.animation, p1Animation.animation, 0.1f);
			// 0.2f/timeScale);

			// if (p1Animation.animation.getDuration() / timeScale > skill
			// .getCooldownModifiableAbsolute())
			// timeScale = p1Animation.animation.getDuration()
			// / skill.getCooldownModifiableAbsolute();
		}
		return p1Animation.setTimeScale(timeScale);
	}

	private MyAnimation getCurrentChannelP1Animation() {
		if (info_animations.current_table_skill_index == -1)
			return null;
		MyAnimation p1Animation = null;
		float duration = 1;

		/*
		 * the first two are spawnAfterDeath and Dying.
		 */

		if (info_animations.current_table_skill_index > 2) {
			// regular skill
			ASkill skill = skills.get(info_animations.current_table_skill_index);
			p1Animation = channel_p1_animations.get(skill.getP1Version());
			duration = 0;
		}

		if (p1Animation == null)
			return null;

		p1Animation.animation.setDuration(duration);
		return p1Animation;
	}

	/**
	 * Returns the new animation or the old one when some conditions aren't
	 * fulfilled. Override {@link #current_track0_animation},
	 * {@link #current_track1_animation}, {@link #current_track2_animation} or
	 * {@link #current_track3_animation} with the return of this method.
	 * 
	 * @param oldAnimation
	 * @param newAnimation
	 * @return
	 */
	private MyAnimation changeAnimationTo(MyAnimation oldAnimation, MyAnimation newAnimation) {

		// TrackEntry track4 = animation_state.getCurrent(TRACK_4);
		// if (track4 != null && track4.getAnimation() ==
		// DEATH_EXPLOSION_ANIMATION.animation)
		// return oldAnimation;

		int trackIndex = newAnimation.track_index;

		if (oldAnimation != null)
			if (oldAnimation.runThrough(newAnimation) == true)
				if (animation_state.getCurrent(trackIndex) != null)
					if (animation_state.getCurrent(trackIndex).isComplete() == false)
						return oldAnimation;

		// De morgan (andere schreibweise als oben): überschreibe nur wenn es
		// komplett ist oder wenn es nicht pass through ist.

		// isComplete kann man genau zwischen beendigung und
		// neuanfang fragen, das heißt
		// man hat eine möglichkeit dafür true zu bekommen,
		// danach wird die animation null

		/*
		 * the timescale of track before has to be reseted to the same timescale as
		 * newAnimation, so the interpolation time between two animations have always
		 * the same length.
		 */
		if (animation_state.getCurrent(newAnimation.track_index) != null)
			animation_state.getCurrent(newAnimation.track_index).setTimeScale(newAnimation.time_scale);

		TrackEntry e = animation_state.setAnimation(newAnimation);

		e.setTimeScale(newAnimation.time_scale);
		newAnimation.begin();
		return newAnimation;

	}

	@Override
	protected void onetimeActionInactive(WorldAction action) {
		super.onetimeActionInactive(action);

		if (isAlive() == true && match_finished == false) {
			switch (action) {
			case MOVE_LEFT:
				if (isCurrentActionActive(WorldAction.MOVE_RIGHT) == true)
					current_track0_animation.flipSkeletonKeyboard(false);
				break;
			case MOVE_RIGHT:
				if (isCurrentActionActive(WorldAction.MOVE_LEFT) == true)
					current_track0_animation.flipSkeletonKeyboard(true);
				break;
			}
		} else {
			// dead == true
		}
	}

	@Override
	protected void onetimeActionActive(WorldAction action) {
		super.onetimeActionActive(action);

		switch (action) {
		case MOVE_LEFT:
			current_track0_animation.flipSkeletonKeyboard(true);
			break;
		case MOVE_RIGHT:
			current_track0_animation.flipSkeletonKeyboard(false);
			break;

		}
	}

	@Override
	protected void continuousAction(WorldAction action) {

	}

	@Override
	public CustomSkeleton getSkeleton() {
		return skeleton;
	}

	@Override
	public void fillDeathBonesArray() {
		bones_for_death = new ArrayList<Bone>();
		bones_for_death.add(head_bone_rotation);
		bones_for_death.add(torso_bone_rotation);
		bones_for_death.add(hand_back_bone_rotation);
		bones_for_death.add(hand_front_bone_rotation);
		bones_for_death.add(shoulder_front1_bone_rotation);
		bones_for_death.add(shoulder_front2_bone_rotation);
		bones_for_death.add(shoulder_back1_bone_rotation);
		bones_for_death.add(shoulder_back2_bone_rotation);
		bones_for_death.add(leg_front1_bone_rotation);
		bones_for_death.add(leg_front2_bone_rotation);
		bones_for_death.add(leg_back1_bone_rotation);
		bones_for_death.add(leg_back2_bone_rotation);
	}

	@Override
	public ArrayList<Bone> getDeathBones() {
		return bones_for_death;
	}

	@Override
	public boolean isFlip() {
		return getHeadFlip();
	}

	/**
	 * Returns the direction of player's head.
	 * 
	 * @return <code>true</code> = looking to the left, otherwise <code>false</code>
	 */
	public boolean getHeadFlip() {
		return CustomSkeleton.getBoneFlipX(head_bone_flip);
	}

	@Override
	public boolean isCombo1Stored() {
		return sparse_combo_storage1.isStoring();
	}

	@Override
	public boolean isCombo2Stored() {
		return sparse_combo_storage2.isStoring();
	}

	@Override
	public CustomParticleEffect getComboHandFrontParticleEffect() {
		return hand_front_combo_particle_effect;
	}

	@Override
	public CustomParticleEffect getComboHandBackParticleEffect() {
		return hand_back_combo_particle_effect;
	}

	private void flipBackHandBone(boolean flipX) {
		CustomSkeleton.setBoneFlipX(flipX, hand_back_bone_flip);
		if (hand_back_particle_effect != null)
			hand_back_particle_effect.setFlipX(flipX);
		if (hand_back_combo_particle_effect != null)
			hand_back_combo_particle_effect.setFlipX(flipX);
	}

	private void flipFrontHandBone(boolean flipX) {
		CustomSkeleton.setBoneFlipX(flipX, hand_front_bone_flip);
		if (hand_front_particle_effect != null)
			hand_front_particle_effect.setFlipX(flipX);
		if (hand_front_combo_particle_effect != null)
			hand_front_combo_particle_effect.setFlipX(flipX);
	}

	private void flipTorsoBone(boolean flipX) {
		CustomSkeleton.setBoneFlipX(flipX, torso_bone_flip);
		if (torso_particle_effect != null)
			torso_particle_effect.setFlipX(flipX);
	}

	private void flipShoulderFrontBone(boolean flipX) {
		CustomSkeleton.setBoneFlipX(flipX, shoulder_front_bone_flip);
		if (shoulder_front_particle_effect != null)
			shoulder_front_particle_effect.setFlipX(flipX);
	}

	private void flipShoulderBackBone(boolean flipX) {
		CustomSkeleton.setBoneFlipX(flipX, shoulder_back_bone_flip);
		if (shoulder_back_particle_effect != null)
			shoulder_back_particle_effect.setFlipX(flipX);
	}

	private void flipLegFrontBone(boolean flipX) {
		CustomSkeleton.setBoneFlipX(flipX, leg_front_bone_flip);
	}

	private void flipLegBackBone(boolean flipX) {
		CustomSkeleton.setBoneFlipX(flipX, leg_back_bone_flip);
	}

	private void flipHeadBone(boolean flipX) {
		CustomSkeleton.setBoneFlipX(flipX, head_bone_flip);
		if (head_particle_effect != null) {
			head_particle_effect.setFlipX(flipX);
		}
	}

	@Override
	public short getLastHealth() {
		return last_health;
	}

	@Override
	public void setLastHealth(short lastHealth) {
		this.last_health = lastHealth;
	}

	@Override
	public DamageDiffbar getDamageDiffBar() {
		return damage_diff_bar;
	}

	/**
	 * Called when skill begins or ends.
	 * 
	 * @param cursorDominant
	 */
	private void setCursorDominant(boolean cursorDominant) {
		this.cursor_dominant = cursorDominant;
		if (cursorDominant == true)
			// skill active
			flipByCursor();
		else if (current_track0_animation != null)
			// skill inactive
			// apply flip information in begin() (animation specific)
			current_track0_animation.begin();
	}

	private class MyAnimation {
		private Animation animation;
		private boolean loop;

		private int flip_skeleton_when_change = 0, track_index;

		private float time_scale = 1f;

		private float duration_extension;

		/**
		 * Universal animation constructor
		 * <p>
		 * Useful when the skeleton should be flipped in one specific direction at an
		 * animation change.
		 * <p>
		 * It's possible, that the animation changes while A or D is already pressed. So
		 * there will be no flipping until a new pressing. For this case the skeleton
		 * can be flipped with {@link #flip_skeleton_when_change}.
		 * <p>
		 * 
		 * @param animation
		 * @param trackIndex             , determine which track this animation uses
		 * @param loop
		 * @param skeletonFlipWhenChange 0-maintaining regardless of active keys, 1-true
		 *                               (eyes look to the left), 2-false (eyes look the
		 *                               right), 3-skeletonFlipWhenChange is depending
		 *                               on active-keys, if no key is pressed the flip
		 *                               is orientated towards head-flipping
		 */
		public MyAnimation(Animation animation, int trackIndex, boolean loop, int skeletonFlipWhenChange) {

			this.animation = animation;
			this.loop = loop;
			this.track_index = trackIndex;

			this.flip_skeleton_when_change = skeletonFlipWhenChange;
			animation_list.add(this);
		}

		public MyAnimation setTimeScale(float timeScale) {
			time_scale = timeScale;
			return this;
		}

		/**
		 * Call this method when this animation starts (after a change)
		 */
		public void begin() {
			duration_extension = 0;
			if (cursor_dominant == false) {
				switch (flip_skeleton_when_change) {
				case 0:
					// do nothing (no flip)
					break;
				case 1:
					flip(true);
					break;
				case 2:
					flip(false);
					break;
				case 3:
					if (isCurrentActionActive(WorldAction.MOVE_LEFT) == false
							&& isCurrentActionActive(WorldAction.MOVE_RIGHT) == true)
						flip(false);

					else if (isCurrentActionActive(WorldAction.MOVE_RIGHT) == false
							&& isCurrentActionActive(WorldAction.MOVE_LEFT) == true)
						flip(true);

					else if (isCurrentActionActive(WorldAction.MOVE_RIGHT) == true
							&& isCurrentActionActive(WorldAction.MOVE_LEFT) == true) {
						if (left_dominant)
							flip(true);
						else
							flip(false);
					} else
						flip(CustomSkeleton.getBoneFlipX(head_bone_flip));

					break;

				}
			}
		}

		/**
		 * True if the animation has to pass-through the whole time / run until the
		 * animation ends. Default return is false, so the physic dictates which
		 * animation is active. You might override this method if you want a other
		 * behaviour.
		 * 
		 * @param nextAnimation
		 * @return default false, this means that if the physic says "switch animation"
		 *         the animation will switch.
		 */
		public boolean runThrough(MyAnimation nextAnimation) {
			return false;
		}

		/**
		 * This method is called when player presses A or D. Some animations has
		 * deactivated the field {@link #flip_a_d_dominant_to_cursor}, so there is no
		 * flipping
		 * 
		 * @param flip
		 */
		public void flipSkeletonKeyboard(boolean flip) {
			if (cursor_dominant == false)
				flip(flip);
		}

		/**
		 * This method is called when player moves the cursor in a way that the player
		 * has to be flipped. The skeleton will only flip when A or D is NOT pressed.
		 * 
		 * @param flip
		 */
		public void flipSkeletonCursor(boolean flip) {
			if (isCurrentActionActive(WorldAction.MOVE_LEFT) == false
					&& isCurrentActionActive(WorldAction.MOVE_RIGHT) == false || cursor_dominant == true)
				flip(flip);
		}

		/**
		 * Wrapper method. All other flip method will pass this method (bottommost
		 * method of all flip-method). To specify the flip override
		 * {@link #flipSpecific(boolean)}
		 * 
		 * @param flip
		 */
		public void flip(boolean flip) {
			if (isAlive() == true)
				flipSpecific(flip);

		}

		/**
		 * Override this method for more specific flipping!
		 * <p>
		 * Default: flip whole skeleton
		 * 
		 * @param flip
		 */
		public void flipSpecific(boolean flip) {
			flipBackHandBone(flip);
			flipFrontHandBone(flip);
			flipTorsoBone(flip);
			flipHeadBone(flip);
			flipShoulderFrontBone(flip);
			flipShoulderBackBone(flip);
			flipLegFrontBone(flip);
			flipLegBackBone(flip);
		}

		@Override
		public String toString() {
			return animation.toString();
		}
	}

	private class MyAnimationState extends AnimationState {

		public MyAnimationState(AnimationStateData data) {
			super(data);

		}

		public TrackEntry setAnimation(MyAnimation animation) {
			return super.setAnimation(animation.track_index, animation.animation, animation.loop);
		}
	}

	/**
	 * Necessary to get the information whether the quest is done or not. In
	 * {@link AGameclassSelectionModule}
	 * 
	 * @param skin
	 */
	public void setSkin(GameclassSkin skin) {
		gameclass_skin = skin;
	}

	public GameclassSkin getSkin() {
		return gameclass_skin;
	}

	public void setAbsorptionChange1(boolean absorptionChange) {
		this.absorption_change1 = absorptionChange;
	}

	/**
	 * returns <code>true</code> when a change of the first combo-storage is
	 * recognized
	 * 
	 * @return
	 */
	public boolean isAbsorptionChange1() {
		return absorption_change1;
	}

	public void setAbsorptionChange2(boolean absorptionChange) {
		this.absorption_change2 = absorptionChange;
	}

	/**
	 * returns <code>true</code> when a change of the second combo-storage is
	 * recognized
	 * 
	 * @return
	 */
	public boolean isAbsorptionChange2() {
		return absorption_change2;
	}

	public SkillThumbnailMovement getSkillThumbnailMovement1() {
		return skill_thumbnail_movement1;
	}

	public AlphaTransition getSkillThumbnailAlphaTransition1() {
		return skill_thumbnail_alpha_transition1;
	}

	public SkillThumbnailMovement getSkillThumbnailMovement2() {
		return skill_thumbnail_movement2;
	}

	public AlphaTransition getSkillThumbnailAlphaTransition2() {
		return skill_thumbnail_alpha_transition2;
	}

	public CustomSpineWidget getSpineComboFeedback() {
		return spine_combo_feedback;
	}

}
