package com.elementar.logic.characters;

public class AlphaTransition {

	/**
	 * Between 0 and 1
	 */
	private float value = 1;

	public void setValue(float transition) {
		value = transition;
	}

	public float getValue() {
		return value;
	}

}
