package com.elementar.logic.characters;

import java.util.ArrayList;
import java.util.LinkedList;

import com.badlogic.gdx.math.Vector2;
import com.elementar.logic.characters.skills.SparseComboStorage;
import com.elementar.logic.emission.SparseProjectile;
import com.elementar.logic.network.requests.InputRequest;
import com.elementar.logic.network.response.AInfo;
import com.elementar.logic.util.SparseObject;
import com.elementar.logic.util.lists.NotNullArrayList;

/**
 * Use this client for sending data which are relevant for ingame. This client
 * is designed for high-frequently sending. So the data that are sent are only
 * the bare necessities.
 * 
 * @author lukassongajlo
 * 
 */
public class SparseClient extends SparseObject {

	public short health_current;
	public short mana_current;

	public SparseComboStorage sparse_combo_storage_transfer = new SparseComboStorage();
	public SparseComboStorage sparse_combo_storage1 = new SparseComboStorage();
	public SparseComboStorage sparse_combo_storage2 = new SparseComboStorage();

	public Vector2 cursor_pos = new Vector2();

	/**
	 * Holds all active projectiles
	 */
	public LinkedList<SparseProjectile> sparse_projectiles = new LinkedList<SparseProjectile>();

	/**
	 * Copies of these input requests were processed by the server. The requests in
	 * this list aren't processed, so you have the same inputs as the server already
	 * processed. With these requests you are able to simulate client movements to
	 * get sound effects, animations, commands of the command rose, and pings on
	 * minimap
	 */
	public NotNullArrayList<InputRequest> redirected_inputs = new NotNullArrayList<InputRequest>();

	public ArrayList<AInfo> player_infos = new ArrayList<>();

	public float cooldown_skill0, cooldown_skill1, cooldown_skill2;

	public SparseClient() {
	}

	public SparseClient(short id) {
		super(id, new Vector2(), new Vector2());
	}

	/**
	 * To send via network from game server to clients
	 * 
	 * @param connectionID
	 * @param playerPos
	 * @param playerVel
	 * @param cursorPos
	 * @param healthCurrent
	 * @param manaCurrent
	 * @param sparseProjectiles
	 * @param nonProcessedInput
	 * @param sparseComboStorage1
	 * @param sparseComboStorage2
	 * @param infos
	 * @param cooldownSkill0
	 * @param cooldownSkill1
	 * @param cooldownSkill2
	 */
	public void set(int connectionID, Vector2 playerPos, Vector2 playerVel, Vector2 cursorPos, short healthCurrent,
			short manaCurrent, LinkedList<SparseProjectile> sparseProjectiles,
			NotNullArrayList<InputRequest> nonProcessedInput, SparseComboStorage sparseComboStorage1,
			SparseComboStorage sparseComboStorage2, ArrayList<AInfo> infos, float cooldownSkill0, float cooldownSkill1,
			float cooldownSkill2) {
		this.connection_id = (short) connectionID;
		this.pos.set(playerPos);
		this.vel.set(playerVel);
		this.cursor_pos.set(cursorPos);
		this.health_current = healthCurrent;
		this.mana_current = manaCurrent;
		this.sparse_projectiles = sparseProjectiles;
		this.redirected_inputs = nonProcessedInput;
		this.sparse_combo_storage1 = sparseComboStorage1;
		this.sparse_combo_storage2 = sparseComboStorage2;
		this.player_infos = infos;
		this.cooldown_skill0 = cooldownSkill0;
		this.cooldown_skill1 = cooldownSkill1;
		this.cooldown_skill2 = cooldownSkill2;
	}

}
