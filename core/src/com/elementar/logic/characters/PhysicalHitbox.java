package com.elementar.logic.characters;

import static com.elementar.logic.util.Shared.PPM;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.elementar.logic.characters.abstracts.APhysicalElement;
import com.elementar.logic.map.buildings.Base;
import com.elementar.logic.util.CollisionData;
import com.elementar.logic.util.Shared.Team;
import com.elementar.logic.util.userdata.CharacterHeadContacts;
import com.elementar.logic.util.userdata.CharacterLandingContacts;
import com.elementar.logic.util.userdata.CharacterLeftBottomContacts;
import com.elementar.logic.util.userdata.CharacterLeftTopContacts;
import com.elementar.logic.util.userdata.CharacterMidBottomContacts;
import com.elementar.logic.util.userdata.CharacterMidTopContacts;
import com.elementar.logic.util.userdata.CharacterPlantContacts;
import com.elementar.logic.util.userdata.CharacterRightBottomContacts;
import com.elementar.logic.util.userdata.CharacterRightTopContacts;
import com.elementar.logic.util.userdata.CharacterUndersideContacts;
import com.elementar.logic.util.userdata.SeparatorContacts;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class PhysicalHitbox extends APhysicalElement {

	protected CharacterMidBottomContacts foot_contacts;
	protected CharacterUndersideContacts underside_contacts;
	protected CharacterRightBottomContacts right_bottom_contacts;
	protected CharacterLeftBottomContacts left_bottom_contacts;
	protected CharacterRightTopContacts right_top_contacts;
	protected CharacterLeftTopContacts left_top_contacts;
	protected CharacterHeadContacts head_contacts;
	protected CharacterMidTopContacts mid_top_contacts;
	protected CharacterLandingContacts landing_contacts;

	protected CharacterPlantContacts plant_contacts;

	protected SeparatorContacts separator_contacts;

	/**
	 * surrounding character. category = foot, mask = plant
	 */
	protected Fixture plant_sensor;

	/**
	 * divided into sensors and solid hitboxes.
	 */
	protected Fixture underside_fixture, torso_fixture, light_fixture, mid_bottom_sensor, left_bottom_sensor,
			right_bottom_sensor, left_top_sensor, right_top_sensor, mid_top_sensor, landing_sensor;

	/**
	 * surrounding character, collides with other characters to avoid that
	 * characters stand in one another.
	 */
	protected Fixture separator_sensor;

	/**
	 * Surrounds character like {@link #plant_sensor}. category =
	 * {@link CollisionData#BIT_CHARACTER_PROJECTILE}, mask =
	 * {@link CollisionData#BIT_PROJECTILE}
	 */
	protected Fixture projectile_sensor;

	protected Team team;

	/**
	 * Userdata for {@link #damage_sensor} and {@link #heal_sensor}
	 */
	protected PhysicalClient physical_client;

	/**
	 * 
	 * @param world
	 * @param physicalClient
	 * @param team
	 */
	public PhysicalHitbox(World world, PhysicalClient physicalClient, Team team) {
		super(world);
		this.team = team;
		physical_client = physicalClient;
		foot_contacts = new CharacterMidBottomContacts();
		underside_contacts = new CharacterUndersideContacts();
		right_bottom_contacts = new CharacterRightBottomContacts();
		left_bottom_contacts = new CharacterLeftBottomContacts();
		right_top_contacts = new CharacterRightTopContacts();
		left_top_contacts = new CharacterLeftTopContacts();
		head_contacts = new CharacterHeadContacts();
		mid_top_contacts = new CharacterMidTopContacts();
		landing_contacts = new CharacterLandingContacts();
		plant_contacts = new CharacterPlantContacts();
		separator_contacts = new SeparatorContacts(physicalClient);

		Vector2 startPos = new Vector2();
		if (team == Team.TEAM_1)
			startPos.set(Base.LEFT_BASE_CENTER);
		else
			// Team2
			startPos.set(Base.RIGHT_BASE_CENTER);

		startPos.y = Base.Y_SPAWN_SHIFT;

		initPhysics(startPos.x, startPos.y);
		setCollisionBits();

	}

	private void setCollisionBits() {
		// set collisions bits

		// define category & mask bits for movement fixtures
		Filter collisionFilter1 = new Filter();
		collisionFilter1.maskBits = CollisionData.BIT_GROUND;
		collisionFilter1.categoryBits = CollisionData.BIT_CHARACTER_MOVEMENT;
		underside_fixture.setFilterData(collisionFilter1);
		torso_fixture.setFilterData(collisionFilter1);
		right_bottom_sensor.setFilterData(collisionFilter1);
		left_bottom_sensor.setFilterData(collisionFilter1);
		right_top_sensor.setFilterData(collisionFilter1);
		left_top_sensor.setFilterData(collisionFilter1);
		mid_top_sensor.setFilterData(collisionFilter1);
		mid_bottom_sensor.setFilterData(collisionFilter1);
		landing_sensor.setFilterData(collisionFilter1);

		// define category & mask bits for hit detection
		Filter collisionFilter2 = new Filter();
		collisionFilter2.maskBits = CollisionData.BIT_PROJECTILE;
		collisionFilter2.categoryBits = CollisionData.BIT_CHARACTER_PROJECTILE;
		projectile_sensor.setFilterData(collisionFilter2);

		// define category & mask bits for collisions with plants
		Filter collisionFilter3 = new Filter();
		collisionFilter3.maskBits = CollisionData.BIT_PLANT;
		collisionFilter3.categoryBits = CollisionData.BIT_CHARACTER_PLANT;
		plant_sensor.setFilterData(collisionFilter3);

		// define category & mask bits for collisions with light (para-world
		// character)
		Filter collisionFilter4 = new Filter();
		collisionFilter4.maskBits = CollisionData.BIT_LIGHT;
		collisionFilter4.categoryBits = CollisionData.BIT_CHARACTER_LIGHT;
		light_fixture.setFilterData(collisionFilter4);

		Filter collisionFilter5 = new Filter();
		collisionFilter5.maskBits = CollisionData.BIT_CHARACTER_SEPARATOR;
		collisionFilter5.categoryBits = CollisionData.BIT_CHARACTER_SEPARATOR;
		separator_sensor.setFilterData(collisionFilter5);

	}

	@Override
	protected void initPhysics(float x, float y) {
		body_def.type = BodyType.DynamicBody;
		body_def.allowSleep = false;
		body_def.position.set(x, y);
		body_def.fixedRotation = true;
		body_def.bullet = true;

		// create body
		body = world.createBody(body_def);

		// create shapes
		CircleShape circleShape = new CircleShape();
		PolygonShape polygonShape = new PolygonShape();
		float scale = 1.5f;
		float yShift = 23f / PPM;
		// create underside fixture
		circleShape.setRadius(scale * 7.5f / PPM);
		circleShape.setPosition(new Vector2(0, -yShift + scale * 2f / PPM));
		fixture_def.shape = circleShape;
		fixture_def.friction = 0f;
		fixture_def.restitution = 0f;
		fixture_def.density = 0f; // also by default = 0...

		underside_fixture = body.createFixture(fixture_def);
		underside_fixture.setUserData(underside_contacts);

		// create torso fixture
		float height = 14f;
		polygonShape.setAsBox(scale * 6.5f / PPM, scale * height / PPM, new Vector2(0, -yShift + scale * height / PPM),
				0);
		fixture_def.shape = polygonShape;
		torso_fixture = body.createFixture(fixture_def);
		torso_fixture.setUserData(head_contacts);

		// create light fixture (para world character)
		light_fixture = body.createFixture(fixture_def);

		// SENSORS:
		fixture_def.isSensor = true;
		fixture_def.shape = polygonShape;

		float sensorSize = scale * 4f / PPM;

		// TOP sensors
		polygonShape.setAsBox(sensorSize, sensorSize, new Vector2(0, -yShift + scale * 32f / PPM), 0);
		mid_top_sensor = body.createFixture(fixture_def);
		mid_top_sensor.setUserData(mid_top_contacts);

		polygonShape.setAsBox(sensorSize, sensorSize, new Vector2(scale * 8f / PPM, -yShift + scale * 26f / PPM), 0);
		right_top_sensor = body.createFixture(fixture_def);
		right_top_sensor.setUserData(right_top_contacts);

		polygonShape.setAsBox(sensorSize, sensorSize, new Vector2(scale * -8f / PPM, -yShift + scale * 26f / PPM), 0);
		left_top_sensor = body.createFixture(fixture_def);
		left_top_sensor.setUserData(left_top_contacts);

		// BOTTOM sensors
		polygonShape.setAsBox(scale * 2f / PPM, scale * 6f / PPM, new Vector2(0, -yShift + scale * -6f / PPM), 0);
		mid_bottom_sensor = body.createFixture(fixture_def);
		mid_bottom_sensor.setUserData(foot_contacts);

		polygonShape.setAsBox(sensorSize, sensorSize, new Vector2(scale * 6f / PPM, -yShift + scale * 4f / PPM), 0);
		right_bottom_sensor = body.createFixture(fixture_def);
		right_bottom_sensor.setUserData(right_bottom_contacts);

		polygonShape.setAsBox(sensorSize, sensorSize, new Vector2(scale * -6f / PPM, -yShift + scale * 4f / PPM), 0);
		left_bottom_sensor = body.createFixture(fixture_def);
		left_bottom_sensor.setUserData(left_bottom_contacts);

		polygonShape.setAsBox(scale * 1.5f / PPM, scale * 20f / PPM, new Vector2(0, -yShift + scale * -10f / PPM), 0);
		landing_sensor = body.createFixture(fixture_def);
		landing_sensor.setUserData(landing_contacts);

		// plant sensor
		polygonShape.setAsBox(scale * 12f / PPM, scale * 25f / PPM, new Vector2(0, -yShift + scale * 24f / PPM), 0);
		plant_sensor = body.createFixture(fixture_def);
		plant_sensor.setUserData(plant_contacts);

		// projectile sensor, same dimensions as plant sensor
		projectile_sensor = body.createFixture(fixture_def);
		projectile_sensor.setUserData(physical_client);

		circleShape.setRadius(SeparatorContacts.SEPARATOR_RADIUS);
		circleShape.setPosition(new Vector2(0, 5 * scale / PPM));
		separator_sensor = body.createFixture(fixture_def);
		separator_sensor.setUserData(separator_contacts);

		polygonShape.dispose();
		circleShape.dispose();

	}

	public void setVelocity(Vector2 vel) {
		body.setLinearVelocity(vel);
	}

	/**
	 * Don't use this method. The physical body of a client needs more than 1
	 * collision filter.
	 */
	@Deprecated
	@Override
	protected Filter getCollisionFilter() {
		return null;
	}

	public Team getTeam() {
		return team;
	}

	public Fixture getProjectileSensor() {
		return projectile_sensor;
	}

	public void setPlantCollisionFilter(boolean visible) {
		if (plant_sensor != null) {
			Filter f = new Filter();
			f.categoryBits = CollisionData.BIT_CHARACTER_PLANT;
			// determine maskBits by visible state
			if (visible == true)
				f.maskBits = CollisionData.BIT_PLANT;
			else
				f.maskBits = 0;
			plant_sensor.setFilterData(f);
		}
	}

}