+spits_center_e2_ground
- Delay -
active: false
- Duration - 
lowMin: 50.0
lowMax: 50.0
- Count - 
min: 1
max: 220
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 200.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 800.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 220.0
highMax: 220.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 220.0
highMax: 220.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 150.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.1965924
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 100.0
lowMax: 100.0
highMin: 1700.0
highMax: 400.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.9411765
scaling2: 0.19607843
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.26712328
timeline2: 0.39041096
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 500.0
lowMax: 500.0
highMin: 10.0
highMax: 360.0
relative: true
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.8245614
scaling2: 0.8596491
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.1369863
timeline2: 0.86986303
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: false
behind: false
premultipliedAlpha: false
spriteMode: random
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/leaf_skill2_1.png
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/leaf_skill2_2.png


+spits_big_e2_ground
- Delay -
active: false
- Duration - 
lowMin: 150.0
lowMax: 150.0
- Count - 
min: 1
max: 1220
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 160.0
highMax: 160.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 800.0
highMax: 1200.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 220.0
highMax: 220.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 220.0
highMax: 220.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 200.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.1965924
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 2600.0
relative: false
scalingCount: 6
scaling0: 1.0
scaling1: 0.93820226
scaling2: 0.58426964
scaling3: 0.16292135
scaling4: 0.039325844
scaling5: 0.0
timelineCount: 6
timeline0: 0.0
timeline1: 0.10243055
timeline2: 0.19270833
timeline3: 0.27430555
timeline4: 0.421875
timeline5: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 500.0
lowMax: 500.0
highMin: 10.0
highMax: 360.0
relative: true
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: true
lowMin: -500.0
lowMax: -500.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.9411765
scaling2: 0.19607843
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.39041096
timeline2: 0.70547944
timeline3: 1.0
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.5263158
scaling2: 0.16292135
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.06849315
timeline2: 0.3963816
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_movement_walk1.png


+sparks
- Delay -
active: false
- Duration - 
lowMin: 150.0
lowMax: 150.0
- Count - 
min: 1
max: 220
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 70.0
highMax: 70.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 800.0
highMax: 600.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 440.0
highMax: 440.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 440.0
highMax: 440.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 30.0
highMax: 250.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.1965924
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 100.0
lowMax: 100.0
highMin: 400.0
highMax: 1500.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 1.0
scaling2: 0.19607843
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.15068494
timeline2: 0.23287672
timeline3: 1.0
- Angle - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 1.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 100.0
lowMax: 100.0
highMin: 10.0
highMax: 360.0
relative: true
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.77254903
colors1: 1.0
colors2: 0.44313726
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.64912283
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.05479452
timeline2: 0.63013697
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_spark1.png


sparks_strong_glow
- Delay -
active: false
- Duration - 
lowMin: 150.0
lowMax: 150.0
- Count - 
min: 1
max: 220
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 2.0
highMax: 2.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 400.0
highMax: 600.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 440.0
highMax: 440.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 440.0
highMax: 440.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 1450.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.1965924
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 100.0
lowMax: 100.0
highMin: 300.0
highMax: 100.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 1.0
scaling2: 0.19607843
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.15068494
timeline2: 0.33561644
timeline3: 1.0
- Angle - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 1.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.73333335
colors1: 1.0
colors2: 0.44313726
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.75438595
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.05479452
timeline2: 0.75342464
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_spark1.png

