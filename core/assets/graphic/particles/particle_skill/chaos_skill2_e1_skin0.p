+combo_success1_chaos_e1
- Delay -
active: false
- Duration - 
lowMin: 600.0
lowMax: 600.0
- Count - 
min: 1
max: 200
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 60.0
highMax: 60.0
relative: true
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 600.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 200.0
relative: false
scalingCount: 5
scaling0: 1.0
scaling1: 0.50980395
scaling2: 0.21568628
scaling3: 0.05882353
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.1849315
timeline2: 0.39726028
timeline3: 0.6643836
timeline4: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 1.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.92941177
colors1: 0.92941177
colors2: 0.92941177
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.9649123
scaling2: 0.31460676
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.19178082
timeline2: 0.48372093
timeline3: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/pre_particle.png


combo_success2_attached
- Delay -
active: false
- Duration - 
lowMin: 5000.0
lowMax: 5000.0
- Count - 
min: 1
max: 1
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 2.0
highMax: 2.0
relative: true
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 5000.0
highMax: 5000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: true
lowMin: -100.0
lowMax: -100.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 100.0
lowMax: 100.0
highMin: 280.0
highMax: 280.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 1.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.92941177
colors1: 0.47843137
colors2: 0.69411767
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.7247191
scaling2: 0.5898876
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.0736
timeline2: 0.8304
timeline3: 1.0
- Options - 
attached: true
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/global_combo_sending1.png


combo_success2_tail
- Delay -
active: false
- Duration - 
lowMin: 800.0
lowMax: 800.0
- Count - 
min: 1
max: 1000
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 80.0
highMax: 80.0
relative: true
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 800.0
highMax: 800.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 900.0
relative: false
scalingCount: 5
scaling0: 1.0
scaling1: 0.50980395
scaling2: 0.21568628
scaling3: 0.05882353
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.1849315
timeline2: 0.39726028
timeline3: 0.6643836
timeline4: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 1000.0
highMax: 1000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 180.0
highMax: 180.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.92941177
colors1: 0.25490198
colors2: 0.4627451
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 6
scaling0: 0.0
scaling1: 0.76404494
scaling2: 0.21910113
scaling3: 0.02247191
scaling4: 0.0
scaling5: 0.0
timelineCount: 6
timeline0: 0.0
timeline1: 0.0448
timeline2: 0.1264
timeline3: 0.3328
timeline4: 0.6992
timeline5: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/global_combo_sending1.png


+combo_success3
- Delay -
active: false
- Duration - 
lowMin: 600.0
lowMax: 600.0
- Count - 
min: 1
max: 200
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 60.0
highMax: 60.0
relative: true
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 600.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 200.0
relative: false
scalingCount: 5
scaling0: 1.0
scaling1: 0.50980395
scaling2: 0.21568628
scaling3: 0.05882353
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.1849315
timeline2: 0.39726028
timeline3: 0.6643836
timeline4: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 1.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.92941177
colors1: 0.92941177
colors2: 0.92941177
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.9649123
scaling2: 0.31460676
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.19178082
timeline2: 0.48372093
timeline3: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/pre_particle.png


*color
- Delay -
active: false
- Duration - 
lowMin: 1000.0
lowMax: 1000.0
- Count - 
min: 0
max: 25
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 32.0
highMax: 32.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Scale - 
active: false
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 0.4745098
colors2: 0.77254903
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/global_empty_particle.jpg
particle.png

