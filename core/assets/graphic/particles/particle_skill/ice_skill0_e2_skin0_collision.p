+ice_e2
- Delay -
active: false
- Duration - 
lowMin: 100.0
lowMax: 100.0
- Count - 
min: 3
max: 320
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 25.0
highMax: 25.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1350.0
highMax: 300.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 150.0
relative: false
scalingCount: 5
scaling0: 0.7058824
scaling1: 0.9019608
scaling2: 0.64705884
scaling3: 0.3529412
scaling4: 0.21568628
timelineCount: 5
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.1780822
timeline3: 0.33561644
timeline4: 0.5068493
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 1400.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.9719101
scaling2: 0.10674157
scaling3: 0.061797753
timelineCount: 4
timeline0: 0.0
timeline1: 0.11660079
timeline2: 0.25889328
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 80.0
highMax: -80.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.7254902
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -800.0
highMax: -800.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.13725491
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.65068495
timeline2: 1.0
- Tint - 
colorsCount: 3
colors0: 0.79607844
colors1: 0.92156863
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.7719298
scaling2: 0.28070176
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.16438356
timeline2: 0.4178082
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/ice_skill0_4.png


glow1
- Delay -
active: false
- Duration - 
lowMin: 50.0
lowMax: 50.0
- Count - 
min: 3
max: 120
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 60.0
highMax: 60.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 400.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 6
scaling0: 0.7058824
scaling1: 0.9019608
scaling2: 0.64705884
scaling3: 0.3529412
scaling4: 0.21568628
scaling5: 0.11764706
timelineCount: 6
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.1780822
timeline3: 0.33561644
timeline4: 0.5068493
timeline5: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 400.0
highMax: 400.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.9719101
scaling2: 0.14044943
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.17125984
timeline2: 0.26771653
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.7254902
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.5568628
colors1: 0.8980392
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 0.9719101
scaling2: 0.20224719
scaling3: 0.06741573
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.09549549
timeline2: 0.24845996
timeline3: 0.42505133
timeline4: 0.7330595
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_spark1.png


ice_e2_2
- Delay -
active: false
- Duration - 
lowMin: 150.0
lowMax: 150.0
- Count - 
min: 3
max: 320
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 55.0
highMax: 55.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1350.0
highMax: 300.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 50.0
relative: false
scalingCount: 5
scaling0: 0.7058824
scaling1: 0.9019608
scaling2: 0.64705884
scaling3: 0.3529412
scaling4: 0.21568628
timelineCount: 5
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.1780822
timeline3: 0.33561644
timeline4: 0.5068493
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 250.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.9719101
scaling2: 0.10674157
scaling3: 0.061797753
timelineCount: 4
timeline0: 0.0
timeline1: 0.11660079
timeline2: 0.25889328
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 60.0
highMax: -60.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.7254902
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -800.0
highMax: -800.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.0
scaling2: 0.27450982
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.4178082
timeline2: 0.7808219
timeline3: 1.0
- Tint - 
colorsCount: 3
colors0: 0.78431374
colors1: 0.94509804
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.98245615
scaling2: 0.47368422
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.77397263
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/ice_skill0_4.png


+ice_e2_3
- Delay -
active: false
- Duration - 
lowMin: 100.0
lowMax: 100.0
- Count - 
min: 3
max: 320
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 15.0
highMax: 15.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1350.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 150.0
relative: false
scalingCount: 5
scaling0: 0.7058824
scaling1: 0.9019608
scaling2: 0.64705884
scaling3: 0.3529412
scaling4: 0.21568628
timelineCount: 5
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.1780822
timeline3: 0.33561644
timeline4: 0.5068493
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 1200.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.9719101
scaling2: 0.10674157
scaling3: 0.061797753
timelineCount: 4
timeline0: 0.0
timeline1: 0.11660079
timeline2: 0.25889328
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 80.0
highMax: -80.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.7254902
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -800.0
highMax: -800.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.13725491
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.65068495
timeline2: 1.0
- Tint - 
colorsCount: 3
colors0: 0.6784314
colors1: 0.8
colors2: 0.8784314
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.7719298
scaling2: 0.28070176
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.16438356
timeline2: 0.4178082
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/ice_skill0_4.png


glow1_spark
- Delay -
active: false
- Duration - 
lowMin: 50.0
lowMax: 50.0
- Count - 
min: 3
max: 120
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 20.0
highMax: 20.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 400.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 1500.0
highMax: 1500.0
relative: false
scalingCount: 6
scaling0: 0.7058824
scaling1: 0.9019608
scaling2: 0.64705884
scaling3: 0.3529412
scaling4: 0.21568628
scaling5: 0.11764706
timelineCount: 6
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.1780822
timeline3: 0.33561644
timeline4: 0.5068493
timeline5: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 400.0
highMax: 400.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.9719101
scaling2: 0.14044943
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.17125984
timeline2: 0.26771653
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.7254902
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.5568628
colors1: 0.8980392
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.45614034
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.11643836
timeline2: 0.7330595
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_spark1.png


*color
- Delay -
active: false
- Duration - 
lowMin: 1000.0
lowMax: 1000.0
- Count - 
min: 0
max: 25
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 32.0
highMax: 32.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Scale - 
active: false
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.3529412
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/global_empty_particle.jpg
particle.png

