sparks
- Delay -
active: false
- Duration - 
lowMin: 600.0
lowMax: 600.0
- Count - 
min: 10
max: 120
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 10.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 600.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 350.0
highMax: 220.0
relative: false
scalingCount: 6
scaling0: 0.27450982
scaling1: 0.8876405
scaling2: 0.6123595
scaling3: 0.3529412
scaling4: 0.19607843
scaling5: 0.11764706
timelineCount: 6
timeline0: 0.0
timeline1: 0.21256039
timeline2: 0.29307568
timeline3: 0.43150684
timeline4: 0.6164383
timeline5: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 400.0
highMax: 400.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 100.0
lowMax: 90.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.7254902
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.40784314
colors1: 0.57254905
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.9298246
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.15753424
timeline2: 1.0
- Options - 
attached: true
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_spark1.png


glow1_new
- Delay -
active: false
- Duration - 
lowMin: 1000.0
lowMax: 1000.0
- Count - 
min: 1
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 5.0
highMax: 5.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 1000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 600.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Scale - 
active: false
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.24313726
colors1: 0.30980393
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.9122807
scaling2: 0.6315789
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.10958904
timeline2: 0.72602737
timeline3: 1.0
- Options - 
attached: true
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_spark1.png


tail_back1
- Delay -
active: false
- Duration - 
lowMin: 600.0
lowMax: 600.0
- Count - 
min: 1
max: 90
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 20.0
highMax: 20.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 400.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 150.0
lowMax: 150.0
highMin: 200.0
highMax: 550.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 400.0
highMax: 400.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 1.0
scaling2: 0.0
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.09589041
timeline2: 0.19178082
timeline3: 1.0
- Angle - 
active: true
lowMin: 100.0
lowMax: 90.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.5964912
scaling2: 0.14035088
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.14383562
timeline2: 0.30821916
timeline3: 1.0
- Options - 
attached: true
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/lightning_skill2_4.png

