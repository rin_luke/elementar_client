sparks1
- Delay -
active: false
- Duration - 
lowMin: 1000.0
lowMax: 1000.0
- Count - 
min: 1
max: 220
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 60.0
highMax: 60.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 500.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 50.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.30136988
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 270.0
highMax: 120.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Angle - 
active: true
lowMin: 100.0
lowMax: 90.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.92156863
colors1: 1.0
colors2: 0.654902
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 7
scaling0: 0.0
scaling1: 1.0
scaling2: 0.16853933
scaling3: 1.0
scaling4: 0.16853933
scaling5: 0.98876405
scaling6: 0.0
timelineCount: 7
timeline0: 0.0
timeline1: 0.09931507
timeline2: 0.2579365
timeline3: 0.38356164
timeline4: 0.5119048
timeline5: 0.6421233
timeline6: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_spark1.png
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/light_skill2_3.png


sparks2
- Delay -
active: false
- Duration - 
lowMin: 1000.0
lowMax: 1000.0
- Count - 
min: 1
max: 220
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 60.0
highMax: 60.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 500.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 50.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.30136988
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 270.0
highMax: 120.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Angle - 
active: true
lowMin: 100.0
lowMax: 90.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 0.67058825
colors2: 0.5882353
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 7
scaling0: 0.0
scaling1: 1.0
scaling2: 0.16853933
scaling3: 1.0
scaling4: 0.16853933
scaling5: 0.98876405
scaling6: 0.0
timelineCount: 7
timeline0: 0.0
timeline1: 0.09931507
timeline2: 0.2579365
timeline3: 0.38356164
timeline4: 0.5119048
timeline5: 0.6421233
timeline6: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_spark1.png
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/light_skill2_3.png


sparks3
- Delay -
active: false
- Duration - 
lowMin: 1000.0
lowMax: 1000.0
- Count - 
min: 1
max: 220
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 60.0
highMax: 60.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 50.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.30136988
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 270.0
highMax: 120.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 100.0
lowMax: 90.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.6745098
colors1: 1.0
colors2: 0.9372549
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 7
scaling0: 0.0
scaling1: 1.0
scaling2: 0.16853933
scaling3: 1.0
scaling4: 0.21052632
scaling5: 1.0
scaling6: 0.0
timelineCount: 7
timeline0: 0.0
timeline1: 0.09931507
timeline2: 0.2579365
timeline3: 0.38356164
timeline4: 0.53424656
timeline5: 0.7191781
timeline6: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_spark1.png
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/light_skill2_3.png


glow1_new
- Delay -
active: false
- Duration - 
lowMin: 0.0
lowMax: 0.0
- Count - 
min: 1
max: 1
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 1000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Scale - 
active: false
- Velocity - 
active: false
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 0.8862745
colors2: 0.57254905
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.6315789
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.16438356
timeline2: 1.0
- Options - 
attached: true
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/pre_particle.png
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_spark1.png

