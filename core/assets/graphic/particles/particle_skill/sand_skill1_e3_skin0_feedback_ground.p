+sand_small_e3_1
- Delay -
active: false
- Duration - 
lowMin: 100.0
lowMax: 100.0
- Count - 
min: 20
max: 320
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 10.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 650.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 250.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 0.9019608
scaling2: 0.64705884
scaling3: 0.3529412
scaling4: 0.21568628
timelineCount: 5
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.1780822
timeline3: 0.33561644
timeline4: 0.5068493
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 700.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.92156863
scaling2: 0.19607843
scaling3: 0.061797753
timelineCount: 4
timeline0: 0.0
timeline1: 0.28767124
timeline2: 0.53424656
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 90.0
highMax: -90.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.7254902
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.7368421
scaling2: 0.50877196
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.15068494
timeline2: 0.65068495
timeline3: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: false
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/sand_skill2_2.png


+sand_small_e3
- Delay -
active: false
- Duration - 
lowMin: 120.0
lowMax: 120.0
- Count - 
min: 1
max: 320
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 650.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 150.0
relative: false
scalingCount: 5
scaling0: 0.7058824
scaling1: 0.9019608
scaling2: 0.64705884
scaling3: 0.3529412
scaling4: 0.21568628
timelineCount: 5
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.1780822
timeline3: 0.33561644
timeline4: 0.5068493
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 1000.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.88235295
scaling2: 0.13725491
scaling3: 0.061797753
timelineCount: 4
timeline0: 0.0
timeline1: 0.21917808
timeline2: 0.39726028
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.75438595
scaling2: 0.2982456
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.16438356
timeline2: 0.44520548
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: false
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/sand_skill2_1.png


sand_small_e3_2
- Delay -
active: false
- Duration - 
lowMin: 120.0
lowMax: 120.0
- Count - 
min: 1
max: 320
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 650.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 400.0
highMax: 650.0
relative: false
scalingCount: 5
scaling0: 0.7058824
scaling1: 0.9019608
scaling2: 0.64705884
scaling3: 0.3529412
scaling4: 0.21568628
timelineCount: 5
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.1780822
timeline3: 0.33561644
timeline4: 0.5068493
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 1000.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.88235295
scaling2: 0.13725491
scaling3: 0.061797753
timelineCount: 4
timeline0: 0.0
timeline1: 0.21917808
timeline2: 0.39726028
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.2982456
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.1780822
timeline2: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: false
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/sand_skill2_1.png


sand_small_e3_spark
- Delay -
active: false
- Duration - 
lowMin: 120.0
lowMax: 120.0
- Count - 
min: 1
max: 320
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 750.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 50.0
relative: false
scalingCount: 5
scaling0: 0.7058824
scaling1: 0.9019608
scaling2: 0.64705884
scaling3: 0.3529412
scaling4: 0.21568628
timelineCount: 5
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.1780822
timeline3: 0.33561644
timeline4: 0.5068493
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 1000.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.88235295
scaling2: 0.13725491
scaling3: 0.061797753
timelineCount: 4
timeline0: 0.0
timeline1: 0.21917808
timeline2: 0.39726028
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 0.77254903
colors2: 0.6627451
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.8947368
scaling2: 0.36842105
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.15068494
timeline2: 0.48630136
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_spark1.png


sand_small_e3_spark2
- Delay -
active: false
- Duration - 
lowMin: 120.0
lowMax: 120.0
- Count - 
min: 1
max: 320
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 350.0
highMax: 350.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 450.0
relative: false
scalingCount: 5
scaling0: 0.7058824
scaling1: 0.9019608
scaling2: 0.64705884
scaling3: 0.3529412
scaling4: 0.21568628
timelineCount: 5
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.1780822
timeline3: 0.33561644
timeline4: 0.5068493
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 300.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.88235295
scaling2: 0.13725491
scaling3: 0.061797753
timelineCount: 4
timeline0: 0.0
timeline1: 0.21917808
timeline2: 0.39726028
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 0.70980394
colors2: 0.6627451
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 1.0
scaling2: 0.9649123
scaling3: 0.31578946
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.20547946
timeline3: 0.32876712
timeline4: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_spark1.png

