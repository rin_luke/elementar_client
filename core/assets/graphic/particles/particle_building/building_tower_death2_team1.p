glow1 small
- Delay -
active: false
- Duration - 
lowMin: 100.0
lowMax: 100.0
- Count - 
min: 10
max: 35
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 20.0
highMax: 20.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 600.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: true
lowMin: 200.0
lowMax: 200.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 150.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 150.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 230.0
highMax: 400.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 360.0
highMax: 430.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: false
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 400.0
highMax: 400.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Tint - 
colorsCount: 3
colors0: 0.12156863
colors1: 0.57254905
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 1.0
scaling2: 0.28070176
scaling3: 0.28070176
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.04109589
timeline2: 0.13013698
timeline3: 0.33561644
timeline4: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/pre_particle.png


glow2
- Delay -
active: false
- Duration - 
lowMin: 300.0
lowMax: 300.0
- Count - 
min: 10
max: 10
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 23.0
highMax: 23.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 2000.0
highMax: 2000.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life Offset - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 200.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Offset - 
active: false
- Y Offset - 
active: true
lowMin: 200.0
lowMax: 200.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 540.0
highMax: 600.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 150.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.0
scaling2: 0.98039216
timelineCount: 3
timeline0: 0.0
timeline1: 0.29452056
timeline2: 0.43150684
- Tint - 
colorsCount: 3
colors0: 0.12156863
colors1: 0.57254905
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.28070176
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.05479452
timeline2: 0.1780822
timeline3: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/pre_particle.png


glow3 white
- Delay -
active: false
- Duration - 
lowMin: 100.0
lowMax: 100.0
- Count - 
min: 10
max: 10
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 2.0
highMax: 2.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 1000.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life Offset - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 200.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Offset - 
active: false
- Y Offset - 
active: true
lowMin: 200.0
lowMax: 200.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 900.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 150.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.0
scaling2: 0.98039216
timelineCount: 3
timeline0: 0.0
timeline1: 0.29452056
timeline2: 0.43150684
- Tint - 
colorsCount: 3
colors0: 0.6901961
colors1: 0.8509804
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.5449438
scaling2: 0.1741573
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.027746947
timeline2: 0.05771365
timeline3: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/pre_particle.png


small1
- Delay -
active: false
- Duration - 
lowMin: 300.0
lowMax: 300.0
- Count - 
min: 10
max: 120
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 2700.0
highMax: 2700.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: true
lowMin: 200.0
lowMax: 200.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 130.0
highMax: 130.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 130.0
highMax: 130.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 25.0
highMax: 10.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.11764706
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 160.0
highMax: 360.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.24657534
timeline2: 1.0
- Angle - 
active: true
lowMin: 100.0
lowMax: 90.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: -100.0
lowMax: -100.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.0
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.47945204
timeline2: 1.0
- Gravity - 
active: true
lowMin: -100.0
lowMax: 100.0
highMin: 200.0
highMax: 300.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Tint - 
colorsCount: 3
colors0: 0.44313726
colors1: 0.627451
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 7
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
scaling3: 1.0
scaling4: 0.0
scaling5: 1.0
scaling6: 0.0
timelineCount: 7
timeline0: 0.0
timeline1: 0.15068494
timeline2: 0.29452056
timeline3: 0.46575344
timeline4: 0.60958904
timeline5: 0.79452056
timeline6: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_fluffy3.png


small2
- Delay -
active: false
- Duration - 
lowMin: 300.0
lowMax: 300.0
- Count - 
min: 10
max: 120
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 40.0
highMax: 40.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 3000.0
highMax: 3000.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: true
lowMin: 200.0
lowMax: 200.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 80.0
highMax: 80.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 80.0
highMax: 80.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 20.0
highMax: 35.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.11764706
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 360.0
highMax: 560.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.84313726
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.6849315
timeline2: 1.0
- Angle - 
active: true
lowMin: 100.0
lowMax: 90.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Wind - 
active: true
lowMin: -300.0
lowMax: -300.0
highMin: 300.0
highMax: 300.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.0
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.5
timeline2: 1.0
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Tint - 
colorsCount: 3
colors0: 0.44313726
colors1: 0.627451
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 9
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
scaling3: 1.0
scaling4: 0.0
scaling5: 1.0
scaling6: 0.0
scaling7: 1.0
scaling8: 0.0
timelineCount: 9
timeline0: 0.0
timeline1: 0.08219178
timeline2: 0.19863014
timeline3: 0.29452056
timeline4: 0.4520548
timeline5: 0.56164384
timeline6: 0.6849315
timeline7: 0.8082192
timeline8: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_fluffy3.png


small3
- Delay -
active: false
- Duration - 
lowMin: 300.0
lowMax: 300.0
- Count - 
min: 10
max: 1120
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 330.0
highMax: 330.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 2200.0
highMax: 2200.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: true
lowMin: 200.0
lowMax: 200.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 80.0
highMax: 80.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 80.0
highMax: 80.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 15.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.11764706
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 260.0
highMax: 420.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.88235295
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.6643836
timeline2: 1.0
- Angle - 
active: true
lowMin: 100.0
lowMax: 90.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Wind - 
active: true
lowMin: -200.0
lowMax: -100.0
highMin: 200.0
highMax: 100.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.0
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.5
timeline2: 1.0
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 200.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Tint - 
colorsCount: 3
colors0: 0.44313726
colors1: 0.627451
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 11
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
scaling3: 1.0
scaling4: 0.0
scaling5: 1.0
scaling6: 0.0
scaling7: 1.0
scaling8: 0.0
scaling9: 1.0
scaling10: 0.0
timelineCount: 11
timeline0: 0.0
timeline1: 0.0989899
timeline2: 0.1919192
timeline3: 0.2929293
timeline4: 0.40202022
timeline5: 0.4989899
timeline6: 0.6
timeline7: 0.6989899
timeline8: 0.80404043
timeline9: 0.8989899
timeline10: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_fluffy3.png


small4 +
- Delay -
active: false
- Duration - 
lowMin: 600.0
lowMax: 600.0
- Count - 
min: 10
max: 520
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 250.0
highMax: 250.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1500.0
highMax: 1500.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: true
lowMin: 200.0
lowMax: 200.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 130.0
highMax: 130.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 130.0
highMax: 130.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 15.0
highMax: 5.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.11764706
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 280.0
highMax: 400.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.8039216
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Angle - 
active: true
lowMin: 100.0
lowMax: 90.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Wind - 
active: true
lowMin: -500.0
lowMax: -500.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.5
timeline2: 1.0
- Gravity - 
active: true
lowMin: -400.0
lowMax: 400.0
highMin: 400.0
highMax: 200.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Tint - 
colorsCount: 3
colors0: 0.44313726
colors1: 0.627451
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.7894737
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.05479452
timeline2: 0.80136985
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_grungy1.png


shockwave
- Delay -
active: false
- Duration - 
lowMin: 100.0
lowMax: 100.0
- Count - 
min: 2
max: 3
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 12.0
highMax: 12.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 1000.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: true
lowMin: 180.0
lowMax: 200.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 250.0
lowMax: 30.0
highMin: 1800.0
highMax: 1600.0
relative: false
scalingCount: 7
scaling0: 0.0
scaling1: 0.35955057
scaling2: 0.5955056
scaling3: 0.7303371
scaling4: 0.8707865
scaling5: 0.93820226
scaling6: 1.0
timelineCount: 7
timeline0: 0.0
timeline1: 0.107064016
timeline2: 0.20860927
timeline3: 0.3200883
timeline4: 0.48013246
timeline5: 0.678808
timeline6: 1.0
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.12156863
colors1: 0.57254905
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 6
scaling0: 0.0
scaling1: 0.54385966
scaling2: 0.2982456
scaling3: 0.10526316
scaling4: 0.03508772
scaling5: 0.0
timelineCount: 6
timeline0: 0.0
timeline1: 0.11643836
timeline2: 0.20547946
timeline3: 0.36301368
timeline4: 0.58219177
timeline5: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_shockwave_512.png
