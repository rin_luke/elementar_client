particle_building_base_core
- Delay -
active: false
- Duration - 
lowMin: 3000.0
lowMax: 3000.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 10.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 3000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 10.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 40.0
lowMax: 155.0
highMin: 50.0
highMax: 120.0
relative: false
scalingCount: 4
scaling0: 0.8235294
scaling1: 0.9019608
scaling2: 0.19607843
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.1369863
timeline2: 0.43150684
timeline3: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 80.0
highMax: 40.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 220.0
highMax: 320.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 360.0
lowMax: 1.0
highMin: 1.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -150.0
highMax: -150.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Tint - 
colorsCount: 3
colors0: 0.36862746
colors1: 0.6039216
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.19298245
scaling2: 0.42105263
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.34246576
timeline2: 0.72602737
timeline3: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: random
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_fluffy3.png
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_fire1.png


particle_building_base_core2
- Delay -
active: false
- Duration - 
lowMin: 3000.0
lowMax: 3000.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 20.0
highMax: 20.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 2000.0
highMax: 1000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 10.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 120.0
lowMax: 120.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.9019608
scaling2: 0.19607843
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.1369863
timeline2: 0.43150684
timeline3: 1.0
- Y Scale - 
active: true
lowMin: 150.0
lowMax: 150.0
highMin: 450.0
highMax: 450.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 80.0
highMax: 40.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 220.0
highMax: 320.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -200.0
highMax: -200.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Tint - 
colorsCount: 3
colors0: 0.36862746
colors1: 0.6039216
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.2631579
scaling2: 0.2631579
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.11643836
timeline2: 0.7123288
timeline3: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_beam1.png
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_fire1.png


leaves
- Delay -
active: false
- Duration - 
lowMin: 3000.0
lowMax: 3000.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 5.0
highMax: 5.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 2000.0
highMax: 4000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 10.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 25.0
highMax: 15.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.78431374
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.63013697
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 880.0
highMax: 640.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.88235295
scaling2: 0.1764706
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.15753424
timeline2: 0.34931508
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 240.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 360.0
lowMax: 1.0
highMin: 1.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: -400.0
lowMax: -400.0
highMin: 400.0
highMax: 400.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.5
timeline2: 1.0
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 150.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Tint - 
colorsCount: 3
colors0: 0.36862746
colors1: 0.6039216
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 0.8947368
scaling2: 0.80701756
scaling3: 0.15789473
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.09589041
timeline2: 0.39726028
timeline3: 0.6232877
timeline4: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_leaf1.png

