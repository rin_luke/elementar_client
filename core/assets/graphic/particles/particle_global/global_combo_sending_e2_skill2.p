*combo_success2_colored
- Delay -
active: true
lowMin: 500.0
lowMax: 500.0
- Duration - 
lowMin: 200.0
lowMax: 200.0
- Count - 
min: 1
max: 1000
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 20.0
highMax: 20.0
relative: true
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 200.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 300.0
lowMax: 300.0
highMin: 2000.0
highMax: 2000.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.39215687
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.60273975
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 180.0
highMax: 180.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 360.0
highMax: 1.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.7254902
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.91573036
scaling2: 0.35955057
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.08199643
timeline2: 0.35472372
timeline3: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/global_combo_sending1.png


+flying_sparks1
- Delay -
active: false
- Duration - 
lowMin: 200.0
lowMax: 200.0
- Count - 
min: 1
max: 190
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 60.0
highMax: 60.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 600.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: true
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 2000.0
highMax: 2000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 2000.0
highMax: 2000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 10.0
lowMax: 10.0
highMin: 150.0
highMax: 250.0
relative: false
scalingCount: 4
scaling0: 0.9411765
scaling1: 0.88235295
scaling2: 0.13725491
scaling3: 0.6666667
timelineCount: 4
timeline0: 0.0
timeline1: 0.36301368
timeline2: 0.79452056
timeline3: 0.8767123
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: -4000.0
lowMax: -4000.0
highMin: 1200.0
highMax: 1300.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 1.0
scaling2: 0.0
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.14383562
timeline2: 0.8082192
timeline3: 1.0
- Angle - 
active: true
lowMin: 100.0
lowMax: 90.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.8651685
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.87463975
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/global_empty_particle.jpg
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_round2_64.png

