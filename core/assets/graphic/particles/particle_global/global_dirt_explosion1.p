dirt_attached
- Delay -
active: false
- Duration - 
lowMin: 1.0
lowMax: 1.0
- Count - 
min: 1
max: 1
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.28767124
timeline2: 0.34246576
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 2000.0
highMax: 2000.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life Offset - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 30.0
highMax: 30.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 1.0
lowMax: 1.0
highMin: 10.0
highMax: 70.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.60784316
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.34931508
timeline2: 1.0
- Angle - 
active: true
lowMin: -90.0
lowMax: -90.0
highMin: 1.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 0.33333334
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Tint - 
colorsCount: 3
colors0: 0.36862746
colors1: 0.30980393
colors2: 0.25882354
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 0.71929824
scaling2: 0.75438595
scaling3: 0.0
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.10273973
timeline2: 0.70547944
timeline3: 0.9109589
timeline4: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: false
behind: false
premultipliedAlpha: false
- Image Path -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_stone1.png
