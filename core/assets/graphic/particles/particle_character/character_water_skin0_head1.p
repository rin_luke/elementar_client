particle_character_water2
- Delay -
active: false
- Duration - 
lowMin: 5000.0
lowMax: 5000.0
- Count - 
min: 1
max: 580
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 10.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 5500.0
highMax: 4000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 30.0
highMax: 30.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 30.0
highMax: 30.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 20.0
lowMax: 20.0
highMin: 450.0
highMax: 640.0
relative: false
scalingCount: 3
scaling0: 0.29411766
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.4041096
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 150.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 1.0
scaling2: 0.7254902
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.33561644
timeline2: 0.6643836
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 180.0
highMax: 180.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 360.0
lowMax: 360.0
highMin: 180.0
highMax: 180.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.42465752
- Wind - 
active: false
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -500.0
highMax: -500.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.34931508
- Tint - 
colorsCount: 3
colors0: 0.59607846
colors1: 0.85882354
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 0.30898875
scaling2: 0.11797753
scaling3: 0.0
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.048611112
timeline2: 0.079166666
timeline3: 0.12361111
timeline4: 1.0
- Options - 
attached: true
continuous: true
aligned: false
additive: false
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/water_skill2_1.png


particle_character_water1_additive
- Delay -
active: false
- Duration - 
lowMin: 1000.0
lowMax: 1000.0
- Count - 
min: 10
max: 180
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 20.0
highMax: 20.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 5500.0
highMax: 4000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: true
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 30.0
highMax: 30.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 30.0
highMax: 30.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 20.0
lowMax: 20.0
highMin: 350.0
highMax: 210.0
relative: false
scalingCount: 3
scaling0: 0.29411766
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.4041096
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 100.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 1.0
scaling2: 0.7254902
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.33561644
timeline2: 0.6643836
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 180.0
highMax: 180.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 360.0
lowMax: 360.0
highMin: 180.0
highMax: 180.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -500.0
highMax: -500.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.34931508
- Tint - 
colorsCount: 3
colors0: 0.35686275
colors1: 0.78039217
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 0.12921348
scaling2: 0.039325844
scaling3: 0.0
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.031944446
timeline2: 0.094444446
timeline3: 0.1736111
timeline4: 1.0
- Options - 
attached: true
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/water_skill2_1.png

