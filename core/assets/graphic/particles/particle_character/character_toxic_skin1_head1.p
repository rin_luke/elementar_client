big_pulse
- Delay -
active: false
- Duration - 
lowMin: 1000.0
lowMax: 1000.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 10.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 1000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: ellipse
edges: true
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 30.0
highMax: 30.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 20.0
highMax: 20.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 80.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.47260273
timeline2: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 50.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 280.0
highMax: 260.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 1.0
lowMax: 360.0
highMin: 360.0
highMax: 0.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.43529412
colors1: 1.0
colors2: 0.56078434
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.40350878
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.48630136
timeline2: 1.0
- Options - 
attached: true
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_grungy1.png


big_pulse
- Delay -
active: false
- Duration - 
lowMin: 1000.0
lowMax: 1000.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 10.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 1000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: ellipse
edges: true
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 40.0
highMax: 40.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 10.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 30.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.47260273
timeline2: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 70.0
highMax: 20.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 120.0
highMax: 340.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 360.0
highMax: 0.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.43529412
colors1: 1.0
colors2: 0.56078434
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.16438356
timeline2: 1.0
- Options - 
attached: true
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_fluffy1.png
