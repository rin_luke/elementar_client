Untitled
- Delay -
active: false
- Duration - 
lowMin: 1000.0
lowMax: 1000.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 10.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 1000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 120.0
relative: false
scalingCount: 2
scaling0: 0.49019608
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 10.0
lowMax: 10.0
highMin: 30.0
highMax: 250.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 1.0
scaling2: 0.19607843
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.29452056
timeline2: 0.6164383
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 180.0
highMax: 230.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 1.0
lowMax: 360.0
highMin: 70.0
highMax: 0.0
relative: true
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.0
colors1: 0.0
colors2: 0.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.28070176
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.2260274
timeline2: 0.5
timeline3: 1.0
- Options - 
attached: true
continuous: true
aligned: false
additive: false
behind: false
premultipliedAlpha: false
- Image Path -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_grungy1.png
