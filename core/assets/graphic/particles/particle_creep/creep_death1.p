crystals explosion
- Delay -
active: false
- Duration - 
lowMin: 120.0
lowMax: 120.0
- Count - 
min: 0
max: 250
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 800.0
highMax: 800.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 100.0
lowMax: 200.0
highMin: 30.0
highMax: 50.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 20.0
highMax: 245.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 2000.0
highMax: 1000.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.92156863
scaling2: 0.23529412
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.21232876
timeline2: 0.39726028
timeline3: 0.96575344
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 360.0
highMax: 1.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.31764707
colors1: 0.77254903
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.9325843
scaling2: 0.28651685
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.027659575
timeline2: 0.3106383
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_creep_death3.png


glow explosion1
- Delay -
active: false
- Duration - 
lowMin: 100.0
lowMax: 100.0
- Count - 
min: 0
max: 250
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 800.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 100.0
lowMax: 200.0
highMin: 30.0
highMax: 50.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 150.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 2000.0
highMax: 600.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.9411765
scaling2: 0.09803922
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.31506848
timeline2: 0.55479455
timeline3: 0.96575344
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 360.0
highMax: 1.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 360.0
lowMax: -360.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.41960785
colors1: 0.80784315
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 1.0
scaling2: 0.8146067
scaling3: 0.46067417
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.013793103
timeline2: 0.10344828
timeline3: 0.35
timeline4: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_spark1.png


glow explosion2
- Delay -
active: false
- Duration - 
lowMin: 100.0
lowMax: 100.0
- Count - 
min: 0
max: 250
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 800.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 100.0
lowMax: 200.0
highMin: 30.0
highMax: 50.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 3000.0
highMax: 250.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 4000.0
highMax: 200.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.9411765
scaling2: 0.09803922
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.31506848
timeline2: 0.55479455
timeline3: 0.96575344
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 360.0
highMax: 1.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 360.0
lowMax: -360.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.13333334
colors1: 0.8745098
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 6
scaling0: 0.0
scaling1: 0.66853935
scaling2: 0.37078652
scaling3: 0.21348314
scaling4: 0.0
scaling5: 0.0
timelineCount: 6
timeline0: 0.0
timeline1: 0.02586207
timeline2: 0.070689656
timeline3: 0.15
timeline4: 0.27586207
timeline5: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/pre_particle.png
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_fluffy2.png


crystals explosion2
- Delay -
active: false
- Duration - 
lowMin: 120.0
lowMax: 120.0
- Count - 
min: 0
max: 250
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1200.0
highMax: 1200.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 100.0
lowMax: 200.0
highMin: 30.0
highMax: 50.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 60.0
highMax: 10.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 2000.0
highMax: 200.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.92156863
scaling2: 0.23529412
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.21232876
timeline2: 0.39726028
timeline3: 0.96575344
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 360.0
highMax: 1.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 1000.0
lowMax: -1000.0
highMin: -1000.0
highMax: 1000.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.023529412
colors1: 0.8352941
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.9325843
scaling2: 0.28651685
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.027659575
timeline2: 0.3106383
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
/E:/Meine Dateien/Projekte/Elementar/Animationen und Sprites/animation_characters/characters/particle_sprites/particle_creep_death3.png

