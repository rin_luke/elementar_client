#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform sampler2D u_texture;
uniform vec2 resolution;

varying vec4 v_color;
varying vec2 v_texCoord;

//RADIUS of our vignette, where 0.5 results in a circle fitting the screen
const float RADIUS = 1.0;

//softness of our vignette, between 0.0 and 1.0
const float SOFTNESS = 0.75;

void main() {
   //sample our texture
	vec4 texColor = v_color*texture2D(u_texture, v_texCoord);
	
	//determine origin
	vec2 position = (gl_FragCoord.xy / resolution.xy) - vec2(0.5);
	
	position.x *= resolution.x / resolution.y;
	//determine the vector length of the center position
	float len = length(position);
	
	//our vignette effect, using smoothstep
	float vignette = smoothstep(RADIUS, RADIUS-SOFTNESS, len);
	
	//apply our vignette
	texColor.rgb *= vignette;
	
	gl_FragColor = texColor;
}