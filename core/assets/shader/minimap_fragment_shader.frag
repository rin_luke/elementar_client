			
uniform sampler2D u_texture;

varying vec4 v_color;
varying vec2 v_texCoord;

void main() {
	vec4 myColor = v_color * texture2D(u_texture, v_texCoord);
	
	myColor.r = 0.058;
	myColor.g = 0.074;
	myColor.b = 0.094;

	gl_FragColor = myColor;
}